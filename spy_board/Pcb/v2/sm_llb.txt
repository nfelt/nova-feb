$PACKAGES
'SMC0402' ! 'SMC0402' ; SMC0402
'SMR0402' ! 'SMR0402' ; SMR0402
'SMSC70ECB' ! 'SMSC70ECB' ; SMSC70ECB
'SMSC70EBC' ! 'SMSC70EBC' ; SMSC70EBC
'SMSC70BEC' ! 'SMSC70BEC' ; SMSC70BEC
'SMSC70123' ! 'SMSC70123' ; SMSC70123
'SMC0603' ! 'SMC0603' ; SMC0603
'SMR0603' ! 'SMR0603' ; SMR0603
'SMC0504' ! 'SMC0504' ; SMC0504
'SMSOT23GDS' ! 'SMSOT23GDS' ; SMSOT23GDS
'SMSOT23DGS' ! 'SMSOT23DGS' ; SMSOT23DGS
'SMSOT23123' ! 'SMSOT23123' ; SMSOT23123
'SMSOT23SGD' ! 'SMSOT23SGD' ; SMSOT23SGD
'SMSOT23EBC' ! 'SMSOT23EBC' ; SMSOT23EBC
'SMSOT23ECB' ! 'SMSOT23ECB' ; SMSOT23ECB
'SMSOT23BCE' ! 'SMSOT23BCE' ; SMSOT23BCE
'SMSOT23DSG' ! 'SMSOT23DSG' ; SMSOT23DSG
'SMSOT23GSD' ! 'SMSOT23GSD' ; SMSOT23GSD
'SMSOT23CBE' ! 'SMSOT23CBE' ; SMSOT23CBE
'SMSOT23SDG' ! 'SMSOT23SDG' ; SMSOT23SDG
'SMSOT23231' ! 'SMSOT23231' ; SMSOT23231
'SMSOT23312' ! 'SMSOT23312' ; SMSOT23312
'SMSOT23BEC' ! 'SMSOT23BEC' ; SMSOT23BEC
'SMSC59ECB' ! 'SMSC59ECB' ; SMSC59ECB
'SMSC59BEC' ! 'SMSC59BEC' ; SMSC59BEC
'SMSC59EBC' ! 'SMSC59EBC' ; SMSC59EBC
'SMSC59123' ! 'SMSC59123' ; SMSC59123
'SML0805' ! 'SML0805' ; SML0805
'SMR0805' ! 'SMR0805' ; SMR0805
'SMC0805' ! 'SMC0805' ; SMC0805
'SMSOT1431234' ! 'SMSOT1431234' ; SMSOT1431234
'SMSOT143CEBE' ! 'SMSOT143CEBE' ; SMSOT143CEBE
'SMSOT143BCEC' ! 'SMSOT143BCEC' ; SMSOT143BCEC
'SMSOT143CBEE' ! 'SMSOT143CBEE' ; SMSOT143CBEE
'SMD0805' ! 'SMD0805' ; SMD0805
'SMD080521' ! 'SMD080521' ; SMD080521
'SMD080512' ! 'SMD080512' ; SMD080512
'SMSOT89123' ! 'SMSOT89123' ; SMSOT89123
'SMSOT89SDG' ! 'SMSOT89SDG' ; SMSOT89SDG
'SMSOT891X3' ! 'SMSOT891X3' ; SMSOT891X3
'SMSOT89EBC' ! 'SMSOT89EBC' ; SMSOT89EBC
'SML1206' ! 'SML1206' ; SML1206
'SML1110' ! 'SML1110' ; SML1110
'SML1212' ! 'SML1212' ; SML1212
'SMD1206' ! 'SMD1206' ; SMD1206
'SMD120612' ! 'SMD120612' ; SMD120612
'SMD120621' ! 'SMD120621' ; SMD120621
'SML1210' ! 'SML1210' ; SML1210
'SMR1206' ! 'SMR1206' ; SMR1206
'SMC1206' ! 'SMC1206' ; SMC1206
'SMR1210' ! 'SMR1210' ; SMR1210
'SMC1210' ! 'SMC1210' ; SMC1210
'SMDO213AA' ! 'SMDO213AA' ; SMDO213AA
'SMDO213AA21' ! 'SMDO213AA21' ; SMDO213AA21
'SMDO213AA12' ! 'SMDO213AA12' ; SMDO213AA12
'SMD1406' ! 'SMD1406' ; SMD1406
'SMD140612' ! 'SMD140612' ; SMD140612
'SMD140621' ! 'SMD140621' ; SMD140621
'SML1614' ! 'SML1614' ; SML1614
'SMDO213AC21' ! 'SMDO213AC21' ; SMDO213AC21
'SMDO213AC12' ! 'SMDO213AC12' ; SMDO213AC12
'SMDO213AC' ! 'SMDO213AC' ; SMDO213AC
'SMDMLL3412' ! 'SMDMLL3412' ; SMDMLL3412
'SMDMLL3421' ! 'SMDMLL3421' ; SMDMLL3421
'SMDMLL34' ! 'SMDMLL34' ; SMDMLL34
'SMC1825' ! 'SMC1825' ; SMC1825
'SML1806' ! 'SML1806' ; SML1806
'SMSOT223GDSD' ! 'SMSOT223GDSD' ; SMSOT223GDSD
'SMSOT223DGSG' ! 'SMSOT223DGSG' ; SMSOT223DGSG
'SMSOT223SDGD' ! 'SMSOT223SDGD' ; SMSOT223SDGD
'SMSOT223ACNC' ! 'SMSOT223ACNC' ; SMSOT223ACNC
'SMSOT2231234' ! 'SMSOT2231234' ; SMSOT2231234
'SMSOT223BCEC' ! 'SMSOT223BCEC' ; SMSOT223BCEC
'SMDPAK' ! 'SMDPAK' ; SMDPAK
'SML2015' ! 'SML2015' ; SML2015
'SML1812' ! 'SML1812' ; SML1812
'SMCT321612' ! 'SMCT321612' ; SMCT321612
'SMCT3216' ! 'SMCT3216' ; SMCT3216
'SMDMLL4121' ! 'SMDMLL4121' ; SMDMLL4121
'SMDMLL4112' ! 'SMDMLL4112' ; SMDMLL4112
'SMDMLL41' ! 'SMDMLL41' ; SMDMLL41
'SMDSOD8721' ! 'SMDSOD8721' ; SMDSOD8721
'SMDSOD8712' ! 'SMDSOD8712' ; SMDSOD8712
'SMDSOD87' ! 'SMDSOD87' ; SMDSOD87
'SMCT3528' ! 'SMCT3528' ; SMCT3528
'SMCT352812' ! 'SMCT352812' ; SMCT352812
'SMC1812' ! 'SMC1812' ; SMC1812
'SMDO214AC12' ! 'SMDO214AC12' ; SMDO214AC12
'SMDO214AC21' ! 'SMDO214AC21' ; SMDO214AC21
'SMDO214AC' ! 'SMDO214AC' ; SMDO214AC
'SMSMB' ! 'SMSMB' ; SMSMB
'SMR2010' ! 'SMR2010' ; SMR2010
'SMDO213AB21' ! 'SMDO213AB21' ; SMDO213AB21
'SMDO213AB12' ! 'SMDO213AB12' ; SMDO213AB12
'SMDO213AB' ! 'SMDO213AB' ; SMDO213AB
'SMDO214AA12' ! 'SMDO214AA12' ; SMDO214AA12
'SMDO214AA21' ! 'SMDO214AA21' ; SMDO214AA21
'SMDO214AA' ! 'SMDO214AA' ; SMDO214AA
'SML2220' ! 'SML2220' ; SML2220
'SMD230921' ! 'SMD230921' ; SMD230921
'SMD2309' ! 'SMD2309' ; SMD2309
'SMD230912' ! 'SMD230912' ; SMD230912
'SMR2512' ! 'SMR2512' ; SMR2512
'SMCT6032' ! 'SMCT6032' ; SMCT6032
'SMCT603212' ! 'SMCT603212' ; SMCT603212
'SMDO214AB21' ! 'SMDO214AB21' ; SMDO214AB21
'SMDO214AB12' ! 'SMDO214AB12' ; SMDO214AB12
'SMDO214AB' ! 'SMDO214AB' ; SMDO214AB
'SMCT7343' ! 'SMCT7343' ; SMCT7343
'SMCT734312' ! 'SMCT734312' ; SMCT734312
'SML3312' ! 'SML3312' ; SML3312
'SMSC59HELP' ! 'SMSC59HELP' ; SMSC59HELP
'SMSOT23HELP' ! 'SMSOT23HELP' ; SMSOT23HELP
'SMSOT223HELP' ! 'SMSOT223HELP' ; SMSOT223HELP
'SMDHELP' ! 'SMDHELP' ; SMDHELP
'SMSOTHELP' ! 'SMSOTHELP' ; SMSOTHELP
'SMCTHELP' ! 'SMCTHELP' ; SMCTHELP
$NETS
$A_PROPERTIES
$END
