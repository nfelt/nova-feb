clear all;

Pulse5F30 = PulseBoundGen('data/PulseAsic5F30.txt');
Pulse5F37 = PulseBoundGen('data/PulseAsic5F37.txt');
Pulse5030 = PulseBoundGen('data/PulseAsic5030.txt');
Pulse5037 = PulseBoundGen('data/PulseAsic5037.txt');
Pulse5733 = PulseBoundGen('data/PulseAsic5733.txt');

save('TestParameters/PulseBounds.mat')