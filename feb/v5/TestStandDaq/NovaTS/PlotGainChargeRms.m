clear all

NValues = 17;

load('TestData\CaltestGainCapSweep\FEB4_0_00157\StdMapBase.mat');
load('TestData\CaltestGainCapSweep\FEB4_0_00157\MagMap.mat');
StdMapTall = repmat(StdMap,8,1);
Rms = StdMapTall .* MagMapAll;
MeanRmsVGain = mean(Rms(1:7,:));
MeanGain = mean(MagMapAll(1:7,:));
FileList = {'base2.mat' 'Std4_7pf.mat' 'Std5_6pf.mat' 'Std6_8pf.mat' 'Std8_2pf.mat' 'Std10pf.mat' 'Std12pf.mat' 'Std18pf.mat' 'Std22pf.mat' 'Std27pf.mat' 'Std33pf.mat' 'Std39pf.mat' 'Std47pf.mat' 'Std56pf.mat' 'Std68pf.mat' 'Std82pf.mat' 'Std100pf.mat'};
CapSweepRms = zeros(NValues,8);
CapSweepSlope = zeros(1,8);
CapSweepIntercept = zeros(1,8);

for InCapValueIndex = 1:NValues;
    InCapName = char(FileList(InCapValueIndex));
load(['TestData\CaltestGainCapSweep\FEB4_0_00157\',InCapName]);

%InCapValue = str2double(InCapName)*1e-6;
x = [0 4.7 5.6 6.8 8.2 10 12 18 22 27 33 39 47 56 68 82 100];
CapSweepRms(InCapValueIndex,:)= StdMap.* MeanGain ;

end

for i = 1:8
    [LinPar S]=polyfit(x,CapSweepRms(:,i)',1); 
     HVNormr= S.normr;
     f = polyval(LinPar,1:100); 
    CapSweepSlope(i) = LinPar(1);
    CapSweepIntercept(i) = LinPar(2); 
    figure (i);
    plot(x,CapSweepRms(:,i)','o',1:100,f,'-')
    disp(i);
    disp(LinPar(1));
    disp(LinPar(2));
end
CapSweepRmsMean = mean(CapSweepRms,2);
[LinPar S]=polyfit(x,CapSweepRmsMean',2);
f = polyval(LinPar,1:100); 
figure (99);
plot(x,CapSweepRms(:,i)','o',1:100,f,'-')
%         [LinPar S]=polyfit(x,y,1); 
%         HVNormr= S.normr;
%     f = polyval(LinPar,x);    

% title(['Linear Fit Slope = ', num2str(LinPar(1)),' Intercept = ', num2str(LinPar(2))]);
% xlabel('pF')
% ylabel('e-')
