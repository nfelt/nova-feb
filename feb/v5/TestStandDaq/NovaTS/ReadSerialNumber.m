%%%%%function writeserial number writes a file called
%%%%%'writeserialnumber.txt' which is the xml file to program the serial
%%%%%number into the feb. it asks for user input for the number. the
%%%%%appended phrase to fill up the extra memory is taken from the file
%%%%%'serialnumberappend.txt' and each feb will have a cycled phrase.
%%%%%written by sam wald, siwald@fas.harvard.edu
function[SerNumMsg, FitCheck] = ReadSerialNumber()
close all
clear all
RepeatTest = 'y';
FitCheck = 0;
    while RepeatTest == 'y';
        % read the contents of FEB's FLASH memory.
        ! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SerNumAdrReset.xml data\logfile.txt
        ! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SerNumRead.xml data\SerNumber.txt
        ! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SerNumRead.xml data\SerNumber.txt
        ! NovaCmdScripts\novacmdapp -o -l 63 NovaCmdScripts\SerNumRead.xml data\SerNumber.txt
        DataCharIn = char(textread('data\SerNumber.txt','%s', 'whitespace', ' -'));
        DataCharIn = DataCharIn(2:2:length(DataCharIn),:);
        if strcmp(DataCharIn(1,:),'read ')
            SerNumMsg = data(1:12);
            return;
        end
        AsciiMsg(1:2:length(DataCharIn)*2) = hex2dec(DataCharIn(:,1:2));
        AsciiMsg(2:2:length(DataCharIn)*2) = hex2dec(DataCharIn(:,3:4));
        SerNumMsg = (char(AsciiMsg));

        if SerNumMsg(1:7)== 'FEB4.0-';
                    FitCheck = 1;
                    RepeatTest = 'n';
        else  
            RepeatTest = input('The test failed do you want repeat? y/n [y]: ', 's');
            if isempty(RepeatTest)
                RepeatTest = 'y';
            end
        end
    end
return