Scribbled secret notebooks, and wild typewritten pages, for your own joy
Submissive to everything, open, listening
Try never get drunk outside your own house
Be in love with your life
Something that you feel will find its own form
Be crazy dumbsaint of the mind
Blow as deep as you want to blow
Write what you want bottomless from bottom of the mind
The unspeakable visions of the individual
No time for poetry but exactly what is
Visionary tics shivering in the chest
In tranced fixation dreaming upon object before you
Remove literary, grammatical and syntactical inhibition
Like Proust be an old teahead of time
Telling the true story of the world in interior monolog
The jewel center of interest is the eye within the eye
Write in recollection and amazement for yourself
Work from pithy middle eye out, swimming in language sea
Accept loss forever
Believe in the holy contour of life
Struggle to sketch the flow that already exists intact in mind
Don't think of words when you stop but to see picture better
Keep track of every day the date emblazoned in yr morning
No fear or shame in the dignity of yr experience, language & knowledge
Write for the world to read and see your exact pictures of it
Bookmovie is the movie in words, the visual American form
In praise of Character in the Bleak inhuman Loneliness
Composing wild, undisciplined, pure, coming in from under, crazier the better
You're a Genius all the time
Writer-Director of Earthly movies Sponsored & Angeled in Heaven
Close your eyes - Landlord knocking  On the back door.  The bottoms of my shoes  are wet  from walking in the rain
In my medicine cabinet, the winter fly  has died of
A dog is the only thing on earth that loves you more than you love yourself.
A government that robs Peter to pay Paul can always depend on the support of Paul.
A hospital bed is a parked taxi with the meter running.
A lot of people are afraid of heights. Not me, I'm afraid of widths.
A nickel ain't worth a dime anymore.
A word to the wise ain't necessary - it's the stupid ones that need the advice.
Alimony is like buying hay for a dead horse.
All right everyone, line up alphabetically according to your height.
All you need is love. But a little chocolate now and then doesn't hurt.
Always end the name of your child with a vowel, so that when you yell the name will carry.
Any girl can be glamorous. All you have to do is stand still and look stupid.
Anyone who says he can see through women is missing a lot.
As I get older, I just prefer to knit.
Between two evils, I always pick the one I never tried before.
Cleanliness becomes more important when godliness is unlikely.
Cross country skiing is great if you live in a small country.
Curiosity killed the cat, but for a while I was a suspect.
Happiness is having a large, loving, caring, close-knit family in another city.
Have enough sense to know, ahead of time, when your skills will not extend to wallpapering.
Honesty is the best policy - when there is money in it.
Human beings are the only creatures on earth that allow their children to come back home.
I always wanted to be somebody, but now I realize I should have been more specific.
I am free of all prejudices. I hate every one equally.
I am not afraid of death, I just don't want to be there when it happens.
I am the literary equivalent of a Big Mac and Fries.
I bought some batteries, but they weren't included.
I busted a mirror and got seven years bad luck, but my lawyer thinks he can get me five.
I can't even get three weeks off to have cosmetic surgery.
I cook with wine, sometimes I even add it to the food.
I did not have three thousand pairs of shoes, I had one thousand and sixty.
I distrust camels, and anyone else who can go a week without a drink.
I don't need you to remind me of my age. I have a bladder to do that for me.
I don't think anyone should write their autobiography until after they're dead.
I don't want to achieve immortality through my work. I want to achieve it through not dying.
Don't stay in bed, unless you can make money in bed.
Drawing on my fine command of the English language, I said nothing.
Electricity is really just organized lightning.
Everywhere is within walking distance if you have the time.
Fashions have done more harm than revolutions.
Fatherhood is pretending the present you love most is soap-on-a-rope.
Food is an important part of a balanced diet.
For your information, I would like to ask a question.
Get your facts first, then you can distort them as you please.
Go to Heaven for the climate, Hell for the company.
God did not intend religion to be an exercise club.
I have a new philosophy. I'm only going to dread one day at a time.
I have never been hurt by what I have not said.
I have tried to know absolutely nothing about a great many things, and I have succeeded fairly well.
I intend to live forever. So far, so good.
I like marriage. The idea.
I never drink water because of the disgusting things that fish do in it.
I never said most of the things I said.
I rant, therefore I am.
I sang in the choir for years, even though my family belonged to another church.
I was married by a judge. I should have asked for a jury.
I was the kid next door's imaginary friend.
I will not eat oysters. I want my food dead. Not sick. Not wounded. Dead.
I wonder if other dogs think poodles are members of a weird religious cult.
I would never die for my beliefs because I might be wrong.
I'd luv to kiss ya, but I just washed my hair.
I'm an idealist. I don't know where I'm going, but I'm on my way.
If love is the answer, could you please rephrase the question?
If two wrongs don't make a right, try three.
If you ask me anything I don't know, I'm not going to answer.
It all started when my dog began getting free roll over minutes.
It is a scientific fact that your body will not absorb cholesterol if you take it from another person's plate.
Life is hard. After all, it kills you.
Moderation is a virtue only in those who are thought to have an alternative.
Money is better than poverty, if only for financial reasons.
My fake plants died because I did not pretend to water them.
My inner child is not wounded.
My theory is that all of Scottish cuisine is based on a dare.
Never fight an inanimate object.
Never floss with a stranger.
Never have more children than you have car windows.
Never wear anything that panics the cat.
Nobody ever went broke underestimating the taste of the American public.
O Lord, help me to be pure, but not yet.
One man's folly is another man's wife.
Originality is the fine art of remembering what you hear but forgetting where you heard it.
Procrastination is the art of keeping up with yesterday.
Recession is when a neighbor loses his job. Depression is when you lose yours.
Smoking kills. If you're killed, you've lost a very important part of your life.
Start every day off with a smile and get it over with.
Television is a medium because anything well done is rare.
The four building blocks of the universe are fire, water, gravel and vinyl.
The reason there are two senators for each state is so that one can be the designated driver.
The superfluous, a very necessary thing.
The two basic items necessary to sustain life are sunshine and coconut milk.
There cannot be a crisis next week. My schedule is already full.
TV is chewing gum for the eyes.
Two things are infinite: the universe and human stupidity; and I'm not sure about the universe.
We learn from experience that men never learn anything from experience.
We'll love you just the way you are if you're perfect.
Weather forecast for tonight: dark.
What does it mean to pre-board? Do you get on before you get on?
What's on your mind, if you will allow the overstatement?
When I was a boy the Dead Sea was only sick.
Wine is constant proof that God loves us and loves to see us happy.
You're only as good as your last haircut.
the bumble bee shouldn't be able to fly, but the bumble bee doesn't know it so it goes on flying anyway.
Anthropology was the science that gave her the platform from which she surveyed, scolded and beamed at the world
Anyone who attempts to generate random numbers by deterministic means is, of course, living in a state of sin.
Bad times have a scientific value. These are occasions a good learner would not miss.
From now on we live in a world where man has walked on the Moon. It's not a miracle; we just decided to go.
Give me a lever long enough and a fulcrum on which to place it, and I shall move the world.
Half of the modern drugs could well be thrown out of the window, except that the birds might eat them.
I am not a scientist. I am, rather, an impresario of scientists.
I see nothing in space as promising as the view from a Ferris wheel.
If a man's wit be wandering, let him study the mathematics.
If we wish to make a new world we have the material ready. The first one, too, was made out of chaos.
Leave the atom alone.
No amount of experimentation can ever prove me right; a single experiment can prove me wrong.
Nothing in the universe can travel at the speed of light, they say, forgetful of the shadow's speed.
Only two things are infinite, the universe and human stupidity, and I'm not sure about the former.
Our scientific power has outrun our spiritual power. We have guided missiles and misguided men.
Politics is for the present, but an equation is for eternity.
Polygraph tests are 20th-century witchcraft.
Reality is merely an illusion, albeit a very persistent one.
Research is what I'm doing when I don't know what I'm doing.
Science and technology revolutionize our lives, but memory, tradition and myth frame our response.
Science is a first-rate piece of furniture for a man's upper chamber, if he has common sense on the ground floor
Science is a way of thinking much more than it is a body of knowledge.
Science is organized common sense where many a beautiful theory was killed by an ugly fact.
Science is the great antidote to the poison of enthusiasm and superstition.
Science never solves a problem without creating ten more.
Science without religion is lame, religion without science is blind.
Click here to find out more!
The distance between insanity and genius is measured only by success.
The great tragedy of science - the slaying of a beautiful hypothesis by an ugly fact.
The most exciting phrase to hear in science is not 'Eureka!' but 'That's funny...'
The nineteenth century believed in science but the twentieth century does not.
The process of scientific discovery is, in effect, a continual flight from wonder.
The saddest aspect of life right now is that science gathers knowledge faster than society gathers wisdom.
The science of today is the technology of tomorrow.
The scientific theory I like best is that the rings of Saturn are composed entirely of lost airline luggage.
There are no shortcuts in evolution.
There is a single light of science, and to brighten it anywhere is to brighten it everywhere.
here was no "before" the beginning of our universe, because once upon a time there was no time.
Touch a scientist and you touch a child.
We can lick gravity, but sometimes the paperwork is overwhelming.
Whenever anyone says, 'theoretically,' they really mean, 'not really.'
You cannot feed the hungry on statistics.
Your theory is crazy, but it's not crazy enough to be true.
Error.
DON'T DO THAT!
