%  Test used to collect Data for calibration test

%%%%%%%%%%%%%%%%% USER SETTINGS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;
NoSamples = 10
%set starting index of data file
DataFilePfix = '';
PulseValue = '0000';
DataFileSfix = '.txt';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Magnitude of conversion factor using set mV and 2.03pf
Adc2E = 2.56*1e-3 * 2.03e-12 * 6.24151e18;
RegAddress;
ConfigCheck;
[Status Error FirmwareVersion]= GetStatusErrorVers();
[SerNumbMsg, SerNumOK] = ReadSerialNumber;
FebSN = SerNumbMsg(1:12);
MagMap = zeros(16,8);
StdMap = zeros(1,8);

for DataFileIndex = 1:NoSamples;
    FebDataFolder= strcat('TestData\BoardCal\',FebSN(1:4),'_',FebSN(6),'_',FebSN(8:12),'\','N',sprintf('%02d',DataFileIndex),'\');
    setenv('FebDataFolder', FebDataFolder);
    system(['mkdir ', FebDataFolder]); 
    for i = 0; % 0:31 %FOR ALL CHAN
        ChanSet = '00000000000000000000000000000000';
        ChanSet(i+1) = '1';
        ChanSelL = dec2hex(bin2dec(ChanSet(17:32)),4)
        ChanSelU = dec2hex(bin2dec(ChanSet(1:16)),4)
        dcmwrite(CHAN_EN_L_ADDR,ChanSelL);
        dcmwrite(CHAN_EN_U_ADDR,ChanSelU);
        for RiseTime = 0:1:15
            for FallTime =0:1:7
                for ShaperVGain = 3; % temp change to just one:7;
                    ConfigCheck;
                    BWSelAuto = dec2hex(RiseTime,4)
                    TFSelAuto = dec2hex(FallTime,4)
                    GSelAuto =  dec2hex(ShaperVGain,4)
                    DataFilename = [DataFilePfix,'Rise',BWSelAuto,'Fall',TFSelAuto,'Gain',GSelAuto,DataFileSfix];
                    system (['NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetAsicBasicNoSerial.xml ' ,'data\logfile.txt']);
                    dcmwrite(BWSEL_ADDR,BWSelAuto);
                    dcmwrite(TFSEL_ADDR,TFSelAuto);
                    %dcmwrite(GSel_ADDR,GSelAuto);
                    system (['NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SendAsicSerial.xml ' ,'data\logfile.txt']);

    %                 %Use the following line for baseline samples
    %                 GetOscopeData44([FebDataFolder,DataFilename]);  
    %                 StdMap(ShaperVGain+1)= GetDcsStdSingle([FebDataFolder,DataFilename]);
    %                 disp(StdMap);

                    %Use the following line for External charge injection
                    GetOscopeDataItrig44([FebDataFolder,DataFilename]); 
                    MagMap(RiseTime+1,FallTime+1) = MagMap(RiseTime+1,FallTime+1)+ Adc2E/GetDcsMagSingle([FebDataFolder,DataFilename]);

                end
            end
        end
    end
    %save('TestData\CaltestGainCapSweep\FEB4_0_00157\Std1pf.mat','StdMap');
end
    MagMapAvg = MagMap ./ NoSamples;
    disp(MagMapAvg);
dlmwrite(['TestData/BoardCal/',FebSN,'SensitivityCal.txt'], MagMapAvg);