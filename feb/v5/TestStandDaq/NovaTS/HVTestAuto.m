function [HVData, LinPar, HVNormr, FitCheck] = HVTestAuto(FebDataFolder)
    Parameters;
    RegAddress;
    RepeatTest = 'y';
    FitCheck = 0;
    while RepeatTest == 'y'
        ConfigCheck;
        % Find a tcpip object.
        obj1 = instrfind('Type', 'tcpip', 'RemoteHost', '140.247.132.061', 'RemotePort', 3490, 'Tag', '');

        % Create the tcpip object if it does not exist
        % otherwise use the object that was found.
        if isempty(obj1)
            obj1 = tcpip('140.247.132.061', 3490);
        else
            fclose(obj1);
            obj1 = obj1(1)
        end
        % Connect to instrument object, obj1.
        fopen(obj1);

        % Communicating with instrument object, obj1.
        fprintf(obj1, '*rst');
        fprintf(obj1, 'conf:volt 10 ');
        fprintf(obj1, ':volt:nplc 10');
        fprintf(obj1, ':trig:sour bus');

        HVData = zeros(1,15,1);
        for i = 1:15
            %with version >= 12 the DAC reg is inverted to make the low
            %setting = low HV.
            dcmwrite(HVReg,strcat('0',dec2hex(i),'00'));
            fprintf(obj1, ':init');
            fprintf(obj1, '*trg');
            fprintf(obj1, 'fetch?');
            HVData(16-i) = str2double(fscanf(obj1))*1000;
        end

        % Disconnect from instrument object, obj1.
        fclose(obj1);

        x = [3584:-256:0];
        [LinPar S]=polyfit(x,HVData,1); 
        HVNormr= S.normr;
        if  (HVSlopeUBound > LinPar(1)) && (HVSlopeLBound < LinPar(1)) && (HVInterceptUbound > LinPar(2)) && (HVInterceptLbound < LinPar(2)) && (HVNormrU > HVNormr);
            FitCheck = 1;
            RepeatTest = 'n';
        else  
            RepeatTest = input('The test failed do you want repeat? y/n [y]: ', 's');
            if isempty(RepeatTest)
                RepeatTest = 'y';
            end
        end
        dlmwrite(strcat(FebDataFolder,'/HVData.txt'), HVData);
    end
end
