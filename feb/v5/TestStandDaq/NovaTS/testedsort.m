

 function [ output_args ] = testedsort()
%FAILSORT deletes the double entries for tested boards and saves the latest
%test results. It then finds failed boards and organizes them all into a
%matrix.

clear matrix

textreadfile = textread('data/FebMiniDaqResults.txt', '%s', 'delimiter', ',');
sz=size(textreadfile);
sz=sz(1);
linecount=sz/39;
febcount=linecount-1;
feb=1;
i=40;
while feb<=febcount;
    
    
     FebSN=textreadfile(i);
     matrix(feb,1)=FebSN;
     
     date=textreadfile(i+1);
     matrix(feb,2)=date;
     
    FirmwareVer=textreadfile(i+2);
    matrix(feb,3)=FirmwareVer;
    
    TestResult=textreadfile(i+3);
    matrix(feb,4)=TestResult;
    
    SerNumOk=textreadfile(i+4);
    matrix(feb,5)=SerNumOk;

    HVFitOK=textreadfile(i+5);
    matrix(feb,6)=HVFitOK;
    
    OscopeFitOK=textreadfile(i+6);
    matrix(feb,7)=OscopeFitOK;
    
    PulseFitOK=textreadfile(i+7);
    matrix(feb,8)=PulseFitOK;
    
    TeccLoopbackFitOK=textreadfile(i+8);
    matrix(feb,9)=TeccLoopbackFitOK;
    
    AnalogVoltage=textreadfile(i+9);
    matrix(feb,10)=AnalogVoltage;
    
    ApdMaxNoise=textreadfile(i+10);
    matrix(feb,11)=ApdMaxNoise;
    
    ApdMinNoise=textreadfile(i+11);
    matrix(feb,12)=ApdMinNoise;
    
    HVFitSlope=textreadfile(i+12);
    matrix(feb,13)=HVFitSlope;
    
    HVRitInter=textreadfile(i+13);
    matrix(feb,14)=HVRitInter;
    
    HVNormr=textreadfile(i+14);
    matrix(feb,15)=HVNormr;
    
    HVDataEFF=textreadfile(i+15);
    matrix(feb,16)=HVDataEFF;
    
    HVDataDFF=textreadfile(i+16);
    matrix(feb,17)=HVDataDFF;
    
    HVDataCFF=textreadfile(i+17);
    matrix(feb,18)=HVDataCFF;

    HVDataBFF=textreadfile(i+18);
    matrix(feb,19)=HVDataBFF;
    
    HVDataAFF=textreadfile(i+19);
    matrix(feb,20)=HVDataAFF;
    
    HVData9FF=textreadfile(i+20);
    matrix(feb,21)=HVData9FF;
    
    HVData8FF=textreadfile(i+21);
    matrix(feb,22)=HVData8FF;
    
    HVData7FF=textreadfile(i+22);
    matrix(feb,23)=HVData7FF;
    
    HVData6FF=textreadfile(i+23);
    matrix(feb,24)=HVData6FF;
    
    HVData5FF=textreadfile(i+24);
    matrix(feb,25)=HVData8FF;
    
    HVData4FF=textreadfile(i+25);
    matrix(feb,26)=HVData4FF;
    
    HVData3FF=textreadfile(i+26);
    matrix(feb,27)=HVData3FF;
    
    HVData2FF=textreadfile(i+27);
    matrix(feb,28)=HVData2FF;
    
    HVData1FF=textreadfile(i+28);
    matrix(feb,29)=HVData1FF;
    
    HVData0FF=textreadfile(i+29);
    matrix(feb,30)=HVData0FF;
    
    LoopData0=textreadfile(i+30);
    matrix(feb,31)=LoopData0;
    
    LoopData1=textreadfile(i+31);
    matrix(feb,32)=LoopData1;
    
    LoopData2=textreadfile(i+32);
    matrix(feb,32)=LoopData2;
    
    LoopData3=textreadfile(i+33);
    matrix(feb,33)=LoopData3;
    
    LoopData4=textreadfile(i+34);
    matrix(feb,34)=LoopData4;

    LoopData5=textreadfile(i+35);
    matrix(feb,35)=LoopData5;
    
    LoopData6=textreadfile(i+36);
    matrix(feb,36)=LoopData6;
    
    LoopData7=textreadfile(i+37);
    matrix(feb,37)=LoopData7;
    
    TesterComment=textreadfile(i+38);
    matrix(feb,38)=TesterComment;
    
    
    i=i+39;
    feb=feb+1;
end

output_args=sortbyfeb(matrix);
 end


