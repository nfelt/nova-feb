This folder contains the FEB USB driver and the readout software.

The main readout daq file is called "feb_test' and can be run using Matlab or by running feb_test.bat.  When run, feb_test will start sampling the first 16 channels of the ASIC and  also provide a delayed external trigger signal available on on pin 33 of the Mictor connector.   The output data found in \data\txt\outfile.txt contains 5k continuous samples total of 16 channels sampled at 500ns.   The feb_test.bat file will only generate a new outfile.txt.  Using feb_test.m will generate a new outfile and  display various graphs of the data. 

The format of the data in outfile.txt is the four msb are the channel number and the 12 lsb are the channel data.  There is a channel map conversion of the channel number.  The Matlab version of feb_test should sort out the channel number.
