ISE Design Suite 12
はじめにお読みください

お客様各位

このたびは、ザイリンクス ISE Design Suite 12 をご購入いただき、誠にありがとうございます。この文書では、ご購入いただきましたソフトウェアおよびマニュアルのインストールについて説明します。さらに詳細な情報が必要な場合は、次の文書をお読みください。

*『ISE Design Suite 12 : インストール、ライセンス、リリース ノート』 : ISE Design Suite 12 のソフトウェアに含まれるアイテム、インストール方法、ライセンスの取得方法について説明します。この文書は、次のサイトからご覧になれます。
  http://www.xilinx.com/cgi-bin/SW_Docs_Redirect/sw_docs_redirect?ver=12.2&locale=jp&topic=release+notes

* 「ISE Design Suite 12 の新機能」(HTML ファイル) : 今リリースで追加された新機能がリストされています。このファイルは、次の方法で参照できます。

  * ISE Project Navigator で [Help] -> [ISE Design Suite InfoCenter] をクリックします。表示される ISE Design Suite 情報センタの「リソース」セクションに、「ISE Design Suite 12 の新機能」へのリンクがあります。 

  * [スタート] → [プログラム] → [Xilinx ISE Design Suite 12.2] → [マニュアル] → [ISE Design Suite 情報センタ] をクリックします。 
 
  * ISE DVD の idata\japanese\ISE_DS_InfoCenter.htm ディレクトリから開きます。


ソフトウェアのインストール
**************************
インストールの詳細は、『ISE Design Suite 12 : インストール、ライセンス、リリース ノート』を参照してください。 

インストール済みのソフトウェア バージョンの詳細は、ザイリンクス XInfo System Checker で確認してください。XInfo System Checker は、ISE Design Suite 12 DVD のルート ディレクトリから xinfo ファイルを実行するか、次のように起動します。

* Windows : [スタート] → [プログラム] → [Xilinx ISE Design Suite 12.2] → [アクセサリ] → [XInfo System Checker] をクリックします。

* Linux : $XILINX/bin/lin から xinfo ファイルを実行します。

* Linux64 : $XILINX/bin/lin64 から xinfo ファイルを実行します。


ソフトウェア マニュアルのインストール方法
*****************************************
ソフトウェア インストール時には英語版のマニュアルがインストールされますが、ザイリンクス Web サイトから日本語版のマニュアルをダウンロードできます。詳細は、『ISE Design Suite 12 : インストール、ライセンス、リリース ノート』を参照してください。

日本語版のマニュアルには、http://japan.xilinx.com/documentation から [デザイン ツール] タブ -> [ISE Design Suite] からもアクセスできます。


既知の問題
**********
『ISE Design Suite 12 : インストール、ライセンス、リリース ノート』に、既知の問題とその解決方法へのリンクが掲載されています。


テクニカル サポート
*******************
テクニカル サポートが必要な場合は、http://japan.xilinx.com/support にアクセスしてください。このサイトから、アンサー データベース、フォーラムなどにアクセスし、問題解決に役立つ情報を入手できます。これらの情報を参照しても問題を解決できない場合は、http://japan.xilinx.com/support/clearexpress/websupport.htm からウェブケースを開いてテクニカル サポート エンジニアにご連絡ください。