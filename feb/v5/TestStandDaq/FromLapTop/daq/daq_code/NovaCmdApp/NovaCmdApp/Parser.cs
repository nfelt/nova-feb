using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using CyUSB;


    class Parser
    {
        bool append = true; //default is to append to output file
        TextWriter logfile = null;
        bool onlyreadout = false;

        int outbreak = 4;  //default number of hex chars per line on outfile

       public byte[] bytes=new byte[4];
       public int argcount=0;
       NovaInterface myNovaInterface;
       CyUSBDevice myDev;
       string infile, outfile;

       public  Parser(){

           myNovaInterface = new NovaInterface();
           myNovaInterface.USBInit();

           myDev = myNovaInterface.getDevice() as CyUSBDevice;            
       }

        public void doUSBUpdate()
        {
            myDev = myNovaInterface.getDevice() as CyUSBDevice;
        }

       public void RunArgList(string [] args){


          // if (myDev == null){ Console.WriteLine("USB device not connected....exiting...."); return;}

           argcount = args.Length;
           bool good = false;
           int loop = 1;

           for(int i = 0; i < argcount; i++){
               if (args[i] == "-e")
               {
                   append = false;
                   Console.WriteLine("Overwriting output file");
               }
               if (args[i] == "-o")
               {
                   onlyreadout = true;
                   Console.WriteLine("Only recording read packets in output file");
                   Console.WriteLine("    Packets seperated by -");
               }
               if(args[i] == "-f"){
                   if(i > argcount-2) 
                       Console.WriteLine("Invalid Input, correct usage => -f <infile> <logfile>");
                   infile = args[i+1];
                   outfile = args[i+2];
                   i += 2;
                   good = true;
               }
               if(args[i] == "-l"){
                   if(i > argcount-3) 
                   Console.WriteLine("Invalid Input, correct usage => -l <N> <infile> <logfile>");
                   loop = int.Parse(args[i+1]);
                   infile = args[i+2];
                   outfile = args[i+3];
                   i += 3;
                   good = true;
               }
               if (args[i] == "-b")
               {
                   if(i<argcount-1)
                       if (!int.TryParse(args[i + 1], out outbreak))
                       {
                           Console.WriteLine("error with -b argument");
                           return;
                       }
                       else
                       {
                           Console.WriteLine("outputing " + outbreak + " hex characters per line");
                       }

               }
           }

           if (!good){ Console.WriteLine("problem with command.... use -h or --help for info"); return;}

           Console.WriteLine("input: " + infile + "  output: " + outfile + "  looping: "+loop);
           //open logfile
    
      
               try
               {
                   logfile = new StreamWriter(outfile, append);
               }
               catch (Exception e)
               {
                   Console.WriteLine(e.ToString());
                   return;
               }

          

           //parse infile to see if it is xml or tsk
           //exit if not one of these

           string[] ext=infile.Split('.');
           int inftype = 0;
           if(ext.Length>1&&ext[1].CompareTo("xml")==0)inftype=1;
           if(ext.Length>1&&ext[1].CompareTo("tsk")==0)inftype=2;

           //now if just xml
           if (inftype == 1)
           {
               for (int i = 0; i < loop && good; i++)
               {
                   parseXMLfile(infile, outfile, i,loop);
               }
           }
           else if (inftype == 2)
           {
               for (int i = 0; i < loop; i++)
               {
                   StreamReader r = new StreamReader(infile);
                   string rline;
                   while ((rline = r.ReadLine()) != null)
                   {
                       //look for comment character
                       if (rline.StartsWith("#") || rline.StartsWith("//")) continue;
                       if (rline.StartsWith("!")){Console.WriteLine(rline);continue;}
                       if (rline.StartsWith("$"))
                       {
                           //pause routine
                           string[] tsrline = rline.Split(' ');
                           int pause = 0;

                           if (tsrline.Length > 1)
                           {
                               if (!int.TryParse(tsrline[1], out pause) || pause < 0)
                               {
                                   Console.WriteLine("error with pause command");
                                   continue;
                               }
                               else
                               {
                                   Console.WriteLine("sleeping for " + pause + " ms");
                                   System.Threading.Thread.Sleep(pause);
                               }

                           }

                       }
                       else
                       {

                           //for each line   (can be x.xml or x.xml number)
                           string[] srline = rline.Split(' ');
                           int inters = 1;
                           bool fgood = false;
                           if (srline.Length == 1)
                           {
                               string[] fname = srline[0].Split('.');
                               if (fname.Length > 1 && fname[1].CompareTo("xml") == 0) fgood = true;
                           }
                           else if (srline.Length == 2)
                           {
                               string[] fname = srline[0].Split('.');
                               if (fname.Length > 1 && fname[1].CompareTo("xml") == 0) fgood = true; else fgood = false;
                               if (int.TryParse(srline[1], out inters)) fgood = true; else fgood = false;
                           }

                           if (fgood)
                           {
                               Console.WriteLine("Running on " + srline[0] + " for " + inters + " iterations");

                               for (int j = 0; j < inters && fgood; j++)
                               {
                                   parseXMLfile(srline[0], outfile, j,inters);
                               }

                           }
                       }
                   }
               }
           }
           else if (inftype == 0)
           {
               Console.WriteLine("Error -- input file must be .xml or .tsk");
               return;
           }

           if (logfile != null) logfile.Close();

       }

       private void parseXMLfile(string inputfile, string outputfile, int rep,int totloops)
       {
           int totalbuffersout = 0;
           int totalbuffersin = 0;
           int totalbytesout = 0;
           int totalbytesin = 0;
           int totalerrorsin = 0;
           int totalerrorsout = 0;


           if (logfile != null&&!onlyreadout)
               logfile.WriteLine("Nova Test Station\r\nTest Started  {0}\r\nProcessing file {1}\r\nWriting output to {2}\r\nIteration {3} of {4}\r\n\r\n", System.DateTime.Now, inputfile, outfile,rep+1,totloops);
       

        
            try{
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.Load(inputfile);  
            XmlNode tempnode = xmldoc.DocumentElement.FirstChild;

            if (tempnode == null) { Console.WriteLine(inputfile + " probably empty"); }


            while (tempnode !=null&&myNovaInterface.getDevice() as CyUSBDevice!=null)
            {
                if (tempnode.Name == "sleep")
                {
                    string time = tempnode.Attributes.GetNamedItem("time").Value;
                    int t=0;
                    if (!int.TryParse(time, out t)||t<0)
                    { Console.WriteLine("bad sleep command in xml file"); continue; }

                    Console.WriteLine("XML sleeping for " + t + " ms");
                    System.Threading.Thread.Sleep(t);

                }

                if (tempnode.Name == "writeblock")
                {
          //          outbufferlength=0;
                    String tempstring = tempnode.InnerText;
                    
                    if(logfile!=null&&!onlyreadout)logfile.WriteLine("***out***---writeblock---");

                    string rawtemp = tempstring;

                    string outtemp = null;
                    int charcount = 0;
                    for (int s = 0; s < rawtemp.Length; s++)
                    {
                        if (rawtemp[s] == '\n' || rawtemp[s] == '\r') continue;
                        if (charcount % outbreak == 0 && charcount > 0) outtemp += "\r\n";
                        charcount++;
                        outtemp += rawtemp[s];

                    }


                    if (logfile != null&&!onlyreadout) logfile.WriteLine(outtemp);

                    if (myNovaInterface.writehexstring(tempstring))
                    {
                        totalbuffersout++;
                        totalbytesout += myNovaInterface.outbufferlength;
                    }
                    else
                    {
                        totalerrorsout++;                      
                        if (logfile != null&&!onlyreadout) logfile.WriteLine("- data send error");
                    }

                    if (logfile != null&&!onlyreadout) logfile.Write("\r\n\r\n");
                }
                else if (tempnode.Name == "readblock")
                {                    
                    if (logfile != null&&!onlyreadout) logfile.WriteLine("***in***---readblock---");

       
                    String tempstring=null;
                    if (myNovaInterface.readhexstring(ref tempstring))
                    {


                        string rawtemp = tempstring;

                        string outtemp = null;
                        int charcount = 0;
                        for (int s = 0; s < rawtemp.Length; s++)
                        {
                            if (rawtemp[s] == '\n' || rawtemp[s] == '\r') continue;
                            if (charcount % outbreak == 0&&charcount>0) outtemp += "\r\n";
                            charcount++;
                            outtemp += rawtemp[s];

                        }


                        if (logfile != null) logfile.WriteLine(outtemp);
                        if (logfile != null&&!onlyreadout) logfile.Write("\r\n\r\n");
                        if (logfile != null && onlyreadout) logfile.WriteLine("-");
                        totalbytesin += myNovaInterface.inbufferlength;
                        totalbuffersin++;

                    }
                    else
                    {
                        if (logfile != null) logfile.WriteLine("- data read error");
                        if (logfile != null && onlyreadout) logfile.WriteLine("-");
                        totalerrorsin++;
                    }
                }
                else if (tempnode.Name == "writemem")
                {

                    uint location = uint.Parse(tempnode.Attributes.GetNamedItem("location").Value, System.Globalization.NumberStyles.AllowHexSpecifier);
                    int count = int.Parse(tempnode.Attributes.GetNamedItem("count").Value, System.Globalization.NumberStyles.AllowHexSpecifier) * 4;

                    myNovaInterface.writemem(location, tempnode.InnerText);

                    if (logfile != null&&!onlyreadout) logfile.WriteLine("***out***---write mem at " + string.Format("{0:x2}", location).ToUpper() + " for " + string.Format("{0:x2}", count / 4+1).ToUpper() + " entries---");

                    string rawtemp = tempnode.InnerText;
             
                    string outtemp = null;
                    int charcount = 0;
                    for (int s = 0; s < rawtemp.Length; s++)
                    {
                        if (rawtemp[s] == '\n' || rawtemp[s] == '\r') continue;
                        if (charcount % outbreak == 0 && charcount > 0) outtemp += "\r\n";
                        charcount++;
                        outtemp += rawtemp[s];

                    }
                    if (logfile != null&&!onlyreadout) logfile.WriteLine(outtemp);
                }
                else if (tempnode.Name == "readmem")
                {
                    
                    if (tempnode.Attributes.GetNamedItem("location") == null) return;
                    if (tempnode.Attributes.GetNamedItem("count") == null) return;
                  

                    uint location=uint.Parse(tempnode.Attributes.GetNamedItem("location").Value,System.Globalization.NumberStyles.AllowHexSpecifier);
                    int count = int.Parse(tempnode.Attributes.GetNamedItem("count").Value, System.Globalization.NumberStyles.AllowHexSpecifier) ;

                    Byte[] buff =new Byte[2000];

                    if (myNovaInterface.readmem(location, ref count, ref buff))
                    {

                        if (logfile != null&&!onlyreadout) logfile.WriteLine("***in***---read mem at " + string.Format("{0:x2}", location).ToUpper() + " for " + string.Format("{0:x2}", count / 4+1).ToUpper() + " entries---");

                        string rawtemp = null;
                        for (int i = 0; i < count; i++)
                        {
                            rawtemp+=string.Format("{0:x2}", buff[i]);
                        }
                    
                            string outtemp=null;
                            int charcount=0;
                            for(int s=0;s<rawtemp.Length;s++)
                            {
                                if(rawtemp[s]=='\n'||rawtemp[s]=='\r')continue;
                                if (charcount % outbreak == 0 && charcount > 0) outtemp += "\r\n";
                                charcount++;
                                outtemp+=rawtemp[s];

                            }
                            if (logfile != null) logfile.Write(outtemp);
                        
                        
                        if (logfile != null) logfile.Write("\r\n");
                        if (logfile != null && onlyreadout) logfile.WriteLine("-");
                    }
                    else
                    {
                        if (logfile != null&&!onlyreadout) logfile.WriteLine("***out***---read mem error");
                        if (logfile != null && onlyreadout) logfile.WriteLine("read mem error\r\n-");
                    }

                }
                else if (tempnode.Name == "usbcommand")
                {
                    if (tempnode.Attributes.Count == 0) return;
                    if (tempnode.Attributes.GetNamedItem("command") == null) return;
                    if (tempnode.Attributes.GetNamedItem("command").Value == "reset device")
                    {
                        myNovaInterface.sendreset();
                       if( logfile!=null &!onlyreadout) logfile.WriteLine("usbcommand \r\n-Reset Device\r\n\r\n");
                    }
                    else if (tempnode.Attributes.GetNamedItem("command").Value == "reset interface")
                    {
                        myNovaInterface.reset();
                       if (logfile!=null &!onlyreadout) logfile.WriteLine("usbcommand \r\n-Reset Interface\r\n\r\n");
                    }
                }
                tempnode = tempnode.NextSibling;
            }

          

            }
            catch (Exception e)
            {
            }

            if (logfile != null&&!onlyreadout)
                logfile.WriteLine("\r\nTest Ended \r\n\r\n");
       


        }






    }
