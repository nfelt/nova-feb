%  Test used to collect Data for calibration test

%%%%%%%%%%%%%%%%% USER SETTINGS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;
MagMap = zeros(16,8);
StdMap = zeros(16,8);
%set starting index of data file
DataFilePfix = '';
for DataFileIndex = 1:1;
DataFileSfix = '.txt';
%Magnitude of conversion factor using 2.43 mV and 2.03pf
Adc2E = 2.43e-3 * 2.03e-12 * 6.24151e18
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FebSN = 'FEB4.0-00400';
FebDataFolder= strcat('TestData\Caltest\',FebSN(1:4),'_',FebSN(6),'_',FebSN(8:12),'\ChargeInjection\',sprintf('%02d',DataFileIndex),'\');
setenv('FebDataFolder', FebDataFolder);       

    for RiseTime = 0:2:15
        for FallTime = 0:2:7
            BWSelAuto = dec2hex(RiseTime,4);
            TFSelAuto = dec2hex(FallTime,4);
            DataFilename = [DataFilePfix,'Rise',BWSelAuto,'Fall',TFSelAuto,DataFileSfix];
            StdMap(RiseTime+1,FallTime+1)= GetDcsStdSingle([FebDataFolder,DataFilename]);
            MagMap(RiseTime+1,FallTime+1) = Adc2E/GetDcsMagSingle([FebDataFolder,DataFilename]);
        end
    end
disp(['The Max DCS ADC value is ',num2str(max(MagMap))]);

end

dlmwrite([FebDataFolder,'MagMapnew.txt'], MagMap)
