function [] = ReformatSingleToCsv(Filename)
	RawData = char(textread(Filename,'%s', 'whitespace', ' -'));
	chan_length = floor(length(RawData)/5);
	RawData = RawData(1:chan_length*5,:);
	Magnitude = hex2dec(RawData(4:5:length(RawData),2:4));
    dlmwrite([Filename(1:length(Filename)-4),'.csv'], Magnitude)
end


