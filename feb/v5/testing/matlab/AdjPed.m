AdcCntPerBit = 14.382022;
NewBaseline = 200
PrevSetting = [7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7];
figure(1);
plot(chan_data);
title(['DATAin']);
legend('ch0','ch1','ch2','ch3','ch4','ch5','ch6','ch7','ch8','ch9','ch10','ch11','ch12','ch13','ch14','ch15','ch16','ch17','ch18','ch19','ch20','ch21','ch22','ch22','ch23','ch24','ch25','ch26','ch27','ch28','ch29','ch30','ch31');
xlabel('X');
ylabel('Y');
ChanMean = mean (chan_data);
Adjustment = (ChanMean - NewBaseline)/AdcCntPerBit
NewSetting = PrevSetting - Adjustment
