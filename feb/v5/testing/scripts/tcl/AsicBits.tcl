#!/bin/sh
# -*- tcl -*-
# The next line is executed by /bin/sh, but not tcl \
exec tclsh8.4 "$0" ${1+"$@"}

# generate asic serial download registers
set Spare "000000"
set VtSel "110"
set RefSel "0111"
set Isel "011"
set BWSel "0111"
set Gsel "011"
set TfSel "011"
set Mux2to1 "0"
set Mux8to1 "1"
set Offs(0) "00010"
set Mask(0) "0"
set Offs(1) "00010"
set Mask(1) "0"
set Offs(2) "00010"
set Mask(2) "0"
set Offs(3) "00010"
set Mask(3) "0"
set Offs(4) "00010"
set Mask(4) "0"
set Offs(5) "00010"
set Mask(5) "0"
set Offs(6) "00010"
set Mask(6) "0"
set Offs(7) "00010"
set Mask(7) "0"
set Offs(8) "00010"
set Mask(8) "0"
set Offs(9) "00010"
set Mask(9) "0"
set Offs(10) "00010"
set Mask(10) "0"
set Offs(11) "00010"
set Mask(11) "0"
set Offs(12) "00010"
set Mask(12) "0"
set Offs(13) "00010"
set Mask(13) "0"
set Offs(14) "00010"
set Mask(14) "0"
set Offs(15) "00010"
set Mask(15) "0"
set Offs(16) "00010"
set Mask(16) "0"
set Offs(17) "00010"
set Mask(17) "0"
set Offs(18) "00010"
set Mask(18) "0"
set Offs(19) "00010"
set Mask(19) "0"
set Offs(20) "00010"
set Mask(20) "0"
set Offs(21) "00010"
set Mask(21) "0"
set Offs(22) "00010"
set Mask(22) "0"
set Offs(23) "00010"
set Mask(23) "0"
set Offs(24) "00010"
set Mask(24) "0"
set Offs(25) "00010"
set Mask(25) "0"
set Offs(26) "00010"
set Mask(26) "0"
set Offs(27) "00010"
set Mask(27) "0"
set Offs(28) "00010"
set Mask(28) "0"
set Offs(29) "00010"
set Mask(29) "0"
set Offs(30) "00010"
set Mask(30) "0"
set Offs(31) "00010"
set Mask(31) "0"

proc hex2bin hex {
    set t [list 0 0000 1 0001 2 0010 3 0011 4 0100 \
	    5 0101 6 0110 7 0111 8 1000 9 1001 \
	    a 1010 b 1011 c 1100 d 1101 e 1110 f 1111 \
	    A 1010 B 1011 C 1100 D 1101 E 1110 F 1111]
    regsub {^0[xX]} $hex {} hex
    return [string map -nocase $t $hex]
}

proc bin2hex bin {
    ## No sanity checking is done
    array set t {
	0000 0 0001 1 0010 2 0011 3 0100 4
	0101 5 0110 6 0111 7 1000 8 1001 9
	1010 a 1011 b 1100 c 1101 d 1110 e 1111 f
    }
    set diff [expr {4-[string length $bin]%4}]
    if {$diff != 4} {
        set bin [format %0${diff}d$bin 0]
    }
    regsub -all .... $bin {$t(&)} hex
    return [subst $hex]
}

set AsicBits [concat $Mux8to1$Mux2to1$TfSel$Gsel$BWSel$Isel$RefSel$VtSel$Spare]
for {set j 0} {$j<=31} {incr j} {
    set AsicBits [concat $Mask($j)$Offs($j)$AsicBits]   
}
set AsicBits [concat 0000$AsicBits]
#puts $AsicBits
set AsicHex [bin2hex $AsicBits]
#puts $AsicHex
for {set i 0} {$i<8} {incr i} {
    puts [string range $AsicHex [expr {48-$i*8}] [expr {51-$i*8}]]
    puts [string range $AsicHex [expr {52-$i*8}] [expr {55-$i*8}]]
}
puts "Press return to exit"
gets stdin
