proc xml {xml_file} {
global clk_16_period
puts $xml_file.xml
set infile [open $xml_file  r]
set outfile [open logfile.txt w]
set EPT0_FIFO_FULL false
set EPT2_FIFO_FULL false
set EPT0_FIFO_EMPTY true
set EPT2_FIFO_EMPTY true
set WAIT_FOR_PACKET false

while {-1 != [gets $infile command]} {
    set i [string first \" $command]
    if {$i != -1} then {
	set j [string last \"\/ $command"]
	set time [string range $command [expr $i+1] [expr $j-1]]
	set command [string range $command 0 [expr $i -2]]
    }
    switch $command {
	puts $command
	"<writeblock>" {
	    puts "sending packeta"
	    set EPT0_FIFO_FULL true
	    set EPT0_FIFO_EMPTY false
	}
	"<readblock/>" {
	    puts "receiving  packet"
	    set WAIT_FOR_PACKET true
	}
 	"<sleep time" {
	    puts "sleep for $time ms"
	    run  [expr $time *500]ns
	}
    }

    while {$EPT0_FIFO_FULL || $WAIT_FOR_PACKET} {
	puts [examine -hex sim:/top_with_dcm_emulator/usb_fifoadr]
	switch [examine -hex sim:/top_with_dcm_emulator/usb_fifoadr] {
	    0 {
		puts "Pointing to USB address 0"
		puts "Wait for packet = "
		puts $WAIT_FOR_PACKET
		force -freeze sim:/top_with_dcm_emulator/usb_flagb $EPT0_FIFO_FULL 0
		force -freeze sim:/top_with_dcm_emulator/usb_flagc $EPT0_FIFO_EMPTY 0
		if {[examine sim:/top_with_dcm_emulator/usb_sloe] == "true"} then {
		    gets $infile EPT0_FD
		    while {$EPT0_FIFO_FULL == "true"} {
			force -drive sim:/top_with_dcm_emulator/usb_fd 16\#$EPT0_FD 0
			gets $infile EPT0_FD
			if {"</writeblock>" == $EPT0_FD} then {
			    set EPT0_FIFO_FULL false
			    set EPT0_FIFO_EMPTY true
			    force -freeze sim:/top_with_dcm_emulator/usb_flagb $EPT0_FIFO_FULL 0
			    force -freeze sim:/top_with_dcm_emulator/usb_flagc $EPT0_FIFO_EMPTY 0
			}
			run $clk_16_period ns
		    }
		    force -drive sim:/top_with_dcm_emulator/usb_fd ZZZZZZZZZZZZZZZZ 0
		}
	    }

	    2 {
		puts "Pointing to USB address 2"
		force -freeze sim:/top_with_dcm_emulator/usb_flagb $EPT2_FIFO_FULL 1
		force -freeze sim:/top_with_dcm_emulator/usb_flagc $EPT2_FIFO_EMPTY 0
		while {[examine sim:/top_with_dcm_emulator/usb_slwr] == "true"} {
		    puts $outfile [examine -hex sim:/top_with_dcm_emulator/usb_fd]
		    set WAIT_FOR_PACKET false
		    run $clk_16_period ns
		}
	    }

	    default {   
		puts "aaie nutin"
	    }
	}
	run $clk_16_period ns
    }
}
close $infile
close $outfile
}

