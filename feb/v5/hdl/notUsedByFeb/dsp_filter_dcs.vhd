--
-- VHDL Architecture feb_p2_lib.sparsification.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 17:10:44 04/ 2/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;
use nova_feb.register_address_map.all;

entity dsp_filter is
  port(
    BEEBUS_READ     : in    boolean                        := false;
    BEEBUS_STRB     : in    boolean                        := false;
    BEEBUS_CLK      : in    std_logic;
    BEEBUS_DATA     : inout unsigned (15 downto 0)         := x"0000";
    BEEBUS_ADDR     : in    unsigned (15 downto 0)         := x"0000";
    RAW_CHAN_NUMBER : in    unsigned (4 downto 0)          := "00000";
    RAW_TIMESTAMP   : in    unsigned (31 downto 0)         := x"00000000";
    RAW_MAGNITUDE   : in    unsigned (11 downto 0)         := x"000";
    ALL_APD_DATA    : out   std_logic_vector (48 downto 0) := '0' & x"000000000000";
    DATA_STRB       : out   boolean                        := false;
    CLK             : in    std_logic                      := '1';
    OSCOPE_TRIGGER  : in    boolean                        := false;
    ENABLE_DAQ      : in    boolean                        := false
    );

-- Declarations

end dsp_filter;

--
architecture dcs of dsp_filter is

  type CHAN_TYPE is record
    DEL_0     : signed(12 downto 0);
    DEL_1     : signed(12 downto 0);
    DEL_2     : signed(12 downto 0);
    DEL_3     : signed(12 downto 0);
    DCS       : signed(12 downto 0);
    MAGNITUDE : signed(12 downto 0);
    TIMESTAMP : unsigned(31 downto 0);
    triggered : boolean;
  end record CHAN_TYPE;

  type CHAN_DATA_TYPE is array (31 downto 0)
    of CHAN_TYPE;
  
  signal CHAN_DATA : CHAN_DATA_TYPE;

  type TABLE_TYPE is array (31 downto 0)
    of unsigned(11 downto 0);
  
  signal THRESHOLD_TABLE : TABLE_TYPE;

  type TEST_MODE_TYPE is (
    NORMAL_DSP,
    OSCILLOSCOPE
    );

  signal TEST_MODE : TEST_MODE_TYPE := NORMAL_DSP;

  signal TEST_MODE_CMD    : unsigned(3 downto 0)      := x"0";
  signal CHAN_NUMBER_DSP  : unsigned (4 downto 0)     := "00000";
  signal MAGNITUDE_DSP    : signed (12 downto 0)      := "0000000000000";
  signal DATA_STRB_DSP    : boolean                   := false;
  signal TIMESTAMP_DSP    : unsigned(31 downto 0)     := x"00000000";
  signal CHAN_DATA_DCS    : signed (12 downto 0)      := "0000000000000";
  signal CHAN_COUNT       : integer range 31 downto 0 := 0;
  signal TIMESTAMP_CLOCK  : unsigned(31 downto 0)     := x"00000000";
  signal ENABLE_DSP       : boolean                   := false;
  signal OSCOPE_TRIGGERED : boolean                   := false;
  signal BEEBUS_READ_del  : boolean                   := false;
  signal BEEBUS_DATA_int  : unsigned(15 downto 0)     := x"0000";
  
begin
  BEEBUS_DATA <= BEEBUS_DATA_int when BEEBUS_READ_del else "ZZZZZZZZZZZZZZZZ";

  process (BEEBUS_CLK)
  begin
    if BEEBUS_CLK'event and BEEBUS_CLK = '1' then
      BEEBUS_READ_del <= false;
      if BEEBUS_ADDR = DAQ_MODE_ADDR then
        if BEEBUS_READ then
          BEEBUS_DATA_int <= x"000" & TEST_MODE_CMD;
          BEEBUS_READ_del <= true;

        elsif BEEBUS_STRB then
          TEST_MODE_CMD <= BEEBUS_DATA(15 downto 12);
        end if;
      end if;

      if BEEBUS_ADDR(15 downto 5) = THRESHOLD_ADDR then
        if BEEBUS_READ then
          BEEBUS_READ_del <= true;
          BEEBUS_DATA_int <= x"0" & THRESHOLD_TABLE (to_integer(BEEBUS_ADDR(5 downto 0)));
        elsif BEEBUS_STRB then
          THRESHOLD_TABLE (to_integer(BEEBUS_ADDR(5 downto 0))) <= (BEEBUS_DATA(11 downto 0));
        end if;
      end if;
    end if;
  end process;

-- Do DCS with FIR 1 0 0 -1
-- with DCS value trigger when over threshold
-- when drop to < 1/2 threshold then mark as READ_RDY

  process (clk) is
  begin
    if clk'event and clk = '1' then
      CHAN_COUNT                      <= to_integer(RAW_CHAN_NUMBER);
      DATA_STRB_DSP                   <= false;
      CHAN_DATA(CHAN_COUNT).TIMESTAMP <= RAW_TIMESTAMP;
      CHAN_DATA(CHAN_COUNT).DEL_0     <= signed('0' & RAW_MAGNITUDE);
      CHAN_DATA(CHAN_COUNT).DEL_1     <= CHAN_DATA(CHAN_COUNT).DEL_0;
      CHAN_DATA(CHAN_COUNT).DEL_2     <= CHAN_DATA(CHAN_COUNT).DEL_1;
      CHAN_DATA(CHAN_COUNT).DEL_3     <= CHAN_DATA(CHAN_COUNT).DEL_2;
      CHAN_DATA(CHAN_COUNT).DCS       <= CHAN_DATA(CHAN_COUNT).DEL_0 - CHAN_DATA(CHAN_COUNT).DEL_3;
      if (CHAN_DATA(CHAN_COUNT).DCS > CHAN_DATA(CHAN_COUNT).MAGNITUDE) and ENABLE_DSP then
        CHAN_DATA(CHAN_COUNT).TRIGGERED <= true;
        CHAN_DATA(CHAN_COUNT).MAGNITUDE <= CHAN_DATA(CHAN_COUNT).DCS;
      elsif (CHAN_DATA(CHAN_COUNT).DCS < "0000000000000") then
        if CHAN_DATA(CHAN_COUNT).TRIGGERED then
          CHAN_NUMBER_DSP                 <= to_UNSIGNED(CHAN_COUNT, 5);
          TIMESTAMP_DSP                   <= CHAN_DATA(CHAN_COUNT).TIMESTAMP;
          MAGNITUDE_DSP                   <= CHAN_DATA(CHAN_COUNT).MAGNITUDE;
          DATA_STRB_DSP                   <= true;  --2
          CHAN_DATA(CHAN_COUNT).MAGNITUDE <= signed('0' & THRESHOLD_TABLE(CHAN_COUNT));
          CHAN_DATA(CHAN_COUNT).TRIGGERED <= false;
        end if;
      end if;
    end if;
  end process;

  decode_mode : process (clk) is
  begin
    if clk'event and clk = '1' then
      OSCOPE_TRIGGERED <= (OSCOPE_TRIGGER or OSCOPE_TRIGGERED) and ENABLE_DAQ and (TEST_MODE = OSCILLOSCOPE);
      case TEST_MODE_CMD is
        when x"0" =>
          TEST_MODE <= NORMAL_DSP;
        when x"1" =>
          TEST_MODE <= OSCILLOSCOPE;
        when others => null;
      end case;
    end if;
  end process decode_mode;

  select_output : process (TEST_MODE, ENABLE_DAQ, OSCOPE_TRIGGERED, DATA_STRB_DSP,
                           RAW_CHAN_NUMBER, RAW_MAGNITUDE, RAW_TIMESTAMP,
                           CHAN_NUMBER_DSP, MAGNITUDE_DSP, TIMESTAMP_DSP) is
  begin
    case TEST_MODE is
      when NORMAL_DSP =>
        ALL_APD_DATA <= std_logic_vector(CHAN_NUMBER_DSP)
                        & std_logic_vector(TIMESTAMP_DSP)
                        & std_logic_vector(MAGNITUDE_DSP(11 downto 0));
        DATA_STRB  <= DATA_STRB_DSP and ENABLE_DAQ;
        ENABLE_DSP <= ENABLE_DAQ;
      when OSCILLOSCOPE =>
        ALL_APD_DATA <= std_logic_vector(RAW_CHAN_NUMBER)
                        & std_logic_vector(RAW_TIMESTAMP)
                        & std_logic_vector(RAW_MAGNITUDE);
        DATA_STRB  <= OSCOPE_TRIGGERED;
        ENABLE_DSP <= false;
      when others =>
        null;
    end case;
  end process select_output;

end architecture dcs;


