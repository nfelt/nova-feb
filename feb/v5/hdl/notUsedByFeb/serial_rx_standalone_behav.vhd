--
-- VHDL Architecture nova_feb.serial_rx_standalone.behav
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 14:52:08 05/26/10
--
-- using Mentor Graphics HDL Designer(TM) 2009.1 (Build 12)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity serial_rx_standalone is
  port(
    kout_out          : out   std_logic;
    data_rx_out       : out   std_logic_vector (3 downto 0);
    SPYBUS_ADDR       : in    unsigned (15 downto 0);
    SPYBUS_DATA       : inout unsigned (31 downto 0);
    SPYBUS_CLK        : in    std_logic;
    SPYBUS_DATA_SEL   : in    boolean;
    BUFFER_READ_READY : out   boolean;
    BUFFER_EMPTY      : out   boolean;
    PKT_END_EARLY     : out   boolean;
    DCM_PKT_END       : out   boolean := false;
    SERIAL_RX         : in    std_logic;
    RESET             : in    boolean;
    CLK_32            : in    std_logic
    );

-- Declarations

end serial_rx_standalone;

--
architecture behav of serial_rx_standalone is
  signal SHIPT_REG_RX_COMMA      : boolean                       := false;
  signal COMMA_FOUND             : boolean                       := false;
  signal COMMA_FOUND_TWICE       : boolean                       := false;
  signal READ_DATA_COUNT         : std_logic_vector(14 downto 0) := "000000000000000";
  signal SPYBUS_DATA_int         : std_logic_vector(15 downto 0) := x"0000";
  signal ENCODED_RX              : std_logic_vector(9 downto 0)  := "0000000000";
  signal DATA_RX                 : std_logic_vector(7 downto 0)  := "00000000";
  signal DATA_RX_STRB            : std_logic                     := '1';
  signal SHIFT_REG_RX            : std_logic_vector(9 downto 0)  := "0000000000";
  signal BIT_COUNT               : unsigned(3 downto 0)          := x"0";
  signal LINKED_int              : boolean                       := false;
  signal DATA_GOOD               : boolean                       := false;
  signal KOUT                    : std_logic                     := '0';
  signal CLK_3_2_int             : std_logic                     := '0';
  signal CLK_3_2_comb            : std_logic                     := '0';
  signal BUFFER_FULL             : std_logic                     := '0';
  signal BUFFER_EMPTY_int        : std_logic                     := '0';
  signal BUFFER_ALMOST_EMPTY_int : std_logic                     := '0';
  signal SPYBUS_DATA_SEL_int     : std_logic                     := '0';
  signal RX_CODE_ERROR                : std_logic                     := '0';
  signal SPYBUS_DATA_SEL_del     : boolean                       := false;
  signal RESET_int               : std_logic                     := '0';
  signal WORDS_IN_PKT            : integer range 15 downto 0     := 0;

  component decode_8b10b
    port (
      clk      : in  std_logic;
      din      : in  std_logic_vector(9 downto 0);
      dout     : out std_logic_vector(7 downto 0);
      kout     : out std_logic;
      code_err : out std_logic);
  end component;

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;


  component fifo_rx_packet_buff
    port (
      rst           : in  std_logic;
      wr_clk        : in  std_logic;
      rd_clk        : in  std_logic;
      din           : in  std_logic_vector(7 downto 0);
      wr_en         : in  std_logic;
      rd_en         : in  std_logic;
      dout          : out std_logic_vector(15 downto 0);
      full          : out std_logic;
      empty         : out std_logic;
      rd_data_count : out std_logic_vector(14 downto 0));
  end component;

begin

  DCM_COMM_DECODE : decode_8b10b
    port map (
      clk      => CLK_3_2_int,
      din      => ENCODED_RX,
      dout     => DATA_RX,
      kout     => KOUT,
      code_err => RX_CODE_ERROR
      );

  FRAME_BITS : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '1' then
      BIT_COUNT  <= BIT_COUNT +1;
      LINKED_int <= LINKED_int and RESET_int = '0';
      if SHIPT_REG_RX_COMMA or (BIT_COUNT = 9) then
        BIT_COUNT         <= x"0";
        COMMA_FOUND       <= SHIPT_REG_RX_COMMA;
        COMMA_FOUND_TWICE <= COMMA_FOUND and SHIPT_REG_RX_COMMA;
        LINKED_int        <= (COMMA_FOUND_TWICE or LINKED_int) and BIT_COUNT = x"9";
        --FIX LINK STATUS AND ERROR FLAG
      end if;

    end if;
  end process FRAME_BITS;
  SHIPT_REG_RX_COMMA <= (SHIFT_REG_RX = "1001111100") or
                        (SHIFT_REG_RX = "0110000011");

  SER_DES : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '1' then
      SHIFT_REG_RX <= SERIAL_RX & SHIFT_REG_RX(9 downto 1);

      if BIT_COUNT = 9 then
        ENCODED_RX <= SHIFT_REG_RX;
      end if;
      
    end if;
  end process SER_DES;

  DATA_GOOD_SINC : process (CLK_3_2_int) is
  begin
    if CLK_3_2_int'event and CLK_3_2_int = '1' then
      DATA_GOOD <= LINKED_int;
--        RESET_int   <= bool2sl(RESET) or RX_CODE_ERROR;

    end if;
  end process DATA_GOOD_SINC;

  CLOCK_GEN_3_2 : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '1' then
      --PUT RISING EDGE OF 3_2 CLOCK HALFWAY THROUGH DATA FRAME
      CLK_3_2_COMB <= BOOL2SL(((BIT_COUNT = 4) or
                               (BIT_COUNT = 5) or
                               (BIT_COUNT = 6))
                              and LINKED_int);
    end if;
    
  end process CLOCK_GEN_3_2;

  CLK_32_BUFG : BUFG
    port map (
      O => CLK_3_2_int,
      I => CLK_3_2_COMB
      );

  DATA_RX_STRB <= (not KOUT) and bool2sl(DATA_GOOD);

  RECEIVED_DATA_BUFFER : fifo_rx_packet_buff
    port map (
      rst           => RESET_int,
      wr_clk        => CLK_3_2_int,
      rd_clk        => SPYBUS_CLK,
      din           => DATA_RX,
      wr_en         => DATA_RX_STRB,
      rd_en         => SPYBUS_DATA_SEL_int,
      dout          => SPYBUS_DATA_int,
      full          => BUFFER_FULL,
      empty         => BUFFER_EMPTY_int,
      rd_data_count => READ_DATA_COUNT);

  DELAY_READ_ENABLE : process (SPYBUS_CLK) is
  begin
    if SPYBUS_CLK'event and SPYBUS_CLK = '1' then
      SPYBUS_DATA_SEL_del <= SPYBUS_DATA_SEL;
    end if;
  end process DELAY_READ_ENABLE;

  SPYBUS_DATA <= "ZZZZZZZZZZZZZZZZ" & unsigned(SPYBUS_DATA_int)
                 when SPYBUS_DATA_SEL
                 else "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";

  PARSE_HEADER : process (SPYBUS_CLK) is
  begin
    if SPYBUS_CLK'event and SPYBUS_CLK = '1' then
      if RESET_int = '1' then
        WORDS_IN_PKT <= 0;
      end if;
      if WORDS_IN_PKT = 0 and BUFFER_EMPTY_int = '0' then
        case SPYBUS_DATA_int(15 downto 13) is
          when "100" =>
            WORDS_IN_PKT  <= 2;
            PKT_END_EARLY <= true;
          when "001" =>
            WORDS_IN_PKT  <= 5;
            PKT_END_EARLY <= false;
          when "010" =>
            WORDS_IN_PKT  <= 5;
            PKT_END_EARLY <= false;
          when "101" =>
            WORDS_IN_PKT  <= 3;
            PKT_END_EARLY <= false;
          when others =>
            WORDS_IN_PKT <= 0;
        end case;
      elsif SPYBUS_DATA_SEL and not (WORDS_IN_PKT = 0) then
        WORDS_IN_PKT <= WORDS_IN_PKT - 1;
      end if;

    end if;
  end process PARSE_HEADER;

  BUFFER_READ_READY <= (("00000000000" & to_unsigned(WORDS_IN_PKT, 4)) <= unsigned(READ_DATA_COUNT))and not
                       (WORDS_IN_PKT = 0);
  DCM_PKT_END <= WORDS_IN_PKT = 1;
  RESET_int   <= bool2sl(RESET) or RX_CODE_ERROR;
  SPYBUS_DATA_SEL_int <= bool2sl(SPYBUS_DATA_SEL and
                                 (SPYBUS_ADDR = x"E108"));
  BUFFER_EMPTY <= BUFFER_EMPTY_int = '1';
  data_rx_out  <= data_rx(3 downto 0);
  kout_out     <= kout;
  
end architecture behav;


