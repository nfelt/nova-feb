--
-- VHDL Architecture nova_feb.clk2sig.behav
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 10:20:16 04/14/10
--
-- using Mentor Graphics HDL Designer(TM) 2008.1a (Build 9)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;
LIBRARY unisim;
USE unisim.all;
library harvard_std;USE harvard_std.lppc_custom_fn_pkg.all;

ENTITY clk2sig IS
   PORT( 
      CLK_32   : IN     std_logic;
      CLK_64   : IN     std_logic;
      SPI_SCLK : OUT    std_logic
   );

-- Declarations

END clk2sig ;

architecture behav of clk2sig is
  signal CLK_int  : std_logic := '0';
  signal CLK2_int : std_logic := '0';
begin
  clkdiv : process (clk_64) is
  begin
    if CLK_64'event and CLK_64 = '1' then
      CLK_int  <= not CLK_int;
      TEMP_CLK <= CLK_int;
    end if;
  end process clkdiv;

  usb_clk : process (CLK_32) is
  begin  -- process usb_clk
    if CLK_32'event and CLK_32 = '1' then  -- rising clock edge
      CLK2_int <= not CLK2_int;
    end if;
  end process usb_clk;

end architecture behav;

