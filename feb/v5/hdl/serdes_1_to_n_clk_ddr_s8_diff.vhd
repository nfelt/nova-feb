------------------------------------------------------------------------------
-- COPYRIGHT (C) 2009 XILINX, INC.
-- THIS DESIGN IS CONFIDENTIAL AND PROPRIETARY OF XILINX, ALL RIGHTS RESERVED.
------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /   VENDOR: XILINX
-- \   \   \/    VERSION: 1.0
--  \   \        FILENAME: SERDES_1_TO_N_CLK_DDR_S8_DIFF.VHD
--  /   /        DATE LAST MODIFIED:  NOVEMBER 5 2009
-- /___/   /\    DATE CREATED: AUGUST 1 2008
-- \   \  /  \
--  \___\/\___\
-- 
--DEVICE:   SPARTAN 6
--PURPOSE:      1-BIT GENERIC 1:N DDR CLOCK RECEIVER MODULE FOR SERDES FACTORS FROM 2 TO 8 WITH DIFFERENTIAL INPUTS
--      INSTANTIATES NECESSARY BUFIO2 CLOCK BUFFERS
--
--REFERENCE:
--    
--REVISION HISTORY:
--    REV 1.0 - FIRST CREATED (NICKS)
------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;

library UNISIM;
use UNISIM.VCOMPONENTS.all;

entity SERDES_1_TO_N_CLK_DDR_S8_DIFF is
  generic (
    S         : integer := 8;        -- PARAMETER TO SET THE SERDES FACTOR 1..8
    DIFF_TERM : boolean := false) ;  -- ENABLE OR DISABLE INTERNAL DIFFERENTIAL TERMINATION
  port (
    CLKIN_P         : in  std_logic;    -- INPUT FROM LVDS RECEIVER PIN
    CLKIN_N         : in  std_logic;    -- INPUT FROM LVDS RECEIVER PIN
    RXIOCLKP        : out std_logic;    -- IO CLOCK NETWORK
    RXIOCLKN        : out std_logic;    -- IO CLOCK NETWORK
    RX_SERDESSTROBE : out std_logic;    -- PARALLEL DATA CAPTURE STROBE
    RX_BUFG_X1      : out std_logic) ;  -- GLOBAL CLOCK
end SERDES_1_TO_N_CLK_DDR_S8_DIFF;

architecture ARCH_SERDES_1_TO_N_CLK_DDR_S8_DIFF of SERDES_1_TO_N_CLK_DDR_S8_DIFF is

  signal DDLY_M        : std_logic;     -- MASTER OUTPUT FROM IODELAY1
  signal DDLY_S        : std_logic;     -- SLAVE OUTPUT FROM IODELAY1
  signal RX_CLK_IN     : std_logic;     --
  signal IOB_DATA_IN_P : std_logic;     --
  signal IOB_DATA_IN_N : std_logic;     --
  signal RX_CLK_IN_P   : std_logic;     --
  signal RX_CLK_IN_N   : std_logic;     --
  signal RX_BUFIO2_X1  : std_logic;     --

  constant RX_SWAP_CLK : std_logic := '0';  -- PINSWAP MASK FOR INPUT CLOCK (0 = NO SWAP (DEFAULT), 1 = SWAP). ALLOWS INPUT TO BE CONNECTED THE WRONG WAY ROUND TO EASE PCB ROUTING.

begin

  IOB_CLK_IN : IBUFDS_DIFF_OUT generic map(
    DIFF_TERM => DIFF_TERM)
    port map (
      I  => CLKIN_P,
      IB => CLKIN_N,
      O  => RX_CLK_IN_P,
      OB => RX_CLK_IN_N) ;

  IOB_DATA_IN_P <= RX_CLK_IN_P xor RX_SWAP_CLK;  -- INVERT CLOCK AS REQUIRED
  IOB_DATA_IN_N <= RX_CLK_IN_N xor RX_SWAP_CLK;  -- INVERT CLOCK AS REQUIRED


  IODELAY_M : IODELAY2 generic map(
    DATA_RATE          => "SDR",        -- <SDR>, DDR
    SIM_TAPDELAY_VALUE => 49,       -- NOMINAL TAP DELAY (SIM PARAMETER ONLY)
    IDELAY_VALUE       => 0,            -- {0 ... 255}
    IDELAY2_VALUE      => 0,            -- {0 ... 255}
    ODELAY_VALUE       => 0,            -- {0 ... 255}
    IDELAY_MODE        => "NORMAL",     -- "NORMAL", "PCI"
    SERDES_MODE        => "MASTER",     -- <NONE>, MASTER, SLAVE
    IDELAY_TYPE        => "FIXED",  -- "DEFAULT", "DIFF_PHASE_DETECTOR", "FIXED", "VARIABLE_FROM_HALF_MAX", "VARIABLE_FROM_ZERO"
    COUNTER_WRAPAROUND => "STAY_AT_LIMIT",  -- <STAY_AT_LIMIT>, WRAPAROUND
    DELAY_SRC          => "IDATAIN")    -- "IO", "IDATAIN", "ODATAIN"
    port map (
      IDATAIN  => IOB_DATA_IN_P,        -- DATA FROM MASTER IOB
      TOUT     => open,                 -- TRI-STATE SIGNAL TO IOB
      DOUT     => open,                 -- OUTPUT DATA TO IOB
      T        => '1',     -- TRI-STATE CONTROL FROM OLOGIC/OSERDES2
      ODATAIN  => '0',                  -- DATA FROM OLOGIC/OSERDES2
      DATAOUT  => DDLY_M,               -- OUTPUT DATA 1 TO ILOGIC/ISERDES2
      DATAOUT2 => open,                 -- OUTPUT DATA 2 TO ILOGIC/ISERDES2
      IOCLK0   => '0',                  -- HIGH SPEED CLOCK FOR CALIBRATION
      IOCLK1   => '0',                  -- HIGH SPEED CLOCK FOR CALIBRATION
      CLK      => '0',     -- FABRIC CLOCK (GCLK) FOR CONTROL SIGNALS
      CAL      => '0',                  -- CALIBRATE ENABLE SIGNAL
      INC      => '0',                  -- INCREMENT COUNTER
      CE       => '0',                  -- CLOCK ENABLE
      RST      => '0',     -- RESET DELAY LINE TO 1/2 MAX IN THIS CASE
      BUSY     => open) ;  -- OUTPUT SIGNAL INDICATING SYNC CIRCUIT HAS FINISHED / CALIBRATION HAS FINISHED


  IODELAY_S : IODELAY2 generic map(
    DATA_RATE          => "SDR",        -- <SDR>, DDR
    SIM_TAPDELAY_VALUE => 49,       -- NOMINAL TAP DELAY (SIM PARAMETER ONLY)
    IDELAY_VALUE       => 0,            -- {0 ... 255}
    IDELAY2_VALUE      => 0,            -- {0 ... 255}
    ODELAY_VALUE       => 0,            -- {0 ... 255}
    IDELAY_MODE        => "NORMAL",     -- "NORMAL", "PCI"
    SERDES_MODE        => "SLAVE",      -- <NONE>, MASTER, SLAVE
    IDELAY_TYPE        => "FIXED",  -- "DEFAULT", "DIFF_PHASE_DETECTOR", "FIXED", "VARIABLE_FROM_HALF_MAX", "VARIABLE_FROM_ZERO"
    COUNTER_WRAPAROUND => "STAY_AT_LIMIT",  -- <STAY_AT_LIMIT>, WRAPAROUND
    DELAY_SRC          => "IDATAIN")    -- "IO", "IDATAIN", "ODATAIN"
    port map (
      IDATAIN  => IOB_DATA_IN_N,        -- DATA FROM SLAVE IOB
      TOUT     => open,                 -- TRI-STATE SIGNAL TO IOB
      DOUT     => open,                 -- OUTPUT DATA TO IOB
      T        => '1',  -- TRI-STATE CONTROL FROM OLOGIC/OSERDES2
      ODATAIN  => '0',                  -- DATA FROM OLOGIC/OSERDES2
      DATAOUT  => DDLY_S,               -- OUTPUT DATA 1 TO ILOGIC/ISERDES2
      DATAOUT2 => open,                 -- OUTPUT DATA 2 TO ILOGIC/ISERDES2
      IOCLK0   => '0',                  -- HIGH SPEED CLOCK FOR CALIBRATION
      IOCLK1   => '0',                  -- HIGH SPEED CLOCK FOR CALIBRATION
      CLK      => '0',  -- FABRIC CLOCK (GCLK) FOR CONTROL SIGNALS
      CAL      => '0',  -- CALIBRATE CONTROL SIGNAL, NEVER NEEDED AS THE SLAVE SUPPLIES THE CLOCK INPUT TO THE PLL
      INC      => '0',                  -- INCREMENT COUNTER
      CE       => '0',                  -- CLOCK ENABLE
      RST      => '0',                  -- RESET DELAY LINE
      BUSY     => open) ;  -- OUTPUT SIGNAL INDICATING SYNC CIRCUIT HAS FINISHED / CALIBRATION HAS FINISHED

  BUFG_PLL_X1 : BUFG port map (I => RX_BUFIO2_X1, O => RX_BUFG_X1);

  BUFIO2_2CLK_INST : BUFIO2_2CLK generic map(
    DIVIDE => S)  -- THE DIVCLK DIVIDER DIVIDE-BY VALUE; DEFAULT 1
    port map (
      I            => DDLY_M,           -- INPUT SOURCE CLOCK 0 DEGREES
      IB           => DDLY_S,           -- INPUT SOURCE CLOCK 0 DEGREES
      IOCLK        => RXIOCLKP,         -- OUTPUT CLOCK FOR IO
      DIVCLK       => RX_BUFIO2_X1,     -- OUTPUT DIVIDED CLOCK
      SERDESSTROBE => RX_SERDESSTROBE) ;  -- OUTPUT SERDES STROBE (CLOCK ENABLE)

  BUFIO2_INST : BUFIO2 generic map(
    I_INVERT      => false,             --
    DIVIDE_BYPASS => false,             --
    USE_DOUBLER   => false)             --
    port map (
      I            => DDLY_S,           -- N_CLK INPUT FROM IDELAY
      IOCLK        => RXIOCLKN,         -- OUTPUT CLOCK
      DIVCLK       => open,             -- OUTPUT DIVIDED CLOCK
      SERDESSTROBE => open) ;           -- OUTPUT SERDES STROBE (CLOCK ENABLE)

end ARCH_SERDES_1_TO_N_CLK_DDR_S8_DIFF;
