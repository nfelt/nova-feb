--
-- VHDL Architecture feb_p2_lib.output_buffer.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 11:49:19 03/ 8/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity output_buffer is
  port(
    APD_DATA_IN           : in  std_logic_vector (50 downto 0) := "000"& x"000000000000";
    APD_DATA_IN_STRB      : in  boolean                        := false;
    APD_DATA_IN_CLK       : in  std_logic                      := '0';
    APD_DATA_OUT          : out unsigned (50 downto 0)         := "000"& x"000000000000";
    APD_DATA_OUT_STRB     : in  boolean                        := false;
    APD_DATA_BUFFER_EMPTY : out boolean                        := false;
    APD_DATA_BUFFER_FULL  : out boolean                        := false;
    APD_DATA_OFLOW_ERR    : out boolean                        := false;
    OUT_CLK               : in  std_logic                      := '0';
    CLK_16                : in  std_logic                      := '0';
    RESET                 : in  boolean                        := false
    );

-- Declarations

end output_buffer;

architecture behav of output_buffer is

  component fifo_apd_data
    port (
      rst    : in  std_logic;
      wr_clk : in  std_logic;
      rd_clk : in  std_logic;
      din    : in  std_logic_vector(50 downto 0);
      wr_en  : in  std_logic;
      rd_en  : in  std_logic;
      dout   : out std_logic_vector(50 downto 0);
      full   : out std_logic;
      empty  : out std_logic);
  end component;


  signal APD_DATA_BUFFER_EMPTY_int : std_logic                     := '0';
  signal APD_DATA_BUFFER_FULL_int  : std_logic                     := '0';
  signal APD_DATA_IN_STRB_int      : std_logic                     := '0';
  signal APD_DATA_OUT_STRB_int     : std_logic                     := '0';
  signal APD_DATA_OUT_int          : std_logic_vector(50 downto 0) := "000"& x"000000000000";
  signal RESET_int                 : std_logic                     := '0';
  
begin

  OUTPUT_BUFFER : fifo_apd_data
    port map (
      rst    => RESET_int,
      wr_clk => APD_DATA_IN_CLK,
      rd_clk => OUT_CLK,
      din    => APD_DATA_IN,
      wr_en  => APD_DATA_IN_STRB_int,
      rd_en  => APD_DATA_OUT_STRB_int,
      dout   => APD_DATA_OUT_int,
      full   => APD_DATA_BUFFER_FULL_int,
      empty  => APD_DATA_BUFFER_EMPTY_int);

  --pulse overflow error on 16MHz clock
  OVERFLOW_SIG_GEN : process (APD_DATA_IN_CLK)
  begin
    if APD_DATA_IN_CLK'event and APD_DATA_IN_CLK = '1' then
      APD_DATA_OFLOW_ERR <= (APD_DATA_IN_STRB and APD_DATA_BUFFER_FULL_int = '1')
                            or (APD_DATA_OFLOW_ERR and not APD_DATA_OFLOW_ERR_16_int);
    end if;
  end process OVERFLOW_SIG_GEN;

  OVERFLOW_SIG_CLK_16 : process (CLK_16)
  begin
    if CLK_16'event and CLK_16 = '1' then
      APD_DATA_OFLOW_ERR_16_int <= APD_DATA_OFLOW_ERR;
    end if;
  end process OVERFLOW_SIG_CLK_16;

  APD_DATA_OFLOW_ERR_16 <= APD_DATA_OFLOW_ERR_16_int;
  APD_DATA_BUFFER_EMPTY <= APD_DATA_BUFFER_EMPTY_int = '1';
  APD_DATA_BUFFER_FULL  <= APD_DATA_BUFFER_FULL_int = '1';
  APD_DATA_OUT          <= unsigned(APD_DATA_OUT_int);
  APD_DATA_OUT_STRB_int <= bool2sl(APD_DATA_OUT_STRB);
  APD_DATA_IN_STRB_int  <= bool2sl(APD_DATA_IN_STRB);
  RESET_int             <= bool2sl(RESET);
end architecture behav;


