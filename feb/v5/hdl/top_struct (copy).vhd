-- VHDL A/E
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 16:52:30 09/17/11
--
-- Top level inst for NOvA FEB4.0, FEB4.1 
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;

entity top is
  port(
    ADC_DOUTAN           : in  std_logic_vector (11 downto 0);
    ADC_DOUTAP           : in  std_logic_vector (11 downto 0);
    ADC_DOUTBN           : in  std_logic_vector (11 downto 0);
    ADC_DOUTBP           : in  std_logic_vector (11 downto 0);
    ADC_ERROR            : in  boolean;
    ASIC_SHIFTOUT        : in  std_logic;
    DCM_CLK_N            : in  std_logic;
    DCM_CLK_P            : in  std_logic;
    DCM_COMMAND_N        : in  std_logic;
    DCM_COMMAND_P        : in  std_logic;
    DCM_SYNC_N           : in  std_logic;
    DCM_SYNC_P           : in  std_logic;
    EXT_TRIG_IN_N        : in  std_logic;
    EXT_TRIG_IN_P        : in  std_logic;
    SPI_DOUT             : in  std_logic;
    ADC_CLKINN           : out std_logic;
    ADC_CLKINP           : out std_logic;
    ASIC_CHIPRESET       : out boolean;
    ASIC_OUTCLK_N        : out std_logic;
    ASIC_OUTCLK_P        : out std_logic;
    ASIC_SHAPERRST       : out boolean;
    ASIC_SHIFTIN         : out std_logic;
    ASIC_SRCK            : out std_logic;
    ASIC_TESTINJECT      : out boolean;
    DCM_DATA_N           : out std_logic;
    DCM_DATA_P           : out std_logic;
    INSTRUMENT_TRIGGER_N : out std_logic;
    INSTRUMENT_TRIGGER_P : out std_logic;
    LA                   : out std_logic_vector (6 downto 0);
    SER_NUM_CS_B         : out boolean;
    SPI_DIN              : out std_logic;
    SPI_SCLK             : out std_logic;
    TECC_ENABLE_B        : out boolean;
    TEC_ADC_CH1_B_CH2    : out std_logic;
    TEC_ADC_CS_B         : out boolean;
    TEC_DAC_LDAC_B       : out boolean;
    TEC_DAC_SYNC_B       : out boolean;
    TEMP_SENSOR_CS_B     : out boolean
    );
end top;

architecture struct of top is

  signal ADC_BUFFER_FIFO_EMPTY   : boolean;
  signal ADC_BUFFER_FIFO_FULL    : boolean;
  signal ADC_CLK_OUT             : std_logic;
  signal ADC_DOUTA               : std_logic_vector(11 downto 0);  -- output
  signal ADC_DOUTB               : std_logic_vector(11 downto 0);  -- output
  signal APD_DATA                : unsigned(50 downto 0);
  signal APD_DATA_BUFFER_EMPTY   : boolean;
  signal APD_DATA_BUFFER_FULL    : boolean;
  signal APD_DATA_OFLOW_ERR      : boolean;
  signal APD_DATA_STRB           : boolean;
  signal ASIC_CHIPRESET_int      : boolean;
  signal ASIC_CLK                : std_logic;
  signal ASIC_CLK_OUT            : std_logic;
  signal ASIC_IBIAS              : boolean;
  signal ASIC_SHIFTIN_int        : std_logic;
  signal ASIC_SRCK_int           : std_logic;
  signal BEEBUS_ADDR             : unsigned(15 downto 0);
  signal BEEBUS_CLK              : std_logic;
  signal BEEBUS_DATA             : unsigned(15 downto 0);
  signal BEEBUS_READ             : boolean;
  signal BEEBUS_STRB             : boolean;
  signal BUFFER_READ_RDY         : boolean;
  signal CLK_16                  : std_logic;
  signal CLK_32                  : std_logic;
  signal CLK_3_2                 : std_logic;
  signal CLK_4                   : std_logic;
  signal CLK_50                  : std_logic;
  signal CLK_64                  : std_logic;
  signal CLK_IN                  : std_logic;
  signal COMM_ERROR              : boolean;
  signal CURRENT_TIME            : unsigned(31 downto 0);
  signal DATA                    : unsigned(11 downto 0);
  signal DATA_BUFFER_GT_511      : boolean;
  signal DATA_STRB               : boolean                       := false;
  signal DCM_CLK_IN              : std_logic;
  signal DCM_COMMAND             : std_logic;
  signal DCM_DATA                : std_logic;
  signal DCM_SYNC                : boolean;
  signal DCM_SYNC_EN             : boolean;
  signal DCM_SYNC_in             : std_logic;
  signal ENABLE_DAQ              : boolean;
  signal ENABLE_STATUS_PKT       : boolean                       := false;
  signal ENABLE_TIME             : boolean                       := false;
  signal EXTERNAL_TRIGGER        : boolean                       := false;
  signal EXT_TRIG_IN             : std_logic;
  signal HIT_DATA_OUT            : std_logic_vector(50 downto 0) := "000" & x"000000000000";
  signal INSTRUMENT_TRIGGER      : std_logic;
  signal INSTRUMENT_TRIGGER_int  : boolean                       := false;
  signal INTERNAL_OR_EXT_TRIG_IN : boolean                       := false;
  signal INTERNAL_TRIGGER        : boolean                       := false;
  signal LOCAL_CLK               : std_logic;
  signal PACKET_ERROR            : boolean;
  signal POWERUP_RESET           : boolean;
  signal RAW_CHAN_NUMBER         : unsigned(4 downto 0)          := "00000";
  signal RAW_MAGNITUDE           : unsigned(11 downto 0);
  signal RAW_MAGNITUDE_ADC       : unsigned(11 downto 0);
  signal RESET                   : boolean;
  signal RESET_DAQ               : boolean;
  signal RX_LINKED               : boolean;
  signal SER_NUM_CS              : boolean;
  signal SLO_CTRL_CLK            : std_logic;
  signal SYS_CLK                 : std_logic;
  signal TECC_ENABLE             : boolean                       := false;
  signal TEC_ADC_CS              : boolean;
  signal TEC_DAC_LDAC            : boolean;
  signal TEC_DAC_SYNC            : boolean;
  signal TEMP_SENSOR_CS          : boolean;
  signal TICK_1US                : boolean;
  signal TIMING_PKT_FEB_STATUS   : std_logic_vector(7 downto 0);
  signal asic_on                 : boolean;
  signal USE_CLK16_INV           : boolean;

begin
  --Data Flow

  --combine 2x16 ADC streams into 1x32
  U_1 : entity nova_feb.adc_interface
    port map (
      BEEBUS_ADDR    => BEEBUS_ADDR,
      BEEBUS_DATA    => BEEBUS_DATA,
      BEEBUS_READ    => BEEBUS_READ,
      BEEBUS_STRB    => BEEBUS_STRB,
      CURRENT_TIME   => CURRENT_TIME,
      D_IN_A         => ADC_DOUTA,
      D_IN_B         => ADC_DOUTB,
      -- enable external trigger
      --   DATA           => RAW_MAGNITUDE_ADC,
      DATA           => RAW_MAGNITUDE,
      --
      CHAN_NUMBER    => RAW_CHAN_NUMBER,
      RESET_CHAN_PTR => ASIC_CHIPRESET_int,
      ENABLE_DAQ     => ENABLE_DAQ,
      ENABLE_TIME    => ENABLE_TIME,
      DCM_SYNC       => DCM_SYNC,
      TICK_1US       => TICK_1US,
      CLK_16         => CLK_16,
      CLK_32         => CLK_32,
      CLK_64         => CLK_64,
      USE_CLK16_INV  => USE_CLK16_INV
      );

--  U_11 : entity nova_feb.data_pipeline
--    port map (
--      CLK_64                  => CLK_64,
--      DATA                    => RAW_MAGNITUDE_ADC,
--      RAW_MAGNITUDE           => RAW_MAGNITUDE,
--      INTERNAL_TRIGGER        => INTERNAL_TRIGGER,
--      EXT_TRIG_IN             => EXT_TRIG_IN,
--      INTERNAL_OR_EXT_TRIG_IN => INTERNAL_OR_EXT_TRIG_IN
--      );
--  U_12 : lvds_ibuf
--    port map (
--      O  => EXT_TRIG_IN,
--      I  => EXT_TRIG_IN_P,
--      IB => EXT_TRIG_IN_N
--      );

  --Data trigger and processing
  U_2 : entity nova_feb.dsp_filter
    port map (
      BEEBUS_READ      => BEEBUS_READ,
      BEEBUS_STRB      => BEEBUS_STRB,
      CLK_16           => CLK_16,
      BEEBUS_DATA      => BEEBUS_DATA,
      BEEBUS_ADDR      => BEEBUS_ADDR,
      RAW_CHAN_NUMBER  => RAW_CHAN_NUMBER,
      RAW_TIMESTAMP    => CURRENT_TIME,
      RAW_MAGNITUDE    => RAW_MAGNITUDE,
      HIT_DATA_OUT     => HIT_DATA_OUT,
      DATA_STRB        => DATA_STRB,
      CLK_64           => CLK_64,
      -- enable data pipeline
      --INTERNAL_TRIGGER => INTERNAL_OR_EXT_TRIG_IN,
      INTERNAL_TRIGGER => INTERNAL_TRIGGER,

      ENABLE_DAQ => ENABLE_DAQ,
      TICK_1US   => TICK_1US,
      DCM_SYNC   => DCM_SYNC,
      RESET      => POWERUP_RESET
      );

  --Buffer for APD Data
  U_0 : entity nova_feb.output_buffer
    port map (
      APD_DATA_IN           => HIT_DATA_OUT,
      APD_DATA_IN_STRB      => DATA_STRB,
      APD_DATA_IN_CLK       => CLK_64,
      APD_DATA_OUT          => APD_DATA,
      APD_DATA_OUT_STRB     => APD_DATA_STRB,
      APD_DATA_BUFFER_EMPTY => APD_DATA_BUFFER_EMPTY,
      APD_DATA_BUFFER_FULL  => APD_DATA_BUFFER_FULL,
      APD_DATA_OFLOW_ERR    => APD_DATA_OFLOW_ERR,
      OUT_CLK               => CLK_32,
      CLK_16                => CLK_16,
      RESET                 => POWERUP_RESET
      );

  -- create data packet and send via DCM protocol
  U_4 : entity nova_feb.dcm_comm_interface
    port map (
      APD_DATA              => APD_DATA,
      APD_DATA_BUFFER_EMPTY => APD_DATA_BUFFER_EMPTY,
      CLK_16                => CLK_16,
      CURRENT_TIME          => CURRENT_TIME,
      ENABLE_STATUS_PKT     => ENABLE_STATUS_PKT,
      ENABLE_TIME           => ENABLE_TIME,
      RESET                 => POWERUP_RESET,
      SERIAL_RX             => DCM_COMMAND,
      TIMING_PKT_FEB_STATUS => TIMING_PKT_FEB_STATUS,
      clk_32                => CLK_32,
      APD_DATA_STRB         => APD_DATA_STRB,
      CLK_3_2               => CLK_3_2,
      COMM_ERROR            => COMM_ERROR,
      FEBUS_ADDR            => BEEBUS_ADDR,
      FEBUS_READ            => BEEBUS_READ,
      FEBUS_STRB            => BEEBUS_STRB,
      PACKET_ERROR          => PACKET_ERROR,
      RX_LINKED             => RX_LINKED,
      SERIAL_TX             => DCM_DATA,
      TM_APD_PKT_ERROR      => open,
      FEBUS_DATA            => BEEBUS_DATA
      );

  --DAQ control via reg read/write
  --SPI interface
  --ASIC Programming
  --DAQ mode sttting
  U_5 : entity nova_feb.controller
    port map (
      APD_DATA_BUFFER_FULL => APD_DATA_BUFFER_FULL,
      ASIC_SHIFTOUT        => ASIC_SHIFTOUT,
      BEEBUS_ADDR          => BEEBUS_ADDR,
      BEEBUS_CLK           => CLK_16,
      BEEBUS_READ          => BEEBUS_READ,
      BEEBUS_STRB          => BEEBUS_STRB,
      CLK_16               => CLK_16,
      CLK_4                => CLK_4,
      CURRENT_TIME         => CURRENT_TIME,
      SPI_DOUT             => SPI_DOUT,
      ASIC_CHIPRESET       => ASIC_CHIPRESET_int,
      ASIC_IBIAS           => ASIC_IBIAS,
      ASIC_SHIFTIN         => ASIC_SHIFTIN_int,
      ASIC_SRCK            => ASIC_SRCK_int,
      ASIC_TESTINJECT      => ASIC_TESTINJECT,
      DCM_SYNC_EN          => DCM_SYNC_EN,
      ENABLE_DAQ           => ENABLE_DAQ,
      ENABLE_STATUS_PKT    => ENABLE_STATUS_PKT,
      ENABLE_TIME          => ENABLE_TIME,
      INSTRUMENT_TRIGGER   => INSTRUMENT_TRIGGER_int,
      INTERNAL_TRIGGER     => INTERNAL_TRIGGER,
      RESET_DAQ            => RESET_DAQ,
      SER_NUM_CS           => SER_NUM_CS,
      SPI_DIN              => SPI_DIN,
      SPI_SCLK             => SPI_SCLK,
      TECC_ENABLE          => TECC_ENABLE,
      TEC_ADC_CH1_B_CH2    => TEC_ADC_CH1_B_CH2,
      TEC_ADC_CS           => TEC_ADC_CS,
      TEC_DAC_LDAC         => TEC_DAC_LDAC,
      TEC_DAC_SYNC         => TEC_DAC_SYNC,
      TEMP_SENSOR_CS       => TEMP_SENSOR_CS,
      BEEBUS_DATA          => BEEBUS_DATA
      );

  --Generate Clocks
  U_9 : entity nova_feb.dcm_comm_emu_clk
    port map (
      DCM_CLK_IN    => DCM_CLK_IN,
      ENABLE_DAQ    => ENABLE_DAQ,
      USE_CLK16_INV => USE_CLK16_INV,
      CLK_3_2       => open,
      CLK_4         => CLK_4,
      CLK_16        => CLK_16,
      CLK_32        => CLK_32,
      CLK_64        => CLK_64,
      ADC_CLK_OUT   => ADC_CLK_OUT,
      ASIC_CLK_OUT  => ASIC_CLK_OUT,
      USB_IFCLK     => open,
      POWERUP_RESET => POWERUP_RESET
      );

  --collect status bits
  U_3 : entity nova_feb.status
    port map (
      ADC_ERROR             => ADC_ERROR,
      APD_DATA_BUFFER_EMPTY => APD_DATA_BUFFER_EMPTY,
      APD_DATA_BUFFER_FULL  => APD_DATA_BUFFER_FULL,
      BEEBUS_ADDR           => BEEBUS_ADDR,
      BEEBUS_READ           => BEEBUS_READ,
      BEEBUS_STRB           => BEEBUS_STRB,
      CLK_16                => CLK_16,
      COMM_ERROR            => COMM_ERROR,
      ENABLE_DAQ            => ENABLE_DAQ,
      DCM_SYNC_EN           => DCM_SYNC_EN,
      ENABLE_STATUS_PKT     => ENABLE_STATUS_PKT,
      ENABLE_TIME           => ENABLE_TIME,
      TECC_ENABLE           => TECC_ENABLE,
      OVERFLOW_ERROR        => APD_DATA_OFLOW_ERR,
      PACKET_ERROR          => PACKET_ERROR,
      POWERUP_RESET         => POWERUP_RESET,
      BEEBUS_DATA           => BEEBUS_DATA,
      TIMING_PKT_FEB_STATUS => TIMING_PKT_FEB_STATUS
      );

  U_8 : entity harvard_std.lvds_ibuf
    port map (
      O  => DCM_SYNC_IN,
      I  => DCM_SYNC_P,
      IB => DCM_SYNC_N
      );
  DCM_SYNC <= DCM_SYNC_IN = '1' and DCM_SYNC_EN;

  U_10 : entity harvard_std.lvds_ibuf
    port map (
      O  => DCM_COMMAND,
      I  => DCM_COMMAND_P,
      IB => DCM_COMMAND_N
      );

  U_7 : entity harvard_std.lvds_ibufg
    port map (
      i  => DCM_CLK_P,
      ib => DCM_CLK_N,
      o  => DCM_CLK_IN
      );
  I6 : entity harvard_std.lvds_ibufn
    port map (
      i  => ADC_DOUTAP,
      ib => ADC_DOUTAN,
      o  => ADC_DOUTA
      );
  I9 : entity harvard_std.lvds_ibufn
    port map (
      i  => ADC_DOUTBP,
      ib => ADC_DOUTBN,
      o  => ADC_DOUTB
      );
  I13 : entity harvard_std.lvds_obuf
    port map (
      i  => ASIC_CLK_OUT,
      o  => ASIC_OUTCLK_P,
      ob => ASIC_OUTCLK_N
      );
  I14 : entity harvard_std.lvds_obuf
    port map (
      i  => DCM_DATA,
      o  => DCM_DATA_P,
      ob => DCM_DATA_N
      );
  I15 : entity harvard_std.lvds_obuf
    port map (
      i  => ADC_CLK_OUT,
      o  => ADC_CLKINP,
      ob => ADC_CLKINN
      );

  --Internal signsl access and Logic analyzer
  ASIC_SHAPERRST <= false;
  ASIC_SRCK      <= ASIC_SRCK_int;
  ASIC_SHIFTIN   <= ASIC_SHIFTIN_int;

  ASIC_DELAY_RESET : process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '0' then
      ASIC_CHIPRESET <= ASIC_CHIPRESET_int;
    end if;
  end process ASIC_DELAY_RESET;

  LA(6 downto 0)   <= "00" & bool2sl(INSTRUMENT_TRIGGER_int) & bool2sl(ENABLE_TIME) & bool2sl(DCM_SYNC_EN) & '1' & bool2sl(DCM_SYNC);
  TEC_DAC_SYNC_B   <= not TEC_DAC_SYNC;
  TEMP_SENSOR_CS_B <= not TEMP_SENSOR_CS;
  TEC_ADC_CS_B     <= not TEC_ADC_CS;
  TEC_DAC_LDAC_B   <= not TEC_DAC_LDAC;
  TECC_ENABLE_B    <= not TECC_ENABLE;
  SER_NUM_CS_b     <= not SER_NUM_CS;

  INSTRUMENT_TRIGGER <= bool2sl(INSTRUMENT_TRIGGER_int);

end struct;
