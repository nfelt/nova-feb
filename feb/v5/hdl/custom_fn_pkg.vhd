library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library nova_feb;
use nova_feb.register_address_map.all;
--use nova_feb.custom_fn_pkg.all;

-- Package definition
package custom_fn_pkg is
  function or_bool_reduce(arg : boolean_vector_type) return boolean;
end custom_fn_pkg;

package body custom_fn_pkg is
  function or_bool_reduce (arg : boolean_vector_type)
    return boolean is
    variable Upper, Lower : boolean;
    variable Half         : integer;
    variable BUS_int      : boolean_vector_type (arg'length - 1 downto 0);
    variable Result       : boolean;
  begin
    if (arg'length < 1) then            -- In the case of a NULL range
      Result := false;
    else
      BUS_int := arg;
      if (BUS_int'length = 1) then
        Result := BUS_int (BUS_int'left);
      elsif (BUS_int'length = 2) then
        Result := BUS_int (BUS_int'right) or BUS_int (BUS_int'left);
      else
        Half := (BUS_int'length + 1) / 2 + BUS_int'right;
        Upper  := or_bool_reduce (BUS_int (BUS_int'left downto Half));
        Lower  := or_bool_reduce (BUS_int (Half - 1 downto BUS_int'right));
        Result := Upper or Lower;
      end if;
    end if;
    return Result;
  end;
end custom_fn_pkg;
