--
-- VHDL Architecture feb_p2_lib.adc_interface.behav
--
-- Created:
--          BY - NATE.UNKNOWN (HEPLPC2)
--          AT - 11:43:21 01/22/2007
--
-- USING MENTOR GRAPHICS HDL DESIGNER(TM) 2006.1 (BUILD 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library UNISIM;
use UNISIM.all;
library HARVARD_STD;
use HARVARD_STD.LPPC_CUSTOM_FN_PKG.all;
library NOVA_FEB;
use NOVA_FEB.REGISTER_ADDRESS_MAP.all;

entity adc_ser_interface is
  port(
    ADC_DOUT_P : in std_logic_vector (15 downto 0);
    ADC_DOUT_N : in std_logic_vector (15 downto 0);

    ADC_CLKOUT0_P : in std_logic;
    ADC_CLKOUT0_N : in std_logic;
    ADC_FRAME0_P  : in std_logic;
    ADC_FRAME0_N  : in std_logic;

    ADC_CLKOUT1_P : in std_logic;
    ADC_CLKOUT1_N : in std_logic;
    ADC_FRAME1_P  : in std_logic;
    ADC_FRAME1_N  : in std_logic;

    MAGNITUDE_CH_ASYNC : out MAGNITUDE_VECTOR_TYPE(15 downto 0);

    RESET        : in  boolean;
    CLK_16        : in  std_logic;
    USE_CLK16_INV : in boolean
    );

-- DECLARATIONS

end ADC_SER_INTERFACE;

--
architecture BEHAV of ADC_SER_INTERFACE is
  
  signal DATA_FRAME0_P  : std_logic_vector (8 downto 0);
  signal DATA_FRAME0_N  : std_logic_vector (8 downto 0);
  signal DATA_FRAME1_P  : std_logic_vector (8 downto 0);
  signal DATA_FRAME1_N  : std_logic_vector (8 downto 0);
  signal ADC_CHAN_DATA0 : MAGNITUDE_VECTOR_TYPE(8 downto 0);
  signal ADC_CHAN_DATA1 : MAGNITUDE_VECTOR_TYPE(8 downto 0);
begin

  DATA_FRAME0_P <= ADC_FRAME0_P & ADC_DOUT_P(7 downto 0);
  DATA_FRAME0_N <= ADC_FRAME0_N & ADC_DOUT_N(7 downto 0);
  DATA_FRAME1_P <= ADC_FRAME1_P & ADC_DOUT_P(15 downto 8);
  DATA_FRAME1_N <= ADC_FRAME1_N & ADC_DOUT_N(15 downto 8);

  DIFF_RX0 : entity NOVA_FEB.GBT_SER_RX
    port map (
      DATAIN_P      => DATA_FRAME0_P,
      DATAIN_N      => DATA_FRAME0_N,
      CLKIN_P       => ADC_CLKOUT0_P,
      CLKIN_N       => ADC_CLKOUT0_N,
      ADC_CHAN_DATA => ADC_CHAN_DATA0,
      RESET         => bool2sl(RESET),
      CLK_16        => CLK_16
      );

  DIFF_RX1 : entity NOVA_FEB.GBT_SER_RX
    port map (
      DATAIN_P      => DATA_FRAME1_P,
      DATAIN_N      => DATA_FRAME1_N,
      CLKIN_P       => ADC_CLKOUT1_P,
      CLKIN_N       => ADC_CLKOUT1_N,
      ADC_CHAN_DATA => ADC_CHAN_DATA1,
      RESET         => bool2sl(RESET),
      CLK_16        => CLK_16
      );
  MAGNITUDE_CH_ASYNC <= ADC_CHAN_DATA1(7 downto 0) & ADC_CHAN_DATA0(7 downto 0);

end architecture BEHAV;
