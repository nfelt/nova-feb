--
-- VHDL Architecture feb_p2_lib.adc_interface.behav
--
-- Created:
--          BY - NATE.UNKNOWN (HEPLPC2)
--          AT - 11:43:21 01/22/2007
--
-- USING MENTOR GRAPHICS HDL DESIGNER(TM) 2006.1 (BUILD 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library UNISIM;
use UNISIM.all;
library HARVARD_STD;
use HARVARD_STD.LPPC_CUSTOM_FN_PKG.all;
library NOVA_FEB;
use NOVA_FEB.REGISTER_ADDRESS_MAP.all;

entity adc_par_interface is
  port(
    ADC_DOUTAN : in std_logic_vector (11 downto 0);
    ADC_DOUTAP : in std_logic_vector (11 downto 0);
    ADC_DOUTBN : in std_logic_vector (11 downto 0);
    ADC_DOUTBP : in std_logic_vector (11 downto 0);

    MAGNITUDE_CH_ASYNC : out MAGNITUDE_VECTOR_TYPE(1 downto 0);

    RESET         : in boolean;
    CLK_32        : in std_logic;
    USE_CLK16_INV : in boolean
    );

-- DECLARATIONS

end ADC_PAR_INTERFACE;

--
architecture BEHAV of ADC_PAR_INTERFACE is
  signal ADC_DOUTA : std_logic_vector(11 downto 0);  -- output
  signal ADC_DOUTB : std_logic_vector(11 downto 0);  -- output

begin
  
  I6 : entity harvard_std.lvds_ibufn
    port map (
      i  => ADC_DOUTAP,
      ib => ADC_DOUTAN,
      o  => ADC_DOUTA
      );

  I9 : entity harvard_std.lvds_ibufn
    port map (
      i  => ADC_DOUTBP,
      ib => ADC_DOUTBN,
      o  => ADC_DOUTB
      );

  APD_DATA_IN : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '1' then
      MAGNITUDE_CH_ASYNC(0) <= unsigned(ADC_DOUTA);
      MAGNITUDE_CH_ASYNC(1) <= unsigned(ADC_DOUTB);
    end if;
  end process;
  
end architecture BEHAV;
