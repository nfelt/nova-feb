--
-- VHDL Architecture feb_p2_lib.dcm_comm_emu_clk.struct
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 16:38:44 11/25/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- Generate all clocking signals based on board XTAL and
-- FNAL DCM LVDS communication clock.
-------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;

entity dcm_comm_emu_clk is
  port(
    DCM_CLK_IN_P  : in  std_logic;
    DCM_CLK_IN_N  : in  std_logic;
    ENABLE_DAQ    : in  boolean;
    USE_CLK16_INV : in  boolean;
    CLK_3_2       : out std_logic;
    CLK_4         : out std_logic;
    CLK_16        : out std_logic;
    CLK_32        : out std_logic;
    CLK_64        : out std_logic;
    CLK_128       : out std_logic;
    ADC_CLK0_OUT  : out std_logic;
    ADC_CLK1_OUT  : out std_logic;
    ASIC_CLK_OUT  : out std_logic;
    USB_IFCLK     : out std_logic;
    POWERUP_RESET : out boolean := true
    );

-- Declarations

end dcm_comm_emu_clk;

--
architecture struct of dcm_comm_emu_clk is

  component feb_clk_6
    port
      (                                 -- Clock in ports
        CLK_IN1_P : in  std_logic;
        CLK_IN1_N : in  std_logic;
        CLKFB_IN  : in  std_logic;
        -- Clock out ports
        CLK_OUT1  : out std_logic;
        CLK_OUT2  : out std_logic;
        CLK_OUT3  : out std_logic;
        CLK_OUT4  : out std_logic;
        CLK_OUT5  : out std_logic;
        CLK_OUT6  : out std_logic;
        CLKFB_OUT : out std_logic;
        -- Status and control signals
        LOCKED    : out std_logic
        );
  end component;

  component BUFIO2 is
    generic (
      DIVIDE        : integer;
      DIVIDE_BYPASS : boolean;
      I_INVERT      : boolean;
      USE_DOUBLER   : boolean
      );
    port(
      DIVCLK       : out std_logic;
      IOCLK        : out std_logic;
      SERDESSTROBE : out std_logic;
      I            : in  std_logic
      );
  end component;

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;

  component BUFGCE
    port(
      O  : out std_ulogic;
      CE : in  std_ulogic;
      I  : in  std_ulogic
      );
  end component;

  component ODDR2
    generic (
      DDR_ALIGNMENT : string;
      INIT          : bit;
      SRTYPE        : string
      );
    port (
      Q  : out std_logic;
      C0 : in  std_logic;
      C1 : in  std_logic;
      CE : in  std_logic;
      D0 : in  std_logic;
      D1 : in  std_logic;
      R  : in  std_logic;
      S  : in  std_logic
      );
  end component;

  signal PLL_CLK_32      : std_logic := '0';
  signal PLL_CLK_64      : std_logic := '0';
  signal PLL_CLK_16      : std_logic := '0';
  signal PLL_CLK_16_180  : std_logic := '0';
  signal CLK_4_int       : std_logic := '0';
  signal PLL_CLK_4       : std_logic := '0';
  signal PLL_CLK_128     : std_logic := '0';
  signal CLK_16_180_intr : std_logic := '0';
  signal CLK_16_intr     : std_logic := '0';
  signal LOCKED          : std_logic := '0';
  signal ACLK_R_EDGE     : std_logic := '0';
  signal ACLK_F_EDGE     : std_logic := '0';
  signal PLL_CLKFB_IN    : std_logic := '0';
  signal PLL_CLKFB_OUT   : std_logic := '0';


begin
  CLK_4  <= CLK_4_int;
  CLK_16 <= CLK_16_intr;

  CLK_PLL_COREGEN : feb_clk_6
    port map
    (
      CLK_IN1_P => DCM_CLK_IN_P,
      CLK_IN1_N => DCM_CLK_IN_N,
      CLKFB_IN  => PLL_CLKFB_IN,
      CLK_OUT1  => PLL_CLK_32,
      CLK_OUT2  => PLL_CLK_64,
      CLK_OUT3  => PLL_CLK_16,
      CLK_OUT4  => PLL_CLK_16_180,
      CLK_OUT5  => PLL_CLK_4,
      CLK_OUT6  => PLL_CLK_128,
      CLKFB_OUT => PLL_CLKFB_OUT,

      LOCKED => LOCKED
      );

  --ASIC changed to claock always
  ACLK_R_EDGE <= not BOOL2SL(USE_CLK16_INV);  --and  BOOL2SL(ENABLE_DAQ);
  ACLK_F_EDGE <= BOOL2SL(USE_CLK16_INV);      --and  BOOL2SL(ENABLE_DAQ);

  ASIC_CLK_ODDR2 : ODDR2
    generic map(
      DDR_ALIGNMENT => "NONE",
      INIT          => '0',
      SRTYPE        => "SYNC") 
    port map (
      Q  => ASIC_CLK_OUT,
      C0 => CLK_16_intr,
      C1 => CLK_16_180_intr,
      CE => '1',
      D0 => ACLK_R_EDGE,
      D1 => ACLK_F_EDGE,
      R  => '0',
      S  => '0'
      );

  ADC_CLK0_ODDR2 : ODDR2
    generic map(
      DDR_ALIGNMENT => "NONE",
      INIT          => '0',
      SRTYPE        => "SYNC") 
    port map (
      Q  => ADC_CLK0_OUT,
      C0 => CLK_16_intr,
      C1 => CLK_16_180_intr,
      CE => '1',
      D0 => ACLK_R_EDGE,
      D1 => ACLK_F_EDGE,
      R  => '0',
      S  => '0'
      );

  ADC_CLK1_ODDR2 : ODDR2
    generic map(
      DDR_ALIGNMENT => "NONE",
      INIT          => '0',
      SRTYPE        => "SYNC") 
    port map (
      Q  => ADC_CLK1_OUT,
      C0 => CLK_16_intr,
      C1 => CLK_16_180_intr,
      CE => '1',
      D0 => ACLK_R_EDGE,
      D1 => ACLK_F_EDGE,
      R  => '0',
      S  => '0'
      );

  IFCLK_ODDR2 : ODDR2
    generic map(
      DDR_ALIGNMENT => "NONE",
      INIT          => '0',
      SRTYPE        => "SYNC") 
    port map (
      Q  => USB_IFCLK,
      C0 => CLK_16_intr,
      C1 => CLK_16_180_intr,
      CE => '1',
      D0 => '1',
      D1 => '0',
      R  => '0',
      S  => '0'
      );

  CLK_32_BUFG : BUFG
    port map (
      O => CLK_32,
      I => PLL_CLK_32
      );

  CLK_64_BUFG : BUFG
    port map (
      O => CLK_64,
      I => PLL_CLK_64
      );

  CLK_16_180_BUFG : BUFG
    port map (
      O => CLK_16_180_intr,
      I => PLL_CLK_16_180
      );

  CLK_16_BUFG : BUFG
    port map (
      O => CLK_16_intr,
      I => PLL_CLK_16
      );

  CLK_4_BUFG : BUFG
    port map (
      O => CLK_4_int,
      I => PLL_CLK_4
      );
  CLK_128_BUFG : BUFG
    port map (
      O => CLK_128,
      I => PLL_CLK_128
      );
  CLK_FB_BUFG : BUFG
    port map (
      O => PLL_CLKFB_IN,
      I => PLL_CLKFB_OUT
      );
  powerup_reset_gen : process (clk_4_int) is
  begin
    if clk_4_int'event and clk_4_int = '1' then
      if locked = '1' then
        POWERUP_RESET <= false;
      end if;
    end if;
  end process powerup_reset_gen;

end architecture struct;




