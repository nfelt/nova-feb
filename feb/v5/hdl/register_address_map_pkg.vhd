--
-- VHDL Package Header nova_feb.register_address_map
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 14:48:24 06/29/10
--
-- using Mentor Graphics HDL Designer(TM) 2009.1 (Build 12)
--
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
package REGISTER_ADDRESS_MAP is
  
-------------------------------------------------------------------------------
-- CONFIGURE FOR FEB4 OR FEB5
  constant NSEG      : integer := 1;  -- FEB4 SET TO 4, FEB4 SET TO 1
  constant NADC_DBUS : integer := 2;    -- FEB4 SET TO 16 FEB4 SET TO 2  
-------------------------------------------------------------------------------
  constant NCHPSEG : integer := 32/nSEG;

  type ADC_CHAN_DATA_TYPE is array (8 downto 0) of unsigned(11 downto 0);

  type EVENT_FIFO_VECTOR_TYPE is array (natural range <>) of unsigned(7 downto 0);

  subtype CHAN_NUM_TYPE is unsigned(4 downto 0);
  type CHAN_NUM_VECTOR_TYPE is array (natural range <>) of CHAN_NUM_TYPE;

  subtype TIMESTAMP_TYPE is unsigned(31 downto 0);
  type TIMESTAMP_VECTOR_TYPE is array (natural range <>) of TIMESTAMP_TYPE;

  subtype MAGNITUDE_TYPE is unsigned(11 downto 0);
  type MAGNITUDE_VECTOR_TYPE is array (natural range <>) of MAGNITUDE_TYPE;

  type BOOLEAN_VECTOR_TYPE is array (natural range <>) of boolean;

  constant FIRMWARE_VERS      : unsigned (15 downto 0) := X"0004";
  constant TIMING_CMD_ADDRESS : unsigned (15 downto 0) := X"0000";

  constant CMD_ADDR           : unsigned (15 downto 0) := X"0101";
  constant STATUS_ADDR        : unsigned (15 downto 0) := X"0102";
  constant ERROR_FLAG_ADDR    : unsigned (15 downto 0) := X"0103";
  constant FIRMWARE_VERS_ADDR : unsigned (15 downto 0) := X"0104";
  constant PRESET_TIME_L_ADDR : unsigned (15 downto 0) := X"0010";
  constant PRESET_TIME_U_ADDR : unsigned (15 downto 0) := X"0011";
  constant TEMP_ADDR          : unsigned (15 downto 0) := X"0120";
  constant TEMP_LTS_ADDR      : unsigned (15 downto 0) := X"0121";
  constant TEMP_UTS_ADDR      : unsigned (15 downto 0) := X"0122";
  constant SERIAL_NUMBER_ADDR : unsigned (15 downto 0) := X"0030";

-- COMMAND REGISTER DEFINED COMMANDS
  constant CMD_GET_TEMP          : unsigned (15 downto 0) := X"0020";
  constant CMD_SER_NUM_ADR_RESET : unsigned (15 downto 0) := X"0030";
  constant CMD_RESET_FEB         : unsigned (15 downto 0) := X"1000";
  constant CMD_START_DAQ         : unsigned (15 downto 0) := X"1001";
  constant CMD_STOP_DAQ          : unsigned (15 downto 0) := X"1002";
--  CONSTANT CMD_RESET_TIME      : UNSIGNED (15 DOWNTO 0) := X"2000";
  constant CMD_START_TIME        : unsigned (15 downto 0) := X"2001";
  constant CMD_STOP_TIME         : unsigned (15 downto 0) := X"2002";
  constant CMD_SET_ASIC          : unsigned (15 downto 0) := X"3000";
  constant CMD_DISABLE_TECC      : unsigned (15 downto 0) := X"5000";
  constant CMD_ENABLE_TECC       : unsigned (15 downto 0) := X"5001";
  constant CMD_GET_DRIVE_MON     : unsigned (15 downto 0) := X"5020";
  constant CMD_GET_TEMP_MON      : unsigned (15 downto 0) := X"5030";
  constant CMD_SER_NUM_WRITE_EN  : unsigned (15 downto 0) := X"E030";

  constant DAQ_MODE_ADDR          : unsigned (15 downto 0) := X"1000";
  constant DCS_DSP_MODE           : unsigned (15 downto 0) := X"0000";
  constant OSCILLOSCOPE_MODE      : unsigned (15 downto 0) := X"1000";
  constant MEMORY_MODE            : unsigned (15 downto 0) := X"2000";
  constant MEMORY_LOOP_MODE       : unsigned (15 downto 0) := X"2001";
  constant SINGLE_DATA_POINT_MODE : unsigned (15 downto 0) := X"3000";

  constant CHAN_EN_U_ADDR : unsigned (15 downto 0) := X"1001";
  constant CHAN_EN_L_ADDR : unsigned (15 downto 0) := X"1002";

  constant TIMING_PKT_RATE_ADDR   : unsigned (15 downto 0) := X"1010";
  constant HIGH_VOLTAGE_ADJ_ADDR  : unsigned (15 downto 0) := X"1020";
  constant DATA_REGULATOR_ADDR    : unsigned (15 downto 0) := X"1030";
  constant MP_NSAMPLES_ADDR       : unsigned (15 downto 0) := X"1040";
  constant TRIG_HOLDOFF_TIME_ADDR : unsigned (15 downto 0) := X"1050";

  constant SETPOINT_ADDR          : unsigned (15 downto 0) := X"5010";
  constant DRIVE_MONITOR_ADDR     : unsigned (15 downto 0) := X"5020";
  constant DRIVE_MONITOR_LTS_ADDR : unsigned (15 downto 0) := X"5021";
  constant DRIVE_MONITOR_UTS_ADDR : unsigned (15 downto 0) := X"5022";
  constant TEMP_MONITOR_ADDR      : unsigned (15 downto 0) := X"5030";
  constant TEMP_MONITOR_LTS_ADDR  : unsigned (15 downto 0) := X"5031";
  constant TEMP_MONITOR_UTS_ADDR  : unsigned (15 downto 0) := X"5032";

  constant THRESHOLD_ADDR          : unsigned (10 downto 0) := X"20" & "000";
  constant PULSER_ENABLE_ADDR      : unsigned (15 downto 0) := X"F000";
  constant PULSER_PERIODICITY_ADDR : unsigned (15 downto 0) := X"F001";
  constant PULSER_WIDTH_ADDR       : unsigned (15 downto 0) := X"F002";
  constant sel_seg_addr            : unsigned (15 downto 0) := X"F020";

  constant SPARE_ADDR   : unsigned (15 downto 0) := X"4040";
  constant VTSEL_ADDR   : unsigned (15 downto 0) := X"4041";
  constant REFSEL_ADDR  : unsigned (15 downto 0) := X"4042";
  constant ISEL_ADDR    : unsigned (15 downto 0) := X"4043";
  constant BWSEL_ADDR   : unsigned (15 downto 0) := X"4044";
  constant GSEL_ADDR    : unsigned (15 downto 0) := X"4045";
  constant TFSEL_ADDR   : unsigned (15 downto 0) := X"4046";
  constant MUX2TO1_ADDR : unsigned (15 downto 0) := X"4047";
  constant MUX8TO1_ADDR : unsigned (15 downto 0) := X"4048";
  constant OFFS_ADDR    : unsigned (10 downto 0) := X"40" & "000";
  constant MASK_ADDR    : unsigned (10 downto 0) := X"40" & "001";

-- INTERNAL FEB COMMANDS
  --SPI INTERFACE
  constant CMD_WRITE_DAC       : unsigned(2 downto 0) := "001";
  constant CMD_READ_ADC        : unsigned(2 downto 0) := "010";
  constant CMD_READ_TEMP       : unsigned(2 downto 0) := "011";
  constant CMD_WRITE_SER_NUM   : unsigned(2 downto 0) := "100";
  constant CMD_READ_SER_NUM    : unsigned(2 downto 0) := "101";
  constant CMD_WREN_SER_NUM    : unsigned(2 downto 0) := "110";
  constant CMD_RST_SER_NUM_PTR : unsigned(2 downto 0) := "111";
  
end REGISTER_ADDRESS_MAP;
