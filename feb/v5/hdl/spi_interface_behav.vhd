--
-- VHDL Architecture nova_feb.spi_interface.behav
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 11:13:08 04/14/09
--
-- using Mentor Graphics HDL Designer(TM) 2008.1a (Build 9)

-------------------------------------------------------------------------------
-- !!! the spi din dout needs to have a workaround for FEB3 !!!
-- Needs to have clock domain sync if beebus is changed to async clk

-------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;
use nova_feb.register_address_map.all;

entity spi_interface is
  port(
    BEEBUS_ADDR       : in    unsigned (15 downto 0);
    CLK_16            : in    std_logic;
    BEEBUS_DATA       : inout unsigned (15 downto 0);
    BEEBUS_READ       : in    boolean;
    BEEBUS_STRB       : in    boolean;
    UPDATE_TEMP_REG   : in    boolean;
    CURRENT_TIME      : in    unsigned (31 downto 0);
    CLK_4             : in    std_logic;
    SPI_DIN           : out   std_logic;
    SPI_DOUT          : in    std_logic;
    SPI_SCLK          : out   std_logic;
    TEC_DAC_LDAC      : out   boolean;
    TEC_DAC_SYNC      : out   boolean;
    TEC_ADC_CS        : out   boolean;
    TEC_ADC_CH1_B_CH2 : out   std_logic;
    TEMP_SENSOR_CS    : out   boolean;
    SER_NUM_CS        : out   boolean;
    RAW_SPI_COMMAND   : in    unsigned(18 downto 0);
    S_CMD_BUSY        : out   boolean;
    NEW_S_CMD         : in    boolean
    );

-- Declarations

end spi_interface;
architecture behav of spi_interface is

  component ODDR2
    generic (
      DDR_ALIGNMENT : string;
      INIT          : bit;
      SRTYPE        : string
      );
    port (
      Q  : out std_logic;
      C0 : in  std_logic;
      C1 : in  std_logic;
      CE : in  std_logic;
      D0 : in  std_logic;
      D1 : in  std_logic;
      R  : in  std_logic;
      S  : in  std_logic
      );
  end component;

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;

  component BUFGCE_1
    port(
      O  : out std_ulogic;
      CE : in  std_ulogic;
      I  : in  std_ulogic
      );
  end component;


  signal SPI_COMMAND      : unsigned(2 downto 0)      := "000";
  signal TEC_DAC_LDAC_int : boolean                   := false;
  signal TEC_DAC_LDAC_del : boolean                   := false;
  signal SPI_SHIFT_DONE   : boolean                   := false;
  signal DC_CHAN_SELECT   : std_logic                 := '0';
  signal SPI_DATA         : unsigned(11 downto 0)     := x"000";
  signal BIT_COUNT        : integer range 31 downto 0 := 0;
  signal SPI_SREG         : unsigned(31 downto 0)     := x"00000000";
  signal S_CMD_BUSY_int   : boolean                   := false;
  signal S_CMD_BUSY_sl    : std_logic                 := '0';
  signal not_clk_4        : std_logic                 := '0';
  signal TEMP_MON_TS      : unsigned(31 downto 0)     := x"00000000";
  signal DRIVE_MON_TS     : unsigned(31 downto 0)     := x"00000000";
  signal TEMP_TS          : unsigned(31 downto 0)     := x"00000000";
  signal DRIVE_MON_DATA   : unsigned(11 downto 0)     := x"000";
  signal TEMP_MON_DATA    : unsigned(11 downto 0)     := x"000";
  signal TEMP_DATA        : unsigned(15 downto 0)     := x"0000";
  signal SERIAL_NUMBER    : unsigned(15 downto 0)     := x"0000";
  signal SER_NUM_MEM_ADDR : unsigned(5 downto 0)      := "000000";
  signal SER_NUM_MEM_DATA : unsigned(15 downto 0)     := x"0000";
  signal BEEBUS_READ_del  : boolean                   := false;
  signal BEEBUS_DATA_int  : unsigned(15 downto 0)     := x"0000";
  signal USE_LONG_SREG    : boolean                   := false;
  signal CLEAR_SPI_CS     : boolean                   := false;
  
begin
  BEEBUS_DATA      <= BEEBUS_DATA_int when BEEBUS_READ_del else "ZZZZZZZZZZZZZZZZ";
  SPI_COMMAND      <= RAW_SPI_COMMAND(18 downto 16);
  SER_NUM_MEM_DATA <= RAW_SPI_COMMAND(15 downto 0);
  DC_CHAN_SELECT   <= RAW_SPI_COMMAND(15);
  SPI_DATA         <= RAW_SPI_COMMAND(11 downto 0);
  --Needs to have clock domain sync if beebus is changed to async clk
  RECEIVE_COMMAND : process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '1' then
      BEEBUS_READ_del <= false;
      if BEEBUS_READ then
        case BEEBUS_ADDR is
          when DRIVE_MONITOR_ADDR =>
            BEEBUS_DATA_int <= x"0" & DRIVE_MON_DATA;
            BEEBUS_READ_del <= true;
          when DRIVE_MONITOR_LTS_ADDR =>
            BEEBUS_DATA_int <= DRIVE_MON_TS(15 downto 0);
            BEEBUS_READ_del <= true;
          when DRIVE_MONITOR_UTS_ADDR =>
            BEEBUS_DATA_int <= DRIVE_MON_TS(31 downto 16);
            BEEBUS_READ_del <= true;
          when TEMP_MONITOR_ADDR =>
            BEEBUS_DATA_int <= x"0" & TEMP_MON_DATA;
            BEEBUS_READ_del <= true;
          when TEMP_MONITOR_LTS_ADDR =>
            BEEBUS_DATA_int <= TEMP_MON_TS(15 downto 0);
            BEEBUS_READ_del <= true;
          when TEMP_MONITOR_UTS_ADDR =>
            BEEBUS_DATA_int <= TEMP_MON_TS(31 downto 16);
            BEEBUS_READ_del <= true;
          when TEMP_ADDR =>
            BEEBUS_DATA_int <= TEMP_DATA;
            BEEBUS_READ_del <= true;
          when TEMP_LTS_ADDR =>
            BEEBUS_DATA_int <= TEMP_TS(15 downto 0);
            BEEBUS_READ_del <= true;
          when TEMP_UTS_ADDR =>
            BEEBUS_DATA_int <= TEMP_TS(31 downto 16);
            BEEBUS_READ_del <= true;
          when SERIAL_NUMBER_ADDR =>
            BEEBUS_DATA_int <= SERIAL_NUMBER;
            BEEBUS_READ_del <= true;
          when others => null;
        end case;
      end if;
    end if;
  end process RECEIVE_COMMAND;

  -- SPI bus clocked process
  SDATA_IO : process (CLK_4) is
  begin
    if CLK_4'event and CLK_4 = '1' then
      SPI_SHIFT_DONE <= CLEAR_SPI_CS;
      if NEW_S_CMD and not S_CMD_BUSY_int then
        S_CMD_BUSY_int <= true;
        case SPI_COMMAND is
          when CMD_WRITE_DAC =>
            SPI_SREG(15 downto 0) <= DC_CHAN_SELECT & "100" & SPI_DATA;
            TEC_DAC_SYNC          <= true;
            BIT_COUNT             <= 15;
            USE_LONG_SREG         <= false;
          when CMD_READ_ADC =>
            TEC_ADC_CS        <= true;
            TEC_ADC_CH1_B_CH2 <= DC_CHAN_SELECT;
            BIT_COUNT         <= 15;
            USE_LONG_SREG     <= false;
          when CMD_READ_TEMP =>
            TEMP_SENSOR_CS <= true;
            BIT_COUNT      <= 15;
            USE_LONG_SREG  <= false;
          when CMD_RST_SER_NUM_PTR =>
            BIT_COUNT     <= 7;
            USE_LONG_SREG <= false;
          when CMD_WREN_SER_NUM =>
            SPI_SREG(15 downto 0) <= "00000110" & x"81";
            SER_NUM_CS            <= true;
            BIT_COUNT             <= 7;
            USE_LONG_SREG         <= false;
          when CMD_WRITE_SER_NUM =>
            SPI_SREG      <= "000000100" & SER_NUM_MEM_ADDR & '0' & SER_NUM_MEM_DATA;
            SER_NUM_CS    <= true;
            BIT_COUNT     <= 31;
            USE_LONG_SREG <= true;
          when CMD_READ_SER_NUM =>
            SPI_SREG      <= "000000110" & SER_NUM_MEM_ADDR & '0' & SER_NUM_MEM_DATA;
            SER_NUM_CS    <= true;
            BIT_COUNT     <= 31;
            USE_LONG_SREG <= true;
          when others =>
            null;
        end case;


      elsif BIT_COUNT /= 0 then
        BIT_COUNT <= BIT_COUNT-1;

      else
        S_CMD_BUSY_int <= false;
        --pulse   SPI_SHIFT_DONE to update results     
        CLEAR_SPI_CS   <= S_CMD_BUSY_int;
      end if;
      -- SPI CS is held for 1 clock period aftr last clock edge
      if CLEAR_SPI_CS then
        TEC_DAC_SYNC   <= false;
        TEC_ADC_CS     <= false;
        TEMP_SENSOR_CS <= false;
        SER_NUM_CS     <= false;
      end if;

      if S_CMD_BUSY_int then
        SPI_SREG <= SPI_SREG(30 downto 0) & SPI_DOUT;
      end if;
      
    end if;
  end process SDATA_IO;

  -- when done shifting data update the results
  RESULTS : process (CLK_4) is
  begin
    if CLK_4'event and CLK_4 = '1' then
      TEC_DAC_LDAC_int <= false;
      if SPI_SHIFT_DONE then
        case SPI_COMMAND is
          when CMD_WRITE_DAC =>
            TEC_DAC_LDAC_int <= true;
          when CMD_READ_ADC=>
            if DC_CHAN_SELECT = '0' then
              DRIVE_MON_DATA <= SPI_SREG(11 downto 0);
              DRIVE_MON_TS   <= CURRENT_TIME;
            else
              TEMP_MON_DATA <= SPI_SREG(11 downto 0);
              TEMP_MON_TS   <= CURRENT_TIME;
            end if;
          when CMD_READ_TEMP =>
            TEMP_DATA <= SPI_SREG(15 downto 0);
            TEMP_TS   <= CURRENT_TIME;
          when CMD_RST_SER_NUM_PTR =>
            SER_NUM_MEM_ADDR <= "000000";
          when CMD_WRITE_SER_NUM =>
            SER_NUM_MEM_ADDR <= SER_NUM_MEM_ADDR + 1;
          when CMD_READ_SER_NUM =>
            SERIAL_NUMBER    <= SPI_SREG(15 downto 0);
            SER_NUM_MEM_ADDR <= SER_NUM_MEM_ADDR + 1;
          when others => null;
        end case;
      end if;
    end if;
  end process RESULTS;
  DELAY_SDIN : process (CLK_4) is
  begin
    if CLK_4'event and CLK_4 = '0' then
      if USE_LONG_SREG then
        SPI_DIN <= SPI_SREG(31);
      else
        SPI_DIN <= SPI_SREG(15);
      end if;
    end if;
  end process DELAY_SDIN;

  TEC_DAC_LDAC  <= TEC_DAC_LDAC_int and not TEC_DAC_LDAC_del;
  not_clk_4     <= not clk_4;
  S_CMD_BUSY_sl <= BOOL2SL(S_CMD_BUSY_int);
  S_CMD_BUSY    <= S_CMD_BUSY_int;
  SPI_CLK_BUF : ODDR2
    generic map(
      DDR_ALIGNMENT => "NONE",
      INIT          => '0',
      SRTYPE        => "SYNC") 
    port map (
      Q  => SPI_SCLK,
      C0 => CLK_4,
      C1 => not_clk_4,
      CE => '1',
      D0 => S_CMD_BUSY_sl,
      D1 => '0',
      R  => '0',
      S  => '0'
      );
end architecture behav;




