
################################################################################
# TIMING RULES                                                                 #
################################################################################

################################################################################
# rule assignments for layer timing                                            #
################################################################################
rule layer TOP (restricted_layer_length_factor 1)
rule layer GND (restricted_layer_length_factor 1)
rule layer POWER (restricted_layer_length_factor 1)
rule layer INNER1 (restricted_layer_length_factor 1)
rule layer INNER2 (restricted_layer_length_factor 1)
rule layer INNER3 (restricted_layer_length_factor 1)
rule layer INNER4 (restricted_layer_length_factor 1)
rule layer BOTTOM (restricted_layer_length_factor 1)
