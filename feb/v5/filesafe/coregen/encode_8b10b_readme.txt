The following files were generated for 'encode_8b10b' in directory 
C:\data\projects\nova\firmware\feb_p2\coregen\:

encode_8b10b.edn:
   Electronic Data Netlist (EDN) file containing the information
   required to implement the module in a Xilinx (R) FPGA.

encode_8b10b.vhd:
   VHDL wrapper file provided to support functional simulation. This
   file contains simulation model customization data that is passed to
   a parameterized simulation model for the core.

encode_8b10b.vho:
   VHO template file containing code that can be used as a model for
   instantiating a CORE Generator module in a VHDL design.

encode_8b10b.xco:
   CORE Generator input file containing the parameters used to
   regenerate a core.

encode_8b10b_encode_8b10b_xst_1.ngc:
   Binary Xilinx implementation netlist. The logic implementation of
   certain CORE Generator IP is described by a combination of a top
   level EDN file plus one or more NGC files.

encode_8b10b_flist.txt:
   Text file listing all of the output files produced when a customized
   core was generated in the CORE Generator.

encode_8b10b_readme.txt:
   Text file indicating the files generated and how they are used.

encode_8b10b_xmdf.tcl:
   ISE Project Navigator interface file. ISE uses this file to determine
   how the files output by CORE Generator for the core can be integrated
   into your ISE project.


Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

