onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/usb_fd
add wave -noupdate /top_with_dcm_emulator/usb_flagb
add wave -noupdate /top_with_dcm_emulator/usb_flagc
add wave -noupdate /top_with_dcm_emulator/usb_pktend
add wave -noupdate /top_with_dcm_emulator/usb_reset_b
add wave -noupdate /top_with_dcm_emulator/usb_sloe
add wave -noupdate /top_with_dcm_emulator/usb_slrd
add wave -noupdate /top_with_dcm_emulator/usb_slwr
add wave -noupdate -divider {CLKs and states}
add wave -noupdate /top_with_dcm_emulator/u_1/u_4/clk_3_2_int
add wave -noupdate /top_with_dcm_emulator/u_0/clk_16
add wave -noupdate /top_with_dcm_emulator/u_0/clk_32
add wave -noupdate /top_with_dcm_emulator/u_0/clk_64
add wave -noupdate /top_with_dcm_emulator/u_0/clk_128
add wave -noupdate /top_with_dcm_emulator/u_0/adc_clk_out
add wave -noupdate /top_with_dcm_emulator/u_0/asic_clk_out
add wave -noupdate /top_with_dcm_emulator/u_0/enable_status_pkt
add wave -noupdate /top_with_dcm_emulator/u_0/enable_time
add wave -noupdate /top_with_dcm_emulator/u_0/enable_daq
add wave -noupdate /top_with_dcm_emulator/u_0/asic_chipreset_int
add wave -noupdate -divider {ADC Data In}
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/adc_douta
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/adc_doutb
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/raw_chan_number
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/raw_magnitude
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/current_time
add wave -noupdate -divider Sparsification
add wave -noupdate /top_with_dcm_emulator/u_0/u_2/chan_number
add wave -noupdate /top_with_dcm_emulator/u_0/u_2/chan_data_dcs
add wave -noupdate /top_with_dcm_emulator/u_0/u_2/dsp_timestamp
add wave -noupdate /top_with_dcm_emulator/u_0/u_2/dsp_magnitude
add wave -noupdate /top_with_dcm_emulator/u_0/u_2/dsp_trigger
add wave -noupdate /top_with_dcm_emulator/u_0/u_2/send_tm
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {104496813 ps} 0} {{Cursor 3} {189950100 ps} 0} {{Cursor 4} {193309895 ps} 0} {{Cursor 5} {192370100 ps} 0}
configure wave -namecolwidth 358
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {61766966 ps} {69828922 ps}
