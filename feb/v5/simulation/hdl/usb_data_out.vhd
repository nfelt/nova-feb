
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
use IEEE.std_logic_textio.all;          -- I/O for logic types
library std;
use std.textio.all;

--entity declaration
entity usb_data_out is
  port (
    USB_FD    : in unsigned(15 downto 0);
    USB_SLWR  : in boolean;
    USB_IFCLK : in std_logic
    );
end usb_data_out;

--architecture definition
architecture Behav of usb_data_out is

begin

  writing : process

    file OUTFILE     : text;
    variable fstatus : file_open_status;
    variable outline : line;
    
  begin
    file_open(fstatus, OUTFILE, "simData/usb_data.txt", write_mode);
--    while true loop
    wait until USB_SLWR;
    while USB_SLWR loop
      wait until falling_edge(USB_IFCLK);
      hwrite(outline, std_logic_vector(USB_FD));
      writeline(outfile, outline);
      wait until rising_edge(USB_IFCLK);
    end loop;
--    end loop;

  end process writing;

end Behav;
