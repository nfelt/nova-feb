onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /top_with_dcm_emulator/u_0/clk_128
add wave -noupdate /top_with_dcm_emulator/dcm_data_n
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_1/u_0/shift_reg_rx
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_1/u_0/encoded_rx
add wave -noupdate /top_with_dcm_emulator/u_1/u_0/buffer_full
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_1/u_0/data_rx
add wave -noupdate /top_with_dcm_emulator/u_1/u_0/data_rx_strb
add wave -noupdate /top_with_dcm_emulator/u_0/dcm_sync
add wave -noupdate /top_with_dcm_emulator/u_0/dcm_sync_en
add wave -noupdate /top_with_dcm_emulator/u_0/enable_time
add wave -noupdate /top_with_dcm_emulator/u_0/enable_status_pkt
add wave -noupdate -expand -group {USB IO} -radix hexadecimal /top_with_dcm_emulator/usb_fd
add wave -noupdate -expand -group {USB IO} /top_with_dcm_emulator/usb_ifclk
add wave -noupdate -expand -group {USB IO} /top_with_dcm_emulator/usb_flagb
add wave -noupdate -expand -group {USB IO} /top_with_dcm_emulator/usb_flagc
add wave -noupdate -expand -group {USB IO} /top_with_dcm_emulator/usb_pktend
add wave -noupdate -expand -group {USB IO} /top_with_dcm_emulator/usb_reset_b
add wave -noupdate -expand -group {USB IO} /top_with_dcm_emulator/usb_sloe
add wave -noupdate -expand -group {USB IO} /top_with_dcm_emulator/usb_slrd
add wave -noupdate -expand -group {USB IO} /top_with_dcm_emulator/usb_slwr
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_1/u_4/clk_3_2_int
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/clk_16
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/clk_32
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/clk_64
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/asic_clk_out
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/enable_status_pkt
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/enable_time
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/enable_daq
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/asic_chipreset_int
add wave -noupdate -expand -group {ADC Data In} /top_with_dcm_emulator/u_0/asic_chipreset
add wave -noupdate -expand -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/raw_chan_number
add wave -noupdate -expand -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/raw_magnitude
add wave -noupdate -expand -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/current_time
add wave -noupdate -divider {serial Input}
add wave -noupdate -expand /top_with_dcm_emulator/u_0/timing_inst/chan_number
add wave -noupdate -expand /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(3)/trigger_inst/chan_number
add wave -noupdate /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(3)/trigger_inst/chan_l
add wave -noupdate /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(3)/trigger_inst/chan_number
add wave -noupdate /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(3)/trigger_inst/raw_chan_number
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/dsp_chan_number(0)
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/dsp_magnitude(0)
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/oscope_en
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/oscope_strb
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/trig_holdoff
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(0)/multi_point_ctrl_inst/thead_fifo_din
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(0)/multi_point_ctrl_inst/thead_fifo_rstrb
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(0)/multi_point_ctrl_inst/event_fifo_din
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(0)/multi_point_ctrl_inst/event_fifo_wstrb
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(0)/multi_point_ctrl_inst/n_data_pkt_words
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {94570100 ps} 0} {{Cursor 3} {159861251 ps} 0} {{Cursor 4} {372769452 ps} 0} {{Cursor 5} {193647022 ps} 0} {{Cursor 5} {12847360 ps} 0} {{Cursor 6} {1593077 ps} 0}
configure wave -namecolwidth 348
configure wave -valuecolwidth 45
configure wave -justifyvalue left
configure wave -signalnamewidth 2
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {1042122 ps}
