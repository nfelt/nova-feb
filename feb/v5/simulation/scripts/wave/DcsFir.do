onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/usb_fd
add wave -noupdate /top_with_dcm_emulator/usb_flagb
add wave -noupdate /top_with_dcm_emulator/usb_flagc
add wave -noupdate /top_with_dcm_emulator/usb_pktend
add wave -noupdate /top_with_dcm_emulator/usb_reset_b
add wave -noupdate /top_with_dcm_emulator/usb_sloe
add wave -noupdate /top_with_dcm_emulator/usb_slrd
add wave -noupdate /top_with_dcm_emulator/usb_slwr
add wave -noupdate /top_with_dcm_emulator/u_0/data_strb
add wave -noupdate /top_with_dcm_emulator/u_1/u_6/synca
add wave -noupdate /top_with_dcm_emulator/u_1/u_6/syncb
add wave -noupdate -height 16 /top_with_dcm_emulator/u_1/u_6/current_state
add wave -noupdate /top_with_dcm_emulator/u_0/u_2/chan_magnitude
add wave -noupdate /top_with_dcm_emulator/u_0/u_2/reset
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/u_2/raw_timestamp
add wave -noupdate /top_with_dcm_emulator/u_0/u_2/raw_magnitude
add wave -noupdate /top_with_dcm_emulator/u_0/u_2/chan_number
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/u_4/u_1/current_time
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/u_4/u_1/data_tx
add wave -noupdate /top_with_dcm_emulator/u_0/enable_time
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/beebus_addr
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/beebus_data
add wave -noupdate /top_with_dcm_emulator/u_0/beebus_strb
add wave -noupdate /top_with_dcm_emulator/u_0/u_2/oscope_triggered
add wave -noupdate /top_with_dcm_emulator/u_0/u_2/internal_trigger
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/u_2/data_regulator_reg
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/u_2/data_regulator_cnt
add wave -noupdate /top_with_dcm_emulator/u_0/dcm_sync
add wave -noupdate /top_with_dcm_emulator/u_0/enable_status_pkt
add wave -noupdate /top_with_dcm_emulator/u_0/dcm_sync_en
add wave -noupdate /top_with_dcm_emulator/u_0/enable_time
add wave -noupdate /top_with_dcm_emulator/u_0/dcm_sync_in
add wave -noupdate /top_with_dcm_emulator/u_0/u_4/u_1/status_pkt_clear
add wave -noupdate /top_with_dcm_emulator/u_0/u_4/u_1/status_pkt_ready
add wave -noupdate -divider {ADC Chan# sinc}
add wave -noupdate /top_with_dcm_emulator/u_0/clk_32
add wave -noupdate /top_with_dcm_emulator/u_0/clk_16
add wave -noupdate /top_with_dcm_emulator/u_0/u_1/use_clk16_inv_int
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/u_1/adc_chan_ptr
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/u_1/asic_chan_ptr
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/u_1/asic_chan_ptr_inv
add wave -noupdate /top_with_dcm_emulator/u_0/enable_daq
add wave -noupdate /top_with_dcm_emulator/u_0/adc_clk_out
add wave -noupdate /top_with_dcm_emulator/u_0/asic_clk_out
add wave -noupdate /top_with_dcm_emulator/u_0/dcm_sync
add wave -noupdate /top_with_dcm_emulator/u_0/asic_chipreset_int
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/current_time
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/raw_chan_number
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/adc_douta
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/adc_doutb
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/raw_magnitude
add wave -noupdate -divider {DCS Sigs}
add wave -noupdate /top_with_dcm_emulator/u_0/u_2/chan_magnitude
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/u_2/chan_data_dcs
add wave -noupdate /top_with_dcm_emulator/u_0/u_2/chan_state
add wave -noupdate /top_with_dcm_emulator/u_0/u_2/chan_timestamp
add wave -noupdate /top_with_dcm_emulator/u_0/u_2/chan_tm_grp
add wave -noupdate /top_with_dcm_emulator/u_0/u_2/chan_number
add wave -noupdate /top_with_dcm_emulator/u_0/u_2/data_strb_dsp
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {114020000 ps} 0} {{Cursor 3} {189950100 ps} 0} {{Cursor 4} {193309895 ps} 0} {{Cursor 5} {192370100 ps} 0}
configure wave -namecolwidth 399
configure wave -valuecolwidth 303
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {113131707 ps} {114679068 ps}
