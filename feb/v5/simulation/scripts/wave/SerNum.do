onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic /top_with_dcm_emulator/ser_num_cs_b
add wave -noupdate -format Logic /top_with_dcm_emulator/spi_din
add wave -noupdate -format Logic /top_with_dcm_emulator/spi_dout
add wave -noupdate -format Logic /top_with_dcm_emulator/spi_sclk
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/usb_fd
add wave -noupdate -format Logic /top_with_dcm_emulator/usb_flagb
add wave -noupdate -format Logic /top_with_dcm_emulator/usb_flagc
add wave -noupdate -format Logic /top_with_dcm_emulator/usb_pktend
add wave -noupdate -format Logic /top_with_dcm_emulator/usb_reset_b
add wave -noupdate -format Logic /top_with_dcm_emulator/usb_sloe
add wave -noupdate -format Logic /top_with_dcm_emulator/usb_slrd
add wave -noupdate -format Logic /top_with_dcm_emulator/usb_slwr
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/data_strb
add wave -noupdate -format Literal /top_with_dcm_emulator/u_0/u_2/chan_magnitude
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_2/reset
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_2/raw_timestamp
add wave -noupdate -format Literal /top_with_dcm_emulator/u_0/u_2/raw_magnitude
add wave -noupdate -format Literal /top_with_dcm_emulator/u_0/u_2/chan_number
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_4/u_1/current_time
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_4/u_1/data_tx
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/enable_time
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/beebus_addr
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/beebus_data
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/beebus_strb
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_2/oscope_triggered
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_2/internal_trigger
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_2/data_regulator_reg
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_2/data_regulator_cnt
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/dcm_sync
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/enable_status_pkt
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/dcm_sync_en
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/enable_time
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/enable_daq
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/dcm_sync_in
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/clk_16
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/clk_32
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_4/u_1/status_pkt_clear
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_4/u_1/status_pkt_ready
add wave -noupdate -divider {DCS Sigs}
add wave -noupdate -format Literal /top_with_dcm_emulator/u_0/u_2/chan_magnitude
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_2/chan_data_dcs
add wave -noupdate -format Literal /top_with_dcm_emulator/u_0/u_2/chan_state
add wave -noupdate -format Literal /top_with_dcm_emulator/u_0/u_2/chan_timestamp
add wave -noupdate -format Literal /top_with_dcm_emulator/u_0/u_2/chan_tm_grp
add wave -noupdate -format Literal /top_with_dcm_emulator/u_0/u_2/chan_number
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_2/data_strb_dsp
add wave -noupdate -divider {verify 2 are not coincident}
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_2/dcs_strb
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_2/oscope_strb
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_2/tm_strb
add wave -noupdate -divider {TIme Group}
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_2/tm_grp_current
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_2/tm_grp_process
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_2/tm_grp_wait
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_2/tm_grp_wait_reset
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_2/tm_grp_lut
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_2/tm_grp_lut_rd
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_2/tm_grp_lut_wr
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_2/timestamp
add wave -noupdate -divider {Raw Data in}
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_2/raw_chan_number
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_2/raw_magnitude
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_2/raw_timestamp
add wave -noupdate -divider {Time Marker Count}
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_2/tick_1us
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_2/time_marker_cnt
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_2/time_marker_rate
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_2/tm_strb
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {35890100 ps} 0} {{Cursor 3} {189950100 ps} 0} {{Cursor 4} {193309895 ps} 0} {{Cursor 5} {192370100 ps} 0}
configure wave -namecolwidth 399
configure wave -valuecolwidth 214
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {174805560 ps} {198080018 ps}
