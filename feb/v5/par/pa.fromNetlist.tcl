
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

hdi::project new -name par_11_5 -dir "/Projects/nova/feb/v4/par_11_5/planAhead_run_1" -netlist "/Projects/nova/feb/v4/synthesis/project_1_impl_1/top.edf" -search_path { {/Projects/nova/feb/v4/par_11_5} {../synthesis/project_1_impl_1} }
hdi::project setArch -name par_11_5 -arch spartan6
hdi::param set -name project.pinAheadLayout -bvalue yes
hdi::param set -name project.paUcfFile -svalue "top.ucf"
hdi::floorplan new -name floorplan_1 -part xc6slx16csg324-3 -project par_11_5
hdi::pconst import -project par_11_5 -floorplan floorplan_1 -file "top.ucf"
