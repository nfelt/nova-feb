The following files were generated for 'fifo_16x16_8' in directory
/Projects/nova/feb/v4/cgtemp/

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * fifo_16x16_8.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * fifo_16x16_8.ngc
   * fifo_16x16_8.vhd
   * fifo_16x16_8.vho
   * fifo_generator_ug175.pdf

Creates an HDL instantiation template:
   Creates an HDL instantiation template for the IP.

   * fifo_16x16_8.vho

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * fifo_16x16_8.asy

Generate ISE metadata:
   Create a metadata file for use when including this core in ISE designs

   * fifo_16x16_8_xmdf.tcl

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * fifo_16x16_8.gise
   * fifo_16x16_8.xise

Deliver Readme:
   Text file indicating the files generated and how they are used.

   * fifo_16x16_8_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * fifo_16x16_8_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

