--
-- VHDL Architecture feb_p2_lib.command_interp.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 16:46:10 02/14/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;
use nova_feb.register_address_map.all;

entity command_interp is
  port(
    BEEBUS_ADDR          : in    unsigned (15 downto 0);
    BEEBUS_DATA          : inout unsigned (15 downto 0);
    BEEBUS_CLK           : in    std_logic;
    BEEBUS_READ          : in    boolean;
    BEEBUS_STRB          : in    boolean;
    CLK                  : in    std_logic;
    DCM_SYNC_EN          : out   boolean := true;
    APD_DATA_BUFFER_FULL : in    boolean;
    ENABLE_DAQ           : out   boolean := false;
    UPDATE_TEMP_REG      : out   boolean := false;
    RESET_FEB            : out   boolean := false;
    SET_ASIC             : out   boolean := false;
    RAW_SPI_COMMAND      : out   unsigned (18 downto 0);
    S_CMD_BUSY           : in    boolean;
    NEW_S_CMD            : out   boolean := false;
    ENABLE_TIME          : out   boolean := false;
    ENABLE_STATUS_PKT    : out   boolean := false;
    TECC_ENABLE          : out   boolean := false
    );

-- Declarations

end command_interp;

--
architecture behav of command_interp is

  type CONTOLLER_STATE_TYPE is (IDLE, STOPPING, STOP, RUNNING);
  signal CONTROLLER_STATE      : CONTOLLER_STATE_TYPE  := IDLE;
  signal BEEBUS_DATA_int       : unsigned(15 downto 0) := x"0000";
  signal BEEBUS_READ_del       : boolean               := false;
  signal HIGH_VOLTAGE          : unsigned(11 downto 0) := x"000";
  signal SETPOINT              : unsigned(11 downto 0) := x"000";
  signal NEW_S_CMD_int         : boolean               := false;
  signal DCM_SYNC_EN_int       : boolean               := false;
  signal ENABLE_TIME_int       : boolean               := false;
  signal ENABLE_STATUS_PKT_int : boolean               := false;
  
begin
  BEEBUS_DATA <= BEEBUS_DATA_int when BEEBUS_READ_del else "ZZZZZZZZZZZZZZZZ";

  RECEIVE_COMMAND : process (BEEBUS_CLK) is
  begin
    if BEEBUS_CLK'event and BEEBUS_CLK = '1' then
      BEEBUS_READ_del <= false;
      RESET_FEB       <= false;
      SET_ASIC        <= false;
      ENABLE_DAQ      <= CONTROLLER_STATE = RUNNING;
      NEW_S_CMD_int   <= NEW_S_CMD_int and not S_CMD_BUSY;
      if (BEEBUS_ADDR = CMD_ADDR and BEEBUS_STRB) then
        case BEEBUS_DATA is
          when CMD_GET_TEMP =>
            RAW_SPI_COMMAND <= CMD_READ_TEMP & "0000" & x"000";
            NEW_S_CMD_int   <= true;

          when CMD_RESET_FEB =>
            RESET_FEB <= true;

          when CMD_SET_ASIC =>
            SET_ASIC <= true;

          when CMD_START_DAQ =>
            CONTROLLER_STATE <= RUNNING;

          when CMD_STOP_DAQ =>
            CONTROLLER_STATE <= STOPPING;

          when CMD_DISABLE_TECC =>
            TECC_ENABLE <= false;

          when CMD_ENABLE_TECC =>
            TECC_ENABLE <= true;

          when CMD_GET_DRIVE_MON =>
            RAW_SPI_COMMAND <= CMD_READ_ADC & "0000" & x"000";
            NEW_S_CMD_int   <= true;

          when CMD_GET_TEMP_MON =>
            RAW_SPI_COMMAND <= CMD_READ_ADC & "1000" & x"000";
            NEW_S_CMD_int   <= true;
            
          when CMD_SER_NUM_WRITE_EN =>
            RAW_SPI_COMMAND <= CMD_WREN_SER_NUM & x"0000";
            NEW_S_CMD_int   <= true;
            
          when CMD_SER_NUM_ADR_RESET =>
            RAW_SPI_COMMAND <= CMD_RST_SER_NUM_PTR & x"0000";
            NEW_S_CMD_int   <= true;
            
          when CMD_START_TIME =>
            ENABLE_TIME_int <= true;
            
          when CMD_STOP_TIME =>
            ENABLE_TIME_int <= false;
            
          when others
            => null;
        end case;

        
      else
        if CONTROLLER_STATE = STOPPING then
          CONTROLLER_STATE <= IDLE;
        end if;
        if CONTROLLER_STATE = RUNNING and APD_DATA_BUFFER_FULL then
          CONTROLLER_STATE <= STOPPING;
        end if;

        case BEEBUS_ADDR is
          when HIGH_VOLTAGE_ADJ_ADDR =>
            if BEEBUS_READ then
              BEEBUS_DATA_int <= x"0" & (HIGH_VOLTAGE);
              BEEBUS_READ_del <= true;
            elsif BEEBUS_STRB then
              HIGH_VOLTAGE    <= BEEBUS_DATA(11 downto 0);
              RAW_SPI_COMMAND <= CMD_WRITE_DAC & "0000" &(not(BEEBUS_DATA(11 downto 0)));
              NEW_S_CMD_int   <= true;
            end if;

          when SETPOINT_ADDR =>
            if BEEBUS_READ then
              BEEBUS_DATA_int <= x"0" & SETPOINT;
              BEEBUS_READ_del <= true;
            elsif BEEBUS_STRB then
              SETPOINT        <= BEEBUS_DATA(11 downto 0);
              RAW_SPI_COMMAND <= CMD_WRITE_DAC & "1000" & BEEBUS_DATA(11 downto 0);
              NEW_S_CMD_int   <= true;
            end if;

          when SERIAL_NUMBER_ADDR =>
            if BEEBUS_READ then
              RAW_SPI_COMMAND <= CMD_READ_SER_NUM & x"0000";
              NEW_S_CMD_int   <= true;
            elsif BEEBUS_STRB then
              RAW_SPI_COMMAND <= CMD_WRITE_SER_NUM & BEEBUS_DATA;
              NEW_S_CMD_int   <= true;
            end if;

          when TIMING_CMD_ADDRESS =>
            if BEEBUS_READ then
              BEEBUS_DATA_int <= "0000000000"& BOOL2SL(ENABLE_STATUS_PKT) & "00"& BOOL2SL(DCM_SYNC_EN_int) & BOOL2SL(ENABLE_TIME_int) & '0';
              BEEBUS_READ_del <= true;
            elsif BEEBUS_STRB then
              ENABLE_TIME_int       <= BEEBUS_DATA(1) = '1';
              DCM_SYNC_EN_int       <= BEEBUS_DATA(2) = '1';
              ENABLE_STATUS_PKT_int <= BEEBUS_DATA(5) = '1';
            end if;

          when others => null;
        end case;
      end if;
      
    end if;
  end process RECEIVE_COMMAND;
  NEW_S_CMD         <= NEW_S_CMD_int;
  DCM_SYNC_EN       <= DCM_SYNC_EN_int;
  ENABLE_TIME       <= ENABLE_TIME_int;
  ENABLE_STATUS_PKT <= ENABLE_STATUS_PKT_int;
end architecture behav;


