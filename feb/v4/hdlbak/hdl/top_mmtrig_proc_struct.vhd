-- VHDL A/E
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 16:52:30 09/17/11
--
-- Top level inst for NOvA FEB4.0, FEB4.1 
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;

entity top is
  port(
    ADC_DOUTAN : in  std_logic_vector (11 downto 0);
    ADC_DOUTAP : in  std_logic_vector (11 downto 0);
    ADC_DOUTBN : in  std_logic_vector (11 downto 0);
    ADC_DOUTBP : in  std_logic_vector (11 downto 0);
    ADC_ERROR  : in  boolean;
    ADC_CLKINN : out std_logic;
    ADC_CLKINP : out std_logic;

    ASIC_CHIPRESET  : out boolean;
    ASIC_OUTCLK_N   : out std_logic;
    ASIC_OUTCLK_P   : out std_logic;
    ASIC_SHAPERRST  : out boolean;
    ASIC_SHIFTIN    : out std_logic;
    ASIC_SRCK       : out std_logic;
    ASIC_TESTINJECT : out boolean;
    ASIC_SHIFTOUT   : in  std_logic;

    DCM_CLK_N     : in  std_logic;
    DCM_CLK_P     : in  std_logic;
    DCM_COMMAND_N : in  std_logic;
    DCM_COMMAND_P : in  std_logic;
    DCM_SYNC_N    : in  std_logic;
    DCM_DATA_N    : out std_logic;
    DCM_DATA_P    : out std_logic;
    DCM_SYNC_P    : in  std_logic;

    EXT_TRIG_IN_N        : in  std_logic;
    EXT_TRIG_IN_P        : in  std_logic;
    INSTRUMENT_TRIGGER_N : out std_logic;
    INSTRUMENT_TRIGGER_P : out std_logic;

    SPI_DOUT : in  std_logic;
    SPI_DIN  : out std_logic;
    SPI_SCLK : out std_logic;

    SER_NUM_CS_B      : out boolean;
    TECC_ENABLE_B     : out boolean;
    TEC_ADC_CH1_B_CH2 : out std_logic;
    TEC_ADC_CS_B      : out boolean;
    TEC_DAC_LDAC_B    : out boolean;
    TEC_DAC_SYNC_B    : out boolean;
    TEMP_SENSOR_CS_B  : out boolean;

    LA : out std_logic_vector (6 downto 0)
    );
end top;

architecture struct of top is
  signal ADC_DOUTA : std_logic_vector(11 downto 0);  -- output
  signal ADC_DOUTB : std_logic_vector(11 downto 0);  -- output

  signal RAW_CHAN_NUMBER   : unsigned(4 downto 0) := "00000";
  signal RAW_MAGNITUDE     : unsigned(11 downto 0);
  signal RAW_MAGNITUDE_ADC : unsigned(11 downto 0);

  signal DSP_CHAN_NUMBER : integer range 31 downto 0 := 0;
  signal DSP_TIMESTAMP   : unsigned (31 downto 0)    := x"00000000";
  signal DSP_MAGNITUDE   : unsigned (11 downto 0)    := x"000";
  signal DSP_TRIGGER     : boolean;
  signal SEND_TM         : boolean;

  signal DATA_MEM_FULL_LATCH : boolean               := false;
  signal EVENT_FIFO_DOUT     : unsigned (7 downto 0) := x"00";
  signal EVENT_FIFO_EMPTY    : boolean               := false;
  signal EVENT_FIFO_RSTRB    : boolean               := false;
  signal N_DATA_PKT_WORDS    : unsigned(4 downto 0);

  signal BEEBUS_ADDR : unsigned(15 downto 0);
  signal BEEBUS_CLK  : std_logic;
  signal BEEBUS_DATA : unsigned(15 downto 0);
  signal BEEBUS_READ : boolean;
  signal BEEBUS_STRB : boolean;

  signal ASIC_CHIPRESET_int : boolean;
  signal ASIC_IBIAS         : boolean;
  signal ASIC_SHIFTIN_int   : std_logic;
  signal ASIC_SRCK_int      : std_logic;

  signal CURRENT_TIME      : unsigned(31 downto 0);
  signal ENABLE_STATUS_PKT : boolean := false;
  signal ENABLE_TIME       : boolean := false;
  signal ENABLE_DAQ        : boolean;

  signal DCM_COMMAND : std_logic;
  signal DCM_DATA    : std_logic;
  signal DCM_SYNC    : boolean;
  signal DCM_SYNC_EN : boolean;
  signal DCM_SYNC_in : std_logic;

  signal EXTERNAL_TRIGGER        : boolean := false;
  signal INSTRUMENT_TRIGGER      : std_logic;
  signal INSTRUMENT_TRIGGER_int  : boolean := false;
  signal INTERNAL_OR_EXT_TRIG_IN : boolean := false;
  signal INTERNAL_TRIGGER        : boolean := false;
  signal EXT_TRIG_IN             : std_logic;

  signal RX_LINKED             : boolean;
  signal TIMING_PKT_FEB_STATUS : std_logic_vector(7 downto 0);

  signal SER_NUM_CS     : boolean;
  signal SLO_CTRL_CLK   : std_logic;
  signal SYS_CLK        : std_logic;
  signal TECC_ENABLE    : boolean := false;
  signal TEC_ADC_CS     : boolean;
  signal TEC_DAC_LDAC   : boolean;
  signal TEC_DAC_SYNC   : boolean;
  signal TEMP_SENSOR_CS : boolean;

  signal COMM_ERROR     : boolean;
  signal TX_ERROR       : boolean;
  signal RX_ERROR       : boolean;
  signal READ_REG_ERROR : boolean;
  signal PACKET_ERROR   : boolean;

  signal POWERUP_RESET : boolean;
  signal RESET         : boolean;
  signal RESET_DAQ     : boolean;
  signal CLK_IN        : std_logic;
  signal USE_CLK16_INV : boolean;
  signal TICK_1US      : boolean;
  signal CLK_3_2       : std_logic;
  signal CLK_4         : std_logic;
  signal CLK_16        : std_logic;
  signal CLK_32        : std_logic;
  signal CLK_64        : std_logic;
  signal ADC_CLK_OUT   : std_logic;
  signal ASIC_CLK      : std_logic;
  signal ASIC_CLK_OUT  : std_logic;
  signal clk_128       : std_logic;

begin
  --Data Flow

  --combine 2x16 ADC streams into 1x32
  U_1 : entity nova_feb.adc_interface
    port map (
      BEEBUS_ADDR => BEEBUS_ADDR,
      BEEBUS_DATA => BEEBUS_DATA,
      BEEBUS_READ => BEEBUS_READ,
      BEEBUS_STRB => BEEBUS_STRB,

      D_IN_A => ADC_DOUTA,
      D_IN_B => ADC_DOUTB,

      CHAN_NUMBER  => RAW_CHAN_NUMBER,
      -- enable external trigger
      --   DATA           => RAW_MAGNITUDE_ADC,
      DATA         => RAW_MAGNITUDE,
      --
      CURRENT_TIME => CURRENT_TIME,

      RESET_CHAN_PTR => ASIC_CHIPRESET_int,

      ENABLE_DAQ  => ENABLE_DAQ,
      ENABLE_TIME => ENABLE_TIME,

      DCM_SYNC      => DCM_SYNC,
      TICK_1US      => TICK_1US,
      CLK_16        => CLK_16,
      CLK_32        => CLK_32,
      CLK_64        => CLK_64,
      USE_CLK16_INV => USE_CLK16_INV
      );

--  U_11 : entity nova_feb.data_pipeline
--    port map (
--      CLK_64                  => CLK_64,
--      DATA                    => RAW_MAGNITUDE_ADC,
--      RAW_MAGNITUDE           => RAW_MAGNITUDE,
--      INTERNAL_TRIGGER        => INTERNAL_TRIGGER,
--      EXT_TRIG_IN             => EXT_TRIG_IN,
--      INTERNAL_OR_EXT_TRIG_IN => INTERNAL_OR_EXT_TRIG_IN
--      );
--  U_12 : lvds_ibuf
--    port map (
--      O  => EXT_TRIG_IN,
--      I  => EXT_TRIG_IN_P,
--      IB => EXT_TRIG_IN_N
--      );

  --Data trigger and processing
  U_2 : entity nova_feb.trigger
    port map (
      BEEBUS_ADDR => BEEBUS_ADDR,
      BEEBUS_DATA => BEEBUS_DATA,
      BEEBUS_READ => BEEBUS_READ,
      BEEBUS_STRB => BEEBUS_STRB,

      RAW_CHAN_NUMBER => RAW_CHAN_NUMBER,
      RAW_TIMESTAMP   => CURRENT_TIME,
      RAW_MAGNITUDE   => RAW_MAGNITUDE,

      N_DATA_PKT_WORDS    => N_DATA_PKT_WORDS,
      DATA_MEM_FULL_LATCH => DATA_MEM_FULL_LATCH,

      DSP_CHAN_NUMBER => DSP_CHAN_NUMBER,
      DSP_TIMESTAMP   => DSP_TIMESTAMP,
      DSP_MAGNITUDE   => DSP_MAGNITUDE,
      DSP_TRIGGER     => DSP_TRIGGER,

      SEND_TM          => SEND_TM,
      -- enable data pipeline
      --INTERNAL_TRIGGER => INTERNAL_OR_EXT_TRIG_IN,
      INTERNAL_TRIGGER => INTERNAL_TRIGGER,
      ENABLE_DAQ       => ENABLE_DAQ,

      RESET    => POWERUP_RESET,
      DCM_SYNC => DCM_SYNC,
      CLK_16   => CLK_16,
      CLK_64   => CLK_64,
      TICK_1US => TICK_1US
      );

  multi_point_ctrl_inst : entity nova_feb.multi_point_ctrl
    port map(
      BEEBUS_ADDR => BEEBUS_ADDR,
      BEEBUS_DATA => BEEBUS_DATA,
      BEEBUS_READ => BEEBUS_READ,
      BEEBUS_STRB => BEEBUS_STRB,

      DSP_CHAN_NUMBER => DSP_CHAN_NUMBER,
      DSP_TIMESTAMP   => DSP_TIMESTAMP,
      DSP_MAGNITUDE   => DSP_MAGNITUDE,
      DSP_TRIGGER     => DSP_TRIGGER,

      SEND_TM => SEND_TM,

      DATA_MEM_FULL_LATCH => DATA_MEM_FULL_LATCH,

      EVENT_FIFO_DOUT  => EVENT_FIFO_DOUT,
      EVENT_FIFO_EMPTY => EVENT_FIFO_EMPTY,
      EVENT_FIFO_RSTRB => EVENT_FIFO_RSTRB,
      N_DATA_PKT_WORDS => N_DATA_PKT_WORDS,

      ENABLE_DAQ => ENABLE_DAQ,
      RESET      => POWERUP_RESET,
      CLK_3_2    => CLK_3_2,
      CLK_16     => CLK_16,
      CLK_32     => CLK_32,
      CLK_64     => CLK_64,
      clk_128    => clk_128
      );

  -- create data packet and send via DCM protocol
  U_4 : entity nova_feb.dcm_comm_interface
    port map (
      FEBUS_ADDR => BEEBUS_ADDR,
      FEBUS_DATA => BEEBUS_DATA,
      FEBUS_READ => BEEBUS_READ,
      FEBUS_STRB => BEEBUS_STRB,

      EVENT_FIFO_DOUT  => EVENT_FIFO_DOUT,
      EVENT_FIFO_EMPTY => EVENT_FIFO_EMPTY,
      EVENT_FIFO_RSTRB => EVENT_FIFO_RSTRB,
      N_DATA_PKT_WORDS => N_DATA_PKT_WORDS,

      CURRENT_TIME          => CURRENT_TIME,
      ENABLE_TIME           => ENABLE_TIME,
      ENABLE_STATUS_PKT     => ENABLE_STATUS_PKT,
      TIMING_PKT_FEB_STATUS => TIMING_PKT_FEB_STATUS,

      SERIAL_TX => DCM_DATA,
      SERIAL_RX => DCM_COMMAND,
      RX_LINKED => RX_LINKED,

      COMM_ERROR     => COMM_ERROR,
      RX_Error       => RX_Error,
      TX_Error       => TX_Error,
      READ_REG_Error => READ_REG_Error,

      RESET   => POWERUP_RESET,
      CLK_3_2 => CLK_3_2,
      CLK_16  => CLK_16,
      clk_32  => CLK_32
      );


  --DAQ control via reg read/write
  --SPI interface
  --ASIC Programming
  --DAQ mode sttting
  U_5 : entity nova_feb.controller
    port map (
      --!! include buffer overflow error and don't send crap data
      BEEBUS_ADDR => BEEBUS_ADDR,
      BEEBUS_DATA => BEEBUS_DATA,
      BEEBUS_CLK  => CLK_16,
      BEEBUS_READ => BEEBUS_READ,
      BEEBUS_STRB => BEEBUS_STRB,

      CURRENT_TIME => CURRENT_TIME,

      ASIC_CHIPRESET  => ASIC_CHIPRESET_int,
      ASIC_IBIAS      => ASIC_IBIAS,
      ASIC_SHIFTIN    => ASIC_SHIFTIN_int,
      ASIC_SHIFTOUT   => ASIC_SHIFTOUT,
      ASIC_SRCK       => ASIC_SRCK_int,
      ASIC_TESTINJECT => ASIC_TESTINJECT,

      DCM_SYNC_EN       => DCM_SYNC_EN,
      ENABLE_DAQ        => ENABLE_DAQ,
      ENABLE_STATUS_PKT => ENABLE_STATUS_PKT,
      ENABLE_TIME       => ENABLE_TIME,

      INSTRUMENT_TRIGGER   => INSTRUMENT_TRIGGER_int,
      INTERNAL_TRIGGER     => INTERNAL_TRIGGER,
      APD_DATA_BUFFER_FULL => DATA_MEM_FULL_LATCH,

      SPI_DOUT   => SPI_DOUT,
      SPI_DIN    => SPI_DIN,
      SER_NUM_CS => SER_NUM_CS,
      SPI_SCLK   => SPI_SCLK,

      TECC_ENABLE       => TECC_ENABLE,
      TEC_ADC_CH1_B_CH2 => TEC_ADC_CH1_B_CH2,
      TEC_ADC_CS        => TEC_ADC_CS,
      TEC_DAC_LDAC      => TEC_DAC_LDAC,
      TEC_DAC_SYNC      => TEC_DAC_SYNC,

      TEMP_SENSOR_CS => TEMP_SENSOR_CS,

      RESET_DAQ => RESET_DAQ,
      CLK_4     => CLK_4,
      CLK_16    => CLK_16
      );

  --Generate Clocks
  U_9 : entity nova_feb.dcm_comm_emu_clk
    port map (
      DCM_CLK_IN_P  => DCM_CLK_P,
      DCM_CLK_IN_N  => DCM_CLK_N,
      ENABLE_DAQ    => ENABLE_DAQ,
      USE_CLK16_INV => USE_CLK16_INV,
      CLK_3_2       => open,
      CLK_4         => CLK_4,
      CLK_16        => CLK_16,
      CLK_32        => CLK_32,
      CLK_64        => CLK_64,
      CLK_128       => CLK_128,
      ADC_CLK_OUT   => ADC_CLK_OUT,
      ASIC_CLK_OUT  => ASIC_CLK_OUT,
      USB_IFCLK     => open,
      POWERUP_RESET => POWERUP_RESET
      );

  --collect status bits
  --!! include new error bits
  U_3 : entity nova_feb.status
    port map (
      ADC_ERROR             => ADC_ERROR,
      APD_DATA_BUFFER_EMPTY => TX_ERROR,
      APD_DATA_BUFFER_FULL  => RX_ERROR,
      BEEBUS_ADDR           => BEEBUS_ADDR,
      BEEBUS_READ           => BEEBUS_READ,
      BEEBUS_STRB           => BEEBUS_STRB,
      CLK_16                => CLK_16,
      COMM_ERROR            => COMM_ERROR,
      ENABLE_DAQ            => ENABLE_DAQ,
      DCM_SYNC_EN           => DCM_SYNC_EN,
      ENABLE_STATUS_PKT     => ENABLE_STATUS_PKT,
      ENABLE_TIME           => ENABLE_TIME,
      TECC_ENABLE           => TECC_ENABLE,
      OVERFLOW_ERROR        => READ_REG_Error,
      PACKET_ERROR          => false,
      POWERUP_RESET         => POWERUP_RESET,
      BEEBUS_DATA           => BEEBUS_DATA,
      TIMING_PKT_FEB_STATUS => TIMING_PKT_FEB_STATUS
      );

  U_8 : entity harvard_std.lvds_ibuf
    port map (
      O  => DCM_SYNC_IN,
      I  => DCM_SYNC_P,
      IB => DCM_SYNC_N
      );
  DCM_SYNC <= DCM_SYNC_IN = '1' and DCM_SYNC_EN;

  U_10 : entity harvard_std.lvds_ibuf
    port map (
      O  => DCM_COMMAND,
      I  => DCM_COMMAND_P,
      IB => DCM_COMMAND_N
      );

  I6 : entity harvard_std.lvds_ibufn
    port map (
      i  => ADC_DOUTAP,
      ib => ADC_DOUTAN,
      o  => ADC_DOUTA
      );
  I9 : entity harvard_std.lvds_ibufn
    port map (
      i  => ADC_DOUTBP,
      ib => ADC_DOUTBN,
      o  => ADC_DOUTB
      );
  I13 : entity harvard_std.lvds_obuf
    port map (
      i  => ASIC_CLK_OUT,
      o  => ASIC_OUTCLK_P,
      ob => ASIC_OUTCLK_N
      );
  I14 : entity harvard_std.lvds_obuf
    port map (
      i  => DCM_DATA,
      o  => DCM_DATA_P,
      ob => DCM_DATA_N
      );
  I15 : entity harvard_std.lvds_obuf
    port map (
      i  => ADC_CLK_OUT,
      o  => ADC_CLKINP,
      ob => ADC_CLKINN
      );

  --Internal signsl access and Logic analyzer
  ASIC_SHAPERRST <= false;
  ASIC_SRCK      <= ASIC_SRCK_int;
  ASIC_SHIFTIN   <= ASIC_SHIFTIN_int;

  ASIC_DELAY_RESET : process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '0' then
      ASIC_CHIPRESET <= ASIC_CHIPRESET_int;
    end if;
  end process ASIC_DELAY_RESET;

  LA(6 downto 0)   <= "00" & bool2sl(INSTRUMENT_TRIGGER_int) & bool2sl(ENABLE_TIME) & bool2sl(DCM_SYNC_EN) & '1' & bool2sl(DCM_SYNC);
  TEC_DAC_SYNC_B   <= not TEC_DAC_SYNC;
  TEMP_SENSOR_CS_B <= not TEMP_SENSOR_CS;
  TEC_ADC_CS_B     <= not TEC_ADC_CS;
  TEC_DAC_LDAC_B   <= not TEC_DAC_LDAC;
  TECC_ENABLE_B    <= not TECC_ENABLE;
  SER_NUM_CS_b     <= not SER_NUM_CS;

  INSTRUMENT_TRIGGER <= bool2sl(INSTRUMENT_TRIGGER_int);

end struct;
