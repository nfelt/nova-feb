--
-- VHDL Architecture nova_feb.data_regulator.behav
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 17:22:57 10/08/10
--
-- using Mentor Graphics HDL Designer(TM) 2009.1 (Build 12)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std; use harvard_std.lppc_custom_fn_pkg.all;

entity data_regulator is
  port(
    APD_DATA_BUFFER_EMPTY_int : in  boolean;
    APD_DATA_STRB             : in  boolean;
    BEEBUS_ADDR               : in  unsigned (15 downto 0);
    BEEBUS_DATA               : in  unsigned (15 downto 0);
    BEEBUS_READ               : in  boolean;
    BEEBUS_STRB               : in  boolean;
    CLK_16                    : in  std_logic;
    APD_DATA_BUFFER_EMPTY     : out boolean
    );

-- Declarations

end data_regulator;

--
architecture behav of data_regulator is
begin
  
  process (CLK_16)
  begin
    if CLK_16'event and CLK_16 = '1' then
      BEEBUS_READ_del <= false;
      if BEEBUS_ADDR = DATA_REGULATOR_ADDR and BEEBUS_STRB then
        DATA_COUNT <= BEEBUS_DATA;
      else
        if DATA_COUNT /= "00" then
          DATA_COUNT  <= DATA_COUNT - '1';
          ENABLE_DATA <= true;
        end if;
        
      end if;
      
    end if;
  end process;
  APD_DATA_BUFFER_EMPTY <= APD_DATA_BUFFER_EMPTY_int and ENABLE_DATA;
end architecture behav;

