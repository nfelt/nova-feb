--
-- VHDL Architecture nova_feb.dsp_filter.dummi
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 16:10:10 08/23/10
--
-- using Mentor Graphics HDL Designer(TM) 2009.1 (Build 12)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;
use nova_feb.register_address_map.all;

entity dsp_filter is
  port(
    BEEBUS_READ     : in    boolean                        := false;
    BEEBUS_STRB     : in    boolean                        := false;
    BEEBUS_CLK      : in    std_logic;
    BEEBUS_DATA     : inout unsigned (15 downto 0)         := x"0000";
    BEEBUS_ADDR     : in    unsigned (15 downto 0)         := x"0000";
    RAW_CHAN_NUMBER : in    unsigned (4 downto 0)          := "00000";
    RAW_TIMESTAMP   : in    unsigned (31 downto 0)         := x"00000000";
    RAW_MAGNITUDE   : in    unsigned (11 downto 0)         := x"000";
    ALL_APD_DATA    : out   std_logic_vector (48 downto 0) := '0' & x"000000000000";
    DATA_STRB       : out   boolean                        := false;
    CLK             : in    std_logic                      := '1';
    OSCOPE_TRIGGER  : in    boolean                        := false;
    ENABLE_DAQ      : in    boolean                        := false
    );

-- Declarations

end dsp_filter;

--
architecture dummi of dsp_filter is
  signal TEST_MODE_CMD      : unsigned(3 downto 0)  := x"0";
  signal OSCOPE_TRIGGERED   : boolean               := false;
  signal BEEBUS_READ_del    : boolean               := false;
  signal OSCOPE_TRIGGER_del : boolean               := false;
  signal BEEBUS_DATA_int    : unsigned(15 downto 0) := x"0000";
  
begin
  BEEBUS_DATA <= BEEBUS_DATA_int when BEEBUS_READ_del else "ZZZZZZZZZZZZZZZZ";

  process (BEEBUS_CLK)
  begin
    if BEEBUS_CLK'event and BEEBUS_CLK = '1' then
      BEEBUS_READ_del <= false;
      if BEEBUS_ADDR = DAQ_MODE_ADDR then
        if BEEBUS_READ then
          BEEBUS_DATA_int <= x"000" & TEST_MODE_CMD;
          BEEBUS_READ_del <= true;
        elsif BEEBUS_STRB then
          TEST_MODE_CMD <= BEEBUS_DATA(15 downto 12);
        end if;
      end if;
    end if;
  end process;

  decode_mode : process (clk) is
  begin
    if clk'event and clk = '1' then
      DATA_STRB          <= (OSCOPE_TRIGGER and not OSCOPE_TRIGGER_del) and ENABLE_DAQ;
      OSCOPE_TRIGGER_del <= OSCOPE_TRIGGER;
    end if;
  end process decode_mode;

  ALL_APD_DATA <= std_logic_vector(RAW_CHAN_NUMBER)
                  & std_logic_vector(RAW_TIMESTAMP)
                  & std_logic_vector(RAW_MAGNITUDE);

end architecture dummi;


