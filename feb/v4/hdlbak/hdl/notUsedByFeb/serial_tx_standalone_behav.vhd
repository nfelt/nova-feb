--
-- VHDL Architecture nova_feb.serial_tx_standalone.behav
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 11:55:05 05/26/10
--
-- using Mentor Graphics HDL Designer(TM) 2009.1 (Build 12)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

ENTITY serial_tx_standalone IS
   PORT( 
      SPYBUS_ADDR : IN     unsigned (15 DOWNTO 0);
      SPYBUS_DATA : IN     unsigned (31 DOWNTO 0);
      SPYBUS_CLK  : IN     std_logic;
      SPYBUS_STRB : IN     boolean;
      RESET       : IN     boolean;
      CLK_32      : IN     std_logic;
      SERIAL_TX   : OUT    std_logic;
      halfclk     : OUT    std_logic;
      doit        : OUT    std_logic;
      kin_out     : OUT    std_logic;
      data_tx_out : OUT    std_logic_vector (3 DOWNTO 0)
   );

-- Declarations

END serial_tx_standalone ;

--
architecture behav of serial_tx_standalone is
  
  signal DATA_TX              : std_logic_vector(7 downto 0)  := "00000000";
  signal ENCODED_TX           : std_logic_vector(9 downto 0)  := "0000000000";
  signal SHIFT_REG_TX         : std_logic_vector(9 downto 0)  := "0000000000";
  signal BIT_COUNT            : unsigned(3 downto 0)          := x"0";
  signal KIN                  : std_logic                     := '0';
  signal CLK_3_2_int          : std_logic                     := '0';
  signal CLK_3_2_comb         : std_logic                     := '0';
  signal USB_FIFO_EMPTY       : std_logic                     := '0';
  signal USB_FIFO_DIN         : std_logic_vector(15 downto 0) := x"0000";
  signal USB_FIFO_DOUT        : std_logic_vector(7 downto 0)  := x"00";
  signal USB_FIFO_WRITE_EN    : std_logic                     := '0';
  signal USB_FIFO_READ_EN     : std_logic                     := '0';
  signal USB_FIFO_READ_EN_del : std_logic                     := '0';
  signal RESET_int            : std_logic                     := '0';


  component encode_8b10b
    port (
      din  : in  std_logic_vector(7 downto 0);
      kin  : in  std_logic;
      clk  : in  std_logic;
      dout : out std_logic_vector(9 downto 0));
  end component;

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;

  component fifo_16X16_8
    port (
      rst    : in  std_logic;
      wr_clk : in  std_logic;
      rd_clk : in  std_logic;
      din    : in  std_logic_vector(15 downto 0);
      wr_en  : in  std_logic;
      rd_en  : in  std_logic;
      dout   : out std_logic_vector(7 downto 0);
      full   : out std_logic;
      empty  : out std_logic);
  end component;



begin
  DCM_COMM_ENCODE : encode_8b10b

    port map (
      din  => DATA_TX,
      kin  => KIN,
      clk  => CLK_3_2_int,
      dout => ENCODED_TX
      );

  FRAME_BITS : process (CLK_32) is
  begin
    --ADD OPTION HERE TO SKIP BITS AND VERIFY FEB RECOVERS FROM DATA ERROR
    if CLK_32'event and CLK_32 = '1' then
      if BIT_COUNT = 9 then
        BIT_COUNT <= x"0";
      else
        BIT_COUNT <= BIT_COUNT +1;
      end if;
    end if;
  end process FRAME_BITS;

  SER_DES : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '1' then
      SHIFT_REG_TX <= '0' & SHIFT_REG_TX(9 downto 1);

      if BIT_COUNT = 9 then
        SHIFT_REG_TX <= ENCODED_TX;
      end if;
      
    end if;
  end process SER_DES;

  CLOCK_GEN_3_2 : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '1' then
      --PUT RISING EDGE OF 3_2 CLOCK HALFWAY THROUGH DATA FRAME
      CLK_3_2_COMB <= BOOL2SL((BIT_COUNT = 4) or
                              (BIT_COUNT = 5) or
                              (BIT_COUNT = 6));
    end if;
    
  end process CLOCK_GEN_3_2;

  CLK_3_2_BUFG : BUFG
    port map (
      O => CLK_3_2_int,
      I => CLK_3_2_COMB
      );

  RECEIVED_DATA_BUFFER : fifo_16X16_8
    port map (
      rst    => RESET_int,
      wr_clk => SPYBUS_CLK,
      rd_clk => CLK_3_2_int,
      din    => USB_FIFO_DIN,
      wr_en  => USB_FIFO_WRITE_EN,
      rd_en  => USB_FIFO_READ_EN,
      dout   => USB_FIFO_DOUT,
      full   => open,
      empty  => USB_FIFO_EMPTY);

  IN_BUFFER : process (CLK_3_2_int) is
  begin
    if CLK_3_2_int'event and CLK_3_2_int = '1' then
      USB_FIFO_READ_EN_del <= USB_FIFO_READ_EN;
      KIN                  <= not(USB_FIFO_READ_EN and USB_FIFO_READ_EN_del);
      RESET_int         <= BOOL2SL(RESET);
   end if;
  end process IN_BUFFER;

  TX_DATA_MUX : process (KIN, USB_FIFO_DOUT) is
  begin
    if KIN = '0' then
      DATA_TX <= USB_FIFO_DOUT;
    else
      DATA_TX <= x"3C";
    end if;
  end process TX_DATA_MUX;
  USB_FIFO_READ_EN  <= not USB_FIFO_EMPTY;
  USB_FIFO_WRITE_EN <= BOOL2SL((SPYBUS_ADDR = x"E005") and SPYBUS_STRB);
  SERIAL_TX         <= SHIFT_REG_TX(0);
  USB_FIFO_DIN      <= std_logic_vector(SPYBUS_DATA(15 downto 0));
  data_tx_out       <= DATA_tx(7 downto 4);
  kin_out           <= kin;
end architecture behav;






