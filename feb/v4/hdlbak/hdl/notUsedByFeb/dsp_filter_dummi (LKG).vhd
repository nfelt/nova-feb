--
-- VHDL Architecture nova_feb.dsp_filter.dummi
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 16:10:10 08/23/10
--
-- using Mentor Graphics HDL Designer(TM) 2009.1 (Build 12)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;
use nova_feb.register_address_map.all;

entity dsp_filter is
  port(
    BEEBUS_READ      : in    boolean                        := false;
    BEEBUS_STRB      : in    boolean                        := false;
    BEEBUS_CLK       : in    std_logic;
    BEEBUS_DATA      : inout unsigned (15 downto 0)         := x"0000";
    BEEBUS_ADDR      : in    unsigned (15 downto 0)         := x"0000";
    RAW_CHAN_NUMBER  : in    unsigned (4 downto 0)          := "00000";
    RAW_TIMESTAMP    : in    unsigned (31 downto 0)         := x"00000000";
    RAW_MAGNITUDE    : in    unsigned (11 downto 0)         := x"000";
    ALL_APD_DATA     : out   std_logic_vector (48 downto 0) := '0' & x"000000000000";
    DATA_STRB        : out   boolean                        := false;
    CLK              : in    std_logic                      := '1';
    INTERNAL_TRIGGER : in    boolean                        := false;
    ENABLE_DAQ       : in    boolean                        := false;
    RESET            : in    boolean                        := false
    );

-- Declarations

end dsp_filter;

--
architecture dummi of dsp_filter is


  type TABLE_TYPE is array (31 downto 0)
    of unsigned(11 downto 0);
  signal THRESHOLD_TABLE : TABLE_TYPE;

  type DATA_DELAY_TYPE is array (95 downto 0)
    of unsigned(11 downto 0);
  signal DATA_DELAY : DATA_DELAY_TYPE;

  type CHAN_TRIGGERED_TYPE is array (31 downto 0)
    of boolean;
  signal CHAN_TRIGGERED : CHAN_TRIGGERED_TYPE;

  type CHAN_MAGNITUDE_TYPE is array (31 downto 0)
    of signed(12 downto 0);
  signal CHAN_MAGNITUDE : CHAN_MAGNITUDE_TYPE;

  type CHAN_TIMESTAMP_TYPE is array (31 downto 0)
    of unsigned(31 downto 0);
  signal CHAN_TIMESTAMP : CHAN_TIMESTAMP_TYPE;

  signal DAQ_MODE             : unsigned(15 downto 0)          := x"0000";
  signal OSCOPE_TRIGGERED     : boolean                        := false;
  signal BEEBUS_READ_del      : boolean                        := false;
  signal INTERNAL_TRIGGER_del : boolean                        := false;
  signal BEEBUS_DATA_int      : unsigned(15 downto 0)          := x"0000";
  signal TIMESTAMP            : unsigned(31 downto 0)          := x"00000000";
  signal CHAN_NUMBER_DSP      : unsigned (4 downto 0)          := "00000";
  signal MAGNITUDE_DSP        : signed (12 downto 0)           := "0000000000000";
  signal DATA_STRB_DSP        : boolean                        := false;
  signal TIMESTAMP_DSP        : unsigned(31 downto 0)          := x"00000000";
  signal CHAN_NUMBER          : integer range 31 downto 0      := 0;
  signal CHAN_DATA_DCS        : signed (12 downto 0)           := "0000000000000";
  signal DATA_REGULATOR_REG   : unsigned (6 downto 0)          := "0000000";
  signal DATA_REGULATOR_CNT   : unsigned (6 downto 0)          := "0000000";
  signal CHAN_EN              : std_logic_vector (31 downto 0) := x"FFFFFFFF";
  
begin
  BEEBUS_DATA <= BEEBUS_DATA_int when BEEBUS_READ_del else "ZZZZZZZZZZZZZZZZ";

  process (BEEBUS_CLK)
  begin
    if BEEBUS_CLK'event and BEEBUS_CLK = '1' then
      BEEBUS_READ_del <= false;
      if BEEBUS_ADDR = DAQ_MODE_ADDR then
        if BEEBUS_READ then
          BEEBUS_DATA_int <= DAQ_MODE;
          BEEBUS_READ_del <= true;
        elsif BEEBUS_STRB then
          DAQ_MODE <= BEEBUS_DATA;
        end if;
      end if;

      if BEEBUS_ADDR(15 downto 5) = THRESHOLD_ADDR then
        if BEEBUS_READ then
          BEEBUS_READ_del <= true;
          BEEBUS_DATA_int <= x"0" & THRESHOLD_TABLE (to_integer(BEEBUS_ADDR(5 downto 0)));
        elsif BEEBUS_STRB then
          THRESHOLD_TABLE (to_integer(BEEBUS_ADDR(5 downto 0))) <= (BEEBUS_DATA(11 downto 0));
        end if;
      end if;

      if BEEBUS_ADDR(15 downto 0) = DATA_REGULATOR_ADDR then
        if BEEBUS_READ then
          BEEBUS_READ_del <= true;
          BEEBUS_DATA_int <= x"000" & DATA_REGULATOR_REG(6 downto 3);
        elsif BEEBUS_STRB then
          DATA_REGULATOR_REG <= BEEBUS_DATA(3 downto 0) & "000";
        end if;
      end if;

      if BEEBUS_ADDR(15 downto 0) = CHAN_EN_U_ADDR then
        if BEEBUS_READ then
          BEEBUS_READ_del <= true;
          BEEBUS_DATA_int <= unsigned(CHAN_EN(31 downto 16));
        elsif BEEBUS_STRB then
          CHAN_EN(31 downto 16) <= std_logic_vector(BEEBUS_DATA);
        end if;
      end if;

      if BEEBUS_ADDR(15 downto 0) = CHAN_EN_L_ADDR then
        if BEEBUS_READ then
          BEEBUS_READ_del <= true;
          BEEBUS_DATA_int <= unsigned(CHAN_EN(15 downto 0));
        elsif BEEBUS_STRB then
          CHAN_EN(15 downto 0) <= std_logic_vector(BEEBUS_DATA);
        end if;
      end if;
      
    end if;
  end process;

  process (clk) is
    variable i : integer := 0;
  begin
    if clk'event and clk = '1' then
      CHAN_NUMBER   <= to_integer(RAW_CHAN_NUMBER);
      TIMESTAMP     <= RAW_TIMESTAMP;  --DELAY TIMESTAMP TO SYNC WITH OTHER DATA
      DATA_STRB_DSP <= false;
      DATA_DELAY    <= DATA_DELAY(94 downto 0) & RAW_MAGNITUDE;
      -- FIR 32 CHANNELS USING (1 0 0 -1) "DCS FILTER"
      CHAN_DATA_DCS <= signed('0' & RAW_MAGNITUDE) - signed('0' & DATA_DELAY(95));
      if ENABLE_DAQ then
        if (CHAN_DATA_DCS > CHAN_MAGNITUDE(CHAN_NUMBER)) then
          CHAN_TRIGGERED(CHAN_NUMBER) <= true;
          CHAN_TIMESTAMP(CHAN_NUMBER) <= TIMESTAMP;
          CHAN_MAGNITUDE(CHAN_NUMBER) <= CHAN_DATA_DCS;
        elsif (CHAN_DATA_DCS(12) = '1') and CHAN_TRIGGERED(CHAN_NUMBER) then
          CHAN_NUMBER_DSP             <= to_UNSIGNED(CHAN_NUMBER, 5);
          TIMESTAMP_DSP               <= CHAN_TIMESTAMP(CHAN_NUMBER);
          MAGNITUDE_DSP               <= CHAN_MAGNITUDE(CHAN_NUMBER);
          DATA_STRB_DSP               <= true;  --2
          CHAN_MAGNITUDE(CHAN_NUMBER) <= signed('0' & THRESHOLD_TABLE(CHAN_NUMBER));
          CHAN_TRIGGERED(CHAN_NUMBER) <= false;
        end if;
      else
        for i in 0 to 31 loop
          CHAN_MAGNITUDE(i) <= signed('0' & THRESHOLD_TABLE(i));
          CHAN_TRIGGERED(i) <= false;
        end loop;
      end if;
    end if;
  end process;


  data_regulator : process (BEEBUS_CLK)
  begin
    if BEEBUS_CLK'event and BEEBUS_CLK = '1' then
      
      if INTERNAL_TRIGGER and (DAQ_MODE = OSCILLOSCOPE_MODE) then
        DATA_REGULATOR_CNT <= DATA_REGULATOR_REG;
      elsif DATA_REGULATOR_CNT /= "0000000" then
        DATA_REGULATOR_CNT <= DATA_REGULATOR_CNT - 1;
      end if;
      
    end if;
  end process data_regulator;

  decode_mode : process (clk) is
  begin
    if clk'event and clk = '1' then
      INTERNAL_TRIGGER_del <= INTERNAL_TRIGGER;
      if ENABLE_DAQ then
        case DAQ_MODE is
          when DCS_DSP_MODE =>
            ALL_APD_DATA <= std_logic_vector(CHAN_NUMBER_DSP)
                            & std_logic_vector(TIMESTAMP_DSP)
                            & std_logic_vector(MAGNITUDE_DSP(11 downto 0));
            DATA_STRB <= DATA_STRB_DSP and (CHAN_EN(TO_INTEGER(CHAN_NUMBER_DSP)) = '1');

          when OSCILLOSCOPE_MODE =>
            OSCOPE_TRIGGERED <= (INTERNAL_TRIGGER or OSCOPE_TRIGGERED) and DATA_REGULATOR_CNT /= "0000001";
            ALL_APD_DATA     <= std_logic_vector(RAW_CHAN_NUMBER)
                                & std_logic_vector(RAW_TIMESTAMP)
                                & std_logic_vector(RAW_MAGNITUDE);
            DATA_STRB <= OSCOPE_TRIGGERED and (CHAN_EN(TO_INTEGER(RAW_CHAN_NUMBER)) = '1');

          when MEMORY_MODE =>
            null;
          when MEMORY_LOOP_MODE =>
            null;
          when SINGLE_DATA_POINT_MODE =>
            ALL_APD_DATA <= std_logic_vector(RAW_CHAN_NUMBER)
                            & std_logic_vector(RAW_TIMESTAMP)
                            & std_logic_vector(RAW_MAGNITUDE);
            DATA_STRB <= INTERNAL_TRIGGER and not INTERNAL_TRIGGER_del;

          when others => null;
        end case;
      else
        OSCOPE_TRIGGERED <= false;
        DATA_STRB        <= false;
      end if;
    end if;
  end process decode_mode;

end architecture dummi;




