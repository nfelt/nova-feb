--
-- VHDL Architecture nova_feb.data_pipeline.struct
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 14:16:58 06/29/11
--
-- using Mentor Graphics HDL Designer(TM) 2010.2a (Build 7)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity data_pipeline is
  port(
    CLK_64                  : in  std_logic;
    DATA                    : in  unsigned (11 downto 0);
    RAW_MAGNITUDE           : out unsigned (11 downto 0);
    INTERNAL_TRIGGER        : in  boolean;
    EXT_TRIG_IN             : in  std_logic;
    INTERNAL_OR_EXT_TRIG_IN : out boolean := false
    );

-- Declarations

end data_pipeline;

architecture struct of data_pipeline is
  
  type DATA_DELAY_TYPE is array (319 downto 0)
    of unsigned(11 downto 0);
  signal DATA_DELAY      : DATA_DELAY_TYPE;
  signal EXT_TRIG_IN_del : boolean := false;
  
begin
  PIPELINE : process (CLK_64) is
  begin
    if CLK_64'event and CLK_64 = '1' then  -- rising clock edge
      DATA_DELAY    <= DATA_DELAY(318 downto 0) & DATA;
      RAW_MAGNITUDE <= DATA_DELAY(319);
    end if;
  end process PIPELINE;

  LEVEL_TO_PULSE : process (CLK_64) is
  begin  -- process LEVEL_TO_PULSE
    if CLK_64'event and CLK_64 = '1' then  -- rising clock edge
      EXT_TRIG_IN_del <= EXT_TRIG_IN = '1';
      
    end if;
  end process LEVEL_TO_PULSE;

  INTERNAL_OR_EXT_TRIG_IN <= INTERNAL_TRIGGER or (EXT_TRIG_IN = '1' and not EXT_TRIG_IN_del);
end architecture struct;

