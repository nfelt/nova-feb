testData = dlmread('data/FebMDR.csv')
fileListLength = length(testData);
 figure(2);
hbin = min(testData(:,1)):0.0001:max(testData(:,1));
hist(testData(:,1),hbin);
title(['HV Regulator linear fit slope using ',num2str(fileListLength),' FEB4.0 boards']);
ylabel('N');
xlabel('V'); 

figure(3);
hbin = min(testData(:,2)):1:max(testData(:,2));
hist(testData(:,2),hbin);
title(['HV Regulator linear fit intercept using  ',num2str(fileListLength),' FEB4.0 boards']);
ylabel('N');
xlabel('V');