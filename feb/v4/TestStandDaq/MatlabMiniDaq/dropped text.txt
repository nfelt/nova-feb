clear all;
HVFitOK = -1;
OscopeFitOK = -1;
PulseFitOK = -1;
TeccLoopbackFitOK = -1;
TestResult = 'Fail';
CheckHV;
AnalogVoltage = Get2_5VA;
ConfigDaq;
ConfigCheck;
[Status Error FirmwareVersion]= GetStatusErrorVers();
[SerNumbMsg, SerNumOK] = writeserialnumber;
FebSN = SerNumbMsg(1:12);
FebDataFolder= strcat('data\',FebSN(1:4),'_',FebSN(6),'_',FebSN(8:12));
setenv('FebDataFolder', FebDataFolder);
!mkdir %FebDataFolder%;
if SerNumOK == 1
    input('Please turn on the High Voltage. [ok] ', 's');
    [HVData, HVFitPar, HVNormr, HVFitOK] = HVTestAuto(FebDataFolder);
    input('Please turn off the High Voltage. [ok] ', 's');
    CheckHV
    [ApdMaxNoise, ApdMinNoise, OscopeFitOK] = OscopeNoiseCheck(FebDataFolder);
