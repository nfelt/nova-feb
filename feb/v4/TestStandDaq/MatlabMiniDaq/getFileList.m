% read a textfile containing feb names and return them as a char array
function [fileList] = getFileList(filename);
    fid = fopen('good4_0List.txt');
    dataCell = textscan(fid, '%s');
    fileList =char(dataCell{1});
    fclose(fid);
end