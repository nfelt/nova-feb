clear all;

%! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\ResetDcmClearFeb.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\FpgaConfig.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\StartDcmClocks.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\StopClockSyncPkt.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetAsicDefault.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetThresholds.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\StartOscopeReadout.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\PulseSingle.xml data\logfile.txt