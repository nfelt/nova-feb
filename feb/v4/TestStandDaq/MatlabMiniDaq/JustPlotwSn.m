function [] = JustPlot(snIn)
number_of_channels = 32;
Filename = (['data\FEB4_1_00',snIn,'/OscopeData.txt']);
disp(['\data\FEB4_1_00',snIn,'\OscopeData.txt']);
RawData = char(textread(Filename,'%s', 'whitespace', ' -'));

ChanOrder = hex2dec(RawData(1:5:160,3:4));

%Magnitude = Magnitude(4:5:length(RawData));
chan_length = length(RawData)/(number_of_channels *5);
[Tmp SortOrder] = sort(ChanOrder);
Magnitude = hex2dec(RawData(4:5:length(RawData),2:4));
Magnitude = reshape(Magnitude,number_of_channels,chan_length)';
Magnitude = Magnitude(:,SortOrder);

Timestamp = hex2dec (horzcat(RawData(3:5:length(RawData),2:4),RawData(3:5:length(RawData),2:4)));
Timestamp = reshape(Timestamp,number_of_channels,chan_length)';
Timestamp = Timestamp(:,SortOrder);


size(Magnitude)

% % figure(1);
% % for i = 0:15
% % subplot(4,4,i+1)
% % plot(Magnitude(:,i+1));
% % title(['DATAin']);
% % xlabel('X');
% % ylabel('Y');
% % end;
% % 
% 
% 
figure(3);
plot(Magnitude);
title(['DATAin']);
legend('ch0','ch1','ch2','ch3','ch4','ch5','ch6','ch7','ch8','ch9','ch10','ch11','ch12','ch13','ch14','ch15','ch16','ch17','ch18','ch19','ch20','ch21','ch22','ch22','ch23','ch24','ch25','ch26','ch27','ch28','ch29','ch30','ch31');
xlabel('X');
ylabel('Y');
return;