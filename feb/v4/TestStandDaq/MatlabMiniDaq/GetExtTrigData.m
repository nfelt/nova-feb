
%%%%%%%%%%%%%%%%% USER SETTINGS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;
DataFile = 'ExtTrigData_';
DataFileIndex = 1;
RiseTime = 7
FallTime = 3
ShaperVGain = 3;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
RegAddress;
RepeatTest = 1;

ConfigCheck;
[SerNumbMsg, SerNumOK] = ReadSerialNumber;
FebSN = SerNumbMsg(1:12);
WorkFolder = 'TestData\ExtTrig\';
FebDataFolder= strcat(WorkFolder,FebSN(1:4),'_',FebSN(6),'_',FebSN(8:12),'\');
setenv('FebDataFolder', FebDataFolder);
system(['mkdir ', FebDataFolder]);

system (['NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetAsicBasicNoSerial.xml ' ,'data\logfile.txt']);
BWSelAuto = dec2hex(RiseTime,4)
TFSelAuto = dec2hex(FallTime,4)
GSelAuto =  dec2hex(ShaperVGain,4)
system (['NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SendAsicSerial.xml ' ,'data\logfile.txt']);

%! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetChanEnableAll.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetOscopeReadout.xml data\logfile.txt
while RepeatTest
    DataTrig = 0;

    ! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\StartDaq.xml data\logfile.txt
    while not(DataTrig)
      pause(2);
      system (['NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\Readblock.xml ' ,WorkFolder,'TempDataCheck1.txt']);
      m_ini = char(textread([WorkFolder,'TempDataCheck1.txt'],'%s', 'whitespace', ' -'));
      DataTrig = not(strcmp(m_ini(3,:),'error'));
    end
    display(['Trigger ',sprintf('%04d',DataFileIndex)]);
    system (['NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\read_39.xml ' ,WorkFolder,'TempDataCheck2.txt']);
    ! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\ClearBuff.xml data\logfile.txt

    number_of_channels = 32;
    m = char(textread([WorkFolder,'TempDataCheck2.txt'],'%s', 'whitespace', ' -'));
    RawData = cat(1,m_ini,m);  
    ChanOrder = hex2dec(RawData(1:5:160,3:4));
    chan_length = length(RawData)/(number_of_channels *5);
    [Tmp SortOrder] = sort(ChanOrder);
    Magnitude = hex2dec(RawData(4:5:length(RawData),2:4));
    Magnitude = reshape(Magnitude,number_of_channels,chan_length)';
    Magnitude = Magnitude(:,SortOrder);    
    dlmwrite([FebDataFolder,DataFile,sprintf('%04d',DataFileIndex),'.csv'], Magnitude)
    DataFileIndex = DataFileIndex + 1;
    
    figure(3);
    plot(Magnitude);
    title(['DATAin']);
    legend('ch0','ch1','ch2','ch3','ch4','ch5','ch6','ch7','ch8','ch9','ch10','ch11','ch12','ch13','ch14','ch15','ch16','ch17','ch18','ch19','ch20','ch21','ch22','ch22','ch23','ch24','ch25','ch26','ch27','ch28','ch29','ch30','ch31');
    xlabel('X');
    ylabel('Y');
    
end
