function [DriveMon, TempMon] = GetTeccDriveTemp()
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\ReadTeccDriveTemp.xml TempData.txt
DataCharIn = char(textread('TempData.txt','%s', 'whitespace', ' -'));
DataCharIn = DataCharIn(2:2:length(DataCharIn),:);
DriveMon = hex2dec(DataCharIn(1,1:4));
TempMon = hex2dec(DataCharIn(2,1:4));