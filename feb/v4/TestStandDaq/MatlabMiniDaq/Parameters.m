ExpectedFirmwareVersion = '0012';
HVSlopeUBound = .03;
HVSlopeLBound = .02;
HVInterceptUbound = 310;
HVInterceptLbound = 290;
HVNormrU = 4;
AnalogVoltageUBound = 2.53;
AnalogVoltageLBound = 2.45;

ChanStdUBound = 575;
ChanStdLBound = 200;

LoopDataU = [864 65  1183 65 1524 65 2338 65];
LoopDataL = [824 55  1143 55 1484 55 2298 55];
