function [HVDaLinPar, HVNormr, FitCheck] = HVTestAuto(DataFile)

	%! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetChanEnableAll.xml data\logfile.txt
	! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetOscopeReadout.xml data\logfile.txt
    % enable one of the following Three to select the external trigger
	! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\PulseSingleInternal.xml data\logfile.txt
	%! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\PulseSingleInExternal.xml data\logfile.txt 
	%! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\PulseSingleInInstrument.xml data\logfile.txt
	system (['NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\read_40.xml ' ,DataFile]);
	! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\ClearBuff.xml data\logfile.txt
return

