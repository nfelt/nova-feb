%%%%%function writeserial number writes a file called
%%%%%'writeserialnumber.txt' which is the xml file to program the serial
%%%%%number into the feb. it asks for user input for the number. the
%%%%%appended phrase to fill up the extra memory is taken from the file
%%%%%'serialnumberappend.txt' and each feb will have a cycled phrase.
%%%%%written by sam wald, siwald@fas.harvard.edu
function[SerNumMsg, FitCheck] = writeserialnumber()
close all
clear all
RepeatTest = 'y';
FitCheck = 0;
    while RepeatTest == 'y';
        file=fopen('writeserialnumber.xml','w'); %open file to write xml
        filephrases=fopen('serialnumberappend.txt'); %open fil to read append phrases

        i=1;
        while(feof(filephrases)==0) %read and save all append phrases
            appenddata=fgetl(filephrases);
            appenddata=cellstr(appenddata);
            appenddataALL(i)=appenddata;
            i=i+1;
        end
        %display(appenddataALL);
        appendlinestotal=i-1;
        %display(appendlinestotal);


        fileappendlinenum=fopen('curappendlinenum.txt','r');
        curappendlinenum=fgetl(fileappendlinenum);
        curappendlinenum=str2num(curappendlinenum);
        %display(curappendlinenum);
        fclose(fileappendlinenum);



        %GET USER INPUT FOR SERIAL NUMBER INFO
        data = input('Please input serial number data now:  ', 's');
        data=horzcat(data,' ');
        data=horzcat(data,char(appenddataALL(:,curappendlinenum)));

        data=char(data);
        display(data);
        hexdata=dec2hex(data);

        sizedata=size(hexdata);
        lengthdata=sizedata(1)*sizedata(2);

        int=mod(lengthdata,4);
        if(int~=0)
            hexdata(sizedata(1)+1,1)='2';
            hexdata(sizedata(1)+1,2)='0';
        end

        sizedata=size(hexdata);
        lengthdata=sizedata(1)*sizedata(2);
        linesdata=lengthdata/4;

        %display(hexdata);
        %display(lengthdata);
        %display(linesdata);

        %%%%%%%%%%%%PRINT INITIAL NINE LINES

        %BEGIN WRITING XML
        fprintf(file,'<?xml version="1.0"?>');
        fprintf(file,'\r\n'); %new line
        fprintf(file,'<test_commands>');
        fprintf(file,'\r\n'); %new line

        %RESET SPI ADDRESS
        fprintf(file,'<!-- Reset spi address -->');
        fprintf(file,'\r\n'); %new line
        fprintf(file,'<writeblock>');
        fprintf(file,'\r\n'); %new line
        fprintf(file,'E005');
        fprintf(file,'\r\n'); %new line
        fprintf(file,'0020');
        fprintf(file,'\r\n'); %new line
        fprintf(file,'0101');
        fprintf(file,'\r\n'); %new line
        fprintf(file,'0030');
        fprintf(file,'\r\n'); %new line
        fprintf(file,'</writeblock>');
        fprintf(file,'\r\n'); %new line

        %%%%%%%%%%%%%%PRINT THE REST OF THE FILE
        i=1;
        lines=1;
        while(lines<=linesdata)
            %WRITE ENABLE
            fprintf(file,'<!-- WRITE ENABLE -->');
            fprintf(file,'\r\n'); %new line
            fprintf(file,'<writeblock>');
            fprintf(file,'\r\n'); %new line
            fprintf(file,'E005');
            fprintf(file,'\r\n'); %new line
            fprintf(file,'0020');
            fprintf(file,'\r\n'); %new line
            fprintf(file,'0101');
            fprintf(file,'\r\n'); %new line
            fprintf(file,'E030');
            fprintf(file,'\r\n'); %new line
            fprintf(file,'</writeblock>');
            fprintf(file,'\r\n'); %new line
            fprintf(file,'<sleep time="10"/>');
            fprintf(file,'\r\n'); %new line

            %WRITE SERNUM MEM
            fprintf(file,'<!-- WRITE SerNum MEM -->');
            fprintf(file,'\r\n'); %new line
            fprintf(file,'<writeblock>');
            fprintf(file,'\r\n'); %new line
            fprintf(file,'E005');
            fprintf(file,'\r\n'); %new line
            fprintf(file,'0020');
            fprintf(file,'\r\n'); %new line
            fprintf(file,'0030');
            fprintf(file,'\r\n'); %new line
            fprintf(file,hexdata(i,1:2));
            fprintf(file,hexdata(i+1,1:2));
            i=i+2;
            lines=lines+1;
            fprintf(file,'\r\n'); %new line
            fprintf(file,'</writeblock>');
            fprintf(file,'\r\n'); %new line
            fprintf(file,'<sleep time="10"/>');
            fprintf(file,'\r\n'); %new line
        end

        fprintf(file,'</test_commands>');

        %display(curappendlinenum);
        curappendlinenum=curappendlinenum+1;
        %display(curappendlinenum);

        if(curappendlinenum>appendlinestotal)
            curappendlinenum=1;
            %display('RESET APPEND LINE TO 1');
        end
        fileappendlinenum=fopen('curappendlinenum.txt','w');
        fprintf(fileappendlinenum,'%d', curappendlinenum);
        fclose(fileappendlinenum);
        fclose(filephrases);
        fclose(file)

        %write the memory
        ! NovaCmdScripts\novacmdapp -o -e -f writeserialnumber.xml data\logfile.txt

        % read the contents of FEB's FLASH memory.
        ! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SerNumAdrReset.xml data\logfile.txt
        ! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SerNumRead.xml data\SerNumber.txt
        ! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SerNumRead.xml data\SerNumber.txt
        ! NovaCmdScripts\novacmdapp -o -l 63 NovaCmdScripts\SerNumRead.xml data\SerNumber.txt
        DataCharIn = char(textread('data\SerNumber.txt','%s', 'whitespace', ' -'));
        DataCharIn = DataCharIn(2:2:length(DataCharIn),:);
        if strcmp(DataCharIn(1,:),'read ')
            SerNumMsg = data(1:12);
            return;
        end
        AsciiMsg(1:2:length(DataCharIn)*2) = hex2dec(DataCharIn(:,1:2));
        AsciiMsg(2:2:length(DataCharIn)*2) = hex2dec(DataCharIn(:,3:4));
        SerNumMsg = (char(AsciiMsg));

        if SerNumMsg(1:12)== data(1:12);
                    FitCheck = 1;
                    RepeatTest = 'n';
        else  
            RepeatTest = input('The test failed do you want repeat? y/n [y]: ', 's');
            if isempty(RepeatTest)
                RepeatTest = 'y';
            end
        end
        fidsn=fopen('data/SerialNumbData','w');
        fprintf(fidsn,'%s', SerNumMsg);
        fclose(fidsn);

    end
%return
