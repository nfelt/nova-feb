function output=dcmwrite(address,data)
%address=register_address_map_pkg(address);
xmlfile=fopen('c:\data\tempregrw.xml','w');
fprintf(xmlfile,'<?xml version="1.0"?>\n');
fprintf(xmlfile,'<test_commands>\n');
fprintf(xmlfile,'<writeblock>\n');
fprintf(xmlfile,'E005\n');
fprintf(xmlfile,'0020\n');
fprintf(xmlfile,'%s',char(address));
fprintf(xmlfile,'\n');
fprintf(xmlfile,'%s',char(data));
fprintf(xmlfile,'\n');
fprintf(xmlfile,'</writeblock>\n');
fprintf(xmlfile,'</test_commands>');
fclose(xmlfile);

! NovaCmdScripts\novacmdapp -o -e -f c:\data\tempregrw.xml data\logfile.txt

display('Done Writing');
output=1;

return