%%%%%%%%%%%%%%%%% USER SETTINGS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;
%set TeccSetpoint in HEX adccounts
TeccSetpoint = '988';

%set folder where the data goes.
%set starting index of data file
DataFilePfix = 'OscopeDataCh';
DataFileIndex = 186;
DataFileSfix = '.txt';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




RegAddress;
RepeatTest = 'y';

ConfigCheck;
[Status Error FirmwareVersion]= GetStatusErrorVers();
[SerNumbMsg, SerNumOK] = ReadSerialNumber;
FebSN = SerNumbMsg(1:12);
FebDataFolder= strcat('TestData\LeakBias\',FebSN(1:4),'_',FebSN(6),'_',FebSN(8:12),'\',sprintf('%02d',DataFileIndex),'\');
setenv('FebDataFolder', FebDataFolder);
system(['mkdir ', FebDataFolder]);

%%%%%%%%%%%%%%%%%%%%Test 1
% 
% 	ChanSelL = 'FFFF'
% 	ChanSelU = 'FFFF'
% 	dcmwrite(CHAN_EN_L_ADDR,ChanSelL);
% 	dcmwrite(CHAN_EN_U_ADDR,ChanSelU);
% 	GetOscopeData([FebDataFolder,'OscopeData32.txt']); 
%     ShowMeFebData([FebDataFolder,'OscopeData32.txt']);
%%%%%%%%%%%%%%%%%%%%Test 2
 		system (['NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetAsicBasicNoSerial.xml ' ,'data\logfile.txt']);
 		dcmwrite(BWSEL_ADDR,'0003');
 		dcmwrite(TFSEL_ADDR,'0003');
 		system (['NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SendAsicSerial.xml ' ,'data\logfile.txt']);
for i = 1:32
	ConfigCheck;
	ChanSet = '00000000000000000000000000000000';
	ChanSet(i) = '1';

	ChanSelL = dec2hex(bin2dec(ChanSet(17:32)),4)
	ChanSelU = dec2hex(bin2dec(ChanSet(1:16)),4)
	dcmwrite(CHAN_EN_L_ADDR,ChanSelL);
	dcmwrite(CHAN_EN_U_ADDR,ChanSelU);
	GetOscopeData44([FebDataFolder,DataFilePfix,sprintf('%02d',i-1),DataFileSfix]);
end


ShowMeFebDataAuto(FebDataFolder);
%JustPlot(FebDataFolder);

%%%%%%%%%%%%%%%%%%%%Test 3
% ConfigCheck;
% dcmwrite(CHAN_EN_L_ADDR,'0800');

% dcmwrite(CHAN_EN_U_ADDR,'0000');
% MagMapA = zeros(16,8);
% StdMapA = zeros(16,8);
% for RiseTime = 0:15
% 	for FallTime = 0:7
% 		ConfigCheck;
% 		system (['NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetAsicBasicNoSerial.xml ' ,'data\logfile.txt']);
% 		dcmwrite(BWSEL_ADDR,dec2hex(RiseTime,4));
% 		dcmwrite(TFSEL_ADDR,dec2hex(FallTime,4));
% 		system (['NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SendAsicSerial.xml ' ,'data\logfile.txt']);
% 		GetOscopeDataExP([FebDataFolder,'TmpData.txt']);
%   		MagMapA(RiseTime+1,FallTime+1) = GetDcsMagSingle([FebDataFolder,'TmpData.txt']);
% 		GetOscopeData44([FebDataFolder,'TmpData.txt']);
% 		StdMapA(RiseTime+1,FallTime+1)= GetDcsStdSingle([FebDataFolder,'TmpData.txt']);
% 	end
% end
% MagMapAdj = (MagMapA(8,4)./MagMapA)* 44;
% StdMapAdj = StdMapA .* MagMapAdj;
% RiseTimePlot = [35, 80,130, 180, 230, 280, 330, 380, 430, 480, 530, 580, 630, 680, 730, 780];
% FallTimePlot = [19 11.5 8.5 7 6.1 5.4 4.9 4.5];
% figure(100)
% surf(FallTimePlot,RiseTimePlot,MagMapAdj), 
% shading interp
% title('Magnitude of Charge inject vs Rise and Fall Time')
% xlabel('Fall Time (us)')
% ylabel('Rise Time (ns)')
% figure(101)
% surf(FallTimePlot,RiseTimePlot,StdMapAdj), 
% shading interp
% title('Noise using FIR [1 0 0 -1] vs Rise and Fall Time')
% xlabel('Fall Time (us)')
% ylabel('Rise Time (ns)')

