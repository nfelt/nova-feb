function [isok] = ConfigCheck()
Parameters;
Reconfig = 'y';
while Reconfig == 'y'
    ! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\GetStatusErrorVers.xml TempLogfile.txt
    DataCharIn = char(textread('TempLogfile.txt','%s', 'whitespace', ' -'));
    if length(DataCharIn) ~= 6
            Reconfig = input('The DAQ seems to have lost configuration do you want to reconfigure and continue this test? y/n [y]: ', 's');
        if isempty(Reconfig)
            Reconfig = 'y';
        end
        if Reconfig == 'y';
            ConfigDaq;    
        end
    else
        Reconfig = 'n';
        isok = '1';
    end  
end
