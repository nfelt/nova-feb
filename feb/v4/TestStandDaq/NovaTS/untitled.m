clear all
PassList = passedsort;
FileList = PassList(:,1);
StdSum = zeros(1,32);
for i = 1:length(FileList)
	FileName = char(FileList(i));
	FileName = ['data/FEB4_0_' FileName(8:12) '/OscopeData.txt']
	number_of_channels = 32;
	RawData = char(textread(FileName,'%s', 'whitespace', ' -'));
	ChanOrder = hex2dec(RawData(1:5:160,3:4));
	chan_length = length(RawData)/(number_of_channels *5);
	[Tmp SortOrder] = sort(ChanOrder);
	Magnitude = hex2dec(RawData(4:5:length(RawData),2:4));
	Magnitude = reshape(Magnitude,number_of_channels,chan_length)';
	Magnitude = Magnitude(:,SortOrder);

	Timestamp = hex2dec (horzcat(RawData(3:5:length(RawData),2:4),RawData(3:5:length(RawData),2:4)));
	Timestamp = reshape(Timestamp,number_of_channels,chan_length)';
	Timestamp = Timestamp(:,SortOrder);
    StdData = std(Magnitude(:,:))*44;
	StdSum = StdSum + StdData;
end
StdMean = StdSum/length(FileList);
figure(86)

bar(StdMean);