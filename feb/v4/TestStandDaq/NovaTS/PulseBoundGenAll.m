clear all;

Pulse5F30Bound = PulseBoundGen('data/PulseAsic5F30.txt');
Pulse5F37Bound = PulseBoundGen('data/PulseAsic5F37.txt');
Pulse5030Bound = PulseBoundGen('data/PulseAsic5030.txt');
Pulse5037Bound = PulseBoundGen('data/PulseAsic5037.txt');
Pulse5733Bound = PulseBoundGen('data/PulseAsic5733.txt');

save('TestParameters/PulseBounds.mat')