function [AnyFail] = PulseBoundsTest(FebDataFolder)
GetChargeInjectionData;
Bound = open('TestParameters/PulseBounds.mat');	
result(1) = PulsedDataInBounds(strcat(FebDataFolder,'/PulseAsic5037.txt'),Bound.Pulse5037(1,:),Bound.Pulse5037(2,:));
result(2) = PulsedDataInBounds(strcat(FebDataFolder,'/PulseAsic5F30.txt'),Bound.Pulse5F30(1,:),Bound.Pulse5F30(2,:));
result(3) = PulsedDataInBounds(strcat(FebDataFolder,'/PulseAsic5F37.txt'),Bound.Pulse5F37(1,:),Bound.Pulse5037(2,:));
result(4) = PulsedDataInBounds(strcat(FebDataFolder,'/PulseAsic5030.txt'),Bound.Pulse5030(1,:),Bound.Pulse5030(2,:));
result(5) = PulsedDataInBounds(strcat(FebDataFolder,'/PulseAsic5733.txt'),Bound.Pulse5733(1,:),Bound.Pulse5733(2,:));
AnyFail = min(result);
end
