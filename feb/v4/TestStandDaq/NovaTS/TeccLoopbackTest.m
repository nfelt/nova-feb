function [LoopData, FitCheck] = TeccLoopbackTest(FebDataFolder)
    Parameters;
    RepeatTest = 'y';
    FitCheck = 0;
    while RepeatTest == 'y'
    ! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\EnableTecc.xml data\logfile.txt
    ! NovaCmdScripts\novacmdapp -o  -f NovaCmdScripts\SetTeccSetpoint000.xml data\logfile.txt
     ! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\ReadTeccDriveTemp.xml TempData.txt
    ! NovaCmdScripts\novacmdapp -o  -f NovaCmdScripts\SetTeccSetpoint800.xml data\logfile.txt
    ! NovaCmdScripts\novacmdapp -o  -f NovaCmdScripts\ReadTeccDriveTemp.xml TempData.txt
    ! NovaCmdScripts\novacmdapp -o  -f NovaCmdScripts\SetTeccSetpointFFF.xml data\logfile.txt
    ! NovaCmdScripts\novacmdapp -o  -f NovaCmdScripts\ReadTeccDriveTemp.xml TempData.txt
    ! NovaCmdScripts\novacmdapp -o  -f NovaCmdScripts\DisableTecc.xml data\logfile.txt
    ! NovaCmdScripts\novacmdapp -o -f NovaCmdScripts\ReadTeccDriveTemp.xml TempData.txt

        DataCharIn = char(textread('TempData.txt','%s', 'whitespace', ' -'));
        DataCharIn = DataCharIn(2:2:length(DataCharIn),:);
        LoopData = hex2dec(DataCharIn(:,1:4));
        LoopData = LoopData';
        if  min(LoopDataU > LoopData) && min(LoopDataL < LoopData);
            FitCheck = 1;
            RepeatTest = 'n';
        else  
            RepeatTest = input('The test failed do you want repeat? y/n [y]: ', 's');
            if isempty(RepeatTest)
                RepeatTest = 'y';
            end
        end
        dlmwrite(strcat(FebDataFolder,'\TeccLoopbackData.txt'), LoopData);
    end
return