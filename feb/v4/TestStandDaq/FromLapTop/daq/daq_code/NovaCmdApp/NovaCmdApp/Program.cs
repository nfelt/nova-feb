using System;
using System.Collections.Generic;
using System.Text;




    
    class Program
    {

        internal  int loopn=1;
        internal  int writeoutputfile=0;

        static void Main(string[] args)
        {
            if (args.Length < 1) { Console.WriteLine("must specify args - use --help for info"); return; }
            
            foreach(string s in args)
            {
                if (s.CompareTo("--help") == 0 || s.CompareTo("-h") == 0) { showhelp(); return; }
            
               
            }

            Parser p = new Parser();
            p.RunArgList(args);
           

        }

        static void showhelp()
        {
            Console.WriteLine("Program Usage:");
            Console.WriteLine("\n appname   [options]  <infile> <outfile>");
            Console.WriteLine("   -f <infile> <logfile>");
            Console.WriteLine("   -l <Number of iterations> <infile> <logfile>");
            Console.WriteLine("   -e to erase(overwrite) output file if it exists");
            Console.WriteLine("   -b <number> to specifiy number of hex chars per output line (default is 4)");
            Console.WriteLine("          (by default appends to outfile if it exists");
            Console.WriteLine("   -o only writes output from board, packets seperated by -");
            Console.WriteLine("\n     if infile is .xml, file is run as xml");
            Console.WriteLine("     if infile is .tsk, file is a text file with absolute ");
            Console.WriteLine("            paths to xml files, one per line,");
            Console.WriteLine("            which are run in order");
        }

    }
