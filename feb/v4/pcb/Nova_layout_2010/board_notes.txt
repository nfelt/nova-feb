Notes for NOvA FEB 4.0.xx  PCB:

1. This is a 6 layer board with the following layer order

		Layer number		file extension
			1				TOP
			2				POWER (Plane)
			3				INNER1
			4				INNER2
			5				GND (Plane)
			6				BOTTOM			

2. Board dimensions are: 160 x 56 mm

3. Minimum line width is 0.127mm, minimum space is .127mm, minimum pitch is 0.8 mm.

4. There is a top and bottom silk layer.  The silkscreen is white.

6. There is a LPI solder mask on both sides. Soldermask color blue.

7. Board thickness is .062"

6. PCB Material for lead free assembly

9. There are 780 holes, smallest hole is 0.152mm, largest hole is 2.3mm.

10 The holes that are specified as 0.152 and 0.203 are drill size.  Finished hole tolerance +0 / -1.27 mm 

11. Quantity of boards is 200.

12. Boards should be pannelized with a V score.  Pannels should have a 0.75 inch border with a 0.125 inch non plated tooling hole in border. 



For further information or questions, please contact:

Nathan Felt
Harvard University LPPC
18 Hammond St.
Cambridge, MA 02138

Phone: 	617 496 2310
Fax:	 	617 495 2678
e-mail:	felt@fas.harvard.edu
