(DEVICE FILE: CAPNP_0603_SMC0402_001UF - used for device: 'CAP NP_0603_SMC0402_0.01UF')

PACKAGE SMC0402
CLASS IC
PINCOUNT 2

PINORDER 'CAP NP_0603_SMC0402_0.01UF' 1 2
FUNCTION G1 'CAP NP_0603_SMC0402_0.01UF' 1 2

PACKAGEPROP PART_NAME 'CAP NP_0603'
PACKAGEPROP VALUE '0.01uF'

END
