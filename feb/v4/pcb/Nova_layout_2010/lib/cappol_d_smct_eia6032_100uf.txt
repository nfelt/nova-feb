(DEVICE FILE: CAPPOL_D_SMCT_EIA6032_100UF - used for device: 'CAP POL_D_SMCT_EIA6032_100UF')

PACKAGE SMCT_EIA6032
CLASS IC
PINCOUNT 2

PINORDER 'CAP POL_D_SMCT_EIA6032_100UF' 1 2
FUNCTION G1 'CAP POL_D_SMCT_EIA6032_100UF' 1 2

PACKAGEPROP PART_NAME 'CAP POL_D'
PACKAGEPROP VALUE 100uF

END
