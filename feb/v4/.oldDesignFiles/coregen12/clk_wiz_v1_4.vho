-------------------------------------------------------------------------------
-- Copyright (c) 2009 Xilinx, Inc.
-- All Rights Reserved
-------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor     : Xilinx
-- \   \   \/     Version    : 1.0
--  \   \         Application: Xilinx CORE Generator
--  /   /         Filename   : clk_wiz_v1_4.vho
-- /___/   /\
-- \   \  /  \
--  \___\/\___\
--
-- Design Name: ISE Instantiation template
-------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- User entered comments
------------------------------------------------------------------------------
-- None
--
------------------------------------------------------------------------------
-- Output     Output      Phase    Duty Cycle   Pk-to-Pk     Phase
-- Clock     Freq (MHz)  (degrees)    (%)     Jitter (ps)  Error (ps)
------------------------------------------------------------------------------
-- CLK_OUT1    32.000      0.000    50.000      367.210    244.085
-- CLK_OUT2    64.000      0.000    50.000      314.052    244.085
-- CLK_OUT3    16.000      0.000    50.000      425.809    244.085
-- CLK_OUT4    16.000    180.000    50.000      425.809    244.085
-- CLK_OUT5     4.000      0.000    50.000      559.821    244.085
-- CLK_OUT6     6.400      0.000    50.000      511.739    244.085
--
------------------------------------------------------------------------------
-- Input Clock   Input Freq (MHz)   Input Jitter (UI)
------------------------------------------------------------------------------
-- primary          32.000            0.010


-- The following code must appear in the VHDL architecture header:
------------- Begin Cut here for COMPONENT Declaration ------ COMP_TAG
component clk_wiz_v1_4
port
 (-- Clock in ports
  CLK_IN1           : in     std_logic;
  -- Clock out ports
  CLK_OUT1          : out    std_logic;
  CLK_OUT2          : out    std_logic;
  CLK_OUT3          : out    std_logic;
  CLK_OUT4          : out    std_logic;
  CLK_OUT5          : out    std_logic;
  CLK_OUT6          : out    std_logic;
  -- Status and control signals
  LOCKED            : out    std_logic
 );
end component;

-- COMP_TAG_END ------ End COMPONENT Declaration ------------
-- The following code must appear in the VHDL architecture
-- body. Substitute your own instance name and net names.
------------- Begin Cut here for INSTANTIATION Template ----- INST_TAG
your_instance_name : clk_wiz_v1_4
  port map
   (-- Clock in ports
    CLK_IN1            => CLK_IN1,
    -- Clock out ports
    CLK_OUT1           => CLK_OUT1,
    CLK_OUT2           => CLK_OUT2,
    CLK_OUT3           => CLK_OUT3,
    CLK_OUT4           => CLK_OUT4,
    CLK_OUT5           => CLK_OUT5,
    CLK_OUT6           => CLK_OUT6,
    -- Status and control signals
    LOCKED             => LOCKED);
-- INST_TAG_END ------ End INSTANTIATION Template ------------
