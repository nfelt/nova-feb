-------------------------------------------------------------------------------
-- Copyright (c) 1999-2006 Xilinx Inc.  All rights reserved.
-------------------------------------------------------------------------------
-- Title      : Virtual I/O Core Xilinx XST Usage Example
-- Project    : ChipScope
-------------------------------------------------------------------------------
-- File       : vio_xst_example.vhd
-- Company    : Xilinx Inc.
-- Created    : 2002/11/26
-------------------------------------------------------------------------------
-- Description: Example of how to instantiate the VIO core in a VHDL design
--              for use with the Xilinx XST synthesis tool.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity vio_xst_example is
end vio_xst_example;

architecture structure of vio_xst_example is


  -------------------------------------------------------------------
  --
  --  VIO core component declaration
  --
  -------------------------------------------------------------------
  component vio
    port
    (
      control     : in    std_logic_vector(35 downto 0);
      async_in    : in    std_logic_vector(15 downto 0);
      async_out   :   out std_logic_vector(15 downto 0)
    );
  end component;


  -------------------------------------------------------------------
  --
  --  VIO core signal declarations
  --
  -------------------------------------------------------------------
  signal control    : std_logic_vector(35 downto 0);
  signal async_in   : std_logic_vector(15 downto 0);
  signal async_out  : std_logic_vector(15 downto 0);


begin


  -------------------------------------------------------------------
  --
  --  VIO core instance
  --
  -------------------------------------------------------------------
  i_vio : vio
    port map
    (
      control   => control,
      async_in  => async_in,
      async_out => async_out
    );


end structure;

