library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library NOVA_FEB;
use NOVA_FEB.REGISTER_ADDRESS_MAP.all;
--USE NOVA_FEB.CUSTOM_FN_PKG.ALL;

-- PACKAGE DEFINITION
package CUSTOM_FN_PKG is
  function OR_BOOL_REDUCE(ARG : BOOLEAN_VECTOR_TYPE) return boolean;
end CUSTOM_FN_PKG;

package body CUSTOM_FN_PKG is
  function OR_BOOL_REDUCE (ARG : BOOLEAN_VECTOR_TYPE)
    return boolean is
    variable UPPER, LOWER : boolean;
    variable HALF         : integer;
    variable BUS_INT      : BOOLEAN_VECTOR_TYPE (ARG'length - 1 downto 0);
    variable RESULT       : boolean;
  begin
    if (ARG'length < 1) then            -- IN THE CASE OF A NULL RANGE
      RESULT := false;
    else
      BUS_INT := ARG;
      if (BUS_INT'length = 1) then
        RESULT := BUS_INT (BUS_INT'left);
      elsif (BUS_INT'length = 2) then
        RESULT := BUS_INT (BUS_INT'right) or BUS_INT (BUS_INT'left);
      else
        HALF   := (BUS_INT'length + 1) / 2 + BUS_INT'right;
        UPPER  := OR_BOOL_REDUCE (BUS_INT (BUS_INT'left downto HALF));
        LOWER  := OR_BOOL_REDUCE (BUS_INT (HALF - 1 downto BUS_INT'right));
        RESULT := UPPER or LOWER;
      end if;
    end if;
    return RESULT;
  end;
end CUSTOM_FN_PKG;
