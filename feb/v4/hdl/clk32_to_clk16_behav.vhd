--
-- VHDL Architecture nova_feb.clk32_to_clk16.behav
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 15:56:52 11/02/10
--
-- using Mentor Graphics HDL Designer(TM) 2009.1 (Build 12)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity clk32_to_clk16 is
  port(
    CLK_32      : in  std_logic;
    CLK_16      : in  std_logic;
    DCM_SYNC_in : in  std_logic;
    DCM_SYNC_EN : in  boolean;
    DCM_SYNC    : out boolean := false
    );

-- Declarations

end clk32_to_clk16;

--
architecture behav of clk32_to_clk16 is

  signal DCM_SYNC_del : std_logic := '0';
begin
  
  process (CLK_16) is

  begin
    if CLK_16'event and CLK_16 = '0' then
      DCM_SYNC_del <= DCM_SYNC_in;
    end if;
  end process;

  process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '1' then
      DCM_SYNC <= ((DCM_SYNC_in or DCM_SYNC_del) = '1') and DCM_SYNC_EN;
    end if;
  end process;
  
end architecture behav;

