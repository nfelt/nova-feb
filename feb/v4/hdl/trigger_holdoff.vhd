--
-- VHDL Architecture nova_feb.dsp_filter.dummi
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 16:10:10 08/23/10
--
-- using Mentor Graphics HDL Designer(TM) 2009.1 (Build 12)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;
use nova_feb.register_address_map.all;

entity trigger_holdoff is
  generic (
    chan_u : integer;
    chan_l : integer);
  port(

    TRIG_HOLDOFF_TIME : in unsigned(3 downto 0);
    N_DATA_PKT_WORDS  : in unsigned(4 downto 0);

    OSCOPE_STRB     : in boolean       := false;
    DSP_STRB        : in boolean       := false;
    DSP_CHAN_NUMBER : in CHAN_NUM_TYPE := to_unsigned(chan_l, 5);

    TRIG_HOLDOFF : out boolean := false;

    RESET  : in boolean   := false;
    CLK_64 : in std_logic := '1'
    );

-- Declarations

end trigger_holdoff;

--
architecture behav of trigger_holdoff is

  type CHAN_HOLDOFF_TYPE is array (chan_u downto chan_l)
    of unsigned(3 downto 0);
  signal CHAN_HOLDOFF : CHAN_HOLDOFF_TYPE := ((others => (others => '0')));


begin
  
  trigger_qualify : process (CLK_64) is
  begin
    if CLK_64'event and CLK_64 = '1' then
      if CHAN_HOLDOFF(to_integer(DSP_CHAN_NUMBER)) = "0000" then
        if DSP_STRB then
          CHAN_HOLDOFF(to_integer(DSP_CHAN_NUMBER)) <= TRIG_HOLDOFF_TIME;
        elsif OSCOPE_STRB then
          CHAN_HOLDOFF(to_integer(DSP_CHAN_NUMBER)) <= (N_DATA_PKT_WORDS(3 downto 0) -1);
        end if;
      else
        CHAN_HOLDOFF(to_integer(DSP_CHAN_NUMBER)) <= CHAN_HOLDOFF(to_integer(DSP_CHAN_NUMBER)) - 1;
      end if;
    end if;
  end process trigger_qualify;

  -- LOOK AT TIMING AND MAYBE PUT THIS IN ABOVE PROCESS
  TRIG_HOLDOFF <= CHAN_HOLDOFF(to_integer(DSP_CHAN_NUMBER)) /= "000";
  
end architecture behav;






