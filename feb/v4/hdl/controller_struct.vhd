-- VHDL ENTITY NOVA_FEB.CONTROLLER.SYMBOL
--
-- CREATED:
--          BY - NATE.NATE (HEPLPC2)
--          AT - 11:18:50 07/08/11
--
-------------------------------------------------------------------------------
-- HANDLE COMMANDS SENT BY DAQ SOFTWARE
--
-- ASIC SERIAL INTERFACE
--
-- PULSE GENERATOR
--
-- SPI DEVICE INTERFACE
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library UNISIM;
use UNISIM.all;
library HARVARD_STD;
use HARVARD_STD.LPPC_CUSTOM_FN_PKG.all;
library NOVA_FEB;

entity CONTROLLER is
  port(
    BEEBUS_ADDR : in    unsigned (15 downto 0);
    BEEBUS_DATA : inout unsigned (15 downto 0);
    BEEBUS_CLK  : in    std_logic;
    BEEBUS_READ : in    boolean;
    BEEBUS_STRB : in    boolean;

    CURRENT_TIME    : in  unsigned (31 downto 0) := X"00000000";
    ASIC_CHIPRESET  : out boolean;
    ASIC_IBIAS      : out boolean;
    ASIC_SHIFTIN    : out std_logic;
    ASIC_SHIFTOUT   : in  std_logic;
    ASIC_SRCK       : out std_logic;
    ASIC_TESTINJECT : out boolean                := false;

    APD_DATA_BUFFER_FULL : in  boolean;
    DCM_SYNC             : in  boolean;
    DCM_SYNC_EN          : out boolean := true;

    ENABLE_DAQ        : out boolean;
    ENABLE_STATUS_PKT : out boolean := false;
    ENABLE_TM_PKT     : out boolean := false;

    INSTRUMENT_TRIGGER : out boolean := false;
    INTERNAL_TRIGGER   : out boolean := false;

    SPI_DIN  : out std_logic;
    SPI_DOUT : in  std_logic;
    SPI_SCLK : out std_logic;

    TECC_ENABLE       : out boolean := false;
    TEC_ERR_SHDN      : out boolean;
    TEC_DAC_LDAC      : out boolean;
    TEC_DAC_SYNC      : out boolean;
    TEC_ADC_CS        : out boolean;
    TEC_ADC_CH1_B_CH2 : out std_logic;
    TEMP_SENSOR_CS    : out boolean;
    SER_NUM_CS        : out boolean;

    RESET_DAQ : out boolean;
    T_67S     : in  boolean;
    CLK_4     : in  std_logic;
    CLK_16    : in  std_logic;
    CLK_32    : in  std_logic
    );

end CONTROLLER;

architecture STRUCT of CONTROLLER is

  signal ENABLE_DAQ_INT   : boolean;
  signal DAQ_SPI_WSTB     : boolean;
  signal DAQ_SPI_COMMAND  : std_logic_vector(18 downto 0);
  signal SET_ASIC         : boolean;
  signal SPI_PROC_BUSY    : boolean;
  signal UPDATE_TEMP_REG  : boolean;
  signal RESET_DAQ_INT    : boolean;
  signal TEC_ERR_SHDN_INT : boolean;
  signal TEC_ERR_RESET    : boolean;

begin
  ENABLE_DAQ <= ENABLE_DAQ_INT;
  RESET_DAQ  <= RESET_DAQ_INT;

  --ASIC SERIAL STRING READ/WRITE
  ASIC_CTRL_INST : entity NOVA_FEB.ASIC_CTRL
    port map (
      BEEBUS_ADDR => BEEBUS_ADDR,
      BEEBUS_DATA => BEEBUS_DATA,
      BEEBUS_READ => BEEBUS_READ,
      BEEBUS_STRB => BEEBUS_STRB,

      SET_ASIC       => SET_ASIC,
      ASIC_CHIPRESET => ASIC_CHIPRESET,
      ASIC_SHIFTIN   => ASIC_SHIFTIN,
      ASIC_SHIFTOUT  => ASIC_SHIFTOUT,
      ASIC_SRCK      => ASIC_SRCK,
      ASIC_IBIAS     => ASIC_IBIAS,
      ASIC_SET_ERROR => open,

      CLK => CLK_16
      );

  --PULSE GENERATOR
  ASIC_PULSE_INJECT_INST : entity NOVA_FEB.ASIC_PULSE_INJECT
    generic map (
      EXT_PULSE_WIDTH => X"0800"
      )
    port map (
      BEEBUS_ADDR => BEEBUS_ADDR,
      BEEBUS_DATA => BEEBUS_DATA,
      BEEBUS_READ => BEEBUS_READ,
      BEEBUS_STRB => BEEBUS_STRB,

      ENABLE_DAQ => ENABLE_DAQ_INT,

      INTERNAL_TRIGGER   => INTERNAL_TRIGGER,
      ASIC_TESTINJECT    => ASIC_TESTINJECT,
      INSTRUMENT_TRIGGER => INSTRUMENT_TRIGGER,
      CLK                => CLK_16
      );

  -- INTERP COMMANDS SENT BY DAQ SOFTWARE
  COMMAND_INTERP_INST : entity NOVA_FEB.COMMAND_INTERP
    port map (
      BEEBUS_ADDR => BEEBUS_ADDR,
      BEEBUS_DATA => BEEBUS_DATA,
      BEEBUS_READ => BEEBUS_READ,
      BEEBUS_STRB => BEEBUS_STRB,

      APD_DATA_BUFFER_FULL => APD_DATA_BUFFER_FULL,
      DCM_SYNC             => DCM_SYNC,
      DCM_SYNC_EN          => DCM_SYNC_EN,
      ENABLE_DAQ           => ENABLE_DAQ_INT,
      UPDATE_TEMP_REG      => UPDATE_TEMP_REG,
      SET_ASIC             => SET_ASIC,

      DAQ_SPI_COMMAND => DAQ_SPI_COMMAND,
      DAQ_SPI_WSTB    => DAQ_SPI_WSTB,

      ENABLE_TM_PKT     => ENABLE_TM_PKT,
      ENABLE_STATUS_PKT => ENABLE_STATUS_PKT,
      TECC_ENABLE       => TECC_ENABLE,
      TEC_ERR_SHDN      => TEC_ERR_SHDN_INT,
      TEC_ERR_RESET     => TEC_ERR_RESET,

      RESET_FEB => RESET_DAQ_INT,
      CLK_16    => CLK_16,
      CLK_32    => CLK_32
      );

  --SPI DEVICE INTERFACE
  SPI_INTERFACE_INST : entity NOVA_FEB.SPI_INTERFACE
    port map (
      BEEBUS_ADDR => BEEBUS_ADDR,
      BEEBUS_DATA => BEEBUS_DATA,
      BEEBUS_READ => BEEBUS_READ,
      BEEBUS_STRB => BEEBUS_STRB,

      UPDATE_TEMP_REG => UPDATE_TEMP_REG,
      CURRENT_TIME    => CURRENT_TIME,

      DAQ_SPI_COMMAND => DAQ_SPI_COMMAND,
      DAQ_SPI_WSTB    => DAQ_SPI_WSTB,
      SPI_PROC_BUSY   => SPI_PROC_BUSY,

      SPI_DIN  => SPI_DIN,
      SPI_DOUT => SPI_DOUT,
      SPI_SCLK => SPI_SCLK,

      TEC_DAC_LDAC      => TEC_DAC_LDAC,
      TEC_DAC_SYNC      => TEC_DAC_SYNC,
      TEC_ADC_CS        => TEC_ADC_CS,
      TEC_ADC_CH1_B_CH2 => TEC_ADC_CH1_B_CH2,
      TEMP_SENSOR_CS    => TEMP_SENSOR_CS,
      SER_NUM_CS        => SER_NUM_CS,

      TEC_ERR_SHDN  => TEC_ERR_SHDN_INT,
      TEC_ERR_RESET => TEC_ERR_RESET,
      RESET         => RESET_DAQ_INT,
      T_67S         => T_67S,
      CLK_4         => CLK_4,
      CLK_16        => CLK_16
      );
  TEC_ERR_SHDN <= TEC_ERR_SHDN_INT;
end STRUCT;
