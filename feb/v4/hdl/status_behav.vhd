--
-- VHDL ARCHITECTURE NOVA_FEB.STATUS.BEHAV
--
-- CREATED:
--          BY - NATE.NATE (HEPLPC2)
--          AT - 10:35:55 08/17/10
--
-- USING MENTOR GRAPHICS HDL DESIGNER(TM) 2009.1 (BUILD 12)
-------------------------------------------------------------------------------
-- STATUS AND ERROR REPORTING
--
--STATUS REGISTER IS A SNAPSHOT OF THE CURRENT STATE OF THE FEB
--
--ERROR REGISTER BITS ARE STICKY UNTIL THE REGISTER IS READ.
--
--ALL STATUS AND ERROR BITS WILL APPEAR IN THE FEB STATUS PACKET
--
--A SUBSET OF THE BITS ARE REPORTED IN THE TIMING PACKET
--
--ERROR BITS WILL CONTINUE TO BE REPORTED UNTIL THE DAQ SOFTWARE READS THE ERROR REGISTER
--
--TECC ENABLE STATUS IS AS SENT TO TECC. I.E. GATED WITH TECC ERROR SHUTDOWN SIGNAL
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library UNISIM;
use UNISIM.all;
library HARVARD_STD;
use HARVARD_STD.LPPC_CUSTOM_FN_PKG.all;
library NOVA_FEB;
use NOVA_FEB.REGISTER_ADDRESS_MAP.all;

entity STATUS is
  port(
    BEEBUS_ADDR : in    unsigned (15 downto 0);
    BEEBUS_DATA : inout unsigned (15 downto 0);
    BEEBUS_READ : in    boolean;
    BEEBUS_STRB : in    boolean;

    --THESE END UP IN THE STATUS REGISTER LS 3 ALSO IN TIMING PKT
    RX_ERROR        : in boolean;
    TX_ERROR        : in boolean;
    READ_REG_ERROR  : in boolean;
    THEAD_OVFLW_ERR : in boolean;
    TDATA_OVFLW_ERR : in boolean;
    EVENT_OVFLW_ERR : in boolean;
    DATA_DROP_ERR   : in boolean;
    TEC_ERR_SHDN    : in boolean;
    ADC_ERROR       : in boolean;
    COMM_ERROR      : in boolean;

    --THESE END UP IN THE ERROR REGISTER LS 4 ALSO IN TIMING PKT
    DCM_SYNC_EN       : in boolean;
    ENABLE_STATUS_PKT : in boolean;
    ENABLE_TM_PKT     : in boolean;
    EVENT_FIFO_EMPTY  : in boolean;
    EVENT_FIFO_FULL   : in boolean;
    TECC_ENABLE       : in boolean;
    ENABLE_DAQ        : in boolean;

    TIMING_PKT_FEB_STATUS : out std_logic_vector (7 downto 0);
    STATUS_PKT_FEB_STATUS : out std_logic_vector (31 downto 0);

    POWERUP_RESET : in boolean;
    CLK_16        : in std_logic;
    CLK_128       : in std_logic
    );


end STATUS;

architecture BEHAV of STATUS is

  signal ISERROR         : boolean                       := false;
  signal CLEAR_ERRORS    : boolean                       := false;
  signal BEEBUS_READ_DEL : boolean                       := false;
  signal BEEBUS_DATA_INT : unsigned(15 downto 0)         := X"0000";
  signal STATUS_REG      : std_logic_vector(15 downto 0) := X"0000";
  signal ERROR_FLAGS_REG : std_logic_vector(15 downto 0) := X"0000";
  
begin
  BEEBUS_DATA <= BEEBUS_DATA_INT when BEEBUS_READ_DEL else "ZZZZZZZZZZZZZZZZ";
  LATCH_ERRORS : process (CLK_128) is
  begin
    if CLK_128'event and CLK_128 = '1' then
      --ERROR REGISTER IS STICKEY
      if CLEAR_ERRORS then
        ERROR_FLAGS_REG <= X"0000";
      else
        ERROR_FLAGS_REG <= ERROR_FLAGS_REG or ("000000"
                                               & BOOL2SL(RX_ERROR)
                                               & BOOL2SL(TX_ERROR)
                                               & BOOL2SL(READ_REG_ERROR)
                                               & BOOL2SL(THEAD_OVFLW_ERR)
                                               & BOOL2SL(TDATA_OVFLW_ERR)
                                               & BOOL2SL(EVENT_OVFLW_ERR)
                                               & BOOL2SL(DATA_DROP_ERR)
                                               & BOOL2SL(TEC_ERR_SHDN)
                                               & BOOL2SL(ADC_ERROR)
                                               & BOOL2SL(COMM_ERROR));
      end if;
    end if;
  end process LATCH_ERRORS;

  GET_STATUS : process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '1' then
      BEEBUS_READ_DEL <= false;
      CLEAR_ERRORS    <= false;
      case BEEBUS_ADDR is
        when FIRMWARE_VERS_ADDR =>
          if BEEBUS_READ then
            BEEBUS_DATA_INT <= FIRMWARE_VERS;
            BEEBUS_READ_DEL <= true;
          end if;
        when STATUS_ADDR =>
          if BEEBUS_READ then
            BEEBUS_DATA_INT <= unsigned(STATUS_REG);
            BEEBUS_READ_DEL <= true;
          end if;
        when ERROR_FLAG_ADDR =>
          if BEEBUS_READ then
            BEEBUS_DATA_INT <= unsigned(ERROR_FLAGS_REG);
            CLEAR_ERRORS    <= true;
            BEEBUS_READ_DEL <= true;
          end if;
        when others => null;
      end case;

    end if;
  end process GET_STATUS;
  --CHANGE THE FOLLOWING TO OR REDUCE WITH SIM UPGRADE.
  --ISERROR    <= OR(ERROR_FLAGS_REG) = '1';
  ISERROR <= (ERROR_FLAGS_REG(15)
              or ERROR_FLAGS_REG(14)
              or ERROR_FLAGS_REG(13)
              or ERROR_FLAGS_REG(12)
              or ERROR_FLAGS_REG(11)
              or ERROR_FLAGS_REG(10)
              or ERROR_FLAGS_REG(9)
              or ERROR_FLAGS_REG(8)
              or ERROR_FLAGS_REG(7)
              or ERROR_FLAGS_REG(6)
              or ERROR_FLAGS_REG(5)
              or ERROR_FLAGS_REG(4)
              or ERROR_FLAGS_REG(3)
              or ERROR_FLAGS_REG(2)
              or ERROR_FLAGS_REG(1)
              or ERROR_FLAGS_REG(0)) = '1';

  STATUS_REG <= BOOL2SL(ISERROR) & "00000000"
                & BOOL2SL(DCM_SYNC_EN)
                & BOOL2SL(ENABLE_STATUS_PKT)
                & BOOL2SL(ENABLE_TM_PKT)
                & BOOL2SL(EVENT_FIFO_EMPTY)
                & BOOL2SL(EVENT_FIFO_FULL)
                & BOOL2SL(TECC_ENABLE)
                & BOOL2SL(ENABLE_DAQ);
  TIMING_PKT_FEB_STATUS <= STATUS_REG(15) & STATUS_REG(2 downto 0) & ERROR_FLAGS_REG(3 downto 0);

  STATUS_PKT_FEB_STATUS <= STATUS_REG & ERROR_FLAGS_REG;
end architecture BEHAV;


