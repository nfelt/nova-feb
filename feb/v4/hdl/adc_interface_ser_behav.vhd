--
-- VHDL Architecture feb_p2_lib.adc_interface.behav
--
-- Created:
--          BY - NATE.UNKNOWN (HEPLPC2)
--          AT - 11:43:21 01/22/2007
--
-- USING MENTOR GRAPHICS HDL DESIGNER(TM) 2006.1 (BUILD 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library UNISIM;
use UNISIM.all;
library HARVARD_STD;
use HARVARD_STD.LPPC_CUSTOM_FN_PKG.all;
library NOVA_FEB;
use NOVA_FEB.REGISTER_ADDRESS_MAP.all;

entity ADC_INTERFACE is
  port(
    BEEBUS_ADDR : in    unsigned (15 downto 0);
    BEEBUS_DATA : inout unsigned (15 downto 0);
    BEEBUS_READ : in    boolean;
    BEEBUS_STRB : in    boolean;

    ADC_DOUT_P : in std_logic_vector (15 downto 0);
    ADC_DOUT_N : in std_logic_vector (15 downto 0);

    ADC_CLKOUT0_P : in std_logic;
    ADC_CLKOUT0_N : in std_logic;
    ADC_FRAME0_P  : in std_logic;
    ADC_FRAME0_N  : in std_logic;

    ADC_CLKOUT1_P : in std_logic;
    ADC_CLKOUT1_N : in std_logic;
    ADC_FRAME1_P  : in std_logic;
    ADC_FRAME1_N  : in std_logic;

    MAGNITUDE    : out MAGNITUDE_VECTOR_TYPE(3 downto 0);
    CHAN_NUMBER  : out CHAN_NUM_TYPE;
    CURRENT_TIME : out TIMESTAMP_TYPE;

    RESET_CHAN_PTR : in boolean;
    ENABLE_DAQ     : in boolean;
    ENABLE_TIME    : in boolean;

    DCM_SYNC      : in  boolean;
    TICK_1US      : out boolean;
    CLK_16        : in  std_logic;
    CLK_32        : in  std_logic;
    CLK_64        : in  std_logic;
    USE_CLK16_INV : out boolean
    );

-- DECLARATIONS

end ADC_INTERFACE;

--
architecture BEHAV of ADC_INTERFACE is
  
  signal DATA_FRAME0_P : std_logic_vector (8 downto 0);
  signal DATA_FRAME0_N : std_logic_vector (8 downto 0);
  signal DATA_FRAME1_P : std_logic_vector (8 downto 0);
  signal DATA_FRAME1_N : std_logic_vector (8 downto 0);

  signal FAKE_DATA            : unsigned (11 downto 0) := X"000";
  signal ADC_CHAN_PTR         : unsigned (1 downto 0)  := "00";
  signal ASIC_CHAN_PTR        : unsigned (1 downto 0)  := "00";
  signal DATA_CHAN_A_RISE     : unsigned (11 downto 0) := X"000";
  signal DATA_CHAN_B_RISE     : unsigned (11 downto 0) := X"000";
  signal DATA_CHAN_A_FALL     : unsigned (11 downto 0) := X"000";
  signal DATA_CHAN_B_FALL     : unsigned (11 downto 0) := X"000";
  signal BEEBUS_DATA_INT      : unsigned (15 downto 0) := X"0000";
  signal PRESET_TIME          : unsigned (29 downto 0) := "00" & X"0000000";
  signal CURRENT_TIME_INT     : unsigned (30 downto 0) := "000" & X"0000000";
  signal BEEBUS_READ_DEL      : boolean                := false;
  signal CURRENT_TIME_INT_DEL : std_logic              := '0';
  signal USE_CLK16_INV_INT    : boolean                := false;
  signal SYNC                 : boolean                := false;
  signal ADC_CHAN_DATA0       : ADC_CHAN_DATA_TYPE;
  signal ADC_CHAN_DATA1       : ADC_CHAN_DATA_TYPE;
begin

  BEEBUS_DATA <= BEEBUS_DATA_INT when BEEBUS_READ_DEL else "ZZZZZZZZZZZZZZZZ";

  SET_ADC_IN_REGS : process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '1' then
      BEEBUS_READ_DEL <= false;

      case BEEBUS_ADDR is
        when PRESET_TIME_L_ADDR =>
          if BEEBUS_STRB then
            PRESET_TIME(13 downto 0) <= BEEBUS_DATA(15 downto 2);
          end if;
          if BEEBUS_READ then
            BEEBUS_READ_DEL <= true;
            BEEBUS_DATA_INT <= PRESET_TIME(13 downto 0) & "00";
          end if;
        when PRESET_TIME_U_ADDR =>
          if BEEBUS_STRB then
            PRESET_TIME(29 downto 14) <= BEEBUS_DATA;
          end if;
          if BEEBUS_READ then
            BEEBUS_READ_DEL <= true;
            BEEBUS_DATA_INT <= PRESET_TIME(29 downto 14);
          end if;
--temp reg for nd FEB testing

        when others => null;
      end case;
    end if;
  end process SET_ADC_IN_REGS;
  DATA_FRAME0_P <= ADC_FRAME0_P & ADC_DOUT_P(7 downto 0);
  DATA_FRAME0_N <= ADC_FRAME0_N & ADC_DOUT_N(7 downto 0);
  DATA_FRAME1_P <= ADC_FRAME1_P & ADC_DOUT_P(15 downto 8);
  DATA_FRAME1_N <= ADC_FRAME1_N & ADC_DOUT_N(15 downto 8);

  DIFF_RX0 : entity NOVA_FEB.GBT_SER_RX
    port map (
      DATAIN_P      => DATA_FRAME0_P,
      DATAIN_N      => DATA_FRAME0_N,
      CLKIN_P       => ADC_CLKOUT0_P,
      CLKIN_N       => ADC_CLKOUT0_N,
      ADC_CHAN_DATA => ADC_CHAN_DATA0,
      RESET         => bool2sl(RESET_CHAN_PTR),
      CLK_16        => CLK_16
      );

  DIFF_RX1 : entity NOVA_FEB.GBT_SER_RX
    port map (
      DATAIN_P      => DATA_FRAME1_P,
      DATAIN_N      => DATA_FRAME1_N,
      CLKIN_P       => ADC_CLKOUT1_P,
      CLKIN_N       => ADC_CLKOUT1_N,
      ADC_CHAN_DATA => ADC_CHAN_DATA1,
      RESET         => bool2sl(RESET_CHAN_PTR),
      CLK_16        => CLK_16
      );

  CURRENT_TIME_CNT : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '1' then  -- RISING CLOCK EDGE
      if DCM_SYNC then
        CURRENT_TIME_INT <= PRESET_TIME & '0';
      elsif ENABLE_TIME then
        CURRENT_TIME_INT <= CURRENT_TIME_INT + 1;
      end if;
    end if;
  end process;

  GEN_1US_TICK : process (CLK_64) is
  begin
    if CLK_64'event and CLK_64 = '1' then  -- RISING CLOCK EDGE
      CURRENT_TIME_INT_DEL <= CURRENT_TIME_INT(5);
    end if;
  end process GEN_1US_TICK;
  TICK_1US <= (CURRENT_TIME_INT_DEL /= CURRENT_TIME_INT(5));

  --DETECT PHASE OF CLK 16 ADJUST AND TRACK ASIC MUX
  ASIC_CHCNT : process (CLK_32) is
  begin
    if RESET_CHAN_PTR then
      if USE_CLK16_INV_INT then
        ASIC_CHAN_PTR <= "00";
      else
        ASIC_CHAN_PTR <= "01";
      end if;
    elsif CLK_32'event and CLK_32 = '1' then
      if DCM_SYNC and (ASIC_CHAN_PTR(0) = '1') then
        USE_CLK16_INV_INT <= not USE_CLK16_INV_INT;
      else
        ASIC_CHAN_PTR <= ASIC_CHAN_PTR + 1;
      end if;
    end if;
  end process ASIC_CHCNT;

  USE_CLK16_INV <= USE_CLK16_INV_INT;

  ADC_PIPELINE : process (CLK_64) is
  begin
    if CLK_64'event and CLK_64 = '1' then
      if RESET_CHAN_PTR then
        ADC_CHAN_PTR <= "00";
        FAKE_DATA    <= X"000";
      else
        FAKE_DATA    <= FAKE_DATA+1;
        ADC_CHAN_PTR <= ADC_CHAN_PTR+1;
      end if;
      --CHAN DATA TIME  MAY NOT NEED THIS PIPELINE STAGE
--      case sel_seg is
-- when "00" =>
--          MAGNITUDE(0) <= ADC_CHAN_DATA0(to_integer('0' &
--                                                    ADC_CHAN_PTR));
-- when "01" =>
--          MAGNITUDE(0) <= ADC_CHAN_DATA0(to_integer('1' &
--                                                    ADC_CHAN_PTR));
-- when "10" =>
--          MAGNITUDE(0) <= ADC_CHAN_DATA1(to_integer('0' &
--                                                    ADC_CHAN_PTR));
-- when "11" =>
--          MAGNITUDE(0) <= ADC_CHAN_DATA1(to_integer('1' &
--                                                    ADC_CHAN_PTR));
-- when others => null;
--      end case;
      MAGNITUDE(0) <= ADC_CHAN_DATA0(to_integer('0' &
                                                ADC_CHAN_PTR));
      MAGNITUDE(1) <= ADC_CHAN_DATA0(to_integer('1' &
                                                ADC_CHAN_PTR));
      MAGNITUDE(2) <= ADC_CHAN_DATA1(to_integer('0' &
                                                ADC_CHAN_PTR));
      MAGNITUDE(3) <= ADC_CHAN_DATA1(to_integer('1' &
                                                ADC_CHAN_PTR));
      -- UNTANGLE ASIC MUX ADC OPERATION.
      --DATA <= "010100000" &
--              ADC_CHAN_PTR &
--              ASIC_CHAN_PTR(1);
      CHAN_NUMBER <= "00"
                     & ADC_CHAN_PTR
                     & ASIC_CHAN_PTR(1);


      CURRENT_TIME <= CURRENT_TIME_INT(30 downto 1) & "00";
    end if;
  end process;

end architecture BEHAV;

