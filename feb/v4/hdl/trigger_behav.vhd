--
-- VHDL Architecture nova_feb.dsp_filter.dummi
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 16:10:10 08/23/10
--
-- using Mentor Graphics HDL Designer(TM) 2009.1 (Build 12)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;
use nova_feb.register_address_map.all;

entity trigger is
  generic (

    chan_l : integer);
  port(

    RAW_CHAN_NUMBER : in CHAN_NUM_TYPE;
    RAW_TIMESTAMP   : in unsigned (31 downto 0) := x"00000000";
    RAW_MAGNITUDE   : in MAGNITUDE_TYPE;

    THRESHOLD_TABLE_SEG : in MAGNITUDE_VECTOR_TYPE;
    DSP_EN              : in boolean := false;

    DATA_PKT_ENCODE :     unsigned(3 downto 0);
    DSP_CHAN_NUMBER : out CHAN_NUM_TYPE          := to_unsigned(chan_l, 5);
    DSP_TIMESTAMP   : out unsigned (31 downto 0) := x"00000000";
    DSP_MAGNITUDE   : out MAGNITUDE_TYPE         := x"000";
    DSP_STRB        : out boolean                := false;

    RESET  : in boolean   := false;
    CLK_64 : in std_logic := '1'
    );

-- Declarations

end trigger;

--
architecture behav of trigger is
  constant nsegment : integer := 8;
  type DATA_DELAY_TYPE is array (96 downto 0)
    of unsigned(11 downto 0);
  signal DATA_DELAY : DATA_DELAY_TYPE := (others=> (others=>'0'));
  
  type CHAN_DATA_DCS_TYPE is array (NCHPSEG downto 0)
    of signed(12 downto 0);
  signal CHAN_DATA_DCS : CHAN_DATA_DCS_TYPE := (others=> (others=>'0'));

  signal CHAN_NUMBER   : unsigned(4 downto 0)  := to_unsigned(THRESHOLD_TABLE_SEG'right, 5);
--  signal CHAN_NUMBER   : unsigned(4 downto 0) := to_unsigned(chan_l,5);
--  signal CHAN_DATA_DCS : signed (12 downto 0)  := "0000000000000";
  signal TIMESTAMP     : unsigned(31 downto 0) := x"00000000";

begin

  DCS_FILTER : process (CLK_64) is
  begin
    if CLK_64'event and CLK_64 = '1' then
      CHAN_NUMBER   <= RAW_CHAN_NUMBER;
      TIMESTAMP     <= RAW_TIMESTAMP;  --DELAY TIMESTAMP TO SYNC WITH OTHER DATA
      DATA_DELAY    <= DATA_DELAY(95 downto 0) & RAW_MAGNITUDE;
      -- FIR 32 CHANNELS USING (1 0 0 -1) "DCS FILTER"
      CHAN_DATA_DCS <= CHAN_DATA_DCS(31 downto 0) & (signed('0' & RAW_MAGNITUDE) - signed('0' & DATA_DELAY(95)));
    end if;
    
  end process DCS_FILTER;
  -- previously out of process
  -- LOOK AT TIMING AND MAYBE PUT THIS IN ABOVE PROCESS
  select_data : process (CLK_64) is
  begin
    if CLK_64'event and CLK_64 = '1' then
      case DATA_PKT_ENCODE is
        when DCS_N0=>
          DSP_MAGNITUDE <= unsigned(CHAN_DATA_DCS(32)(11 downto 0));
        when RAW_N1 =>
          DSP_MAGNITUDE <= DATA_DELAY(32);
        when RAW_N2 =>
          DSP_MAGNITUDE <= DATA_DELAY(64);
        when others =>
          DSP_MAGNITUDE <= DATA_DELAY(32);
      end case;

      DSP_STRB <= (CHAN_DATA_DCS(NCHPSEG) > signed('0' & THRESHOLD_TABLE_SEG(to_integer(CHAN_NUMBER)))
                   and CHAN_DATA_DCS(NCHPSEG) > CHAN_DATA_DCS(0)
                   and DSP_EN);
      DSP_CHAN_NUMBER <= CHAN_NUMBER;
      DSP_TIMESTAMP   <= TIMESTAMP;
    end if;
  end process select_data;
  
end architecture behav;






