--
-- VHDL Architecture nova_feb.temp_reg.behav
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 09:40:08 04/14/09
--
-- using Mentor Graphics HDL Designer(TM) 2008.1a (Build 9)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity temp_reg is
-- Declarations

end temp_reg;

--
architecture behav of temp_reg is
begin
  RECEIVE_COMMAND : process (BEEBUS_CLK) is
  begin
    if BEEBUS_CLK'event and BEEBUS_CLK = '1' then
      BEEBUS_DATA  <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
      DAQ_INTERUPT <= DAQ_INTERUPT and not GOT_THAT_COMMAND;
      if (BEEBUS_ADDR = x"0009" and BEEBUS_READ)then
        if TEMP_CALC_BUSY then
          BEEBUS_DATA <= x"80000000"
        else
          BEEBUS_DATA <= FEB_TEMPERATURE;          
        end if;
      end if;
    end if;    
  end process RECEIVE_COMMAND;
end architecture behav;

