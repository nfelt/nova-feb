--
-- VHDL Architecture nova_feb.dcm_com_beebus_interface.behav
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 11:58:48 07/28/09
--
-- using Mentor Graphics HDL Designer(TM) 2008.1a (Build 9)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity dcm_com_beebus_interface is
  port(
    DCMBUS_ADDR : out   unsigned (15 downto 0);
    DCMBUS_DATA : inout unsigned (15 downto 0);
    DCMBUS_READ : out   boolean;
    DCMBUS_STRB : out   boolean;

    DATA_RX      : in  unsigned (7 downto 0);
    DATA_RX_STRB : in  boolean;
    DATA_TX      : out unsigned (7 downto 0);
    DATA_TX_STRB : out boolean;

    EVENT_FIFO_DOUT  : in  unsigned (7 downto 0) := x"00";
    EVENT_FIFO_EMPTY : in  boolean               := false;
    EVENT_FIFO_RSTRB : out boolean               := false;
    N_DATA_PKT_WORDS : in  unsigned(4 downto 0);

    CURRENT_TIME          : in unsigned (31 downto 0);
    ENABLE_STATUS_PKT     : in boolean;
    TIMING_PKT_FEB_STATUS : in std_logic_vector (7 downto 0);
    STATUS_PKT_FEB_STATUS : in std_logic_vector (31 downto 0);

    RX_Error       : out boolean;
    TX_Error       : out boolean;
    READ_REG_ERROR : out boolean;

    RESET   : in boolean;
    CLK_3_2 : in std_logic;
    CLK_16  : in std_logic;
    CLK_32  : in std_logic
    );

-- Declarations

end dcm_com_beebus_interface;

architecture behav of dcm_com_beebus_interface is

  type COMMAND_TYPE is (
    IDLE,
    NULL_CMD,
    WRITE_REG,
    READ_REG
    );

  attribute ENUM_ENCODING                 : string;
  attribute ENUM_ENCODING of COMMAND_TYPE : type is
    "00" &                              -- IDLE
    "01" &                              -- NULL_CMD
    "10" &                              -- WRITE_REG
    "11";                               -- READ_REG
  
  type TX_PKT_STATE_TYPE is (
    WAIT_FOR_DATA,
    TX_EVENT_HEAD_U,
    TX_EVENT_FIFO,
    TX_REG_HEAD_U,
    TX_REG_HEAD_L,
    TX_REG_U,
    TX_REG_L,
    TX_STATUS_HEAD_U,
    TX_STATUS_HEAD_L,
    TX_STATUS,
    TX_STATUS_0,
    TX_STATUS_1,
    TX_STATUS_2,
    TX_STATUS_3
    );

  type PACKET_DATA_TYPE is array (15 downto 0)
    of unsigned (7 downto 0);

  signal DCMBUS_DATA_int     : unsigned(15 downto 0)     := x"0000";
  signal DCMBUS_STRB_int     : boolean                   := false;
  signal PKT_SEQ             : unsigned(2 downto 0)      := "000";
  signal CMD_PACKET_DATA     : PACKET_DATA_TYPE;
  signal CURRENT_COMMAND     : COMMAND_TYPE;
  signal CMD_PACKET_COUNT    : integer range 15 downto 0 := 0;
  signal PROCESS_PACKET      : boolean                   := false;
  signal PACKET_DONE         : boolean                   := false;
  signal WAITING_FOR_COMMAND : boolean                   := false;
  signal TX_PKT_STATE        : TX_PKT_STATE_TYPE         := WAIT_FOR_DATA;
  signal NEXT_TX_PKT_STATE   : TX_PKT_STATE_TYPE         := WAIT_FOR_DATA;

  signal READ_REG_DATA         : unsigned(15 downto 0) := x"0000";
  signal READ_REG_READY        : boolean               := false;
  signal DCMBUS_READ_del       : boolean               := false;
  signal TX_REG_LAST           : boolean               := false;
  signal TX_REG_LAST_16        : boolean               := false;
  signal TX_REG_LAST_16_DEL    : boolean               := false;
  signal STATUS_PKT_READY      : boolean               := false;
  signal TX_STATUS_LAST        : boolean               := false;
  signal TX_STATUS_LAST_32     : boolean               := false;
  signal TX_STATUS_LAST_32_del : boolean               := false;

  signal PKT_LENGTH        : unsigned(5 downto 0) := "000000";
  signal PERIOD_BIT_del    : std_logic            := '0';
  signal DCMBUS_READ_int   : boolean              := false;
  signal SINC_COUNT        : unsigned(9 downto 0) := "0000000000";
  signal INSERT_COMMA      : boolean              := false;
  signal DATA_TX_STRB_int  : boolean              := false;
  signal TIMING_PKT_TIME   : unsigned (31 downto 0);
  signal WITH_APD_DATA     : boolean              := false;
  signal WITH_TM           : boolean              := false;
  signal TM_SENT           : boolean              := false;
  signal PKT_SEQ_inc       : boolean              := false;
  signal END_OF_PACKET     : boolean              := false;
  signal NON_EVENT_PENDING : boolean              := false;
  
begin
  CLOCKED : process (CLK_3_2) is
  begin  -- process CLOCKED_PROCESS
    if CLK_3_2'event and CLK_3_2 = '1' then  -- rising clock edge
      RX_Error <= false;
      -- fill in data on DATA_RX_STRB
      if DATA_RX_STRB then
        --flag data command overflow
        RX_Error <= PROCESS_PACKET;

        if CURRENT_COMMAND = IDLE then
          case DATA_RX(7 downto 4) is
            when "0000" =>
              CURRENT_COMMAND  <= NULL_CMD;
              CMD_PACKET_COUNT <= 1;
            when "0010" =>
              CURRENT_COMMAND  <= WRITE_REG;
              CMD_PACKET_COUNT <= 4;
            when "0011" =>
              CURRENT_COMMAND  <= READ_REG;
              CMD_PACKET_COUNT <= 4;
            when others =>
              --flag unknown command;
              RX_ERROR         <= true;
              CURRENT_COMMAND  <= IDLE;
              CMD_PACKET_COUNT <= 1;
          end case;
        else
          CMD_PACKET_DATA(CMD_PACKET_COUNT) <= DATA_RX;
          CMD_PACKET_COUNT                  <= CMD_PACKET_COUNT - 1;
        end if;

        -- if no data strobe wait for more data or data is done
      else
        PROCESS_PACKET <= (CMD_PACKET_COUNT = 0) and (CURRENT_COMMAND /= IDLE);
        if PACKET_DONE then
          CURRENT_COMMAND <= IDLE;
          PROCESS_PACKET  <= false;
        end if;
        
      end if;

    end if;
    
  end process CLOCKED;

  DCMBUS_DATA <= DCMBUS_DATA_int when DCMBUS_STRB_int else "ZZZZZZZZZZZZZZZZ";
  DCMBUS_STRB <= DCMBUS_STRB_int;

  CMD_PACKET_DATA_INTERPRETER : process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '1' then
      DCMBUS_DATA     <= "ZZZZZZZZZZZZZZZZ";
      DCMBUS_READ_int <= false;
      DCMBUS_STRB_int <= false;

      -- handshake across clock domains      
      PACKET_DONE <= PACKET_DONE and PROCESS_PACKET;
      if PROCESS_PACKET and not PACKET_DONE then
        PACKET_DONE <= true;
        case CURRENT_COMMAND is
          when WRITE_REG =>
            DCMBUS_ADDR     <= CMD_PACKET_DATA(4)& CMD_PACKET_DATA(3);
            DCMBUS_DATA_int <= CMD_PACKET_DATA(2)& CMD_PACKET_DATA(1);
            DCMBUS_STRB_int <= true;
          when READ_REG =>
            DCMBUS_ADDR     <= CMD_PACKET_DATA(4)& CMD_PACKET_DATA(3);
            DCMBUS_READ_int <= true;
          when others => null;
        end case;
      end if;
      -- ON REG READ BUFFER DATA UNTIL IT CAN BE SENT

      TX_REG_LAST_16     <= TX_REG_LAST;
      TX_REG_LAST_16_del <= TX_REG_LAST_16;
      READ_REG_READY     <= READ_REG_READY
                            and (not TX_REG_LAST_16_del or TX_REG_LAST_16);
      
      DCMBUS_READ_del <= DCMBUS_READ_int;
      if DCMBUS_READ_del then
        READ_REG_DATA  <= DCMBUS_DATA;
        READ_REG_ERROR <= READ_REG_READY;
        READ_REG_READY <= true;
      end if;
    end if;
  end process CMD_PACKET_DATA_INTERPRETER;

  DCMBUS_READ <= DCMBUS_READ_int;

  SEND_STATUS_PKT : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '1' then
      TX_STATUS_LAST_32     <= TX_STATUS_LAST;
      TX_STATUS_LAST_32_del <= TX_STATUS_LAST_32;
      STATUS_PKT_READY      <= STATUS_PKT_READY
                               and (not TX_STATUS_LAST_32_del or TX_STATUS_LAST_32);
      PERIOD_BIT_del <= CURRENT_TIME(25);
      if PERIOD_BIT_del = '0' and CURRENT_TIME(25) = '1' then
        STATUS_PKT_READY <= ENABLE_STATUS_PKT;
      end if;
    end if;
  end process SEND_STATUS_PKT;

  send_packets_CLK : process (CLK_3_2) is
  begin
    if CLK_3_2'event and CLK_3_2 = '1' then
      if RESET then
        TX_PKT_STATE <= WAIT_FOR_DATA;
        PKT_LENGTH   <= "000000";
      else
        TX_PKT_STATE <= NEXT_TX_PKT_STATE;
        TX_ERROR     <= false;

        if PKT_SEQ_inc then
          PKT_SEQ <= PKT_SEQ+1;
        end if;

        if not DATA_TX_STRB_int then
          SINC_COUNT <= "0000000000";
        else
          SINC_COUNT <= SINC_COUNT +1;
        end if;
        INSERT_COMMA <= SINC_COUNT(9 downto 0) > "1111000000";

        if PKT_LENGTH /= "000000" then
          PKT_LENGTH <= PKT_LENGTH - 1;
        end if;
      end if;

      case TX_PKT_STATE is
        when TX_EVENT_HEAD_U =>
          if not EVENT_FIFO_EMPTY then
            if EVENT_FIFO_DOUT(7 downto 5) = "001" then
              --send APD data
              PKT_LENGTH <= (N_DATA_PKT_WORDS + x"2") & '0';
            elsif EVENT_FIFO_DOUT(7 downto 5) = "101" then
              --send Time Marker data
              PKT_LENGTH <= "000100";
            else
              TX_ERROR   <= true;
              PKT_LENGTH <= "000001";
            end if;
          end if;
        when TX_STATUS_HEAD_U =>
          PKT_LENGTH <= "001000";
        when others=>
          null;
      end case;
    end if;
  end process;
  END_OF_PACKET     <= (PKT_LENGTH = "000000");
  NON_EVENT_PENDING <= READ_REG_READY
                       or STATUS_PKT_READY
                       or INSERT_COMMA;

  send_packets_COMB : process(
    TX_PKT_STATE,
    EVENT_FIFO_DOUT,
    PKT_SEQ,
    EVENT_FIFO_EMPTY,
    READ_REG_DATA,
    TIMING_PKT_FEB_STATUS,
    PKT_LENGTH,
    READ_REG_READY,
    STATUS_PKT_READY,
    INSERT_COMMA,
    END_OF_PACKET,
    NON_EVENT_PENDING
    ) is

  begin
    PKT_SEQ_inc      <= false;
    EVENT_FIFO_RSTRB <= false;
    DATA_TX_STRB_int <= false;
    TX_REG_LAST      <= false;
    TX_STATUS_LAST   <= false;
    case TX_PKT_STATE is
      
      when WAIT_FOR_DATA =>
        if READ_REG_READY then
          NEXT_TX_PKT_STATE <= TX_REG_HEAD_U;
        elsif STATUS_PKT_READY then
          NEXT_TX_PKT_STATE <= TX_STATUS_HEAD_U;
          --       elsif not EVENT_FIFO_EMPTY then
        elsif not EVENT_FIFO_EMPTY then
          NEXT_TX_PKT_STATE <= TX_EVENT_HEAD_U;
        else
          NEXT_TX_PKT_STATE <= WAIT_FOR_DATA;
        end if;

      when TX_EVENT_HEAD_U =>
        DATA_TX <= EVENT_FIFO_DOUT(7 downto 5)
                   & PKT_SEQ
                   & EVENT_FIFO_DOUT(1 downto 0);
        if not EVENT_FIFO_EMPTY then
          DATA_TX_STRB_int  <= true;
          PKT_SEQ_inc       <= true;
          EVENT_FIFO_RSTRB  <= true;
          NEXT_TX_PKT_STATE <= TX_EVENT_FIFO;
        else
          NEXT_TX_PKT_STATE <= WAIT_FOR_DATA;
        end if;

      when TX_EVENT_FIFO =>
        DATA_TX          <= EVENT_FIFO_DOUT;
        DATA_TX_STRB_int <= not EVENT_FIFO_EMPTY;
        EVENT_FIFO_RSTRB <= not EVENT_FIFO_EMPTY;
        if END_OF_PACKET then
          if NON_EVENT_PENDING then
            NEXT_TX_PKT_STATE <= WAIT_FOR_DATA;
          else
            NEXT_TX_PKT_STATE <= TX_EVENT_HEAD_U;
          end if;
        else
          NEXT_TX_PKT_STATE <= TX_EVENT_FIFO;
        end if;

      when TX_REG_HEAD_U =>
        PKT_SEQ_inc       <= true;
        DATA_TX           <= "100" & PKT_SEQ & "10";
        DATA_TX_STRB_int  <= true;
        NEXT_TX_PKT_STATE <= TX_REG_HEAD_L;
      when TX_REG_HEAD_L =>
        DATA_TX           <= x"00";
        DATA_TX_STRB_int  <= true;
        NEXT_TX_PKT_STATE <= TX_REG_U;
      when TX_REG_U =>
        DATA_TX           <= READ_REG_DATA(15 downto 8);
        DATA_TX_STRB_int  <= true;
        NEXT_TX_PKT_STATE <= TX_REG_L;
      when TX_REG_L =>
        TX_REG_LAST       <= true;
        DATA_TX           <= READ_REG_DATA(7 downto 0);
        DATA_TX_STRB_int  <= true;
        NEXT_TX_PKT_STATE <= WAIT_FOR_DATA;

      when TX_STATUS_HEAD_U =>
        PKT_SEQ_inc       <= true;
        DATA_TX           <= "010" & PKT_SEQ & "00";
        DATA_TX_STRB_int  <= true;
        NEXT_TX_PKT_STATE <= TX_STATUS_HEAD_L;
      when TX_STATUS_HEAD_L =>
        DATA_TX           <= x"00";
        DATA_TX_STRB_int  <= true;
        NEXT_TX_PKT_STATE <= TX_STATUS_0;
      when TX_STATUS_0 =>
        DATA_TX           <= unsigned(STATUS_PKT_FEB_STATUS(31 downto 24));
        DATA_TX_STRB_int  <= true;
        NEXT_TX_PKT_STATE <= TX_STATUS_1;
      when TX_STATUS_1 =>
        DATA_TX           <= unsigned(STATUS_PKT_FEB_STATUS(23 downto 16));
        DATA_TX_STRB_int  <= true;
        NEXT_TX_PKT_STATE <= TX_STATUS_2;
      when TX_STATUS_2 =>
        DATA_TX           <= unsigned(STATUS_PKT_FEB_STATUS(15 downto 8));
        DATA_TX_STRB_int  <= true;
        NEXT_TX_PKT_STATE <= TX_STATUS_3;
      when TX_STATUS_3 =>
        DATA_TX           <= unsigned(STATUS_PKT_FEB_STATUS(7 downto 0));
        DATA_TX_STRB_int  <= true;
        NEXT_TX_PKT_STATE <= TX_STATUS;
      when TX_STATUS =>
        DATA_TX          <= x"00";
        DATA_TX_STRB_int <= true;
        if END_OF_PACKET then
          TX_STATUS_LAST    <= true;
          NEXT_TX_PKT_STATE <= WAIT_FOR_DATA;
        else
          NEXT_TX_PKT_STATE <= TX_STATUS;
        end if;

      when others =>
        NEXT_TX_PKT_STATE <= WAIT_FOR_DATA;
    end case;
  end process send_packets_COMB;

  DATA_TX_STRB <= DATA_TX_STRB_int;
end architecture behav;




