
function [apdDat, regDat] = processFebPktsDebug()
%nWordsData = 5;
    FebDataFile = '../simData/usb_data.txt';
    usbDataFid = fopen(FebDataFile);
    DataCharIn = fscanf(usbDataFid, '%s ', [4 inf]);
    DataCharIn = DataCharIn'
    fclose(usbDataFid);
    %DataCharIn = uint16(DataCharIn());
    %DataPkts = DataCharIn(2 : 2 : n).*256 + DataCharIn(1 : 2 : n);
    %dec2hex(DataPkts)
    DataPkts = hex2dec(DataCharIn);
    DataPktsLength = length(DataPkts);
    regDat = [];
    regDatIndex = 1;

    apdDat.chanNumber = [];
    apdDat.timestamp = [];
    apdDat.magnitude = [];
    apdDatIndex = 1;
    pktIndex = 1;
    done = '0';

    while pktIndex <= DataPktsLength;
        %disp(dec2hex(DataPkts(pktIndex)))
        pktHeader = dec2bin(DataPkts(pktIndex), 16);
        pktType = pktHeader(1 : 3);
        switch pktType;
            
          case '000';
            %disp('Empty Frame Packet /n');
            pktIndex = pktIndex + 5;
            
          case '001'
            %disp('Data Frame Packet /n');
            nWordsData = bin2dec(pktHeader(9 : 11));
            DataPkts(pktIndex : min(pktIndex+3, length(DataPkts)));
            apdDat.chanNumber(apdDatIndex) = bin2dec(pktHeader(12 : 16));
            apdDat.timestamp(apdDatIndex) = uint32(DataPkts(pktIndex + 1))* 2^16 + uint32(DataPkts(pktIndex+2));
            for pktDataIndex = 1 : nWordsData
                apdDat.magnitude(apdDatIndex, pktDataIndex) = DataPkts(pktIndex ...
                                                                  + 2 + pktDataIndex);
            end
            pktIndex = pktIndex + 3+ nWordsData;
            apdDatIndex = apdDatIndex+1;
            
            
          case '010'
            %disp('Status Packet /n');
            pktIndex = pktIndex + 5;
            
          case '100'
            %disp('register Packet /n');
            regDat(regDatIndex) = DataPkts(pktIndex+1);
            pktIndex = pktIndex + 2;
            regDatIndex = regDatIndex +1;
            
          case '101'
            %disp('time Marker Frame Packet /n');
            pktIndex = pktIndex + 3;
            
          otherwise
            disp (['Unknown Packet type ', num2str(pktType)]);
            disp (['Line# ', num2str(pktIndex)]);
            for erln = 1:30
                disp(dec2hex(DataPkts(erln),4));
            end
             error('Unknown Packet type');
           
            
        end
    end
    apdDat.magnitude
    magHex = dec2hex(apdDat.magnitude, 3)
    %    magHex = reshape(magHex, [], nWordsData, 3)
%    permute(magHex, [2 3 1]);
figure(2)
    plot(apdDat.chanNumber, apdDat.magnitude, 'X')
figure(3)
    plot(apdDat.chanNumber, apdDat.timestamp, 'X')
    return