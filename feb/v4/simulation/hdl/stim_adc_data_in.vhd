
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
--library std;
--use std.textio.all;
use ieee.std_logic_textio.all;
use std.textio.all;
entity stim_adc_data_in is
  generic (
    stim_file_name : string := "simData/adcDataInc.txt"
    );
  port (
    ADC_DOUTAP     : out std_logic_vector(11 downto 0);
    ADC_DOUTAN     : out std_logic_vector(11 downto 0);
    ADC_DOUTBP     : out std_logic_vector(11 downto 0);
    ADC_DOUTBN     : out std_logic_vector(11 downto 0);
    ASIC_CHIPRESET : in  boolean;
    ADC_CLK        : in  std_logic
    );
end stim_adc_data_in;

architecture behav of stim_adc_data_in is
--  signal ENABLE     : boolean := false;
  signal endoffile  : bit     := '0';
  signal dataread   : unsigned(7 downto 0);
  signal datatosave : unsigned(7 downto 0);
  signal linenumber : integer := 1;
  type CHAN_DATA_TYPE is array (31 downto 0)
    of bit_vector(11 downto 0);
begin

  reading : process

    file infile         : text;              -- is "hdl/1.txt";
    variable inline     : line;
    variable CHAN_DATA  : CHAN_DATA_TYPE;
    variable CHAN_INDEX : integer;
    variable fstatus    : file_open_status;  -- declare file
    variable ENABLE     : boolean := false;



  begin
    if not ENABLE then
      wait until ASIC_CHIPRESET;
      wait until not ASIC_CHIPRESET;
      wait until ASIC_CHIPRESET;
      ENABLE     := true;
      file_open(fstatus, infile, stim_file_name, read_mode);
      ADC_DOUTAP <= x"aaa";
    end if;
    if (not endfile(infile)) then
      readline(infile, inline);

      for CHAN_INDEX in 0 to 31 loop
        hread(inline, CHAN_DATA(CHAN_INDEX));
      end loop;

      for DATA_INDEX in 0 to 7 loop
        ADC_DOUTAP <= to_stdlogicvector(CHAN_DATA(DATA_INDEX + 8));
        ADC_DOUTAN <= not(to_stdlogicvector(CHAN_DATA(DATA_INDEX + 8)));
        ADC_DOUTBP <= to_stdlogicvector(CHAN_DATA(DATA_INDEX + 24));
        ADC_DOUTBN <= not(to_stdlogicvector(CHAN_DATA(DATA_INDEX + 24)));
        wait until ADC_CLK = '0' and ADC_CLK'event;

        ADC_DOUTAP <= to_stdlogicvector(CHAN_DATA(DATA_INDEX));
        ADC_DOUTAN <= not(to_stdlogicvector(CHAN_DATA(DATA_INDEX)));
        ADC_DOUTBP <= to_stdlogicvector(CHAN_DATA(DATA_INDEX + 16));
        ADC_DOUTBN <= not(to_stdlogicvector(CHAN_DATA(DATA_INDEX + 16)));
        wait until ADC_CLK = '1' and ADC_CLK'event;
        
      end loop;
      
    else
      endoffile <= '1';
      file_close(infile);               -- closes file
      enable    := false;
      wait until ADC_CLK = '1' and ADC_CLK'event;
    end if;
  end process reading;

end architecture behav;

