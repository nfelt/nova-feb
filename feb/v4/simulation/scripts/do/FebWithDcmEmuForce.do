restart -force -nowave
set clk_in_period 31.25
set clk_16_period [expr $clk_in_period * 2]

force -freeze sim:/top_with_dcm_emulator/dcm_clk_in 1 0, 0 [expr $clk_in_period /2] ns -r $clk_in_period ns
force -freeze sim:/top_with_dcm_emulator/u_0/ASIC_SHIFTOUT '1' 0
force -freeze sim:/top_with_dcm_emulator/usb_flagb false 0
force -freeze sim:/top_with_dcm_emulator/usb_flagc true 0

run 1ns
run 100 ns
view wave 


