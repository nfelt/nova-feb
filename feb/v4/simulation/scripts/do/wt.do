onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Literal /dcm_comm_emu_send/bit_count
add wave -noupdate -format Logic /dcm_comm_emu_send/clk_3_2_int
add wave -noupdate -format Logic -radix hexadecimal /dcm_comm_emu_send/clk_32
add wave -noupdate -format Literal -radix hexadecimal /dcm_comm_emu_send/data_10par_out
add wave -noupdate -format Literal -radix hexadecimal /dcm_comm_emu_send/data_in
add wave -noupdate -format Literal -radix hexadecimal /dcm_comm_emu_send/encoded_data
add wave -noupdate -color {Cornflower Blue} -format Literal -radix hexadecimal /dcm_comm_emu_send/latched_data_in
add wave -noupdate -color {Cornflower Blue} -format Logic /dcm_comm_emu_send/latched_k_in
add wave -noupdate -format Literal -radix hexadecimal /dcm_comm_emu_send/shift_reg
add wave -noupdate -color Violet -format Logic /dcm_comm_emu_send/sync
add wave -noupdate -color Violet -format Logic /dcm_comm_emu_send/ser_data
add wave -noupdate -color Violet -format Logic /dcm_comm_emu_send/clk_3_2_sig
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {998600 ps} 0}
configure wave -namecolwidth 273
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
update
WaveRestoreZoom {989294 ps} {1000006 ps}
