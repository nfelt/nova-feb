The following files were generated for 'fifo_apd_data' in directory 
/Projects/nova/feb/v4/coregen/

fifo_apd_data_flist.txt:
   Text file listing all of the output files produced when a customized
   core was generated in the CORE Generator.

fifo_apd_data.asy:
   Graphical symbol information file. Used by the ISE tools and some
   third party tools to create a symbol representing the core.

fifo_apd_data.gise:
   ISE Project Navigator support file. This is a generated file and should
   not be edited directly.

fifo_apd_data.ise:
   ISE Project Navigator support file. This is a generated file and should
   not be edited directly.

fifo_apd_data.ngc:
   Binary Xilinx implementation netlist file containing the information
   required to implement the module in a Xilinx (R) FPGA.

fifo_apd_data.vhd:
   VHDL wrapper file provided to support functional simulation. This
   file contains simulation model customization data that is passed to
   a parameterized simulation model for the core.

fifo_apd_data.vho:
   VHO template file containing code that can be used as a model for
   instantiating a CORE Generator module in a VHDL design.

fifo_apd_data.xco:
   CORE Generator input file containing the parameters used to
   regenerate a core.

fifo_apd_data.xise:
   ISE Project Navigator support file. This is a generated file and should
   not be edited directly.

fifo_apd_data_readme.txt:
   Text file indicating the files generated and how they are used.

fifo_apd_data_xmdf.tcl:
   ISE Project Navigator interface file. ISE uses this file to determine
   how the files output by CORE Generator for the core can be integrated
   into your ISE project.

fifo_generator_ug175.pdf:
   Please see the core data sheet.


Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

