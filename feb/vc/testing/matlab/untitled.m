dat=[  436.1250 390.0437  349.8750  302.4375  266.6250  220.2688 181.2812 132.6813 103.6312  55.9375  14.6625 0 ]
figure(1);
stairs(0:11,dat)
title('Chan Offset Adjustment (Per Channel)');
legend('ch31');
xlabel('ASIC Setting');
ylabel('ADC Count');