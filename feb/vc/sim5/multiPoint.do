vsim -gstim_file_name=simData/adcDataCh.txt nova_feb.top_with_dcm_emulator
do scripts/do/FebWithDcmEmuForce.do
force -freeze sim:/top_with_dcm_emulator/u_0/spi_dout 0 0
do scripts/tcl/XmlFileIo.tcl
do scripts/wave/multipointSignals.do
do scripts/do/setThresholds.do
#xml scripts/xml/DcmStartup.xml
xml scripts/xml/DcsReadout_MP.xml
#run 50 us
#xml scripts/xml/regWriteRead.xml

