onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /top_with_dcm_emulator/u_0/dcm_sync
add wave -noupdate /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/data_tx_strb_int
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/time_marker_rate
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/time_marker_cnt
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/tick_1us
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/send_tm
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/dcm_sync
add wave -noupdate /top_with_dcm_emulator/u_0/data_processing_inst/send_tm
add wave -noupdate -expand /top_with_dcm_emulator/u_0/data_processing_inst/dsp_trigger
add wave -noupdate /top_with_dcm_emulator/u_0/data_processing_inst/dsp_chan_number
add wave -noupdate /top_with_dcm_emulator/u_0/data_processing_inst/dsp_en
add wave -noupdate /top_with_dcm_emulator/u_0/data_processing_inst/dsp_magnitude
add wave -noupdate /top_with_dcm_emulator/u_0/data_processing_inst/dsp_strb
add wave -noupdate /top_with_dcm_emulator/u_0/data_processing_inst/dsp_timestamp
add wave -noupdate /top_with_dcm_emulator/u_0/data_processing_inst/dsp_trigger
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/data_tx
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/tm_ts_sync_error
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/event_fifo_dout
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/tm_ts_buff
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/tm_ts_last
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/event_fifo_tm
add wave -noupdate -group {slect event fifo} /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/event_fifo_type
add wave -noupdate -group {slect event fifo} /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/end_of_packet
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/tx_pkt_state
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/tm_ts_cnt
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/tm_ts_mask
add wave -noupdate -group {slect event fifo} /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_empty
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/tm_ts_tx_latch
add wave -noupdate -group {slect event fifo} /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/data_tx_strb
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/event_fifo_sel_clear
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/serial_tx
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/serial_rx
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/data_rx
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/data_rx_strb
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/data_tx
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/data_tx_strb
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/tx_pkt_state
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/serial_tx
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/serial_rx
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/data_rx
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/data_rx_strb
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/data_tx
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/data_tx_strb
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/tx_pkt_state
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/u_1/u_6/current_state
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/u_1/u_6/dcm_reset
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/u_1/u_6/use_config_data_cld
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/u_1/u_3/clk_32
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/u_1/u_3/enable_clk
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/u_1/u_3/shift_bits
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/u_1/u_3/not_clk_32
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/u_1/u_3/use_config_data
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/u_1/u_3/config_clk
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/u_0/dcm_comm_emu_clk_inst/dcm_clk_in_p
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/u_0/dcm_comm_emu_clk_inst/dcm_clk_in_n
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/u_0/dcm_comm_emu_clk_inst/locked
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/u_0/dcm_comm_emu_clk_inst/clk_16
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/u_0/dcm_comm_emu_clk_inst/powerup_reset
add wave -noupdate -group {USB IO} -radix hexadecimal /top_with_dcm_emulator/usb_fd
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/usb_ifclk
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/usb_flagb
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/usb_flagc
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/usb_pktend
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/usb_reset_b
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/usb_sloe
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/usb_slrd
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/usb_slwr
add wave -noupdate -group {USB IO} -radix hexadecimal /top_with_dcm_emulator/usb_fd
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/usb_ifclk
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/usb_flagb
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/usb_flagc
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/usb_pktend
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/usb_reset_b
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/usb_sloe
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/usb_slrd
add wave -noupdate -group {USB IO} /top_with_dcm_emulator/usb_slwr
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_1/u_4/clk_3_2_int
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/clk_16
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/clk_32
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/clk_64
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/asic_clk_out
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/enable_status_pkt
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/enable_daq
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/asic_chipreset_int
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_1/u_4/clk_3_2_int
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/clk_16
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/clk_32
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/clk_64
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/asic_clk_out
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/enable_status_pkt
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/enable_daq
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/asic_chipreset_int
add wave -noupdate -group {ADC Data In} /top_with_dcm_emulator/u_0/dcm_sync
add wave -noupdate -group {ADC Data In} /top_with_dcm_emulator/u_0/dcm_sync_p
add wave -noupdate -group {ADC Data In} /top_with_dcm_emulator/u_0/timing_inst/dcm_sync_del
add wave -noupdate -group {ADC Data In} /top_with_dcm_emulator/u_0/timing_inst/current_time_int_del
add wave -noupdate -group {ADC Data In} /top_with_dcm_emulator/u_0/timing_inst/use_clk16_inv_int
add wave -noupdate -group {ADC Data In} /top_with_dcm_emulator/u_0/asic_chipreset
add wave -noupdate -group {ADC Data In} /top_with_dcm_emulator/u_0/asic_chipreset_int
add wave -noupdate -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/timing_inst/asic_chan_ptr
add wave -noupdate -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/timing_inst/adc_chan_ptr
add wave -noupdate -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/raw_chan_number
add wave -noupdate -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/raw_magnitude
add wave -noupdate -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/current_time
add wave -noupdate -group {ADC Data In} /top_with_dcm_emulator/u_0/asic_chipreset
add wave -noupdate -group {ADC Data In} /top_with_dcm_emulator/generate_feb4_tb/adc_input_stimulus/asic_chan_cnt
add wave -noupdate -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/adc_doutbp
add wave -noupdate -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/adc_doutap
add wave -noupdate -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/generate_feb4_tb/adc_input_stimulus/adc_data_in
add wave -noupdate -group {ADC Data In} -color {Orange Red} -radix hexadecimal /top_with_dcm_emulator/u_0/generate_feb4_din/adc_par_interface_inst/magnitude_ch_async
add wave -noupdate -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/raw_chan_number
add wave -noupdate -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/raw_magnitude
add wave -noupdate -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/current_time
add wave -noupdate -group {event fifo data processing} -expand /top_with_dcm_emulator/u_0/data_processing_inst/dsp_trigger
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/send_tm
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/dsp_timestamp
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_dout_vector
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_empty_vector
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_full_vector
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_rstrb_vector
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_empty_int
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_sel
add wave -noupdate -group {event fifo data processing} /top_with_dcm_emulator/u_0/data_processing_inst/dsp_trigger
add wave -noupdate -group {event fifo data processing} /top_with_dcm_emulator/u_0/data_processing_inst/raw_timestamp
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_dout_vector
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_empty_vector
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_full_vector
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_rstrb_vector
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_empty_int
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_sel
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {119625073 ps} 0} {{Cursor 3} {159861251 ps} 0} {{Cursor 4} {1021060936 ps} 0} {{Cursor 5} {193647022 ps} 0} {{Cursor 5} {12125666 ps} 0} {{Cursor 6} {59406350 ps} 0}
configure wave -namecolwidth 348
configure wave -valuecolwidth 45
configure wave -justifyvalue left
configure wave -signalnamewidth 2
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {325729324 ps}
