#vsim -t ps  -gstim_file_name=simData/adcDataCh.txt nova_feb.top_with_dcm_emulator
#vsim -gstim_file_name=simData/adcDataInc.txt nova_feb.top_with_dcm_emulator
vsim -gstim_file_name=simData/adcDataInMax.txt nova_feb.top_with_dcm_emulator
do scripts/do/FebWithDcmEmuForce.do
do scripts/tcl/XmlFileIo.tcl
do scripts/wave/multipointSignals.do
do scripts/do/setThresholds.do
xml scripts/xml/DcmStartup.xml
xml scripts/xml/setRegistersDefault.xml
xml scripts/xml/dcmSyncPreset.xml
run 10us
xml scripts/xml/dcmSync1.xml
xml scripts/xml/setAsic.xml
xml scripts/xml/oscopeReadout.xml
run 100us
