
fileID = fopen('../simData/adcDataInc.txt','w');
for r = 1 : 4
    for i = 0 : 127
        for j = 0 : 31
            fprintf(fileID, '%4s', dec2hex(j+i*32, 3));
        end
        fprintf(fileID, '%s\n', ' ');
    end
    for i = 127 : -1 : 0
        for j = 31 : -1 : 0
            fprintf(fileID, '%4s', dec2hex(j+i*32, 3));
        end
        fprintf(fileID, '%s\n', ' ');
    end
end
fclose(fileID);