onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /top_with_dcm_emulator/u_0/clk_128
add wave -noupdate /top_with_dcm_emulator/u_0/status_inst/error_flags_reg
add wave -noupdate /top_with_dcm_emulator/u_0/status_inst/status_reg
add wave -noupdate /top_with_dcm_emulator/u_0/dcm_sync
add wave -noupdate /top_with_dcm_emulator/u_0/controller_inst/command_interp_inst/dcm_sync_del1
add wave -noupdate /top_with_dcm_emulator/u_0/controller_inst/command_interp_inst/enable_tm_pkt_preset
add wave -noupdate /top_with_dcm_emulator/u_0/dcm_sync_en
add wave -noupdate /top_with_dcm_emulator/u_0/enable_status_pkt
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/send_tm
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/dcm_sync
add wave -noupdate /top_with_dcm_emulator/u_0/controller_inst/command_interp_inst/dcm_sync_en_int
add wave -noupdate /top_with_dcm_emulator/u_0/controller_inst/command_interp_inst/enable_tm_pkt_int
add wave -noupdate -group DSP -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/data_pkt_daqid
add wave -noupdate -group DSP -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/data_pkt_daqid
add wave -noupdate -group DSP -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/data_pkt_encode
add wave -noupdate -group DSP -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/data_pkt_encode_legacy
add wave -noupdate -group DSP -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/data_pkt_start
add wave -noupdate -group DSP -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/dsp_chan_number
add wave -noupdate -group DSP -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(0)/trigger_inst/data_delay(0)
add wave -noupdate -group DSP -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(0)/trigger_inst/data_delay(32)
add wave -noupdate -group DSP -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/dsp_magnitude
add wave -noupdate -group DSP -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/dsp_timestamp
add wave -noupdate -group DSP -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/dsp_trigger
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/dsp_strb
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/tm_ts_sync_error
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/event_fifo_dout
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/tm_ts_buff
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/tm_ts_last
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/event_fifo_tm
add wave -noupdate -group {slect event fifo} /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/event_fifo_type
add wave -noupdate -group {slect event fifo} /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/end_of_packet
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/tx_pkt_state
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/tm_ts_cnt
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/tm_ts_mask
add wave -noupdate -group {slect event fifo} /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_empty
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/tm_ts_tx_latch
add wave -noupdate -group {slect event fifo} /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/data_tx_strb
add wave -noupdate -group {slect event fifo} -radix hexadecimal /top_with_dcm_emulator/u_0/event_fifo_sel_clear
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/serial_tx
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/serial_rx
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/data_rx
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/data_rx_strb
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/data_tx
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/data_tx_strb
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/tx_pkt_state
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/serial_tx
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/serial_rx
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/data_rx
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/data_rx_strb
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/data_tx
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/data_tx_strb
add wave -noupdate -group {serial io} -radix hexadecimal /top_with_dcm_emulator/u_0/dcm_comm_interface_inst/u_1/tx_pkt_state
add wave -noupdate -expand -group {USB IO} /top_with_dcm_emulator/u_0/dcm_comm_emu_clk_inst/clk_16
add wave -noupdate -expand -group {USB IO} -radix hexadecimal /top_with_dcm_emulator/usb_fd
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_1/u_4/clk_3_2_int
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/clk_16
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/clk_32
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/clk_64
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/asic_clk_out
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/enable_status_pkt
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/enable_daq
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/asic_chipreset_int
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_1/u_4/clk_3_2_int
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/clk_16
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/clk_32
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/clk_64
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/asic_clk_out
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/enable_status_pkt
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/enable_daq
add wave -noupdate -group {Clks and FEB States} /top_with_dcm_emulator/u_0/asic_chipreset_int
add wave -noupdate -group {ADC Data In} /top_with_dcm_emulator/u_0/dcm_sync
add wave -noupdate -group {ADC Data In} /top_with_dcm_emulator/u_0/dcm_sync_p
add wave -noupdate -group {ADC Data In} /top_with_dcm_emulator/u_0/timing_inst/dcm_sync_del
add wave -noupdate -group {ADC Data In} /top_with_dcm_emulator/u_0/timing_inst/current_time_int_del
add wave -noupdate -group {ADC Data In} /top_with_dcm_emulator/u_0/timing_inst/use_clk16_inv_int
add wave -noupdate -group {ADC Data In} /top_with_dcm_emulator/u_0/asic_chipreset
add wave -noupdate -group {ADC Data In} /top_with_dcm_emulator/u_0/asic_chipreset_int
add wave -noupdate -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/timing_inst/asic_chan_ptr
add wave -noupdate -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/timing_inst/adc_chan_ptr
add wave -noupdate -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/raw_chan_number
add wave -noupdate -group {ADC Data In} -radix hexadecimal -expand -subitemconfig {/top_with_dcm_emulator/u_0/raw_magnitude(3) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(2) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(1) {-height 18 -radix hexadecimal -expand} /top_with_dcm_emulator/u_0/raw_magnitude(1)(11) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(1)(10) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(1)(9) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(1)(8) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(1)(7) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(1)(6) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(1)(5) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(1)(4) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(1)(3) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(1)(2) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(1)(1) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(1)(0) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(0) {-height 18 -radix hexadecimal -expand} /top_with_dcm_emulator/u_0/raw_magnitude(0)(11) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(0)(10) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(0)(9) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(0)(8) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(0)(7) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(0)(6) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(0)(5) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(0)(4) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(0)(3) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(0)(2) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(0)(1) {-height 18 -radix hexadecimal} /top_with_dcm_emulator/u_0/raw_magnitude(0)(0) {-height 18 -radix hexadecimal}} /top_with_dcm_emulator/u_0/raw_magnitude
add wave -noupdate -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/current_time
add wave -noupdate -group {ADC Data In} /top_with_dcm_emulator/u_0/asic_chipreset
add wave -noupdate -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/raw_chan_number
add wave -noupdate -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/raw_magnitude
add wave -noupdate -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/current_time
add wave -noupdate -group {event fifo data processing} -expand /top_with_dcm_emulator/u_0/data_processing_inst/dsp_trigger
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/send_tm
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/dsp_timestamp
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_dout_vector
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_empty_vector
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_full_vector
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_rstrb_vector
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_empty_int
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_sel
add wave -noupdate -group {event fifo data processing} /top_with_dcm_emulator/u_0/data_processing_inst/dsp_trigger
add wave -noupdate -group {event fifo data processing} /top_with_dcm_emulator/u_0/data_processing_inst/raw_timestamp
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_dout_vector
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_empty_vector
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_full_vector
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_rstrb_vector
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_empty_int
add wave -noupdate -group {event fifo data processing} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_sel
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(0)/trigger_inst/chan_data_dcs
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(0)/trigger_inst/magnitude_fixed
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(0)/trigger_inst/magnitude_float
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(3)/trigger_inst/magnitude_fixed
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(3)/trigger_inst/magnitude_float
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(1)/trigger_inst/magnitude_fixed
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(1)/trigger_inst/magnitude_float
add wave -noupdate -expand -group {adc pwrup} /top_with_dcm_emulator/u_0/controller_inst/adc_pwr_cmd
add wave -noupdate -expand -group {adc pwrup} /top_with_dcm_emulator/u_0/controller_inst/adc_pwr_direction
add wave -noupdate -expand -group {adc pwrup} /top_with_dcm_emulator/u_0/controller_inst/adc_power_ctrl_inst/adc_pwr_direction_latch
add wave -noupdate -expand -group {adc pwrup} /top_with_dcm_emulator/u_0/controller_inst/adc_power_ctrl_inst/ramp_cnt
add wave -noupdate -expand -group {adc pwrup} /top_with_dcm_emulator/u_0/controller_inst/adc_power_ctrl_inst/serial_number_dec
add wave -noupdate -expand -group {adc pwrup} /top_with_dcm_emulator/u_0/controller_inst/spi_pwrup_done
add wave -noupdate -expand -group {adc pwrup} /top_with_dcm_emulator/u_0/adc_pdwn0
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {119625073 ps} 0} {{Cursor 3} {134528275 ps} 0} {{Cursor 4} {1021060936 ps} 0} {{Cursor 5} {268603402 ps} 0} {{Cursor 5} {5889524 ps} 0} {{Cursor 6} {87953225 ps} 0} {{Cursor 7} {150429777 ps} 0} {{Cursor 8} {150928577 ps} 0} {{Cursor 9} {151428082 ps} 0}
configure wave -namecolwidth 399
configure wave -valuecolwidth 187
configure wave -justifyvalue left
configure wave -signalnamewidth 2
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {180510864 ps}
