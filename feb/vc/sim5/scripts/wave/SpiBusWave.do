onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -group BEEBUS -format Literal -radix hexadecimal /top/beebus_addr
add wave -noupdate -group BEEBUS -format Logic /top/clk_16
add wave -noupdate -group BEEBUS -format Literal -radix hexadecimal /top/beebus_data
add wave -noupdate -group BEEBUS -format Logic /top/beebus_read
add wave -noupdate -group BEEBUS -format Logic /top/beebus_strb
add wave -noupdate -format Logic /top/clk_16
add wave -noupdate -format Logic /top/clk_32
add wave -noupdate -format Logic /top/clk_64
add wave -noupdate -divider {SPI Signals}
add wave -noupdate -format Logic /top/spi_dout
add wave -noupdate -format Logic /top/spi_sclk
add wave -noupdate -format Logic /top/spi_din
add wave -noupdate -format Logic /top/tec_adc_cs
add wave -noupdate -format Logic /top/tec_dac_ldac
add wave -noupdate -format Logic /top/tec_dac_sync
add wave -noupdate -format Logic /top/temp_sensor_cs
add wave -noupdate -divider internal
add wave -noupdate -format Literal -radix hexadecimal /top/u_5/u_3/spi_command
add wave -noupdate -format Literal -radix hexadecimal /top/u_5/u_3/spi_data
add wave -noupdate -format Literal /top/u_5/u_3/bit_count
add wave -noupdate -format Literal -radix hexadecimal /top/u_5/u_3/spi_sreg
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {755361000 ps} 0} {{Cursor 2} {426670000 ps} 0} {{Cursor 3} {1972950000 ps} 0} {{Cursor 4} {2792512000 ps} 0}
configure wave -namecolwidth 281
configure wave -valuecolwidth 106
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {1402 ns} {11043 ns}
