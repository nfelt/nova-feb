onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group BEEBUS -format Literal -radix hexadecimal /top/beebus_addr
add wave -noupdate -expand -group BEEBUS -format Logic /top/clk_16
add wave -noupdate -expand -group BEEBUS -format Literal -radix hexadecimal /top/beebus_data
add wave -noupdate -expand -group BEEBUS -format Logic /top/beebus_read
add wave -noupdate -expand -group BEEBUS -format Logic /top/beebus_strb
add wave -noupdate -expand -group SPYBUS -format Literal -radix hexadecimal /top/u_4/spybus_data
add wave -noupdate -expand -group SPYBUS -format Logic -radix hexadecimal /top/u_4/spybus_read
add wave -noupdate -format Logic /top/u_4/u_0/spybus_data_sel
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/usb_fd
add wave -noupdate -format Literal /top/u_4/u_6/beebus_dbyte_reg
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/spybus_data
add wave -noupdate -format Logic /top/u_4/spybus_read
add wave -noupdate -format Logic /top/u_4/spybus_strb
add wave -noupdate -format Logic /top/powerup_reset
add wave -noupdate -divider {rx tx data}
add wave -noupdate -format Logic /top/u_4/u_1/data_packet_done
add wave -noupdate -format Logic /top/u_4/u_1/data_packet_ready
add wave -noupdate -format Logic /top/apd_data_buffer_empty
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/data_rx
add wave -noupdate -format Logic /top/u_4/data_rx_strb
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/data_tx
add wave -noupdate -format Logic /top/u_4/data_tx_strb
add wave -noupdate -divider {DCM_Emulator Receive}
add wave -noupdate -format Logic /top/u_4/u_0/linked_int
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_0/data_rx
add wave -noupdate -format Logic /top/u_4/u_0/data_rx_strb
add wave -noupdate -format Logic /top/u_4/u_0/clk_3_2_int
add wave -noupdate -format Literal /top/u_4/u_0/last_count
add wave -noupdate -format Literal /top/u_4/u_0/current_count
add wave -noupdate -format Logic /top/u_4/u_0/buffer_read_ready
add wave -noupdate -format Logic /top/u_4/u_0/buffer_empty
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_0/read_data_count
add wave -noupdate -format Logic /top/u_4/u_0/spybus_clk
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_0/spybus_data_int
add wave -noupdate -format Logic /top/u_4/u_0/spybus_data_sel_int
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {10073729000 ps} 0} {{Cursor 2} {2879590 ps} 0} {{Cursor 3} {1972950000 ps} 0} {{Cursor 4} {1079882000 ps} 0}
configure wave -namecolwidth 281
configure wave -valuecolwidth 142
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {2726149 ps}
