
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library STD;
use STD.TEXTIO.all;

entity STIM_SADC_DATA_IN is
  generic (
    stim_file_name : string := "simData/adcDataPing.txt"
    );
  port (
    ADC_DOUT_P     : out std_logic_vector(15 downto 0);
    ADC_DOUT_N     : out std_logic_vector(15 downto 0);
    CLKOUT_P       : out std_logic := '0';
    CLKOUT_N       : out std_logic;
    FRAME_P        : out std_logic;
    FRAME_N        : out std_logic;
    ASIC_CHIPRESET : in  boolean;
    ADC_CLK        : in  std_logic
    );
end STIM_SADC_DATA_IN;

architecture BEHAV of STIM_SADC_DATA_IN is
--  SIGNAL ENABLE     : BOOLEAN := FALSE;
  constant ADC_OUT_DELAY : time                          := 3.2 ns;
  signal ENDOFFILE       : bit                           := '0';
  signal DATAREAD        : unsigned(7 downto 0);
  signal DATATOSAVE      : unsigned(7 downto 0);
  signal LINENUMBER      : integer                       := 1;
  signal CLKOUT          : std_logic                     := '0';
  signal FRAME           : std_logic                     := '1';
  signal ADC_DOUT        : std_logic_vector(15 downto 0) := X"AAAA";
  type CHAN_DATA_TYPE is array (31 downto 0)
    of bit_vector(11 downto 0);
begin
  CLKOUT_P   <= CLKOUT       after ADC_OUT_DELAY;
  CLKOUT_N   <= not CLKOUT   after ADC_OUT_DELAY;
  FRAME_P    <= FRAME        after ADC_OUT_DELAY;
  FRAME_N    <= not FRAME    after ADC_OUT_DELAY;
  ADC_DOUT_P <= ADC_DOUT     after ADC_OUT_DELAY;
  ADC_DOUT_N <= not ADC_DOUT after ADC_OUT_DELAY;

  READING : process

    file INFILE        : text;
    variable INLINE    : line;
    variable CHAN_DATA : CHAN_DATA_TYPE;
    variable FSTATUS   : file_open_status;
    variable ENABLE    : boolean := false;
    variable ADC_INDEX : integer;
    variable BIT_INDEX : integer;


  begin
    if not ENABLE then
      wait until ASIC_CHIPRESET;
      ADC_DOUT <= X"0000";
      wait until not ASIC_CHIPRESET;
      -- FEB SIM USES 2 RESETS
      -- WAIT UNTIL ASIC_CHIPRESET;
      -- WAIT UNTIL NOT ASIC_CHIPRESET;
      ENABLE   := true;
      ADC_DOUT <= X"0000";
      FILE_OPEN(FSTATUS, INFILE, STIM_FILE_NAME, read_mode);
    end if;
    if (not ENDFILE(INFILE)) then
      READLINE(INFILE, INLINE);
      for CHAN_INDEX in 0 to 31 loop
        HREAD(INLINE, CHAN_DATA(CHAN_INDEX));
      end loop;

      for MUX_INDEX in 0 to 1 loop
        wait until ADC_CLK = '1' and ADC_CLK'event;
        for BIT_INDEX in 11 downto 0 loop
          CLKOUT <= '0';
          for ADC_INDEX in 0 to 15 loop
            CLKOUT              <= not CLKOUT;
            ADC_DOUT(ADC_INDEX) <= TO_STDLOGICVECTOR(CHAN_DATA(2*ADC_INDEX + MUX_INDEX))(BIT_INDEX);

          end loop;
          if BIT_INDEX < 6 then
            FRAME <= '0';
          else
            FRAME <= '1';
          end if;
          wait for 5.208 ns;
        end loop;
      end loop;
      
    else
      ENDOFFILE <= '1';
      FILE_CLOSE(INFILE);
      ENABLE    := false;
      wait until ADC_CLK = '1' and ADC_CLK'event;
    end if;
  end process READING;

end architecture BEHAV;

