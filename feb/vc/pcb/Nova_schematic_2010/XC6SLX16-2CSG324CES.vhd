-- VHDL created by Orcad Capture
Library ieee;
Use ieee.std_logic_1164.all;
Use ieee.numeric_std.all;

ENTITY \xc6slx16-2csg324ces\ is
	PORT (
		\1_2VDS_CLOCKIN\	: INOUT STD_LOGIC;
		SPI_DOUT	: INOUT STD_LOGIC;
		TEC_DAC_SYNC_B	: INOUT STD_LOGIC;
		SER_NUM_CS_B	: INOUT STD_LOGIC;
		TEC_ADC_CH1_B_CH2	: INOUT STD_LOGIC;
		TEC_DAC_LDAC_B	: INOUT STD_LOGIC;
		TEC_ADC_CS_B	: INOUT STD_LOGIC;
		SPI_DIN	: INOUT STD_LOGIC;
		SPI_SCLK	: INOUT STD_LOGIC;
		TEMP_SENSOR_CS_B	: INOUT STD_LOGIC;
		TEC_ENABLE_B	: INOUT STD_LOGIC;
		USB_SLWR	: INOUT STD_LOGIC;
		USB_SLRD	: INOUT STD_LOGIC;
		LA6	: INOUT STD_LOGIC;
		USB_FD14	: INOUT STD_LOGIC;
		USB_FD15	: INOUT STD_LOGIC;
		USB_FD12	: INOUT STD_LOGIC;
		USB_FD13	: INOUT STD_LOGIC;
		USB_FD10	: INOUT STD_LOGIC;
		USB_FD11	: INOUT STD_LOGIC;
		LA4	: INOUT STD_LOGIC;
		USB_FD8	: INOUT STD_LOGIC;
		USB_FD9	: INOUT STD_LOGIC;
		LA2	: INOUT STD_LOGIC;
		USB_IFCLK	: INOUT STD_LOGIC;
		LA5	: INOUT STD_LOGIC;
		USB_FIFOADR1	: INOUT STD_LOGIC;
		USB_PKTEND_B	: INOUT STD_LOGIC;
		USB_SLOE	: INOUT STD_LOGIC;
		USB_FIFOADR0	: INOUT STD_LOGIC;
		USB_FLAGB	: INOUT STD_LOGIC;
		USB_FLAGC	: INOUT STD_LOGIC;
		USB_FD6	: INOUT STD_LOGIC;
		USB_FD7	: INOUT STD_LOGIC;
		USB_FD4	: INOUT STD_LOGIC;
		USB_FD5	: INOUT STD_LOGIC;
		USB_FD2	: INOUT STD_LOGIC;
		USB_FD3	: INOUT STD_LOGIC;
		USB_FD0	: INOUT STD_LOGIC;
		USB_FD1	: INOUT STD_LOGIC;
		USB_RESET_B	: INOUT STD_LOGIC;
		LA0	: INOUT STD_LOGIC;
		LA1	: INOUT STD_LOGIC;
		LA3	: INOUT STD_LOGIC;
		ADC_DoutB2n	: INOUT STD_LOGIC;
		ADC_DoutB2p	: INOUT STD_LOGIC;
		ADC_DoutA10n	: INOUT STD_LOGIC;
		ADC_DoutA10p	: INOUT STD_LOGIC;
		ADC_DoutB11n	: INOUT STD_LOGIC;
		ADC_DoutB11p	: INOUT STD_LOGIC;
		ADC_DoutB5n	: INOUT STD_LOGIC;
		ADC_DoutB5p	: INOUT STD_LOGIC;
		ADC_DoutB0n	: INOUT STD_LOGIC;
		ADC_DoutB0p	: INOUT STD_LOGIC;
		ADC_DoutB3n	: INOUT STD_LOGIC;
		ADC_DoutB3p	: INOUT STD_LOGIC;
		ADC_DoutA8n	: INOUT STD_LOGIC;
		ADC_DoutA8p	: INOUT STD_LOGIC;
		ADC_DoutB7n	: INOUT STD_LOGIC;
		ADC_DoutB7p	: INOUT STD_LOGIC;
		ADC_DoutB1n	: INOUT STD_LOGIC;
		ADC_DoutB1p	: INOUT STD_LOGIC;
		ADC_DoutA5n	: INOUT STD_LOGIC;
		ADC_DoutA5p	: INOUT STD_LOGIC;
		ADC_DoutA11n	: INOUT STD_LOGIC;
		ADC_DoutA11p	: INOUT STD_LOGIC;
		ADC_DoutA9n	: INOUT STD_LOGIC;
		ADC_DoutA9p	: INOUT STD_LOGIC;
		ADC_DoutB10n	: INOUT STD_LOGIC;
		ADC_DoutB10p	: INOUT STD_LOGIC;
		ADC_DoutA4n	: INOUT STD_LOGIC;
		ADC_DoutA4p	: INOUT STD_LOGIC;
		ADC_DoutA7n	: INOUT STD_LOGIC;
		ADC_DoutA7p	: INOUT STD_LOGIC;
		ADC_DoutB9n	: INOUT STD_LOGIC;
		ADC_DoutB9p	: INOUT STD_LOGIC;
		ADC_DoutA2n	: INOUT STD_LOGIC;
		ADC_DoutA2p	: INOUT STD_LOGIC;
		ADC_DoutA6n	: INOUT STD_LOGIC;
		ADC_DoutA6p	: INOUT STD_LOGIC;
		ADC_DoutA1n	: INOUT STD_LOGIC;
		ADC_DoutA1p	: INOUT STD_LOGIC;
		ADC_DoutA3n	: INOUT STD_LOGIC;
		ADC_DoutA3p	: INOUT STD_LOGIC;
		ADC_DoutA0n	: INOUT STD_LOGIC;
		ADC_DoutA0p	: INOUT STD_LOGIC;
		ADC_DoutB8n	: INOUT STD_LOGIC;
		ADC_DoutB8p	: INOUT STD_LOGIC;
		ADC_CLKinn	: INOUT STD_LOGIC;
		ADC_CLKinp	: INOUT STD_LOGIC;
		ADC_DoutB4n	: INOUT STD_LOGIC;
		ADC_DoutB4p	: INOUT STD_LOGIC;
		ADC_DoutB6n	: INOUT STD_LOGIC;
		ADC_DoutB6p	: INOUT STD_LOGIC;
		FEB_TCK	: INOUT STD_LOGIC;
		FEB_TDI	: INOUT STD_LOGIC;
		FEB_TDO	: INOUT STD_LOGIC;
		FEB_TMS	: INOUT STD_LOGIC;
		C_DONE	: INOUT STD_LOGIC;
		C_M1	: INOUT STD_LOGIC;
		tbdn2	: INOUT STD_LOGIC;
		tbdp2	: INOUT STD_LOGIC;
		ASIC_SHAPERRST	: INOUT STD_LOGIC;
		C_M0	: INOUT STD_LOGIC;
		C_CLK	: INOUT STD_LOGIC;
		ASIC_SHIFTOUT	: INOUT STD_LOGIC;
		\ReceiveClock-\	: INOUT STD_LOGIC;
		\ReceiveClock+\	: INOUT STD_LOGIC;
		tbdn3	: INOUT STD_LOGIC;
		tbdp3	: INOUT STD_LOGIC;
		C_DIN	: INOUT STD_LOGIC;
		ASIC_SRCK	: INOUT STD_LOGIC;
		\TransmitData-\	: INOUT STD_LOGIC;
		\TransmitData+\	: INOUT STD_LOGIC;
		ASIC_SHFTIN	: INOUT STD_LOGIC;
		ADC_Error	: INOUT STD_LOGIC;
		tbdn1	: INOUT STD_LOGIC;
		tbdp1	: INOUT STD_LOGIC;
		ASIC_OUTCLK_N	: INOUT STD_LOGIC;
		ASIC_OUTCLK_P	: INOUT STD_LOGIC;
		ASIC_CHIPRESET	: INOUT STD_LOGIC;
		ASIC_TESTINJECT	: INOUT STD_LOGIC;
		\ReceiveSync-\	: INOUT STD_LOGIC;
		\ReceiveSync+\	: INOUT STD_LOGIC;
		\ReceiveCommand-\	: INOUT STD_LOGIC;
		\ReceiveCommand+\	: INOUT STD_LOGIC;
		C_INIT_B	: INOUT STD_LOGIC;
		C_PROG_B	: INOUT STD_LOGIC);
END \xc6slx16-2csg324ces\;

ARCHITECTURE behavior OF \xc6slx16-2csg324ces\ IS
BEGIN
END behavior;
