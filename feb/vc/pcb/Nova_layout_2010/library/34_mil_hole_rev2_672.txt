(DEVICE FILE: 34_MIL_HOLE_REV2_672)

PACKAGE 34_MIL_HOLE_REV2_672
CLASS IC
PINCOUNT 1

PINORDER 34_MIL_HOLE_REV2_672 'TP-1'
FUNCTION 'TS-1' 34_MIL_HOLE_REV2_672 1

PACKAGEPROP PACKAGE_HEIGHT_MAX '1 MM'
PACKAGEPROP HEIGHT '1.0000000'

END
