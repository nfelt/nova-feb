--
-- VHDL Architecture nova_feb.trigger_behav.vhd
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 16:10:10 08/23/10
--
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;
use nova_feb.register_address_map.all;

entity trigger is
  generic (

    chan_l : integer);
  port(

    RAW_CHAN_NUMBER : in CHAN_NUM_TYPE;
    RAW_TIMESTAMP   : in unsigned (31 downto 0) := x"00000000";
    RAW_MAGNITUDE   : in MAGNITUDE_TYPE;

    THRESHOLD_TABLE_SEG : in MAGNITUDE_VECTOR_TYPE;
    DSP_EN              : in boolean := false;

    DATA_PKT_ENCODE :     unsigned(3 downto 0);
    DATA_PKT_START  :     unsigned(3 downto 0);
    DSP_CHAN_NUMBER : out CHAN_NUM_TYPE          := to_unsigned(chan_l, 5);
    DSP_TIMESTAMP   : out unsigned (31 downto 0) := x"00000000";
    DSP_MAGNITUDE   : out MAGNITUDE_TYPE         := x"000";
    DSP_STRB        : out boolean                := false;

    RESET  : in boolean   := false;
    CLK_64 : in std_logic := '1'
    );

-- Declarations

end trigger;

--
architecture behav of trigger is
  constant nsegment : integer := 8;
  type     DATA_DELAY_TYPE is array (128/NSEG downto 0)
    of unsigned(11 downto 0);
  signal DATA_DELAY : DATA_DELAY_TYPE := (others => (others => '0'));

  type CHAN_DATA_DCS_TYPE is array (NCHPSEG downto 0)
    of signed(12 downto 0);
  signal CHAN_DATA_DCS : CHAN_DATA_DCS_TYPE := (others => (others => '0'));

  signal CHAN_NUMBER        : unsigned(4 downto 0)  := to_unsigned(THRESHOLD_TABLE_SEG'right, 5);
--  signal CHAN_NUMBER   : unsigned(4 downto 0) := to_unsigned(chan_l,5);
--  signal CHAN_DATA_DCS : signed (12 downto 0)  := "0000000000000";
  signal TIMESTAMP          : unsigned(31 downto 0) := x"00000000";
  signal MAGNITUDE_FIXED    : MAGNITUDE_TYPE        := x"000";
  signal MAGNITUDE_TO_FLOAT : MAGNITUDE_TYPE        := x"000";
  signal MAGNITUDE_FLOAT    : MAGNITUDE_TYPE        := x"000";
  
begin

  DCS_FILTER : process (CLK_64) is
  begin
    if CLK_64'event and CLK_64 = '1' then
      CHAN_NUMBER <= RAW_CHAN_NUMBER;
      TIMESTAMP   <= RAW_TIMESTAMP;  --DELAY TIMESTAMP TO SYNC WITH OTHER DATA
      DATA_DELAY  <= DATA_DELAY(128/NSEG - 1 downto 0) & RAW_MAGNITUDE;
      -- FIR 32 CHANNELS USING (1 0 0 -1) "DCS FILTER"

      -- the channel DCS value is delayed by one channel sample to check that the 
      -- dcs trigger is the max dcs value rather than first over threshold
      -- this means there is one sample for all channels buffered to accomidate
      -- RAW_MAGNITUDE -DATA_DELAY(96/NSEG-1) is FIR (1 0 0 -1)

      CHAN_DATA_DCS <= CHAN_DATA_DCS(NCHPSEG - 1 downto 0) & (signed('0' & RAW_MAGNITUDE) - signed('0' & DATA_DELAY(96/NSEG-1)));
    end if;

  end process DCS_FILTER;

-- previously out of process
-- LOOK AT TIMING AND MAYBE PUT THIS IN ABOVE PROCESS
  select_data : process (CLK_64) is
  begin
    if CLK_64'event and CLK_64 = '1' then

      case DATA_PKT_START is
        when x"0" =>
          MAGNITUDE_FIXED    <= DATA_DELAY(32/NSEG-1);
          MAGNITUDE_TO_FLOAT <= DATA_DELAY(32/NSEG-2);
          
        when x"1" =>
          MAGNITUDE_FIXED    <= DATA_DELAY(64/NSEG-1);
          MAGNITUDE_TO_FLOAT <= DATA_DELAY(64/NSEG-2);
          
        when x"2" =>
          MAGNITUDE_FIXED    <= DATA_DELAY(96/NSEG-1);
          MAGNITUDE_TO_FLOAT <= DATA_DELAY(96/NSEG-2);
          
        when x"3" =>
          MAGNITUDE_FIXED    <= DATA_DELAY(128/NSEG-1);
          MAGNITUDE_TO_FLOAT <= DATA_DELAY(128/NSEG-2);
          
        when others =>
          MAGNITUDE_FIXED    <= DATA_DELAY(128/NSEG-1);
          MAGNITUDE_TO_FLOAT <= DATA_DELAY(128/NSEG-2);
          
      end case;

      case DATA_PKT_ENCODE is
        when DCS =>
          DSP_MAGNITUDE <= unsigned(CHAN_DATA_DCS(NCHPSEG)(11 downto 0));

        when FIXED =>
          DSP_MAGNITUDE <= MAGNITUDE_FIXED;

        when FLOAT =>
          DSP_MAGNITUDE <= MAGNITUDE_FLOAT;
          
        when others =>
          null;
      end case;

      --strobe when the delayed dcs value is over threshold and > the current DCS value

      DSP_STRB <= (CHAN_DATA_DCS(NCHPSEG) > signed('0' & THRESHOLD_TABLE_SEG(to_integer(CHAN_NUMBER)))
                   and CHAN_DATA_DCS(NCHPSEG) > CHAN_DATA_DCS(0)
                   and DSP_EN);
      DSP_CHAN_NUMBER <= CHAN_NUMBER;
      DSP_TIMESTAMP   <= TIMESTAMP;
    end if;
  end process select_data;

  FIX_TO_FLOAT_INST : entity NOVA_FEB.FIX_TO_FLOAT
    port map (
      MAGNITUDE_FIXED => MAGNITUDE_TO_FLOAT,
      MAGNITUDE_FLOAT => MAGNITUDE_FLOAT,
      RESET           => RESET,
      CLK_64          => CLK_64
      );

end architecture BEHAV;






