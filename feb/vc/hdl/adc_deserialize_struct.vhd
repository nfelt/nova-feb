--
-- VHDL Architecture nova_feb.adc_deserialize.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 11:43:21 01/22/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;
use nova_feb.register_address_map.all;

entity adc_deserialize is
  port(
    reset              : in  std_logic;  -- reset (active high)
    datain_p, datain_n : in  std_logic_vector(7 downto 0);  -- differential data inputs
    clkin_p, clkin_n   : in  std_logic;  -- differential clock input
    dummy_out          : out std_logic_vector(63 downto 0)  -- dummy outputs
    CLK_16             : in  std_logic;
    CLK_32             : in  std_logic;
    CLK_64             : in  std_logic;
    );

-- Declarations

end adc_deserialize;

--
architecture struct of adc_deserialize is

  component serdes_1_to_n_clk_ddr_s8_diff is
    generic
      (
        S : integer := 8
        ) ;  -- Parameter to set the serdes factor 1..8
    port (
      clkin_p         : in  std_logic;  -- Input from LVDS receiver pin
      clkin_n         : in  std_logic;  -- Input from LVDS receiver pin
      rxioclkp        : out std_logic;  -- IO Clock network
      rxioclkn        : out std_logic;  -- IO Clock network
      rx_serdesstrobe : out std_logic;  -- Parallel data capture strobe
      rx_bufg_x1      : out std_logic
      ) ;                               -- Global clock
  end component;

  component serdes_1_to_n_data_ddr_s8_diff is
    generic
      (
        S : integer := 8;  -- Parameter to set the serdes factor 1..8
        D : integer := 16
        ) ;                             -- Set the number of inputs and outputs
    port (
      use_phase_detector : in  std_logic;  -- '1' enables the phase detector logic if USE_PD = TRUE
      datain_p           : in  std_logic_vector(D-1 downto 0);  -- Input from LVDS receiver pin
      datain_n           : in  std_logic_vector(D-1 downto 0);  -- Input from LVDS receiver pin
      rxioclkp           : in  std_logic;  -- IO Clock network
      rxioclkn           : in  std_logic;  -- IO Clock network
      rxserdesstrobe     : in  std_logic;  -- Parallel data capture strobe
      reset              : in  std_logic;  -- Reset line
      gclk               : in  std_logic;  -- Global clock
      bitslip            : in  std_logic;  -- Bitslip control line
      data_out           : out std_logic_vector((D*S)-1 downto 0);  -- Output data
      debug_in           : in  std_logic_vector(1 downto 0);  -- Debug Inputs, set to '0' if not required
      debug              : out std_logic_vector((2*D)+6 downto 0)
      ) ;  -- Debug output bus, 2D+6 = 2 lines per input (from mux and ce) + 7, leave nc if debug not required
  end component;

-- constants for serdes factor and number of IO pins

  constant S  : integer := 8;           -- Set the serdes factor to 8
  constant D  : integer := 8;           -- Set the number of inputs and outputs
  constant DS : integer := (D*S)-1;  -- Used for bus widths = serdes factor * number of inputs - 1

  signal rst             : std_logic;
  signal rxd             : std_logic_vector(DS downto 0);  -- Data from serdeses
  signal rxr             : std_logic_vector(DS downto 0);  -- signalistered Data from serdeses
  signal state           : std_logic;
  signal bslip           : std_logic;
  signal count           : std_logic_vector(3 downto 0);
  signal rxioclkp        : std_logic;
  signal rxioclkn        : std_logic;
  signal rx_serdesstrobe : std_logic;
  signal rx_bufg_x1      : std_logic;

begin

  rst       <= reset;                   -- active high reset pin
  dummy_out <= rxr;

-- Clock Input. Generate ioclocks via BUFIO2

  inst_clkin : serdes_1_to_n_clk_ddr_s8_diff generic map(
    S => S)         
    port map (
      clkin_p         => clkin_p,
      clkin_n         => clkin_n,
      rxioclkp        => rxioclkp,
      rxioclkn        => rxioclkn,
      rx_serdesstrobe => rx_serdesstrobe,
      rx_bufg_x1      => rx_bufg_x1);

-- Data Inputs

  inst_datain : serdes_1_to_n_data_ddr_s8_diff generic map(
    S => S,
    D => D)
    port map (
      use_phase_detector => '1',        -- '1' enables the phase detector logic
      datain_p           => datain_p,
      datain_n           => datain_n,
      rxioclkp           => rxioclkp,
      rxioclkn           => rxioclkn,
      rxserdesstrobe     => rx_serdesstrobe,
      gclk               => rx_bufg_x1,
      bitslip            => bslip,
      reset              => rst,
      data_out           => rxd,
      debug_in           => "00",
      debug              => open);

  process (rx_bufg_x1, rst)             -- example bitslip logic, if required
  begin
    if rst = '1' then
      state <= '0';
      bslip <= '1';
      count <= "0000";
    elsif rx_bufg_x1'event and rx_bufg_x1 = '1' then
      if state = '0' then
        if rxd(63 downto 60) /= "0011" then
          bslip <= '1';                 -- bitslip needed
          state <= '1';
          count <= "0000";
        end if;
      elsif state = '1' then
        bslip <= '0';                   -- bitslip low
        count <= count + 1;
        if count = "1111" then
          state <= '0';
        end if;
      end if;
    end if;
  end process;

  process (rx_bufg_x1)                  -- process received data
  begin
    if rx_bufg_x1'event and rx_bufg_x1 = '1' then
      rxr <= rxd;
    end if;
  end process;

end arch_top_nto1_ddr_diff_rx;



end architecture struct;
