--
-- VHDL ARCHITECTURE NOVA_FEB.DSP_FILTER.DUMMI
--
-- CREATED:
--          BY - NATE.NATE (HEPLPC2)
--          AT - 16:10:10 08/23/10
--
-- USING MENTOR GRAPHICS HDL DESIGNER(TM) 2009.1 (BUILD 12)
--
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library UNISIM;
use UNISIM.all;
library HARVARD_STD;
use HARVARD_STD.LPPC_CUSTOM_FN_PKG.all;
library NOVA_FEB;
--USE NOVA_FEB.CUSTOM_FN_PKG.ALL;
use NOVA_FEB.REGISTER_ADDRESS_MAP.all;

entity select_event_fifo is
  port(
    EVENT_FIFO_SEL       : out unsigned (1 downto 0) := "00";
    EVENT_FIFO_EMPTY     : in  boolean;
    EVENT_FIFO_SEL_NEXT  : in  boolean               := false;
    EVENT_FIFO_SEL_TM    : in  boolean               := false;
    EVENT_FIFO_SEL_CLEAR : in  boolean               := false;
    EVENT_FIFO_SEL_ERROR : out boolean;
    CLK_3_2              : in  std_logic;
    CLK_32               : in  std_logic             := '1'
    );


end select_event_fifo;

--
architecture BEHAV of select_event_fifo is


  signal TOGG3_2                  : boolean                         := false;
  signal TOGG3_2_DEL1             : boolean                         := false;
  signal EVENT_FIFO_SEL_NEXT_DEL1 : boolean                         := false;
  signal TOGG3_2_DEL2             : boolean                         := false;
  signal EVENT_FIFO_SEL_NEXT_DEL2 : boolean                         := false;
  signal START_SCAN_PTR           : unsigned (1 downto 0);
  signal EVENT_FIFO_SEL_INT       : unsigned (1 downto 0)           := "00";
  signal EVENT_FIFO_SEL_TM_HOLD   : BOOLEAN_VECTOR_TYPE(3 downto 0) := (others => false);
  
begin

  TOGG3_2_TIMING : process (CLK_3_2) is
  begin
    if CLK_3_2'event and CLK_3_2 = '1' then
      TOGG3_2 <= not TOGG3_2;
      if EVENT_FIFO_SEL_CLEAR then
        EVENT_FIFO_SEL_TM_HOLD <= (others => false);
        EVENT_FIFO_SEL_ERROR   <= EVENT_FIFO_SEL_TM_HOLD(to_integer(EVENT_FIFO_SEL_INT));
      elsif EVENT_FIFO_SEL_TM then
        EVENT_FIFO_SEL_TM_HOLD(to_integer(EVENT_FIFO_SEL_INT)) <= true;
        --ERROR IF SECOND TM FOR ONE FIFO BEFORE OTHER FIFO(S') TM.
        EVENT_FIFO_SEL_ERROR                                   <= EVENT_FIFO_SEL_TM_HOLD(to_integer(EVENT_FIFO_SEL_INT));
      end if;
    end if;
  end process TOGG3_2_TIMING;

  SCAN_EVENT_FIFOS : process (CLK_32) is
    variable SCAN_PERIODIC  : boolean := false;
    variable SCAN_ROUND_RBN : boolean := false;
  begin
    if CLK_32'event and CLK_32 = '1' then
      --CLK_3_2 NOT PHASE ALLIGNED
      TOGG3_2_DEL1             <= TOGG3_2;
      TOGG3_2_DEL2             <= TOGG3_2_DEL1;
      EVENT_FIFO_SEL_NEXT_DEL1 <= EVENT_FIFO_SEL_NEXT;
      EVENT_FIFO_SEL_NEXT_DEL2 <= EVENT_FIFO_SEL_NEXT_DEL1;

      SCAN_PERIODIC  := TOGG3_2_DEL1 /= TOGG3_2_DEL2;
      SCAN_ROUND_RBN := EVENT_FIFO_SEL_NEXT_DEL1 and not EVENT_FIFO_SEL_NEXT_DEL2;
      -- DATA TX IS ACTIVE WHEN FIFO IS NOT EMPTY
      --SIGNAL ROUND ROBIN SCAN AT END OF ACTIVE PACKET
      if not EVENT_FIFO_EMPTY
        and not EVENT_FIFO_SEL_TM_HOLD(to_integer(EVENT_FIFO_SEL_INT))
        and not SCAN_ROUND_RBN then
        START_SCAN_PTR <= EVENT_FIFO_SEL_INT;
      elsif (EVENT_FIFO_SEL_INT /= START_SCAN_PTR)
        --NEED PERIOD AND RR, PULSES MAY NOT BE ALLIGNED DUE TO PHASE OF CLK 3_2
        or SCAN_PERIODIC
        or SCAN_ROUND_RBN then
        EVENT_FIFO_SEL_INT <= EVENT_FIFO_SEL_INT +1;
      end if;
      --if one event fifo is being "held" then there must be at least
      --the time marker packets in the other event fifos
      --the ptr can't make a round trip back to a "held" fifo without
      -- stopping at the other fifos' time markers
    end if;
  end process SCAN_EVENT_FIFOS;

  EVENT_FIFO_SEL <= EVENT_FIFO_SEL_INT;

end BEHAV;
