--
-- VHDL ARCHITECTURE FEB_P2_LIB.DCM_COMM_EMU_CLK.STRUCT
--
-- CREATED:
--          BY - NATE.UNKNOWN (HEPLPC2)
--          AT - 16:38:44 11/25/2007
--
-- USING MENTOR GRAPHICS HDL DESIGNER(TM) 2006.1 (BUILD 72)
-------------------------------------------------------------------------------
-- GENERATE ALL CLOCKING SIGNALS BASED ON BOARD XTAL AND
-- FNAL DCM LVDS COMMUNICATION CLOCK.
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library UNISIM;
use UNISIM.all;
library HARVARD_STD;
use HARVARD_STD.LPPC_CUSTOM_FN_PKG.all;
library NOVA_FEB;

entity DCM_COMM_EMU_CLK is
  port(
    DCM_CLK_IN_P  : in  std_logic;
    DCM_CLK_IN_N  : in  std_logic;
    LOCKED        : out boolean;
    ENABLE_DAQ    : in  boolean;
    USE_CLK16_INV : in  boolean;
    T_IUS         : out boolean;
    T_67S         : out boolean;
    CLK_3_2       : out std_logic;
    CLK_4         : out std_logic;
    CLK_16        : out std_logic;
    CLK_32        : out std_logic;
    CLK_64        : out std_logic;
    CLK_128       : out std_logic;
    ADC_CLK0_OUT  : out std_logic;
    ADC_CLK1_OUT  : out std_logic;
    ASIC_CLK_OUT  : out std_logic;
    USB_IFCLK     : out std_logic;
    POWERUP_RESET : out boolean := true
    );

-- DECLARATIONS

end DCM_COMM_EMU_CLK;

--
architecture STRUCT of DCM_COMM_EMU_CLK is

  component FEB_CLK_6
    port
      (                                 -- CLOCK IN PORTS
        CLK_IN1_P : in  std_logic;
        CLK_IN1_N : in  std_logic;
        CLKFB_IN  : in  std_logic;
        -- CLOCK OUT PORTS
        CLK_OUT1  : out std_logic;
        CLK_OUT2  : out std_logic;
        CLK_OUT3  : out std_logic;
        CLK_OUT4  : out std_logic;
        CLK_OUT5  : out std_logic;
        CLK_OUT6  : out std_logic;
        CLKFB_OUT : out std_logic;
        -- STATUS AND CONTROL SIGNALS
        LOCKED    : out std_logic
        );
  end component;

  component BUFIO2 is
    generic (
      DIVIDE        : integer;
      DIVIDE_BYPASS : boolean;
      I_INVERT      : boolean;
      USE_DOUBLER   : boolean
      );
    port(
      DIVCLK       : out std_logic;
      IOCLK        : out std_logic;
      SERDESSTROBE : out std_logic;
      I            : in  std_logic
      );
  end component;

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;

  component BUFGCE
    port(
      O  : out std_ulogic;
      CE : in  std_ulogic;
      I  : in  std_ulogic
      );
  end component;

  component ODDR2
    generic (
      DDR_ALIGNMENT : string;
      INIT          : bit;
      SRTYPE        : string
      );
    port (
      Q  : out std_logic;
      C0 : in  std_logic;
      C1 : in  std_logic;
      CE : in  std_logic;
      D0 : in  std_logic;
      D1 : in  std_logic;
      R  : in  std_logic;
      S  : in  std_logic
      );
  end component;

  signal PLL_CLK_32      : std_logic := '0';
  signal PLL_CLK_64      : std_logic := '0';
  signal PLL_CLK_16      : std_logic := '0';
  signal PLL_CLK_16_180  : std_logic := '0';
  signal CLK_4_INT       : std_logic := '0';
  signal PLL_CLK_4       : std_logic := '0';
  signal PLL_CLK_128     : std_logic := '0';
  signal CLK_16_180_INTR : std_logic := '0';
  signal CLK_16_INTR     : std_logic := '0';
  signal LOCKED_INT      : std_logic := '0';
  signal ACLK_R_EDGE     : std_logic := '0';
  signal ACLK_F_EDGE     : std_logic := '0';
  signal PLL_CLKFB_IN    : std_logic := '0';
  signal PLL_CLKFB_OUT   : std_logic := '0';
  signal TICK_COUNTER    : unsigned(27 downto 0);


begin
  CLK_4  <= CLK_4_INT;
  CLK_16 <= CLK_16_INTR;

  CLK_PLL_COREGEN : FEB_CLK_6
    port map
    (
      CLK_IN1_P => DCM_CLK_IN_P,
      CLK_IN1_N => DCM_CLK_IN_N,
      CLKFB_IN  => PLL_CLKFB_IN,
      CLK_OUT1  => PLL_CLK_32,
      CLK_OUT2  => PLL_CLK_64,
      CLK_OUT3  => PLL_CLK_16,
      CLK_OUT4  => PLL_CLK_16_180,
      CLK_OUT5  => PLL_CLK_4,
      CLK_OUT6  => PLL_CLK_128,
      CLKFB_OUT => PLL_CLKFB_OUT,

      LOCKED => LOCKED_INT
      );

  --ASIC CHANGED TO CLAOCK ALWAYS
  ACLK_R_EDGE <= not BOOL2SL(USE_CLK16_INV);  --AND  BOOL2SL(ENABLE_DAQ);
  ACLK_F_EDGE <= BOOL2SL(USE_CLK16_INV);      --AND  BOOL2SL(ENABLE_DAQ);

  ASIC_CLK_ODDR2 : ODDR2
    generic map(
      DDR_ALIGNMENT => "NONE",
      INIT          => '0',
      SRTYPE        => "SYNC") 
    port map (
      Q  => ASIC_CLK_OUT,
      C0 => CLK_16_INTR,
      C1 => CLK_16_180_INTR,
      CE => '1',
      D0 => ACLK_R_EDGE,
      D1 => ACLK_F_EDGE,
      R  => '0',
      S  => '0'
      );

  ADC_CLK0_ODDR2 : ODDR2
    generic map(
      DDR_ALIGNMENT => "NONE",
      INIT          => '0',
      SRTYPE        => "SYNC") 
    port map (
      Q  => ADC_CLK0_OUT,
      C0 => CLK_16_INTR,
      C1 => CLK_16_180_INTR,
      CE => '1',
      D0 => ACLK_R_EDGE,
      D1 => ACLK_F_EDGE,
      R  => '0',
      S  => '0'
      );

  ADC_CLK1_ODDR2 : ODDR2
    generic map(
      DDR_ALIGNMENT => "NONE",
      INIT          => '0',
      SRTYPE        => "SYNC") 
    port map (
      Q  => ADC_CLK1_OUT,
      C0 => CLK_16_INTR,
      C1 => CLK_16_180_INTR,
      CE => '1',
      D0 => ACLK_R_EDGE,
      D1 => ACLK_F_EDGE,
      R  => '0',
      S  => '0'
      );

  IFCLK_ODDR2 : ODDR2
    generic map(
      DDR_ALIGNMENT => "NONE",
      INIT          => '0',
      SRTYPE        => "SYNC") 
    port map (
      Q  => USB_IFCLK,
      C0 => CLK_16_INTR,
      C1 => CLK_16_180_INTR,
      CE => '1',
      D0 => '1',
      D1 => '0',
      R  => '0',
      S  => '0'
      );

  CLK_32_BUFG : BUFG
    port map (
      O => CLK_32,
      I => PLL_CLK_32
      );

  CLK_64_BUFG : BUFG
    port map (
      O => CLK_64,
      I => PLL_CLK_64
      );

  CLK_16_180_BUFG : BUFG
    port map (
      O => CLK_16_180_INTR,
      I => PLL_CLK_16_180
      );

  CLK_16_BUFG : BUFG
    port map (
      O => CLK_16_INTR,
      I => PLL_CLK_16
      );

  CLK_4_BUFG : BUFG
    port map (
      O => CLK_4_INT,
      I => PLL_CLK_4
      );
  CLK_128_BUFG : BUFG
    port map (
      O => CLK_128,
      I => PLL_CLK_128
      );
  CLK_FB_BUFG : BUFG
    port map (
      O => PLL_CLKFB_IN,
      I => PLL_CLKFB_OUT
      );

  TIME_TICK_GEN : process (CLK_4_INT) is
  begin
    if CLK_4_INT'event and CLK_4_INT = '1' then
      TICK_COUNTER <= TICK_COUNTER + 1;
    end if;
  end process TIME_TICK_GEN;
  T_IUS <= TICK_COUNTER(3) = '1';
  T_67S <= TICK_COUNTER(27) = '1';

  POWERUP_RESET_GEN : process (CLK_4_INT) is
  begin
    if CLK_4_INT'event and CLK_4_INT = '1' then
      if LOCKED_INT = '1' then
        POWERUP_RESET <= false;
      end if;
    end if;
  end process POWERUP_RESET_GEN;
  LOCKED <= LOCKED_INT = '1';
end architecture STRUCT;




