--
-- VHDL Architecture feb_p2_lib.ASIC_CTRL.BEHAV
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 17:00:32 02/26/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;
use nova_feb.register_address_map.all;

entity ASIC_CTRL is
  port(
    BEEBUS_ADDR    : in    unsigned (15 downto 0);
    CLK            : in    std_logic;
    BEEBUS_DATA    : inout unsigned (15 downto 0);
    BEEBUS_READ    : in    boolean;
    BEEBUS_STRB    : in    boolean;
    SET_ASIC       : in    boolean;
    ASIC_CHIPRESET : out   boolean;
    ASIC_SHIFTIN   : out   std_logic;
    ASIC_SHIFTOUT  : in    std_logic;
    ASIC_SRCK      : out   std_logic;
    ASIC_IBIAS     : out   boolean;
    ASIC_SET_ERROR : out   boolean
    );

end ASIC_CTRL;

architecture BEHAV of ASIC_CTRL is
 -- add extra MSb to shift readback by one
  signal ASIC_SHIFT_REGISTER  : std_logic_vector (221 downto 1) := '0'& x"0820820820820820820820820820820820820820820820829B76F80";
  signal ASIC_IBIAS_COUNTER   : unsigned(23 downto 0)           := x"000000";
  signal ASIC_SHIFT_COUNT     : unsigned(7 downto 0)            := "11011100";
  signal ASIC_COMMAND_SENT    : boolean                         := false;
  signal ASIC_COMMAND_SENT_c1 : boolean                         := false;
  signal ASIC_COMMAND_SENT_c2 : boolean                         := false;
  signal ASIC_SRCK_int        : std_logic                       := '0';
  signal BEEBUS_READ_del      : boolean                         := false;
  signal BEEBUS_DATA_int      : unsigned(15 downto 0)           := x"0000";

  
begin
  BEEBUS_DATA <= BEEBUS_DATA_int when BEEBUS_READ_del else "ZZZZZZZZZZZZZZZZ";

  UPDATE_ASIC_BITS : process (CLK) is
    variable i : integer := 0;
  begin
    if CLK'event and CLK = '1' then
      BEEBUS_READ_del      <= false;
      ASIC_COMMAND_SENT_c1 <= ASIC_COMMAND_SENT;
      ASIC_COMMAND_SENT_c2 <= ASIC_COMMAND_SENT_c1;
      ASIC_CHIPRESET       <= ASIC_COMMAND_SENT_c1 and not ASIC_COMMAND_SENT_c2;
      BEEBUS_READ_del      <= false;

      if not ASIC_COMMAND_SENT then
        ASIC_SRCK_int  <= not ASIC_SRCK_int;
        ASIC_SET_ERROR <= BEEBUS_STRB or BEEBUS_READ;
        if ASIC_SRCK_int = '1' then
          ASIC_SHIFT_REGISTER <= ASIC_SHIFTOUT & ASIC_SHIFT_REGISTER(221 downto 2);
          ASIC_SHIFT_COUNT    <= ASIC_SHIFT_COUNT+1;
        end if;
      elsif SET_ASIC then
        ASIC_SHIFT_COUNT <= "00000000";
        ASIC_SRCK_int    <= '0';
        ASIC_SET_ERROR   <= BEEBUS_STRB or BEEBUS_READ;
      else
        case BEEBUS_ADDR is
          when SPARE_ADDR =>
            if BEEBUS_STRB then
              ASIC_SHIFT_REGISTER(6 downto 1) <= std_logic_vector(BEEBUS_DATA(5 downto 0));
            end if;
            if BEEBUS_READ then
              BEEBUS_DATA_int <= "0000000000" & unsigned(ASIC_SHIFT_REGISTER(6 downto 1));
              BEEBUS_READ_del <= true;
            end if;
          when VTSEL_ADDR =>
            if BEEBUS_STRB then
              ASIC_SHIFT_REGISTER(9 downto 7) <= std_logic_vector(BEEBUS_DATA(2 downto 0));
            end if;
            if BEEBUS_READ then
              BEEBUS_READ_del <= true;
              BEEBUS_DATA_int <= "0000000000000" & unsigned(ASIC_SHIFT_REGISTER(9 downto 7));
            end if;
          when REFSEL_ADDR =>
            if BEEBUS_STRB then
              ASIC_SHIFT_REGISTER(13 downto 10) <= std_logic_vector(BEEBUS_DATA(3 downto 0));
            end if;
            if BEEBUS_READ then
              BEEBUS_READ_del <= true;
              BEEBUS_DATA_int <= "000000000000" & unsigned(ASIC_SHIFT_REGISTER(13 downto 10));
            end if;
          when ISEL_ADDR =>
            if BEEBUS_STRB then
              ASIC_SHIFT_REGISTER(16 downto 14) <= std_logic_vector(BEEBUS_DATA(2 downto 0));
            end if;
            if BEEBUS_READ then
              BEEBUS_READ_del <= true;
              BEEBUS_DATA_int <= "0000000000000" & unsigned(ASIC_SHIFT_REGISTER(16 downto 14));
            end if;
          when BWSEL_ADDR =>
            if BEEBUS_STRB then
              ASIC_SHIFT_REGISTER(20 downto 17) <= std_logic_vector(BEEBUS_DATA(3 downto 0));
            end if;
            if BEEBUS_READ then
              BEEBUS_READ_del <= true;
              BEEBUS_DATA_int <= "000000000000" & unsigned(ASIC_SHIFT_REGISTER(20 downto 17));
            end if;
          when GSEL_ADDR =>
            if BEEBUS_STRB then
              ASIC_SHIFT_REGISTER(23 downto 21) <= std_logic_vector(BEEBUS_DATA(2 downto 0));
            end if;
            if BEEBUS_READ then
              BEEBUS_READ_del <= true;
              BEEBUS_DATA_int <= "0000000000000" & unsigned(ASIC_SHIFT_REGISTER(23 downto 21));
            end if;
          when TFSEL_ADDR =>
            if BEEBUS_STRB then
              ASIC_SHIFT_REGISTER(26 downto 24) <= std_logic_vector(BEEBUS_DATA(2 downto 0));
            end if;
            if BEEBUS_READ then
              BEEBUS_READ_del <= true;
              BEEBUS_DATA_int <= "0000000000000" & unsigned(ASIC_SHIFT_REGISTER(26 downto 24));
            end if;
          when MUX2TO1_ADDR =>
            if BEEBUS_STRB then
              ASIC_SHIFT_REGISTER(27) <= std_logic(BEEBUS_DATA(0));
            end if;
            if BEEBUS_READ then
              BEEBUS_READ_del <= true;
              BEEBUS_DATA_int <= "000000000000000" & ASIC_SHIFT_REGISTER(27);
            end if;
          when MUX8TO1_ADDR =>
            if BEEBUS_STRB then
              ASIC_SHIFT_REGISTER(28) <= std_logic(BEEBUS_DATA(0));
            end if;
            if BEEBUS_READ then
              BEEBUS_READ_del <= true;
              BEEBUS_DATA_int <= "000000000000000" & ASIC_SHIFT_REGISTER(28);
            end if;
          when others => null;
        end case;

        for i in 0 to 31 loop
          if BEEBUS_ADDR = OFFS_ADDR & to_unsigned(i, 5) then
            if BEEBUS_STRB then
              ASIC_SHIFT_REGISTER(6*i+33 downto 6*i+29) <= std_logic_vector(BEEBUS_DATA(4 downto 0));
            end if;
            if BEEBUS_READ then
              BEEBUS_READ_del <= true;
              BEEBUS_DATA_int <= "00000000000" & unsigned(ASIC_SHIFT_REGISTER(6*i+33 downto 6*i+29));
            end if;
          end if;
          if BEEBUS_ADDR = MASK_ADDR & to_unsigned(i, 5) then
            if BEEBUS_STRB then
              ASIC_SHIFT_REGISTER(6*i+34) <= BEEBUS_DATA(0);
            end if;
            if BEEBUS_READ then
              BEEBUS_READ_del <= true;
              BEEBUS_DATA_int <= "000000000000000" & ASIC_SHIFT_REGISTER(6*i+34);
            end if;
          end if;
        end loop;
        
      end if;
    end if;
  end process UPDATE_ASIC_BITS;

  
  update_asic : process (ASIC_SRCK_int, ASIC_SHIFT_REGISTER(1), ASIC_COMMAND_SENT_c1,
                         ASIC_COMMAND_SENT_c2, ASIC_SHIFT_COUNT) is
  begin
    ASIC_COMMAND_SENT <= ASIC_SHIFT_COUNT = "11011100";
    ASIC_SHIFTIN      <= ASIC_SHIFT_REGISTER(1);
    ASIC_SRCK         <= ASIC_SRCK_int;
  end process update_asic;

end architecture BEHAV;

