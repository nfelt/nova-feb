--
-- VHDL Architecture nova_feb.dsp_filter.dummi
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 16:10:10 08/23/10
--
-- using Mentor Graphics HDL Designer(TM) 2009.1 (Build 12)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;
--use nova_feb.custom_fn_pkg.all;
use NOVA_FEB.REGISTER_ADDRESS_MAP.all;

entity data_processing is
  port(
    BEEBUS_READ : in    boolean                := false;
    BEEBUS_STRB : in    boolean                := false;
    BEEBUS_DATA : inout unsigned (15 downto 0) := x"0000";
    BEEBUS_ADDR : in    unsigned (15 downto 0) := x"0000";

    RAW_CHAN_NUMBER : in CHAN_NUM_VECTOR_TYPE;
    RAW_TIMESTAMP   : in TIMESTAMP_TYPE;
    RAW_MAGNITUDE   : in MAGNITUDE_VECTOR_TYPE;

    INTERNAL_TRIGGER : in boolean := false;

    EVENT_FIFO_DOUT      : out unsigned (7 downto 0) := x"00";
    EVENT_FIFO_EMPTY     : out boolean               := false;
    EVENT_FIFO_FULL      : out boolean               := false;
    EVENT_FIFO_RSTRB     : in  boolean               := false;
    EVENT_FIFO_SEL_NEXT  : in  boolean               := false;
    EVENT_FIFO_SEL_TM    : in  boolean               := false;
    EVENT_FIFO_SEL_CLEAR : in  boolean               := false;
    EVENT_FIFO_SEL_ERROR : out boolean               := false;
    THEAD_OVFLW_ERR      : out boolean;
    TDATA_OVFLW_ERR      : out boolean;
    EVENT_OVFLW_ERR      : out boolean;
    DATA_DROP_ERR        : out boolean;

    TIMING_PKT_FEB_STATUS : in  std_logic_vector(7 downto 0);
    DCM_SYNC              : in  boolean   := false;
    ENABLE_DAQ            : in  boolean   := false;
    ENABLE_TM_PKT         : in  boolean   := false;
    N_DATA_PKT_WORDS      : out unsigned(4 downto 0);
    DATA_MEM_FULL_LATCH   : out boolean   := false;
    RESET                 : in  boolean   := false;
    CLK_3_2               : in  std_logic;
    CLK_16                : in  std_logic;
    CLK_64                : in  std_logic := '1';
    CLK_32                : in  std_logic := '1';
    CLK_128               : in  std_logic := '1';
    TICK_1US              : in  boolean   := false
    );

-- Declarations

end data_processing;

--
architecture behav of data_processing is

  type TABLE_TYPE is array (31 downto 0)
    of unsigned(11 downto 0);
  signal THRESHOLD_TABLE : MAGNITUDE_VECTOR_TYPE(31 downto 0);

  signal SEL_MAGNITUDE : MAGNITUDE_TYPE;

  constant ADC_SEG_INDEX : integer := 2;  -- NEAR DETECTOR 4, FAR 1

  signal BEEBUS_READ_del : boolean               := false;
  signal BEEBUS_DATA_int : unsigned(15 downto 0) := x"0000";

  signal N_DATA_PKT_WORDS_int   : unsigned (4 downto 0) := "00010";
  signal N_DATA_PKT_WORDS_HLDOF : unsigned(4 downto 0);
  signal MP_NSAMPLES            : unsigned (4 downto 0) := "00000";
  signal DATA_PKT_ENCODE_legacy : unsigned (3 downto 0) := x"0";
  signal DATA_PKT_ENCODE_int    : unsigned (3 downto 0) := x"1";
  signal DATA_PKT_ENCODE        : unsigned (3 downto 0);
  signal DATA_PKT_DAQID         : unsigned (4 downto 0) := "00000";
  signal DATA_PKT_START         : unsigned (3 downto 0) := x"0";
  signal DATA_PKT_START_int     : unsigned (3 downto 0) := x"0";
  signal LEGACY_MODE            : boolean;

  signal TIME_MARKER_RATE : unsigned (7 downto 0) := x"00";
  signal TIME_MARKER_CNT  : unsigned (7 downto 0) := x"00";

  signal DATA_REGULATOR_REG : unsigned (18 downto 0) := "000" & x"0000";
  signal DATA_REGULATOR_CNT : unsigned (18 downto 0) := "000" & x"0000";
  signal SEND_TM            : boolean                := false;
  signal SEND_TM_del1       : boolean                := false;
  signal SEND_TM_del2       : boolean                := false;

  signal DSP_TRIGGER     : BOOLEAN_VECTOR_TYPE(RAW_MAGNITUDE'range);
  signal DSP_CHAN_NUMBER : CHAN_NUM_VECTOR_TYPE(RAW_MAGNITUDE'range);
  signal DSP_TIMESTAMP   : TIMESTAMP_VECTOR_TYPE(RAW_MAGNITUDE'range);
  signal DSP_MAGNITUDE   : MAGNITUDE_VECTOR_TYPE(RAW_MAGNITUDE'range);

  signal TRIG_HOLDOFF               : BOOLEAN_VECTOR_TYPE(RAW_MAGNITUDE'range);
  signal TRIG_HOLDOFF_TIME          : unsigned (3 downto 0) := x"8";
  signal DATA_MEM_FULL_LATCH_int    : boolean;
  signal DATA_MEM_FULL_LATCH_VECTOR : BOOLEAN_VECTOR_TYPE(RAW_MAGNITUDE'range);
  signal EVENT_FIFO_DOUT_VECTOR     : EVENT_FIFO_VECTOR_TYPE(RAW_MAGNITUDE'range);
  signal EVENT_FIFO_EMPTY_VECTOR    : BOOLEAN_VECTOR_TYPE(RAW_MAGNITUDE'range);
  signal EVENT_FIFO_FULL_VECTOR     : BOOLEAN_VECTOR_TYPE(RAW_MAGNITUDE'range);
  signal EVENT_FIFO_RSTRB_VECTOR    : BOOLEAN_VECTOR_TYPE(RAW_MAGNITUDE'range);
  signal EVENT_FIFO_EMPTY_INT       : boolean;
  signal EVENT_FIFO_SEL             : unsigned (1 downto 0) := "00";
  signal THEAD_OVFLW_ERR_VECTOR     : BOOLEAN_VECTOR_TYPE(RAW_MAGNITUDE'range);
  signal TDATA_OVFLW_ERR_VECTOR     : BOOLEAN_VECTOR_TYPE(RAW_MAGNITUDE'range);
  signal EVENT_OVFLW_ERR_VECTOR     : BOOLEAN_VECTOR_TYPE(RAW_MAGNITUDE'range);
  signal DATA_DROP_ERR_VECTOR       : BOOLEAN_VECTOR_TYPE(RAW_MAGNITUDE'range);

  signal CHAN_EN   : std_logic_vector (31 downto 0) := x"FFFFFFFF";
  signal DAQ_MODE  : unsigned(15 downto 0)          := x"0000";
  signal OSCOPE_EN : boolean                        := false;
  signal DSP_EN    : boolean                        := false;

  signal OSCOPE_STRB : boolean := false;
  signal DSP_STRB    : BOOLEAN_VECTOR_TYPE(RAW_MAGNITUDE'range);

begin
  BEEBUS_DATA <= BEEBUS_DATA_int when BEEBUS_READ_del else "ZZZZZZZZZZZZZZZZ";

  set_select_clk : process (clk_16)
  begin  -- process set_select_clk
    if rising_edge(clk_16) then
      -- # SAMPLES = 0 - USE LEGACY MODE, 2 DATA IN PKT HOLDOF 1, REPLACE LAST W/CAFE
      -- disable multipoint mode
      -- LEGACY_MODE            <= true;       --MP_NSAMPLES = "00000";
      -- enable multipoint mode
      if MP_NSAMPLES = "00000" then
        LEGACY_MODE            <= true;
        N_DATA_PKT_WORDS_int   <= "00010";
        N_DATA_PKT_WORDS_HLDOF <= "00001";
        DATA_PKT_ENCODE        <= DATA_PKT_ENCODE_LEGACY;
        DATA_PKT_START         <= x"1";
      else
        LEGACY_MODE            <= false;
        N_DATA_PKT_WORDS_int   <= MP_NSAMPLES;
        N_DATA_PKT_WORDS_HLDOF <= MP_NSAMPLES;
        DATA_PKT_ENCODE        <= DATA_PKT_ENCODE_int;
        DATA_PKT_START         <= DATA_PKT_START_int;
      end if;
      --when in legacy mode select either raw data or DCS, depending on dso mode
      DATA_PKT_ENCODE_LEGACY <= ("000"& bool2sl(DAQ_MODE = OSCILLOSCOPE_MODE));
      
    end if;
  end process set_select_clk;

  process (CLK_16)
  begin
    if CLK_16'event and CLK_16 = '1' then
      BEEBUS_READ_del <= false;

      case BEEBUS_ADDR is

        when DAQ_MODE_ADDR =>
          if BEEBUS_READ then
            BEEBUS_DATA_int <= DAQ_MODE;
            BEEBUS_READ_del <= true;
          elsif BEEBUS_STRB then
            DAQ_MODE <= BEEBUS_DATA;
          end if;

        when (THRESHOLD_ADDR & "-----") =>
          if BEEBUS_READ then
            BEEBUS_READ_del <= true;
            BEEBUS_DATA_int <= x"0" & THRESHOLD_TABLE (to_integer(BEEBUS_ADDR(5 downto 0)));
          elsif BEEBUS_STRB then
            THRESHOLD_TABLE (to_integer(BEEBUS_ADDR(5 downto 0))) <= (BEEBUS_DATA(11 downto 0));
          end if;

        when DATA_REGULATOR_ADDR =>
          if BEEBUS_READ then
            BEEBUS_READ_del <= true;
            BEEBUS_DATA_int <= DATA_REGULATOR_REG(18 downto 3);
          elsif BEEBUS_STRB then
            DATA_REGULATOR_REG <= BEEBUS_DATA & "000";
          end if;


        when TRIG_HOLDOFF_TIME_ADDR =>
          if BEEBUS_READ then
            BEEBUS_READ_del <= true;
            BEEBUS_DATA_int <= "000000000000" & unsigned(TRIG_HOLDOFF_TIME);
          elsif BEEBUS_STRB then
            TRIG_HOLDOFF_TIME <= BEEBUS_DATA(3 downto 0);
          end if;

        when CHAN_EN_U_ADDR =>
          if BEEBUS_READ then
            BEEBUS_READ_del <= true;
            BEEBUS_DATA_int <= unsigned(CHAN_EN(31 downto 16));
          elsif BEEBUS_STRB then
            CHAN_EN(31 downto 16) <= std_logic_vector(BEEBUS_DATA);
          end if;

        when CHAN_EN_L_ADDR =>
          if BEEBUS_READ then
            BEEBUS_READ_del <= true;
            BEEBUS_DATA_int <= unsigned(CHAN_EN(15 downto 0));
          elsif BEEBUS_STRB then
            CHAN_EN(15 downto 0) <= std_logic_vector(BEEBUS_DATA);
          end if;

        when TIMING_PKT_RATE_ADDR =>
          if BEEBUS_READ then
            BEEBUS_READ_del <= true;
            BEEBUS_DATA_int <= x"00" & TIME_MARKER_RATE;
          elsif BEEBUS_STRB then
            TIME_MARKER_RATE <= BEEBUS_DATA(7 downto 0);
          end if;

        when MP_NSAMPLES_ADDR =>
          if BEEBUS_READ then
            BEEBUS_DATA_int <= "00000000000"& MP_NSAMPLES;
            BEEBUS_READ_del <= true;
          elsif BEEBUS_STRB then
            MP_NSAMPLES <= BEEBUS_DATA(4 downto 0);
          end if;
          
        when DATA_PKT_ENCODE_ADDR =>
          if BEEBUS_READ then
            BEEBUS_DATA_int <= x"000"& DATA_PKT_ENCODE_int;
            BEEBUS_READ_del <= true;
          elsif BEEBUS_STRB then
            DATA_PKT_ENCODE_int <= BEEBUS_DATA(3 downto 0);
          end if;
          
        when DATA_PKT_DAQID_ADDR =>
          if BEEBUS_READ then
            BEEBUS_DATA_int <= "00000000000"& DATA_PKT_DAQID;
            BEEBUS_READ_del <= true;
          elsif BEEBUS_STRB then
            DATA_PKT_DAQID <= BEEBUS_DATA(4 downto 0);
          end if;
          
        when DATA_PKT_START_ADDR =>
          if BEEBUS_READ then
            BEEBUS_DATA_int <= x"000"& DATA_PKT_START_int;
            BEEBUS_READ_del <= true;
          elsif BEEBUS_STRB then
            DATA_PKT_START_int <= BEEBUS_DATA(3 downto 0);
          end if;
          
        when others => null;
      end case;

    end if;
  end process;

  SEND_TIME_MARKER : process (CLK_64) is
  begin
    if CLK_64'event and CLK_64 = '1' then
      SEND_TM_del1 <= false;
      SEND_TM_del2 <= SEND_TM_del1;
      SEND_TM      <= SEND_TM_del2;
      if DCM_SYNC then
        TIME_MARKER_CNT <= x"01";
      elsif TICK_1US then
        if (TIME_MARKER_CNT = TIME_MARKER_RATE)
          and (TIME_MARKER_RATE /= x"00") then
          TIME_MARKER_CNT <= x"01";
          SEND_TM_del1    <= ENABLE_TM_PKT;
        else
          TIME_MARKER_CNT <= TIME_MARKER_CNT + 1;
        end if;
      end if;
    end if;
  end process SEND_TIME_MARKER;


  SEND_OSCOPE_DATA : process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '1' then
      OSCOPE_EN <= (INTERNAL_TRIGGER or OSCOPE_EN)
                   and (DAQ_MODE = OSCILLOSCOPE_MODE)
                   and not (DATA_REGULATOR_CNT = "000" & x"0001")
                   and not DATA_MEM_FULL_LATCH_int;
      
      if INTERNAL_TRIGGER then
        DATA_REGULATOR_CNT <= DATA_REGULATOR_REG;
      elsif DATA_REGULATOR_CNT /= "000" & x"0000" then
        DATA_REGULATOR_CNT <= DATA_REGULATOR_CNT - 1;
      end if;
      
    end if;
  end process SEND_OSCOPE_DATA;
  OSCOPE_STRB <= OSCOPE_EN and not DATA_MEM_FULL_LATCH_int;
  DSP_EN      <= DAQ_MODE = DCS_DSP_MODE;

  ADC_SEG_PROC_GEN : for ADC_SEG_INDEX in RAW_MAGNITUDE'range generate
    TRIGGER_INST : entity nova_feb.trigger
      generic map (
        chan_l => ADC_SEG_INDEX*NCHPSEG
        )
      port map (

        RAW_CHAN_NUMBER => RAW_CHAN_NUMBER(ADC_SEG_INDEX),
        RAW_TIMESTAMP   => RAW_TIMESTAMP,
        RAW_MAGNITUDE   => RAW_MAGNITUDE(ADC_SEG_INDEX),

        THRESHOLD_TABLE_SEG => THRESHOLD_TABLE(ADC_SEG_INDEX*NCHPSEG + NCHPSEG - 1 downto ADC_SEG_INDEX*NCHPSEG),
        DSP_EN              => DSP_EN,
        DATA_PKT_ENCODE     => DATA_PKT_ENCODE,
        DATA_PKT_START      => DATA_PKT_START,

        DSP_CHAN_NUMBER => DSP_CHAN_NUMBER(ADC_SEG_INDEX),
        DSP_TIMESTAMP   => DSP_TIMESTAMP(ADC_SEG_INDEX),
        DSP_MAGNITUDE   => DSP_MAGNITUDE(ADC_SEG_INDEX),
        DSP_STRB        => DSP_STRB(ADC_SEG_INDEX),

        RESET  => RESET,
        CLK_64 => CLK_64
        );  

    TRIGGER_HOLDOFF_INST : entity nova_feb.trigger_holdoff
      generic map (
        chan_u => ADC_SEG_INDEX*NCHPSEG + NCHPSEG - 1,
        chan_l => ADC_SEG_INDEX*NCHPSEG
        )
      port map (
        TRIG_HOLDOFF_TIME => TRIG_HOLDOFF_TIME,
        N_DATA_PKT_WORDS  => N_DATA_PKT_WORDS_HLDOF,

        OSCOPE_STRB     => OSCOPE_STRB,
        DSP_STRB        => DSP_STRB(ADC_SEG_INDEX),
        DSP_CHAN_NUMBER => DSP_CHAN_NUMBER(ADC_SEG_INDEX),

        TRIG_HOLDOFF => TRIG_HOLDOFF(ADC_SEG_INDEX),

        RESET  => RESET,
        CLK_64 => CLK_64
        );

    multi_point_ctrl_inst : entity nova_feb.multi_point_ctrl
      port map(
        DSP_CHAN_NUMBER => DSP_CHAN_NUMBER(ADC_SEG_INDEX),
        DSP_TIMESTAMP   => DSP_TIMESTAMP(ADC_SEG_INDEX),
        DSP_MAGNITUDE   => DSP_MAGNITUDE(ADC_SEG_INDEX),
        DSP_TRIGGER     => DSP_TRIGGER(ADC_SEG_INDEX),

        SEND_TM => SEND_TM,

        EVENT_FIFO_DOUT  => EVENT_FIFO_DOUT_VECTOR(ADC_SEG_INDEX),
        EVENT_FIFO_EMPTY => EVENT_FIFO_EMPTY_VECTOR(ADC_SEG_INDEX),
        EVENT_FIFO_FULL  => EVENT_FIFO_FULL_VECTOR(ADC_SEG_INDEX),
        EVENT_FIFO_RSTRB => EVENT_FIFO_RSTRB_VECTOR(ADC_SEG_INDEX),
        THEAD_OVFLW_ERR  => THEAD_OVFLW_ERR_VECTOR(ADC_SEG_INDEX),
        TDATA_OVFLW_ERR  => TDATA_OVFLW_ERR_VECTOR(ADC_SEG_INDEX),
        EVENT_OVFLW_ERR  => EVENT_OVFLW_ERR_VECTOR(ADC_SEG_INDEX),
        DATA_DROP_ERR    => DATA_DROP_ERR_VECTOR(ADC_SEG_INDEX),

        TIMING_PKT_FEB_STATUS => TIMING_PKT_FEB_STATUS,

        DATA_PKT_DAQID      => DATA_PKT_DAQID,
        LEGACY_MODE         => LEGACY_MODE,
        ENABLE_DAQ          => ENABLE_DAQ,
        DATA_MEM_FULL_LATCH => DATA_MEM_FULL_LATCH_VECTOR(ADC_SEG_INDEX),
        N_DATA_PKT_WORDS    => N_DATA_PKT_WORDS_int,
        RESET               => RESET,
        CLK_3_2             => CLK_3_2,
        CLK_16              => CLK_16,
        CLK_32              => CLK_32,
        CLK_64              => CLK_64,
        clk_128             => clk_128
        );


    -- LOOK AT TIMING AND MAYBE PUT THIS IN ABOVE PROCESS
    DSP_TRIGGER(ADC_SEG_INDEX) <= (DSP_STRB(ADC_SEG_INDEX) or OSCOPE_STRB)
                                  and not TRIG_HOLDOFF(ADC_SEG_INDEX)
                                  and (CHAN_EN(to_integer(DSP_CHAN_NUMBER(ADC_SEG_INDEX))) = '1')
                                  and ENABLE_DAQ;

  end generate ADC_SEG_PROC_GEN;


  DATA_MEM_FULL_LATCH_int <= or_bool_reduce(DATA_MEM_FULL_LATCH_VECTOR);
  THEAD_OVFLW_ERR         <= or_bool_reduce(THEAD_OVFLW_ERR_VECTOR);
  TDATA_OVFLW_ERR         <= or_bool_reduce(TDATA_OVFLW_ERR_VECTOR);
  EVENT_OVFLW_ERR         <= or_bool_reduce(EVENT_OVFLW_ERR_VECTOR);
  DATA_DROP_ERR           <= or_bool_reduce(DATA_DROP_ERR_VECTOR);
-- hard coded to feb4 fix when working on feb 5!

  GENERATE_FEB5_EVENT_SEL : if GEN_FEB5 generate
    SELECT_EVENT_FIFO_inst : entity nova_feb.SELECT_EVENT_FIFO
      port map(
        EVENT_FIFO_SEL       => EVENT_FIFO_SEL,
        EVENT_FIFO_EMPTY     => EVENT_FIFO_EMPTY_INT,
        EVENT_FIFO_SEL_NEXT  => EVENT_FIFO_SEL_NEXT,
        EVENT_FIFO_SEL_TM    => EVENT_FIFO_SEL_TM,
        EVENT_FIFO_SEL_CLEAR => EVENT_FIFO_SEL_CLEAR,
        EVENT_FIFO_SEL_ERROR => EVENT_FIFO_SEL_ERROR,
        CLK_3_2              => CLK_3_2,
        CLK_32               => CLK_32
        );

    sel_output : process (
      EVENT_FIFO_DOUT_VECTOR,
      EVENT_FIFO_EMPTY_VECTOR,
      EVENT_FIFO_RSTRB,
      EVENT_FIFO_SEL,
      EVENT_FIFO_EMPTY_INT
      )
    begin
      EVENT_FIFO_DOUT                                     <= EVENT_FIFO_DOUT_VECTOR(to_integer(EVENT_FIFO_SEL));
      EVENT_FIFO_EMPTY_INT                                <= EVENT_FIFO_EMPTY_VECTOR(to_integer(EVENT_FIFO_SEL));
      EVENT_FIFO_EMPTY                                    <= EVENT_FIFO_EMPTY_INT;
      EVENT_FIFO_RSTRB_VECTOR                             <= (others => false);
      EVENT_FIFO_RSTRB_VECTOR(to_integer(EVENT_FIFO_SEL)) <= EVENT_FIFO_RSTRB;
--      for i in EVENT_FIFO_RSTRB_VECTOR'range loop
--        EVENT_FIFO_RSTRB_VECTOR(i) <= (to_unsigned(i, 2) = EVENT_FIFO_SEL)
--                                      and EVENT_FIFO_RSTRB;
--      end loop;
    end process sel_output;

  end generate GENERATE_FEB5_EVENT_SEL;

  GENERATE_FEB4_EVENT_SEL : if not GEN_FEB5 generate
    EVENT_FIFO_DOUT            <= EVENT_FIFO_DOUT_VECTOR(0);
    EVENT_FIFO_EMPTY           <= EVENT_FIFO_EMPTY_VECTOR(0);
    EVENT_FIFO_RSTRB_VECTOR(0) <= EVENT_FIFO_RSTRB;
  end generate GENERATE_FEB4_EVENT_SEL;

--
--                                    
--  sel_output : process (EVENT_FIFO_DOUT_VECTOR, EVENT_FIFO_EMPTY_VECTOR, EVENT_FIFO_RSTRB, SEL_SEG_REG)
--  begin
--    EVENT_FIFO_DOUT  <= EVENT_FIFO_DOUT_VECTOR(to_integer(SEL_SEG_REG));
--    EVENT_FIFO_EMPTY <= EVENT_FIFO_EMPTY_VECTOR(to_integer(SEL_SEG_REG));
--
--    for i in EVENT_FIFO_RSTRB_VECTOR'range loop
--      EVENT_FIFO_RSTRB_VECTOR(i) <= (to_unsigned(i, 2) = SEL_SEG_REG)
--                                    and EVENT_FIFO_RSTRB;
--    end loop;
--  end process sel_output;

  N_DATA_PKT_WORDS    <= N_DATA_PKT_WORDS_int;
  DATA_MEM_FULL_LATCH <= DATA_MEM_FULL_LATCH_int;

  --
end architecture behav;

