-- VHDL A/E
--
-- CREATED:
--          BY - NATE.NATE (HEPLPC2)
--          AT - 16:52:30 09/17/11
--
-- TOP LEVEL INST FOR NOVA FEB4.0, FEB4.1 FEB5.X
--
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library UNISIM;
use UNISIM.all;
library HARVARD_STD;
use HARVARD_STD.LPPC_CUSTOM_FN_PKG.all;

library NOVA_FEB;
use NOVA_FEB.REGISTER_ADDRESS_MAP.all;

entity TOP is
  port(
--FEB4---------------------------------------
    ADC_DOUTAN : in  std_logic_vector (11 downto 0);
    ADC_DOUTAP : in  std_logic_vector (11 downto 0);
    ADC_DOUTBN : in  std_logic_vector (11 downto 0);
    ADC_DOUTBP : in  std_logic_vector (11 downto 0);
    ADC_ERROR  : in  boolean;
    ADC_CLKINN : out std_logic;
    ADC_CLKINP : out std_logic;


--FEB 5------------------------------------
    ADC_DOUT_P : in std_logic_vector (15 downto 0);
    ADC_DOUT_N : in std_logic_vector (15 downto 0);

    ADC_CLKOUT0_P : in  std_logic;
    ADC_CLKOUT0_N : in  std_logic;
    ADC_FRAME0_P  : in  std_logic;
    ADC_FRAME0_N  : in  std_logic;
    ADC_CLKIN0_N  : out std_logic;
    ADC_CLKIN0_P  : out std_logic;
    ADC_PDWN0     : out boolean;
    ADC_SCLK0    : out std_logic;
    ADC_CS0_B    : out boolean;

    ADC_CLKOUT1_P   : in  std_logic;
    ADC_CLKOUT1_N   : in  std_logic;
    ADC_FRAME1_P    : in  std_logic;
    ADC_FRAME1_N    : in  std_logic;
    ADC_CLKIN1_N    : out std_logic;
    ADC_CLKIN1_P    : out std_logic;
    ADC_PDWN1       : out boolean;
    ADC_SCLK1      : out std_logic;
    ADC_CS1_B      : out boolean;
------------------------------------------
    ASIC_CHIPRESET  : out boolean;
    ASIC_SHAPERRST  : out boolean;
    ASIC_SHIFTIN    : out std_logic;
    ASIC_SHIFTOUT   : in  std_logic;
    ASIC_SRCK       : out std_logic;
    ASIC_TESTINJECT : out boolean;
    ASIC_OUTCLK_N   : out std_logic;
    ASIC_OUTCLK_P   : out std_logic;

    DCM_COMMAND_N : in  std_logic;
    DCM_COMMAND_P : in  std_logic;
    DCM_DATA_P    : out std_logic;
    DCM_DATA_N    : out std_logic;
    DCM_SYNC_P    : in  std_logic;
    DCM_SYNC_N    : in  std_logic;
    DCM_CLK_N     : in  std_logic;
    DCM_CLK_P     : in  std_logic;

    EXT_TRIG_IN_N        : in  std_logic;
    EXT_TRIG_IN_P        : in  std_logic;
    INSTRUMENT_TRIGGER_N : out std_logic;
    INSTRUMENT_TRIGGER_P : out std_logic;

    SPI_DOUT : in  std_logic;
    SPI_DIN  : out std_logic;
    SPI_SCLK : out std_logic;

    SER_NUM_CS_B      : out boolean;
    TECC_ENABLE_B     : out boolean;
    TEC_ADC_CH1_B_CH2 : out std_logic;
    TEC_ADC_CS_B      : out boolean;
    TEC_DAC_LDAC_B    : out boolean;
    TEC_DAC_SYNC_B    : out boolean;
    TEMP_SENSOR_CS_B  : out boolean;

    LA : out std_logic_vector (3 downto 0)
    );
end TOP;

architecture STRUCT of TOP is
  signal FRAMEOUT0 : std_logic;
  signal FRAMEOUT1 : std_logic;

  signal MAGNITUDE_CH_ASYNC : MAGNITUDE_VECTOR_TYPE(NADC_DBUS-1 downto 0);
  signal RAW_CHAN_NUMBER    : CHAN_NUM_VECTOR_TYPE(NSEG-1 downto 0);
  signal RAW_MAGNITUDE      : MAGNITUDE_VECTOR_TYPE(NSEG-1 downto 0);
  signal CURRENT_TIME       : TIMESTAMP_TYPE;
--  SIGNAL RAW_MAGNITUDE_ADC : MAGNITUDE; --ONLY USED FOR EXT TRIG?;

  signal DATA_MEM_FULL_LATCH  : boolean               := false;
  signal EVENT_FIFO_DOUT      : unsigned (7 downto 0) := X"00";
  signal EVENT_FIFO_EMPTY     : boolean               := false;
  signal EVENT_FIFO_FULL      : boolean               := false;
  signal EVENT_FIFO_RSTRB     : boolean               := false;
  signal EVENT_FIFO_NEXT      : boolean               := false;
  signal EVENT_FIFO_TM        : boolean               := false;
  signal EVENT_FIFO_SEL_CLEAR : boolean               := false;
  signal EVENT_FIFO_SEL_ERROR : boolean               := false;
  signal TM_TS_SYNC_ERROR     : boolean               := false;

  signal THEAD_OVFLW_ERR : boolean;
  signal TDATA_OVFLW_ERR : boolean;
  signal EVENT_OVFLW_ERR : boolean;
  signal DATA_DROP_ERR   : boolean;

  signal N_DATA_PKT_WORDS : unsigned(4 downto 0);

  signal BEEBUS_ADDR : unsigned(15 downto 0);
  signal BEEBUS_CLK  : std_logic;
  signal BEEBUS_DATA : unsigned(15 downto 0);
  signal BEEBUS_READ : boolean;
  signal BEEBUS_STRB : boolean;

  signal ASIC_CHIPRESET_INT  : boolean;
  signal ASIC_CHIPRESET_del1 : boolean;
  signal ASIC_IBIAS          : boolean;
  signal ASIC_SHIFTIN_INT    : std_logic;
  signal ASIC_SRCK_INT       : std_logic;

  signal ENABLE_STATUS_PKT : boolean := false;
  signal ENABLE_TM_PKT     : boolean := false;
  signal ENABLE_DAQ        : boolean;

  signal DCM_COMMAND : std_logic;
  signal DCM_DATA    : std_logic;
  signal DCM_SYNC    : boolean;
  signal DCM_SYNC_EN : boolean;
  signal DCM_SYNC_IN : std_logic;

  signal EXTERNAL_TRIGGER        : boolean := false;
  signal INSTRUMENT_TRIGGER      : std_logic;
  signal INSTRUMENT_TRIGGER_INT  : boolean := false;
  signal INTERNAL_OR_EXT_TRIG_IN : boolean := false;
  signal INTERNAL_TRIGGER        : boolean := false;
  signal EXT_TRIG_IN             : std_logic;

  signal RX_LINKED             : boolean;
  signal TIMING_PKT_FEB_STATUS : std_logic_vector(7 downto 0);
  signal STATUS_PKT_FEB_STATUS : std_logic_vector(31 downto 0);

  signal SER_NUM_CS     : boolean;
  signal SLO_CTRL_CLK   : std_logic;
  signal SYS_CLK        : std_logic;
  signal TECC_ENABLE    : boolean := false;
  signal TEC_ERR_SHDN   : boolean;
  signal TEC_ADC_CS     : boolean;
  signal TEC_DAC_LDAC   : boolean;
  signal TEC_DAC_SYNC   : boolean;
  signal TEMP_SENSOR_CS : boolean;

  signal COMM_ERROR     : boolean;
  signal TX_ERROR       : boolean;
  signal RX_ERROR       : boolean;
  signal READ_REG_ERROR : boolean;
  signal PACKET_ERROR   : boolean;

  signal SPI_PWRUP_DONE : boolean;
  signal ADC_ENABLE     : boolean := false;
  signal ADC_PWR_CMD    : boolean;

  signal BITSLIP_FORCE : boolean;
  signal PLL_LOCKED    : boolean;
  signal POWERUP_RESET : boolean;
  signal RESET         : boolean;
  signal RESET_DAQ     : boolean;
  signal CLK_IN        : std_logic;
  signal USE_CLK16_INV : boolean;
  signal T_67S         : boolean;
  signal T_1US         : boolean;
  signal TICK_1US      : boolean;
  signal CLK_3_2       : std_logic;
  signal CLK_4         : std_logic;

  signal CLK_16       : std_logic;
  signal CLK_32       : std_logic;
  signal CLK_64       : std_logic;
  signal ADC_CLK0_OUT : std_logic;
  signal ADC_CLK1_OUT : std_logic;
  signal ASIC_CLK     : std_logic;
  signal ASIC_CLK_OUT : std_logic;
  signal CLK_128      : std_logic;

begin
-------------------------------------------------------------------------------
-- instantiate FEB 4

  GENERATE_FEB4_DIN : if not GEN_FEB5 generate
    ADC_PAR_INTERFACE_INST : entity NOVA_FEB.ADC_PAR_INTERFACE
      port map (
        ADC_DOUTAN         => ADC_DOUTAN,
        ADC_DOUTAP         => ADC_DOUTAP,
        ADC_DOUTBN         => ADC_DOUTBN,
        ADC_DOUTBP         => ADC_DOUTBP,
        MAGNITUDE_CH_ASYNC => MAGNITUDE_CH_ASYNC,
        RESET              => ASIC_CHIPRESET_INT,
        CLK_32             => CLK_32,
        USE_CLK16_INV      => USE_CLK16_INV
        );

    --FEB4 NEEDS ONE CLOCK
    ADC_CLKIN_BUF : entity HARVARD_STD.LVDS_OBUF
      port map (
        I  => ADC_CLK0_OUT,
        O  => ADC_CLKINP,
        OB => ADC_CLKINN
        );
  end generate GENERATE_FEB4_DIN;

-------------------------------------------------------------------------------
-- instantiate FEB 5

  GENERATE_FEB5_DIN : if GEN_FEB5 generate
    -- 16X GIGABIT SERIAL ADC INTERFACE
    --COMBINE 2X16 ADC STREAMS INTO 1X32
    ADC_SER_INTERFACE_INST : entity NOVA_FEB.ADC_SER_INTERFACE
      port map (
        ADC_DOUT_P => ADC_DOUT_P,
        ADC_DOUT_N => ADC_DOUT_N,

        ADC_CLKOUT0_P => ADC_CLKOUT0_P,
        ADC_CLKOUT0_N => ADC_CLKOUT0_N,
        ADC_FRAME0_P  => ADC_FRAME0_P,
        ADC_FRAME0_N  => ADC_FRAME0_N,
        FRAMEOUT0     => FRAMEOUT0,
        FRAMEOUT1     => FRAMEOUT1,

        ADC_CLKOUT1_P => ADC_CLKOUT1_P,
        ADC_CLKOUT1_N => ADC_CLKOUT1_N,
        ADC_FRAME1_P  => ADC_FRAME1_P,
        ADC_FRAME1_N  => ADC_FRAME1_N,

        MAGNITUDE_CH_ASYNC => MAGNITUDE_CH_ASYNC,

        USE_CLK16_INV => USE_CLK16_INV,
        BITSLIP_FORCE => BITSLIP_FORCE,
        RESET         => ASIC_CHIPRESET_INT,
        CLK_16        => CLK_16,
        CLK_32        => CLK_32
        );

    -- FEB5 NEEDS TWO clocks
    ADC_CLKIN0_BUF : entity HARVARD_STD.LVDS_OBUF
      port map (
        I  => ADC_CLK0_OUT,
        O  => ADC_CLKIN0_P,
        OB => ADC_CLKIN0_N
        );
    ADC_CLKIN1_BUF : entity HARVARD_STD.LVDS_OBUF
      port map (
        I  => ADC_CLK1_OUT,
        O  => ADC_CLKIN1_P,
        OB => ADC_CLKIN1_N
        );
  end generate GENERATE_FEB5_DIN;
-------------------------------------------------------------------------------

  --DATA FLOW
  TIMING_INST : entity NOVA_FEB.TIMING
    port map (
      BEEBUS_ADDR => BEEBUS_ADDR,
      BEEBUS_DATA => BEEBUS_DATA,
      BEEBUS_READ => BEEBUS_READ,
      BEEBUS_STRB => BEEBUS_STRB,

      MAGNITUDE_CH_ASYNC => MAGNITUDE_CH_ASYNC,

      MAGNITUDE    => RAW_MAGNITUDE,
      CHAN_NUMBER  => RAW_CHAN_NUMBER,
      CURRENT_TIME => CURRENT_TIME,

      RESET_CHAN_PTR => ASIC_CHIPRESET_INT,
      ENABLE_DAQ     => ENABLE_DAQ,

      DCM_SYNC      => DCM_SYNC,
      TICK_1US      => TICK_1US,
      CLK_16        => CLK_16,
      CLK_32        => CLK_32,
      CLK_64        => CLK_64,
      USE_CLK16_INV => USE_CLK16_INV
      );

--  U_11 : ENTITY NOVA_FEB.DATA_PIPELINE
--    PORT MAP (
--      CLK_64                  => CLK_64,
--      DATA                    => RAW_MAGNITUDE_ADC,
--      RAW_MAGNITUDE           => RAW_MAGNITUDE,
--      INTERNAL_TRIGGER        => INTERNAL_TRIGGER,
--      EXT_TRIG_IN             => EXT_TRIG_IN,
--      INTERNAL_OR_EXT_TRIG_IN => INTERNAL_OR_EXT_TRIG_IN
--      );
--  U_12 : LVDS_IBUF
--    PORT MAP (
--      O  => EXT_TRIG_IN,
--      I  => EXT_TRIG_IN_P,
--      IB => EXT_TRIG_IN_N
--      );

  --DATA TRIGGER AND PROCESSING
  DATA_PROCESSING_INST : entity NOVA_FEB.DATA_PROCESSING
    port map (
      BEEBUS_ADDR => BEEBUS_ADDR,
      BEEBUS_DATA => BEEBUS_DATA,
      BEEBUS_READ => BEEBUS_READ,
      BEEBUS_STRB => BEEBUS_STRB,

      RAW_CHAN_NUMBER => RAW_CHAN_NUMBER,
      RAW_TIMESTAMP   => CURRENT_TIME,
      RAW_MAGNITUDE   => RAW_MAGNITUDE,

      -- ENABLE DATA PIPELINE
      --INTERNAL_TRIGGER => INTERNAL_OR_EXT_TRIG_IN,
      INTERNAL_TRIGGER => INTERNAL_TRIGGER,

      EVENT_FIFO_DOUT      => EVENT_FIFO_DOUT,
      EVENT_FIFO_EMPTY     => EVENT_FIFO_EMPTY,
      EVENT_FIFO_FULL      => EVENT_FIFO_FULL,
      EVENT_FIFO_RSTRB     => EVENT_FIFO_RSTRB,
      EVENT_FIFO_SEL_NEXT  => EVENT_FIFO_NEXT,
      EVENT_FIFO_SEL_TM    => EVENT_FIFO_TM,
      EVENT_FIFO_SEL_CLEAR => EVENT_FIFO_SEL_CLEAR,
      EVENT_FIFO_SEL_ERROR => EVENT_FIFO_SEL_ERROR,
      THEAD_OVFLW_ERR      => THEAD_OVFLW_ERR,
      TDATA_OVFLW_ERR      => TDATA_OVFLW_ERR,
      EVENT_OVFLW_ERR      => EVENT_OVFLW_ERR,
      DATA_DROP_ERR        => DATA_DROP_ERR,

      TIMING_PKT_FEB_STATUS => TIMING_PKT_FEB_STATUS,

      DCM_SYNC            => DCM_SYNC,
      ENABLE_TM_PKT       => ENABLE_TM_PKT,
      ENABLE_DAQ          => ENABLE_DAQ,
      N_DATA_PKT_WORDS    => N_DATA_PKT_WORDS,
      DATA_MEM_FULL_LATCH => DATA_MEM_FULL_LATCH,
      RESET               => POWERUP_RESET,
      CLK_3_2             => CLK_3_2,
      CLK_16              => CLK_16,
      CLK_32              => CLK_32,
      CLK_64              => CLK_64,
      CLK_128             => CLK_128,
      TICK_1US            => TICK_1US
      );

  -- CREATE DATA PACKET AND SEND VIA DCM PROTOCOL
  DCM_COMM_INTERFACE_INST : entity NOVA_FEB.DCM_COMM_INTERFACE
    port map (
      FEBUS_ADDR => BEEBUS_ADDR,
      FEBUS_DATA => BEEBUS_DATA,
      FEBUS_READ => BEEBUS_READ,
      FEBUS_STRB => BEEBUS_STRB,

      EVENT_FIFO_DOUT      => EVENT_FIFO_DOUT,
      EVENT_FIFO_EMPTY     => EVENT_FIFO_EMPTY,
      EVENT_FIFO_RSTRB     => EVENT_FIFO_RSTRB,
      EVENT_FIFO_SEL_NEXT  => EVENT_FIFO_NEXT,
      EVENT_FIFO_SEL_TM    => EVENT_FIFO_TM,
      EVENT_FIFO_SEL_CLEAR => EVENT_FIFO_SEL_CLEAR,
      TM_TS_SYNC_ERROR     => TM_TS_SYNC_ERROR,
      N_DATA_PKT_WORDS     => N_DATA_PKT_WORDS,

      CURRENT_TIME          => CURRENT_TIME,
      ENABLE_STATUS_PKT     => ENABLE_STATUS_PKT,
      TIMING_PKT_FEB_STATUS => TIMING_PKT_FEB_STATUS,
      STATUS_PKT_FEB_STATUS => STATUS_PKT_FEB_STATUS,

      SERIAL_TX => DCM_DATA,
      SERIAL_RX => DCM_COMMAND,
      RX_LINKED => RX_LINKED,

      COMM_ERROR     => COMM_ERROR,
      RX_ERROR       => RX_ERROR,
      TX_ERROR       => TX_ERROR,
      READ_REG_ERROR => READ_REG_ERROR,

      RESET   => POWERUP_RESET,
      CLK_3_2 => CLK_3_2,
      CLK_16  => CLK_16,
      CLK_32  => CLK_32
      );


  --DAQ CONTROL VIA REG READ/WRITE
  --SPI INTERFACE
  --PULSE GENERATOR
  --ASIC PROGRAMMING
  --DAQ MODE STTTING
  CONTROLLER_INST : entity NOVA_FEB.CONTROLLER
    port map (
      --!! INCLUDE BUFFER OVERFLOW ERROR AND DON'T SEND CRAP DATA
      BEEBUS_ADDR => BEEBUS_ADDR,
      BEEBUS_DATA => BEEBUS_DATA,
      BEEBUS_CLK  => CLK_16,
      BEEBUS_READ => BEEBUS_READ,
      BEEBUS_STRB => BEEBUS_STRB,

      CURRENT_TIME => CURRENT_TIME,

      ASIC_CHIPRESET  => ASIC_CHIPRESET_INT,
      ASIC_IBIAS      => ASIC_IBIAS,
      ASIC_SHIFTIN    => ASIC_SHIFTIN_INT,
      ASIC_SHIFTOUT   => ASIC_SHIFTOUT,
      ASIC_SRCK       => ASIC_SRCK_INT,
      ASIC_TESTINJECT => ASIC_TESTINJECT,

      APD_DATA_BUFFER_FULL => DATA_MEM_FULL_LATCH,
      DCM_SYNC             => DCM_SYNC,
      DCM_SYNC_EN          => DCM_SYNC_EN,

      ENABLE_DAQ        => ENABLE_DAQ,
      ENABLE_STATUS_PKT => ENABLE_STATUS_PKT,
      ENABLE_TM_PKT     => ENABLE_TM_PKT,

      INSTRUMENT_TRIGGER => INSTRUMENT_TRIGGER_INT,
      INTERNAL_TRIGGER   => INTERNAL_TRIGGER,

      SPI_DIN  => SPI_DIN,
      SPI_DOUT => SPI_DOUT,
      SPI_SCLK => SPI_SCLK,

      TECC_ENABLE       => TECC_ENABLE,
      TEC_ERR_SHDN      => TEC_ERR_SHDN,
      TEC_DAC_LDAC      => TEC_DAC_LDAC,
      TEC_DAC_SYNC      => TEC_DAC_SYNC,
      TEC_ADC_CS        => TEC_ADC_CS,
      TEC_ADC_CH1_B_CH2 => TEC_ADC_CH1_B_CH2,
      TEMP_SENSOR_CS    => TEMP_SENSOR_CS,
      SER_NUM_CS        => SER_NUM_CS,
      ADC_ENABLE        => ADC_ENABLE,
      SPI_PWRUP_DONE    => SPI_PWRUP_DONE,
      ADC_PWR_CMD       => ADC_PWR_CMD,

      BITSLIP_FORCE => BITSLIP_FORCE,
      RESET_DAQ     => RESET_DAQ,
      T_67S         => T_67S,
      CLK_4         => CLK_4,
      CLK_16        => CLK_16,
      CLK_32        => CLK_32
      );

  --GENERATE CLOCKS
  DCM_COMM_EMU_CLK_INST : entity NOVA_FEB.DCM_COMM_EMU_CLK
    port map (
      DCM_CLK_IN_P  => DCM_CLK_P,
      DCM_CLK_IN_N  => DCM_CLK_N,
      LOCKED        => PLL_LOCKED,
      ENABLE_DAQ    => ENABLE_DAQ,
      USE_CLK16_INV => USE_CLK16_INV,
      T_IUS         => T_1US,
      T_67S         => T_67S,
      CLK_3_2       => open,
      CLK_4         => CLK_4,
      CLK_16        => CLK_16,
      CLK_32        => CLK_32,
      CLK_64        => CLK_64,
      CLK_128       => CLK_128,
      ADC_CLK0_OUT  => ADC_CLK0_OUT,
      ADC_CLK1_OUT  => ADC_CLK1_OUT,
      ASIC_CLK_OUT  => ASIC_CLK_OUT,
      USB_IFCLK     => open,
      POWERUP_RESET => POWERUP_RESET
      );

  --COLLECT STATUS BITS
  --!! INCLUDE NEW ERROR BITS
  STATUS_INST : entity NOVA_FEB.STATUS
    port map (
      BEEBUS_ADDR => BEEBUS_ADDR,
      BEEBUS_DATA => BEEBUS_DATA,
      BEEBUS_READ => BEEBUS_READ,
      BEEBUS_STRB => BEEBUS_STRB,

      --THESE END UP IN THE ERROR REGISTER LS 4 ALSO IN TIMING PKT
      RX_ERROR             => RX_ERROR,
      TX_ERROR             => TX_ERROR,
      READ_REG_ERROR       => READ_REG_ERROR,
      THEAD_OVFLW_ERR      => THEAD_OVFLW_ERR,
      TDATA_OVFLW_ERR      => TDATA_OVFLW_ERR,
      EVENT_OVFLW_ERR      => EVENT_OVFLW_ERR,
      DATA_DROP_ERR        => DATA_DROP_ERR,
      TEC_ERR_SHDN         => TEC_ERR_SHDN,
      ADC_ERROR            => ADC_ERROR,
      COMM_ERROR           => COMM_ERROR,
      TM_TS_SYNC_ERROR     => TM_TS_SYNC_ERROR,
      EVENT_FIFO_SEL_ERROR => EVENT_FIFO_SEL_ERROR,
      --THESE END UP IN THE STATUS REGISTER LS 3 ALSO IN TIMING PKT

      ADC_ENABLE        => ADC_ENABLE,
      USE_CLK16_INV     => USE_CLK16_INV,
      DCM_SYNC_EN       => DCM_SYNC_EN,
      ENABLE_STATUS_PKT => ENABLE_STATUS_PKT,
      ENABLE_TM_PKT     => ENABLE_TM_PKT,
      EVENT_FIFO_EMPTY  => EVENT_FIFO_EMPTY,
      EVENT_FIFO_FULL   => EVENT_FIFO_FULL,
      TECC_ENABLE       => TECC_ENABLE,
      ENABLE_DAQ        => ENABLE_DAQ,

      TIMING_PKT_FEB_STATUS => TIMING_PKT_FEB_STATUS,
      STATUS_PKT_FEB_STATUS => STATUS_PKT_FEB_STATUS,

      POWERUP_RESET => POWERUP_RESET,
      CLK_16        => CLK_16,
      CLK_128       => CLK_128
      );

  LVDS_IBUF_SYNC : entity HARVARD_STD.LVDS_IBUF
    port map (
      O  => DCM_SYNC_IN,
      I  => DCM_SYNC_P,
      IB => DCM_SYNC_N
      );

  SYNC_LOCAL : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '1' then
      DCM_SYNC <= (DCM_SYNC_IN = '1' and DCM_SYNC_EN) after 1 ns;
    end if;
  end process SYNC_LOCAL;

  LVDS_IBUF_CMD : entity HARVARD_STD.LVDS_IBUF
    port map (
      O  => DCM_COMMAND,
      I  => DCM_COMMAND_P,
      IB => DCM_COMMAND_N
      );

  LVDS_OBUF_ASIC_CLK : entity HARVARD_STD.LVDS_OBUF
    port map (
      I  => ASIC_CLK_OUT,
--      I  => '0',-- HOLD ASIC MUX
      O  => ASIC_OUTCLK_P,
      OB => ASIC_OUTCLK_N
      );
  LVDS_OBUF_DATA : entity HARVARD_STD.LVDS_OBUF
    port map (
      I  => DCM_DATA,
      O  => DCM_DATA_P,
      OB => DCM_DATA_N
      );

  --INTERNAL SIGNSL ACCESS AND LOGIC ANALYZER
  ASIC_SHAPERRST <= false;
  ASIC_SRCK      <= ASIC_SRCK_INT;
  ASIC_SHIFTIN   <= ASIC_SHIFTIN_INT;

  ASIC_DELAY_RESET : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '1' then
      ASIC_CHIPRESET_del1 <= ASIC_CHIPRESET_INT;
      if not USE_CLK16_INV then
        ASIC_CHIPRESET <= ASIC_CHIPRESET_INT;
      else
        ASIC_CHIPRESET <= ASIC_CHIPRESET_del1;
      end if;
    end if;
  end process ASIC_DELAY_RESET;

  LA(3 downto 0) <= BOOL2SL(ADC_PWR_CMD) &  --BOOL2SL(INSTRUMENT_TRIGGER_INT) &
                    BOOL2SL(SPI_PWRUP_DONE) &
                    BOOL2SL(ADC_PWR_CMD) &
                    BOOL2SL(not ADC_ENABLE);
  
  TEC_DAC_SYNC_B   <= not TEC_DAC_SYNC;
  TEMP_SENSOR_CS_B <= not TEMP_SENSOR_CS;
  TEC_ADC_CS_B     <= not TEC_ADC_CS;
  TEC_DAC_LDAC_B   <= not TEC_DAC_LDAC;
  TECC_ENABLE_B    <= not (TECC_ENABLE and PLL_LOCKED);
  SER_NUM_CS_B     <= not SER_NUM_CS;

  INSTRUMENT_TRIGGER <= BOOL2SL(INSTRUMENT_TRIGGER_INT);
  ADC_PDWN0          <= not ADC_ENABLE;
  ADC_PDWN1          <= not ADC_ENABLE;
  ADC_SCLK0         <= '0';
  ADC_CS0_B         <= true;
  ADC_SCLK1         <= '0';
  ADC_CS1_B         <= true;
  
end STRUCT;
