--
-- VHDL Architecture feb_p2_lib.comm_decode.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 16:42:19 11/ 7/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

ENTITY comm_decode IS
   PORT( 
      CLK_32          : IN     std_logic;
      CLK_3_2         : OUT    std_logic;
      DATA_RX         : OUT    unsigned (7 DOWNTO 0);
      DATA_TX_IN      : IN     unsigned (7 DOWNTO 0);
      DATA_TX_IN_STRB : IN     boolean;
      DATA_RX_STRB    : OUT    boolean;
      COMM_ERROR      : OUT    boolean;
      RX_LINKED       : OUT    boolean;
      SERIAL_TX       : OUT    std_logic;
      SERIAL_RX       : IN     std_logic
   );

-- Declarations

END comm_decode ;


--
architecture behav of comm_decode is
  signal SHIPT_REG_RX_COMMA : boolean                      := false;
  signal COMMA_FOUND        : boolean                      := false;
  signal COMMA_FOUND_TWICE  : boolean                      := false;
  signal DATA_RX_int        : std_logic_vector(7 downto 0) := x"00";
  signal DATA_TX            : std_logic_vector(7 downto 0) := x"00";
  signal ENCODED_TX         : std_logic_vector(9 downto 0) := "0000000000";
  signal ENCODED_RX         : std_logic_vector(9 downto 0) := "0000000000";
  signal SHIFT_REG_TX       : std_logic_vector(9 downto 0) := "0000000000";
  signal SHIFT_REG_RX       : std_logic_vector(9 downto 0) := "0000000000";
  signal BIT_COUNT          : unsigned(3 downto 0)         := x"0";
  signal LINKED_pre         : boolean                      := false;
  signal LINKED             : boolean                      := false;
  signal KOUT               : std_logic                    := '1';
  signal KIN                : std_logic                    := '1';
  signal CLK_3_2_int        : std_logic                    := '0';
  signal CLK_3_2_COMB       : std_logic                    := '0';

  component decode_8b10b
    port (
      clk      : in  std_logic;
      din      : in  std_logic_vector(9 downto 0);
      dout     : out std_logic_vector(7 downto 0);
      kout     : out std_logic;
      code_err : out std_logic);
  end component;

  component encode_8b10b
    port (
      din  : in  std_logic_vector(7 downto 0);
      kin  : in  std_logic;
      clk  : in  std_logic;
      dout : out std_logic_vector(9 downto 0));
  end component;

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;

  
begin
  DCM_COMM_DECODE : decode_8b10b
    port map (
      clk      => CLK_3_2_int,
      din      => ENCODED_RX,
      dout     => DATA_RX_int,
      kout     => KOUT,
      code_err => open
      );

  DCM_COMM_ENCODE : encode_8b10b

    port map (
      din  => DATA_TX,
      kin  => KIN,
      clk  => CLK_3_2_int,
      dout => ENCODED_TX
      );

  FRAME_BITS : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '1' then
      BIT_COUNT <= BIT_COUNT +1;

      if SHIPT_REG_RX_COMMA or (BIT_COUNT = 9) then
        BIT_COUNT         <= x"0";
        COMMA_FOUND       <= SHIPT_REG_RX_COMMA;
        COMMA_FOUND_TWICE <= COMMA_FOUND and SHIPT_REG_RX_COMMA;
        LINKED_pre        <= (COMMA_FOUND_TWICE or LINKED_pre) and BIT_COUNT = x"9";
        --FIX LINK STATUS AND ERROR FLAG
        COMM_ERROR        <= LINKED_pre and not BIT_COUNT = x"9";
      end if;

    end if;
  end process FRAME_BITS;
  SHIPT_REG_RX_COMMA <= (SHIFT_REG_RX = "1001111100") or
                        (SHIFT_REG_RX = "0110000011");

  SER_DES : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '1' then
      SHIFT_REG_TX <= '0' & SHIFT_REG_TX(9 downto 1);
      SHIFT_REG_RX <= SERIAL_RX & SHIFT_REG_RX(9 downto 1);

      if BIT_COUNT = 9 then
        SHIFT_REG_TX <= ENCODED_TX;
        ENCODED_RX   <= SHIFT_REG_RX;
      end if;
      
    end if;
  end process SER_DES;

  CLOCK_GEN_3_2 : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '1' then
      --PUT RISING EDGE OF 3_2 CLOCK HALFWAY THROUGH DATA FRAME
      CLK_3_2_COMB <= BOOL2SL(((BIT_COUNT = 4) or
                               (BIT_COUNT = 5) or
                               (BIT_COUNT = 6))
                              and LINKED_pre);
    end if;
    
  end process CLOCK_GEN_3_2;

  CLK_32_BUFG : BUFG
    port map (
      O => CLK_3_2_int,
      I => CLK_3_2_COMB
      );

  CLK_3_2 <= CLK_3_2_int;

-- the following will be changed to a dual port memory FIFO
  IN_BUFFER : process (CLK_3_2_int) is
  begin
    if CLK_3_2_int'event and CLK_3_2_int = '1' then
      
      if DATA_TX_IN_STRB then
        DATA_TX <= std_logic_vector(DATA_TX_IN);
        KIN     <= '0';
      else
        DATA_TX <= x"3C";
        KIN     <= '1';
      end if;

    end if;
  end process IN_BUFFER;

  sync_linked_status : process (CLK_3_2_int) is
  begin
    if CLK_3_2_int'event and CLK_3_2_int = '1' then
      LINKED <= LINKED_PRE;
    end if;
  end process sync_linked_status;
  RX_LINKED    <= LINKED;
  DATA_RX_STRB <= (KOUT = '0') and LINKED;
  DATA_RX      <= unsigned(DATA_RX_int);
  SERIAL_TX    <= SHIFT_REG_TX(0);
end architecture behav;

