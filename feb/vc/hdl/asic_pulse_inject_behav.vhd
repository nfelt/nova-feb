--
-- VHDL Architecture feb_p2_lib.asic_pulse_inject.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 16:50:32 02/28/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

-- remove pulse width generic. it is now a register.

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;
use nova_feb.register_address_map.all;

entity asic_pulse_inject is
  generic(
    ext_pulse_width : unsigned(15 downto 0)
    );
  port(
    BEEBUS_ADDR        : in  unsigned (15 downto 0);
    BEEBUS_DATA        : in  unsigned (15 downto 0);
    BEEBUS_READ        : in  boolean;
    BEEBUS_STRB        : in  boolean;
    ENABLE_DAQ         : in  boolean;
    INTERNAL_TRIGGER   : out boolean := false;
    ASIC_TESTINJECT    : out boolean := false;
    INSTRUMENT_TRIGGER : out boolean := false;
    CLK                : in  std_logic
    );

-- Declarations

end asic_pulse_inject;

--
architecture behav of asic_pulse_inject is
  
  signal TEST_INJECT_RATE     : unsigned(19 downto 0)  := x"00000";
  signal ext_pulse_width_reg  : unsigned(15 downto 0)  := x"0000";
  signal DEL_SHIFT_REG        : unsigned(127 downto 0) := x"00000000000000000000000000000000";
  signal TRIGGER_SELECT       : unsigned(3 downto 0)   := x"0";
  signal RATE_COUNT           : unsigned(19 downto 0)  := x"00000";
  signal extrig_pulse_count   : unsigned(15 downto 0)  := x"0000";
  signal ENABLE_PRDIC_INJECT  : boolean                := false;
  signal TRIGGER              : boolean                := false;
  signal DELAYED_TRIGGER      : boolean                := false;
  signal EXTERNAL_TRIGGER_del : boolean                := false;
  signal ASIC_SRCK_int        : std_logic              := '0';

begin

  RECEIVE_COMMAND : process (CLK) is
  begin
    if CLK'event and CLK = '1' then
      TRIGGER <= FALSE;
      if BEEBUS_STRB then
        case BEEBUS_ADDR is
          
          when PULSER_ENABLE_ADDR =>
            TRIGGER_SELECT <= BEEBUS_DATA(3 downto 0);

          when PULSER_PERIODICITY_ADDR =>
            TRIGGER          <= BEEBUS_DATA = x"0001";
            TEST_INJECT_RATE <= BEEBUS_DATA & x"0";
            RATE_COUNT       <= x"00001";
            
          when PULSER_WIDTH_ADDR =>
            ext_pulse_width_reg <= BEEBUS_DATA;
            
          when others => null;
        end case;
      end if;

      if ENABLE_PRDIC_INJECT then
        if RATE_COUNT = TEST_INJECT_RATE then
          RATE_COUNT <= x"00001";
          TRIGGER    <= true;
        else
          RATE_COUNT <= RATE_COUNT+1;
        end if;
      end if;
      
    end if;
  end process RECEIVE_COMMAND;

  ENABLE_PRDIC_INJECT <= TEST_INJECT_RATE > x"00010" and ENABLE_DAQ;

  SELECT_TRIG_OUT : process (TRIGGER, TRIGGER_SELECT, ENABLE_DAQ, DELAYED_TRIGGER) is
  begin
    INTERNAL_TRIGGER   <= (TRIGGER and (TRIGGER_SELECT(0) = '1'));
    ASIC_TESTINJECT    <= DELAYED_TRIGGER and (TRIGGER_SELECT(1) = '1');
    INSTRUMENT_TRIGGER <= DELAYED_TRIGGER and (TRIGGER_SELECT(2) = '1');
    
  end process SELECT_TRIG_OUT;

  EXT_TRIG_SHIFT_REG : process (clk) is
  begin
    if clk'event and clk = '1' then
      DEL_SHIFT_REG        <= DEL_SHIFT_REG(126 downto 0) & BOOL2SL(TRIGGER);
      EXTERNAL_TRIGGER_del <= DEL_SHIFT_REG(1) = '1';
    end if;
  end process EXT_TRIG_SHIFT_REG;

  extrig_pulse_stretch : process (clk) is
  begin
    if clk'event and clk = '1' then
      DELAYED_TRIGGER <= false;
      if EXTERNAL_TRIGGER_del then
        extrig_pulse_count <= ext_pulse_width_reg;
      elsif not(extrig_pulse_count = x"0000") then
        extrig_pulse_count <= extrig_pulse_count - 1;
        DELAYED_TRIGGER    <= true;
      end if;
    end if;
  end process extrig_pulse_stretch;
  
end architecture behav;

