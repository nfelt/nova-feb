--
-- VHDL ARCHITECTURE FEB_P2_LIB.ADC_INTERFACE.BEHAV
--
-- CREATED:
--          BY - NATE.UNKNOWN (HEPLPC2)
--          AT - 11:43:21 01/22/2007
--
-- USING MENTOR GRAPHICS HDL DESIGNER(TM) 2006.1 (BUILD 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library UNISIM;
use UNISIM.all;
library HARVARD_STD;
use HARVARD_STD.LPPC_CUSTOM_FN_PKG.all;
library NOVA_FEB;
use NOVA_FEB.REGISTER_ADDRESS_MAP.all;

entity GBT_SER_RX_PLL is
  port (
    DATAIN_P      : in  std_logic_vector(8 downto 0);
    DATAIN_N      : in  std_logic_vector(8 downto 0);
    CLKIN_P       : in  std_logic;
    CLKIN_N       : in  std_logic;
    ADC_CHAN_DATA : out MAGNITUDE_VECTOR_TYPE(8 downto 0);
    FRAMEOUT      : out std_logic;
    BITSLIP_FORCE : in  boolean;
    RESET         : in  std_logic;
    CLK_16        : in  std_logic;
    CLK_32        : in  std_logic
    );
end GBT_SER_RX_PLL;

architecture ARCH of GBT_SER_RX is

  type ADC_MS_TEMP_TYPE is array (15 downto 0) of std_logic_vector(5 downto 0);

  constant S  : integer := 6;           -- SET THE SERDES FACTOR
  constant D  : integer := 9;           -- SET THE NUMBER OF INPUTS AND OUTPUTS
  constant DS : integer := (D*S)-1;  -- USED FOR BUS WIDTHS = SERDES FACTOR * NUMBER OF INPUTS - 1

  signal ADC_MS_TEMP      : ADC_MS_TEMP_TYPE;
  signal RXD              : std_logic_vector(DS downto 0);  -- DATA FROM SERDESES
  signal RXR              : std_logic_vector(DS downto 0);  -- SIGNALISTERED DATA FROM SERDESES
  signal RX_BUFG_X1       : std_logic;
  signal CLK_ISERDES_DATA : std_logic_vector(6 downto 0);
  signal CAPTURE          : std_logic_vector(6 downto 0);
  signal COUNTER          : std_logic_vector(3 downto 0);
  signal BITSLIP          : std_logic;
  signal RST              : std_logic;
  signal RX_SERDESSTROBE  : std_logic;
  signal RX_BUFPLL_CLK_XN : std_logic;
  signal RX_BUFPLL_LCKD   : std_logic;
  signal NOT_BUFPLL_LCKD  : std_logic;

begin

  RST <= RESET;                         -- ACTIVE HIGH RESET PIN

  APD_DATA_IN_MS : process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '0' then
      for ADC_INDEX in 8 downto 0 loop
        ADC_CHAN_DATA(ADC_INDEX) <= unsigned(ADC_MS_TEMP(ADC_INDEX)) &
                                    unsigned(RXR((ADC_INDEX * 6)+5 downto ADC_INDEX * 6));
      end loop;  -- ADC_TEMP_INDEX
    end if;
  end process;

  APD_DATA_IN_LS : process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '1' then
      for ADC_TMP_INDEX in 8 downto 0 loop
        ADC_MS_TEMP(ADC_TMP_INDEX) <= RXR((ADC_TMP_INDEX * 6)+5 downto ADC_TMP_INDEX * 6);
      end loop;  -- ADC_TEMP_INDEX
    end if;
  end process;



  CLKIN : entity NOVA_FEB.SERDES_1_TO_N_CLK_PLL_S8_DIFF generic map(
    CLKIN_PERIOD => 6.700,
    PLLD         => 1,
    PLLX         => S,
    S            => S,
    BS           => true)  -- PARAMETER TO ENABLE BITSLIP TRUE OR FALSE (HAS TO BE TRUE FOR VIDEO APPLICATIONS)
    port map (
      CLKIN_P         => CLKIN_P,
      CLKIN_N         => CLKIN_N,
      RXIOCLK         => RX_BUFPLL_CLK_XN,
      PATTERN1        => "111111",
      PATTERN2        => "000000",
      RX_SERDESSTROBE => RX_SERDESSTROBE,
      RX_BUFG_PLL_X1  => RX_BUFG_X1,
      BITSLIP         => BITSLIP,
      RESET           => RST,
      DATAIN          => CLK_ISERDES_DATA,
      RX_BUFPLL_LCKD  => RX_BUFPLL_LCKD
      ) ;

  NOT_BUFPLL_LCKD <= not RX_BUFPLL_LCKD;

  DATAIN : entity NOVA_FEB.SERDES_1_TO_N_DATA_S8_DIFF
    generic map(
      S => S,
      D => D
      )
    port map (
      USE_PHASE_DETECTOR => '1',
      DATAIN_P           => DATAIN_P,
      DATAIN_N           => DATAIN_N,
      RXIOCLK            => RX_BUFPLL_CLK_XN,
      RXSERDESSTROBE     => RX_SERDESSTROBE,
      GCLK               => RX_BUFG_X1,
      BITSLIP            => BITSLIP,
      RESET              => NOT_BUFPLL_LCKD,
      DEBUG_IN           => "00",
      DATA_OUT           => RXD,
      DEBUG              => open
      ) ;


  process (RX_BUFG_X1)
  begin
    if RX_BUFG_X1'event and RX_BUFG_X1 = '1' then
      RXR      <= RXD;
      FRAMEOUT <= RXD(DS);
    end if;
  end process;

end ARCH;

