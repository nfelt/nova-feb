------------------------------------------------------------------------------
-- COPYRIGHT (C) 2009 XILINX, INC.
-- THIS DESIGN IS CONFIDENTIAL AND PROPRIETARY OF XILINX, ALL RIGHTS RESERVED.
------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /   VENDOR: XILINX
-- \   \   \/    VERSION: 1.0
--  \   \        FILENAME: SERDES_1_TO_N_DATA_DDR_S8_DIFF.VHD
--  /   /        DATE LAST MODIFIED:  NOVEMBER 5 2009
-- /___/   /\    DATE CREATED: AUGUST 1 2008
-- \   \  /  \
--  \___\/\___\
-- 
--DEVICE:   SPARTAN 6
--PURPOSE:      D-BIT GENERIC 1:N DATA RECEIVER MODULE WITH DIFFERENTIAL INPUTS FOR DDR SYSTEMS
--      TAKES IN 1 BIT OF DIFFERENTIAL DATA AND DESERIALISES THIS TO N BITS
--      DATA IS RECEIVED LSB FIRST
--      SERIAL INPUT WORDS
--      LINE0     : 0,   ...... DS-(S+1)
--      LINE1     : 1,   ...... DS-(S+2)
--      LINE(D-1) : .           .
--      LINE(D)  : D-1, ...... DS
--      PARALLEL OUTPUT WORD
--      DS, DS-1 ..... 1, 0
--
--      INCLUDES STATE MACHINE TO CONTROL CALIBRATION ONLY
--      DATA INVERSION CAN BE ACCOMPLISHED VIA THE RX_RX_SWAP_MASK PARAMETER IF REQUIRED
--
--REFERENCE:
--    
--REVISION HISTORY:
--    REV 1.0 - FIRST CREATED (NICKS)
------------------------------------------------------------------------------
--
--  DISCLAIMER: 
--
--      THIS DISCLAIMER IS NOT A LICENSE AND DOES NOT GRANT ANY RIGHTS TO THE MATERIALS 
--              DISTRIBUTED HEREWITH. EXCEPT AS OTHERWISE PROVIDED IN A VALID LICENSE ISSUED TO YOU 
--              BY XILINX, AND TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW: 
--              (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL FAULTS, 
--              AND XILINX HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, 
--              INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT, OR 
--              FITNESS FOR ANY PARTICULAR PURPOSE; AND (2) XILINX SHALL NOT BE LIABLE (WHETHER IN CONTRACT 
--              OR TORT, INCLUDING NEGLIGENCE, OR UNDER ANY OTHER THEORY OF LIABILITY) FOR ANY LOSS OR DAMAGE 
--              OF ANY KIND OR NATURE RELATED TO, ARISING UNDER OR IN CONNECTION WITH THESE MATERIALS, 
--              INCLUDING FOR ANY DIRECT, OR ANY INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL LOSS 
--              OR DAMAGE (INCLUDING LOSS OF DATA, PROFITS, GOODWILL, OR ANY TYPE OF LOSS OR DAMAGE SUFFERED 
--              AS A RESULT OF ANY ACTION BROUGHT BY A THIRD PARTY) EVEN IF SUCH DAMAGE OR LOSS WAS 
--              REASONABLY FORESEEABLE OR XILINX HAD BEEN ADVISED OF THE POSSIBILITY OF THE SAME.
--
--  CRITICAL APPLICATIONS:
--
--      XILINX PRODUCTS ARE NOT DESIGNED OR INTENDED TO BE FAIL-SAFE, OR FOR USE IN ANY APPLICATION 
--      REQUIRING FAIL-SAFE PERFORMANCE, SUCH AS LIFE-SUPPORT OR SAFETY DEVICES OR SYSTEMS, 
--      CLASS III MEDICAL DEVICES, NUCLEAR FACILITIES, APPLICATIONS RELATED TO THE DEPLOYMENT OF AIRBAGS,
--      OR ANY OTHER APPLICATIONS THAT COULD LEAD TO DEATH, PERSONAL INJURY, OR SEVERE PROPERTY OR 
--      ENVIRONMENTAL DAMAGE (INDIVIDUALLY AND COLLECTIVELY, "CRITICAL APPLICATIONS"). CUSTOMER ASSUMES 
--      THE SOLE RISK AND LIABILITY OF ANY USE OF XILINX PRODUCTS IN CRITICAL APPLICATIONS, SUBJECT ONLY 
--      TO APPLICABLE LAWS AND REGULATIONS GOVERNING LIMITATIONS ON PRODUCT LIABILITY.
--
--  THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT ALL TIMES.
--
------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.NUMERIC_STD.all;

library UNISIM;
use UNISIM.VCOMPONENTS.all;

entity SERDES_1_TO_N_DATA_DDR_S8_DIFF is
  generic (
    S         : integer := 6;        -- PARAMETER TO SET THE SERDES FACTOR 1..8
    D         : integer := 9;          -- SET THE NUMBER OF INPUTS AND OUTPUTS
    DIFF_TERM : boolean := false) ;  -- ENABLE OR DISABLE INTERNAL DIFFERENTIAL TERMINATION
  port (
    USE_PHASE_DETECTOR : in  std_logic;  -- '1' ENABLES THE PHASE DETECTOR LOGIC IF USE_PD = TRUE
    DATAIN_P           : in  std_logic_vector(D-1 downto 0);  -- INPUT FROM LVDS RECEIVER PIN
    DATAIN_N           : in  std_logic_vector(D-1 downto 0);  -- INPUT FROM LVDS RECEIVER PIN
    RXIOCLKP           : in  std_logic;  -- IO CLOCK NETWORK
    RXIOCLKN           : in  std_logic;  -- IO CLOCK NETWORK
    RXSERDESSTROBE     : in  std_logic;  -- PARALLEL DATA CAPTURE STROBE
    RESET              : in  std_logic;  -- RESET LINE
    GCLK               : in  std_logic;  -- GLOBAL CLOCK
    clk_32               : in  std_logic;  -- 6bit clk
    BITSLIP            : in  std_logic;  -- BITSLIP CONTROL LINE
    DATA_OUT           : out std_logic_vector((D*S)-1 downto 0);  -- OUTPUT DATA
    DEBUG_IN           : in  std_logic_vector(1 downto 0);  -- DEBUG INPUTS, SET TO '0' IF NOT REQUIRED
    DEBUG              : out std_logic_vector((2*D)+6 downto 0)) ;  -- DEBUG OUTPUT BUS, 2D+6 = 2 LINES PER INPUT (FROM MUX AND CE) + 7, LEAVE NC IF DEBUG NOT REQUIRED
end SERDES_1_TO_N_DATA_DDR_S8_DIFF;

architecture ARCH_SERDES_1_TO_N_DATA_DDR_S8_DIFF of SERDES_1_TO_N_DATA_DDR_S8_DIFF is

  signal DDLY_M          : std_logic_vector(D-1 downto 0);  -- MASTER OUTPUT FROM IODELAY1
  signal DDLY_S          : std_logic_vector(D-1 downto 0);  -- SLAVE OUTPUT FROM IODELAY1
  signal MDATAOUT        : std_logic_vector((8*D)-1 downto 0);
  signal CASCADE         : std_logic_vector(D-1 downto 0);
  signal PD_EDGE         : std_logic_vector(D-1 downto 0);
  signal BUSYS           : std_logic_vector(D-1 downto 0);
  signal BUSYM           : std_logic_vector(D-1 downto 0);
  signal RX_DATA_IN      : std_logic_vector(D-1 downto 0);
  signal RX_DATA_IN_FIX  : std_logic_vector(D-1 downto 0);
  signal STATE           : integer range 0 to 8;
  signal BUSY_DATA_D     : std_logic;
  signal BUSY_DATA       : std_logic_vector(D-1 downto 0);
  signal INC_DATA        : std_logic;
  signal CE_DATA         : std_logic_vector(D-1 downto 0);
  signal INCDEC_DATA_D   : std_logic;
  signal VALID_DATA_D    : std_logic;
  signal COUNTER         : unsigned(8 downto 0);
  signal ENABLE          : std_logic;
  signal CAL_DATA_MASTER : std_logic;
  signal RST_DATA        : std_logic;
  signal PDCOUNTER       : unsigned(4 downto 0);
  signal CE_DATA_INT     : std_logic_vector(D-1 downto 0);
  signal INC_DATA_INT    : std_logic;
  signal INCDEC_DATA     : std_logic_vector(D-1 downto 0);
  signal CAL_DATA_SLAVE  : std_logic;
  signal VALID_DATA      : std_logic_vector(D-1 downto 0);
  signal MUX             : std_logic_vector(D-1 downto 0);
  signal CE_DATA_INTA    : std_logic;
  signal FLAG            : std_logic;
  signal CAL_DATA_SINT   : std_logic;
  signal INCDEC_DATA_OR  : std_logic_vector(D downto 0);
  signal INCDEC_DATA_IM  : std_logic_vector(D-1 downto 0);
  signal VALID_DATA_OR   : std_logic_vector(D downto 0);
  signal VALID_DATA_IM   : std_logic_vector(D-1 downto 0);
  signal BUSY_DATA_OR    : std_logic_vector(D downto 0);
  signal ALL_CE          : std_logic_vector(D-1 downto 0);

  constant RX_SWAP_MASK : std_logic_vector(D-1 downto 0) := (others => '0');  -- PINSWAP MASK FOR INPUT BITS (0 = NO SWAP (DEFAULT), 1 = SWAP). ALLOWS INPUTS TO BE CONNECTED THE WRONG WAY ROUND TO EASE PCB ROUTING.

begin

  CAL_DATA_SLAVE <= CAL_DATA_SINT;
  DEBUG          <= MUX & CAL_DATA_MASTER & RST_DATA & CAL_DATA_SLAVE & BUSY_DATA_D & INC_DATA & CE_DATA & VALID_DATA_D & INCDEC_DATA_D;

  process (GCLK, RESET)
  begin
    if RESET = '1' then
      STATE           <= 0;
      CAL_DATA_MASTER <= '0';
      CAL_DATA_SINT   <= '0';
      ENABLE          <= '0';
      COUNTER         <= (others => '0');
      MUX             <= (0      => '1', others => '0');
    elsif GCLK'event and GCLK = '1' then
      COUNTER <= COUNTER + 1;
      if COUNTER(8) = '1' then
        COUNTER <= "000000000";
      end if;
      if COUNTER(5) = '1' then
        ENABLE <= '1';
      end if;
      if STATE = 0 and ENABLE = '1' then  -- WAIT FOR ALL IODELAYS TO BE AVAILABLE
        CAL_DATA_MASTER <= '0';
        CAL_DATA_SINT   <= '0';
        RST_DATA        <= '0';
        if BUSY_DATA_D = '0' then
          STATE <= 1;
        end if;
      elsif STATE = 1 then  -- ISSUE CALIBRATE COMMAND TO BOTH MASTER AND SLAVE
        CAL_DATA_MASTER <= '1';
        CAL_DATA_SINT   <= '1';
        if BUSY_DATA_D = '1' then       -- AND WAIT FOR COMMAND TO BE ACCEPTED
          STATE <= 2;
        end if;
      elsif STATE = 2 then  -- NOW RST ALL MASTER AND SLAVE IODELAYS
        CAL_DATA_MASTER <= '0';
        CAL_DATA_SINT   <= '0';
        if BUSY_DATA_D = '0' then
          RST_DATA <= '1';
          STATE    <= 3;
        end if;
      elsif STATE = 3 then  -- WAIT FOR ALL IODELAYS TO BE AVAILABLE
        RST_DATA <= '0';
        if BUSY_DATA_D = '0' then
          STATE <= 4;
        end if;
      elsif STATE = 4 then              -- HANG AROUND
        if COUNTER(8) = '1' then
          STATE <= 5;
        end if;
      elsif STATE = 5 then              -- CALIBRATE SLAVE ONLY
        if BUSY_DATA_D = '0' then
          CAL_DATA_SINT <= '1';
          STATE         <= 6;
          if D /= 1 then
            MUX <= MUX(D-2 downto 0) & MUX(D-1);
          end if;
        end if;
      elsif STATE = 6 then              -- WAIT FOR COMMAND TO BE ACCEPTED
        if BUSY_DATA_D = '1' then
          CAL_DATA_SINT <= '0';
          STATE         <= 7;
        end if;
      elsif STATE = 7 then  -- WAIT FOR ALL IODELAYS TO BE AVAILABLE, IE CAL COMMAND FINISHED
        CAL_DATA_SINT <= '0';
        if BUSY_DATA_D = '0' then
          STATE <= 4;
        end if;
      end if;
    end if;
  end process;

  process (GCLK, RESET)
  begin
    if RESET = '1' then
      PDCOUNTER    <= "10000";
      CE_DATA_INTA <= '0';
      FLAG         <= '0';
    elsif GCLK'event and GCLK = '1' then
      BUSY_DATA_D <= BUSY_DATA_OR(D);
      if USE_PHASE_DETECTOR = '1' then  -- DECIDE WHTHER PD IS USED
        INCDEC_DATA_D <= INCDEC_DATA_OR(D);
        VALID_DATA_D  <= VALID_DATA_OR(D);
        if CE_DATA_INTA = '1' then
          CE_DATA <= MUX;
        else
          CE_DATA <= (others => '0');
        end if;
        if STATE = 7 then
          FLAG <= '0';
        elsif STATE /= 4 or BUSY_DATA_D = '1' then  -- RESET FILTER IF STATE MACHINE ISSUES A CAL COMMAND OR UNIT IS BUSY
          PDCOUNTER    <= "10000";
          CE_DATA_INTA <= '0';
        elsif PDCOUNTER = "11111" and FLAG = '0' then  -- FILTER HAS REACHED POSITIVE MAX - INCREMENT THE TAP COUNT
          CE_DATA_INTA <= '1';
          INC_DATA_INT <= '1';
          PDCOUNTER    <= "10000";
          FLAG         <= '0';
        elsif PDCOUNTER = "00000" and FLAG = '0' then  -- FILTER HAS REACHED NEGATIVE MAX - DECREMENT THE TAP COUNT
          CE_DATA_INTA <= '1';
          INC_DATA_INT <= '0';
          PDCOUNTER    <= "10000";
          FLAG         <= '0';
        elsif VALID_DATA_D = '1' then   -- INCREMENT FILTER
          CE_DATA_INTA <= '0';
          if INCDEC_DATA_D = '1' and PDCOUNTER /= "11111" then
            PDCOUNTER <= PDCOUNTER + 1;
          elsif INCDEC_DATA_D = '0' and PDCOUNTER /= "00000" then  -- DECREMENT FILTER
            PDCOUNTER <= PDCOUNTER - 1;
          end if;
        else
          CE_DATA_INTA <= '0';
        end if;
      else
        CE_DATA      <= ALL_CE;
        INC_DATA_INT <= DEBUG_IN(1);
      end if;
    end if;
  end process;

  INC_DATA <= INC_DATA_INT;

  INCDEC_DATA_OR(0) <= '0';  -- INPUT MUX - INITIALISE GENERATE LOOP OR GATES
  VALID_DATA_OR(0)  <= '0';
  BUSY_DATA_OR(0)   <= '0';

  RX_DATA_IN_FIX <= RX_DATA_IN;         -- INVERT SIGNALS AS REQUIRED
  LOOP0 : for I in 0 to (D - 1) generate

    BUSY_DATA(I) <= BUSYS(I);

    INCDEC_DATA_IM(I)   <= INCDEC_DATA(I) and MUX(I);        -- INPUT MUXES
    INCDEC_DATA_OR(I+1) <= INCDEC_DATA_IM(I) or INCDEC_DATA_OR(I);  -- AND GATES TO ALLOW JUST ONE SIGNAL THROUGH AT A TOME
    VALID_DATA_IM(I)    <= VALID_DATA(I) and MUX(I);  -- FOLLOWED BY AN OR
    VALID_DATA_OR(I+1)  <= VALID_DATA_IM(I) or VALID_DATA_OR(I);  -- FOR THE THREE INPUTS FROM EACH PD
    BUSY_DATA_OR(I+1)   <= BUSY_DATA(I) or BUSY_DATA_OR(I);  -- THE BUSY SIGNALS JUST NEED AN OR GATE

    ALL_CE(I) <= DEBUG_IN(0);

    -- RX_DATA_IN_FIX(I) <= RX_DATA_IN(I) XOR RX_SWAP_MASK(I);  -- INVERT SIGNALS AS REQUIRED

    IOB_CLK_IN : IBUFGDS generic map(
      DIFF_TERM => DIFF_TERM)
      port map (
        I  => DATAIN_P(I),
        IB => DATAIN_N(I),
        O  => RX_DATA_IN(I));

    IODELAY_M : IODELAY2 generic map(
      DATA_RATE          => "DDR",      -- <SDR>, DDR
      IDELAY_VALUE       => 0,          -- {0 ... 255}
      IDELAY2_VALUE      => 0,          -- {0 ... 255}
      IDELAY_MODE        => "NORMAL",   -- NORMAL, PCI
      ODELAY_VALUE       => 0,          -- {0 ... 255}
      IDELAY_TYPE        => "DIFF_PHASE_DETECTOR",  -- "DEFAULT", "DIFF_PHASE_DETECTOR", "FIXED", "VARIABLE_FROM_HALF_MAX", "VARIABLE_FROM_ZERO"
      COUNTER_WRAPAROUND => "WRAPAROUND",  -- <STAY_AT_LIMIT>, WRAPAROUND
      DELAY_SRC          => "IDATAIN",  -- "IO", "IDATAIN", "ODATAIN"
      SERDES_MODE        => "MASTER",   -- <NONE>, MASTER, SLAVE
      SIM_TAPDELAY_VALUE => 49)         --
      port map (
        IDATAIN  => RX_DATA_IN_FIX(I),  -- DATA FROM PRIMARY IOB
        TOUT     => open,               -- TRI-STATE SIGNAL TO IOB
        DOUT     => open,               -- OUTPUT DATA TO IOB
        T        => '1',     -- TRI-STATE CONTROL FROM OLOGIC/OSERDES2
        ODATAIN  => '0',                -- DATA FROM OLOGIC/OSERDES2
        DATAOUT  => DDLY_M(I),          -- OUTPUT DATA 1 TO ILOGIC/ISERDES2
        DATAOUT2 => open,               -- OUTPUT DATA 2 TO ILOGIC/ISERDES2
        IOCLK0   => RXIOCLKP,           -- HIGH SPEED CLOCK FOR CALIBRATION
        IOCLK1   => RXIOCLKN,           -- HIGH SPEED CLOCK FOR CALIBRATION
        CLK      => GCLK,    -- FABRIC CLOCK (GCLK) FOR CONTROL SIGNALS
        CAL      => CAL_DATA_MASTER,    -- CALIBRATE CONTROL SIGNAL
        INC      => INC_DATA,           -- INCREMENT COUNTER
        CE       => CE_DATA(I),         -- CLOCK ENABLE
        RST      => RST_DATA,           -- RESET DELAY LINE
        BUSY     => open) ;  -- OUTPUT SIGNAL INDICATING SYNC CIRCUIT HAS FINISHED / CALIBRATION HAS FINISHED

    IODELAY_S : IODELAY2 generic map(
      DATA_RATE          => "DDR",      -- <SDR>, DDR
      IDELAY_VALUE       => 0,          -- {0 ... 255}
      IDELAY2_VALUE      => 0,          -- {0 ... 255}
      IDELAY_MODE        => "NORMAL",   -- NORMAL, PCI
      ODELAY_VALUE       => 0,          -- {0 ... 255}
      IDELAY_TYPE        => "DIFF_PHASE_DETECTOR",  -- "DEFAULT", "DIFF_PHASE_DETECTOR", "FIXED", "VARIABLE_FROM_HALF_MAX", "VARIABLE_FROM_ZERO"
      COUNTER_WRAPAROUND => "WRAPAROUND",  -- <STAY_AT_LIMIT>, WRAPAROUND
      DELAY_SRC          => "IDATAIN",  -- "IO", "IDATAIN", "ODATAIN"
      SERDES_MODE        => "SLAVE",    -- <NONE>, MASTER, SLAVE
      SIM_TAPDELAY_VALUE => 49)         --
      port map (
        IDATAIN  => RX_DATA_IN_FIX(I),  -- DATA FROM PRIMARY IOB
        TOUT     => open,               -- TRI-STATE SIGNAL TO IOB
        DOUT     => open,               -- OUTPUT DATA TO IOB
        T        => '1',         -- TRI-STATE CONTROL FROM OLOGIC/OSERDES2
        ODATAIN  => '0',                -- DATA FROM OLOGIC/OSERDES2
        DATAOUT  => DDLY_S(I),          -- OUTPUT DATA 1 TO ILOGIC/ISERDES2
        DATAOUT2 => open,               -- OUTPUT DATA 2 TO ILOGIC/ISERDES2
        IOCLK0   => RXIOCLKP,           -- HIGH SPEED CLOCK FOR CALIBRATION
        IOCLK1   => RXIOCLKN,           -- HIGH SPEED CLOCK FOR CALIBRATION
        CLK      => GCLK,        -- FABRIC CLOCK (GCLK) FOR CONTROL SIGNALS
        CAL      => CAL_DATA_SLAVE,     -- CALIBRATE CONTROL SIGNAL
        INC      => INC_DATA,           -- INCREMENT COUNTER
        CE       => CE_DATA(I),         -- CLOCK ENABLE
        RST      => RST_DATA,           -- RESET DELAY LINE
        BUSY     => BUSYS(I)) ;  -- OUTPUT SIGNAL INDICATING SYNC CIRCUIT HAS FINISHED / CALIBRATION HAS FINISHED

    ISERDES_M : ISERDES2 generic map (
      DATA_WIDTH     => S,  -- SERDES WORD WIDTH.  THIS SHOULD MATCH THE SETTING IS BUFPLL
      DATA_RATE      => "DDR",          -- <SDR>, DDR
      BITSLIP_ENABLE => true,           -- <FALSE>, TRUE
      SERDES_MODE    => "MASTER",       -- <DEFAULT>, MASTER, SLAVE
      INTERFACE_TYPE => "NETWORKING")  -- NETWORKING, NETWORKING_PIPELINED, <RETIMED>
      port map (
        D         => DDLY_M(I),
        CE0       => '1',
        CLK0      => RXIOCLKP,
        CLK1      => RXIOCLKN,
         IOCE      => RXSERDESSTROBE,
      --  IOCE      => '1',
        RST       => RESET,
          CLKDIV    => GCLK,
        SHIFTIN   => PD_EDGE(I),
        BITSLIP   => BITSLIP,
        FABRICOUT => open,
        Q4        => MDATAOUT((8*I)+7),
        Q3        => MDATAOUT((8*I)+6),
        Q2        => MDATAOUT((8*I)+5),
        Q1        => MDATAOUT((8*I)+4),
        DFB       => open,  -- ARE THESE THE SAME AS ABOVE? THESE WERE IN JOHNS DESIGN
        CFB0      => open,
        CFB1      => open,
        VALID     => VALID_DATA(I),
        INCDEC    => INCDEC_DATA(I),
        SHIFTOUT  => CASCADE(I));

    ISERDES_S : ISERDES2 generic map(
      DATA_WIDTH     => S,  -- SERDES WORD WIDTH.  THIS SHOULD MATCH THE SETTING IS BUFPLL
      DATA_RATE      => "DDR",          -- <SDR>, DDR
      BITSLIP_ENABLE => true,           -- <FALSE>, TRUE
      SERDES_MODE    => "SLAVE",        -- <DEFAULT>, MASTER, SLAVE
      INTERFACE_TYPE => "NETWORKING")  -- NETWORKING, NETWORKING_PIPELINED, <RETIMED>
      port map (
        D         => DDLY_S(I),
        CE0       => '1',
        CLK0      => RXIOCLKP,
        CLK1      => RXIOCLKN,
           IOCE      => RXSERDESSTROBE,
      --  IOCE      => '1',
        RST       => RESET,
        CLKDIV    => GCLK,
        SHIFTIN   => CASCADE(I),
        BITSLIP   => BITSLIP,
        FABRICOUT => open,
        Q4        => MDATAOUT((8*I)+3),
        Q3        => MDATAOUT((8*I)+2),
        Q2        => MDATAOUT((8*I)+1),
        Q1        => MDATAOUT((8*I)+0),
        DFB       => open,  -- ARE THESE THE SAME AS ABOVE? THESE WERE IN JOHNS DESIGN
        CFB0      => open,
        CFB1      => open,
        VALID     => open,
        INCDEC    => open,
        SHIFTOUT  => PD_EDGE(I));

    LOOP1 : for J in S-1 downto 0 generate
      --serial data sent MSb first
      DATA_OUT((S*I)+J) <= MDATAOUT((I*8)+7-J);
    end generate;
  end generate;

end ARCH_SERDES_1_TO_N_DATA_DDR_S8_DIFF;
