--
-- VHDL Architecture nova_feb.trigger_behav.vhd
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 16:10:10 08/23/10
--
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;
use nova_feb.register_address_map.all;

entity fix_to_float is
  
  port(

    MAGNITUDE_FIXED : in  MAGNITUDE_TYPE;
    MAGNITUDE_FLOAT : out MAGNITUDE_TYPE;

    RESET  : in boolean   := false;
    CLK_64 : in std_logic := '1'
    );

-- Declarations

end fix_to_float;

--
architecture behav of fix_to_float is

  signal EXPONENT_int : unsigned(2 downto 0) := "000";
  
  
begin

  EXPONENT_int <= "111" when MAGNITUDE_FIXED(11) = '1' else
                  "110" when MAGNITUDE_FIXED(10) = '1' else
                  "101" when MAGNITUDE_FIXED(9) = '1'  else
                  "100" when MAGNITUDE_FIXED(8) = '1'  else
                  "011" when MAGNITUDE_FIXED(7) = '1'  else
                  "010" when MAGNITUDE_FIXED(6) = '1'  else
                  "001" when MAGNITUDE_FIXED(5) = '1'  else
                  "000";

  shift_bits : process (CLK_64)
  begin
    if rising_edge(CLK_64) then
      if EXPONENT_int = "00" then
        MAGNITUDE_FLOAT(4 downto 0) <= MAGNITUDE_FIXED(4 downto 0);
      else
        MAGNITUDE_FLOAT(4 downto 0) <= MAGNITUDE_FIXED(3 + to_integer(EXPONENT_int) downto to_integer(EXPONENT_int)-1);        
      end if;
      MAGNITUDE_FLOAT(7 downto 5) <= EXPONENT_int;
    end if;
  end process shift_bits;
end architecture behav;
