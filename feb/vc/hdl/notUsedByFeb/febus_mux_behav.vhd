--
-- VHDL Architecture nova_feb.febus_mux.behav
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 16:45:04 06/15/10
--
-- using Mentor Graphics HDL Designer(TM) 2009.1 (Build 12)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;
LIBRARY unisim;
USE unisim.all;
library harvard_std;
USE harvard_std.lppc_custom_fn_pkg.all;

ENTITY febus_mux IS
   PORT( 
      CLK_16      : IN     std_logic;
      DCMBUS_ADDR : IN     unsigned (15 DOWNTO 0);
      DCMBUS_READ : IN     boolean;
      DCMBUS_STRB : IN     boolean;
      SPYBUS_ADDR : IN     unsigned (15 DOWNTO 0);
      SPYBUS_READ : IN     boolean;
      SPYBUS_STRB : IN     boolean;
      FEBUS_ADDR  : OUT    unsigned (15 DOWNTO 0);
      FEBUS_READ  : OUT    boolean;
      FEBUS_STRB  : OUT    boolean;
      DCMBUS_DATA : INOUT  unsigned (15 DOWNTO 0);
      FEBUS_DATA  : INOUT  unsigned (31 DOWNTO 0);
      SPYBUS_DATA : INOUT  unsigned (31 DOWNTO 0)
   );

-- Declarations

END febus_mux ;

--
ARCHITECTURE behav OF febus_mux IS
  signal USE_SPYBUS        : boolean                      := false;

  
BEGIN

    RECEIVE_COMMAND : process (BEEBUS_CLK) is
  begin
    if FEBUS_CLK'event and FEBUS_CLK = '1' then
      FEBUS_DATA <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";

      if (FEBUS_ADDR = x"0093") and FEBUS_STRB then
        USE_SPYBUS          <= FEBUS_DATA(0);
      end if;
    end if;
  end process RECEIVE_COMMAND;
    
  BUS_SELECT: process (FEBUS_CLK) is
  begin  -- process BUS_SELECT
    if FEBUS_CLK'event and FEBUS_CLK = '1' then  -- rising clock edge
      if <reset name> = '1' then        -- synchronous reset (active high)
        
      else
        
      end if;
    end if;
  end process ;

    BUS_SELECT: process is
     begin
       if USE_SPYBUS then
         FEBUS_ADDR <= SPYBUS_ADDR;
         FEBUS_READ <= SPYBUS_READ;
         FEBUS_STRB <= SPYBUS_STRB;
         
       end if;
     end process BUS_SELECT; 
END ARCHITECTURE behav;


