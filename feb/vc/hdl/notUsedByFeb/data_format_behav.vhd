--
-- VHDL Architecture feb_p2_lib.data_format.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 15:36:38 04/ 9/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std; use harvard_std.lppc_custom_fn_pkg.all;

ENTITY data_format IS
   PORT( 
      CHAN_NUMBER      : IN     unsigned (4 DOWNTO 0);
      MAGNITUDE        : IN     signed (12 DOWNTO 0);
      TIMESTAMP        : IN     unsigned (31 DOWNTO 0);
      TRIGGER_STRB     : IN     boolean;
      GOT_ANYTHING_NEW : OUT    boolean;
      FMAT_DATA        : OUT    unsigned (15 DOWNTO 0);
      FMAT_DATA_STRB   : OUT    boolean;
      CONTINUOUS_DATA  : IN     boolean;
      CLK              : IN     std_logic
   );

-- Declarations

END data_format ;

--
architecture behav of data_format is

  type FORMAT_STATE_TYPE is (
    CHECK_FOR_NEW_DATA,
    SEND_CHAN_NUMBER,
    SEND_CHAN_MAGNITUDE,
    SEND_CHAN_TIMESTAMP_U,
    SEND_CHAN_TIMESTAMP_L
    );
  attribute enum_encoding                      : string;
  attribute enum_encoding of FORMAT_STATE_TYPE : type is "000 001 010 011 100";

  signal FMAT_STATE           : FORMAT_STATE_TYPE;
  signal FMAT_CHAN_NUMBER     : unsigned(4 downto 0);
  signal FMAT_MAGNITUDE       : signed(12 downto 0);
  signal FMAT_TIMESTAMP       : unsigned(31 downto 0);
  signal GOT_ANYTHING_NEW_int : boolean := false;

begin
  update : process (clk) is
  begin  -- process update
    if clk'event and clk = '1' then     -- rising clock edge     
      FMAT_DATA_STRB <= false;

      if CONTINUOUS_DATA then
        FMAT_DATA      <= CHAN_NUMBER(3 downto 0) & unsigned(MAGNITUDE(11 downto 0));
        FMAT_DATA_STRB <= true;
      else
        case FMAT_STATE is

          when CHECK_FOR_NEW_DATA =>
            if TRIGGER_STRB then
              FMAT_CHAN_NUMBER     <= CHAN_NUMBER;
              FMAT_MAGNITUDE       <= MAGNITUDE;
              FMAT_TIMESTAMP       <= TIMESTAMP;
              FMAT_STATE           <= SEND_CHAN_NUMBER;
              GOT_ANYTHING_NEW_int <= false;
            else
              GOT_ANYTHING_NEW_int <= true;
            end if;
            
          when SEND_CHAN_NUMBER =>
            FMAT_DATA      <= "00000000000" & FMAT_CHAN_NUMBER;
            FMAT_DATA_STRB <= true;
            FMAT_STATE     <= SEND_CHAN_MAGNITUDE;

          when SEND_CHAN_MAGNITUDE =>
            FMAT_DATA      <= "000" & unsigned(FMAT_MAGNITUDE);
            FMAT_DATA_STRB <= true;
            FMAT_STATE     <= SEND_CHAN_TIMESTAMP_U;

          when SEND_CHAN_TIMESTAMP_U =>
            FMAT_DATA      <= FMAT_TIMESTAMP(31 downto 16);
            FMAT_DATA_STRB <= true;
            FMAT_STATE     <= SEND_CHAN_TIMESTAMP_L;

          when SEND_CHAN_TIMESTAMP_L =>
            FMAT_DATA            <= FMAT_TIMESTAMP(15 downto 0);
            FMAT_DATA_STRB       <= true;
            FMAT_STATE           <= CHECK_FOR_NEW_DATA;
            GOT_ANYTHING_NEW_int <= true;
            
          when others =>
            FMAT_STATE <= CHECK_FOR_NEW_DATA;
            
        end case;
      end if;
    end if;
  end process update;

  GOT_ANYTHING_NEW <= GOT_ANYTHING_NEW_int and not TRIGGER_STRB;

end architecture behav;
