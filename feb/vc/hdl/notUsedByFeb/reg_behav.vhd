--
-- VHDL Architecture feb_p2_lib.reg.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 16:41:41 10/26/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity reg is
  port(
    BEEBUS_ADDR : in    unsigned (15 downto 0);
    BEEBUS_DATA : inout unsigned (15 downto 0);
    BEEBUS_READ : in    boolean;
    BEEBUS_CLK  : in    std_logic;
    BEEBUS_STRB : in    boolean;
    REG_DATA    : inout std_logic_vector (15 downto 0)
    );

-- Declarations

end reg;

--
architecture behav of reg is

begin
  RECEIVE_COMMAND : process (BEEBUS_CLK) is
  begin
    if BEEBUS_CLK'event and BEEBUS_CLK = '1' then
      BEEBUS_DATA <= "ZZZZZZZZZZZZZZZZ";
      if (BEEBUS_ADDR = x"aa55")then
        if BEEBUS_READ then
          BEEBUS_DATA <= unsigned(REG_DATA);
        elsif BEEBUS_STRB then
          REG_DATA <= std_logic_vector(BEEBUS_DATA);
        end if;
      end if;
    end if;
  end process RECEIVE_COMMAND;

end architecture behav;


