--
-- VHDL Architecture feb_p2_lib.adc_interface.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 11:43:21 01/22/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;
use nova_feb.register_address_map.all;

entity GBT_SER_RX is
  port (
    DATAIN_P      : in  std_logic_vector(8 downto 0);
    DATAIN_N      : in  std_logic_vector(8 downto 0);
    CLKIN_P       : in  std_logic;
    CLKIN_N       : in  std_logic;
    ADC_CHAN_DATA : out MAGNITUDE_VECTOR_TYPE(8 downto 0);
    FRAMEOUT      : out std_logic;
    BITSLIP_FORCE : in  boolean;
    RESET         : in  std_logic;
    CLK_16        : in  std_logic;
    CLK_32        : in  std_logic
    );
end GBT_SER_RX;

architecture ARCH of GBT_SER_RX is

  type ADC_MS_TEMP_TYPE is array (15 downto 0) of std_logic_vector(5 downto 0);

  constant S  : integer := 6;           -- SET THE SERDES FACTOR
  constant D  : integer := 9;           -- SET THE NUMBER OF INPUTS AND OUTPUTS
  constant DS : integer := (D*S)-1;  -- USED FOR BUS WIDTHS = SERDES FACTOR * NUMBER OF INPUTS - 1

  signal ADC_MS_TEMP     : ADC_MS_TEMP_TYPE;
  signal RST             : std_logic;
  signal RXD             : std_logic_vector(DS downto 0);  -- DATA FROM SERDESES
  signal RXR             : std_logic_vector(DS downto 0);  -- SIGNALISTERED DATA FROM SERDESES
  signal STATE           : std_logic;
  signal BSLIP           : std_logic;
  signal COUNT           : unsigned(3 downto 0);
  signal RXIOCLKP        : std_logic;
  signal RXIOCLKN        : std_logic;
  signal RX_SERDESSTROBE : std_logic;
  signal RX_BUFG_X1      : std_logic;

begin

  RST <= RESET;                         -- ACTIVE HIGH RESET PIN

--APD_DATA_IN_MS : process (CLK_16) is
--  begin
--    if CLK_16'event and CLK_16 = '0' then
--      for ADC_INDEX in 8 downto 0 loop
--        ADC_CHAN_DATA(ADC_INDEX) <= unsigned(ADC_MS_TEMP(ADC_INDEX)) &
--                                    unsigned(RXR((ADC_INDEX * 6)+5 downto ADC_INDEX * 6));
--      end loop;  -- ADC_TEMP_INDEX
--    end if;
--  end process;

  APD_DATA_IN_LS : process (CLK_32) is
  begin
    if rising_edge(CLK_32) then
      for ADC_INDEX in 8 downto 0 loop
        if RXR(DS) = '1' then
          ADC_MS_TEMP(ADC_INDEX) <= RXR((ADC_INDEX * 6)+5 downto ADC_INDEX * 6);
        else
          ADC_CHAN_DATA(ADC_INDEX) <= unsigned(ADC_MS_TEMP(ADC_INDEX)) &
                                      unsigned(RXR((ADC_INDEX * 6)+5 downto ADC_INDEX * 6));
        end if;
      end loop;  -- ADC_TEMP_INDEX
    end if;
  end process;


  INST_CLKIN : entity NOVA_FEB.SERDES_1_TO_N_CLK_DDR_S8_DIFF
    generic map(
      S => S
      )
    port map (
      CLKIN_P         => CLKIN_P,
      CLKIN_N         => CLKIN_N,
      RXIOCLKP        => RXIOCLKP,
      RXIOCLKN        => RXIOCLKN,
      RX_SERDESSTROBE => RX_SERDESSTROBE,
      RX_BUFG_X1      => RX_BUFG_X1);

  INST_DATAIN : entity NOVA_FEB.SERDES_1_TO_N_DATA_DDR_S8_DIFF
    generic map(
      S => S,
      D => D
      )
    port map (
      USE_PHASE_DETECTOR => '1',        -- '1' ENABLES THE PHASE DETECTOR LOGIC
      DATAIN_P           => DATAIN_P,
      DATAIN_N           => DATAIN_N,
      RXIOCLKP           => RXIOCLKP,
      RXIOCLKN           => RXIOCLKN,
      RXSERDESSTROBE     => RX_SERDESSTROBE,
      clk_32             => clk_32,
      GCLK               => RX_BUFG_X1,
      BITSLIP            => BSLIP,
      RESET              => RST,
      DATA_OUT           => RXD,
      DEBUG_IN           => "00",
      DEBUG              => open
      );

  process (RX_BUFG_X1, RST)             -- EXAMPLE BITSLIP LOGIC, IF REQUIRED
--    VARIABLE RXD_FRAME : STD_LOGIC_VECTOR(5 DOWNTO 0);
--  process (clk_32, RST)             -- EXAMPLE BITSLIP LOGIC, IF REQUIRED
  begin
    if RST = '1' then
      STATE <= '0';
      BSLIP <= '0';
      COUNT <= "0000";
    elsif RX_BUFG_X1'event and RX_BUFG_X1 = '1' then
--    elsif rising_edge(CLK_32) then
      if STATE = '0' then
        if ((RXD(DS downto DS - 5) /= "111111") and
            (RXD(DS downto DS - 5) /= "000000")) then
--        if BITSLIP_FORCE then
          BSLIP <= '1';                 -- BITSLIP NEEDED
          STATE <= '1';
          COUNT <= "0000";
        end if;
      elsif STATE = '1' then
        BSLIP <= '0';                   -- BITSLIP LOW
        COUNT <= COUNT + 1;
        if COUNT = "1111" then
          STATE <= '0';
        end if;
      end if;
    end if;
  end process;

--  process (RX_BUFG_X1)                  -- PROCESS RECEIVED DATA
--  process (CLK_32)                  -- PROCESS RECEIVED DATA
--  begin
--    if RX_BUFG_X1'event and RX_BUFG_X1 = '1' then
----    if rising_edge(CLK_32) then
--      RXR      <= RXD;
--      FRAMEOUT <= RXD(DS);
--    end if;
--  end process;

  RXR      <= RXD;
--  FRAMEOUT <= RXD(DS);
  FRAMEOUT <= RXD(0);

end ARCH;

