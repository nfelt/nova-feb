--
-- VHDL ARCHITECTURE FEB_P2_LIB.COMMAND_INTERP.BEHAV
--
-- CREATED:
--          BY - NATE.UNKNOWN (HEPLPC2)
--          AT - 16:46:10 02/14/2007
-------------------------------------------------------------------------------
-- FEB IS CONTROLLED USING A COMBINATION OF REGISTER SETTINGS AND
-- COMMANDS WRITTEN TO THE COMMAND REGISTER
--
-- TECC IS ENABLED OR DISABLED BY ISSUING THE APPROPRIATE COMMAND
--
-- TECC ERROR CONDITION CAN BE RESET BY ISSUING A DISABLE AND ENABLE
--
-- SPI DEVICE COMMUNICATION INITIATED BY FORMING A GENERIC SPI INSTRUCTION
-- AND SENDING IT TO THE SPI INTERFACE
--
-- TIMING COMMAND REGISTER ADDED FOR DAQ HARDWARE COMPATIBILITY
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library UNISIM;
use UNISIM.all;
library HARVARD_STD;
use HARVARD_STD.LPPC_CUSTOM_FN_PKG.all;
library NOVA_FEB;
use NOVA_FEB.REGISTER_ADDRESS_MAP.all;

entity COMMAND_INTERP is
  port(
    BEEBUS_ADDR : in    unsigned (15 downto 0);
    BEEBUS_DATA : inout unsigned (15 downto 0);
    BEEBUS_READ : in    boolean;
    BEEBUS_STRB : in    boolean;

    APD_DATA_BUFFER_FULL : in  boolean;
    DCM_SYNC             : in  boolean;
    DCM_SYNC_EN          : out boolean := true;
    ENABLE_DAQ           : out boolean := false;
    UPDATE_TEMP_REG      : out boolean := false;
    SET_ASIC             : out boolean := false;

    DAQ_SPI_COMMAND : out std_logic_vector (18 downto 0);
    DAQ_SPI_WSTB    : out boolean := false;

    ENABLE_TM_PKT     : out boolean := false;
    ENABLE_STATUS_PKT : out boolean := false;
    TECC_ENABLE       : out boolean := false;
    TEC_ERR_SHDN      : in  boolean := false;
    TEC_ERR_RESET     : out boolean := false;

    ADC_PWR_CMD       : out boolean := false;
    ADC_PWR_DIRECTION : out std_logic := '1';

    BITSLIP_FORCE : out boolean := false;
    RESET_FEB     : out boolean := false;
    CLK_16        : in  std_logic;
    CLK_32        : in  std_logic
    );

-- DECLARATIONS

end COMMAND_INTERP;

--
architecture BEHAV of COMMAND_INTERP is

  type   CONTOLLER_STATE_TYPE is (IDLE, STOPPING, STOP, RUNNING);
  signal CONTROLLER_STATE      : CONTOLLER_STATE_TYPE  := IDLE;
  signal BEEBUS_DATA_INT       : unsigned(15 downto 0) := X"0000";
  signal BEEBUS_READ_DEL       : boolean               := false;
  signal HIGH_VOLTAGE          : unsigned(11 downto 0) := X"000";
  signal SETPOINT              : unsigned(11 downto 0) := X"000";
  signal DCM_SYNC_DEL1         : boolean               := false;
  signal DCM_SYNC_CLK16        : boolean               := false;
  signal DCM_SYNC_EN_INT       : boolean               := false;
  signal ENABLE_TM_PKT_INT     : boolean               := false;
  signal ENABLE_TM_PKT_PRESET  : boolean               := false;
  signal ENABLE_STATUS_PKT_INT : boolean               := false;
  signal TECC_ENABLE_INT       : boolean               := false;
  
begin
  BEEBUS_DATA <= BEEBUS_DATA_INT when BEEBUS_READ_DEL else "ZZZZZZZZZZZZZZZZ";
  SYNC_DEL32 : process (CLK_32) is
  begin
    if rising_edge(CLK_32) then
      DCM_SYNC_DEL1 <= DCM_SYNC after 1 ns;
    end if;
  end process SYNC_DEL32;

  RECEIVE_COMMAND : process (CLK_16) is
  begin
    if rising_edge(CLK_16) then
      BEEBUS_READ_DEL <= false;
      RESET_FEB       <= false;
      SET_ASIC        <= false;
      TEC_ERR_RESET   <= false;
      ENABLE_DAQ      <= CONTROLLER_STATE = RUNNING;
      DAQ_SPI_WSTB    <= false;
      BITSLIP_FORCE   <= false;
      ADC_PWR_CMD     <= false;

      if DCM_SYNC_DEL1 or DCM_SYNC then
        ENABLE_TM_PKT_INT <= ENABLE_TM_PKT_PRESET;
        DCM_SYNC_EN_INT   <= false;
      end if;
      ENABLE_TM_PKT <= ENABLE_TM_PKT_INT;
      DCM_SYNC_EN   <= DCM_SYNC_EN_INT;
      --INTERP COMMANDS SENT TO COMMAND REGISTER
      if (BEEBUS_ADDR = CMD_ADDR and BEEBUS_STRB) then
        case BEEBUS_DATA is
          when CMD_GET_TEMP =>
            DAQ_SPI_COMMAND <= std_logic_vector(CMD_READ_TEMP) & "0000" & X"000";
            DAQ_SPI_WSTB    <= true;

          when CMD_RESET_FEB =>
            RESET_FEB <= true;

          when CMD_SET_ASIC =>
            SET_ASIC <= true;

          when CMD_START_DAQ =>
            CONTROLLER_STATE <= RUNNING;

          when CMD_STOP_DAQ =>
            CONTROLLER_STATE <= STOPPING;

          when CMD_DISABLE_TECC =>
            TECC_ENABLE_INT <= false;
            TEC_ERR_RESET   <= true;

          when CMD_ENABLE_TECC =>
            TECC_ENABLE_INT <= true;
          when CMD_GET_DRIVE_MON =>
            DAQ_SPI_COMMAND <= std_logic_vector(CMD_READ_ADC) & "0000" & X"000";
            DAQ_SPI_WSTB    <= true;

          when CMD_GET_TEMP_MON =>
            DAQ_SPI_COMMAND <= std_logic_vector(CMD_READ_ADC) & "1000" & X"000";
            DAQ_SPI_WSTB    <= true;
            
          when CMD_SER_NUM_WRITE_EN =>
            DAQ_SPI_COMMAND <= std_logic_vector(CMD_WREN_SER_NUM) & X"0000";
            DAQ_SPI_WSTB    <= true;
            
          when CMD_SER_NUM_ADR_RESET =>
            DAQ_SPI_COMMAND <= std_logic_vector(CMD_RST_SER_NUM_PTR) & X"0000";
            DAQ_SPI_WSTB    <= true;
            
          when CMD_START_TIME =>
            --ENABLE TIME MARKERS ON BIT SET "PRESET", ENABLE ON SYNC
            ENABLE_TM_PKT_PRESET <= true;
            
          when CMD_STOP_TIME =>
            --DISABLE TIME MARKERS ON BIT RESET
            ENABLE_TM_PKT_PRESET <= false;
            ENABLE_TM_PKT_INT    <= false;
            
          when CMD_BITSLIP_FORCE =>
            BITSLIP_FORCE <= true;
            
          when CMD_ADC_PWR_UP =>
            ADC_PWR_CMD       <= true;
            ADC_PWR_DIRECTION <= '1';
            
          when CMD_ADC_PWR_DOWN =>
            ADC_PWR_CMD       <= true;
            ADC_PWR_DIRECTION <= '0';
            
          when others
 => null;
        end case;

        
      else
        if CONTROLLER_STATE = STOPPING then
          CONTROLLER_STATE <= IDLE;
        end if;
        if CONTROLLER_STATE = RUNNING and APD_DATA_BUFFER_FULL then
          CONTROLLER_STATE <= STOPPING;
        end if;

        case BEEBUS_ADDR is
          when HIGH_VOLTAGE_ADJ_ADDR =>
            if BEEBUS_READ then
              BEEBUS_DATA_INT <= X"0" & (HIGH_VOLTAGE);
              BEEBUS_READ_DEL <= true;
            elsif BEEBUS_STRB then
              HIGH_VOLTAGE    <= BEEBUS_DATA(11 downto 0);
              DAQ_SPI_COMMAND <= std_logic_vector(CMD_WRITE_DAC) & "0000" &std_logic_vector((not(BEEBUS_DATA(11 downto 0))));
              DAQ_SPI_WSTB    <= true;
            end if;

          when SETPOINT_ADDR =>
            if BEEBUS_READ then
              BEEBUS_DATA_INT <= X"0" & SETPOINT;
              BEEBUS_READ_DEL <= true;
            elsif BEEBUS_STRB then
              SETPOINT        <= BEEBUS_DATA(11 downto 0);
              DAQ_SPI_COMMAND <= std_logic_vector(CMD_WRITE_DAC) & "1000" & std_logic_vector(BEEBUS_DATA(11 downto 0));
              DAQ_SPI_WSTB    <= true;
            end if;

          when SERIAL_NUMBER_ADDR =>
            if BEEBUS_READ then
              DAQ_SPI_COMMAND <= std_logic_vector(CMD_READ_SER_NUM) & X"0000";
              DAQ_SPI_WSTB    <= true;
            elsif BEEBUS_STRB then
              DAQ_SPI_COMMAND <= std_logic_vector(CMD_WRITE_SER_NUM) & std_logic_vector(BEEBUS_DATA);
              DAQ_SPI_WSTB    <= true;
            end if;

          when TIMING_CMD_ADDRESS =>
            if BEEBUS_READ then
              BEEBUS_DATA_INT <= "0000000000"
                                 & BOOL2SL(ENABLE_STATUS_PKT_INT)
                                 & "00"& BOOL2SL(DCM_SYNC_EN_INT)
                                 & BOOL2SL(ENABLE_TM_PKT_PRESET)
                                 & '0';
              BEEBUS_READ_DEL <= true;
            elsif BEEBUS_STRB then
              --DISABLE TIME MARKERS ON BIT RESET
              if BEEBUS_DATA(1) = '0' then
                ENABLE_TM_PKT_INT <= false;
              end if;
              --SET OR RESET TIME MARKER "PRESET", ENABLE ON SYNC
              ENABLE_TM_PKT_PRESET  <= BEEBUS_DATA(1) = '1';
              --SET OR RESET DCM SYNC "PRESET", ENABLE ON SYNC
              DCM_SYNC_EN_INT       <= BEEBUS_DATA(2) = '1';
              ENABLE_STATUS_PKT_INT <= BEEBUS_DATA(5) = '1';
            end if;

          when others => null;
        end case;
      end if;
      
    end if;
  end process RECEIVE_COMMAND;
  ENABLE_STATUS_PKT <= ENABLE_STATUS_PKT_INT;
  TECC_ENABLE       <= TECC_ENABLE_INT and not TEC_ERR_SHDN;
end architecture BEHAV;


