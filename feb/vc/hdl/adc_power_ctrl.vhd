--
-- VHDL ARCHITECTURE NOVA_FEB.SPI_INTERFACE.BEHAV
--
-- CREATED:
--          BY - NATE.NATE (HEPLPC2)
--          AT - 11:13:08 04/14/09
--
-------------------------------------------------------------------------------
--
-- NEEDS TO HAVE CLOCK DOMAIN SYNC IF BEEBUS IS CHANGED TO ASYNC CLK
--
-- SPI DEVICE INTERFACE CONTROLLED USING GENERIC SPI COMMAND GENERATED AND SENT
-- BY THE COMMAND INTERPRETER
--
-- THE REGISTER ADDRESS MAP CONTAINS A LIST OF SPI
-- OPERATIONS THAT WILL EXECUTE ON A SET INTERVAL OF ~30 S
-- 
-- THE CURRENT LIST IS GET DRIVE MON, GET TEMP MON, AND GET TEMPERATURE
-- WHILE AUTO-UPDATING, SPI OPERATIONS REQUESTED BY SOFTWARE DAQ WILL BE BUFFERED
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library UNISIM;
use UNISIM.all;
library HARVARD_STD;
use HARVARD_STD.LPPC_CUSTOM_FN_PKG.all;
library NOVA_FEB;
use NOVA_FEB.REGISTER_ADDRESS_MAP.all;

entity adc_power_ctrl is
  port(
    ADC_PWR_CMD       : in boolean;
    ADC_PWR_DIRECTION : in std_logic;

    SPI_PWRUP_DONE    : in boolean;
    SERIAL_NUMBER_DEC : in unsigned(7 downto 0);

    ADC_ENABLE : out boolean := false;
    CLK_16     : in  std_logic
    );


end adc_power_ctrl;
architecture BEHAV of adc_power_ctrl is

  signal ADC_PWR_DIRECTION_LATCH : std_logic             := '1';
  signal RAMP_CNT                : unsigned(17 downto 0) := "00" & x"0000";
  signal SPI_PWRUP_DONE_16       : boolean               := false;
begin
  
  PWR_RAMP : process (CLK_16)
  begin
    if rising_edge(CLK_16) then
      SPI_PWRUP_DONE_16 <= SPI_PWRUP_DONE;
      if ADC_PWR_CMD then
        RAMP_CNT                <= "00" & x"0000";
        ADC_PWR_DIRECTION_LATCH <= ADC_PWR_DIRECTION;

      elsif SPI_PWRUP_DONE_16 then
        if RAMP_CNT(17 downto 10) = SERIAL_NUMBER_DEC then
          ADC_ENABLE <= ADC_PWR_DIRECTION_LATCH = '1';
        else
          RAMP_CNT <= RAMP_CNT + 1;
        end if;
      end if;
    end if;
  end process PWR_RAMP;
end architecture BEHAV;
