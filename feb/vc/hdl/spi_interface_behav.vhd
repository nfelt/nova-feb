--
-- VHDL ARCHITECTURE NOVA_FEB.SPI_INTERFACE.BEHAV
--
-- CREATED:
--          BY - NATE.NATE (HEPLPC2)
--          AT - 11:13:08 04/14/09
--
-------------------------------------------------------------------------------
--
-- NEEDS TO HAVE CLOCK DOMAIN SYNC IF BEEBUS IS CHANGED TO ASYNC CLK
--
-- SPI DEVICE INTERFACE CONTROLLED USING GENERIC SPI COMMAND GENERATED AND SENT
-- BY THE COMMAND INTERPRETER
--
-- THE REGISTER ADDRESS MAP CONTAINS A LIST OF SPI
-- OPERATIONS THAT WILL EXECUTE ON A SET INTERVAL OF ~30 S
-- 
-- THE CURRENT LIST IS GET DRIVE MON, GET TEMP MON, AND GET TEMPERATURE
-- WHILE AUTO-UPDATING, SPI OPERATIONS REQUESTED BY SOFTWARE DAQ WILL BE BUFFERED
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library UNISIM;
use UNISIM.all;
library HARVARD_STD;
use HARVARD_STD.LPPC_CUSTOM_FN_PKG.all;
library NOVA_FEB;
use NOVA_FEB.REGISTER_ADDRESS_MAP.all;

entity SPI_INTERFACE is
  port(
    BEEBUS_ADDR : in    unsigned (15 downto 0);
    BEEBUS_DATA : inout unsigned (15 downto 0);
    BEEBUS_READ : in    boolean;
    BEEBUS_STRB : in    boolean;

    UPDATE_TEMP_REG : in boolean;
    CURRENT_TIME    : in unsigned (31 downto 0);

    DAQ_SPI_COMMAND : in  std_logic_vector(18 downto 0);
    DAQ_SPI_WSTB    : in  boolean;
    SPI_PROC_BUSY   : out boolean;

    SPI_DIN  : out std_logic;
    SPI_DOUT : in  std_logic;
    SPI_SCLK : out std_logic;

    TEC_DAC_LDAC      : out boolean;
    TEC_DAC_SYNC      : out boolean;
    TEC_ADC_CS        : out boolean;
    TEC_ADC_CH1_B_CH2 : out std_logic;
    TEMP_SENSOR_CS    : out boolean;
    SER_NUM_CS        : out boolean;

    SPI_PWRUP_DONE    : out boolean := false;
    SERIAL_NUMBER_DEC : out unsigned(7 downto 0);

    TEC_ERR_SHDN  : out boolean;
    TEC_ERR_RESET : in  boolean;
    RESET         : in  boolean;
    T_67S         : in  boolean;
    CLK_4         : in  std_logic;
    CLK_16        : in  std_logic
    );


end SPI_INTERFACE;
architecture BEHAV of SPI_INTERFACE is

  component ODDR2
    generic (
      DDR_ALIGNMENT : string;
      INIT          : bit;
      SRTYPE        : string
      );
    port (
      Q  : out std_logic;
      C0 : in  std_logic;
      C1 : in  std_logic;
      CE : in  std_logic;
      D0 : in  std_logic;
      D1 : in  std_logic;
      R  : in  std_logic;
      S  : in  std_logic
      );
  end component;

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;

  component BUFGCE_1
    port(
      O  : out std_ulogic;
      CE : in  std_ulogic;
      I  : in  std_ulogic
      );
  end component;
  component SPI_CMD_FIFO
    port (
      RST    : in  std_logic;
      WR_CLK : in  std_logic;
      RD_CLK : in  std_logic;
      DIN    : in  std_logic_vector(18 downto 0);
      WR_EN  : in  std_logic;
      RD_EN  : in  std_logic;
      DOUT   : out std_logic_vector(18 downto 0);
      FULL   : out std_logic;
      EMPTY  : out std_logic
      );
  end component;

  type SPI_INTERFACE_STATE_TYPE is (
    READY,
    DECODE_SPI_RAW,
    SHIFT_BUSY,
    CS_CLEAR,
    UPDATE_REG
    );
  signal SPI_INTERFACE_STATE : SPI_INTERFACE_STATE_TYPE := READY;

  signal BEEBUS_READ_DEL  : boolean                   := false;
  signal BEEBUS_DATA_INT  : unsigned(15 downto 0)     := X"0000";
  signal SPI_COMMAND      : unsigned(2 downto 0)      := "000";
  signal SPI_DATA         : unsigned(11 downto 0)     := X"000";
  signal SPI_SREG         : unsigned(31 downto 0)     := X"00000000";
  signal S_CMD_BUSY_SL    : std_logic                 := '0';
  signal TEC_DAC_LDAC_INT : boolean                   := false;
  signal TEC_DAC_LDAC_DEL : boolean                   := false;
  signal DC_CHAN_SELECT   : std_logic                 := '0';
  signal BIT_COUNT        : integer range 31 downto 0 := 4;
  signal NOT_CLK_4        : std_logic                 := '0';

  signal TEMP_MON_TS           : unsigned(31 downto 0) := X"00000000";
  signal DRIVE_MON_TS          : unsigned(31 downto 0) := X"00000000";
  signal TEMP_TS               : unsigned(31 downto 0) := X"00000000";
  signal DRIVE_MON_DATA        : unsigned(11 downto 0) := X"A5E";
  signal TEMP_MON_DATA         : unsigned(11 downto 0) := X"FFF";
  signal TEMP_DATA             : unsigned(15 downto 0) := X"BA5E";
  signal SERIAL_NUMBER         : unsigned(15 downto 0) := X"aaaa";
  signal SERIAL_NUMBER_DEC_int : unsigned(7 downto 0)  := X"FF";
  signal SER_NUM_MEM_ADDR      : unsigned(5 downto 0)  := "000000";
  signal SER_NUM_MEM_DATA      : unsigned(15 downto 0) := X"0000";

  signal TEC_DRIVE_ERR_THRSHLD : unsigned(2 downto 0) := "000";
  signal TEC_DRIVE_WARN_CNT    : unsigned(4 downto 0) := "00000";
  signal TEC_ERR_SHDN_INT      : boolean              := false;

  signal SPI_RAW : std_logic_vector(18 downto 0);

  signal AUTO_UPD      : boolean                   := false;
  signal AUTO_UPD_PTR  : integer range 15 downto 0 := 0;
  signal AUTO_UPD_STRB : boolean                   := false;

  signal PWRUP_SEQ_DONE  : boolean                   := false;
  signal PWRUP_SEQ_PTR   : integer range 31 downto 0 := PWRUP_SEQ_LIST'left;
  signal PWRUP_SEQ_STRB  : boolean                   := false;
  signal PWRUP_SEQ_CNT   : unsigned(15 downto 0)     := X"0000";
  signal ADC_DISABLE_INT : boolean                   := false;

  signal USE_LONG_SREG      : boolean   := false;
  signal SPI_CMD_FIFO_DOUT  : std_logic_vector(18 downto 0);
  signal SPI_CMD_FIFO_DIN   : std_logic_vector(18 downto 0);
  signal SPI_CMD_FIFO_FULL  : std_logic := '0';
  signal SPI_CMD_FIFO_EMPTY : std_logic := '0';
  signal SPI_CMD_FIFO_STRB  : std_logic := '0';

  signal T_67S_DEL : boolean := false;
  
begin
  BEEBUS_DATA <= BEEBUS_DATA_INT when BEEBUS_READ_DEL else "ZZZZZZZZZZZZZZZZ";

  SPI_COMMAND      <= unsigned(SPI_RAW(18 downto 16));
  SER_NUM_MEM_DATA <= unsigned(SPI_RAW(15 downto 0));
  DC_CHAN_SELECT   <= SPI_RAW(15);
  SPI_DATA         <= unsigned(SPI_RAW(11 downto 0));
  SPI_CMD_FIFO_INST : SPI_CMD_FIFO
    port map (
      RST    => BOOL2SL(RESET),
      WR_CLK => CLK_16,
      RD_CLK => CLK_4,
      DIN    => DAQ_SPI_COMMAND,
      WR_EN  => BOOL2SL(DAQ_SPI_WSTB),
      RD_EN  => SPI_CMD_FIFO_STRB,
      DOUT   => SPI_CMD_FIFO_DOUT,
      FULL   => SPI_CMD_FIFO_FULL,
      EMPTY  => SPI_CMD_FIFO_EMPTY
      );

-- At powerup get the last 2 digits of SN
-- and any other SPI operation in PWRUP list
  PWRUP_SEQ : process (CLK_4)
  begin
    if CLK_4'event and CLK_4 = '1' then
      
      if PWRUP_SEQ_STRB then
        --GO TO NEXT OPERATION OR END        
        if PWRUP_SEQ_PTR = 0 then
          PWRUP_SEQ_DONE                    <= true;  --END OF UPDATE,
          --RESULTS FROM LAST AUTO UPDATE COMMAND NOT VALID YET
          --we are here at START of last command.
          --get 2nd from last nibble of SN
          SERIAL_NUMBER_DEC_int(3 downto 0) <= SERIAL_NUMBER(11 downto 8);
        else
          --GO TO NEXT COMMAND IN LIST
          PWRUP_SEQ_PTR                     <= PWRUP_SEQ_PTR-1;
          --get last nibble of SN
          SERIAL_NUMBER_DEC_int(7 downto 4) <= SERIAL_NUMBER(3 downto 0);
        end if;
      end if;

      
    end if;
  end process PWRUP_SEQ;

  AUTO_UPD_TIMER : process (CLK_4)
  begin
    if CLK_4'event and CLK_4 = '1' then
      T_67S_DEL <= T_67S;
      --DO THESE THINGS ONCE EVERY ~33 SECONDS
      if (T_67S_DEL /= T_67S) then
        AUTO_UPD     <= true;
        -- POINT TO FIRST COMMAND IN AUTOUPDATE LIST
        AUTO_UPD_PTR <= AUTO_UPD_LIST'left;
        --WAIT UNTIL END OF AN SPI OPERATION
      elsif AUTO_UPD_STRB then
        --GO TO NEXT OPERATION OR END
        if AUTO_UPD_PTR = 0 then
          AUTO_UPD <= false;            --END OF UPDATE, CHECK RESULTS
          --RESULTS FROM LAST AUTO UPDATE COMMAND NOT VALID YET
          --we are here at START of last command.
          --read temperature is last command so TECC values are valid
          if (DRIVE_MON_DATA < TEC_DRIVE_WARN_THRSHLD) then
            --put this part before the 30 sec tick to allow force update
            TEC_DRIVE_WARN_CNT <= "00000";
          elsif not TEC_ERR_SHDN_INT then
            TEC_DRIVE_WARN_CNT <= TEC_DRIVE_WARN_CNT+1;
          end if;
        else
          --GO TO NEXT COMMAND IN LIST
          AUTO_UPD_PTR <= AUTO_UPD_PTR-1;
        end if;
      end if;
    end if;
  end process AUTO_UPD_TIMER;

  --DETECT ERROR CONDITION
  TEC_ERR_LATCH : process (CLK_16)
  begin  -- USE CLK 16 FOR RESET SIGNAL
    if CLK_16'event and CLK_16 = '1' then
      TEC_ERR_SHDN_INT <= ((TEC_DRIVE_WARN_CNT > ('0' & TEC_DRIVE_ERR_THRSHLD & '1'))
                           or (TEMP_MON_DATA < TEC_TEMP_ERR_THRSHLD)
                           or TEC_ERR_SHDN_INT)
                          and not TEC_ERR_RESET;
    end if;
  end process TEC_ERR_LATCH;

  -- DAQ Software can read the results of the SPI operation here
  RECEIVE_COMMAND : process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '1' then
      BEEBUS_READ_DEL <= false;
      --READ ONLY REGISTERS
      if BEEBUS_READ then
        case BEEBUS_ADDR is
          when DRIVE_MONITOR_ADDR =>
            BEEBUS_DATA_INT <= X"0" & DRIVE_MON_DATA;
            BEEBUS_READ_DEL <= true;
          when DRIVE_MONITOR_LTS_ADDR =>
            BEEBUS_DATA_INT <= DRIVE_MON_TS(15 downto 0);
            BEEBUS_READ_DEL <= true;
          when DRIVE_MONITOR_UTS_ADDR =>
            BEEBUS_DATA_INT <= DRIVE_MON_TS(31 downto 16);
            BEEBUS_READ_DEL <= true;
          when TEMP_MONITOR_ADDR =>
            BEEBUS_DATA_INT <= X"0" & TEMP_MON_DATA;
            BEEBUS_READ_DEL <= true;
          when TEMP_MONITOR_LTS_ADDR =>
            BEEBUS_DATA_INT <= TEMP_MON_TS(15 downto 0);
            BEEBUS_READ_DEL <= true;
          when TEMP_MONITOR_UTS_ADDR =>
            BEEBUS_DATA_INT <= TEMP_MON_TS(31 downto 16);
            BEEBUS_READ_DEL <= true;
          when TEMP_ADDR =>
            BEEBUS_DATA_INT <= TEMP_DATA;
            BEEBUS_READ_DEL <= true;
          when TEMP_LTS_ADDR =>
            BEEBUS_DATA_INT <= TEMP_TS(15 downto 0);
            BEEBUS_READ_DEL <= true;
          when TEMP_UTS_ADDR =>
            BEEBUS_DATA_INT <= TEMP_TS(31 downto 16);
            BEEBUS_READ_DEL <= true;
          when SERIAL_NUMBER_ADDR =>
            BEEBUS_DATA_INT <= SERIAL_NUMBER;
            BEEBUS_READ_DEL <= true;
          when SERIAL_NUMBER_DEC_ADDR =>
            BEEBUS_DATA_INT <= x"00" & SERIAL_NUMBER_DEC_int;
            BEEBUS_READ_DEL <= true;
          when others => null;
        end case;
      end if;

      -- READ WRITE REGISTERS
      if BEEBUS_ADDR = TEC_DRIVE_ERR_THRSHLD_ADDR then
        if BEEBUS_READ then
          BEEBUS_DATA_INT <= X"000" & '0' & TEC_DRIVE_ERR_THRSHLD;
          BEEBUS_READ_DEL <= true;
        elsif BEEBUS_STRB then
          TEC_DRIVE_ERR_THRSHLD <= BEEBUS_DATA(2 downto 0);
        end if;
      end if;
    end if;
  end process RECEIVE_COMMAND;

  -- SPI BUS CLOCKED PROCESS
  SDATA_IO : process (CLK_4) is
  begin
    if CLK_4'event and CLK_4 = '1' then
      SPI_CMD_FIFO_STRB <= '0';
      AUTO_UPD_STRB     <= false;
      PWRUP_SEQ_STRB    <= false;
      case SPI_INTERFACE_STATE is
        when READY =>

          --There are 3 sources of SPI Commands

          -- IF powerup get last digit of SN
          if not PWRUP_SEQ_DONE then
            SPI_RAW             <= PWRUP_SEQ_LIST(PWRUP_SEQ_PTR);
            PWRUP_SEQ_STRB      <= true;
            SPI_INTERFACE_STATE <= DECODE_SPI_RAW;

            -- IF IT'S TIME OF AN AUTO UPDATE THIS IS PRIORITY
          elsif AUTO_UPD then
            SPI_RAW             <= AUTO_UPD_LIST(AUTO_UPD_PTR);
            AUTO_UPD_STRB       <= true;
            SPI_INTERFACE_STATE <= DECODE_SPI_RAW;

            -- ELSE TAKE COMMAND FORM COMMAND BUFFER
          elsif SPI_CMD_FIFO_EMPTY = '0' then
            SPI_CMD_FIFO_STRB   <= '1';
            SPI_RAW             <= SPI_CMD_FIFO_DOUT;
            SPI_INTERFACE_STATE <= DECODE_SPI_RAW;
          end if;

          --DECODE COMMAND STRING AND BUILD SPI SERIAL STRING TO BE SENT TO SPI
          --DEVICE
        when DECODE_SPI_RAW =>
          SPI_INTERFACE_STATE <= SHIFT_BUSY;
          case SPI_COMMAND is
            when CMD_WRITE_DAC =>
              SPI_SREG(15 downto 0) <= DC_CHAN_SELECT & "100" & SPI_DATA;
              TEC_DAC_SYNC          <= true;
              BIT_COUNT             <= 15;
              USE_LONG_SREG         <= false;
            when CMD_READ_ADC =>
              TEC_ADC_CS        <= true;
              TEC_ADC_CH1_B_CH2 <= DC_CHAN_SELECT;
              BIT_COUNT         <= 15;
              USE_LONG_SREG     <= false;
            when CMD_READ_TEMP =>
              TEMP_SENSOR_CS <= true;
              BIT_COUNT      <= 15;
              USE_LONG_SREG  <= false;
            when CMD_RST_SER_NUM_PTR =>
              BIT_COUNT     <= 7;
              USE_LONG_SREG <= false;
            when CMD_WREN_SER_NUM =>
              SPI_SREG(15 downto 0) <= "00000110" & X"81";
              SER_NUM_CS            <= true;
              BIT_COUNT             <= 7;
              USE_LONG_SREG         <= false;
            when CMD_WRITE_SER_NUM =>
              SPI_SREG      <= "000000100" & SER_NUM_MEM_ADDR & '0' & SER_NUM_MEM_DATA;
              SER_NUM_CS    <= true;
              BIT_COUNT     <= 31;
              USE_LONG_SREG <= true;
            when CMD_READ_SER_NUM =>
              SPI_SREG      <= "000000110" & SER_NUM_MEM_ADDR & '0' & SER_NUM_MEM_DATA;
              SER_NUM_CS    <= true;
              BIT_COUNT     <= 31;
              USE_LONG_SREG <= true;
            when others =>
              null;
          end case;
        when SHIFT_BUSY =>
          SPI_SREG <= SPI_SREG(30 downto 0) & SPI_DOUT;
          if BIT_COUNT = 0 then
            SPI_INTERFACE_STATE <= CS_CLEAR;
          else
            BIT_COUNT <= BIT_COUNT-1;
          end if;
        when CS_CLEAR =>
          TEC_DAC_SYNC        <= false;
          TEC_ADC_CS          <= false;
          TEMP_SENSOR_CS      <= false;
          SER_NUM_CS          <= false;
          SPI_INTERFACE_STATE <= UPDATE_REG;
        when UPDATE_REG =>
          SPI_INTERFACE_STATE <= READY;
        when others =>
          SPI_INTERFACE_STATE <= READY;
      end case;
    end if;
  end process SDATA_IO;

-- WHEN DONE SHIFTING DATA UPDATE THE RESULTS
-- RECORD TIME THE REGISTER WAS UPDATED AND CLEAN-UP
  RESULTS : process (CLK_4) is
  begin
    if CLK_4'event and CLK_4 = '1' then
      TEC_DAC_LDAC_INT <= false;
      if SPI_INTERFACE_STATE = UPDATE_REG then
        case SPI_COMMAND is
          when CMD_WRITE_DAC =>
            TEC_DAC_LDAC_INT <= true;
          when CMD_READ_ADC =>
            if DC_CHAN_SELECT = '0' then
              DRIVE_MON_DATA <= SPI_SREG(11 downto 0);
              DRIVE_MON_TS   <= CURRENT_TIME;
            else
              TEMP_MON_DATA <= SPI_SREG(11 downto 0);
              TEMP_MON_TS   <= CURRENT_TIME;
            end if;
          when CMD_READ_TEMP =>
            TEMP_DATA <= SPI_SREG(15 downto 0);
            TEMP_TS   <= CURRENT_TIME;
          when CMD_RST_SER_NUM_PTR =>
            SER_NUM_MEM_ADDR <= "000000";
          when CMD_WRITE_SER_NUM =>
            SER_NUM_MEM_ADDR <= SER_NUM_MEM_ADDR + 1;
          when CMD_READ_SER_NUM =>
            SERIAL_NUMBER    <= SPI_SREG(15 downto 0);
            SER_NUM_MEM_ADDR <= SER_NUM_MEM_ADDR + 1;
          when others => null;
        end case;
      end if;
    end if;
  end process RESULTS;

  --SPI STRINGS ARE DIFFERENT LENGTHS
  DELAY_SDIN : process (CLK_4) is
  begin
    if CLK_4'event and CLK_4 = '0' then
      if USE_LONG_SREG then
        SPI_DIN <= SPI_SREG(31);
      else
        SPI_DIN <= SPI_SREG(15);
      end if;
    end if;
  end process DELAY_SDIN;

  TEC_DAC_LDAC      <= TEC_DAC_LDAC_INT and not TEC_DAC_LDAC_DEL;
  NOT_CLK_4         <= not CLK_4;
  S_CMD_BUSY_SL     <= BOOL2SL(SPI_INTERFACE_STATE = SHIFT_BUSY);
  SPI_PROC_BUSY     <= SPI_INTERFACE_STATE = SHIFT_BUSY;
  TEC_ERR_SHDN      <= TEC_ERR_SHDN_INT;
  SPI_PWRUP_DONE    <= PWRUP_SEQ_DONE;
  SERIAL_NUMBER_DEC <= SERIAL_NUMBER_DEC_int;

  --CLOCK FORWARDING
  SPI_CLK_BUF : ODDR2
    generic map(
      DDR_ALIGNMENT => "NONE",
      INIT          => '0',
      SRTYPE        => "SYNC") 
    port map (
      Q  => SPI_SCLK,
      C0 => CLK_4,
      C1 => NOT_CLK_4,
      CE => '1',
      D0 => S_CMD_BUSY_SL,
      D1 => '0',
      R  => '0',
      S  => '0'
      );
end architecture BEHAV;




