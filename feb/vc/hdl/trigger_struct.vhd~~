--
-- VHDL Architecture nova_feb.dsp_filter.dummi
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 16:10:10 08/23/10
--
-- using Mentor Graphics HDL Designer(TM) 2009.1 (Build 12)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;
use nova_feb.register_address_map.all;

entity dsp_filter is
  port(
    BEEBUS_READ      : in    boolean                        := false;
    BEEBUS_STRB      : in    boolean                        := false;
    CLK_16           : in    std_logic;
    BEEBUS_DATA      : inout unsigned (15 downto 0)         := x"0000";
    BEEBUS_ADDR      : in    unsigned (15 downto 0)         := x"0000";
    RAW_CHAN_NUMBER  : in    unsigned (4 downto 0)          := "00000";
    RAW_TIMESTAMP    : in    unsigned (31 downto 0)         := x"00000000";
    RAW_MAGNITUDE    : in    unsigned (11 downto 0)         := x"000";
    HIT_DATA_OUT     : out   std_logic_vector (50 downto 0) := "000" & x"000000000000";
    DATA_STRB        : out   boolean                        := false;
    CLK_64           : in    std_logic                      := '1';
    INTERNAL_TRIGGER : in    boolean                        := false;
    ENABLE_DAQ       : in    boolean                        := false;
    TICK_1US         : in    boolean                        := false;
    DCM_SYNC         : in    boolean                        := false;
    RESET            : in    boolean                        := false
    );

-- Declarations

end dsp_filter;

--
architecture dummi of dsp_filter is

  type CHAN_STATE_TYPE is (
    IDLE,
    TRIGGERED,
    READ_RDY
    );
  type CHAN_STATE_ARRAY_TYPE is array (31 downto 0)
    of CHAN_STATE_TYPE;
  signal CHAN_STATE : CHAN_STATE_ARRAY_TYPE;

  type TABLE_TYPE is array (31 downto 0)
    of unsigned(11 downto 0);
  signal THRESHOLD_TABLE : TABLE_TYPE;

  type DATA_DELAY_TYPE is array (95 downto 0)
    of unsigned(11 downto 0);
  signal DATA_DELAY : DATA_DELAY_TYPE;

  type CHAN_MAGNITUDE_TYPE is array (31 downto 0)
    of signed(12 downto 0);
  signal CHAN_MAGNITUDE : CHAN_MAGNITUDE_TYPE;

  type CHAN_TIMESTAMP_TYPE is array (31 downto 0)
    of unsigned(31 downto 0);
  signal CHAN_TIMESTAMP : CHAN_TIMESTAMP_TYPE;

  type CHAN_TM_GRP_TYPE is array (31 downto 0)
    of unsigned(1 downto 0);
  signal CHAN_TM_GRP : CHAN_TM_GRP_TYPE;

  type TM_GRP_LUT_TYPE is array (3 downto 0)
    of unsigned(31 downto 0);
  signal TM_GRP_LUT : TM_GRP_LUT_TYPE;

  signal DAQ_MODE             : unsigned(15 downto 0)          := x"0000";
  signal OSCOPE_TRIGGERED     : boolean                        := false;
  signal BEEBUS_READ_del      : boolean                        := false;
  signal INTERNAL_TRIGGER_del : boolean                        := false;
  signal BEEBUS_DATA_int      : unsigned(15 downto 0)          := x"0000";
  signal TIMESTAMP            : unsigned(31 downto 0)          := x"00000000";
  signal CHAN_NUMBER_DSP      : unsigned (4 downto 0)          := "00000";
  signal MAGNITUDE_DSP        : signed (12 downto 0)           := "0000000000000";
  signal DATA_STRB_DSP        : boolean                        := false;
  signal TIMESTAMP_DSP        : unsigned(31 downto 0)          := x"00000000";
  signal CHAN_NUMBER          : integer range 31 downto 0      := 0;
  signal CHAN_DATA_DCS        : signed (12 downto 0)           := "0000000000000";
  signal DATA_REGULATOR_REG   : unsigned (6 downto 0)          := "0000000";
  signal DATA_REGULATOR_CNT   : unsigned (6 downto 0)          := "0000000";
  signal CHAN_EN              : std_logic_vector (31 downto 0) := x"FFFFFFFF";
  signal ALL_APD_DATA         : std_logic_vector (48 downto 0) := '0' & x"000000000000";
  signal TM_STRB              : boolean                        := false;
  signal OSCOPE_STRB          : boolean                        := false;
  signal DCS_STRB             : boolean                        := false;
  signal TM_GRP_WAIT          : unsigned (5 downto 0)          := "000000";
  signal TIME_MARKER_RATE     : unsigned (7 downto 0)          := x"00";
  signal TIME_MARKER_CNT      : unsigned (7 downto 0)          := x"00";
  signal TM_GRP_CURRENT       : unsigned (1 downto 0)          := "00";
  signal TM_GRP_PROCESS       : unsigned (1 downto 0)          := "00";
  signal TM_GRP_WAIT_RESET    : boolean                        := false;
  signal TM_GRP_LUT_WR        : boolean                        := false;
  signal TM_GRP_LUT_RD        : boolean                        := false;
  signal APD_DATA_STRB        : boolean                        := false;
  
begin
  BEEBUS_DATA <= BEEBUS_DATA_int when BEEBUS_READ_del else "ZZZZZZZZZZZZZZZZ";

  process (CLK_16)
  begin
    if CLK_16'event and CLK_16 = '1' then
      BEEBUS_READ_del <= false;
      if BEEBUS_ADDR = DAQ_MODE_ADDR then
        if BEEBUS_READ then
          BEEBUS_DATA_int <= DAQ_MODE;
          BEEBUS_READ_del <= true;
        elsif BEEBUS_STRB then
          DAQ_MODE <= BEEBUS_DATA;
        end if;
      end if;

      if BEEBUS_ADDR(15 downto 5) = THRESHOLD_ADDR then
        if BEEBUS_READ then
          BEEBUS_READ_del <= true;
          BEEBUS_DATA_int <= x"0" & THRESHOLD_TABLE (to_integer(BEEBUS_ADDR(5 downto 0)));
        elsif BEEBUS_STRB then
          THRESHOLD_TABLE (to_integer(BEEBUS_ADDR(5 downto 0))) <= (BEEBUS_DATA(11 downto 0));
        end if;
      end if;

      if BEEBUS_ADDR(15 downto 0) = DATA_REGULATOR_ADDR then
        if BEEBUS_READ then
          BEEBUS_READ_del <= true;
          BEEBUS_DATA_int <= x"000" & DATA_REGULATOR_REG(6 downto 3);
        elsif BEEBUS_STRB then
          DATA_REGULATOR_REG <= BEEBUS_DATA(3 downto 0) & "000";
        end if;
      end if;

      if BEEBUS_ADDR(15 downto 0) = CHAN_EN_U_ADDR then
        if BEEBUS_READ then
          BEEBUS_READ_del <= true;
          BEEBUS_DATA_int <= unsigned(CHAN_EN(31 downto 16));
        elsif BEEBUS_STRB then
          CHAN_EN(31 downto 16) <= std_logic_vector(BEEBUS_DATA);
        end if;
      end if;

      if BEEBUS_ADDR(15 downto 0) = CHAN_EN_L_ADDR then
        if BEEBUS_READ then
          BEEBUS_READ_del <= true;
          BEEBUS_DATA_int <= unsigned(CHAN_EN(15 downto 0));
        elsif BEEBUS_STRB then
          CHAN_EN(15 downto 0) <= std_logic_vector(BEEBUS_DATA);
        end if;
      end if;

      if BEEBUS_ADDR(15 downto 0) = TIMING_PKT_RATE_ADDR then
        if BEEBUS_READ then
          BEEBUS_READ_del <= true;
          BEEBUS_DATA_int <= x"00" & TIME_MARKER_RATE;
        elsif BEEBUS_STRB then
          TIME_MARKER_RATE <= BEEBUS_DATA(7 downto 0);
        end if;
      end if;
      
    end if;
  end process;

  SEND_TIME_MARKER : process (CLK_64) is
  begin
    if CLK_64'event and CLK_64 = '1' then
      TM_GRP_LUT_WR <= false;
      TM_GRP_LUT_RD <= false;
      if TM_GRP_LUT_WR then
        TM_GRP_LUT(TO_INTEGER(TM_GRP_CURRENT)) <= TIMESTAMP;
      end if;

      if DCM_SYNC then
        TIME_MARKER_CNT <= x"01";
      elsif TICK_1US then
        if (TIME_MARKER_CNT = TIME_MARKER_RATE)
          and (TIME_MARKER_RATE /= x"00") then
          TIME_MARKER_CNT <= x"01";
          TM_GRP_LUT_WR   <= true;
          TM_GRP_CURRENT  <= TM_GRP_CURRENT + 1;
        else
          TIME_MARKER_CNT <= TIME_MARKER_CNT + 1;
        end if;
      end if;

      --Wait for all 'hits' to be sent before processing the next time marker group
      if TM_GRP_WAIT_RESET then
        -- Wait for 32 counts if it is still there it will reset again
        TM_GRP_WAIT <= "011111";
      elsif (TM_GRP_WAIT /= "000000")then
        TM_GRP_WAIT <= TM_GRP_WAIT - 1;
      elsif TM_GRP_CURRENT /= TM_GRP_PROCESS then
        --nothing is waiting and can process the next group
        TM_GRP_PROCESS <= TM_GRP_PROCESS + 1;
        TM_GRP_WAIT    <= "100000";
        TM_GRP_LUT_RD  <= true;
      else
        TM_GRP_WAIT <= "000000";
      end if;
      
    end if;
  end process SEND_TIME_MARKER;

  process (CLK_64) is
    variable i : integer := 0;
  begin
    if CLK_64'event and CLK_64 = '1' then
      TM_GRP_WAIT_RESET <= false;
      CHAN_NUMBER       <= to_integer(RAW_CHAN_NUMBER);
      TIMESTAMP         <= RAW_TIMESTAMP;  --DELAY TIMESTAMP TO SYNC WITH OTHER DATA
      DATA_STRB_DSP     <= false;
      DATA_DELAY        <= DATA_DELAY(94 downto 0) & RAW_MAGNITUDE;
      -- FIR 32 CHANNELS USING (1 0 0 -1) "DCS FILTER"
      CHAN_DATA_DCS     <= signed('0' & RAW_MAGNITUDE) - signed('0' & DATA_DELAY(95));


      if ENABLE_DAQ then

        --Filtered data over threshold -Mark as TRIGGERED
        if CHAN_DATA_DCS > CHAN_MAGNITUDE(CHAN_NUMBER)then
          CHAN_STATE(CHAN_NUMBER)     <= TRIGGERED;
          CHAN_TIMESTAMP(CHAN_NUMBER) <= TIMESTAMP;
          CHAN_MAGNITUDE(CHAN_NUMBER) <= CHAN_DATA_DCS;
          CHAN_TM_GRP(CHAN_NUMBER)    <= TM_GRP_CURRENT;
          if TM_GRP_CURRENT = TM_GRP_PROCESS then
            TM_GRP_WAIT_RESET <= true;
          end if;

          --Chan is ready to be readout -- Send or keep as READ_RDY
        elsif ((CHAN_DATA_DCS(12) = '1')
               and (CHAN_STATE(CHAN_NUMBER) = TRIGGERED))
          or (CHAN_STATE(CHAN_NUMBER) = READ_RDY) then
          if CHAN_TM_GRP(CHAN_NUMBER) = TM_GRP_PROCESS then
            -- Send with 'Processing' time group
            CHAN_NUMBER_DSP             <= to_UNSIGNED(CHAN_NUMBER, 5);
            TIMESTAMP_DSP               <= CHAN_TIMESTAMP(CHAN_NUMBER);
            MAGNITUDE_DSP               <= CHAN_MAGNITUDE(CHAN_NUMBER);
            DATA_STRB_DSP               <= true;  --2
            CHAN_MAGNITUDE(CHAN_NUMBER) <= signed('0' & THRESHOLD_TABLE(CHAN_NUMBER));
            CHAN_STATE(CHAN_NUMBER)     <= IDLE;
          else
            --not currently serving that TM_GRP so keep as READ_RDY
            CHAN_STATE(CHAN_NUMBER) <= READ_RDY;
          end if;

          -- Chan is still in Triggered state with now Processing TM_GRP
        elsif (CHAN_STATE(CHAN_NUMBER) = TRIGGERED)
          and (CHAN_TM_GRP(CHAN_NUMBER) = TM_GRP_PROCESS) then
          TM_GRP_WAIT_RESET <= true;
        end if;

        --Clear everyone when Idle
      else
        for i in 0 to 31 loop
          CHAN_MAGNITUDE(i)       <= signed('0' & THRESHOLD_TABLE(i));
          CHAN_STATE(CHAN_NUMBER) <= IDLE;
        end loop;
      end if;
    end if;
  end process;


  data_regulator : process (CLK_16)
  begin
    if CLK_16'event and CLK_16 = '1' then
      
      if INTERNAL_TRIGGER
        and (DAQ_MODE = OSCILLOSCOPE_MODE) then
        DATA_REGULATOR_CNT <= DATA_REGULATOR_REG;
      elsif DATA_REGULATOR_CNT /= "0000000" then
        DATA_REGULATOR_CNT <= DATA_REGULATOR_CNT - 1;
      end if;
      
    end if;
  end process data_regulator;

  decode_mode : process (CLK_64) is
  begin
    if CLK_64'event and CLK_64 = '1' then
      INTERNAL_TRIGGER_del <= INTERNAL_TRIGGER;

      OSCOPE_TRIGGERED <= false;
      OSCOPE_STRB      <= false;
      DCS_STRB         <= false;
      TM_STRB          <= false;

      case DAQ_MODE is
        
        when DCS_DSP_MODE =>
          if TM_GRP_LUT_RD then
            ALL_APD_DATA <= "00000"
                            & std_logic_vector(TM_GRP_LUT(TO_INTEGER(TM_GRP_PROCESS)))
                            & x"abc";
            TM_STRB <= true;
          else
            ALL_APD_DATA <= std_logic_vector(CHAN_NUMBER_DSP)
                            & std_logic_vector(TIMESTAMP_DSP)
                            & std_logic_vector(MAGNITUDE_DSP(11 downto 0));
            DCS_STRB <= DATA_STRB_DSP and (CHAN_EN(TO_INTEGER(CHAN_NUMBER_DSP)) = '1');
          end if;
          
        when OSCILLOSCOPE_MODE =>
          TM_STRB          <= TM_GRP_LUT_WR;
          OSCOPE_TRIGGERED <= (INTERNAL_TRIGGER or OSCOPE_TRIGGERED) and DATA_REGULATOR_CNT /= "0000001" and ENABLE_DAQ;
          ALL_APD_DATA     <= std_logic_vector(TO_UNSIGNED(CHAN_NUMBER, 5))
                              & std_logic_vector(TIMESTAMP)
                              & std_logic_vector(DATA_DELAY(0));
          OSCOPE_STRB <= OSCOPE_TRIGGERED and (CHAN_EN(CHAN_NUMBER) = '1');

        when MEMORY_MODE =>
          null;
          
        when MEMORY_LOOP_MODE =>
          null;
          
        when SINGLE_DATA_POINT_MODE =>
          TM_STRB      <= TM_GRP_LUT_WR;
          ALL_APD_DATA <= std_logic_vector(TO_UNSIGNED(CHAN_NUMBER, 5))
                          & std_logic_vector(TIMESTAMP)
                          & std_logic_vector(DATA_DELAY(0));
          OSCOPE_STRB <= INTERNAL_TRIGGER and not INTERNAL_TRIGGER_del;

        when others => null;
      end case;
    end if;
  end process decode_mode;
  SEND_TO_OUTPUT_BUFFER : process (TM_STRB, APD_DATA_STRB, ALL_APD_DATA,
                                   OSCOPE_STRB, DCS_STRB, ENABLE_DAQ, TIMESTAMP) is
  begin
    HIT_DATA_OUT <= bool2sl(TM_STRB) & bool2sl(APD_DATA_STRB)
                    & ALL_APD_DATA;
    APD_DATA_STRB <= (OSCOPE_STRB or DCS_STRB)and ENABLE_DAQ;
    DATA_STRB     <= TM_STRB or APD_DATA_STRB;
    
  end process SEND_TO_OUTPUT_BUFFER;
  
end architecture dummi;






