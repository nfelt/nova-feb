--
-- VHDL Architecture feb_p2_lib.adc_interface.behav
--
-- Created:
--          BY - NATE.UNKNOWN (HEPLPC2)
--          AT - 11:43:21 01/22/2007
--
-- USING MENTOR GRAPHICS HDL DESIGNER(TM) 2006.1 (BUILD 72)
-------------------------------------------------------------------------------
-- ADC Data from 32 channels is put into 1 processing stream in the case of FEB4
-- or 4 processing streams in the case of feb5 
-- the channel number is also alligned with the corresponding data stream.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library UNISIM;
use UNISIM.all;
library HARVARD_STD;
use HARVARD_STD.LPPC_CUSTOM_FN_PKG.all;
library NOVA_FEB;
use NOVA_FEB.REGISTER_ADDRESS_MAP.all;

entity timing is
  port(
    BEEBUS_ADDR : in    unsigned (15 downto 0);
    BEEBUS_DATA : inout unsigned (15 downto 0);
    BEEBUS_READ : in    boolean;
    BEEBUS_STRB : in    boolean;


    MAGNITUDE_CH_ASYNC : in MAGNITUDE_VECTOR_TYPE;

    MAGNITUDE    : out MAGNITUDE_VECTOR_TYPE;
    --    CHAN_NUMBER  : out CHAN_NUM_VECTOR_TYPE(NSEG-1 downto 0) := ("11100", "10011", "01010", "00001");
    CHAN_NUMBER  : out CHAN_NUM_VECTOR_TYPE(NSEG-1 downto 0) := CHAN_NUMBER_INIT;
    CURRENT_TIME : out TIMESTAMP_TYPE;

    RESET_CHAN_PTR : in boolean;
    ENABLE_DAQ     : in boolean;

    DCM_SYNC      : in  boolean;
    TICK_1US      : out boolean;
    CLK_16        : in  std_logic;
    CLK_32        : in  std_logic;
    CLK_64        : in  std_logic;
    USE_CLK16_INV : out boolean
    );

-- DECLARATIONS

end timing;

--
architecture BEHAV of timing is

  signal FAKE_DATA            : unsigned (11 downto 0) := X"000";
  signal ADC_CHAN_PTR         : unsigned (1 downto 0)  := "00";
  signal ASIC_CHAN_PTR        : unsigned (3 downto 0)  := "0000";
  signal BEEBUS_DATA_INT      : unsigned (15 downto 0) := X"0000";
  signal PRESET_TIME          : unsigned (29 downto 0) := "00" & X"0000000";
  signal CURRENT_TIME_INT     : unsigned (30 downto 0) := "000" & X"0000000";
  signal BEEBUS_READ_DEL      : boolean                := false;
  signal CURRENT_TIME_INT_DEL : std_logic              := '0';
  signal USE_CLK16_INV_INT    : boolean                := false;
  signal DCM_SYNC_del         : boolean                := false;
  signal DCM_SYNC_del2        : boolean                := false;
  signal SYNC                 : boolean                := false;
  signal ADC_CHAN_DATA0       : ADC_CHAN_DATA_TYPE;
  signal ADC_CHAN_DATA1       : ADC_CHAN_DATA_TYPE;
  
begin

  BEEBUS_DATA <= BEEBUS_DATA_INT when BEEBUS_READ_DEL else "ZZZZZZZZZZZZZZZZ";

  SET_ADC_IN_REGS : process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '1' then
      BEEBUS_READ_DEL <= false;

      case BEEBUS_ADDR is
        when PRESET_TIME_L_ADDR =>
          if BEEBUS_STRB then
            PRESET_TIME(13 downto 0) <= BEEBUS_DATA(15 downto 2);
          end if;
          if BEEBUS_READ then
            BEEBUS_READ_DEL <= true;
            BEEBUS_DATA_INT <= PRESET_TIME(13 downto 0) & "00";
          end if;
        when PRESET_TIME_U_ADDR =>
          if BEEBUS_STRB then
            PRESET_TIME(29 downto 14) <= BEEBUS_DATA;
          end if;
          if BEEBUS_READ then
            BEEBUS_READ_DEL <= true;
            BEEBUS_DATA_INT <= PRESET_TIME(29 downto 14);
          end if;
--temp reg for nd FEB testing

        when others => null;
      end case;
    end if;
  end process SET_ADC_IN_REGS;

  CURRENT_TIME_CNT : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '1' then
      --use delayed sync to mask 1us tick after time reset
      DCM_SYNC_del  <= DCM_SYNC;
      DCM_SYNC_del2 <= DCM_SYNC_del;
      --use delayed sync to allign time
      if DCM_SYNC_del then
        --preset time are in units of clk_16
        --using clk_32 will accomidate phase of clk_16
        CURRENT_TIME_INT <= PRESET_TIME & '0';
      else
        CURRENT_TIME_INT <= CURRENT_TIME_INT + 1;
      end if;
    end if;
  end process;

  GEN_1US_TICK : process (CLK_64) is
  begin
    if CLK_64'event and CLK_64 = '1' then
      CURRENT_TIME_INT_DEL <= CURRENT_TIME_INT(5);
    end if;
  end process GEN_1US_TICK;
  --need to mask the tick_1us with delayed sync to prevent tick on preset
  TICK_1US <= (CURRENT_TIME_INT_DEL /= CURRENT_TIME_INT(5))
              and not DCM_SYNC_del
              and not DCM_SYNC_del2;

  --DETECT PHASE OF CLK 16 ADJUST AND TRACK ASIC MUX
  ASIC_CHCNT : process (CLK_32) is
  begin
    if RESET_CHAN_PTR then
      --accomidate for the phase of clk_16
      if USE_CLK16_INV_INT then        
        ASIC_CHAN_PTR <= ASIC_CH_START & '0';
      else
        ASIC_CHAN_PTR <= ASIC_CH_START & '1';
      end if;
    elsif CLK_32'event and CLK_32 = '1' then
      --invert asic and adc clk to put sync in 2nd half
      if DCM_SYNC and (ASIC_CHAN_PTR(0) = '1') then
        USE_CLK16_INV_INT <= not USE_CLK16_INV_INT;
      else
        ASIC_CHAN_PTR <= ASIC_CHAN_PTR + 1;
      end if;
    end if;
  end process ASIC_CHCNT;

  USE_CLK16_INV <= USE_CLK16_INV_INT;

  ADC_PIPELINE : process (CLK_64) is
    --EVERYTHING IS PIPELINED 1 CLK_64 HERE
  begin
    if CLK_64'event and CLK_64 = '1' then
      if RESET_CHAN_PTR then
        ADC_CHAN_PTR <= "00";
        FAKE_DATA    <= X"000";
      else
        FAKE_DATA    <= FAKE_DATA+1;
        ADC_CHAN_PTR <= ADC_CHAN_PTR+1;
      end if;
      --CHAN DATA TIME  MAY NOT NEED THIS PIPELINE STAGE
      if GEN_FEB5 then
        --FEB5 HAS 4 DATA SEGMENTS EACH WITH 8 CHANNELS
        for ADC_GROUP_INDEX in 3 downto 0 loop
          MAGNITUDE(ADC_GROUP_INDEX)    <= MAGNITUDE_CH_ASYNC(ADC_GROUP_INDEX*4 + TO_INTEGER(ADC_CHAN_PTR));
          CHAN_NUMBER (ADC_GROUP_INDEX) <= TO_UNSIGNED(ADC_GROUP_INDEX, 2)
                                           & ADC_CHAN_PTR
                                           & not ASIC_CHAN_PTR(1);
        end loop;         
      else
        --FEB4 HAS 1 SEGMENT WITH 32 CHANNELS
        MAGNITUDE(0)   <= MAGNITUDE_CH_ASYNC(TO_INTEGER(ADC_CHAN_PTR(0 downto 0)));
        CHAN_NUMBER(0) <= ADC_CHAN_PTR(0)  -- 5TH BIT
                          & (ADC_CHAN_PTR(1) xor BOOL2SL(USE_CLK16_INV_INT))
                          --& ADC_CHAN_PTR(1)
                          & ASIC_CHAN_PTR(3 downto 1);
      end if;

      CURRENT_TIME <= CURRENT_TIME_INT(30 downto 1) & "00";
    end if;
  end process;

end architecture BEHAV;
