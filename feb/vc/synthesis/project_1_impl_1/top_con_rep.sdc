###################################################################################
# Mentor Graphics Corporation
#
###################################################################################


################
# create clock #
################

# Precision Generated
create_clock [get_ports {DCM_CLK_P}] -name {DCM_CLK_P} -period 3.33333 -waveform {0 1.66667} -constraint_source derived
create_clock [get_pins {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/clkin1_buf/O}] -name {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/clkin1_buf/O} -period 31.25 -waveform {0 15.625} -constraint_source derived
create_clock [get_pins {DATA_PROCESSING_INST/ADC_SEG_PROC_GEN_0_multi_point_ctrl_inst/ix117/out}] -name {DATA_PROCESSING_INST/ADC_SEG_PROC_GEN_0_multi_point_ctrl_inst/ix117/out} -period 3.33333 -waveform {0 1.66667} -constraint_source derived

##########################
# create generated clock #
##########################

# Precision Generated
create_generated_clock -name {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKFBOUT} -source [get_pins {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/clkin1_buf/O}] -multiply_by 16 -duty_cycle 50 [get_pins {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKFBOUT}] -constraint_source derived
create_generated_clock -name {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKOUT0} -source [get_pins {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/clkin1_buf/O}] -divide_by 1 -duty_cycle 50 [get_pins {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKOUT0}] -constraint_source derived
create_generated_clock -name {DCM_COMM_INTERFACE_INST/U_11/reg_CLK_3_2_COMB/out} -source [get_pins {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKOUT0}] -divide_by 2 -duty_cycle 50 [get_pins {DCM_COMM_INTERFACE_INST/U_11/reg_CLK_3_2_COMB/out}] -constraint_source derived
create_generated_clock -name {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKOUT1} -source [get_pins {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/clkin1_buf/O}] -multiply_by 2 -duty_cycle 50 [get_pins {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKOUT1}] -constraint_source derived
create_generated_clock -name {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKOUT2} -source [get_pins {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/clkin1_buf/O}] -divide_by 2 -duty_cycle 50 [get_pins {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKOUT2}] -constraint_source derived
create_generated_clock -name {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKOUT3} -source [get_pins {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/clkin1_buf/O}] -divide_by 2 -duty_cycle 50 [get_pins {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKOUT3}] -phase 180 -constraint_source derived
create_generated_clock -name {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKOUT4} -source [get_pins {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/clkin1_buf/O}] -divide_by 8 -duty_cycle 50 [get_pins {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKOUT4}] -constraint_source derived
create_generated_clock -name {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKOUT5} -source [get_pins {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/clkin1_buf/O}] -multiply_by 4 -duty_cycle 50 [get_pins {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKOUT5}] -constraint_source derived
create_generated_clock -name {DCM_COMM_INTERFACE_INST/U_1/reg_TX_PKT_STATE(2)/out} -source [get_pins {DCM_COMM_INTERFACE_INST/U_11/reg_CLK_3_2_COMB/out}] -divide_by 2 -duty_cycle 25 [get_pins {DCM_COMM_INTERFACE_INST/U_1/reg_TX_PKT_STATE(2)/out}] -constraint_source derived
