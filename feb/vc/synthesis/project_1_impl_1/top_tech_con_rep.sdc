###################################################################################
# Mentor Graphics Corporation
#
###################################################################################


################
# create clock #
################

# Precision Generated
create_clock [get_ports {DCM_CLK_P}] -name {DCM_CLK_P} -domain {DCM_CLK_P_PS} -period 3.33333 -waveform {0 1.66667} -constraint_source derived
create_clock [get_pins {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_clkin1_buf/O}] -name {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_clkin1_buf/O} -domain {DCM_CLK_P_PS} -period 31.25 -waveform {0 15.625} -constraint_source derived
create_clock [get_pins {DATA_PROCESSING_INST/ADC_SEG_PROC_GEN_0_multi_point_ctrl_inst/ix31249z1313/O}] -name {DATA_PROCESSING_INST/ADC_SEG_PROC_GEN_0_multi_point_ctrl_inst/ix31249z1313/O} -domain {DCM_CLK_P_PS} -period 3.33333 -waveform {0 1.66667} -constraint_source derived

##########################
# create generated clock #
##########################

# Precision Generated
#Info: Skipping internally generated clock constraint as it is not driving any logic
#	create_generated_clock -name {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKFBOUT} -domain {DCM_CLK_P_PS} -source [get_pins {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_clkin1_buf/O}] -multiply_by 16 -duty_cycle 50 [get_pins {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst/CLKFBOUT}] -constraint_source derived
create_generated_clock -name {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKOUT0} -domain {DCM_CLK_P_PS} -source [get_pins {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_clkin1_buf/O}] -divide_by 1 -duty_cycle 50 [get_pins {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst/CLKOUT0}] -constraint_source derived
create_generated_clock -name {DCM_COMM_INTERFACE_INST/U_11/reg_CLK_3_2_COMB/out} -domain {DCM_CLK_P_PS} -source [get_pins {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst/CLKOUT0}] -divide_by 2 -duty_cycle 50 [get_pins {DCM_COMM_INTERFACE_INST_U_11/reg_CLK_3_2_COMB/Q}] -constraint_source derived
create_generated_clock -name {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKOUT2} -domain {DCM_CLK_P_PS} -source [get_pins {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_clkin1_buf/O}] -divide_by 2 -duty_cycle 50 [get_pins {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst/CLKOUT2}] -constraint_source derived
create_generated_clock -name {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKOUT3} -domain {DCM_CLK_P_PS} -source [get_pins {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_clkin1_buf/O}] -divide_by 2 -duty_cycle 50 [get_pins {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst/CLKOUT3}] -phase 180 -constraint_source derived
create_generated_clock -name {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKOUT1} -domain {DCM_CLK_P_PS} -source [get_pins {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_clkin1_buf/O}] -multiply_by 2 -duty_cycle 50 [get_pins {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst/CLKOUT1}] -constraint_source derived
create_generated_clock -name {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKOUT4} -domain {DCM_CLK_P_PS} -source [get_pins {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_clkin1_buf/O}] -divide_by 8 -duty_cycle 50 [get_pins {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst/CLKOUT4}] -constraint_source derived
create_generated_clock -name {DCM_COMM_EMU_CLK_INST/CLK_PLL_COREGEN/pll_base_inst/CLKOUT5} -domain {DCM_CLK_P_PS} -source [get_pins {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_clkin1_buf/O}] -multiply_by 4 -duty_cycle 50 [get_pins {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst/CLKOUT5}] -constraint_source derived
create_generated_clock -name {TIMING_INST/reg_USE_CLK16_INV_INT/Q} -domain {DCM_CLK_P_PS} -source [get_pins {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst/CLKOUT0}] -divide_by 2 -duty_cycle 50 [get_pins {TIMING_INST/reg_USE_CLK16_INV_INT/Q}] -constraint_source derived
create_generated_clock -name {DCM_COMM_INTERFACE_INST/U_1/reg_TX_PKT_STATE(2)/out} -domain {DCM_CLK_P_PS} -source [get_pins {DCM_COMM_INTERFACE_INST_U_11/reg_CLK_3_2_COMB/Q}] -divide_by 2 -duty_cycle 25 [get_pins {DCM_COMM_INTERFACE_INST_U_1/reg_TX_PKT_STATE(2)/Q}] -constraint_source derived

#################
# set attribute #
#################
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAN(11)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAN(10)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAN(9)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAN(8)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAN(7)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAN(6)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAN(5)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAN(4)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAN(3)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAN(2)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAN(1)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAN(0)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAP(11)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAP(10)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAP(9)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAP(8)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAP(7)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAP(6)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAP(5)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAP(4)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAP(3)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAP(2)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAP(1)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTAP(0)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBN(11)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBN(10)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBN(9)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBN(8)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBN(7)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBN(6)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBN(5)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBN(4)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBN(3)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBN(2)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBN(1)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBN(0)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBP(11)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBP(10)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBP(9)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBP(8)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBP(7)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBP(6)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBP(5)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBP(4)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBP(3)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBP(2)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBP(1)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_DOUTBP(0)}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_CLKINN}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ADC_CLKINP}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ASIC_OUTCLK_N}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {ASIC_OUTCLK_P}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {DCM_COMMAND_N}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {DCM_COMMAND_P}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {DCM_DATA_P}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {DCM_DATA_N}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {DCM_SYNC_P}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {DCM_SYNC_N}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {DCM_CLK_N}] -constraint_source precisiongatelevel
set_attribute -name NOPAD -value {TRUE} [get_ports {DCM_CLK_P}] -constraint_source precisiongatelevel
set_attribute -name iostandard -value {DEFAULT} [get_cells {LVDS_IBUF_SYNC_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {LVDS_IBUF_SYNC_u}] -constraint_source precisiongatelevel
set_attribute -name iostandard -value {DEFAULT} [get_cells {LVDS_IBUF_CMD_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {LVDS_IBUF_CMD_u}] -constraint_source precisiongatelevel
set_attribute -name iostandard -value {DEFAULT} [get_cells {LVDS_OBUF_ASIC_CLK_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {LVDS_OBUF_ASIC_CLK_u}] -constraint_source precisiongatelevel
set_attribute -name iostandard -value {DEFAULT} [get_cells {LVDS_OBUF_DATA_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {LVDS_OBUF_DATA_u}] -constraint_source precisiongatelevel
set_attribute -name iostandard -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_CLKIN_BUF_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_CLKIN_BUF_u}] -constraint_source precisiongatelevel
set_attribute -name DDR_ALIGNMENT -value {NONE} [get_cells {DCM_COMM_EMU_CLK_INST_ASIC_CLK_ODDR2}] -constraint_source precisiongatelevel
set_attribute -name SRTYPE -value {SYNC} [get_cells {DCM_COMM_EMU_CLK_INST_ASIC_CLK_ODDR2}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {DCM_COMM_EMU_CLK_INST_ASIC_CLK_ODDR2}] -constraint_source precisiongatelevel
set_attribute -name DDR_ALIGNMENT -value {NONE} [get_cells {DCM_COMM_EMU_CLK_INST_ADC_CLK0_ODDR2}] -constraint_source precisiongatelevel
set_attribute -name SRTYPE -value {SYNC} [get_cells {DCM_COMM_EMU_CLK_INST_ADC_CLK0_ODDR2}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {DCM_COMM_EMU_CLK_INST_ADC_CLK0_ODDR2}] -constraint_source precisiongatelevel
set_attribute -name DDR_ALIGNMENT -value {NONE} [get_cells {DCM_COMM_EMU_CLK_INST_ADC_CLK1_ODDR2}] -constraint_source precisiongatelevel
set_attribute -name SRTYPE -value {SYNC} [get_cells {DCM_COMM_EMU_CLK_INST_ADC_CLK1_ODDR2}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {DCM_COMM_EMU_CLK_INST_ADC_CLK1_ODDR2}] -constraint_source precisiongatelevel
set_attribute -name DDR_ALIGNMENT -value {NONE} [get_cells {DCM_COMM_EMU_CLK_INST_IFCLK_ODDR2}] -constraint_source precisiongatelevel
set_attribute -name SRTYPE -value {SYNC} [get_cells {DCM_COMM_EMU_CLK_INST_IFCLK_ODDR2}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {DCM_COMM_EMU_CLK_INST_IFCLK_ODDR2}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_32_BUFG}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_64_BUFG}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_16_180_BUFG}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_16_BUFG}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_4_BUFG}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_128_BUFG}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_FB_BUFG}] -constraint_source precisiongatelevel
set_attribute -name BOX_TYPE -value {PRIMITIVE} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_clkin1_buf}] -constraint_source precisiongatelevel
set_attribute -name CAPACITANCE -value {DONT_CARE} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_clkin1_buf}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_clkin1_buf}] -constraint_source precisiongatelevel
set_attribute -name IBUF_DELAY_VALUE -value {0} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_clkin1_buf}] -constraint_source precisiongatelevel
set_attribute -name IBUF_LOW_PWR -value {TRUE} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_clkin1_buf}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_clkin1_buf}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_clkin1_buf}] -constraint_source precisiongatelevel
set_attribute -name BOX_TYPE -value {PRIMITIVE} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name BANDWIDTH -value {OPTIMIZED} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKFBOUT_MULT -value {16} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKFBOUT_PHASE -value {0.000000} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKIN_PERIOD -value {31.250000} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKOUT0_DIVIDE -value {16} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKOUT0_DUTY_CYCLE -value {0.500000} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKOUT0_PHASE -value {0.000000} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKOUT1_DIVIDE -value {8} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKOUT1_DUTY_CYCLE -value {0.500000} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKOUT1_PHASE -value {0.000000} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKOUT2_DIVIDE -value {32} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKOUT2_DUTY_CYCLE -value {0.500000} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKOUT2_PHASE -value {0.000000} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKOUT3_DIVIDE -value {32} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKOUT3_DUTY_CYCLE -value {0.500000} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKOUT3_PHASE -value {180.000000} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKOUT4_DIVIDE -value {128} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKOUT4_DUTY_CYCLE -value {0.500000} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKOUT4_PHASE -value {0.000000} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKOUT5_DIVIDE -value {4} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKOUT5_DUTY_CYCLE -value {0.500000} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLKOUT5_PHASE -value {0.000000} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name CLK_FEEDBACK -value {CLKFBOUT} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name COMPENSATION -value {SYSTEM_SYNCHRONOUS} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name DIVCLK_DIVIDE -value {1} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name REF_JITTER -value {0.010000} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name RESET_ON_LOSS_OF_LOCK -value {FALSE} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {DCM_COMM_EMU_CLK_INST_CLK_PLL_COREGEN_pll_base_inst}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_11_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_11_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_11_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_10_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_10_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_10_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_9_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_9_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_9_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_8_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_8_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_8_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_7_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_7_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_7_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_6_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_6_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_6_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_5_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_5_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_5_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_4_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_4_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_4_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_3_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_3_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_3_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_2_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_2_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_2_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_1_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_1_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_1_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_0_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_0_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I6_lvds_ibuf_vec_0_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_11_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_11_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_11_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_10_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_10_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_10_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_9_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_9_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_9_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_8_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_8_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_8_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_7_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_7_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_7_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_6_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_6_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_6_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_5_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_5_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_5_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_4_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_4_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_4_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_3_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_3_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_3_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_2_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_2_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_2_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_1_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_1_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_1_u}] -constraint_source precisiongatelevel
set_attribute -name DIFF_TERM -value {FALSE} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_0_u}] -constraint_source precisiongatelevel
set_attribute -name IOSTANDARD -value {DEFAULT} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_0_u}] -constraint_source precisiongatelevel
set_attribute -name DONT_TOUCH -value {T} [get_cells {GENERATE_FEB4_DIN_ADC_PAR_INTERFACE_INST_I9_lvds_ibuf_vec_0_u}] -constraint_source precisiongatelevel
set_attribute -name PART -value {6SLX45FGG484-3} /work/top/STRUCT -constraint_source precisiongatelevel
set_attribute -name INIT_A -value {ffffffffffffffff} [get_cells {DATA_PROCESSING_INST/TDATA_MEM_LRPTR/TDATA_MEM_LRPTR_1}] -constraint_source precisiongatelevel
set_attribute -name INIT_B -value {ffffffffffffffff} [get_cells {DATA_PROCESSING_INST/TDATA_MEM_LRPTR/TDATA_MEM_LRPTR_1}] -constraint_source precisiongatelevel
set_attribute -name INIT_C -value {5555555555555555} [get_cells {DATA_PROCESSING_INST/TDATA_MEM_LRPTR/TDATA_MEM_LRPTR_1}] -constraint_source precisiongatelevel
set_attribute -name INIT_A -value {0000000000000000} [get_cells {DATA_PROCESSING_INST/TDATA_CNT/TDATA_CNT_1}] -constraint_source precisiongatelevel
set_attribute -name INIT_B -value {0000000000000000} [get_cells {DATA_PROCESSING_INST/TDATA_CNT/TDATA_CNT_1}] -constraint_source precisiongatelevel
set_attribute -name INIT_C -value {0000000000000000} [get_cells {DATA_PROCESSING_INST/TDATA_CNT/TDATA_CNT_1}] -constraint_source precisiongatelevel
set_attribute -name INIT_D -value {0000000000000000} [get_cells {DATA_PROCESSING_INST/TDATA_CNT/TDATA_CNT_1}] -constraint_source precisiongatelevel
