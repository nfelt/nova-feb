###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name PART -value "6SLX16CSG324-3" -type string /work/output_buffer/behav -design gatelevel 
set_attribute -name USER_ALIAS -value "N63" -net -type string OUTPUT_BUFFER/rd_en -design gatelevel 
set_attribute -name USER_ALIAS -value "N62" -net -type string OUTPUT_BUFFER/wr_en -design gatelevel 
set_attribute -name USER_ALIAS -value "N178" -net -type string OUTPUT_BUFFER/full -design gatelevel 
set_attribute -name USER_ALIAS -value "N182" -net -type string OUTPUT_BUFFER/empty -design gatelevel 
set_attribute -name USER_ALIAS -value "N7" -net -type string OUTPUT_BUFFER/wr_clk -design gatelevel 
set_attribute -name USER_ALIAS -value "N5" -net -type string OUTPUT_BUFFER/rst -design gatelevel 
set_attribute -name USER_ALIAS -value "N9" -net -type string OUTPUT_BUFFER/rd_clk -design gatelevel 
set_attribute -name USER_ALIAS -value "N11" -net -type string OUTPUT_BUFFER/din<50> -design gatelevel 
set_attribute -name USER_ALIAS -value "N12" -net -type string OUTPUT_BUFFER/din<49> -design gatelevel 
set_attribute -name USER_ALIAS -value "N13" -net -type string OUTPUT_BUFFER/din<48> -design gatelevel 
set_attribute -name USER_ALIAS -value "N14" -net -type string OUTPUT_BUFFER/din<47> -design gatelevel 
set_attribute -name USER_ALIAS -value "N15" -net -type string OUTPUT_BUFFER/din<46> -design gatelevel 
set_attribute -name USER_ALIAS -value "N16" -net -type string OUTPUT_BUFFER/din<45> -design gatelevel 
set_attribute -name USER_ALIAS -value "N17" -net -type string OUTPUT_BUFFER/din<44> -design gatelevel 
set_attribute -name USER_ALIAS -value "N18" -net -type string OUTPUT_BUFFER/din<43> -design gatelevel 
set_attribute -name USER_ALIAS -value "N19" -net -type string OUTPUT_BUFFER/din<42> -design gatelevel 
set_attribute -name USER_ALIAS -value "N20" -net -type string OUTPUT_BUFFER/din<41> -design gatelevel 
set_attribute -name USER_ALIAS -value "N21" -net -type string OUTPUT_BUFFER/din<40> -design gatelevel 
set_attribute -name USER_ALIAS -value "N22" -net -type string OUTPUT_BUFFER/din<39> -design gatelevel 
set_attribute -name USER_ALIAS -value "N23" -net -type string OUTPUT_BUFFER/din<38> -design gatelevel 
set_attribute -name USER_ALIAS -value "N24" -net -type string OUTPUT_BUFFER/din<37> -design gatelevel 
set_attribute -name USER_ALIAS -value "N25" -net -type string OUTPUT_BUFFER/din<36> -design gatelevel 
set_attribute -name USER_ALIAS -value "N26" -net -type string OUTPUT_BUFFER/din<35> -design gatelevel 
set_attribute -name USER_ALIAS -value "N27" -net -type string OUTPUT_BUFFER/din<34> -design gatelevel 
set_attribute -name USER_ALIAS -value "N28" -net -type string OUTPUT_BUFFER/din<33> -design gatelevel 
set_attribute -name USER_ALIAS -value "N29" -net -type string OUTPUT_BUFFER/din<32> -design gatelevel 
set_attribute -name USER_ALIAS -value "N30" -net -type string OUTPUT_BUFFER/din<31> -design gatelevel 
set_attribute -name USER_ALIAS -value "N31" -net -type string OUTPUT_BUFFER/din<30> -design gatelevel 
set_attribute -name USER_ALIAS -value "N32" -net -type string OUTPUT_BUFFER/din<29> -design gatelevel 
set_attribute -name USER_ALIAS -value "N33" -net -type string OUTPUT_BUFFER/din<28> -design gatelevel 
set_attribute -name USER_ALIAS -value "N34" -net -type string OUTPUT_BUFFER/din<27> -design gatelevel 
set_attribute -name USER_ALIAS -value "N35" -net -type string OUTPUT_BUFFER/din<26> -design gatelevel 
set_attribute -name USER_ALIAS -value "N36" -net -type string OUTPUT_BUFFER/din<25> -design gatelevel 
set_attribute -name USER_ALIAS -value "N37" -net -type string OUTPUT_BUFFER/din<24> -design gatelevel 
set_attribute -name USER_ALIAS -value "N38" -net -type string OUTPUT_BUFFER/din<23> -design gatelevel 
set_attribute -name USER_ALIAS -value "N39" -net -type string OUTPUT_BUFFER/din<22> -design gatelevel 
set_attribute -name USER_ALIAS -value "N40" -net -type string OUTPUT_BUFFER/din<21> -design gatelevel 
set_attribute -name USER_ALIAS -value "N41" -net -type string OUTPUT_BUFFER/din<20> -design gatelevel 
set_attribute -name USER_ALIAS -value "N42" -net -type string OUTPUT_BUFFER/din<19> -design gatelevel 
set_attribute -name USER_ALIAS -value "N43" -net -type string OUTPUT_BUFFER/din<18> -design gatelevel 
set_attribute -name USER_ALIAS -value "N44" -net -type string OUTPUT_BUFFER/din<17> -design gatelevel 
set_attribute -name USER_ALIAS -value "N45" -net -type string OUTPUT_BUFFER/din<16> -design gatelevel 
set_attribute -name USER_ALIAS -value "N46" -net -type string OUTPUT_BUFFER/din<15> -design gatelevel 
set_attribute -name USER_ALIAS -value "N47" -net -type string OUTPUT_BUFFER/din<14> -design gatelevel 
set_attribute -name USER_ALIAS -value "N48" -net -type string OUTPUT_BUFFER/din<13> -design gatelevel 
set_attribute -name USER_ALIAS -value "N49" -net -type string OUTPUT_BUFFER/din<12> -design gatelevel 
set_attribute -name USER_ALIAS -value "N50" -net -type string OUTPUT_BUFFER/din<11> -design gatelevel 
set_attribute -name USER_ALIAS -value "N51" -net -type string OUTPUT_BUFFER/din<10> -design gatelevel 
set_attribute -name USER_ALIAS -value "N52" -net -type string OUTPUT_BUFFER/din<9> -design gatelevel 
set_attribute -name USER_ALIAS -value "N53" -net -type string OUTPUT_BUFFER/din<8> -design gatelevel 
set_attribute -name USER_ALIAS -value "N54" -net -type string OUTPUT_BUFFER/din<7> -design gatelevel 
set_attribute -name USER_ALIAS -value "N55" -net -type string OUTPUT_BUFFER/din<6> -design gatelevel 
set_attribute -name USER_ALIAS -value "N56" -net -type string OUTPUT_BUFFER/din<5> -design gatelevel 
set_attribute -name USER_ALIAS -value "N57" -net -type string OUTPUT_BUFFER/din<4> -design gatelevel 
set_attribute -name USER_ALIAS -value "N58" -net -type string OUTPUT_BUFFER/din<3> -design gatelevel 
set_attribute -name USER_ALIAS -value "N59" -net -type string OUTPUT_BUFFER/din<2> -design gatelevel 
set_attribute -name USER_ALIAS -value "N60" -net -type string OUTPUT_BUFFER/din<1> -design gatelevel 
set_attribute -name USER_ALIAS -value "N61" -net -type string OUTPUT_BUFFER/din<0> -design gatelevel 
set_attribute -name USER_ALIAS -value "N127" -net -type string OUTPUT_BUFFER/dout<50> -design gatelevel 
set_attribute -name USER_ALIAS -value "N128" -net -type string OUTPUT_BUFFER/dout<49> -design gatelevel 
set_attribute -name USER_ALIAS -value "N129" -net -type string OUTPUT_BUFFER/dout<48> -design gatelevel 
set_attribute -name USER_ALIAS -value "N130" -net -type string OUTPUT_BUFFER/dout<47> -design gatelevel 
set_attribute -name USER_ALIAS -value "N131" -net -type string OUTPUT_BUFFER/dout<46> -design gatelevel 
set_attribute -name USER_ALIAS -value "N132" -net -type string OUTPUT_BUFFER/dout<45> -design gatelevel 
set_attribute -name USER_ALIAS -value "N133" -net -type string OUTPUT_BUFFER/dout<44> -design gatelevel 
set_attribute -name USER_ALIAS -value "N134" -net -type string OUTPUT_BUFFER/dout<43> -design gatelevel 
set_attribute -name USER_ALIAS -value "N135" -net -type string OUTPUT_BUFFER/dout<42> -design gatelevel 
set_attribute -name USER_ALIAS -value "N136" -net -type string OUTPUT_BUFFER/dout<41> -design gatelevel 
set_attribute -name USER_ALIAS -value "N137" -net -type string OUTPUT_BUFFER/dout<40> -design gatelevel 
set_attribute -name USER_ALIAS -value "N138" -net -type string OUTPUT_BUFFER/dout<39> -design gatelevel 
set_attribute -name USER_ALIAS -value "N139" -net -type string OUTPUT_BUFFER/dout<38> -design gatelevel 
set_attribute -name USER_ALIAS -value "N140" -net -type string OUTPUT_BUFFER/dout<37> -design gatelevel 
set_attribute -name USER_ALIAS -value "N141" -net -type string OUTPUT_BUFFER/dout<36> -design gatelevel 
set_attribute -name USER_ALIAS -value "N142" -net -type string OUTPUT_BUFFER/dout<35> -design gatelevel 
set_attribute -name USER_ALIAS -value "N143" -net -type string OUTPUT_BUFFER/dout<34> -design gatelevel 
set_attribute -name USER_ALIAS -value "N144" -net -type string OUTPUT_BUFFER/dout<33> -design gatelevel 
set_attribute -name USER_ALIAS -value "N145" -net -type string OUTPUT_BUFFER/dout<32> -design gatelevel 
set_attribute -name USER_ALIAS -value "N146" -net -type string OUTPUT_BUFFER/dout<31> -design gatelevel 
set_attribute -name USER_ALIAS -value "N147" -net -type string OUTPUT_BUFFER/dout<30> -design gatelevel 
set_attribute -name USER_ALIAS -value "N148" -net -type string OUTPUT_BUFFER/dout<29> -design gatelevel 
set_attribute -name USER_ALIAS -value "N149" -net -type string OUTPUT_BUFFER/dout<28> -design gatelevel 
set_attribute -name USER_ALIAS -value "N150" -net -type string OUTPUT_BUFFER/dout<27> -design gatelevel 
set_attribute -name USER_ALIAS -value "N151" -net -type string OUTPUT_BUFFER/dout<26> -design gatelevel 
set_attribute -name USER_ALIAS -value "N152" -net -type string OUTPUT_BUFFER/dout<25> -design gatelevel 
set_attribute -name USER_ALIAS -value "N153" -net -type string OUTPUT_BUFFER/dout<24> -design gatelevel 
set_attribute -name USER_ALIAS -value "N154" -net -type string OUTPUT_BUFFER/dout<23> -design gatelevel 
set_attribute -name USER_ALIAS -value "N155" -net -type string OUTPUT_BUFFER/dout<22> -design gatelevel 
set_attribute -name USER_ALIAS -value "N156" -net -type string OUTPUT_BUFFER/dout<21> -design gatelevel 
set_attribute -name USER_ALIAS -value "N157" -net -type string OUTPUT_BUFFER/dout<20> -design gatelevel 
set_attribute -name USER_ALIAS -value "N158" -net -type string OUTPUT_BUFFER/dout<19> -design gatelevel 
set_attribute -name USER_ALIAS -value "N159" -net -type string OUTPUT_BUFFER/dout<18> -design gatelevel 
set_attribute -name USER_ALIAS -value "N160" -net -type string OUTPUT_BUFFER/dout<17> -design gatelevel 
set_attribute -name USER_ALIAS -value "N161" -net -type string OUTPUT_BUFFER/dout<16> -design gatelevel 
set_attribute -name USER_ALIAS -value "N162" -net -type string OUTPUT_BUFFER/dout<15> -design gatelevel 
set_attribute -name USER_ALIAS -value "N163" -net -type string OUTPUT_BUFFER/dout<14> -design gatelevel 
set_attribute -name USER_ALIAS -value "N164" -net -type string OUTPUT_BUFFER/dout<13> -design gatelevel 
set_attribute -name USER_ALIAS -value "N165" -net -type string OUTPUT_BUFFER/dout<12> -design gatelevel 
set_attribute -name USER_ALIAS -value "N166" -net -type string OUTPUT_BUFFER/dout<11> -design gatelevel 
set_attribute -name USER_ALIAS -value "N167" -net -type string OUTPUT_BUFFER/dout<10> -design gatelevel 
set_attribute -name USER_ALIAS -value "N168" -net -type string OUTPUT_BUFFER/dout<9> -design gatelevel 
set_attribute -name USER_ALIAS -value "N169" -net -type string OUTPUT_BUFFER/dout<8> -design gatelevel 
set_attribute -name USER_ALIAS -value "N170" -net -type string OUTPUT_BUFFER/dout<7> -design gatelevel 
set_attribute -name USER_ALIAS -value "N171" -net -type string OUTPUT_BUFFER/dout<6> -design gatelevel 
set_attribute -name USER_ALIAS -value "N172" -net -type string OUTPUT_BUFFER/dout<5> -design gatelevel 
set_attribute -name USER_ALIAS -value "N173" -net -type string OUTPUT_BUFFER/dout<4> -design gatelevel 
set_attribute -name USER_ALIAS -value "N174" -net -type string OUTPUT_BUFFER/dout<3> -design gatelevel 
set_attribute -name USER_ALIAS -value "N175" -net -type string OUTPUT_BUFFER/dout<2> -design gatelevel 
set_attribute -name USER_ALIAS -value "N176" -net -type string OUTPUT_BUFFER/dout<1> -design gatelevel 
set_attribute -name USER_ALIAS -value "N177" -net -type string OUTPUT_BUFFER/dout<0> -design gatelevel 
set_attribute -name BUS_INFO -value "10:INPUT:prog_empty_thresh_assert<9:0>" -instance -type string OUTPUT_BUFFER/BU2 -design gatelevel 
set_attribute -name CHECK_LICENSE_TYPE -value "fifo_apd_data,fifo_generator_v5_3,NONE,NONE" -instance -type string OUTPUT_BUFFER/BU2 -design gatelevel 
set_attribute -name CORE_GENERATION_INFO -value "fifo_apd_data,fifo_generator_v5_3,{c_has_int_clk=0,c_has_srst=0,c_rd_freq=1,c_enable_rst_sync=1,c_has_rd_data_count=0,c_din_width=51,c_has_wr_data_count=0,c_full_flags_rst_val=1,c_implementation_type=2,c_use_embedded_reg=0,c_wr_freq=1,c_use_dout_rst=1,c_underflow_low=0,c_has_overflow=0,c_preload_latency=0,c_dout_width=51,c_msgon_val=1,c_rd_depth=1024,c_error_injection_type=0,c_has_underflow=0,c_has_almost_full=0,c_has_rst=1,c_data_count_width=10,c_has_wr_ack=0,c_use_ecc=0,c_common_clock=0,c_wr_ack_low=0,c_rd_pntr_width=10,c_use_fwft_data_count=0,c_has_almost_empty=0,c_rd_data_count_width=10,c_overflow_low=0,c_wr_pntr_width=10,c_prog_empty_type=0,c_wr_data_count_width=10,c_preload_regs=1,c_dout_rst_val=0,c_has_data_count=0,c_prog_full_thresh_negate_val=1022,c_wr_depth=1024,c_prog_empty_thresh_negate_val=5,c_prog_empty_thresh_assert_val=4,c_has_valid=0,c_prog_full_thresh_assert_val=1023,component_name=fifo_apd_data,c_valid_low=0,c_prim_fifo_type=1kx36,c_prog_full_type=0,c_memory_type=1,}" -instance -type string OUTPUT_BUFFER/BU2 -design gatelevel 
set_attribute -name NB_BUSPIN_PROPS -value "OK" -instance -type string OUTPUT_BUFFER/BU2 -design gatelevel 
set_attribute -name NLW_UNIQUE_ID -value "0" -instance -type integer OUTPUT_BUFFER/BU2 -design gatelevel 
set_attribute -name NLW_MACRO_TAG -value "1" -instance -type integer OUTPUT_BUFFER/BU2 -design gatelevel 
set_attribute -name NLW_MACRO_ALIAS -value "fifo_apd_data_fifo_generator_v5_3_xst_1_BU2" -instance -type string OUTPUT_BUFFER/BU2 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" /fifo_apd_data_lib/fifo_apd_data/view_1_precision_fifo_apd_data -design gatelevel 
set_attribute -name NDF -value "TRUE" /fifo_apd_data_lib/fifo_apd_data/view_1_precision_fifo_apd_data -design gatelevel 
set_attribute -name contains_init_values -value "true" /fifo_apd_data_lib/fifo_apd_data/view_1_precision_fifo_apd_data -design gatelevel 
set_attribute -name NGC_DONTTOUCH -value "true" /fifo_apd_data_lib/fifo_apd_data/view_1_precision_fifo_apd_data -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/XST_GND -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/XST_VCC -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/ram_empty_fb_i -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/ram_full_i -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/ram_full_fb_i -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/rd_rst_asreg_d2 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/rd_rst_asreg_d2 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/rd_rst_asreg_d2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/wr_rst_asreg_d2 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/wr_rst_asreg_d2 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/wr_rst_asreg_d2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/rd_rst_asreg_d1 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/rd_rst_asreg_d1 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/rd_rst_asreg_d1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/wr_rst_asreg_d1 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/wr_rst_asreg_d1 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/wr_rst_asreg_d1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/rd_rst_asreg -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/rd_rst_asreg -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/rd_rst_asreg -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/ngwrdrst.grst.rd_rst_reg_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/ngwrdrst.grst.rd_rst_reg_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/ngwrdrst.grst.rd_rst_reg_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/wr_rst_asreg -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/wr_rst_asreg -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/wr_rst_asreg -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/ngwrdrst.grst.wr_rst_reg_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/ngwrdrst.grst.wr_rst_reg_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_50 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_49 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_48 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_47 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_46 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_45 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_44 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_43 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_42 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_41 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_40 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_39 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_38 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_37 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_36 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_35 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_34 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_33 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_32 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_31 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_30 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_29 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_28 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_27 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_26 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_25 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_24 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_23 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_22 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_21 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_20 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_19 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_18 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_17 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_16 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_15 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_14 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_13 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_12 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_11 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_10 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_9 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_8 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/dout_i_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c1/gmux.gm[4].gms.ms} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c1/gmux.gm[3].gms.ms} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c1/gmux.gm[2].gms.ms} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c1/gmux.gm[1].gms.ms} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c1/gmux.gm[0].gm1.m1} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c2/gmux.gm[4].gms.ms} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c2/gmux.gm[3].gms.ms} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c2/gmux.gm[2].gms.ms} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c2/gmux.gm[1].gms.ms} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c2/gmux.gm[0].gm1.m1} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c0/gmux.gm[4].gms.ms} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c0/gmux.gm[3].gms.ms} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c0/gmux.gm[2].gms.ms} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c0/gmux.gm[1].gms.ms} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c0/gmux.gm[0].gm1.m1} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c1/gmux.gm[4].gms.ms} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c1/gmux.gm[3].gms.ms} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c1/gmux.gm[2].gms.ms} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c1/gmux.gm[1].gms.ms} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c1/gmux.gm[0].gm1.m1} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_d1_9 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_d1_8 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_d1_7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_d1_6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_d1_5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_d1_4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_d1_3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_d1_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_d1_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_d1_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_9 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_8 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/gc0.count_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gr1.rfwft/curr_fwft_state_FSM_FFd2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gr1.rfwft/curr_fwft_state_FSM_FFd1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gr1.rfwft/empty_fwft_i -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gr1.rfwft/empty_fwft_fb -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d2_9 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d2_8 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d2_7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d2_6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d2_5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d2_4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d2_3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d2_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d2_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d2_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d1_9 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d1_8 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d1_7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d1_6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d1_5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d1_4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d1_3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d1_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d1_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_d1_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_9 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_8 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/gic0.gc0.count_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_bin_9 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_bin_8 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_bin_7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_bin_6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_bin_5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_bin_4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_bin_3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_bin_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_bin_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_bin_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_bin_9 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_bin_8 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_bin_7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_bin_6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_bin_5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_bin_4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_bin_3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_bin_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_bin_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_bin_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_d1_9 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_d1_8 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_d1_7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_d1_6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_d1_5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_d1_4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_d1_3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_d1_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_d1_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_d1_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_d1_9 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_d1_8 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_d1_7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_d1_6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_d1_5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_d1_4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_d1_3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_d1_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_d1_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_d1_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_9 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_9 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_9 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_8 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_8 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_8 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_7 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_7 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_6 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_6 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_5 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_5 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_4 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_4 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_3 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_3 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_2 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_2 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_1 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_1 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_0 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_0 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_asreg_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_9 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_9 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_9 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_8 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_8 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_8 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_7 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_7 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_6 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_6 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_5 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_5 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_4 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_4 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_3 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_3 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_2 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_2 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_1 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_1 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_0 -design gatelevel 
set_attribute -name ASYNC_REG -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_0 -design gatelevel 
set_attribute -name MSGON -value "TRUE" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_asreg_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_9 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_8 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/rd_pntr_gc_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_9 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_8 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/wr_pntr_gc_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/ram_wr_en_i1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/ram_wr_en_i1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/n00601 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/n00601 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/rd_rst_comb1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/rd_rst_comb1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/wr_rst_comb1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/rstblk/wr_rst_comb1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c1/v1<4>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c1/v1<4>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c1/v1<3>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c1/v1<3>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c1/v1<2>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c1/v1<2>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c1/v1<1>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c1/v1<1>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c1/v1<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c1/v1<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c2/v1<4>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c2/v1<4>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c2/v1<3>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c2/v1<3>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c2/v1<2>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c2/v1<2>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c2/v1<1>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c2/v1<1>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c2/v1<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/gwas.wsts/c2/v1<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c0/v1<4>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c0/v1<4>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c0/v1<3>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c0/v1<3>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c0/v1<2>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c0/v1<2>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c0/v1<1>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c0/v1<1>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c0/v1<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c0/v1<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c1/v1<4>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c1/v1<4>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c1/v1<3>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c1/v1<3>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c1/v1<2>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c1/v1<2>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c1/v1<1>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c1/v1<1>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c1/v1<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/c1/v1<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002771 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002771 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002731 -design gatelevel 
set_attribute -name PK_HLUTNM -value "___XLNM___0___U0/grf.rf/gl0.rd/rpntr/Mmux_n002721" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002731 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002731 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002721 -design gatelevel 
set_attribute -name PK_HLUTNM -value "___XLNM___0___U0/grf.rf/gl0.rd/rpntr/Mmux_n002721" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002721 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002721 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n0027411 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n0027411 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gr1.rfwft/curr_fwft_state_FSM_FFd2-In1 -design gatelevel 
set_attribute -name PK_HLUTNM -value "___XLNM___1___U0/grf.rf/gl0.rd/gr1.rfwft/curr_fwft_state_FSM_FFd1-In1" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gr1.rfwft/curr_fwft_state_FSM_FFd2-In1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gr1.rfwft/curr_fwft_state_FSM_FFd2-In1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gr1.rfwft/curr_fwft_state_FSM_FFd1-In1 -design gatelevel 
set_attribute -name PK_HLUTNM -value "___XLNM___1___U0/grf.rf/gl0.rd/gr1.rfwft/curr_fwft_state_FSM_FFd1-In1" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gr1.rfwft/curr_fwft_state_FSM_FFd1-In1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gr1.rfwft/curr_fwft_state_FSM_FFd1-In1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gr1.rfwft/RAM_REGOUT_EN1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gr1.rfwft/RAM_REGOUT_EN1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gr1.rfwft/Mmux_RAM_RD_EN_FWFT11 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gr1.rfwft/Mmux_RAM_RD_EN_FWFT11 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gr1.rfwft/Mmux_n005211 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gr1.rfwft/Mmux_n005211 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<3>11 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<3>11 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<7>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<7>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<6>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<6>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/Mmux_n002731 -design gatelevel 
set_attribute -name PK_HLUTNM -value "___XLNM___2___U0/grf.rf/gl0.wr/wpntr/Mmux_n002721" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/Mmux_n002731 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/Mmux_n002731 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/Mmux_n002721 -design gatelevel 
set_attribute -name PK_HLUTNM -value "___XLNM___2___U0/grf.rf/gl0.wr/wpntr/Mmux_n002721" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/Mmux_n002721 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/Mmux_n002721 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/n01081 -design gatelevel 
set_attribute -name PK_HLUTNM -value "___XLNM___3___U0/grf.rf/gcx.clkx/n01091" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/n01081 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/n01081 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/n01091 -design gatelevel 
set_attribute -name PK_HLUTNM -value "___XLNM___3___U0/grf.rf/gcx.clkx/n01091" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/n01091 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/n01091 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/n00461 -design gatelevel 
set_attribute -name PK_HLUTNM -value "___XLNM___4___U0/grf.rf/gcx.clkx/n00471" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/n00461 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/n00461 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/n00471 -design gatelevel 
set_attribute -name PK_HLUTNM -value "___XLNM___4___U0/grf.rf/gcx.clkx/n00471" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/n00471 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/n00471 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0004_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0004_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0005_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0005_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0006_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0006_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0007_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0007_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0008_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0008_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0009_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0009_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0010_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0010_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0066_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0066_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0011_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0011_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0012_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0012_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0067_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0067_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0068_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0068_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0069_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0069_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0070_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0070_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0071_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0071_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0072_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0072_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0073_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0073_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0074_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/Mxor_n0074_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_391_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_391_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_401_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_401_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_411_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_411_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_171_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_171_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_181_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_181_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_191_xo<0>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_191_xo<0>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_421_xo<0>_SW0 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_421_xo<0>_SW0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_421_xo<0> -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_421_xo<0> -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_201_xo<0>_SW0 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_201_xo<0>_SW0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_201_xo<0> -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_201_xo<0> -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_431_xo<0>_SW0 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_431_xo<0>_SW0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_431_xo<0> -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_431_xo<0> -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_211_xo<0>_SW0 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_211_xo<0>_SW0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_211_xo<0> -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_211_xo<0> -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_441_xo<0> -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_441_xo<0> -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_221_xo<0> -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_221_xo<0> -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_451_xo<0>_SW0 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_451_xo<0>_SW0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_451_xo<0> -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_451_xo<0> -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_231_xo<0>_SW0 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_231_xo<0>_SW0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_231_xo<0> -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gcx.clkx/reduce_xor_231_xo<0> -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<8>_SW0 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<8>_SW0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<8> -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<8> -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<9>_SW0 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<9>_SW0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<9> -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<9> -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.tmp_ram_rd_en1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.tmp_ram_rd_en1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/n00601 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/gras.rsts/n00601 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002710_SW1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002710_SW1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002710 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002710 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n00279_SW1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n00279_SW1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n00279 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n00279 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002742 -design gatelevel 
set_attribute -name PK_HLUTNM -value "___XLNM___5___U0/grf.rf/gl0.rd/rpntr/Mmux_n002751" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002742 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002742 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<3>2 -design gatelevel 
set_attribute -name PK_HLUTNM -value "___XLNM___6___U0/grf.rf/gl0.wr/wpntr/n0027<4>1" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<3>2 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<3>2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002751 -design gatelevel 
set_attribute -name PK_HLUTNM -value "___XLNM___5___U0/grf.rf/gl0.rd/rpntr/Mmux_n002751" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002751 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002751 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<4>1 -design gatelevel 
set_attribute -name PK_HLUTNM -value "___XLNM___6___U0/grf.rf/gl0.wr/wpntr/n0027<4>1" -instance -type string OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<4>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<4>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002761 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002761 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<5>1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/n0027<5>1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n00278 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n00278 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.rd/rpntr/Mmux_n002711_INV_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean OUTPUT_BUFFER/BU2/U0/grf.rf/gl0.wr/wpntr/Mmux_n002711_INV_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name BUS_INFO -value "4:INPUT:DIPA<3:0>" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name DATA_WIDTH_A -value "18" -instance -type integer {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name DATA_WIDTH_B -value "18" -instance -type integer {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name DOA_REG -value "0" -instance -type integer {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name DOB_REG -value "0" -instance -type integer {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name EN_RSTRAM_A -value "FALSE" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name EN_RSTRAM_B -value "TRUE" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name SRVAL_A -value "000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_00 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_01 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_02 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_03 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_04 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_05 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_06 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_07 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_00 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_01 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_02 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_03 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_04 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_05 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_06 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_07 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_08 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_09 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_0A -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_0B -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_0C -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_0D -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_0E -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_0F -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_10 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_11 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_12 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_13 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_14 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_15 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_16 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_17 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_18 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_19 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_1A -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_1B -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_1C -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_1D -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_1E -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_1F -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_20 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_21 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_22 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_23 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_24 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_25 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_26 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_27 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_28 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_29 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_2A -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_2B -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_2C -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_2D -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_2E -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_2F -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_30 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_31 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_32 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_33 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_34 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_35 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_36 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_37 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_38 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_39 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_3A -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_3B -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_3C -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_3D -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_3E -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_3F -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name init_file -value "NONE" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name RSTTYPE -value "SYNC" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name RST_PRIORITY_A -value "CE" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name RST_PRIORITY_B -value "CE" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name SIM_COLLISION_CHECK -value "ALL" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name SIM_DEVICE -value "SPARTAN6" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_A -value "000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_B -value "000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name WRITE_MODE_B -value "READ_FIRST" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name SRVAL_B -value "000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[2].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name BUS_INFO -value "4:INPUT:DIPA<3:0>" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name DATA_WIDTH_A -value "18" -instance -type integer {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name DATA_WIDTH_B -value "18" -instance -type integer {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name DOA_REG -value "0" -instance -type integer {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name DOB_REG -value "0" -instance -type integer {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name EN_RSTRAM_A -value "FALSE" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name EN_RSTRAM_B -value "TRUE" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name SRVAL_A -value "000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_00 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_01 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_02 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_03 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_04 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_05 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_06 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_07 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_00 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_01 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_02 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_03 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_04 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_05 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_06 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_07 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_08 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_09 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_0A -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_0B -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_0C -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_0D -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_0E -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_0F -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_10 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_11 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_12 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_13 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_14 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_15 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_16 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_17 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_18 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_19 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_1A -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_1B -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_1C -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_1D -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_1E -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_1F -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_20 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_21 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_22 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_23 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_24 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_25 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_26 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_27 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_28 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_29 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_2A -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_2B -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_2C -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_2D -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_2E -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_2F -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_30 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_31 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_32 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_33 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_34 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_35 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_36 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_37 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_38 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_39 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_3A -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_3B -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_3C -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_3D -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_3E -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_3F -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name init_file -value "NONE" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name RSTTYPE -value "SYNC" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name RST_PRIORITY_A -value "CE" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name RST_PRIORITY_B -value "CE" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name SIM_COLLISION_CHECK -value "ALL" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name SIM_DEVICE -value "SPARTAN6" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_A -value "000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_B -value "000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name WRITE_MODE_B -value "READ_FIRST" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name SRVAL_B -value "000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[1].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name BUS_INFO -value "4:INPUT:DIPA<3:0>" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name DATA_WIDTH_A -value "18" -instance -type integer {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name DATA_WIDTH_B -value "18" -instance -type integer {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name DOA_REG -value "0" -instance -type integer {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name DOB_REG -value "0" -instance -type integer {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name EN_RSTRAM_A -value "FALSE" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name EN_RSTRAM_B -value "TRUE" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name SRVAL_A -value "000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_00 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_01 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_02 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_03 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_04 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_05 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_06 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INITP_07 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_00 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_01 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_02 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_03 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_04 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_05 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_06 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_07 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_08 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_09 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_0A -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_0B -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_0C -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_0D -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_0E -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_0F -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_10 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_11 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_12 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_13 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_14 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_15 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_16 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_17 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_18 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_19 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_1A -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_1B -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_1C -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_1D -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_1E -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_1F -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_20 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_21 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_22 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_23 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_24 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_25 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_26 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_27 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_28 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_29 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_2A -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_2B -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_2C -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_2D -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_2E -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_2F -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_30 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_31 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_32 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_33 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_34 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_35 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_36 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_37 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_38 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_39 -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_3A -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_3B -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_3C -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_3D -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_3E -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_3F -value "0000000000000000000000000000000000000000000000000000000000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name init_file -value "NONE" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name RSTTYPE -value "SYNC" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name RST_PRIORITY_A -value "CE" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name RST_PRIORITY_B -value "CE" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name SIM_COLLISION_CHECK -value "ALL" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name SIM_DEVICE -value "SPARTAN6" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_A -value "000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name INIT_B -value "000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name WRITE_MODE_B -value "READ_FIRST" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name SRVAL_B -value "000000000" -instance -type string {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance {OUTPUT_BUFFER/BU2/U0/grf.rf/mem/gbm.gbmg.gbmga.ngecc.bmg/blk_mem_generator/valid.cstr/ramloop[0].ram.r/s6_noinit.ram/SDP.SIMPLE_PRIM18.ram} -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" /fifo_apd_data_lib/fifo_apd_data_fifo_generator_v5_3_xst_1/view_1_precision_fifo_apd_data -design gatelevel 
set_attribute -name NDF -value "true" /fifo_apd_data_lib/fifo_apd_data_fifo_generator_v5_3_xst_1/view_1_precision_fifo_apd_data -design gatelevel 
set_attribute -name contains_init_values -value "true" /fifo_apd_data_lib/fifo_apd_data_fifo_generator_v5_3_xst_1/view_1_precision_fifo_apd_data -design gatelevel 




##################
# Clocks
##################

