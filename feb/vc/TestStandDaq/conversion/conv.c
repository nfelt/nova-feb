    #include <stdio.h>
    #include<string.h>
    #include<math.h>

    int main()
    {
      	//***********BEGIN CHANGING BINARY TO HEX**********
		int i=0,a[5],t=0;
		int n;
		char buffer[100];
		int bufferint;
		int bitsperline;
		int eofloc;
		int currloc;
		int j;

		remove("hex.txt");

		//*****USER ENTER BITS PER LINE (multiples of 4 only)*******
		bitsperline=16;

		//open new hex file for writing
		FILE * hexfile=fopen("hex.txt","a+");

		//*****USER ENTER open binary file for reading
		#
		char text[20];
		fputs("enter binary file name: ", stdout);
		fflush(stdout);
		if ( fgets(text, sizeof text, stdin) != NULL )
		{
			char *newline = strchr(text, '\n'); /* search for newline character */
			if ( newline != NULL )
			{
				*newline = '\0'; /* overwrite trailing newline */
			}

		}


		FILE * binfile=fopen(text,"rb");

			i=0;
		    	while (!feof(binfile))
			{
		      		fgetc(binfile);
		      		i++;
				//printf("%d\n",i);
		     	}
			eofloc=ftell(binfile);
			rewind(binfile);

	currloc=ftell(binfile);
	//printf("EOF %d\n",eofloc);
	//printf("CURRLOC %d\n",currloc);



	//while file has not reached EOF
	while(currloc<eofloc-1)
	{

		//read and write the entire line
		i=1;
		while(i<=bitsperline/4)
		{
			//read in four bits
			fread(buffer,1,4,binfile);

			//change all bins to integers by x1000
			bufferint=atoi(buffer)*1000;
			//printf("%s\n",buffer);

			//cases for bin to hex
			 switch(bufferint)
			  {
			    case 0:
			     fprintf(hexfile,"%s","0");
			     break;
			    case 1000:
			     fprintf(hexfile,"%s","1");
			     break;
			    case 10000:
			     fprintf(hexfile,"%s","2");
			     break;
			    case 11000:
			     fprintf(hexfile,"%s","3");
			     break;
			    case 100000:
			     fprintf(hexfile,"%s","4");
			     break;
			    case 101000:
			     fprintf(hexfile,"%s","5");
			     break;
			    case 110000:
			     fprintf(hexfile,"%s","6");
			     break;
			    case 111000:
			     fprintf(hexfile,"%s","7");
			     break;
			    case 1000000:
			     fprintf(hexfile,"%s","8");
			     break;
			    case 1001000:
			     fprintf(hexfile,"%s","9");
			     break;
			    case 1010000:
			     fprintf(hexfile,"%s","A");
			     break;
			    case 1011000:
			     fprintf(hexfile,"%s","B");
			     break;
			    case 1100000:
			     fprintf(hexfile,"%s","C");
			     break;
			    case 1101000:
			     fprintf(hexfile,"%s","D");
			     break;
			    case 1110000:
			     fprintf(hexfile,"%s","E");
			     break;
			    case 1111000:
			     fprintf(hexfile,"%s","F");
			     break;
			}
			i++;
			currloc=ftell(binfile);

		}
		fprintf(hexfile,"\n");
		fseek(binfile,1,SEEK_CUR);
		currloc=ftell(binfile);
		//printf("%d\n",currloc);

	}
	//printf("%d",currloc);
	fclose(hexfile);
	fclose(binfile);


	//BEGING PUTTING TOGETHER ENTIRE FILE W HEX AND INSTRUCTIONS
	hexfile=fopen("hex.txt","rb");
	remove("FpgaConfig.xml");
	FILE* newfile=fopen("FpgaConfig.xml","w+");
	fprintf(newfile,"<?xml version=\"1.0\"?>\n<test_commands>\n<!-- Set ASIC REGs -->\n<!-- Write SPARE Reg -->\n");

	i=0;
	while (!feof(hexfile))
	{
		fgetc(hexfile);
		i++;
	}
	eofloc=ftell(hexfile);
	rewind(hexfile);

	int hexlines=(eofloc/6);
	int fullsectionsA=(hexlines/128);
	int fullsections=floor(fullsectionsA);
	int remaininglines=hexlines-128*fullsections;
	printf("hexlines %d\n",hexlines);
	printf("fullsectionsA %d\n",fullsectionsA);
	printf("remaininglines %d\n",remaininglines);

	char buffer2[6*128];

	for(i=1;i<=fullsections;i++)
	{
		//printf("%s",buffer2);
		fprintf(newfile,"<writeblock>\n");
		fread(buffer2,1,6*128,hexfile);
		fprintf(newfile,buffer2);
		fseek(newfile , -5 , SEEK_CUR );
		fprintf(newfile,"</writeblock>\n");
	}

		char buffer3[6*remaininglines];

		fprintf(newfile,"<writeblock>\n");
		fread(buffer3,1,6*remaininglines,hexfile);
		fprintf(newfile,buffer3);
		fseek(newfile,-4,SEEK_CUR);
		fprintf(newfile,"\n</writeblock>\n");

		fprintf(newfile,"</test_commands>");

		printf("\n\nDONE\n\n");


	fclose(newfile);
	fclose(hexfile);
}

