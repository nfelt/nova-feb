function [] = JustPlot(Filename)
%Filename = 'TestData\ADCINtie\FEB4_0_00556\00\';
number_of_channels = 32;
RawData = char(textread([Filename,'OscopeDataCh00.txt'],'%s', 'whitespace', ' -'));
chan_length = floor(length(RawData)/5);
Magnitude = zeros(chan_length,number_of_channels);
Timestamp = zeros(chan_length,number_of_channels);

for i = 0:number_of_channels-1
	RawData = char(textread([Filename,'OscopeDataCh',sprintf('%02d',i),'.txt'],'%s', 'whitespace', ' -'));
	RawData = RawData(1:chan_length*5,:);
	Magnitude(:,i+1) = hex2dec(RawData(4:5:length(RawData),2:4));
	Timestamp(:,i+1) = hex2dec (horzcat(RawData(3:5:length(RawData),2:4),RawData(3:5:length(RawData),2:4)));
end

figure(3);
plot(Magnitude);
title(['DATAin']);
legend('ch0','ch1','ch2','ch3','ch4','ch5','ch6','ch7','ch8','ch9','ch10','ch11','ch12','ch13','ch14','ch15','ch16','ch17','ch18','ch19','ch20','ch21','ch22','ch22','ch23','ch24','ch25','ch26','ch27','ch28','ch29','ch30','ch31');
xlabel('X');
ylabel('Y');
return;