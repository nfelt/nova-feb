function [] = GetOscopeDataItrig44(DataFile)

	%! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetChanEnableAll.xml data\logfile.txt
	! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetOscopeReadout.xml data\logfile.txt
	! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\PulseSingleItrig.xml data\logfile.txt
	system (['NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\read_44.xml ' ,DataFile]);
	! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\ClearBuff.xml data\logfile.txt
return

