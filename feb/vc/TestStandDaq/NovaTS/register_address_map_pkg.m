function addresshex=register_address_map_pkg(name)
if strcmp(name,'FIRMWARE_VERS')==1
    addresshex = '000B';

elseif strcmp(name,'CMD_ADDR')==1
    addresshex = '0101';

elseif strcmp(name,'ERROR_FLAG_ADDR')==1
    addresshex='0103';

elseif strcmp(name,'FIRMWARE_VERS_ADDR)==1')
    addresshex='0104';

elseif strcmp(name,'CURRENT_TIME_L_ADDR')==1
    addresshex='0010';

elseif strcmp(name,'CURRENT_TIME_U_ADDR')==1
    addresshex='0011';

elseif strcmp(name,'TEMP_ADDR')==1
    addresshex='0020';

elseif strcmp(name,'TEMP_LTS_ADDR')==1
    addresshex='0021';

elseif strcmp(name,'TEMP_UTS_ADDR')==1
    addresshex='0022';

elseif strcmp(name,'SERIAL_NUMBER_ADDR')==1
    addresshex='0030';

elseif strcmp(name,'CMD_GET_TEMP')==1
    addresshex='0020';

elseif strcmp(name,'CMD_RESET_FEB')==1
    addresshex='1000';

elseif strcmp(name,'CMD_SET_ASIC')==1
    addresshex='3000';

elseif strcmp(name,'CMD_START_DAQ')==1
    addresshex='1001';

elseif strcmp(name,'CMD_STOP_DAQ')==1
    addresshex='1002';

elseif strcmp(name,'CMD_RESET_TIME')==1
    addresshex='2000';

elseif strcmp(name,'CMD_START_TIME')==1
    addresshex='2001';

elseif strcmp(name,'CMD_STOP_TIME')==1
    addresshex='2002';

elseif strcmp(name,'CMD_DISABLE_TECC')==1
    addresshex='5000';

elseif strcmp(name,'CURRENT_TIME_L_ADDR')==1
    addresshex='5001';

elseif strcmp(name,'CMD_GET_DRIVE_MON')==1
    addresshex='5020';

elseif strcmp(name,'CMD_GET_TEMP_MON')==1
    addresshex='5030';

elseif strcmp(name,'CMD_SER_NUM_ADR_RESET')==1
    addresshex='0030';

elseif strcmp(name,'CMD_SER_NUM_WRITE_EN')==1
    addresshex='E030';

elseif strcmp(name,'DAQ_MODE_ADDR')==1
    addresshex='1000';

elseif strcmp(name,'DCS_DSP_MODE')==1
    addresshex='0000';

elseif strcmp(name,'OSCILLOSCOPE_MODE')==1
    addresshex='1000';

elseif strcmp(name,'MEMORY_MODE')==1
    addresshex='2000';

elseif strcmp(name,'MEMORY_LOOP_MODE')==1
    addresshex='2001';

elseif strcmp(name,'SINGLE_DATA_POINT_MODE')==1
    addresshex='3000';

elseif strcmp(name,'TIMING_PKT_RATE_ADDR')==1
    addresshex='1010';

elseif strcmp(name,'HIGH_VOLTAGE_ADJ_ADDR')==1
    addresshex='1020';

elseif strcmp(name,'SETPOINT_ADDR')==1
    addresshex='5010';

elseif strcmp(name,'DRIVE_MONITOR_ADDR')==1
    addresshex='5020';

elseif strcmp(name,'DRIVE_MONITOR_LTS_ADDR')==1
    addresshex='5021';

elseif strcmp(name,'DRIVE_MONITOR_UTS_ADDR')==1
    addresshex='5022';

elseif strcmp(name,'TEMP_MONITOR_ADDR')==1
    addresshex='5030';

elseif strcmp(name,'TEMP_MONITOR_LTS_ADDR')==1
    addresshex='5031';

elseif strcmp(name,'TEMP_MONITOR_UTS_ADDR')==1
    addresshex='5032';

elseif strcmp(name,'PULSER_ENABLE_ADDR')==1
    addresshex='F000';

elseif strcmp(name,'PULSER_PERIODICITY_ADDR')==1
    addresshex='F001';

elseif strcmp(name,'PULSER_WIDTH_ADDR')==1
    addresshex='F002';

elseif strcmp(name,'SPARE_ADDR')==1
    addresshex='4040';

elseif strcmp(name,'VTSEL_ADDR')==1
    addresshex='4041';

elseif strcmp(name,'REFSEL_ADDR')==1
    addresshex='4042';

elseif strcmp(name,'ISEL_ADDR')==1
    addresshex='4043';

elseif strcmp(name,'BWSEL_ADDR')==1
    addresshex='4044';

elseif strcmp(name,'GSEL_ADDR')==1
    addresshex='4045';

elseif strcmp(name,'TFSEL_ADDR')==1
    addresshex='4046';

elseif strcmp(name,'MUX2TO1_ADDR')==1
    addresshex='4047';

elseif strcmp(name,'MUX8TO1_ADDR')==1
    addresshex='4048';
    
else
    error('INVALID ADDRESS NAME');
end


%    CMD_WRITE_DAC       = '001';
%    CMD_READ_ADC        = '010';
%    CMD_READ_TEMP       = '011';
%    CMD_WRITE_SER_NUM   = '100';
%    CMD_READ_SER_NUM    = '101';
%    CMD_WREN_SER_NUM    = '110';
%    CMD_RST_SER_NUM_PTR = '111';
%    
%    OFFS_ADDR    = '40' & '000';
%    MASK_ADDR    = '40' & '001';
%    THRESHOLD_ADDR = '20' & '000';
return
