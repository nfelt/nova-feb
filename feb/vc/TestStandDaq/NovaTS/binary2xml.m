function binary2xml
binfile=fopen('../../par_11_5/top.rbt','r')
xmlfile=fopen('NovaCmdScripts/FebV4_00_12.xml','w');

i=1;
while(i<=7)
    headerline=fgetl(binfile);
    display(headerline);
    i=i+1;
end

fprintf(xmlfile,'<?xml version="1.0"?>');
fprintf(xmlfile,'\r\n'); %new line
fprintf(xmlfile,'<test_commands>');
fprintf(xmlfile,'\r\n'); %new line

 while(feof(binfile)==0)
     fprintf(xmlfile,'<writeblock>\nE006\n');
     i=1;
     while(i<=255 && feof(binfile)==0)
        binline=fgetl(binfile);
        hexline=bin2hex(binline);
        %display(hexline);
        fprintf(xmlfile,[hexline,'\n']);
        i=i+1;
     end
    fprintf(xmlfile,'</writeblock>\n');
 end
 
fprintf(xmlfile,'<sleep time="1033"/>');
fprintf(xmlfile,'</test_commands>');
 
     
    
    


fclose(binfile);
fclose(xmlfile);
return