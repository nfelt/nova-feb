function [] = ShowMeFebData(Filename)
number_of_channels = 32;
RawData = char(textread(Filename,'%s', 'whitespace', ' -'));
ChanOrder = hex2dec(RawData(1:5:160,3:4));

%Magnitude = Magnitude(4:5:length(RawData));
chan_length = length(RawData)/(number_of_channels *5);
[Tmp SortOrder] = sort(ChanOrder);
Magnitude = hex2dec(RawData(4:5:length(RawData),2:4));
Magnitude = reshape(Magnitude,number_of_channels,chan_length)';
Magnitude = Magnitude(:,SortOrder);

Timestamp = hex2dec (horzcat(RawData(3:5:length(RawData),2:4),RawData(3:5:length(RawData),2:4)));
Timestamp = reshape(Timestamp,number_of_channels,chan_length)';
Timestamp = Timestamp(:,SortOrder);


size(Magnitude)

% % figure(1);
% % for i = 0:15
% % subplot(4,4,i+1)
% % plot(Magnitude(:,i+1));
% % title(['DATAin']);
% % xlabel('X');
% % ylabel('Y');
% % end;
% % 
% 
% 
figure(3);
plot(Magnitude);
title(['DATAin']);
legend('ch0','ch1','ch2','ch3','ch4','ch5','ch6','ch7','ch8','ch9','ch10','ch11','ch12','ch13','ch14','ch15','ch16','ch17','ch18','ch19','ch20','ch21','ch22','ch22','ch23','ch24','ch25','ch26','ch27','ch28','ch29','ch30','ch31');
xlabel('X');
ylabel('Y');

DCS = [1 -1];
MagnitudeSpec = filter(DCS,1,Magnitude);
MagnitudeSpec = MagnitudeSpec(4:length(MagnitudeSpec),1:32);

Fs = 2e6;                    % Sampling frequency
T = 1/Fs;                     % Sample time
L = length(MagnitudeSpec(:,1));                     % Length of signal
t = (0:L-1)*T;                % Time vector

NFFT = 2^nextpow2(L); % Next power of 2 from length of y
Y = fft(MagnitudeSpec,NFFT)/L;
f = Fs/2*linspace(0,1,NFFT/2);
fm = [0:31];
% Plot single-sided amplitude spectrum.
figure(15);
mag = 2*abs(Y(1:NFFT/2,:));
surf(fm,f,mag), 
shading interp
title('Single-Sided Amplitude Spectrum of y(t)')
xlabel('Frequency (Hz)')
ylabel('|Y(f)|')

DCS = [1 0 0 -1];
Magnitude = filter(DCS,1,Magnitude);
Magnitude = Magnitude(4:length(Magnitude),1:32);


for i = 1:32
    for j = 1:32
        coa(i,j) = corr2(Magnitude(:,i),Magnitude(:,j));
    end
end
figure(4)
h = bar3(coa);
for i = 1:length(h)
    zdata = ones(6*length(h),4);
    k = 1;
    for j = 0:6:(6*length(h)-6)
        zdata(j+1:j+6,:) = coa(k,i);
        k = k+1;
    end
    set(h(i),'Cdata',zdata)
end
colormap jet
colorbar
shading interp
for i = 1:length(h)
    zdata = get(h(i),'Zdata');
    set(h(i),'Cdata',zdata)
    set(h,'EdgeColor','k')
end
 chan_mean = mean(Magnitude);
chan_mean'

figure(2);
for i = 0:31
subplot(8,4,i+1)
title(['DATAin']);
xlabel('X');
ylabel('Y');
hbin = min(Magnitude(:,i+1)):1:max(Magnitude(:,i+1));
hist(Magnitude(:,i+1),hbin);
stdh(i+1)= std(Magnitude(:,i+1));

title(['CH',num2str(i),' STD = ', num2str((std(Magnitude(:,i+1))))]);
end;
size(Magnitude)
stdh' * 44

figure(86)
std_data = std(Magnitude(:,:))*44;
bar(std_data);


num2str(mean(std_data))

figure(12);
title(['DATAin']);
xlabel('X');
ylabel('Y');
hbin = min(std_data):1:max(std_data);
hist(std_data,hbin);

figure(13);
plot(Magnitude);
title(['DATAin']);
legend('ch0','ch1','ch2','ch3','ch4','ch5','ch6','ch7','ch8','ch9','ch10','ch11','ch12','ch13','ch14','ch15','ch16','ch17','ch18','ch19','ch20','ch21','ch22','ch23','ch24','ch25','ch26','ch27','ch28','ch29','ch30','ch31');
xlabel('X');
ylabel('Y');
end
