function [Status, ErrorFlag, FirmwareVersion] = GetStatusErrorVers()
    Parameters;
    ! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\GetStatusErrorVers.xml TempLogfile.txt
    DataCharIn = char(textread('TempLogfile.txt','%s', 'whitespace', ' -'));
    if length(DataCharIn) ~= 6
        disp('error in GetStatusErrorVers, Check configuration.');
        Status = '0000';
        ErrorFlag = '0000';
        FirmwareVersion =  '0000';
    else 
        Status = DataCharIn(2,:);
        ErrorFlag = DataCharIn(4,:);
        FirmwareVersion = DataCharIn(6,:);
        if (FirmwareVersion ~= ExpectedFirmwareVersion)
            error('Unexpected Firmware Version');
        end  
    end
return;
