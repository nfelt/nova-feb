
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetAsic5037.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetOscopeReadout.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\PulseSingleInExternal.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\read_40.xml %FebDataFolder%\PulseAsic5037.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\ClearBuff.xml data\logfile.txt

! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetAsic5030.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetOscopeReadout.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\PulseSingleInExternal.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\read_40.xml %FebDataFolder%\PulseAsic5030.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\ClearBuff.xml data\logfile.txt

! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetAsic5F37.xml data\.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetOscopeReadout.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\PulseSingleInExternal.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\read_40.xml %FebDataFolder%\PulseAsic5F37.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\ClearBuff.xml data\logfile.txt

! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetAsic5F30.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetOscopeReadout.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\PulseSingleInExternal.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\read_40.xml %FebDataFolder%\PulseAsic5F30.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\ClearBuff.xml data\logfile.txt

! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetAsic5733.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetOscopeReadout.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\PulseSingleInExternal.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\read_40.xml %FebDataFolder%\PulseAsic5733.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\ClearBuff.xml data\logfile.txt