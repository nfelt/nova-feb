y = [257 333 402 643 780];
x = [2.7 10 18 33 47];

        [LinPar S]=polyfit(x,y,1); 
        HVNormr= S.normr;
    f = polyval(LinPar,x);    
        plot(x,y,'o',x,f,'-')
title(['Linear Fit Slope = ', num2str(LinPar(1)),' Intercept = ', num2str(LinPar(2))]);
xlabel('pF')
ylabel('e-')