clear all;
% ! novacmdapp -o -e -f scripts\start_daq.xml data\txt\outfile.txt
% ! novacmdapp  -o -e -l 20 scripts\read_5k.xml data\txt\outfile.txt
fid = fopen('\\Heplpc5\data\daq\data\txt\outfile.txt');
m = textscan(fid, '%s', 'commentStyle', '-');
m = m{1};
fclose(fid);
hit_data = m(4:end-1,:);
hit_length = length(hit_data)/4;
channel_data = hex2dec(hit_data(1:4:end,:));
magnitude_data = hex2dec(hit_data(2:4:end,:));
timestamp_data = (hex2dec(cat(2,hit_data(3:4:end,:),hit_data(4:4:end,:)))).*.5e-6;
a = hit_data(3:4:end,:);
b= hit_data(4:4:end,:);
diff = timestamp_data(2:end) - timestamp_data(1:end-1)
x = linspace(0,length(diff)/64,length(diff));
figure (1)
plot(x,diff);
% ! novacmdapp -o -e -f scripts\stop_daq.xml data\txt\outfile.txt
figure (2)
plot(channel_data);