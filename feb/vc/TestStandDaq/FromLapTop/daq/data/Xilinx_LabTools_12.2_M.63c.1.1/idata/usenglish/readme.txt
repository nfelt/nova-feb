ISE Design Suite 12
READ ME

Dear Xilinx Customer,

Welcome, and thank you for choosing the Xilinx ISE Design Suite 12 software.
This file contains information about software and documentation installation. 
See the following resources for additional information:

* READ ME FIRST (printed hard copy in DVD shipments only): provides important 
  information that you must know before installing the software.

* ISE Design Suite 12: Installation, Licensing, and Release Notes (available
  on the Xilinx website at www.xilinx.com/cgi-bin/SW_Docs_Redirect/
  sw_docs_redirect?ver=12.2&locale=en&topic=release+notes): 
  lists ISE Design Suite 12 software contents and provides installation and
  licensing instructions. 

* What's New in Xilinx ISE Design Suite 12 (HTML): lists the new features in 
  this release. This file can be viewed as follows from the ISE Design Suite InfoCenter:
  
  * From the Project Navigator Workspace: Select the menu command
    Help > ISE Design Suite InfoCenter. The ISE Design Suite InfoCenter includes 
    a link under the Resources section to the What's New in Xilinx ISE Design Suite.

  * Windows: Select Start > Programs > Xilinx ISE Design Suite 12.2 > 
    Documentation > ISE Design Suite InfoCenter. 
 
  * From the idata/usenglish directory on the ISE DVD.


Software Installation
*********************
For more information on software installation, see the 
ISE Design Suite 12: Installation, Licensing, and Release Notes.

For information on the software version loaded on your computer, see the
Xilinx XInfo System Checker. You can launch the XInfo System Checker by
running the xinfo file from the root directory of the 
ISE Design Suite 12 DVD or from your operating system as follows:

* Windows: Select Start > Programs > Xilinx ISE Design Suite 12.2 > Accessories > 
  XInfo System Checker.

* Linux: Run the xinfo file from the $XILINX/bin/lin directory.

* Linux64: Run the xinfo file from the $XILINX/bin/lin64 directory.


Software Manuals Installation
*****************************
The Xilinx ISE Design Suite documentation automatically installs as part of the
software installation. After you install the software, you can select
Help > Software Manuals to access the Software Manuals collection.

For added convenience, Xilinx Software Manuals are also on the Web. Go 
to www.xilinx.com/documentation, select the Design Tools tab and expand
ISE Design Suite.


Known Issues 
************
The ISE Design Suite 12: Installation, Licensing, and Release Notes contains a 
link to known issues and workaround information. 


Technical Support
*****************
For Technical Support Issues, visit www.xilinx.com/support where features
such as the Answers Database and User Forums may help to resolve your issue. If 
your issue can not be resolved on the support site, a WebCase can be created at 
www.xilinx.com/support/clearexpress/websupport.htm and a Technical Support 
engineer can further assist you. 



