clear all;
% ! C:\data\daq\novacmdapp -o -e -f C:\data\daq\cmds\ped_trig.xml C:\data\daq\cmds\outfile.txt
! novacmdapp -o -e -f scripts\feb_test.xml data\txt\outfile.txt
% ! "C:\Program Files\XEmacs\XEmacs-21.4.20\i586-pc-win32\xemacs.exe"
% C:\data\daq\cmds\outfile.txt
number_of_channels = 32;
m = char(textread('data\txt\outfile.txt', '%s', 'whitespace', ' -'));
chan_length = length(m)/number_of_channels;
chan_nun= hex2dec(m(:,1));
chan_data = hex2dec(m(:,2:4));
chan_data = reshape(chan_data,number_of_channels,chan_length)';
chan_0_index = -find((chan_nun(1:32)==15) & (chan_nun(2:33)==0))+1;
chan_data_tmp = circshift(chan_data,[0,chan_0_index]);
chan_data(:,[1:(number_of_channels/2)]) = chan_data_tmp (:,[2:2:number_of_channels]);
chan_data(:,[(number_of_channels/2+1):number_of_channels]) = chan_data_tmp (:,[1:2:number_of_channels]);


% figure(1);
% for i = 0:15
% subplot(4,4,i+1)
% plot(chan_data(:,i+1));
% title(['DATAin']);
% xlabel('X');
% ylabel('Y');
% end;
% 

figure(3);
plot(chan_data);
title(['DATAin']);
legend('ch0','ch1','ch2','ch3','ch4','ch5','ch6','ch7','ch8','ch9','ch10','ch11','ch12','ch13','ch14','ch15');
xlabel('X');
ylabel('Y');

% for i = 1:16
%     for j = 1:16
%         coa(i,j) = corr2(chan_data(:,i),chan_data(:,j));
%     end
% end
% figure(4)
% bar3(coa)
% figure(5)
% image(30 * (coa + 1))
% meancorr = mean(mean(abs(coa)))
% title(['mean corr = ',num2str(meancorr)])
% colormap(hot)
% f = findobj('Type','surface');
% shading interp
 chan_mean = mean(chan_data);
chan_mean'
DCS = [1 0 0 -1];
chan_data = filter(DCS,1,chan_data);
chan_data = chan_data(5:length(chan_data),1:16);

figure(2);

for i = 0:15
subplot(4,4,i+1)
title(['DATAin']);
xlabel('X');
ylabel('Y');
hbin = min(chan_data(:,i+1)):1:max(chan_data(:,i+1));
hist(chan_data(:,i+1),hbin);
stdh(i+1)= std(chan_data(:,i+1));

title(['CH',num2str(i),' STD = ', num2str((std(chan_data(:,i+1))))]);
end;



stdh'
num2str(mean(std(chan_data(:,i+1))))
