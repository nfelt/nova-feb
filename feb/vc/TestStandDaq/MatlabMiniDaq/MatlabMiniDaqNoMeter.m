clear all;

    OscopeFitOK = -1;
    PulseFitOK = -1;
    TeccLoopbackFitOK = -1;
    TestResult = 'Fail';
    ConfigDaq;
    ConfigCheck;
    FebDataFolder= 'tmpdata';
    setenv('FebDataFolder', FebDataFolder);
    !mkdir %FebDataFolder%;
        [ApdMaxNoise, ApdMinNoise, OscopeFitOK] = OscopeNoiseCheck(FebDataFolder);
        if min([OscopeFitOK])>0
            TestResult = 'Pass';
        end;
    disp('********************');
    disp(['**  Test ',TestResult,'ed!  **']);
    disp('********************');

    
    