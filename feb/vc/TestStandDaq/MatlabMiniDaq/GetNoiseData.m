%clear all;

DataFilePfix = 'OscopeData';
DataFileIndex = 1;
DataFileSfix = '.txt';

RepeatTest = 'y';%
while RepeatTest == 'y'
   % CheckHV;
   % ConfigDaq;
    ConfigCheck;
    [Status Error FirmwareVersion]= GetStatusErrorVers();
    [SerNumbMsg, SerNumOK] = ReadSerialNumber;
    FebSN = SerNumbMsg(1:12);
    FebDataFolder= strcat('TestData\',FebSN(1:4),'_',FebSN(6),'_',FebSN(8:12),'\');
    setenv('FebDataFolder', FebDataFolder);
    !mkdir %FebDataFolder%;
   % maxArraySum = zeros(21,32);
   % runCount = 0;
    if SerNumOK == 1
        while RepeatTest == 'y'
            ConfigCheck;
            DataFileIndex = DataFileIndex +1;
            GetOscopeData([FebDataFolder,DataFilePfix,sprintf('%03d',DataFileIndex),DataFileSfix]);
            ShowMeFebData([FebDataFolder,DataFilePfix,sprintf('%03d',DataFileIndex),DataFileSfix]);
            %use the following for pulse inject average
            %runCount = runCount+1,
            %maxArraySum = ShowMeFebDataRetMax21([FebDataFolder,DataFilePfix,sprintf('%03d',DataFileIndex),DataFileSfix])+ maxArraySum;
            %figure(182)
            %maxArraySumCorrected = maxArraySum ./runCount;
            %plot(maxArraySumCorrected);
     
          RepeatTest = input('do you want repeat? y/n [y]: ', 's');
            if isempty(RepeatTest)
                RepeatTest = 'y';
            end
        end
       
    end
    RepeatTest = input('The testis finished. Do you want to test another FEB? y/n [y]: ', 's');
    if isempty(RepeatTest)
        RepeatTest = 'y';
    end
end
    