clear all;
RepeatTest = 'y';
while RepeatTest == 'y'
    HVFitOK = -1;
    OscopeFitOK = -1;
    PulseFitOK = -1;
    TeccLoopbackFitOK = -1;
    TestResult = 'Fail';
    CheckHV;
    AnalogVoltage = Get2_5VA;
    ConfigDaq;
    ConfigCheck;
    [Status Error FirmwareVersion]= GetStatusErrorVers();
    [SerNumbMsg, SerNumOK] = writeserialnumber;
    FebSN = SerNumbMsg(1:12);
    FebDataFolder= strcat('data\',FebSN(1:4),'_',FebSN(6),'_',FebSN(8:12),'\');
    setenv('FebDataFolder', FebDataFolder);
    !mkdir %FebDataFolder%;
    if SerNumOK == 1
        input('Please cover the box and turn on the High Voltage. [ok] ', 's');
        [HVData, HVFitPar, HVNormr, HVFitOK] = HVTestAuto(FebDataFolder);
        input('Please turn off the High Voltage. [ok] ', 's');
        CheckHV
        [ApdMaxNoise, ApdMinNoise, OscopeFitOK] = OscopeNoiseCheck(FebDataFolder);
        PulseFitOK = PulseBoundsTest(FebDataFolder);
        [LoopData, TeccLoopbackFitOK] = TeccLoopbackTest(FebDataFolder);
        if min([SerNumOK, HVFitOK, OscopeFitOK, PulseFitOK, TeccLoopbackFitOK])>0
            TestResult = 'Pass';
        end;
    end
    disp('********************');
    disp(['**  Test ',TestResult,'ed!  **']);
    disp('********************');
    TesterComment = input('Is there anything you would like to say? : ', 's');
    if isempty(TesterComment)
        TesterComment = 'No commant.';
    end
     fid = fopen('data\FebMiniDaqResults.txt', 'a');
    if SerNumOK == 1
       fprintf(fid, '\n%s,%s,%s,%s,%d,%d,%d,%d,%d,%1.3f,%3.0f,%3.0f,%1.4f,%3.1f,%1.3f,%3.2f,%3.2f,%3.2f,%3.2f,%3.2f,%3.2f,%3.2f,%3.2f,%3.2f,%3.2f,%3.2f,%3.2f,%3.2f,%3.2f,%3.2f,%d,%d,%d,%d,%d,%d,%d,%d,%s',FebSN,datestr(now,30),FirmwareVersion,TestResult,SerNumOK, HVFitOK, OscopeFitOK, PulseFitOK, TeccLoopbackFitOK,AnalogVoltage,ApdMaxNoise,ApdMinNoise,HVFitPar,HVNormr,HVData,LoopData,TesterComment);
    else
       fprintf(fid, '\n%s,%s,,%s,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,%s',FebSN,date,TestResult,TesterComment); 
    end
    fclose(fid);
    RepeatTest = input('The test is finished. Do you want to test another FEB? y/n [y]: ', 's');
    if isempty(RepeatTest)
        RepeatTest = 'y';
    end
end
    