function [bound] = PulseBoundGen(Filename)
	number_of_channels = 32;
	m = char(textread(Filename,'%s', 'whitespace', ' -'));
	chan_data = hex2dec(m(:,2:4));
	chan_data = chan_data(4:5:length(chan_data));
	chan_length = length(chan_data)/number_of_channels;
	chan_data = reshape(chan_data,number_of_channels,chan_length)';
	chan_data_m = mean(chan_data(1:10,:));
	chan_data = chan_data - repmat(chan_data_m,length(chan_data),1);
	ubound = max(chan_data') + 80;
	lbound = min(chan_data') - 80;
	bounds = cat(2,ubound(1:62)',lbound(1:62)',ubound(3:64)',lbound(3:64)',ubound(2:63)',lbound(2:63)');
	ubound = floor(max(bounds'));
	lbound = floor(min(bounds'));
	ubound = cat(2, ubound(1), ubound, ubound(length(ubound)));
	lbound = cat(2, lbound(1), lbound, lbound(length(lbound)));
	bound = cat(1,ubound,lbound);
return;

end

