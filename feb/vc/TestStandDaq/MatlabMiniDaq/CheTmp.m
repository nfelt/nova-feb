clear all;
% Find a tcpip object.
for i = 1:20
obj1 = instrfind('Type', 'tcpip', 'RemoteHost', '140.247.132.120', 'RemotePort', 3490, 'Tag', '');
% Create the tcpip object if it does not exist
% otherwise use the object that was found.
if isempty(obj1)
    obj1 = tcpip('140.247.132.120', 3490);
else
    fclose(obj1);
    obj1 = obj1(1)
end
% Connect to instrument object, obj1.
fopen(obj1);
pause(.5)
% Communicating with instrument object, obj1.
fprintf(obj1, 'conf:volt 10');
fprintf(obj1, ':volt:nplc 10');
fprintf(obj1, ':trig:sour bus');
fprintf(obj1, ':init');
fprintf(obj1, '*trg');
fprintf(obj1, 'fetch?');
data5 = fscanf(obj1);
disp(data5);

% Disconnect from instrument object, obj1.
fclose(obj1);
pause(1)
% Configure instrument object, obj1.
%set(obj1, 'Name', 'TCPIP-140.247.132.89');
%set(obj1, 'RemoteHost', '140.247.132.89');
end