function [DcsStd] = GetDcsStdSingle(Filename)
	RawData = char(textread(Filename,'%s', 'whitespace', ' -'));
	chan_length = floor(length(RawData)/5);
	RawData = RawData(1:chan_length*5,:);
	Magnitude = hex2dec(RawData(4:5:length(RawData),2:4));

	figure(3);
	plot(Magnitude);
	title(['DATAin']);
	legend('chxx');
	xlabel('X');
	ylabel('Y');


	DCS = [1 0 0 -1];
	Magnitude = filter(DCS,1,Magnitude);
	Magnitude = Magnitude(4:length(Magnitude));

	DcsStd = std(Magnitude);
end


