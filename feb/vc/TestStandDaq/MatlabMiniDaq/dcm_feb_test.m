clear all;
% ! C:\data\daq\novacmdapp -o -e -f C:\data\daq\cmds\ped_trig.xml C:\data\daq\cmds\outfile.txt
% TestStandDaq\NovaCmdScripts\scripts
% ! "C:\Program Files\XEmacs\XEmacs-21.4.20\i586-pc-win32\xemacs.exe"
% C:\data\daq\cmds\outfile.txt
number_of_channels = 32;
m = char(textread('N:\Projects\nova\feb\v4\TestStandDaq\FromLapTop\daq\data\outfile.txt', '%s', 'whitespace', ' -'));

chan_nun= hex2dec(m(:,1));
chan_data = hex2dec(m(:,2:4));
chan_data = chan_data(4:5:length(chan_data));
chan_length = length(chan_data)/number_of_channels;
chan_data = reshape(chan_data,number_of_channels,chan_length)';
size(chan_data)


size(chan_data)

% figure(1);
% for i = 0:15
% subplot(4,4,i+1)
% plot(chan_data(:,i+1));
% title(['DATAin']);
% xlabel('X');
% ylabel('Y');
% end;
% 

figure(3);
plot(chan_data);
title(['DATAin']);
legend('ch0','ch1','ch2','ch3','ch4','ch5','ch6','ch7','ch8','ch9','ch10','ch11','ch12','ch13','ch14','ch15','ch16','ch17','ch18','ch19','ch20','ch21','ch22','ch22','ch23','ch24','ch25','ch26','ch27','ch28','ch29','ch30','ch31');
xlabel('X');
ylabel('Y');
size(chan_data)
for i = 1:32
    for j = 1:32
        coa(i,j) = corr2(chan_data(:,i),chan_data(:,j));
    end
end
figure(4)
h = bar3(coa);
for i = 1:length(h)
    zdata = ones(6*length(h),4);
    k = 1;
    for j = 0:6:(6*length(h)-6)
        zdata(j+1:j+6,:) = coa(k,i);
        k = k+1;
    end
    set(h(i),'Cdata',zdata)
end
colormap jet
colorbar
shading interp
for i = 1:length(h)
    zdata = get(h(i),'Zdata');
    set(h(i),'Cdata',zdata)
    set(h,'EdgeColor','k')
end
 chan_mean = mean(chan_data);
chan_mean'

DCS = [1 -1];
chan_data = filter(DCS,1,chan_data);
chan_data = chan_data(7:length(chan_data),1:32);


Fs = 2e6;                    % Sampling frequency
T = 1/Fs;                     % Sample time
L = length(chan_data(:,1));                     % Length of signal
t = (0:L-1)*T;                % Time vector

NFFT = 2^nextpow2(L); % Next power of 2 from length of y
Y = fft(chan_data,NFFT)/L;
f = Fs/2*linspace(0,1,NFFT/2);
fm = [0:31];
% Plot single-sided amplitude spectrum.
figure(15);
mag = 2*abs(Y(1:NFFT/2,:));
surf(fm,f,mag), 
shading interp
title('Single-Sided Amplitude Spectrum of y(t)')
xlabel('Frequency (Hz)')
ylabel('|Y(f)|')


figure(2);
for i = 0:31
subplot(8,4,i+1)
title(['DATAin']);
xlabel('X');
ylabel('Y');
hbin = min(chan_data(:,i+1)):1:max(chan_data(:,i+1));
hist(chan_data(:,i+1),hbin);
stdh(i+1)= std(chan_data(:,i+1));

title(['CH',num2str(i),' STD = ', num2str((std(chan_data(:,i+1))))]);
end;
size(chan_data)
stdh' * 44

std_data = std(chan_data(:,:))*44;
num2str(mean(std_data))

figure(12);
title(['DATAin']);
xlabel('X');
ylabel('Y');
hbin = min(std_data):1:max(std_data);
hist(std_data,hbin);

figure(13);
plot(chan_data);
title(['DATAin']);
legend('ch0','ch1','ch2','ch3','ch4','ch5','ch6','ch7','ch8','ch9','ch10','ch11','ch12','ch13','ch14','ch15','ch16','ch17','ch18','ch19','ch20','ch21','ch22','ch23','ch24','ch25','ch26','ch27','ch28','ch29','ch30','ch31');
xlabel('X');
ylabel('Y');
