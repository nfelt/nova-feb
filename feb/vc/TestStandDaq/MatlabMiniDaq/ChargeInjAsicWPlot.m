ConfigCheck
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetAsic5037.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\SetOscopeReadout.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\PulseSingleInExternal.xml data\logfile.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\read_40.xml TestData\ChargeInjAsicWPlotResults.txt
! NovaCmdScripts\novacmdapp -o -e -f NovaCmdScripts\ClearBuff.xml data\logfile.txt


number_of_channels = 32;
	m = char(textread('TestData\ChargeInjAsicWPlotResults.txt','%s', 'whitespace', ' -'));
	chan_nun= hex2dec(m(:,1));
	chan_data = hex2dec(m(:,2:4));
	chan_data = chan_data(4:5:length(chan_data));
	chan_length = length(chan_data)/number_of_channels;
	chan_data = reshape(chan_data,number_of_channels,chan_length)';
	chan_data_m = mean(chan_data(1:10,:));
	chan_data = chan_data - repmat(chan_data_m,length(chan_data),1);

    figure(3);
    plot(chan_data);
    title(['DATAin']);
    legend('ch0','ch1','ch2','ch3','ch4','ch5','ch6','ch7','ch8','ch9','ch10','ch11','ch12','ch13','ch14','ch15','ch16','ch17','ch18','ch19','ch20','ch21','ch22','ch22','ch23','ch24','ch25','ch26','ch27','ch28','ch29','ch30','ch31');
    xlabel('X');
    ylabel('Y');


