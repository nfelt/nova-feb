function [AnalogVoltage] = Get2_5VA()
    Parameters;
    RegAddress;
    RepeatTest = 'y';
    FitCheck = 0;
while RepeatTest == 'y'
        % Find a tcpip object.
        obj1 = instrfind('Type', 'tcpip', 'RemoteHost', lvipaddr, 'RemotePort', 3490, 'Tag', '');
        % Create the tcpip object if it does not exist
        % otherwise use the object that was found.
        if isempty(obj1)
            obj1 = tcpip(lvipaddr, 3490);
        else
            fclose(obj1);
            obj1 = obj1(1);
        end
        % Connect to instrument object, obj1.
pause(.5)
        fopen(obj1);

        % Communicating with instrument object, obj1.
        fprintf(obj1, '*rst');
        fprintf(obj1, 'conf:volt 10 ');
        fprintf(obj1, ':volt:nplc 10');
        fprintf(obj1, ':trig:sour bus');
        fprintf(obj1, ':init');
        fprintf(obj1, '*trg');
        fprintf(obj1, 'fetch?');
         pause(1)
         AnalogVoltage = str2double(fscanf(obj1));

        % Disconnect from instrument object, obj1.
        fclose(obj1);

        if  (AnalogVoltageUBound > AnalogVoltage) && (AnalogVoltageLBound < AnalogVoltage)
            FitCheck = 1;
            RepeatTest = 'n';
        else  
            RepeatTest = input('The 2.5V test failed Is the 3.3 V supply on? repeat? y/n [y]: ', 's');
            if isempty(RepeatTest)
                RepeatTest = 'y';
            end
        end
    end
end
