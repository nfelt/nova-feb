function [HVData, FitCheck] = HVTestAuto()
    Parameters;
    RegAddress;
    RepeatTest = 'y';
    FitCheck = 0;
    while RepeatTest == 'y'
        ConfigCheck;
        % Find a tcpip object.
        obj1 = instrfind('Type', 'tcpip', 'RemoteHost', '140.247.132.082', 'RemotePort', 3490, 'Tag', '');

        % Create the tcpip object if it does not exist
        % otherwise use the object that was found.
        if isempty(obj1)
            obj1 = tcpip('140.247.132.082', 3490);
        else
            fclose(obj1);
            obj1 = obj1(1)
        end
        % Connect to instrument object, obj1.
        fopen(obj1);

        % Communicating with instrument object, obj1.
        fprintf(obj1, '*rst');
        fprintf(obj1, 'conf:volt 10 ');
        fprintf(obj1, ':volt:nplc 10');
        fprintf(obj1, ':trig:sour bus');

        HVData = zeros(1,16,1);
        for i = 0:15
        dcmwrite(HVReg,strcat('0',dec2hex(i),'00'));
        fprintf(obj1, ':init');
        fprintf(obj1, '*trg');
        fprintf(obj1, 'fetch?');
        HVData(i+1) = str2double(fscanf(obj1))*1000;
        end

        % Disconnect from instrument object, obj1.
        fclose(obj1);

        x = [3840:-256:0];
        LinPar=polyfit(3840:-256:0,HVData,1); 

        if  (HVSlopeUBound > LinPar(1)) && (HVSlopeLBound < LinPar(1)) && (HVInterceptUbound > LinPar(2)) && (HVInterceptLbound < LinPar(2));
            FitCheck = 1;
            RepeatTest = 'n';
        else  
            RepeatTest = input('The test failed do you want repeat? y/n [y]: ', 's');
            if isempty(RepeatTest)
                RepeatTest = 'y';
            end
        end
    end
end
