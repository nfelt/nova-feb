
% find the RMS of std of all FEBs in a text file list

fileName = 'good4_0List.txt'

febsToProcess = getFileList(fileName);
febsToProcess(:,[5 7]) = '_';
fileListLength = size(febsToProcess,1);
number_of_channels = 32;

std_data = zeros(fileListLength,number_of_channels);
for i = 1:fileListLength;
    febFileLoc = ['data/',febsToProcess(i,:),'/OscopeData.txt'];
    disp(febFileLoc);
    RawData = char(textread(febFileLoc,'%s', 'whitespace', ' -'));
    ChanOrder = hex2dec(RawData(1:5:160,3:4));
    
    %Magnitude = Magnitude(4:5:length(RawData));
    chan_length = length(RawData)/(number_of_channels *5);
    [Tmp SortOrder] = sort(ChanOrder);
    Magnitude = hex2dec(RawData(4:5:length(RawData),2:4));
    Magnitude = reshape(Magnitude,number_of_channels,chan_length)';
    Magnitude = Magnitude(:,SortOrder);
    
    %Timestamp = hex2dec (horzcat(RawData(3:5:length(RawData),2:4),RawData(3:5:length(RawData),2:4)));
    %Timestamp = reshape(Timestamp,number_of_channels,chan_length)';
    %Timestamp = Timestamp(:,SortOrder);
    
    DCS = [1 0 0 -1];
    Magnitude = filter(DCS,1,Magnitude);
    Magnitude = Magnitude(4:length(Magnitude),1:32);
    std_data(i,:) = std(Magnitude(:,:))*44;
end
disp([num2str(fileListLength),' total processed boards']);
figure(86)
stdDataAll =  sqrt(mean(std_data.^2));
bar(stdDataAll);
xlabel('Channel');
ylabel('\sigma in e-');
title(['Standard deveation of each channel on ',num2str(fileListLength),' FEB4.0 boards']);

figure(2);
flatStd = reshape(std_data,1,[]);
hbin = min(flatStd(:,:)):1:max(flatStd(:,:));
hist(flatStd,hbin);
title(['Standard deveation of all channels on ',num2str(fileListLength),' FEB4.0 boards']);
ylabel('N');
xlabel('\sigma in e-');
