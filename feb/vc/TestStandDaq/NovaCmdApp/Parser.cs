using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using CyUSB;

namespace ConsoleApplication1
{
    class Parser
    {
       public byte[] bytes=new byte[4];
       public int argcount=0;
       NovaInterface myNovaInterface;
       CyUSBDevice myDev;
       string infile, outfile;

       public  Parser(){

           myNovaInterface = new NovaInterface();
           myNovaInterface.USBInit(doUSBUpdate);

           myDev = myNovaInterface.getDevice() as CyUSBDevice;            
       }

        public void doUSBUpdate()
        {
            myDev = myNovaInterface.getDevice() as CyUSBDevice;
        }

       public void RunArgList(string [] args){
           argcount = args.Length;
           bool good = false;
           int loop = 1;

           for(int i = 0; i < argcount; i++){
               if(args[i] == "-f"){
                   if(i > argcount-2) 
                       Console.WriteLine("Invalid Input, correct usage => -f <infile> <logfile>");
                   infile = args[i+1];
                   outfile = args[i+2];
                   i += 2;
                   good = true;
               }
               if(args[i] == "-l"){
                   if(i > argcount-3) 
                   Console.WriteLine("Invalid Input, correct usage => -l <N> <infile> <logfile>");
                   loop = int.Parse(args[i+1]);
                   infile = args[i+2];
                   outfile = args[i+3];
                   i += 3;
                   good = true;
               }
           }
           
           for(int i = 0; i < loop && good; i++){
               parseXMLfile(infile, outfile, i);
           }

       }

       private void parseXMLfile(string inputfile, string outputfile, int rep)
       {
           int totalbuffersout = 0;
           int totalbuffersin = 0;
           int totalbytesout = 0;
           int totalbytesin = 0;
           int totalerrorsin = 0;
           int totalerrorsout = 0;
            
           TextWriter logfile;
           if (rep == 0)
           {
               if (outputfile == "-") logfile = null;
               else logfile = new StreamWriter(outputfile);

               if (logfile != null)
                   logfile.WriteLine("Nova Test Station\r\nTest Started  {0}\r\n\r\n", System.DateTime.Now);
           }

            if (inputfile == "-") return;
            try{
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.Load(inputfile);
            XmlNode tempnode = xmldoc.DocumentElement.FirstChild;
            while (tempnode !=null&&myNovaInterface.getDevice() as CyUSBDevice!=null)
            {
                if (tempnode.Name == "writeblock")
                {
          //          outbufferlength=0;
                    String tempstring = tempnode.InnerText;
                    
                    if(logfile!=null)logfile.WriteLine("***out***---writeblock---");
                    if (logfile != null) logfile.Write(tempstring);

                    if (myNovaInterface.writehexstring(tempstring))
                    {
                        totalbuffersout++;
                        totalbytesout += myNovaInterface.outbufferlength;
                    }
                    else
                    {
                        totalerrorsout++;                      
                        if (logfile != null) logfile.WriteLine("data send error");
                    }

                    if (logfile != null) logfile.Write("\r\n\r\n");
                }
                else if (tempnode.Name == "readblock")
                {                    
                    if (logfile != null) logfile.WriteLine("***in***---readblock---");

       
                    String tempstring=null;
                    if (myNovaInterface.readhexstring(ref tempstring))
                    {
                        if (logfile != null) logfile.Write(tempstring);
                        if (logfile != null) logfile.Write("\r\n\r\n");
                        totalbytesin += myNovaInterface.inbufferlength;
                        totalbuffersin++;

                    }
                    else
                    {
                        if (logfile != null) logfile.WriteLine("data read error");
                        totalerrorsin++;
                    }
                }
                else if (tempnode.Name == "writemem")
                {

                    uint location = uint.Parse(tempnode.Attributes.GetNamedItem("location").Value, System.Globalization.NumberStyles.AllowHexSpecifier);
                    int count = int.Parse(tempnode.Attributes.GetNamedItem("count").Value, System.Globalization.NumberStyles.AllowHexSpecifier) * 4;

                    myNovaInterface.writemem(location, tempnode.InnerText);

                    if (logfile != null) logfile.WriteLine("***out***---write mem at " + string.Format("{0:x2}", location).ToUpper() + " for " + string.Format("{0:x2}", count / 4).ToUpper() + " entries---");
                    if (logfile != null) logfile.WriteLine(tempnode.InnerText);
                }
                else if (tempnode.Name == "readmem")
                {
                    
                    if (tempnode.Attributes.GetNamedItem("location") == null) return;
                    if (tempnode.Attributes.GetNamedItem("count") == null) return;
                  

                    uint location=uint.Parse(tempnode.Attributes.GetNamedItem("location").Value,System.Globalization.NumberStyles.AllowHexSpecifier);
                    int count = int.Parse(tempnode.Attributes.GetNamedItem("count").Value, System.Globalization.NumberStyles.AllowHexSpecifier) * 4;

                    Byte[] buff =new Byte[2000];

                    if (myNovaInterface.readmem(location, ref count, ref buff))
                    {

                        if (logfile != null) logfile.WriteLine("***in***---read mem at " + string.Format("{0:x2}", location).ToUpper() + " for " + string.Format("{0:x2}", count / 4).ToUpper() + " entries---");

                        for (int i = 0; i < count; i++)
                        {
                            if (logfile != null) logfile.Write(string.Format("{0:x2}", buff[i]));
                        }
                        
                        if (logfile != null) logfile.Write("\r\n");
                    }
                    else
                    {
                        if (logfile != null) logfile.WriteLine("***out***---read mem error");
                    }

                }
                else if (tempnode.Name == "usbcommand")
                {
                    if (tempnode.Attributes.Count == 0) return;
                    if (tempnode.Attributes.GetNamedItem("command") == null) return;
                    if (tempnode.Attributes.GetNamedItem("command").Value == "reset device")
                    {
                        myNovaInterface.sendreset();
                        logfile.WriteLine("usbcommand \r\n-Reset Device\r\n\r\n");
                    }
                    else if (tempnode.Attributes.GetNamedItem("command").Value == "reset interface")
                    {
                        myNovaInterface.reset();
                        logfile.WriteLine("usbcommand \r\n-Reset Interface\r\n\r\n");
                    }
                }
                tempnode = tempnode.NextSibling;
            }

            if (logfile != null) logfile.Close();

            }
            catch (Exception e)
            {
            }
        }






    }
}
