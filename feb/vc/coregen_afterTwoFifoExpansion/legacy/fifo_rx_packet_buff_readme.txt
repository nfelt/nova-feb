The following files were generated for 'fifo_rx_packet_buff' in directory
/Projects/nova/feb/v4/cgtemp/

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * fifo_rx_packet_buff.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * fifo_generator_ug175.pdf
   * fifo_rx_packet_buff.ngc
   * fifo_rx_packet_buff.vhd
   * fifo_rx_packet_buff.vho

Creates an HDL instantiation template:
   Creates an HDL instantiation template for the IP.

   * fifo_rx_packet_buff.vho

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * fifo_rx_packet_buff.asy

Generate ISE metadata:
   Create a metadata file for use when including this core in ISE designs

   * fifo_rx_packet_buff_xmdf.tcl

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * fifo_rx_packet_buff.gise
   * fifo_rx_packet_buff.xise

Deliver Readme:
   Text file indicating the files generated and how they are used.

   * fifo_rx_packet_buff_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * fifo_rx_packet_buff_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

