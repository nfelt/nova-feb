onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /top_with_dcm_emulator/u_0/clk_128
add wave -noupdate /top_with_dcm_emulator/dcm_data_n
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_1/u_0/shift_reg_rx
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_1/u_0/encoded_rx
add wave -noupdate /top_with_dcm_emulator/u_1/u_0/buffer_full
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_1/u_0/data_rx
add wave -noupdate /top_with_dcm_emulator/u_1/u_0/data_rx_strb
add wave -noupdate /top_with_dcm_emulator/u_0/data_processing_inst/tick_1us
add wave -noupdate /top_with_dcm_emulator/u_0/timing_inst/current_time(5)
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/time_marker_cnt
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/time_marker_rate
add wave -noupdate /top_with_dcm_emulator/u_0/data_processing_inst/send_tm
add wave -noupdate /top_with_dcm_emulator/u_0/dcm_sync
add wave -noupdate /top_with_dcm_emulator/u_0/dcm_sync_en
add wave -noupdate /top_with_dcm_emulator/u_0/enable_time
add wave -noupdate /top_with_dcm_emulator/u_0/enable_status_pkt
add wave -noupdate -expand -group {USB IO} -radix hexadecimal /top_with_dcm_emulator/usb_fd
add wave -noupdate -expand -group {USB IO} /top_with_dcm_emulator/usb_ifclk
add wave -noupdate -expand -group {USB IO} /top_with_dcm_emulator/usb_flagb
add wave -noupdate -expand -group {USB IO} /top_with_dcm_emulator/usb_flagc
add wave -noupdate -expand -group {USB IO} /top_with_dcm_emulator/usb_pktend
add wave -noupdate -expand -group {USB IO} /top_with_dcm_emulator/usb_reset_b
add wave -noupdate -expand -group {USB IO} /top_with_dcm_emulator/usb_sloe
add wave -noupdate -expand -group {USB IO} /top_with_dcm_emulator/usb_slrd
add wave -noupdate -expand -group {USB IO} /top_with_dcm_emulator/usb_slwr
add wave -noupdate -expand -group {Clks and FEB States} /top_with_dcm_emulator/u_1/u_4/clk_3_2_int
add wave -noupdate -expand -group {Clks and FEB States} /top_with_dcm_emulator/u_0/clk_16
add wave -noupdate -expand -group {Clks and FEB States} /top_with_dcm_emulator/u_0/clk_32
add wave -noupdate -expand -group {Clks and FEB States} /top_with_dcm_emulator/u_0/clk_64
add wave -noupdate -expand -group {Clks and FEB States} /top_with_dcm_emulator/u_0/asic_clk_out
add wave -noupdate -expand -group {Clks and FEB States} /top_with_dcm_emulator/u_0/enable_status_pkt
add wave -noupdate -expand -group {Clks and FEB States} /top_with_dcm_emulator/u_0/enable_time
add wave -noupdate -expand -group {Clks and FEB States} /top_with_dcm_emulator/u_0/enable_daq
add wave -noupdate -expand -group {Clks and FEB States} /top_with_dcm_emulator/u_0/asic_chipreset_int
add wave -noupdate -group stim_in /top_with_dcm_emulator/adc_input_stimulus/adc_clk
add wave -noupdate -group stim_in /top_with_dcm_emulator/adc_input_stimulus/adc_doutan
add wave -noupdate -group stim_in /top_with_dcm_emulator/adc_input_stimulus/adc_doutap
add wave -noupdate -group stim_in /top_with_dcm_emulator/adc_input_stimulus/adc_doutbn
add wave -noupdate -group stim_in /top_with_dcm_emulator/adc_input_stimulus/adc_doutbp
add wave -noupdate -group stim_in /top_with_dcm_emulator/adc_input_stimulus/asic_chipreset
add wave -noupdate -expand -group {ADC Data In} /top_with_dcm_emulator/u_0/asic_chipreset
add wave -noupdate -expand -group {ADC Data In} /top_with_dcm_emulator/u_0/dcm_sync
add wave -noupdate -expand -group {ADC Data In} /top_with_dcm_emulator/u_0/asic_clk_out
add wave -noupdate -expand -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/timing_inst/adc_chan_ptr
add wave -noupdate -expand -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/timing_inst/asic_chan_ptr
add wave -noupdate -expand -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/timing_inst/chan_number
add wave -noupdate -expand -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/timing_inst/clk_32
add wave -noupdate -expand -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/timing_inst/use_clk16_inv_int
add wave -noupdate -expand -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/adc_doutap
add wave -noupdate -expand -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/adc_doutbp
add wave -noupdate -expand -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/raw_chan_number
add wave -noupdate -expand -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/raw_magnitude
add wave -noupdate -expand -group {ADC Data In} -radix hexadecimal /top_with_dcm_emulator/u_0/current_time
add wave -noupdate -expand -group {Data Proc} /top_with_dcm_emulator/u_0/data_processing_inst/legacy_mode
add wave -noupdate -expand -group {Data Proc} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/raw_chan_number
add wave -noupdate -expand -group {Data Proc} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/raw_magnitude
add wave -noupdate -expand -group {Data Proc} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/dsp_chan_number
add wave -noupdate -expand -group {Data Proc} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/dsp_magnitude
add wave -noupdate -expand -group {Data Proc} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/dsp_strb
add wave -noupdate -expand -group {Data Proc} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/dsp_trigger
add wave -noupdate -expand -group {Data Proc} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/event_fifo_dout
add wave -noupdate -expand -group {Data Proc} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/oscope_strb
add wave -noupdate -expand -group {Data Proc} /top_with_dcm_emulator/u_0/data_processing_inst/send_tm
add wave -noupdate -expand -group {Data Proc} /top_with_dcm_emulator/u_0/data_processing_inst/tick_1us
add wave -noupdate -expand -group {Data Proc} /top_with_dcm_emulator/u_0/data_processing_inst/time_marker_cnt
add wave -noupdate -expand -group {Data Proc} /top_with_dcm_emulator/u_0/data_processing_inst/time_marker_rate
add wave -noupdate -expand -group {Data Proc} -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/trig_holdoff
add wave -noupdate /top_with_dcm_emulator/u_0/event_fifo_dout
add wave -noupdate /top_with_dcm_emulator/u_0/event_fifo_empty
add wave -noupdate /top_with_dcm_emulator/u_0/event_fifo_rstrb
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(0)/multi_point_ctrl_inst/event_fifo_din
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(0)/multi_point_ctrl_inst/event_fifo_wstrb
add wave -noupdate -radix hexadecimal /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(0)/multi_point_ctrl_inst/write_cafe_del
add wave -noupdate -expand -group {mp buffers} /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(0)/multi_point_ctrl_inst/event_fifo_full_int
add wave -noupdate -expand -group {mp buffers} /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(0)/multi_point_ctrl_inst/event_fifo_wstrb
add wave -noupdate -expand -group {mp buffers} /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(0)/multi_point_ctrl_inst/tdata_mem_full
add wave -noupdate -expand -group {mp buffers} /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(0)/multi_point_ctrl_inst/tdata_mem_wdata
add wave -noupdate -expand -group {mp buffers} /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(0)/multi_point_ctrl_inst/thead_fifo_full
add wave -noupdate -expand -group {mp buffers} /top_with_dcm_emulator/u_0/data_processing_inst/adc_seg_proc_gen(0)/multi_point_ctrl_inst/thead_fifo_wstrb
add wave -noupdate -group Spi /top_with_dcm_emulator/u_0/controller_inst/spi_interface_inst/spi_interface_state
add wave -noupdate -group Spi /top_with_dcm_emulator/u_0/controller_inst/spi_interface_inst/tec_err_shdn_int
add wave -noupdate -group Spi /top_with_dcm_emulator/u_0/controller_inst/spi_interface_inst/auto_upd
add wave -noupdate -group Spi /top_with_dcm_emulator/u_0/controller_inst/tec_err_reset
add wave -noupdate -group Spi /top_with_dcm_emulator/u_0/controller_inst/tec_err_shdn
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {79853000 ps} 0} {{Cursor 3} {159861251 ps} 0} {{Cursor 4} {437562600 ps} 0} {{Cursor 5} {192370100 ps} 0} {{Cursor 5} {58590100 ps} 0}
configure wave -namecolwidth 214
configure wave -valuecolwidth 42
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {63684193 ps} {97136318 ps}
