onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/usb_fd
add wave -noupdate -format Logic /top_with_dcm_emulator/usb_flagb
add wave -noupdate -format Logic /top_with_dcm_emulator/usb_flagc
add wave -noupdate -format Logic /top_with_dcm_emulator/usb_pktend
add wave -noupdate -format Logic /top_with_dcm_emulator/usb_reset_b
add wave -noupdate -format Logic /top_with_dcm_emulator/usb_sloe
add wave -noupdate -format Logic /top_with_dcm_emulator/usb_slrd
add wave -noupdate -format Logic /top_with_dcm_emulator/usb_slwr
add wave -noupdate -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/all_apd_data
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/data_strb
add wave -noupdate -format Literal /top_with_dcm_emulator/u_0/u_2/chan_magnitude
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_2/reset
add wave -noupdate -format Literal /top_with_dcm_emulator/u_0/u_2/chan_triggered
add wave -noupdate -format Literal /top_with_dcm_emulator/u_0/u_2/raw_timestamp
add wave -noupdate -format Literal /top_with_dcm_emulator/u_0/u_2/raw_magnitude
add wave -noupdate -format Literal /top_with_dcm_emulator/u_0/u_2/chan_number
add wave -noupdate -divider {spi sigs}
add wave -noupdate -format Logic /top_with_dcm_emulator/tecc_enable_b
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_5/new_s_cmd
add wave -noupdate -format Literal /top_with_dcm_emulator/u_0/u_5/raw_spi_command
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_5/s_cmd_busy
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_5/set_asic
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_5/tec_adc_ch1_b_ch2
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_5/tec_adc_cs
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_5/tec_dac_sync
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_5/tec_dac_ldac
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_5/temp_sensor_cs
add wave -noupdate -format Logic /top_with_dcm_emulator/ser_num_cs_b
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_5/spi_din
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_5/spi_sclk
add wave -noupdate -format Logic /top_with_dcm_emulator/u_0/u_5/spi_dout
add wave -noupdate -expand -group {A/d C Values} -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_5/u_0/setpoint
add wave -noupdate -expand -group {A/d C Values} -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_5/u_3/drive_mon_data
add wave -noupdate -expand -group {A/d C Values} -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_5/u_3/temp_data
add wave -noupdate -expand -group {A/d C Values} -format Literal -radix hexadecimal /top_with_dcm_emulator/u_0/u_5/u_3/temp_mon_data
add wave -noupdate -format Literal /top_with_dcm_emulator/u_0/u_5/u_3/ser_num_mem_addr
add wave -noupdate -format Literal /top_with_dcm_emulator/u_0/u_5/u_3/serial_number
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {93920200 ps} 0}
configure wave -namecolwidth 385
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {64133081 ps} {90115963 ps}
