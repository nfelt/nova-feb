onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group BEEBUS -format Literal -radix hexadecimal /top/beebus_addr
add wave -noupdate -expand -group BEEBUS -format Logic /top/clk_16
add wave -noupdate -expand -group BEEBUS -format Literal -radix hexadecimal /top/beebus_data
add wave -noupdate -expand -group BEEBUS -format Logic /top/beebus_read
add wave -noupdate -expand -group BEEBUS -format Logic /top/beebus_strb
add wave -noupdate -format Logic /top/usb_sloe
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/usb_fd
add wave -noupdate -format Logic /top/usb_slrd
add wave -noupdate -format Logic /top/usb_slwr
add wave -noupdate -format Logic /top/usb_pktend
add wave -noupdate -expand -group SPYBUS -format Literal -radix hexadecimal /top/u_4/spybus_data
add wave -noupdate -expand -group SPYBUS -format Logic -radix hexadecimal /top/u_4/spybus_read
add wave -noupdate -format Literal /top/u_5/u_1/beebus_data_int
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_1/dcmbus_data
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_1/dcmbus_data_int
add wave -noupdate -format Logic /top/u_4/u_0/spybus_data_sel
add wave -noupdate -format Literal /top/u_4/u_6/beebus_dbyte_reg
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/spybus_data
add wave -noupdate -format Logic /top/u_4/spybus_read
add wave -noupdate -format Logic /top/u_4/spybus_strb
add wave -noupdate -format Logic /top/powerup_reset
add wave -noupdate -divider {Fpga Config bits}
add wave -noupdate -format Literal /top/u_4/u_3/spybus_addr
add wave -noupdate -format Logic /top/u_4/u_3/serialize_word_done
add wave -noupdate -format Literal /top/u_4/u_3/config_data
add wave -noupdate -format Logic /top/u_4/u_3/shift_bits
add wave -noupdate -format Literal /top/u_4/u_3/bit_count
add wave -noupdate -format Logic /top/config_clk
add wave -noupdate -format Logic /top/config_serial_out
add wave -noupdate -divider DcmBeebusInterface
add wave -noupdate -format Literal /top/u_4/u_1/cmd_packet_count
add wave -noupdate -format Literal /top/u_4/u_1/current_command
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_1/data_rx
add wave -noupdate -format Logic /top/u_4/u_1/data_rx_strb
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {10073729000 ps} 0} {{Cursor 2} {98691725 ps} 0} {{Cursor 3} {1972950000 ps} 0} {{Cursor 4} {1079882000 ps} 0}
configure wave -namecolwidth 281
configure wave -valuecolwidth 142
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {7377405 ps} {18012769 ps}
