onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group BEEBUS
add wave -noupdate -group BEEBUS -format Literal -radix hexadecimal /top/beebus_addr
add wave -noupdate -group BEEBUS -format Logic /top/clk_16
add wave -noupdate -group BEEBUS -format Literal -radix hexadecimal /top/beebus_data
add wave -noupdate -group BEEBUS -format Logic /top/beebus_read
add wave -noupdate -group BEEBUS -format Logic /top/beebus_strb
add wave -noupdate -format Logic /top/usb_sloe
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/usb_fd
add wave -noupdate -format Logic /top/usb_slrd
add wave -noupdate -format Logic /top/usb_slwr
add wave -noupdate -format Logic /top/usb_pktend
add wave -noupdate -expand -group febus
add wave -noupdate -group febus -format Literal /top/u_4/febus_addr
add wave -noupdate -group febus -format Literal /top/u_4/febus_data
add wave -noupdate -expand -group SPYBUS
add wave -noupdate -group SPYBUS -format Literal -radix hexadecimal /top/u_4/spybus_data
add wave -noupdate -group SPYBUS -format Logic -radix hexadecimal /top/u_4/spybus_read
add wave -noupdate -format Literal /top/u_5/u_1/beebus_data_int
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_1/dcmbus_data
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_1/dcmbus_data_int
add wave -noupdate -format Logic /top/u_4/u_0/spybus_data_sel
add wave -noupdate -format Literal /top/u_4/u_6/beebus_dbyte_reg
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/spybus_data
add wave -noupdate -format Logic /top/u_4/spybus_read
add wave -noupdate -format Logic /top/u_4/spybus_strb
add wave -noupdate -format Logic /top/powerup_reset
add wave -noupdate -format Logic /top/enable_daq
add wave -noupdate -format Logic /top/apd_data_buffer_full
add wave -noupdate -format Logic /top/u_0/in_clk
add wave -noupdate -format Logic /top/apd_data_buffer_empty
add wave -noupdate -format Logic /top/u_0/out_clk
add wave -noupdate -divider {ASIC Shift Bits}
add wave -noupdate -format Literal -radix hexadecimal /top/current_time
add wave -noupdate -format Literal -radix hexadecimal /top/u_5/u_1/asic_shift_register
add wave -noupdate -format Logic /top/u_5/u_1/asic_chipreset
add wave -noupdate -format Logic /top/u_5/u_1/asic_srck
add wave -noupdate -format Logic /top/u_5/u_1/asic_shiftin
add wave -noupdate -format Logic /top/u_5/u_1/asic_shiftout
add wave -noupdate -format Logic /top/asic_chipreset
add wave -noupdate -divider {DAQ Modes}
add wave -noupdate -format Literal /top/u_5/u_0/controller_state
add wave -noupdate -format Logic /top/u_5/u_0/enable_daq
add wave -noupdate -format Logic /top/u_5/u_0/reset_feb
add wave -noupdate -format Logic /top/u_5/u_0/set_asic
add wave -noupdate -format Logic /top/u_5/u_0/update_temp_reg
add wave -noupdate -divider Pulser
add wave -noupdate -format Literal -radix hexadecimal /top/u_5/u_2/rate_count
add wave -noupdate -format Literal -radix hexadecimal /top/u_5/u_2/test_inject_rate
add wave -noupdate -format Logic /top/asic_testinject
add wave -noupdate -format Logic /top/instrument_trigger
add wave -noupdate -format Logic /top/internal_trigger
add wave -noupdate -divider {APD Data}
add wave -noupdate -format Literal -radix hexadecimal /top/fmat_data
add wave -noupdate -format Logic /top/fmat_data_strb
add wave -noupdate -format Logic /top/apd_data_buffer_empty
add wave -noupdate -format Literal -radix hexadecimal /top/apd_data
add wave -noupdate -format Logic /top/apd_data_strb
add wave -noupdate -format Logic /top/u_0/apd_data_in_strb_int
add wave -noupdate -format Logic /top/u_0/apd_data_out_strb_int
add wave -noupdate -divider {rx tx data}
add wave -noupdate -format Logic /top/u_4/u_1/data_packet_done
add wave -noupdate -format Logic /top/u_4/u_1/data_packet_ready
add wave -noupdate -format Logic /top/apd_data_buffer_empty
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/data_rx
add wave -noupdate -format Logic /top/u_4/data_rx_strb
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/data_tx
add wave -noupdate -format Logic /top/u_4/data_tx_strb
add wave -noupdate -divider DcmEmuSend
add wave -noupdate -format Literal /top/u_4/u_2/data_tx
add wave -noupdate -format Logic /top/u_4/u_2/kin
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_2/usb_fifo_dout
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_2/usb_fifo_din
add wave -noupdate -format Logic /top/u_4/u_2/usb_fifo_empty
add wave -noupdate -format Logic /top/u_4/u_2/usb_fifo_read_en
add wave -noupdate -format Logic /top/u_4/u_2/usb_fifo_write_en
add wave -noupdate -divider DcmBeebusInterface
add wave -noupdate -format Literal /top/u_4/u_1/cmd_packet_count
add wave -noupdate -format Literal /top/u_4/u_1/current_command
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_1/data_rx
add wave -noupdate -format Logic /top/u_4/u_1/data_rx_strb
add wave -noupdate -format Logic /top/u_4/u_1/clk_3_2
add wave -noupdate -divider {DCM_Emulator Receive}
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_0/data_rx
add wave -noupdate -format Logic /top/u_4/u_0/data_rx_strb
add wave -noupdate -format Logic /top/u_4/u_0/buffer_empty
add wave -noupdate -format Literal /top/u_4/u_0/words_in_pkt
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_0/read_data_count
add wave -noupdate -format Logic /top/u_4/u_0/buffer_read_ready
add wave -noupdate -format Logic /top/u_4/u_0/spybus_data_sel
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_0/spybus_data_int
add wave -noupdate -format Logic /top/u_4/u_0/dcm_pkt_end
add wave -noupdate -format Logic /top/u_4/u_0/spybus_data_sel_int
add wave -noupdate -divider buffer
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_0/read_data_count
add wave -noupdate -format Logic /top/u_4/u_0/clk_3_2_int
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_0/data_rx
add wave -noupdate -format Logic /top/u_4/u_0/data_rx_strb
add wave -noupdate -format Logic /top/u_4/u_0/reset_int
add wave -noupdate -format Logic /top/u_4/u_0/spybus_clk
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_0/spybus_data_int
add wave -noupdate -format Logic /top/u_4/u_0/spybus_data_sel_int
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {10073729000 ps} 0} {{Cursor 2} {32371000 ps} 0} {{Cursor 3} {73611000 ps} 0} {{Cursor 4} {4646361000 ps} 0}
configure wave -namecolwidth 281
configure wave -valuecolwidth 142
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {17396101 ps} {79542141 ps}
