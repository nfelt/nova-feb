onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic /top/usb_slwr
add wave -noupdate -format Logic /top/usb_flagb
add wave -noupdate -format Logic /top/usb_flagc
add wave -noupdate -format Logic /top/usb_sloe
add wave -noupdate -format Literal -radix hexadecimal /top/usb_fd
add wave -noupdate -expand -group BEEBUS -format Literal -radix hexadecimal /top/beebus_addr
add wave -noupdate -expand -group BEEBUS -format Logic /top/clk_16
add wave -noupdate -expand -group BEEBUS -format Literal -radix hexadecimal /top/beebus_data
add wave -noupdate -expand -group BEEBUS -format Logic /top/beebus_read
add wave -noupdate -expand -group BEEBUS -format Logic /top/beebus_strb
add wave -noupdate -format Logic /top/powerup_reset
add wave -noupdate -format Literal -radix hexadecimal /top/fmat_data
add wave -noupdate -format Logic -radix hexadecimal /top/fmat_data_strb
add wave -noupdate -format Literal -radix hexadecimal /top/u_2/u_1/chan_data
add wave -noupdate -format Literal -radix hexadecimal /top/raw_adc_data
add wave -noupdate -format Literal -radix hexadecimal /top/chan_number
add wave -noupdate -format Literal -radix hexadecimal /top/adc_douta
add wave -noupdate -format Literal -radix hexadecimal /top/adc_doutb
add wave -noupdate -format Literal -radix hexadecimal /top/u_1/data_chan_a_rise
add wave -noupdate -format Literal -radix hexadecimal /top/u_1/data_chan_b_rise
add wave -noupdate -format Literal -radix hexadecimal /top/u_1/data_chan_a_fall
add wave -noupdate -format Literal -radix hexadecimal /top/u_1/data_chan_b_fall
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {10073729 ns} 0} {{Cursor 2} {59015 ns} 0} {{Cursor 3} {1972950 ns} 0} {{Cursor 4} {1079882 ns} 0}
configure wave -namecolwidth 281
configure wave -valuecolwidth 106
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {1007048 ns} {1007176 ns}
