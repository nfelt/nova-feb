
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library std;
use std.textio.all;


--entity declaration
entity stim_adc_data_in is
end stim_adc_data_in;

--architecture definition
architecture Behav of stim_adc_data_in is
--period of clock,bit for indicating end of file.
  signal clock, endoffile : bit     := '0';
--data read from the file.
  signal dataread         : unsigned(7 downto 0);
--data to be saved into the output file.
  signal datatosave       : unsigned(7 downto 0);
--line number of the file read or written.
  signal linenumber       : integer := 1;

begin


  clock <= not (clock) after 1 ns;      --clock with time period 2 ns


--read process
  reading :
  process
    file infile         : text open read_mode is "hdl/1.txt";  --declare input file
    variable inline     : line;         --line number declaration
    variable dataread1  : bit_vector(7 downto 0);
    variable CHAN_INDEX : integer;
  begin
    if (not endfile(infile)) then  --checking the "END OF FILE" is not reached.
      readline(infile, inline);         --reading a line from the file.
      --reading the data from the line and putting it in a real type variable.
      for CHAN_INDEX in 0 to 3 loop
        hread(inline, dataread1);
        dataread <= unsigned(to_stdlogicvector(dataread1));  --put the value available in variable in a signal.
        wait until clock = '1' and clock'event;
      end loop;  -- CHAN_INDEX
    else
      endoffile <= '1';  --set signal to tell end of file read file is reached.
      wait;
    end if;
  end process reading;

--write process
  writing :
  process
    file outfile     : text open write_mode is "hdl/2.txt";  --declare output file
    variable outline : line;            --line number declaration  
  begin
    wait until clock = '0' and clock'event;
    if(endoffile = '0') then            --if the file end is not reached.
--write(linenumber,value(real type),justified(side),field(width),digits(natural));
      hwrite(outline, to_bitvector(std_logic_vector(dataread)), right, 16);
-- write line to external file.
      writeline(outfile, outline);
      linenumber <= linenumber + 1;
    else
      null;
    end if;

  end process writing;

end Behav;
