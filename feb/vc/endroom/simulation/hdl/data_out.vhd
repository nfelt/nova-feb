
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library std;
use std.textio.all;


--entity declaration
entity stim_adc_data_in is
  port (
    USB_FD   : in std_logic_vector(15 downto 0);
    USB_SLWR : in boolean;
    USB_CLK  : in std_logic
    );
end stim_adc_data_in;

--architecture definition
architecture Behav of DATA_OUT is

begin

  writing : process

    file OUTFILE     : text;
    variable fstatus : file_open_status;
    variable outline : line;
    
  begin
    file_open(fstatus, OUTFILE, "hdl/data_out.txt", write_mode);
--    while true loop
    wait until rising_edge(USB_SLWR);
    while USB_SLWR loop
      hwrite(outline, to_bitvector(USB_FD), right, 16);
      writeline(outfile, outline);
      wait until rising_edge(USB_CLK);
    end loop;
--    end loop;

  end process writing;

end Behav;
