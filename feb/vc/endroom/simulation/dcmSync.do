####################
#select input data 
#vsim -gstim_file_name=simData/adcDataCh.txt nova_feb.top_with_dcm_emulator
#vsim -gstim_file_name=simData/adcDataInMax.txt nova_feb.top_with_dcm_emulator
#vsim -gstim_file_name=simData/adcDataInMC.txt nova_feb.top_with_dcm_emulator
vsim -gstim_file_name=simData/adcDataInc.txt nova_feb.top_with_dcm_emulator

do scripts/do/FebWithDcmEmuForce.do
do scripts/tcl/XmlFileIo.tcl
do scripts/wave/multipointSignals.do
do scripts/do/setThresholds.do
xml scripts/xml/DcmStartup.xml

####################
#set readout mode  
xml scripts/xml/oscopeMode.xml
#xml scripts/xml/dcsMode.xml

xml scripts/xml/configMPLegacy.xml

#do sync
force -freeze sim:/top_with_dcm_emulator/u_0/dcm_sync_in 1 2ns -cancel 31.25ns
run 625ns
force -freeze sim:/top_with_dcm_emulator/u_0/dcm_sync_in 1 2ns -cancel 31.25ns
run 625ns
run 31.25ns
force -freeze sim:/top_with_dcm_emulator/u_0/dcm_sync_in 1 2ns -cancel 31.25ns
run 625ns
force -freeze sim:/top_with_dcm_emulator/u_0/dcm_sync_in 1 2ns -cancel 31.25ns
run 625ns
run 31.25ns
force -freeze sim:/top_with_dcm_emulator/u_0/dcm_sync_in 1 2ns -cancel 31.25ns
run 625ns
run 31.25ns
force -freeze sim:/top_with_dcm_emulator/u_0/dcm_sync_in 1 2ns -cancel 31.25ns
run 625ns
run 31.25ns
force -freeze sim:/top_with_dcm_emulator/u_0/dcm_sync_in 1 2ns -cancel 31.25ns
run 625ns

xml scripts/xml/pulseSingle.xml
