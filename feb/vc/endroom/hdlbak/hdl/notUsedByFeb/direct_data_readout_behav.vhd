--
-- VHDL Architecture nova_feb.direct_data_readout.behav
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 11:19:39 08/05/10
--
-- using Mentor Graphics HDL Designer(TM) 2009.1 (Build 12)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;
LIBRARY unisim;
USE unisim.all;
library harvard_std;
USE harvard_std.lppc_custom_fn_pkg.all;

ENTITY direct_data_readout IS
-- Declarations

END direct_data_readout ;

--
ARCHITECTURE behav OF direct_data_readout IS
BEGIN
END ARCHITECTURE behav;

