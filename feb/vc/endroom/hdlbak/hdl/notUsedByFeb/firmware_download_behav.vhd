--
-- VHDL Architecture nova_feb.firmware_download.behav
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 11:12:45 07/16/10
--
-- using Mentor Graphics HDL Designer(TM) 2009.1 (Build 12)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity firmware_download is
  port(
    SPYBUS_ADDR         : in  unsigned (15 downto 0);
    SPYBUS_DATA         : in  unsigned (31 downto 0);
    SPYBUS_STRB         : in  boolean;
    USE_CONFIG_DATA     : in  boolean;
    SERIALIZE_WORD_DONE : out boolean;
    SERIAL_OUT          : out std_logic;
    CONFIG_CLK          : out std_logic;
    clk_32              : in  std_logic
    );

-- Declarations

end firmware_download;

--
architecture behav of firmware_download is

  component ODDR2
    generic (
      DDR_ALIGNMENT : string;
      INIT          : bit;
      SRTYPE        : string
      );
    port (
      Q  : out std_logic;
      C0 : in  std_logic;
      C1 : in  std_logic;
      CE : in  std_logic;
      D0 : in  std_logic;
      D1 : in  std_logic;
      R  : in  std_logic;
      S  : in  std_logic
      );
  end component;

  signal CONFIG_DATA_STRB_del : boolean                       := false;
  signal ENABLE_CLK           : std_logic                     := '0';
  signal CONFIG_DATA_STRB     : boolean                       := false;
  signal CONFIG_DATA          : std_logic_vector(15 downto 0) := x"0000";
  signal BIT_COUNT            : integer range 15 downto 0     := 0;
  signal SHIFT_BITS           : std_logic                     := '0';
  signal not_CLK_32           : std_logic                     := '0';
  
begin
  GET_PARALLEL : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '1' then
      CONFIG_DATA_STRB_del <= CONFIG_DATA_STRB;
      if CONFIG_DATA_STRB_del and CONFIG_DATA_STRB then
        CONFIG_DATA <= std_logic_vector(SPYBUS_DATA(15 downto 0));
        BIT_COUNT   <= 0;
        SHIFT_BITS  <= '1';
      elsif BIT_COUNT /= 15 then
        CONFIG_DATA <= CONFIG_DATA(14 downto 0) & '0';
        BIT_COUNT   <= BIT_COUNT+ 1;
        SHIFT_BITS  <= '1';
      else
        SHIFT_BITS <= '0';
      end if;
    end if;
  end process GET_PARALLEL;

  IFCLK_ODDR2 : ODDR2
    generic map(
      DDR_ALIGNMENT => "NONE",
      INIT          => '0',
      SRTYPE        => "SYNC") 
    port map (
      Q  => CONFIG_CLK,
      C0 => CLK_32,
      C1 => not_CLK_32,
      CE => '1',
      D0 => ENABLE_CLK,
      D1 => '0',
      R  => '0',
      S  => '0'
      );

  ENABLE_CLK          <= SHIFT_BITS or not BOOL2SL(USE_CONFIG_DATA);
  CONFIG_DATA_STRB    <= SPYBUS_STRB and (SPYBUS_ADDR = x"E006");
  not_CLK_32          <= not CLK_32;
  SERIAL_OUT          <= CONFIG_DATA(15);
  SERIALIZE_WORD_DONE <= BIT_COUNT = 13;
end architecture behav;


