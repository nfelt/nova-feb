--
-- VHDL Architecture nova_feb.dsp_filter.dummi
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 16:10:10 08/23/10
--
-- using Mentor Graphics HDL Designer(TM) 2009.1 (Build 12)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;
use nova_feb.register_address_map.all;

entity trigger is
  port(
    BEEBUS_READ : in    boolean                := false;
    BEEBUS_STRB : in    boolean                := false;
    BEEBUS_DATA : inout unsigned (15 downto 0) := x"0000";
    BEEBUS_ADDR : in    unsigned (15 downto 0) := x"0000";

    RAW_CHAN_NUMBER : in unsigned (4 downto 0)  := "00000";
    RAW_TIMESTAMP   : in unsigned (31 downto 0) := x"00000000";
    RAW_MAGNITUDE   : in unsigned (11 downto 0) := x"000";

    N_DATA_PKT_WORDS    : in unsigned(4 downto 0);
    DATA_MEM_FULL_LATCH : in boolean := false;

    DSP_CHAN_NUMBER : out integer range 31 downto 0 := 0;
    DSP_TIMESTAMP   : out unsigned (31 downto 0)    := x"00000000";
    DSP_MAGNITUDE   : out unsigned (11 downto 0)    := x"000";
    DSP_TRIGGER     : out boolean                   := false;

    SEND_TM : out boolean := false;

    INTERNAL_TRIGGER : in boolean := false;
    ENABLE_DAQ       : in boolean := false;

    RESET    : in boolean   := false;
    DCM_SYNC : in boolean   := false;
    CLK_16   : in std_logic;
    CLK_64   : in std_logic := '1';
    TICK_1US : in boolean   := false
    );

-- Declarations

end trigger;

--
architecture behav of trigger is

  type TABLE_TYPE is array (31 downto 0)
    of unsigned(11 downto 0);
  signal THRESHOLD_TABLE : TABLE_TYPE;

  type DATA_DELAY_TYPE is array (96 downto 0)
    of unsigned(11 downto 0);
  signal DATA_DELAY : DATA_DELAY_TYPE;

  type CHAN_HOLDOFF_TYPE is array (31 downto 0)
    of unsigned(3 downto 0);
  signal CHAN_HOLDOFF : CHAN_HOLDOFF_TYPE := ((others => (others => '0')));


  signal DAQ_MODE           : unsigned(15 downto 0)          := x"0000";
  signal BEEBUS_READ_del    : boolean                        := false;
  signal BEEBUS_DATA_int    : unsigned(15 downto 0)          := x"0000";
  signal CHAN_NUMBER        : integer range 31 downto 0      := 0;
  signal CHAN_DATA_DCS      : signed (12 downto 0)           := "0000000000000";
  signal TIMESTAMP          : unsigned(31 downto 0)          := x"00000000";
  signal CHAN_EN            : std_logic_vector (31 downto 0) := x"FFFFFFFF";
  signal TIME_MARKER_RATE   : unsigned (7 downto 0)          := x"00";
  signal TIME_MARKER_CNT    : unsigned (7 downto 0)          := x"00";
  signal DATA_REGULATOR_REG : unsigned (18 downto 0)         := "000" & x"0000";
  signal DATA_REGULATOR_CNT : unsigned (18 downto 0)         := "000" & x"0000";
  signal OSCOPE_EN          : boolean                        := false;
  signal OSCOPE_STRB        : boolean                        := false;
  signal DSP_STRB           : boolean                        := false;
  signal TRIG_HOLDOFF_TIME  : unsigned (3 downto 0)          := "0000";

begin
  BEEBUS_DATA <= BEEBUS_DATA_int when BEEBUS_READ_del else "ZZZZZZZZZZZZZZZZ";

  process (CLK_16)
  begin
    if CLK_16'event and CLK_16 = '1' then
      BEEBUS_READ_del <= false;
      if BEEBUS_ADDR = DAQ_MODE_ADDR then
        if BEEBUS_READ then
          BEEBUS_DATA_int <= DAQ_MODE;
          BEEBUS_READ_del <= true;
        elsif BEEBUS_STRB then
          DAQ_MODE <= BEEBUS_DATA;
        end if;
      end if;

      if BEEBUS_ADDR(15 downto 5) = THRESHOLD_ADDR then
        if BEEBUS_READ then
          BEEBUS_READ_del <= true;
          BEEBUS_DATA_int <= x"0" & THRESHOLD_TABLE (to_integer(BEEBUS_ADDR(5 downto 0)));
        elsif BEEBUS_STRB then
          THRESHOLD_TABLE (to_integer(BEEBUS_ADDR(5 downto 0))) <= (BEEBUS_DATA(11 downto 0));
        end if;
      end if;

      if BEEBUS_ADDR(15 downto 0) = DATA_REGULATOR_ADDR then
        if BEEBUS_READ then
          BEEBUS_READ_del <= true;
          BEEBUS_DATA_int <= DATA_REGULATOR_REG(18 downto 3);
        elsif BEEBUS_STRB then
          DATA_REGULATOR_REG <= BEEBUS_DATA & "000";
        end if;
      end if;

      if BEEBUS_ADDR(15 downto 0) = TRIG_HOLDOFF_TIME_ADDR then
        if BEEBUS_READ then
          BEEBUS_READ_del <= true;
          BEEBUS_DATA_int <= "000000000000" & unsigned(TRIG_HOLDOFF_TIME);
        elsif BEEBUS_STRB then
          TRIG_HOLDOFF_TIME <= BEEBUS_DATA(3 downto 0);
        end if;
      end if;

      if BEEBUS_ADDR(15 downto 0) = CHAN_EN_U_ADDR then
        if BEEBUS_READ then
          BEEBUS_READ_del <= true;
          BEEBUS_DATA_int <= unsigned(CHAN_EN(31 downto 16));
        elsif BEEBUS_STRB then
          CHAN_EN(31 downto 16) <= std_logic_vector(BEEBUS_DATA);
        end if;
      end if;

      if BEEBUS_ADDR(15 downto 0) = CHAN_EN_L_ADDR then
        if BEEBUS_READ then
          BEEBUS_READ_del <= true;
          BEEBUS_DATA_int <= unsigned(CHAN_EN(15 downto 0));
        elsif BEEBUS_STRB then
          CHAN_EN(15 downto 0) <= std_logic_vector(BEEBUS_DATA);
        end if;
      end if;

      if BEEBUS_ADDR(15 downto 0) = TIMING_PKT_RATE_ADDR then
        if BEEBUS_READ then
          BEEBUS_READ_del <= true;
          BEEBUS_DATA_int <= x"00" & TIME_MARKER_RATE;
        elsif BEEBUS_STRB then
          TIME_MARKER_RATE <= BEEBUS_DATA(7 downto 0);
        end if;
      end if;
      
    end if;
  end process;

  SEND_TIME_MARKER : process (CLK_64) is
  begin
    if CLK_64'event and CLK_64 = '1' then
      SEND_TM <= false;
      if DCM_SYNC then
        TIME_MARKER_CNT <= x"01";
      elsif TICK_1US then
        if (TIME_MARKER_CNT = TIME_MARKER_RATE)
          and (TIME_MARKER_RATE /= x"00") then
          TIME_MARKER_CNT <= x"01";
          SEND_TM         <= true;
        else
          TIME_MARKER_CNT <= TIME_MARKER_CNT + 1;
        end if;
      end if;
    end if;
  end process SEND_TIME_MARKER;

  process (CLK_64) is

  begin
    if CLK_64'event and CLK_64 = '1' then
      CHAN_NUMBER   <= to_integer(RAW_CHAN_NUMBER);
      TIMESTAMP     <= RAW_TIMESTAMP;  --DELAY TIMESTAMP TO SYNC WITH OTHER DATA
      DATA_DELAY    <= DATA_DELAY(95 downto 0) & RAW_MAGNITUDE;
      -- FIR 32 CHANNELS USING (1 0 0 -1) "DCS FILTER"
      CHAN_DATA_DCS <= signed('0' & RAW_MAGNITUDE) - signed('0' & DATA_DELAY(95));
    end if;
  end process;

  SEND_OSCOPE_DATA : process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '1' then
      OSCOPE_EN <= (INTERNAL_TRIGGER or OSCOPE_EN)
                   and (DAQ_MODE = OSCILLOSCOPE_MODE)
                   and not (DATA_REGULATOR_CNT = "000" & x"0001")
                   and not DATA_MEM_FULL_LATCH;
      
      if INTERNAL_TRIGGER then
        DATA_REGULATOR_CNT <= DATA_REGULATOR_REG;
      elsif DATA_REGULATOR_CNT /= "000" & x"0000" then
        DATA_REGULATOR_CNT <= DATA_REGULATOR_CNT - 1;
      end if;
      
    end if;
  end process SEND_OSCOPE_DATA;


  DSP_STRB <= (CHAN_DATA_DCS > signed('0' & THRESHOLD_TABLE(CHAN_NUMBER)))
              and (DAQ_MODE = DCS_DSP_MODE);
  
  OSCOPE_STRB <= OSCOPE_EN and not DATA_MEM_FULL_LATCH;

  trigger_qualify : process (CLK_64) is
  begin
    if CLK_64'event and CLK_64 = '1' then
      if CHAN_HOLDOFF(CHAN_NUMBER) = "0000" then
        if DSP_STRB then
          CHAN_HOLDOFF(CHAN_NUMBER) <= TRIG_HOLDOFF_TIME;
        elsif OSCOPE_STRB then
          CHAN_HOLDOFF(CHAN_NUMBER) <= (N_DATA_PKT_WORDS(3 downto 0) -1);
        end if;
      else
        CHAN_HOLDOFF(CHAN_NUMBER) <= CHAN_HOLDOFF(CHAN_NUMBER) - 1;
      end if;
    end if;
  end process trigger_qualify;

  -- LOOK AT TIMING AND MAYBE PUT THIS IN ABOVE PROCESS
  DSP_TRIGGER <= (DSP_STRB or OSCOPE_STRB)
                 and (CHAN_HOLDOFF(CHAN_NUMBER) = "000")
                 and (CHAN_EN(CHAN_NUMBER) = '1')
                 and ENABLE_DAQ;
  
  DSP_CHAN_NUMBER <= CHAN_NUMBER;
  DSP_TIMESTAMP   <= TIMESTAMP;
  DSP_MAGNITUDE   <= DATA_DELAY(32);
  --
end architecture behav;






