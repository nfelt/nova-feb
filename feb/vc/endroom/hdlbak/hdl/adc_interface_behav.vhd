--
-- VHDL Architecture feb_p2_lib.adc_interface.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 11:43:21 01/22/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;
use nova_feb.register_address_map.all;

entity adc_interface is
  port(
    BEEBUS_ADDR    : in    unsigned (15 downto 0);
    BEEBUS_DATA    : inout unsigned (15 downto 0);
    BEEBUS_READ    : in    boolean;
    BEEBUS_STRB    : in    boolean;
    CURRENT_TIME   : out   unsigned (31 downto 0);
    D_IN_A         : in    std_logic_vector (11 downto 0);
    D_IN_B         : in    std_logic_vector (11 downto 0);
    DATA           : out   unsigned (11 downto 0);
    CHAN_NUMBER    : out   unsigned (4 downto 0);
    RESET_CHAN_PTR : in    boolean;
    ENABLE_DAQ     : in    boolean;
    ENABLE_TIME    : in    boolean;
    DCM_SYNC       : in    boolean;
    TICK_1US       : out   boolean;
    CLK_16         : in    std_logic;
    CLK_32         : in    std_logic;
    CLK_64         : in    std_logic;
    USE_CLK16_INV  : out   boolean
    );

-- Declarations

end adc_interface;

--
architecture behav of adc_interface is
  signal fake_data            : unsigned (11 downto 0) := x"000";
  signal ADC_CHAN_PTR         : unsigned (1 downto 0)  := "00";
  signal ASIC_CHAN_PTR        : unsigned (2 downto 0)  := "000";
  signal ASIC_CHAN_PTR_INV    : unsigned (2 downto 0)  := "000";
  signal DATA_CHAN_A_RISE     : unsigned (11 downto 0) := x"000";
  signal DATA_CHAN_B_RISE     : unsigned (11 downto 0) := x"000";
  signal DATA_CHAN_A_FALL     : unsigned (11 downto 0) := x"000";
  signal DATA_CHAN_B_FALL     : unsigned (11 downto 0) := x"000";
  signal BEEBUS_DATA_int      : unsigned (15 downto 0) := x"0000";
  signal PRESET_TIME          : unsigned (29 downto 0) := "00" & x"0000000";
  signal CURRENT_TIME_int     : unsigned (30 downto 0) := "000" & x"0000000";
  signal BEEBUS_READ_del      : boolean                := false;
  signal CURRENT_TIME_int_del : std_logic              := '0';
  signal USE_CLK16_INV_int    : boolean                := false;
  signal SYNC                 : boolean                := false;
  signal SYNC_INV             : boolean                := false;
begin

  BEEBUS_DATA <= BEEBUS_DATA_int when BEEBUS_READ_del else "ZZZZZZZZZZZZZZZZ";

  SET_ADC_IN_REGS : process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '1' then
      BEEBUS_READ_del <= false;

      case BEEBUS_ADDR is
        when PRESET_TIME_L_ADDR =>
          if BEEBUS_STRB then
            PRESET_TIME(13 downto 0) <= BEEBUS_DATA(15 downto 2);
          end if;
          if BEEBUS_READ then
            BEEBUS_READ_del <= true;
            BEEBUS_DATA_int <= PRESET_TIME(13 downto 0) & "00";
          end if;
          
        when PRESET_TIME_U_ADDR =>
          if BEEBUS_STRB then
            PRESET_TIME(29 downto 14) <= BEEBUS_DATA;
          end if;
          if BEEBUS_READ then
            BEEBUS_READ_del <= true;
            BEEBUS_DATA_int <= PRESET_TIME(29 downto 14);
          end if;
          
        when others => null;
      end case;
    end if;
  end process SET_ADC_IN_REGS;

  CURRENT_TIME_CNT : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '1' then  -- rising clock edge
      if DCM_SYNC then
        CURRENT_TIME_int <= PRESET_TIME & '0';
      elsif ENABLE_TIME then
        CURRENT_TIME_int <= CURRENT_TIME_int + 1;
      end if;
    end if;
  end process;

  GEN_1US_TICK : process (CLK_64) is
  begin
    if CLK_64'event and CLK_64 = '1' then  -- rising clock edge
      CURRENT_TIME_int_del <= CURRENT_TIME_int(5);
    end if;
  end process GEN_1US_TICK;
  TICK_1US <= (CURRENT_TIME_int_del /= CURRENT_TIME_int(5));

  APD_DATA_IN_RISE : process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '1' then
      DATA_CHAN_A_RISE <= unsigned(D_IN_A);
      DATA_CHAN_B_RISE <= unsigned(D_IN_B);
    end if;
  end process;

  APD_DATA_IN_FALL : process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '0' then
      DATA_CHAN_A_FALL <= unsigned(D_IN_A);
      DATA_CHAN_B_FALL <= unsigned(D_IN_B);
    end if;
  end process;
  
  ASIC_CHCNT : process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '1' then
      if DCM_SYNC then
        USE_CLK16_INV_int <= false;
      elsif SYNC_INV then
        USE_CLK16_INV_int <= true;
      end if;
      --follow prt_inv 180 out of phase
      ASIC_CHAN_PTR <= ASIC_CHAN_PTR_INV;
    end if;
  end process ASIC_CHCNT;

  ASIC_CHCNT_INV : process (CLK_16, RESET_CHAN_PTR)
  begin
    if RESET_CHAN_PTR then
      ASIC_CHAN_PTR_INV <= "000";
    elsif CLK_16'event and CLK_16 = '0' then
      
      if DCM_SYNC and not USE_CLK16_INV_int then
        SYNC_INV <= true;
      else
        ASIC_CHAN_PTR_INV <= ASIC_CHAN_PTR_INV +1;
        SYNC_INV          <= false;
      end if;
      
    end if;
  end process ASIC_CHCNT_INV;

  USE_CLK16_INV <= USE_CLK16_INV_int;

  ADC_PIPELINE : process (CLK_64) is
  begin
    if CLK_64'event and CLK_64 = '1' then
      if RESET_CHAN_PTR then
        ADC_CHAN_PTR <= "00";
        fake_data    <= x"000";
      else
        fake_data    <= fake_data+1;
        ADC_CHAN_PTR <= ADC_CHAN_PTR+1;
      end if;
      --CHAN DATA TIME  MAY NOT NEED THIS PIPELINE STAGE
      case ADC_CHAN_PTR is
        when "00" =>
          DATA <= DATA_CHAN_A_FALL;
        when "01" =>
          DATA <= DATA_CHAN_B_FALL;
        when "10" =>
          DATA <= DATA_CHAN_A_RISE;
        when "11" =>
          DATA <= DATA_CHAN_B_RISE;
        when others => null;
      end case;
      -- untangle ASIC mux ADC operation.
      if USE_CLK16_INV_int then
        CHAN_NUMBER <= ADC_CHAN_PTR(0) & ADC_CHAN_PTR(1) & ASIC_CHAN_PTR_INV;
      else
        CHAN_NUMBER <= ADC_CHAN_PTR(0) & not ADC_CHAN_PTR(1) & ASIC_CHAN_PTR;
      end if;
      CURRENT_TIME <= CURRENT_TIME_int(30 downto 1) & "00";
    end if;
  end process;

end architecture behav;

