--
-- VHDL Architecture nova_feb.dsp_filter.dummi
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 16:10:10 08/23/10
--
-- using Mentor Graphics HDL Designer(TM) 2009.1 (Build 12)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;
use nova_feb.register_address_map.all;
use nova_feb.all;

entity multi_point_ctrl is
  port(
    BEEBUS_ADDR : in    unsigned (15 downto 0) := x"0000";
    BEEBUS_DATA : inout unsigned (15 downto 0) := x"0000";
    BEEBUS_READ : in    boolean                := false;
    BEEBUS_STRB : in    boolean                := false;

    DATA_MEM_FULL_LATCH : out boolean := false;

    DSP_CHAN_NUMBER : in integer range 31 downto 0 := 0;
    DSP_TIMESTAMP   : in unsigned (31 downto 0)    := x"00000000";
    DSP_MAGNITUDE   : in unsigned (11 downto 0)    := x"000";
    DSP_TRIGGER     : in boolean                   := false;

    SEND_TM : in boolean := false;

    EVENT_FIFO_DOUT  : out unsigned (7 downto 0) := x"00";
    EVENT_FIFO_EMPTY : out boolean               := false;
    EVENT_FIFO_RSTRB : in  boolean               := false;
    N_DATA_PKT_WORDS : out unsigned(4 downto 0);

    ENABLE_DAQ : in boolean   := false;
    RESET      : in boolean   := false;
    CLK_3_2    : in std_logic := '1';
    CLK_16     : in std_logic;
    CLK_32     : in std_logic := '1';
    CLK_64     : in std_logic := '1';
    CLK_128    : in std_logic := '1'
    );


-- Declarations

end multi_point_ctrl;

--
architecture behav of multi_point_ctrl is

--  component TRIGGER_FIFO
--    port (
--      rst    : in  std_logic;
--      wr_clk : in  std_logic;
--      rd_clk : in  std_logic;
--      din    : in  std_logic_vector(44 downto 0);
--      wr_en  : in  std_logic;
--      rd_en  : in  std_logic;
--      dout   : out std_logic_vector(44 downto 0);
--      full   : out std_logic;
--      empty  : out std_logic
--      );
--  end component;

--  component TRIG_DATA_MEM
--    port (
--      clka  : in  std_logic;
--      wea   : in  std_logic_vector(0 downto 0);
--      addra : in  std_logic_vector(10 downto 0);
--      dina  : in  std_logic_vector(11 downto 0);
--      clkb  : in  std_logic;
--      addrb : in  std_logic_vector(10 downto 0);
--      doutb : out std_logic_vector(11 downto 0)
--      );
--  end component;
--
--  component EVENT_FIFO
--    port (
--      rst    : in  std_logic;
--      wr_clk : in  std_logic;
--      rd_clk : in  std_logic;
--      din    : in  std_logic_vector(15 downto 0);
--      wr_en  : in  std_logic;
--      rd_en  : in  std_logic;
--      dout   : out std_logic_vector(7 downto 0);
--      full   : out std_logic;
--      empty  : out std_logic
--      );
--  end component;

  type BUILD_PACKET_STATE_TYPE is (
    WAIT_FOR_DATA,
    TM_SEND_HEAD,
    TM_SEND_TS_U,
    TM_SEND_TS_L,
    TDATA_SEND_HEAD,
    TDATA_SEND_TS_U,
    TDATA_SEND_TS_L,
    TDATA_SEND_DATA
    );
  signal BUILD_PACKET_STATE      : BUILD_PACKET_STATE_TYPE := WAIT_FOR_DATA;
  signal NEXT_BUILD_PACKET_STATE : BUILD_PACKET_STATE_TYPE := WAIT_FOR_DATA;

  type TDATA_MEM_WPTR_TYPE is array (31 downto 0)
    of unsigned(4 downto 0);
  signal TDATA_MEM_WPTR : TDATA_MEM_WPTR_TYPE := ((others => (others => '0')));

  type TDATA_MEM_LRPTR_TYPE is array (31 downto 0)
    of unsigned(4 downto 0);
  signal TDATA_MEM_LRPTR : TDATA_MEM_LRPTR_TYPE := ((others => (others => '1')));

  type TDATA_CNT_TYPE is array (31 downto 0)
    of unsigned(4 downto 0);
  signal TDATA_CNT : TDATA_CNT_TYPE := ((others => (others => '0')));

  type CHAN_HOLD_TYPE is array (31 downto 0)
    of boolean;
  signal CHAN_HOLD : CHAN_HOLD_TYPE := (others => false);

  signal BEEBUS_DATA_int : unsigned(15 downto 0) := x"0000";
  signal BEEBUS_READ_del : boolean               := false;

  signal DAQ_MODE : unsigned(15 downto 0) := x"0000";

  signal EVENT_CH_SEL         : unsigned (4 downto 0) := "00000";
  signal KEEP_TDATA           : boolean               := false;
  signal N_DATA_PKT_WORDS_int : unsigned (4 downto 0) := "00001";
  signal DSP_TRIGGER_MEM_OK   : boolean               := true;

  signal THEAD_FIFO_WSTRB : boolean   := false;
  signal THEAD_FIFO_RSTRB : boolean   := false;
  signal THEAD_FIFO_DIN   : std_logic_vector(43 downto 0);
  signal THEAD_FIFO_DOUT  : std_logic_vector(43 downto 0);
  signal THEAD_FIFO_FULL  : std_logic := '0';
  signal THEAD_FIFO_EMPTY : std_logic := '0';

  signal THEAD_FIFO_SEND_TM         : boolean               := false;
  signal THEAD_FIFO_SEND_DATA       : boolean               := false;
  signal THEAD_FIFO_DSP_TIMESTAMP   : unsigned(31 downto 0) := x"00000000";
  signal THEAD_FIFO_DSP_CHAN_NUMBER : unsigned(4 downto 0)  := "00000";
  signal THEAD_FIFO_TDATA_HEAD      : unsigned(4 downto 0)  := "00000";

  -- Change TRIGGER_TYPE to refelct actual.
  signal TRIGGER_TYPE            : unsigned(1 downto 0)          := "00";
  signal TDATA_MEM_RADDR         : unsigned(9 downto 0)          := x"00"&"00";
  signal TDATA_MEM_RDATA         : std_logic_vector(11 downto 0) := x"000";
  signal TDATA_MEM_WADDR         : unsigned(9 downto 0)          := x"00"&"00";
  signal TDATA_MEM_WDATA         : unsigned(11 downto 0)         := x"000";
  signal TDATA_MEM_RPTR          : unsigned(4 downto 0)          := "00000";
  signal TDATA_MEM_PTRDIFF       : unsigned(4 downto 0)          := "00000";
  signal TDATA_MEM_WSTRB         : std_logic_vector(0 downto 0)  := "0";
  signal TDATA_MEM_WPTR_del1     : unsigned(4 downto 0)          := "00000";
  signal TDATA_MEM_WPTR_del2     : unsigned(4 downto 0)          := "00000";
  signal TDATA_MEM_DATAOK        : boolean                       := false;
  signal TDATA_MEM_FULL          : boolean                       := false;
  signal DATA_MEM_FULL_latch_int : boolean                       := false;

  signal EVENT_FIFO_WSTRB_TAG      : boolean := false;
  signal EVENT_FIFO_WSTRB_DATA     : boolean := false;
  signal EVENT_FIFO_WSTRB_DATA_DEL : boolean := false;
  signal EVENT_FIFO_WSTRB          : boolean := false;
  signal EVENT_FIFO_DIN            : std_logic_vector(15 downto 0);
  signal EVENT_FIFO_FULL           : boolean := false;
  signal EVENT_FIFO_PROG_FULL      : boolean := false;

  signal EVENT_FIFO_FULL_int      : std_logic := '0';
  signal EVENT_FIFO_PROG_FULL_int : std_logic := '0';
  signal EVENT_FIFO_EMPTY_int     : std_logic := '0';
  signal EVENT_FIFO_DOUT_int      : std_logic_vector(7 downto 0);



begin
  BEEBUS_DATA <= BEEBUS_DATA_int when BEEBUS_READ_del else "ZZZZZZZZZZZZZZZZ";

  REGISTER_IN : process (CLK_16)
  begin
    if CLK_16'event and CLK_16 = '1' then
      BEEBUS_READ_del <= false;
      if BEEBUS_ADDR = DAQ_MODE_ADDR then
        if BEEBUS_READ then
          BEEBUS_DATA_int <= DAQ_MODE;
          BEEBUS_READ_del <= true;
        elsif BEEBUS_STRB then
          DAQ_MODE <= BEEBUS_DATA;
        end if;
      end if;

      if BEEBUS_ADDR = MP_NSAMPLES_ADDR then
        if BEEBUS_READ then
          BEEBUS_DATA_int <= "00000000000"& N_DATA_PKT_WORDS_int;
          BEEBUS_READ_del <= true;
        elsif BEEBUS_STRB then
          N_DATA_PKT_WORDS_int <= BEEBUS_DATA(4 downto 0);
        end if;
      end if;
      
    end if;
  end process;
  N_DATA_PKT_WORDS <= N_DATA_PKT_WORDS_int;

  DSP_TRIGGER_MEM_OK <= DSP_TRIGGER
                        and not CHAN_HOLD(DSP_CHAN_NUMBER);
  
  BUFFER_EVENT_DATA : process (CLK_64) is
  begin
    if CLK_64'event and CLK_64 = '1' then
      if DSP_TRIGGER_MEM_OK then
        TDATA_CNT(DSP_CHAN_NUMBER)      <= N_DATA_PKT_WORDS_int - 1;
        TDATA_MEM_WPTR(DSP_CHAN_NUMBER) <= TDATA_MEM_WPTR(DSP_CHAN_NUMBER)+1;
        TDATA_MEM_WSTRB                 <= "1";
      elsif TDATA_CNT(DSP_CHAN_NUMBER) /= "00000" then
        TDATA_CNT(DSP_CHAN_NUMBER)      <= TDATA_CNT(DSP_CHAN_NUMBER) - 1;
        TDATA_MEM_WPTR(DSP_CHAN_NUMBER) <= TDATA_MEM_WPTR(DSP_CHAN_NUMBER) + 1;
        TDATA_MEM_WSTRB                 <= "1";
      else
        TDATA_MEM_WSTRB <= "0";
      end if;
      --LATCh MEM FULL UNTIL DAQ DISABLED
      DATA_MEM_FULL_LATCH_int <= (DATA_MEM_FULL_LATCH_int or TDATA_MEM_FULL)
                                 and ENABLE_DAQ;
      --MAY NOT NEED THIS PIPELINE STAGE.
      CHAN_HOLD(DSP_CHAN_NUMBER) <= TDATA_MEM_FULL;
      TDATA_MEM_WADDR            <= to_UNSIGNED(DSP_CHAN_NUMBER, 5)
                                    & TDATA_MEM_WPTR(DSP_CHAN_NUMBER);
      TDATA_MEM_WDATA <= DSP_MAGNITUDE;

    end if;
  end process;
  
  DATA_MEM_FULL_LATCH <= DATA_MEM_FULL_LATCH_int;
  TDATA_MEM_FULL <= (TDATA_MEM_LRPTR(DSP_CHAN_NUMBER)
                     - TDATA_MEM_WPTR(DSP_CHAN_NUMBER))
                 <= N_DATA_PKT_WORDS_int;


  TRIG_HEAD_FIFO_inst : entity nova_feb.TRIGGER_FIFO
    port map (
      rst    => bool2sl(RESET),
      --    rst    => '1',
      wr_clk => CLK_64,
      rd_clk => CLK_128,
      din    => THEAD_FIFO_DIN,
      wr_en  => bool2sl(THEAD_FIFO_WSTRB),
      rd_en  => bool2sl(THEAD_FIFO_RSTRB),
      dout   => THEAD_FIFO_DOUT,
      full   => THEAD_FIFO_FULL,
      empty  => THEAD_FIFO_EMPTY
      );
  THEAD_FIFO_WSTRB <= (DSP_TRIGGER_MEM_OK or SEND_TM);
  THEAD_FIFO_DIN   <= bool2sl(SEND_TM)
                      & bool2sl(DSP_TRIGGER_MEM_OK)
                      & std_logic_vector(DSP_TIMESTAMP)
                      & std_logic_vector(to_unsigned(DSP_CHAN_NUMBER, 5))
                      & std_logic_vector(TDATA_MEM_WPTR(DSP_CHAN_NUMBER));
  THEAD_FIFO_SEND_TM         <= THEAD_FIFO_DOUT(43) = '1';
  THEAD_FIFO_SEND_DATA       <= THEAD_FIFO_DOUT(42) = '1';
  THEAD_FIFO_DSP_TIMESTAMP   <= unsigned(THEAD_FIFO_DOUT(41 downto 10));
  THEAD_FIFO_DSP_CHAN_NUMBER <= unsigned(THEAD_FIFO_DOUT(9 downto 5));
  THEAD_FIFO_TDATA_HEAD      <= unsigned(THEAD_FIFO_DOUT(4 downto 0));
  TRIG_DATA_MEM_inst : entity nova_feb.TRIG_DATA_MEM
    port map (
      clka  => CLK_64,
      wea   => TDATA_MEM_WSTRB,
      addra => std_logic_vector(TDATA_MEM_WADDR),
      dina  => std_logic_vector(TDATA_MEM_WDATA),
      clkb  => CLK_128,
      addrb => std_logic_vector(TDATA_MEM_RADDR),
      doutb => TDATA_MEM_RDATA
      );
  -- TDATA_MEM_RADDR <= THEAD_FIFO_DSP_CHAN_NUMBER & TDATA_MEM_RPTR;
  TDATA_MEM_RADDR <= THEAD_FIFO_DSP_CHAN_NUMBER & TDATA_MEM_RPTR;

  EVENT_FIFO_inst : entity nova_feb.EVENT_FIFO
    port map (
      rst       => BOOL2SL(RESET),
      wr_clk    => CLK_128,
      rd_clk    => CLK_3_2,
      din       => EVENT_FIFO_DIN,
      wr_en     => bool2sl(EVENT_FIFO_WSTRB),
      rd_en     => bool2sl(EVENT_FIFO_RSTRB),
      dout      => EVENT_FIFO_DOUT_int,
      full      => EVENT_FIFO_FULL_int,
      empty     => EVENT_FIFO_EMPTY_int,
      prog_full => EVENT_FIFO_PROG_FULL_int
      );
  EVENT_FIFO_FULL      <= EVENT_FIFO_FULL_int = '1';
  EVENT_FIFO_PROG_FULL <= EVENT_FIFO_PROG_FULL_int = '1';
  EVENT_FIFO_EMPTY     <= EVENT_FIFO_EMPTY_int = '1';
  EVENT_FIFO_DOUT      <= unsigned(EVENT_FIFO_DOUT_int);

  GEN_EVENT_CLK : process (CLK_128) is
  begin
    if rising_edge(CLK_128) then
      if RESET then
        BUILD_PACKET_STATE <= WAIT_FOR_DATA;
      else
        BUILD_PACKET_STATE <= NEXT_BUILD_PACKET_STATE;
      end if;
      -- to avoid colision during Write, mem contents are not valid for reading
      -- until after wptr_del2 is updated 
      TDATA_MEM_WPTR_del1       <= TDATA_MEM_WPTR(to_integer(THEAD_FIFO_DSP_CHAN_NUMBER));
      TDATA_MEM_WPTR_del2       <= TDATA_MEM_WPTR_del1;
      EVENT_FIFO_WSTRB_DATA_DEL <= EVENT_FIFO_WSTRB_DATA;
      -- mem dout is registered on clk_128
      case BUILD_PACKET_STATE is
        when TM_SEND_HEAD =>
          TDATA_MEM_RPTR <= THEAD_FIFO_TDATA_HEAD;
        when TDATA_SEND_HEAD =>
          TDATA_MEM_RPTR                                          <= THEAD_FIFO_TDATA_HEAD;
          --the location of this read ptr only can move forward
          -- use this to detect data collision
          TDATA_MEM_LRPTR(to_integer(THEAD_FIFO_DSP_CHAN_NUMBER)) <= THEAD_FIFO_TDATA_HEAD;
        when TDATA_SEND_DATA =>
          if TDATA_MEM_DATAOK then
            TDATA_MEM_RPTR <= TDATA_MEM_RPTR + 1;
          end if;
        when others =>
          null;
      end case;
    end if;
  end process;
  TDATA_MEM_DATAOK  <= TDATA_MEM_RPTR /= TDATA_MEM_WPTR_del2;
  TDATA_MEM_PTRDIFF <= TDATA_MEM_WPTR_del2 - TDATA_MEM_RPTR;
  EVENT_FIFO_WSTRB  <= EVENT_FIFO_WSTRB_TAG or EVENT_FIFO_WSTRB_DATA_del;
  
  GEN_EVENT_COMB : process(BUILD_PACKET_STATE,
                           THEAD_FIFO_DSP_TIMESTAMP,
                           TRIGGER_TYPE,
                           THEAD_FIFO_DSP_CHAN_NUMBER,
                           THEAD_FIFO_EMPTY,
                           TDATA_MEM_WPTR,
                           TDATA_MEM_RPTR,
                           TDATA_MEM_RDATA,
                           TDATA_MEM_DATAOK,
                           EVENT_FIFO_PROG_FULL
                           )
  begin  -- process GEN_EVENT_COMB
    EVENT_FIFO_DIN        <= "0000" & TDATA_MEM_RDATA;
    THEAD_FIFO_RSTRB      <= false;
    EVENT_FIFO_WSTRB_DATA <= false;
    EVENT_FIFO_WSTRB_TAG  <= false;
    case BUILD_PACKET_STATE is
      when WAIT_FOR_DATA =>
        if THEAD_FIFO_EMPTY = '0' and not EVENT_FIFO_PROG_FULL then
          if THEAD_FIFO_SEND_TM then
            NEXT_BUILD_PACKET_STATE <= TM_SEND_HEAD;
          elsif THEAD_FIFO_SEND_DATA then
            NEXT_BUILD_PACKET_STATE <= TDATA_SEND_HEAD;
          end if;
        else
          NEXT_BUILD_PACKET_STATE <= WAIT_FOR_DATA;
        end if;

        
      when TM_SEND_HEAD =>
        EVENT_FIFO_DIN          <= x"A000";
        EVENT_FIFO_WSTRB_TAG    <= true;
        NEXT_BUILD_PACKET_STATE <= TM_SEND_TS_U;
      when TM_SEND_TS_U =>
        EVENT_FIFO_DIN          <= std_logic_vector(THEAD_FIFO_DSP_TIMESTAMP(31 downto 16));
        EVENT_FIFO_WSTRB_TAG    <= true;
        NEXT_BUILD_PACKET_STATE <= TM_SEND_TS_L;
      when TM_SEND_TS_L =>
        EVENT_FIFO_DIN       <= std_logic_vector(THEAD_FIFO_DSP_TIMESTAMP(15 downto 0));
        EVENT_FIFO_WSTRB_TAG <= true;
        if THEAD_FIFO_SEND_DATA then
          NEXT_BUILD_PACKET_STATE <= TDATA_SEND_HEAD;
        else
          THEAD_FIFO_RSTRB        <= true;
          NEXT_BUILD_PACKET_STATE <= WAIT_FOR_DATA;
        end if;
        
      when TDATA_SEND_HEAD =>
        EVENT_FIFO_DIN <= "001000" & std_logic_vector(TRIGGER_TYPE)
                          & std_logic_vector(N_DATA_PKT_WORDS_int(2 downto 0))
                          & std_logic_vector(THEAD_FIFO_DSP_CHAN_NUMBER);
        EVENT_FIFO_WSTRB_TAG    <= true;
        NEXT_BUILD_PACKET_STATE <= TDATA_SEND_TS_U;
      when TDATA_SEND_TS_U =>
        EVENT_FIFO_DIN          <= std_logic_vector(THEAD_FIFO_DSP_TIMESTAMP(31 downto 16));
        EVENT_FIFO_WSTRB_TAG    <= true;
        NEXT_BUILD_PACKET_STATE <= TDATA_SEND_TS_L;
      when TDATA_SEND_TS_L =>
        EVENT_FIFO_DIN          <= std_logic_vector(THEAD_FIFO_DSP_TIMESTAMP(15 downto 0));
        EVENT_FIFO_WSTRB_TAG    <= true;
        NEXT_BUILD_PACKET_STATE <= TDATA_SEND_DATA;
      when TDATA_SEND_DATA =>
        if ((TDATA_MEM_RPTR + 1) = (THEAD_FIFO_TDATA_HEAD + N_DATA_PKT_WORDS_int))
          and TDATA_MEM_DATAOK then
          NEXT_BUILD_PACKET_STATE <= WAIT_FOR_DATA;
          EVENT_FIFO_WSTRB_DATA   <= true;
          THEAD_FIFO_RSTRB        <= true;
        else
          NEXT_BUILD_PACKET_STATE <= TDATA_SEND_DATA;
        end if;
        EVENT_FIFO_WSTRB_DATA <= TDATA_MEM_DATAOK;
      when others =>
        NEXT_BUILD_PACKET_STATE <= WAIT_FOR_DATA;
    end case;
  end process GEN_EVENT_COMB;

end architecture behav;


--INCLUDE SM OVERHEAD
--FIX SYNTAX
--CHANGE INTERFACING COMPONENTS
--SIMULATE


