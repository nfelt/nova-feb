-- VHDL Entity nova_feb.controller.symbol
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 11:18:50 07/08/11
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2010.2a (Build 7)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;
LIBRARY unisim;
USE unisim.all;
library harvard_std;
USE harvard_std.lppc_custom_fn_pkg.all;

ENTITY controller IS
   PORT( 
      APD_DATA_BUFFER_FULL : IN     boolean;
      ASIC_SHIFTOUT        : IN     std_logic;
      BEEBUS_ADDR          : IN     unsigned (15 DOWNTO 0);
      BEEBUS_CLK           : IN     std_logic;
      BEEBUS_READ          : IN     boolean;
      BEEBUS_STRB          : IN     boolean;
      CLK_16               : IN     std_logic;
      CLK_4                : IN     std_logic;
      CURRENT_TIME         : IN     unsigned (31 DOWNTO 0) := x"00000000";
      SPI_DOUT             : IN     std_logic;
      ASIC_CHIPRESET       : OUT    boolean;
      ASIC_IBIAS           : OUT    boolean;
      ASIC_SHIFTIN         : OUT    std_logic;
      ASIC_SRCK            : OUT    std_logic;
      ASIC_TESTINJECT      : OUT    boolean                := false;
      DCM_SYNC_EN          : OUT    boolean                := true;
      ENABLE_DAQ           : OUT    boolean;
      ENABLE_STATUS_PKT    : OUT    boolean                := false;
      ENABLE_TIME          : OUT    boolean                := false;
      INSTRUMENT_TRIGGER   : OUT    boolean                := false;
      INTERNAL_TRIGGER     : OUT    boolean                := false;
      RESET_DAQ            : OUT    boolean;
      SER_NUM_CS           : OUT    boolean;
      SPI_DIN              : OUT    std_logic;
      SPI_SCLK             : OUT    std_logic;
      TECC_ENABLE          : OUT    boolean                := false;
      TEC_ADC_CH1_B_CH2    : OUT    std_logic;
      TEC_ADC_CS           : OUT    boolean;
      TEC_DAC_LDAC         : OUT    boolean;
      TEC_DAC_SYNC         : OUT    boolean;
      TEMP_SENSOR_CS       : OUT    boolean;
      BEEBUS_DATA          : INOUT  unsigned (15 DOWNTO 0)
   );

-- Declarations

END controller ;

--
-- VHDL Architecture nova_feb.controller.struct
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 11:18:50 07/08/11
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2010.2a (Build 7)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;
LIBRARY unisim;
USE unisim.all;
library harvard_std;
USE harvard_std.lppc_custom_fn_pkg.all;

LIBRARY nova_feb;

ARCHITECTURE struct OF controller IS

   -- Architecture declarations

   -- Internal signal declarations
   SIGNAL ENABLE_DAQ_int  : boolean;
   SIGNAL NEW_S_CMD       : boolean;
   SIGNAL RAW_SPI_COMMAND : unsigned(18 DOWNTO 0);
   SIGNAL SET_ASIC        : boolean;
   SIGNAL S_CMD_BUSY      : boolean;
   SIGNAL UPDATE_TEMP_REG : boolean;


   -- Component Declarations
   COMPONENT ASIC_CTRL
   PORT (
      BEEBUS_ADDR    : IN     unsigned (15 DOWNTO 0);
      CLK            : IN     std_logic ;
      BEEBUS_DATA    : INOUT  unsigned (15 DOWNTO 0);
      BEEBUS_READ    : IN     boolean ;
      BEEBUS_STRB    : IN     boolean ;
      SET_ASIC       : IN     boolean ;
      ASIC_CHIPRESET : OUT    boolean ;
      ASIC_SHIFTIN   : OUT    std_logic ;
      ASIC_SHIFTOUT  : IN     std_logic ;
      ASIC_SRCK      : OUT    std_logic ;
      ASIC_IBIAS     : OUT    boolean ;
      ASIC_SET_ERROR : OUT    boolean 
   );
   END COMPONENT;
   COMPONENT asic_pulse_inject
   GENERIC (
      ext_pulse_width : unsigned(15 downto 0)
   );
   PORT (
      BEEBUS_ADDR        : IN     unsigned (15 DOWNTO 0);
      BEEBUS_DATA        : IN     unsigned (15 DOWNTO 0);
      BEEBUS_READ        : IN     boolean ;
      BEEBUS_STRB        : IN     boolean ;
      ENABLE_DAQ         : IN     boolean ;
      INTERNAL_TRIGGER   : OUT    boolean  := false;
      ASIC_TESTINJECT    : OUT    boolean  := false;
      INSTRUMENT_TRIGGER : OUT    boolean  := false;
      CLK                : IN     std_logic 
   );
   END COMPONENT;
   COMPONENT command_interp
   PORT (
      BEEBUS_ADDR          : IN     unsigned (15 DOWNTO 0);
      BEEBUS_DATA          : INOUT  unsigned (15 DOWNTO 0);
      BEEBUS_CLK           : IN     std_logic ;
      BEEBUS_READ          : IN     boolean ;
      BEEBUS_STRB          : IN     boolean ;
      CLK                  : IN     std_logic ;
      DCM_SYNC_EN          : OUT    boolean  := true;
      APD_DATA_BUFFER_FULL : IN     boolean ;
      ENABLE_DAQ           : OUT    boolean  := false;
      UPDATE_TEMP_REG      : OUT    boolean  := false;
      RESET_FEB            : OUT    boolean  := false;
      SET_ASIC             : OUT    boolean  := false;
      RAW_SPI_COMMAND      : OUT    unsigned (18 DOWNTO 0);
      S_CMD_BUSY           : IN     boolean ;
      NEW_S_CMD            : OUT    boolean  := false;
      ENABLE_TIME          : OUT    boolean  := false;
      ENABLE_STATUS_PKT    : OUT    boolean  := false;
      TECC_ENABLE          : OUT    boolean  := false
   );
   END COMPONENT;
   COMPONENT spi_interface
   PORT (
      BEEBUS_ADDR       : IN     unsigned (15 DOWNTO 0);
      CLK_16            : IN     std_logic ;
      BEEBUS_DATA       : INOUT  unsigned (15 DOWNTO 0);
      BEEBUS_READ       : IN     boolean ;
      BEEBUS_STRB       : IN     boolean ;
      UPDATE_TEMP_REG   : IN     boolean ;
      CURRENT_TIME      : IN     unsigned (31 DOWNTO 0);
      CLK_4             : IN     std_logic ;
      SPI_DIN           : OUT    std_logic ;
      SPI_DOUT          : IN     std_logic ;
      SPI_SCLK          : OUT    std_logic ;
      TEC_DAC_LDAC      : OUT    boolean ;
      TEC_DAC_SYNC      : OUT    boolean ;
      TEC_ADC_CS        : OUT    boolean ;
      TEC_ADC_CH1_B_CH2 : OUT    std_logic ;
      TEMP_SENSOR_CS    : OUT    boolean ;
      SER_NUM_CS        : OUT    boolean ;
      RAW_SPI_COMMAND   : IN     unsigned (18 DOWNTO 0);
      S_CMD_BUSY        : OUT    boolean ;
      NEW_S_CMD         : IN     boolean 
   );
   END COMPONENT;

   -- Optional embedded configurations
   -- pragma synthesis_off
   FOR ALL : ASIC_CTRL USE ENTITY nova_feb.ASIC_CTRL;
   FOR ALL : asic_pulse_inject USE ENTITY nova_feb.asic_pulse_inject;
   FOR ALL : command_interp USE ENTITY nova_feb.command_interp;
   FOR ALL : spi_interface USE ENTITY nova_feb.spi_interface;
   -- pragma synthesis_on


BEGIN
   -- Architecture concurrent statements
   -- HDL Embedded Text Block 1 eb1
   -- eb1 1                                        
   ENABLE_DAQ <= ENABLE_DAQ_int;
   


   -- Instance port mappings.
   U_1 : ASIC_CTRL
      PORT MAP (
         BEEBUS_ADDR    => BEEBUS_ADDR,
         CLK            => CLK_16,
         BEEBUS_DATA    => BEEBUS_DATA,
         BEEBUS_READ    => BEEBUS_READ,
         BEEBUS_STRB    => BEEBUS_STRB,
         SET_ASIC       => SET_ASIC,
         ASIC_CHIPRESET => ASIC_CHIPRESET,
         ASIC_SHIFTIN   => ASIC_SHIFTIN,
         ASIC_SHIFTOUT  => ASIC_SHIFTOUT,
         ASIC_SRCK      => ASIC_SRCK,
         ASIC_IBIAS     => ASIC_IBIAS,
         ASIC_SET_ERROR => OPEN
      );
   U_2 : asic_pulse_inject
      GENERIC MAP (
         ext_pulse_width => x"0800"
      )
      PORT MAP (
         BEEBUS_ADDR        => BEEBUS_ADDR,
         BEEBUS_DATA        => BEEBUS_DATA,
         BEEBUS_READ        => BEEBUS_READ,
         BEEBUS_STRB        => BEEBUS_STRB,
         ENABLE_DAQ         => ENABLE_DAQ_int,
         INTERNAL_TRIGGER   => INTERNAL_TRIGGER,
         ASIC_TESTINJECT    => ASIC_TESTINJECT,
         INSTRUMENT_TRIGGER => INSTRUMENT_TRIGGER,
         CLK                => CLK_16
      );
   U_0 : command_interp
      PORT MAP (
         BEEBUS_ADDR          => BEEBUS_ADDR,
         BEEBUS_DATA          => BEEBUS_DATA,
         BEEBUS_CLK           => BEEBUS_CLK,
         BEEBUS_READ          => BEEBUS_READ,
         BEEBUS_STRB          => BEEBUS_STRB,
         CLK                  => CLK_16,
         DCM_SYNC_EN          => DCM_SYNC_EN,
         APD_DATA_BUFFER_FULL => APD_DATA_BUFFER_FULL,
         ENABLE_DAQ           => ENABLE_DAQ_int,
         UPDATE_TEMP_REG      => UPDATE_TEMP_REG,
         RESET_FEB            => RESET_DAQ,
         SET_ASIC             => SET_ASIC,
         RAW_SPI_COMMAND      => RAW_SPI_COMMAND,
         S_CMD_BUSY           => S_CMD_BUSY,
         NEW_S_CMD            => NEW_S_CMD,
         ENABLE_TIME          => ENABLE_TIME,
         ENABLE_STATUS_PKT    => ENABLE_STATUS_PKT,
         TECC_ENABLE          => TECC_ENABLE
      );
   U_3 : spi_interface
      PORT MAP (
         BEEBUS_ADDR       => BEEBUS_ADDR,
         CLK_16            => BEEBUS_CLK,
         BEEBUS_DATA       => BEEBUS_DATA,
         BEEBUS_READ       => BEEBUS_READ,
         BEEBUS_STRB       => BEEBUS_STRB,
         UPDATE_TEMP_REG   => UPDATE_TEMP_REG,
         CURRENT_TIME      => CURRENT_TIME,
         CLK_4             => CLK_4,
         SPI_DIN           => SPI_DIN,
         SPI_DOUT          => SPI_DOUT,
         SPI_SCLK          => SPI_SCLK,
         TEC_DAC_LDAC      => TEC_DAC_LDAC,
         TEC_DAC_SYNC      => TEC_DAC_SYNC,
         TEC_ADC_CS        => TEC_ADC_CS,
         TEC_ADC_CH1_B_CH2 => TEC_ADC_CH1_B_CH2,
         TEMP_SENSOR_CS    => TEMP_SENSOR_CS,
         SER_NUM_CS        => SER_NUM_CS,
         RAW_SPI_COMMAND   => RAW_SPI_COMMAND,
         S_CMD_BUSY        => S_CMD_BUSY,
         NEW_S_CMD         => NEW_S_CMD
      );

END struct;
