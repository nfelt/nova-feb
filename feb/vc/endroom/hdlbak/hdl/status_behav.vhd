--
-- VHDL Architecture nova_feb.status.behav
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 10:35:55 08/17/10
--
-- using Mentor Graphics HDL Designer(TM) 2009.1 (Build 12)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library nova_feb;
use nova_feb.register_address_map.all;

entity status is
  port(
    ADC_ERROR             : in    boolean;
    APD_DATA_BUFFER_EMPTY : in    boolean;
    APD_DATA_BUFFER_FULL  : in    boolean;
    BEEBUS_ADDR           : in    unsigned (15 downto 0);
    BEEBUS_READ           : in    boolean;
    BEEBUS_STRB           : in    boolean;
    CLK_16                : in    std_logic;
    COMM_ERROR            : in    boolean;
    ENABLE_DAQ            : in    boolean;
    OVERFLOW_ERROR        : in    boolean;
    PACKET_ERROR          : in    boolean;
    POWERUP_RESET         : in    boolean;
    DCM_SYNC_EN           : in    boolean;
    ENABLE_STATUS_PKT     : in    boolean;
    ENABLE_TIME           : in    boolean;
    TECC_ENABLE           : in    boolean;
    BEEBUS_DATA           : inout unsigned (15 downto 0);
    TIMING_PKT_FEB_STATUS : out   std_logic_vector (7 downto 0)
    );

-- Declarations

end status;

--
architecture behav of status is

  signal ISERROR         : boolean                       := false;
  signal BEEBUS_READ_del : boolean                       := false;
  signal BEEBUS_DATA_int : unsigned(15 downto 0)         := x"0000";
  signal STATUS_REG      : std_logic_vector(15 downto 0) := x"0000";
  signal ERROR_FLAGS_REG : std_logic_vector(15 downto 0) := x"0000";
  
begin
  BEEBUS_DATA <= BEEBUS_DATA_int when BEEBUS_READ_del else "ZZZZZZZZZZZZZZZZ";
  GET_STATUS : process (CLK_16) is
    variable i : integer := 0;
  begin
    if CLK_16'event and CLK_16 = '1' then
      BEEBUS_READ_del <= false;
      ERROR_FLAGS_REG <= ERROR_FLAGS_REG or ("000000000000"
                                             & BOOL2SL(ADC_ERROR) & BOOL2SL(OVERFLOW_ERROR)
                                             & BOOL2SL(PACKET_ERROR) & BOOL2SL(COMM_ERROR));
      case BEEBUS_ADDR is
        when FIRMWARE_VERS_ADDR =>
          if BEEBUS_STRB then
            --MY_REGISTER <= std_logic_vector(BEEBUS_DATA);
          end if;
          if BEEBUS_READ then
            BEEBUS_DATA_int <= FIRMWARE_VERS;
            BEEBUS_READ_del <= true;
          end if;
        when STATUS_ADDR =>
          if BEEBUS_READ then
            BEEBUS_DATA_int <= unsigned(STATUS_REG);
            BEEBUS_READ_del <= true;
          end if;
        when ERROR_FLAG_ADDR =>
          if BEEBUS_READ then
            BEEBUS_DATA_int <= unsigned(ERROR_FLAGS_REG);
            ERROR_FLAGS_REG <= x"0000";
            BEEBUS_READ_del <= true;
          end if;
        when others => null;
      end case;

    end if;
  end process GET_STATUS;
  --change the following to or reduce with sim upgrade.
  --ISERROR    <= or(ERROR_FLAGS_REG) = '1';
  iserror <= (ERROR_FLAGS_REG(15)
              or ERROR_FLAGS_REG(14)
              or ERROR_FLAGS_REG(13)
              or ERROR_FLAGS_REG(12)
              or ERROR_FLAGS_REG(11)
              or ERROR_FLAGS_REG(10)
              or ERROR_FLAGS_REG(9)
              or ERROR_FLAGS_REG(8)
              or ERROR_FLAGS_REG(7)
              or ERROR_FLAGS_REG(6)
              or ERROR_FLAGS_REG(5)
              or ERROR_FLAGS_REG(4)
              or ERROR_FLAGS_REG(3)
              or ERROR_FLAGS_REG(2)
              or ERROR_FLAGS_REG(1)
              or ERROR_FLAGS_REG(0)) = '1';

  STATUS_REG <= bool2sl(ISERROR) & "00000000" & BOOL2SL(DCM_SYNC_EN) & BOOL2SL(ENABLE_STATUS_PKT)
                & BOOL2SL(ENABLE_TIME) & BOOL2SL(TECC_ENABLE)
                & BOOL2SL(APD_DATA_BUFFER_FULL) & BOOL2SL(APD_DATA_BUFFER_EMPTY)
                & BOOL2SL(ENABLE_DAQ);
  TIMING_PKT_FEB_STATUS <= STATUS_REG(3 downto 0) & ERROR_FLAGS_REG(3 downto 0);
end architecture behav;


