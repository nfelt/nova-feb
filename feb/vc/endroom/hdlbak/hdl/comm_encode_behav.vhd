--
-- VHDL Architecture feb_p2_lib.comm_encode.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 16:30:17 11/ 7/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;


entity comm_encode is
  port(
    CLK_16         : in  std_logic;
    CLK_1_6        : out std_logic;
    SERIAL_OUT     : out std_logic;
    DATA_IN        : in  std_logic_vector (7 downto 0);
    DATA_10par_out : out std_logic_vector (9 downto 0);
    DATA_FRAME     : out boolean;
    DATA_STRB      : in  boolean
    );

-- Declarations

end comm_encode;

--
architecture behav of comm_encode is
  signal ENCODED_DATA   : std_logic_vector(9 downto 0) := "0000000000";
  signal SHIFT_REG      : std_logic_vector(9 downto 0) := "0000000000";
  signal BIT_COUNT      : integer range 15 downto 0    := 0;
  signal DATA_FRAME_int : std_logic                    := '0';
  signal CLK_1_6_sig    : std_logic                    := '0';
  signal CLK_1_6_int    : std_logic                    := '0';
  signal DATA_FRAME_del : boolean                      := false;
  signal first_skipped  : boolean                      := false;

  component encode_8b10b
    port (
      din  : in  std_logic_vector(7 downto 0);
      kin  : in  std_logic;
      clk  : in  std_logic;
      dout : out std_logic_vector(9 downto 0));
  end component;

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;
  
begin
  
  DCM_COMM_ENCODE : encode_8b10b

    port map (
      din  => DATA_IN,
      kin  => '0',
      clk  => CLK_1_6_int,
      dout => ENCODED_DATA
      );

  par_to_ser : process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '1' then
      SHIFT_REG      <= SHIFT_REG(0) & SHIFT_REG(9 downto 1);
      DATA_FRAME     <= false;
      BIT_COUNT      <= BIT_COUNT +1;
      DATA_FRAME_int <= '0';

      if (BIT_COUNT = 4) then
        CLK_1_6_sig <= '1';
      end if;

      if BIT_COUNT >= 9 then
        SHIFT_REG      <= ENCODED_DATA;
        first_skipped  <= true;
        DATA_FRAME     <= first_skipped;
        BIT_COUNT      <= 0;
        DATA_10par_out <= SHIFT_REG;
        CLK_1_6_sig    <= '0';
      end if;
      
    end if;
  end process par_to_ser;
  SERIAL_OUT <= SHIFT_REG(0);

  COMM_CLK_IN : BUFG
    port map (
      O => CLK_1_6_int,
      I => CLK_1_6_sig
      );
  CLK_1_6 <= CLK_1_6_int;
  
end architecture behav;

