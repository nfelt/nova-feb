-- VHDL Entity nova_feb.top_with_dcm_emulator.symbol
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 15:02:10 07/05/11
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2010.2a (Build 7)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;
library simulation;
entity top_with_dcm_emulator is
  port(
    ADC_ERROR            : in    boolean;
    ASIC_SHIFTOUT        : in    std_logic;
    DCM_CLK_IN           : in    std_logic;
    EXT_TRIG_IN_N        : in    std_logic;
    EXT_TRIG_IN_P        : in    std_logic;
    SPI_DOUT             : in    std_logic;
    USB_FLAGB            : in    boolean;
    USB_FLAGC            : in    boolean;
    ASIC_OUTCLK_N        : out   std_logic;
    ASIC_OUTCLK_P        : out   std_logic;
    ASIC_SHAPERRST       : out   boolean;
    ASIC_SHIFTIN         : out   std_logic;
    ASIC_SRCK            : out   std_logic;
    ASIC_TESTINJECT      : out   boolean;
    INSTRUMENT_TRIGGER_N : out   std_logic;
    INSTRUMENT_TRIGGER_P : out   std_logic;
    LA                   : out   std_logic_vector (4 downto 0);
    SER_NUM_CS_B         : out   boolean;
    SPI_DIN              : out   std_logic;
    SPI_SCLK             : out   std_logic;
    TECC_ENABLE_B        : out   boolean;
    TEC_ADC_CH1_B_CH2    : out   std_logic;
    TEC_ADC_CS_B         : out   boolean;
    TEC_DAC_LDAC_B       : out   boolean;
    TEC_DAC_SYNC_B       : out   boolean;
    TEMP_SENSOR_CS_B     : out   boolean;
    USB_FIFOADR          : out   unsigned (1 downto 0);
    USB_IFCLK            : out   std_logic;
    USB_PKTEND           : out   boolean;
    USB_RESET_B          : out   boolean;
    USB_SLOE             : out   boolean;
    USB_SLRD             : out   boolean;
    USB_SLWR             : out   boolean;
    USB_FD               : inout unsigned (15 downto 0)
    );

-- Declarations

end top_with_dcm_emulator;

--
-- VHDL Architecture nova_feb.top_with_dcm_emulator.struct
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 15:02:10 07/05/11
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2010.2a (Build 7)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

library DcmEmulator;
library nova_feb;

architecture struct of top_with_dcm_emulator is

  -- Architecture declarations

  -- Internal signal declarations
  signal DCM_CLK_N      : std_logic;
  signal DCM_CLK_P      : std_logic;
  signal DCM_COMMAND_N  : std_logic;
  signal DCM_COMMAND_P  : std_logic;
  signal DCM_DATA_N     : std_logic;
  signal DCM_DATA_P     : std_logic;
  signal DCM_SYNC_N     : std_logic;
  signal DCM_SYNC_P     : std_logic;
  signal ADC_DOUTAN     : std_logic_vector (11 downto 0);
  signal ADC_DOUTAP     : std_logic_vector (11 downto 0);
  signal ADC_DOUTBN     : std_logic_vector (11 downto 0);
  signal ADC_DOUTBP     : std_logic_vector (11 downto 0);
  signal ADC_CLKINN     : std_logic;
  signal ADC_CLKINP     : std_logic;
  signal ASIC_CHIPRESET : boolean;
  signal USB_SLWR_int   : boolean;
  signal USB_IFCLK_int  : std_logic;


  -- Component Declarations


begin

  -- Instance port mappings.
  U_1 : entity DcmEmulator.top
    port map (
      DCM_CLK_IN    => DCM_CLK_IN,
      DCM_DATA_N    => DCM_DATA_N,
      DCM_DATA_P    => DCM_DATA_P,
      USB_FLAGB     => USB_FLAGB,
      USB_FLAGC     => USB_FLAGC,
      DCM_CLK_N     => DCM_CLK_N,
      DCM_CLK_P     => DCM_CLK_P,
      DCM_COMMAND_N => DCM_COMMAND_N,
      DCM_COMMAND_P => DCM_COMMAND_P,
      DCM_SYNC_N    => DCM_SYNC_N,
      DCM_SYNC_P    => DCM_SYNC_P,
      LA            => LA,
      USB_FIFOADR   => USB_FIFOADR,
      USB_IFCLK     => USB_IFCLK_int,
      USB_PKTEND    => USB_PKTEND,
      USB_RESET_B   => USB_RESET_B,
      USB_SLOE      => USB_SLOE,
      USB_SLRD      => USB_SLRD,
      USB_SLWR      => USB_SLWR_int,
      USB_FD        => USB_FD,
      POD_CLOSED    => true
      );
  USB_SLWR  <= USB_SLWR_int;
  USB_IFCLK <= USB_IFCLK_int;

  U_0 : entity nova_feb.top
    port map (
      ADC_DOUTAN           => ADC_DOUTAN,
      ADC_DOUTAP           => ADC_DOUTAP,
      ADC_DOUTBN           => ADC_DOUTBN,
      ADC_DOUTBP           => ADC_DOUTBP,
      ADC_ERROR            => ADC_ERROR,
      ASIC_SHIFTOUT        => ASIC_SHIFTOUT,
      DCM_CLK_N            => DCM_CLK_N,
      DCM_CLK_P            => DCM_CLK_P,
      DCM_COMMAND_N        => DCM_COMMAND_N,
      DCM_COMMAND_P        => DCM_COMMAND_P,
      DCM_SYNC_N           => DCM_SYNC_N,
      DCM_SYNC_P           => DCM_SYNC_P,
      EXT_TRIG_IN_N        => EXT_TRIG_IN_N,
      EXT_TRIG_IN_P        => EXT_TRIG_IN_P,
      SPI_DOUT             => SPI_DOUT,
      ADC_CLKINN           => ADC_CLKINN,
      ADC_CLKINP           => ADC_CLKINP,
      ASIC_CHIPRESET       => ASIC_CHIPRESET,
      ASIC_OUTCLK_N        => ASIC_OUTCLK_N,
      ASIC_OUTCLK_P        => ASIC_OUTCLK_P,
      ASIC_SHAPERRST       => ASIC_SHAPERRST,
      ASIC_SHIFTIN         => ASIC_SHIFTIN,
      ASIC_SRCK            => ASIC_SRCK,
      ASIC_TESTINJECT      => ASIC_TESTINJECT,
      DCM_DATA_N           => DCM_DATA_N,
      DCM_DATA_P           => DCM_DATA_P,
      INSTRUMENT_TRIGGER_N => INSTRUMENT_TRIGGER_N,
      INSTRUMENT_TRIGGER_P => INSTRUMENT_TRIGGER_P,
      LA                   => open,
      SER_NUM_CS_B         => SER_NUM_CS_B,
      SPI_DIN              => SPI_DIN,
      SPI_SCLK             => SPI_SCLK,
      TECC_ENABLE_B        => TECC_ENABLE_B,
      TEC_ADC_CH1_B_CH2    => TEC_ADC_CH1_B_CH2,
      TEC_ADC_CS_B         => TEC_ADC_CS_B,
      TEC_DAC_LDAC_B       => TEC_DAC_LDAC_B,
      TEC_DAC_SYNC_B       => TEC_DAC_SYNC_B,
      TEMP_SENSOR_CS_B     => TEMP_SENSOR_CS_B
      );

  adc_input_stimulus : entity simulation.stim_adc_data_in
    port map (
      ADC_DOUTAP     => ADC_DOUTAP,
      ADC_DOUTAN     => ADC_DOUTAN,
      ADC_DOUTBP     => ADC_DOUTBP,
      ADC_DOUTBN     => ADC_DOUTBN,
      ASIC_CHIPRESET => ASIC_CHIPRESET,
      ADC_CLK        => ADC_CLKINP
      );
  pkt_data_output : entity simulation.usb_data_out
    port map (
      USB_FD    => USB_FD,
      USB_SLWR  => USB_SLWR_int,
      USB_IFCLK => USB_IFCLK_int
      );
end struct;
