using System;

public class bytehandler
{

    public byte[] bytes=new byte[4];
    public int bytecount=0;

    public  bytehandler(uint input){
        uint temp;

        bytes[0] = 0;
        bytes[1] = 0;
        bytes[2] = 0;
        bytes[3] = 0;
 


        temp=input &  0x00ff;
        bytes[0] = (byte)temp;
        temp = input >> 8;
        temp = temp & 0x00ff;
        bytes[1] = (byte)temp;
        bytecount = 2;

        temp = input >> 16;
        temp = temp & 0x00ff;
        bytes[2] = (byte)temp;
        bytecount++;

        temp = input >> 24;
        temp = temp & 0x00ff;
        bytes[3] = (byte)temp;
        bytecount++;

        if (bytes[2] == 0 && bytes[3] == 0) bytecount -= 2;  //we only had a 16 bit uint

    }




}