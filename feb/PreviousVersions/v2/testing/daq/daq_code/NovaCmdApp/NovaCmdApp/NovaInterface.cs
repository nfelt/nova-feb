using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

using System.Text;

using System.Xml;
using System.IO;
using CyUSB;


public class NovaInterface
{


    USBDeviceList usbDevices;
    CyUSBDevice myDev;
    Byte[] inbuffer = new Byte[5000];
    Byte[] outbuffer = new Byte[5000];




   public  int outbufferlength = 0;
   public  int inbufferlength = 0;
    int usbcount = 0;


    public CyUSBDevice getDevice()
    {
        return myDev as CyUSBDevice;

    }


    

    public bool USBInit()
    {

    



        App_PnP_Callback evHandler = new App_PnP_Callback(PnP_Event_Handler);

        usbDevices = new USBDeviceList(CyConst.DEVICES_CYUSB, evHandler);
        usbcount= usbDevices.Count;
        // Get the first device having VendorID == 0x04B4 and ProductID == 0x8613  

        myDev = null;
        myDev = usbDevices[0] as CyUSBDevice;

        if (myDev == null) return false;

       


       setTimeouts(10);

        return true;
    }


    public bool setBulkInTimeout(uint msecs)
    {
        if (myDev==null||myDev.BulkInEndPt==null)return false;
        myDev.BulkInEndPt.TimeOut = msecs;
        return true;
    }

    public bool setBulkOutTimeout(uint msecs)
    {
        if (myDev==null)return false;
        myDev.BulkOutEndPt.TimeOut = msecs;
        return true;
        
    }

    public bool setTimeouts(uint msecs){
        return this.setBulkInTimeout(msecs)&&this.setBulkOutTimeout(msecs);
    }


    public bool reset()
    {

        return myDev.Reset();
    }

    public bool sendbuffer( ref byte[] buff,ref int length)
    {

        if (myDev == null) return false;
        return myDev.BulkOutEndPt.XferData(ref buff, ref length);
    }

    public bool getbuffer(ref byte[] buff,ref int length)
    {
        if (myDev == null) return false;
        return myDev.BulkInEndPt.XferData(ref buff, ref length);
    }

    public bool writehexstring(string tempstring)
    {
        string tstring = tempstring;
        tempstring=null;
        for (int i = 0; i < tstring.Length; i++)
        {
            if (tstring[i] == '\n' || tstring[i] == '\r') continue;
            tempstring += tstring[i];
        }

   
        if (myDev == null) return false;
        outbufferlength = 0;
        for (int i = 0; i < tempstring.Length / 2; i ++)
        {
            Buffer.SetByte(outbuffer, i, byte.Parse(tempstring.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber));
            
            outbufferlength ++;
        }


        flipbuffer(ref outbuffer, ref outbufferlength);

        return myDev.BulkOutEndPt.XferData(ref outbuffer, ref outbufferlength);
        
    }

    public void flipbuffer(ref byte[] buf, ref int length)
    {
        for (int i = 0; i < length; i = i + 2)
        {
            byte temp = Buffer.GetByte(buf, i);
            Buffer.SetByte(buf, i, Buffer.GetByte(buf, i + 1));
            Buffer.SetByte(buf, i + 1, temp);

        }


    }


    public bool readhexstring(ref string hexstring)
    {
        int len = 512;
     return readhexstring(ref hexstring, ref len);
    }


    public bool readhexstring(ref string hexstring, ref int inbufferlength)
    {
        if (myDev == null) return false;

    

        hexstring = null;
                    if (myDev.BulkInEndPt != null)
                    {

                        
                        if (myDev.BulkInEndPt.XferData(ref inbuffer, ref inbufferlength))
                        {



                            flipbuffer(ref inbuffer, ref inbufferlength);



                            for (int i = 0; i < inbufferlength; i++)
                            {
                                hexstring+=(String.Format("{0:x2}", Buffer.GetByte(inbuffer, i)));
                            }

                       
                            return true;
                        }
                     }

        return false;
    }






    public bool writemem( uint startloc,  int count,  byte[] buff)
    {
        if (myDev == null) return false;
       Buffer.SetByte(outbuffer,0,(byte) 00);
       Buffer.SetByte(outbuffer,1,(byte) 02);

       bytehandler bhand = new bytehandler(startloc);
       Buffer.SetByte(outbuffer, 2, bhand.bytes[1]);
       Buffer.SetByte(outbuffer, 3, bhand.bytes[0]);

       outbufferlength = 4;
      
       Buffer.BlockCopy(buff, 0, outbuffer, outbufferlength, count);
       outbufferlength += count;

        string outstring=null;
        for (int i = 0; i < outbufferlength; i++)
        {
            outstring += String.Format("{0:x2}", outbuffer[i])+" ";

        }
       // MessageBox.Show(outbufferlength+"\r\n"+outstring);

        flipbuffer(ref outbuffer, ref outbufferlength);


        return myDev.BulkOutEndPt.XferData(ref outbuffer, ref outbufferlength);

    }


    public bool sendreset()
    {
        if (myDev == null) return false;
        Buffer.SetByte(outbuffer, 0, 0x00);
        Buffer.SetByte(outbuffer, 1, 0x01);
        outbufferlength = 2;

        return myDev.BulkOutEndPt.XferData(ref outbuffer, ref outbufferlength);

    }

    public bool readmem(uint startloc,ref int count, ref byte[] buff)
    {
        if (myDev == null) return false;
        Buffer.SetByte(outbuffer, 0, (byte)00);
        Buffer.SetByte(outbuffer, 1, (byte)03);

        bytehandler bhand = new bytehandler(startloc);
        Buffer.SetByte(outbuffer, 2, bhand.bytes[1]);
        Buffer.SetByte(outbuffer, 3, bhand.bytes[0]);

        uint ccc = uint.Parse( count.ToString());

     
        
        bytehandler cnt = new bytehandler(ccc);
        Buffer.SetByte(outbuffer, 4, cnt.bytes[1]);
        Buffer.SetByte(outbuffer, 5, cnt.bytes[0]);
    
        outbufferlength = 6;

        flipbuffer(ref outbuffer, ref outbufferlength);

        if (!myDev.BulkOutEndPt.XferData(ref outbuffer, ref outbufferlength)) return false;
              
        count = buff.Length;
        if (!myDev.BulkInEndPt.XferData(ref buff, ref count)) return false;
       
        flipbuffer(ref buff, ref count);

        return true;

    }

    public bool writemem(uint startloc, string data)
    {
        string tstring = data;
        data = null;
        for (int i = 0; i < tstring.Length; i++)
        {
            if (tstring[i] == '\n' || tstring[i] == '\r') continue;
            data += tstring[i];
        }




        byte[] buff = new byte[2000];
        int buffsize=0;

        for(int i=0;i<data.Length/2;i++){
            Buffer.SetByte(buff,i,byte.Parse(data.Substring(i*2,2)));
            buffsize++;
        }

       return writemem(startloc, buffsize, buff);


    }


    public int getUSBcount()
    {
        return usbcount;
    }


    public void PnP_Event_Handler(IntPtr pnpEvent, IntPtr hRemovedDevice)
    {
 

        if (pnpEvent.Equals(CyConst.DBT_DEVICEREMOVECOMPLETE))
        {


            usbDevices.Remove(hRemovedDevice);
            usbcount = usbDevices.Count;

            myDev = null;

            myDev = usbDevices[0] as CyUSBDevice;


          
        }



        if (pnpEvent.Equals(CyConst.DBT_DEVICEARRIVAL))
        {


            usbDevices.Add();
            usbcount = usbDevices.Count;

            myDev = null;
            myDev = usbDevices[0] as CyUSBDevice;


            
        }


    }   
        

}


