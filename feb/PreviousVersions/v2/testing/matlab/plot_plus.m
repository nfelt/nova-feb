
clear all;


figure(1);

DATAin = dlmread(['C:\data\projects\MINOS\nova\data\nova_readout.dat'],',');
for i = 0:19;
DEMUX(:,i+1)= DATAin(16+768*i:2:750+768*i);
DEMUXa(:,i+1)= DATAin(17+768*i:2:750+768*i);
end

DAVG = mean(DEMUX,2);
DAVGa = mean(DEMUXa,2);

figure(1);
subplot(2,1,1);
plot(DAVGa);
title(['STD = ', num2str(mean(std(DEMUXa)))])

subplot(2,1,2)
plot(DAVG);
%title(['DATAin']);
%xlabel('X');
%ylabel('Y');
%hbin = min(DEMUX(:,:)):1:max(DEMUX(:,:));
%hist(DEMUX(:,:),hbin);
title(['STD = ', num2str(mean(std(DEMUX)))])