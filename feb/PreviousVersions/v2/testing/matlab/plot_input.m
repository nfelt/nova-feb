
% Do it all debugger program
% plots the avg data, raw data, hist, std, and fft,

sets = 40 % number of data sets in file
npt = 102300% pad with zeros for FFT


%r = dlmread('C:\data\projects\nova\data\nova_readout.dat',',')
        r = dlmread(['C:\data\projects\nova\data\p2_tests\first.dat'],','); 
figure (2)
rzm = r - mean(r);
hbin = min(r):1:max(r);
hist(r,hbin);
title(['Hist. STD = ', num2str(std(r))])
