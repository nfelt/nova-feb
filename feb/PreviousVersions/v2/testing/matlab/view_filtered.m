clear all;

MODEL = get_sample(5,1);
%make filter
ws1 = .005;
wp1 = .1;
wp2 = .7;
ws2 = .8;
d = .1;
ap = -20*log10(1 - d);
as = -20*log10(d);
Wp = [wp1 wp2];
Ws = [ws1 ws2];
[n,Wn] = buttord(Wp,Ws,ap,as);
[b,a] = butter(n,Wn,'bandpass')


[n,Wn] = buttord(wp1,ws1,ap,as);
[b,a] = butter(n,Wn,'low')


MODELf = filter(b,a,MODEL);

figure(2);
subplot(2,1,1)
plot(MODEL);
subplot(2,1,2)
plot(MODELf);

w = 0:2*pi/256:2*pi-(pi/256);
G= freqz(b,a,w);
figure(3);
plot(w/pi,abs(G));grid;
legend(['Btterworth filter order ',num2str(n)]);
title('Magnitude Spectrum |H(e^{j\omega})|');
xlabel('\omega /\pi');
ylabel('Amplitude');
