clear all;
figure(1);
DATAin = dlmread(['\\Heplpc5\data\daq\cmds\outfile.txt']);
data1 = [1 16]
chans = (reshape(data1,8,[]))';    

 % chans(:,1:8) = chansb(:,[14:16 1:13]);  
    
plot(chans);
title(['DATAin']);
xlabel('X');
ylabel('Y');


figure(2);
for i = 0:7

subplot(4,4,i+1)
title(['DATAin']);
xlabel('X');
ylabel('Y');
hbin = min(chans(:,i+1)):1:max(chans(:,i+1));
hist(chans(:,i+1),hbin);
title(['CH',num2str(i),' STD = ', num2str(mean(std(chans(:,i+1))))])

end;



for i = 1:16
    for j = 1:16
        coa(i,j) = corr2(chans(:,i),chans(:,j));
    end
end
figure(3)
image(50 * coa)
meancorr = mean(mean(coa))
title(['mean corr = ',num2str(meancorr)])
colormap(hot)
f = findobj('Type','surface');
shading interp



