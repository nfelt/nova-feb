/*
 * MATLAB Compiler: 4.8 (R2008a)
 * Date: Thu Sep  4 18:06:27 2008
 * Arguments: "-B" "macro_default" "-m" "-W" "main" "-T" "link:exe" "-v" "-R"
 * "-nojvm" "Untitled" 
 */

#include "mclmcrrt.h"

#ifdef __cplusplus
extern "C" {
#endif
const unsigned char __MCC_Untitled_session_key[] = {
    '6', '8', '9', '4', '9', '8', '6', 'D', '0', '5', '0', 'E', '4', '6', '9',
    '7', '0', '2', 'F', 'F', '2', 'B', 'E', 'E', '1', '7', '6', '1', '3', '2',
    'E', '1', '8', '8', '3', '6', '6', '2', 'E', '6', '2', 'C', '5', '4', '7',
    '7', '5', 'F', '9', 'C', '7', '2', '1', '3', 'F', '1', '5', '3', 'C', '9',
    '9', '2', 'E', '1', '9', '8', '2', '7', '2', 'E', '1', '6', '9', '4', '3',
    '1', '5', 'F', 'A', '2', 'B', 'D', 'C', '0', 'F', '4', '4', 'E', 'B', '2',
    'E', '9', 'B', '2', '4', 'C', '2', '1', '2', '2', 'F', '3', 'A', '7', '9',
    '4', '6', '3', 'C', '4', '4', '2', '4', 'E', 'E', 'A', '8', 'B', 'D', '3',
    'B', '5', '0', '6', '4', '3', '6', '4', '8', 'E', '9', '2', '4', '5', '4',
    '2', 'B', 'D', '5', '6', '7', '4', '9', '6', 'A', '8', '0', '2', 'E', 'D',
    'F', 'B', '5', 'D', '2', '2', 'A', '5', 'E', '1', '0', '5', 'E', '8', '4',
    'A', '3', 'F', 'C', '6', '5', '6', 'A', '4', '0', '4', 'A', 'D', '3', '5',
    '0', '2', 'F', 'F', 'B', '9', 'B', 'A', 'B', 'A', '5', '0', '3', 'D', '7',
    '9', 'C', '8', '3', 'C', 'C', '5', '4', 'A', '3', '1', '2', '8', 'B', '9',
    'D', '1', 'F', 'A', '9', 'F', 'C', 'F', '4', '6', '9', '8', 'D', 'C', '1',
    'F', 'B', 'C', '5', '6', '7', '4', '5', '6', '7', '3', 'A', '8', '0', '7',
    '1', '8', '3', '9', '1', 'B', '2', '7', '8', '5', '4', '2', 'C', '5', 'F',
    '4', '\0'};

const unsigned char __MCC_Untitled_public_key[] = {
    '3', '0', '8', '1', '9', 'D', '3', '0', '0', 'D', '0', '6', '0', '9', '2',
    'A', '8', '6', '4', '8', '8', '6', 'F', '7', '0', 'D', '0', '1', '0', '1',
    '0', '1', '0', '5', '0', '0', '0', '3', '8', '1', '8', 'B', '0', '0', '3',
    '0', '8', '1', '8', '7', '0', '2', '8', '1', '8', '1', '0', '0', 'C', '4',
    '9', 'C', 'A', 'C', '3', '4', 'E', 'D', '1', '3', 'A', '5', '2', '0', '6',
    '5', '8', 'F', '6', 'F', '8', 'E', '0', '1', '3', '8', 'C', '4', '3', '1',
    '5', 'B', '4', '3', '1', '5', '2', '7', '7', 'E', 'D', '3', 'F', '7', 'D',
    'A', 'E', '5', '3', '0', '9', '9', 'D', 'B', '0', '8', 'E', 'E', '5', '8',
    '9', 'F', '8', '0', '4', 'D', '4', 'B', '9', '8', '1', '3', '2', '6', 'A',
    '5', '2', 'C', 'C', 'E', '4', '3', '8', '2', 'E', '9', 'F', '2', 'B', '4',
    'D', '0', '8', '5', 'E', 'B', '9', '5', '0', 'C', '7', 'A', 'B', '1', '2',
    'E', 'D', 'E', '2', 'D', '4', '1', '2', '9', '7', '8', '2', '0', 'E', '6',
    '3', '7', '7', 'A', '5', 'F', 'E', 'B', '5', '6', '8', '9', 'D', '4', 'E',
    '6', '0', '3', '2', 'F', '6', '0', 'C', '4', '3', '0', '7', '4', 'A', '0',
    '4', 'C', '2', '6', 'A', 'B', '7', '2', 'F', '5', '4', 'B', '5', '1', 'B',
    'B', '4', '6', '0', '5', '7', '8', '7', '8', '5', 'B', '1', '9', '9', '0',
    '1', '4', '3', '1', '4', 'A', '6', '5', 'F', '0', '9', '0', 'B', '6', '1',
    'F', 'C', '2', '0', '1', '6', '9', '4', '5', '3', 'B', '5', '8', 'F', 'C',
    '8', 'B', 'A', '4', '3', 'E', '6', '7', '7', '6', 'E', 'B', '7', 'E', 'C',
    'D', '3', '1', '7', '8', 'B', '5', '6', 'A', 'B', '0', 'F', 'A', '0', '6',
    'D', 'D', '6', '4', '9', '6', '7', 'C', 'B', '1', '4', '9', 'E', '5', '0',
    '2', '0', '1', '1', '1', '\0'};

static const char * MCC_Untitled_matlabpath_data[] = 
  { "Untitled/", "toolbox/compiler/deploy/", "$TOOLBOXMATLABDIR/general/",
    "$TOOLBOXMATLABDIR/ops/", "$TOOLBOXMATLABDIR/lang/",
    "$TOOLBOXMATLABDIR/elmat/", "$TOOLBOXMATLABDIR/elfun/",
    "$TOOLBOXMATLABDIR/specfun/", "$TOOLBOXMATLABDIR/matfun/",
    "$TOOLBOXMATLABDIR/datafun/", "$TOOLBOXMATLABDIR/polyfun/",
    "$TOOLBOXMATLABDIR/funfun/", "$TOOLBOXMATLABDIR/sparfun/",
    "$TOOLBOXMATLABDIR/scribe/", "$TOOLBOXMATLABDIR/graph2d/",
    "$TOOLBOXMATLABDIR/graph3d/", "$TOOLBOXMATLABDIR/specgraph/",
    "$TOOLBOXMATLABDIR/graphics/", "$TOOLBOXMATLABDIR/uitools/",
    "$TOOLBOXMATLABDIR/strfun/", "$TOOLBOXMATLABDIR/imagesci/",
    "$TOOLBOXMATLABDIR/iofun/", "$TOOLBOXMATLABDIR/audiovideo/",
    "$TOOLBOXMATLABDIR/timefun/", "$TOOLBOXMATLABDIR/datatypes/",
    "$TOOLBOXMATLABDIR/verctrl/", "$TOOLBOXMATLABDIR/codetools/",
    "$TOOLBOXMATLABDIR/helptools/", "$TOOLBOXMATLABDIR/demos/",
    "$TOOLBOXMATLABDIR/timeseries/", "$TOOLBOXMATLABDIR/hds/",
    "$TOOLBOXMATLABDIR/guide/", "$TOOLBOXMATLABDIR/plottools/",
    "toolbox/local/", "toolbox/shared/dastudio/",
    "$TOOLBOXMATLABDIR/datamanager/", "toolbox/compiler/",
    "toolbox/images/images/", "toolbox/images/iptutils/",
    "toolbox/shared/imageslib/", "toolbox/images/medformats/" };

static const char * MCC_Untitled_classpath_data[] = 
  { "java/jar/toolbox/images.jar" };

static const char * MCC_Untitled_libpath_data[] = 
  { "" };

static const char * MCC_Untitled_app_opts_data[] = 
  { "" };

static const char * MCC_Untitled_run_opts_data[] = 
  { "-nojvm" };

static const char * MCC_Untitled_warning_state_data[] = 
  { "off:MATLAB:dispatcher:nameConflict" };


mclComponentData __MCC_Untitled_component_data = { 

  /* Public key data */
  __MCC_Untitled_public_key,

  /* Component name */
  "Untitled",

  /* Component Root */
  "",

  /* Application key data */
  __MCC_Untitled_session_key,

  /* Component's MATLAB Path */
  MCC_Untitled_matlabpath_data,

  /* Number of directories in the MATLAB Path */
  41,

  /* Component's Java class path */
  MCC_Untitled_classpath_data,
  /* Number of directories in the Java class path */
  1,

  /* Component's load library path (for extra shared libraries) */
  MCC_Untitled_libpath_data,
  /* Number of directories in the load library path */
  0,

  /* MCR instance-specific runtime options */
  MCC_Untitled_app_opts_data,
  /* Number of MCR instance-specific runtime options */
  0,

  /* MCR global runtime options */
  MCC_Untitled_run_opts_data,
  /* Number of MCR global runtime options */
  1,
  
  /* Component preferences directory */
  "Untitled_90B80D9290CCCF44C16D06443D2E04FF",

  /* MCR warning status data */
  MCC_Untitled_warning_state_data,
  /* Number of MCR warning status modifiers */
  1,

  /* Path to component - evaluated at runtime */
  NULL

};

#ifdef __cplusplus
}
#endif


