charge = [.1:.1:1] * 25 * 2;
output = [.226 .440 .711 .980 1.18 1.39 1.60 1.87 2.05 2.30];
VpfC= mean(output ./ charge);

figure(1)
plot(charge,output,'x')
title([num2str(VpfC*1000),' mv per fC as measured at ASIC output', ]);
xlabel('fC');
ylabel('V');

charge = [.1:.1:.7] * 25 * 2;
output = 3825 - [3320 2790 2320 1660 1350 800 310];
ADCpfC= mean(output ./ charge);

figure(2)
plot(charge,output,'x')
title([num2str(ADCpfC),' ADC counts per fC as measured from USB readout', ]);
xlabel('fC');
ylabel('ADC');