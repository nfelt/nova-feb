add_input_file {"C:/data/projects/standard_libs/harvard_std/hdl/lppc_custom_fn_pkg.vhd"} -format {VHDL} -work harvard_std
add_input_file {"C:/data/projects/standard_libs/harvard_std/hdl/lppc_custom_fn_pkg_body.vhd"} -format {VHDL} -work harvard_std
add_input_file {"C:/data/projects/nova/firmware/feb_p2/feb_p2_lib/hdl/command_interp_behav.vhd"} -format {VHDL} -work feb_p2_lib
add_input_file {"C:/data/projects/nova/firmware/feb_p2/feb_p2_lib/hdl/ASIC_CTRL_BEHAV.vhd"} -format {VHDL} -work feb_p2_lib
add_input_file {"C:/data/projects/nova/firmware/feb_p2/feb_p2_lib/hdl/asic_pulse_inject_behav.vhd"} -format {VHDL} -work feb_p2_lib
add_input_file {"C:/data/projects/nova/firmware/feb_p2/feb_p2_lib/hdl/clock_control_struct.vhd"} -format {VHDL} -work feb_p2_lib
add_input_file {"C:/data/projects/nova/firmware/feb_p2/feb_p2_lib/hdl/controller_struct.vhd"} -format {VHDL} -work feb_p2_lib
setup_design -design controller
setup_design -architecture struct
