###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name CLKDV_DIVIDE -value "2.0" -instance -type default U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKFX_DIVIDE -value "1" -instance -type integer U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKFX_MULTIPLY -value "2" -instance -type integer U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKIN_DIVIDE_BY_2 -value "0" -instance -type default U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKIN_PERIOD -value "62.5" -instance -type default U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKOUT_PHASE_SHIFT -value "NONE" -instance -type string U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLK_FEEDBACK -value "NONE" -instance -type string U_3/CLK_IN_DCM -design rtl 
set_attribute -name DESKEW_ADJUST -value "SYSTEM_SYNCHRONOUS" -instance -type string U_3/CLK_IN_DCM -design rtl 
set_attribute -name DFS_FREQUENCY_MODE -value "LOW" -instance -type string U_3/CLK_IN_DCM -design rtl 
set_attribute -name DLL_FREQUENCY_MODE -value "LOW" -instance -type string U_3/CLK_IN_DCM -design rtl 
set_attribute -name DUTY_CYCLE_CORRECTION -value "1" -instance -type default U_3/CLK_IN_DCM -design rtl 
set_attribute -name FACTORY_JF -value "c080" -instance -type default U_3/CLK_IN_DCM -design rtl 
set_attribute -name PHASE_SHIFT -value "0" -instance -type integer U_3/CLK_IN_DCM -design rtl 
set_attribute -name STARTUP_WAIT -value "1" -instance -type default U_3/CLK_IN_DCM -design rtl 

##################
# Clocks
##################
create_clock { U_3/ASIC_BGCE/O } -domain ClockDomain0 -name U_3/ASIC_BGCE/O -period 31.200000 -waveform { 0.000000 15.600000 } -design rtl 
create_clock { CLK_IN } -domain ClockDomain0 -name CLK_IN -period 31.200000 -waveform { 0.000000 15.600000 } -design rtl 

