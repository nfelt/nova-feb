--
-- VHDL Architecture feb_p2_lib.adc_interface.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 11:43:21 01/22/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

ENTITY adc_interface IS
   PORT( 
      D_IN_A         : IN     std_logic_vector (11 DOWNTO 0);
      D_IN_B         : IN     std_logic_vector (11 DOWNTO 0);
      DATA           : OUT    unsigned (11 DOWNTO 0);
      CHAN_NUMBER    : OUT    unsigned (4 DOWNTO 0);
      RESET_CHAN_PTR : IN     boolean;
      ENABLE_DAQ     : IN     boolean;
      CLK_16         : IN     std_logic;
      CLK_64         : IN     std_logic
   );

-- Declarations

END adc_interface ;

--
architecture behav of adc_interface is
  signal CHAN_PTR           : unsigned (4 downto 0)  := "00000";
  signal DATA_CH_15_00_EVEN : unsigned (11 downto 0) := x"000";
  signal DATA_CH_31_16_EVEN : unsigned (11 downto 0) := x"000";
  signal DATA_CH_15_00_ODD  : unsigned (11 downto 0) := x"000";
  signal DATA_CH_31_16_ODD  : unsigned (11 downto 0) := x"000";

begin

  process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '1' then
      DATA_CH_15_00_ODD <= unsigned(not D_IN_A);
      DATA_CH_31_16_ODD <= unsigned(not D_IN_B);
    end if;
  end process;

  process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '0' then
      DATA_CH_15_00_EVEN <= unsigned(not D_IN_A);
      DATA_CH_31_16_EVEN <= unsigned(not D_IN_B);
    end if;
  end process;

  process (CLK_64) is
  begin
    if CLK_64'event and CLK_64 = '0' then
      if RESET_CHAN_PTR then
        CHAN_PTR <= "00000";
      elsif ENABLE_DAQ then
        CHAN_PTR <= CHAN_PTR+1;
      end if;
      case CHAN_PTR(1 downto 0) is
        when "00" =>
          DATA <= DATA_CH_15_00_ODD;
        when "01" =>
          DATA <= DATA_CH_15_00_EVEN;
        when "10" =>
          DATA <= DATA_CH_31_16_ODD;
        when "11" =>
          DATA <= DATA_CH_31_16_EVEN;
        when others => null;
      end case;
      -- untangle ASIC mux ADC operation.
      CHAN_NUMBER <= CHAN_PTR(2 downto 0) & CHAN_PTR(4 downto 3);
    end if;
  end process;

end architecture behav;

