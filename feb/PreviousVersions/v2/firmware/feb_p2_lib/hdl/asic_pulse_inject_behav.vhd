--
-- VHDL Architecture feb_p2_lib.asic_pulse_inject.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 16:50:32 02/28/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

ENTITY asic_pulse_inject IS
   GENERIC( 
      ext_pulse_width : unsigned(15 downto 0)
   );
   PORT( 
      BEEBUS_ADDR      : IN     unsigned (15 DOWNTO 0);
      BEEBUS_CLK       : IN     std_logic;
      BEEBUS_DATA      : INOUT  unsigned (31 DOWNTO 0);
      BEEBUS_READ      : IN     boolean;
      BEEBUS_STRB      : IN     boolean;
      ENABLE_DAQ       : IN     boolean;
      INTERNAL_TRIGGER : OUT    boolean  := false;
      ASIC_TESTINJECT  : OUT    boolean  := false;
      EXTERNAL_TRIGGER : OUT    boolean  := false;
      CLK              : IN     std_logic
   );

-- Declarations

END asic_pulse_inject ;

--
architecture behav of asic_pulse_inject is
  
  signal TEST_INJECT_RATE     : unsigned(19 downto 0)  := x"00000";
  signal DEL_SHIFT_REG        : unsigned(127 downto 0) := x"00000000000000000000000000000000";
  signal TRIGGER_SELECT       : unsigned(3 downto 0)   := x"0";
  signal COMMAND_REGISTER     : unsigned(31 downto 0)  := x"00000000";
  signal RATE_COUNT           : unsigned(19 downto 0)  := x"00000";
  signal extrig_pulse_count   : unsigned(15 downto 0)  := x"0000";
  signal ENABLE_PRDIC_INJECT  : boolean                := false;
  signal DAQ_INTERUPT         : boolean                := false;
  signal GET_NEW_COMMAND      : boolean                := false;
  signal GOT_THAT_COMMAND     : boolean                := false;
  signal NEW_COMMAND          : boolean                := false;
  signal TRIGGER              : boolean                := false;
  signal EXTERNAL_TRIGGER_int : boolean                := false;
  signal EXTERNAL_TRIGGER_del : boolean                := false;
  signal ASIC_SRCK_int        : std_logic              := '0';

begin
  RECEIVE_COMMAND : process (BEEBUS_CLK) is
  begin
    if BEEBUS_CLK'event and BEEBUS_CLK = '1' then
      BEEBUS_DATA  <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
      DAQ_INTERUPT <= DAQ_INTERUPT and not GOT_THAT_COMMAND;
      if (BEEBUS_ADDR = x"0003")then
        if BEEBUS_READ then
          BEEBUS_DATA <= COMMAND_REGISTER;
        elsif BEEBUS_STRB then
          COMMAND_REGISTER <= BEEBUS_DATA;
          DAQ_INTERUPT     <= true;
        end if;
      end if;
    end if;
  end process RECEIVE_COMMAND;

  NEW_COMMAND         <= GET_NEW_COMMAND and not GOT_THAT_COMMAND;
  ENABLE_PRDIC_INJECT <= TEST_INJECT_RATE > x"00001" and ENABLE_DAQ;
  UPDATE_CONTOLLER_STATE : process (CLK) is
  begin
    if clk'event and clk = '1' then
      GET_NEW_COMMAND  <= DAQ_INTERUPT;
      GOT_THAT_COMMAND <= GET_NEW_COMMAND;
      TRIGGER          <= false;

      if NEW_COMMAND then
        TRIGGER          <= COMMAND_REGISTER(19 downto 0) = x"00001";
        TEST_INJECT_RATE <= COMMAND_REGISTER(19 downto 0);
        TRIGGER_SELECT   <= COMMAND_REGISTER(23 downto 20);
        RATE_COUNT       <= x"00001";
      end if;

      if ENABLE_PRDIC_INJECT then
        if RATE_COUNT = TEST_INJECT_RATE then
          RATE_COUNT <= x"00001";
          TRIGGER    <= true;
        else
          RATE_COUNT <= RATE_COUNT+1;
        end if;
      end if;
      
    end if;
  end process UPDATE_CONTOLLER_STATE;

  SELECT_TRIG_OUT : process (TRIGGER, TRIGGER_SELECT) is
  begin
    INTERNAL_TRIGGER     <= TRIGGER and (TRIGGER_SELECT(0) = '1');
    ASIC_TESTINJECT      <= TRIGGER and (TRIGGER_SELECT(1) = '1');
    EXTERNAL_TRIGGER_int <= TRIGGER and (TRIGGER_SELECT(2) = '1');
  end process SELECT_TRIG_OUT;

  EXT_TRIG_SHIFT_REG : process (clk) is
  begin
    if clk'event and clk = '1' then
      DEL_SHIFT_REG        <= DEL_SHIFT_REG(126 downto 0) & BOOL2SL(EXTERNAL_TRIGGER_int);
      EXTERNAL_TRIGGER_del <= DEL_SHIFT_REG(127) = '1';
    end if;
  end process EXT_TRIG_SHIFT_REG;

  extrig_pulse_stretch : process (clk) is
  begin
    if clk'event and clk = '1' then
      EXTERNAL_TRIGGER <= false;
      if EXTERNAL_TRIGGER_del then
        extrig_pulse_count <= ext_pulse_width;
      elsif not(extrig_pulse_count = x"0000") then
        extrig_pulse_count <= extrig_pulse_count - 1;
        EXTERNAL_TRIGGER   <= true;
      end if;
    end if;
  end process extrig_pulse_stretch;
  
end architecture behav;

