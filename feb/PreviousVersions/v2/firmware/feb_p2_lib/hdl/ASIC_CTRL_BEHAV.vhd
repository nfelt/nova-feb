--
-- VHDL Architecture feb_p2_lib.ASIC_CTRL.BEHAV
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 17:00:32 02/26/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

ENTITY ASIC_CTRL IS
   PORT( 
      BEEBUS_ADDR    : IN     unsigned (15 DOWNTO 0);
      BEEBUS_CLK     : IN     std_logic;
      BEEBUS_DATA    : INOUT  unsigned (31 DOWNTO 0);
      BEEBUS_READ    : IN     boolean;
      BEEBUS_STRB    : IN     boolean;
      CLK            : IN     std_logic;
      ASIC_CHIPRESET : OUT    boolean;
      ASIC_SHIFTIN   : OUT    std_logic;
      ASIC_SHIFTOUT  : IN     std_logic;
      ASIC_SRCK      : OUT    std_logic;
      ASIC_IBIAS     : OUT    boolean;
      ASIC_INTEGRST  : OUT    boolean
   );

-- Declarations

END ASIC_CTRL ;

--
architecture BEHAV of ASIC_CTRL is

  signal COMMAND_REGISTER      : unsigned(31 downto 0) := x"00000000";
  signal ASIC_INTEGRST_COUNTER : unsigned(23 downto 0) := x"000000";
  signal ASIC_IBIAS_COUNTER    : unsigned(23 downto 0) := x"000000";
  signal ASIC_SHIFT_COUNT      : unsigned(4 downto 0)  := "00000";
  signal ASIC_SHIFT_REGISTER   : unsigned(22 downto 0) := "00000000000000000000000";
  signal ASIC_COMMAND_SENT     : boolean               := false;
  signal ASIC_COMMAND_SENT_c1  : boolean               := false;
  signal ASIC_COMMAND_SENT_c2  : boolean               := false;
  signal DAQ_INTERUPT          : boolean               := false;
  signal GET_NEW_COMMAND       : boolean               := false;
  signal GOT_THAT_COMMAND      : boolean               := false;
  signal NEW_COMMAND           : boolean               := false;
  signal ASIC_SRCK_int         : std_logic             := '0';




  
begin

  RECEIVE_COMMAND : process (BEEBUS_CLK) is
  begin
    if BEEBUS_CLK'event and BEEBUS_CLK = '1' then
      BEEBUS_DATA  <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
      DAQ_INTERUPT <= DAQ_INTERUPT and not GOT_THAT_COMMAND;

      if (BEEBUS_ADDR = x"0007" and BEEBUS_STRB)then
        ASIC_INTEGRST_COUNTER <= BEEBUS_DATA(23 downto 0);
      elsif ASIC_INTEGRST_COUNTER /= "000000" then
        ASIC_INTEGRST_COUNTER <= ASIC_INTEGRST_COUNTER-1;
        ASIC_INTEGRST         <= true;
      else
        ASIC_INTEGRST <= false;
      end if;

      if (BEEBUS_ADDR = x"0008" and BEEBUS_STRB)then
        ASIC_IBIAS_COUNTER <= BEEBUS_DATA(23 downto 0);
      elsif ASIC_IBIAS_COUNTER /= "000000" then
        ASIC_IBIAS_COUNTER <= ASIC_IBIAS_COUNTER-1;
        ASIC_IBIAS         <= true;
      else
        ASIC_IBIAS <= false;
      end if;

      if (BEEBUS_ADDR = x"0002")then
        if BEEBUS_READ then
          BEEBUS_DATA <= "000000000" & ASIC_SHIFT_REGISTER;
        elsif BEEBUS_STRB then
          COMMAND_REGISTER <= BEEBUS_DATA;
          DAQ_INTERUPT     <= true;
        end if;
      end if;
    end if;
  end process RECEIVE_COMMAND;


  UPDATE_CONTOLLER_STATE : process (CLK) is
  begin
    if clk'event and clk = '1' then
      GET_NEW_COMMAND      <= DAQ_INTERUPT;
      GOT_THAT_COMMAND     <= GET_NEW_COMMAND;
      ASIC_COMMAND_SENT_c1 <= ASIC_COMMAND_SENT;
      ASIC_COMMAND_SENT_c2 <= ASIC_COMMAND_SENT_c1;

      if NEW_COMMAND then
        ASIC_SHIFT_COUNT    <= "00000";
        ASIC_SHIFT_REGISTER <= COMMAND_REGISTER(22 downto 0);
        ASIC_SRCK_int       <= '0';
      elsif not ASIC_COMMAND_SENT then
        ASIC_SRCK_int <= not ASIC_SRCK_int;
        if ASIC_SRCK_int = '1' then
          ASIC_SHIFT_REGISTER <= ASIC_SHIFTOUT & ASIC_SHIFT_REGISTER(22 downto 1);
          ASIC_SHIFT_COUNT    <= ASIC_SHIFT_COUNT+1;
        end if;
      end if;
    end if;
  end process UPDATE_CONTOLLER_STATE;

  DELAY_CHIPRESET : process (CLK) is
  begin
    if clk'event and clk = '0' then
      ASIC_CHIPRESET <= ASIC_COMMAND_SENT_c1 and not ASIC_COMMAND_SENT_c2;
    end if;
  end process DELAY_CHIPRESET;
  
  update_asic : process (ASIC_SRCK_int, ASIC_SHIFT_REGISTER(0), ASIC_COMMAND_SENT_c1,
                         ASIC_COMMAND_SENT_c2, ASIC_SHIFT_COUNT, GET_NEW_COMMAND,
                         GOT_THAT_COMMAND) is
  begin
    NEW_COMMAND       <= GET_NEW_COMMAND and not GOT_THAT_COMMAND;
    ASIC_COMMAND_SENT <= ASIC_SHIFT_COUNT = "10111";
    ASIC_SHIFTIN      <= ASIC_SHIFT_REGISTER(0);
    ASIC_SRCK         <= ASIC_SRCK_int;
  end process update_asic;

end architecture BEHAV;

