--
-- VHDL Architecture feb_p2_lib.clock_control.struct
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 14:38:14 03/ 9/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity clock_control is
  port(
    CLK_IN       : in  std_logic;
    CLK_16_PH0   : out std_logic;
    CLK_16_PH180 : out std_logic;
    CLK_32_PH0   : out std_logic;
    CLK_FX       : out std_logic;
    ASIC_CLK     : out std_logic;
    ASIC_CLK_EN  : in  boolean;
    ADC_CLK      : out std_logic;
    BEEBUS_CLK   : out std_logic;
    DAQ_CLK      : out std_logic
    );

-- Declarations

end clock_control;

--
architecture struct of clock_control is

  component DCM_PS
    generic(
      CLKDV_DIVIDE          : real       := 2.0;
      CLKFX_DIVIDE          : integer    := 1;
      CLKFX_MULTIPLY        : integer    := 4;
      CLKIN_DIVIDE_BY_2     : boolean    := false;
      CLKIN_PERIOD          : real       := 62.5;
      CLKOUT_PHASE_SHIFT    : string     := "NONE";
      CLK_FEEDBACK          : string     := "1X";
      DESKEW_ADJUST         : string     := "SYSTEM_SYNCHRONOUS";
      DFS_FREQUENCY_MODE    : string     := "LOW";
      DLL_FREQUENCY_MODE    : string     := "LOW";
      DUTY_CYCLE_CORRECTION : boolean    := true;
      FACTORY_JF            : bit_vector := X"C080";
      PHASE_SHIFT           : integer    := 0;
      STARTUP_WAIT          : boolean    := false
      );

    port (
      CLK0     : out std_ulogic;
      CLK180   : out std_ulogic;
      CLK270   : out std_ulogic;
      CLK2X    : out std_ulogic;
      CLK2X180 : out std_ulogic;
      CLK90    : out std_ulogic;
      CLKDV    : out std_ulogic;
      CLKFX    : out std_ulogic;
      CLKFX180 : out std_ulogic;
      LOCKED   : out std_ulogic;
      PSDONE   : out std_ulogic;
      STATUS   : out std_ulogic;
      CLKFB    : in  std_ulogic;
      CLKIN    : in  std_ulogic;
      PSCLK    : in  std_ulogic;
      PSEN     : in  std_ulogic;
      PSINCDEC : in  std_ulogic;
      RST      : in  std_ulogic
      );
  end component;

  component IBUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;


  component BUFGCE
    port(
      O  : out std_ulogic;
      CE : in  std_ulogic;
      I  : in  std_ulogic
      );
  end component;
  
   FOR ALL : DCM_PS USE ENTITY UNISIM.DCM_PS;

  signal treset_count      : unsigned(15 downto 0) := x"0000";
  signal treset            : std_logic             := '1';
  signal CLK_IN_int        : std_logic             := '1';
  signal CLK_16_PH0_int    : std_logic             := '1';
  signal CLK_16_PH180_int  : std_logic             := '1';
  signal CLK_32_PH0_int    : std_logic             := '1';
  signal CLK_FX_int        : std_logic             := '1';
  signal CLK_IN_DCM_CLK0   : std_logic             := '1';
  signal CLK_IN_DCM_CLK180 : std_logic             := '1';
  signal CLK_IN_DCM_CLK2X  : std_logic             := '1';
  signal CLK_IN_DCM_CLKFX  : std_logic             := '1';
  signal ASIC_CLK_EN_int   : boolean               := false;
  

begin


  CLK_IN_DCM : DCM_PS
    generic map (
      CLKDV_DIVIDE          => 2.0,
      CLKFX_DIVIDE          => 2,
      CLKFX_MULTIPLY        => 3,
      CLKIN_DIVIDE_BY_2     => false,
      CLKIN_PERIOD          => 62.5,
      CLKOUT_PHASE_SHIFT    => "NONE",
      CLK_FEEDBACK          => "1X",
      DESKEW_ADJUST         => "SYSTEM_SYNCHRONOUS",
      DFS_FREQUENCY_MODE    => "LOW",
      DLL_FREQUENCY_MODE    => "LOW",
      DUTY_CYCLE_CORRECTION => true,
      FACTORY_JF            => X"C080",
      PHASE_SHIFT           => 0,
      STARTUP_WAIT          => false
      )

    port map (
      CLK0     => CLK_IN_DCM_CLK0,
      CLK180   => CLK_IN_DCM_CLK180,
      CLK270   => open,
      CLK2X    => CLK_IN_DCM_CLK2X,
      CLK2X180 => open,
      CLK90    => open,
      CLKDV    => open,
      CLKFX    => CLK_IN_DCM_CLKFX,
      CLKFX180 => open,
      LOCKED   => open,
      PSDONE   => open,
      STATUS   => open,
      CLKFB    => CLK_16_PH0_int,
      CLKIN    => CLK_IN_int,
      PSCLK    => '0',
      PSEN     => '0',
      PSINCDEC => '0',
      RST      => treset
      );

  DCM_CLK_IN : IBUFG
    port map (
      O => CLK_IN_int,
      I => CLK_IN
      );

  CLK_16_PH0_int_BG : BUFG
    port map (
      O => CLK_16_PH0_int,
      I => CLK_IN_DCM_CLK0
      );

  CLK_16_PH180_BG : BUFG
    port map (
      O => CLK_16_PH180_int,
      I => CLK_IN_DCM_CLK180
      );

  CLK_32_PH0_BG : BUFG
    port map (
      O => CLK_32_PH0_int,
      I => CLK_IN_DCM_CLK2X
      );

  CLK_FX_BG : BUFG
    port map (
      O => CLK_FX_int,
      I => CLK_IN_DCM_CLKFX
      );

  ASIC_BGCE : BUFGCE
    port map (
      O  => ASIC_CLK,                   -- Clock buffer ouptput
      CE => BOOL2SL(ASIC_CLK_EN_int),   -- Clock enable input
      I  => CLK_16_PH180_int            -- Clock buffer input
      );
  ASSIGN_CLKS : process (CLK_16_PH0_int, CLK_16_PH180_int, CLK_32_PH0_int, CLK_FX_int) is
  begin
    CLK_16_PH0   <= CLK_16_PH0_int;
    CLK_16_PH180 <= CLK_16_PH180_int;
    CLK_32_PH0   <= CLK_32_PH0_int;
    CLK_FX       <= CLK_FX_int;

    ADC_CLK    <= CLK_16_PH180_int;
    BEEBUS_CLK <= CLK_16_PH0_int;
    DAQ_CLK    <= CLK_32_PH0_int;
  end process ASSIGN_CLKS;

  enable_sinc : process (CLK_16_PH180_int) is
  begin
    if CLK_16_PH180_int'event and CLK_16_PH180_int = '0' then
      ASIC_CLK_EN_int <= ASIC_CLK_EN;
    end if;
  end process enable_sinc;

  TRESET_GEN : process (CLK_IN_int) is
  begin
    if CLK_IN_int'event and CLK_IN_int = '1' then  -- rising clock edge
      if treset_count < x"ffff" then
        treset_count <= treset_count+1;
        treset       <= '1';
      else
        treset <= '0';
      end if;
    end if;
  end process TRESET_GEN;
end architecture struct;

