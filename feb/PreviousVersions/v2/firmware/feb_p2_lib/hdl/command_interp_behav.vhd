--
-- VHDL Architecture feb_p2_lib.command_interp.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 16:46:10 02/14/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

ENTITY command_interp IS
   PORT( 
      BEEBUS_ADDR : IN     unsigned (15 DOWNTO 0);
      BEEBUS_DATA : INOUT  unsigned (31 DOWNTO 0);
      BEEBUS_CLK  : IN     std_logic;
      BEEBUS_READ : IN     boolean;
      BEEBUS_STRB : IN     boolean;
      CLK         : IN     std_logic;
      ENABLE_DAQ  : OUT    boolean;
      RESET_DAQ   : OUT    boolean
   );

-- Declarations

END command_interp ;

--
architecture behav of command_interp is

  type CONTOLLER_STATE_TYPE is (IDLE, STOPPING, STOP, RUNNING);
  signal CONTOLLER_STATE      : CONTOLLER_STATE_TYPE := IDLE;
  signal NEXT_CONTOLLER_STATE : CONTOLLER_STATE_TYPE := IDLE;

  signal COMMAND_REGISTER : unsigned(31 downto 0) := x"00000000";
  signal DAQ_INTERUPT     : boolean               := false;
  signal GET_NEW_COMMAND  : boolean               := false;
  signal GOT_THAT_COMMAND : boolean               := false;
  signal NEW_COMMAND      : boolean;
  signal RESET_DAQ_int    : boolean;
  
begin
  RECEIVE_COMMAND : process (BEEBUS_CLK) is
  begin
    if BEEBUS_CLK'event and BEEBUS_CLK = '1' then
      BEEBUS_DATA  <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
      DAQ_INTERUPT <= DAQ_INTERUPT and not GOT_THAT_COMMAND;
      if (BEEBUS_ADDR = x"0001")then
        if BEEBUS_READ then
          BEEBUS_DATA <= COMMAND_REGISTER;
        elsif BEEBUS_STRB then
          COMMAND_REGISTER <= BEEBUS_DATA;
          DAQ_INTERUPT     <= true;
        end if;
      end if;
    end if;
  end process RECEIVE_COMMAND;

  UPDATE_CONTOLLER_STATE : process (CLK) is
  begin
    if clk'event and clk = '1' then
      GET_NEW_COMMAND  <= DAQ_INTERUPT;
      GOT_THAT_COMMAND <= GET_NEW_COMMAND;
      NEW_COMMAND      <= GET_NEW_COMMAND and not GOT_THAT_COMMAND;
      RESET_DAQ_int    <= NEW_COMMAND and COMMAND_REGISTER(31 downto 28) = x"3";
      if RESET_DAQ_int then
        CONTOLLER_STATE <= IDLE;
      else
        CONTOLLER_STATE <= NEXT_CONTOLLER_STATE;
      end if;
    end if;
  end process UPDATE_CONTOLLER_STATE;

  FIND_NEXT_STATE : process (CONTOLLER_STATE, COMMAND_REGISTER, NEW_COMMAND, RESET_DAQ_int) is
  begin
    ENABLE_DAQ <= false;
    RESET_DAQ  <= RESET_DAQ_int;
    case CONTOLLER_STATE is
      when IDLE =>
        if NEW_COMMAND and COMMAND_REGISTER(31 downto 28) = x"1" then
          NEXT_CONTOLLER_STATE <= RUNNING;
        else
          NEXT_CONTOLLER_STATE <= IDLE;
        end if;
      when RUNNING =>
        if NEW_COMMAND and COMMAND_REGISTER(31 downto 28) = x"2" then
          NEXT_CONTOLLER_STATE <= STOPPING;
        else
          NEXT_CONTOLLER_STATE <= RUNNING;
        end if;
        ENABLE_DAQ <= true;
      when STOPPING =>
        NEXT_CONTOLLER_STATE <= IDLE;
      when others =>
        NEXT_CONTOLLER_STATE <= STOPPING;
    end case;

  end process FIND_NEXT_STATE;

end architecture behav;

