--
-- VHDL Architecture feb_p2_lib.reg.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 16:41:41 10/26/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity reg is
  port(
    BEEBUS_ADDR : in    unsigned (15 downto 0);
    BEEBUS_DATA : inout unsigned (31 downto 0);
    BEEBUS_READ : in    boolean;
    BEEBUS_CLK  : in    std_logic;
    BEEBUS_STRB : in    boolean;
    REGIN_DATA  : out   std_logic_vector (31 downto 0);
    REGOUT_DATA : in    std_logic_vector (31 downto 0)
    );

-- Declarations

end reg;

--
architecture behav of reg is

begin
  RECEIVE_COMMAND : process (BEEBUS_CLK) is
  begin
    if BEEBUS_CLK'event and BEEBUS_CLK = '1' then
      BEEBUS_DATA <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
      if (BEEBUS_ADDR = x"0020")then
        if BEEBUS_READ then
          BEEBUS_DATA <= unsigned(REGOUT_DATA);
        elsif BEEBUS_STRB then
          REGIN_DATA <= std_logic_vector(BEEBUS_DATA);
        end if;
      end if;
    end if;
  end process RECEIVE_COMMAND;

end architecture behav;

