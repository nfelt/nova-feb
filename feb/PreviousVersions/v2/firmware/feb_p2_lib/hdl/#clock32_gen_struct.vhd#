--
-- VHDL Architecture feb_p2_lib.clock32_gen.struct
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 16:33:09 01/14/2008
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;
LIBRARY unisim;
USE unisim.all;
library harvard_std;
USE harvard_std.lppc_custom_fn_pkg.all;


ENTITY clock32_gen IS
  port(
    CLK_IN          : in  std_logic;
    CLK_32          : out std_logic;
    BEEBUS_CLK      : out std_logic;
    ASIC_CLK        : out std_logic;
    ASIC_CLK_EN_ext : out std_logic;
    ASIC_CLK_EN     : in  boolean;
    CLOCK_RESET_EXT : out boolean;
    asic_on         : out boolean;
    DCM_LOCKED_EXT  : out boolean;
    POWERUP_RESET   : out boolean
    );

END ENTITY clock32_gen;

--
ARCHITECTURE struct OF clock32_gen IS
  attribute CLK_FEEDBACK          : string;
  attribute CLKDV_DIVIDE          : string;
  attribute CLKFX_DIVIDE          : string;
  attribute CLKFX_MULTIPLY        : string;
  attribute CLKIN_DIVIDE_BY_2     : string;
  attribute CLKIN_PERIOD          : string;
  attribute CLKOUT_PHASE_SHIFT    : string;
  attribute DESKEW_ADJUST         : string;
  attribute DFS_FREQUENCY_MODE    : string;
  attribute DLL_FREQUENCY_MODE    : string;
  attribute DUTY_CYCLE_CORRECTION : string;
  attribute FACTORY_JF            : string;
  attribute PHASE_SHIFT           : string;
  attribute STARTUP_WAIT          : string;

  component DCM_SP
    -- synopsys translate_off
    generic(CLK_FEEDBACK          : string     := "1X";
            CLKDV_DIVIDE          : real       := 2.0;
            CLKFX_DIVIDE          : integer    := 2;
            CLKFX_MULTIPLY        : integer    := 8;
            CLKIN_DIVIDE_BY_2     : boolean    := false;
            CLKIN_PERIOD          : real       := 62.500;
            CLKOUT_PHASE_SHIFT    : string     := "NONE";
            DESKEW_ADJUST         : string     := "SYSTEM_SYNCHRONOUS";
            DFS_FREQUENCY_MODE    : string     := "LOW";
            DLL_FREQUENCY_MODE    : string     := "LOW";
            DUTY_CYCLE_CORRECTION : boolean    := true;
            FACTORY_JF            : bit_vector := x"C080";
            PHASE_SHIFT           : integer    := 0;
            STARTUP_WAIT          : boolean    := false;
            DSS_MODE              : string     := "NONE");
    -- synopsys translate_on
    port (CLKIN    : in  std_logic;
          CLKFB    : in  std_logic;
          RST      : in  std_logic;
          PSEN     : in  std_logic;
          PSINCDEC : in  std_logic;
          PSCLK    : in  std_logic;
          DSSEN    : in  std_logic;
          CLK0     : out std_logic;
          CLK90    : out std_logic;
          CLK180   : out std_logic;
          CLK270   : out std_logic;
          CLKDV    : out std_logic;
          CLK2X    : out std_logic;
          CLK2X180 : out std_logic;
          CLKFX    : out std_logic;
          CLKFX180 : out std_logic;
          STATUS   : out std_logic_vector (7 downto 0);
          LOCKED   : out std_logic;
          PSDONE   : out std_logic);
  end component;

  attribute CLK_FEEDBACK of DCM_SP_INST          : label is "1X";
  attribute CLKDV_DIVIDE of DCM_SP_INST          : label is "2.0";
  attribute CLKFX_DIVIDE of DCM_SP_INST          : label is "2";
  attribute CLKFX_MULTIPLY of DCM_SP_INST        : label is "8";
  attribute CLKIN_DIVIDE_BY_2 of DCM_SP_INST     : label is "FALSE";
  attribute CLKIN_PERIOD of DCM_SP_INST          : label is "62.500";
  attribute CLKOUT_PHASE_SHIFT of DCM_SP_INST    : label is "NONE";
  attribute DESKEW_ADJUST of DCM_SP_INST         : label is "SYSTEM_SYNCHRONOUS";
  attribute DFS_FREQUENCY_MODE of DCM_SP_INST    : label is "LOW";
  attribute DLL_FREQUENCY_MODE of DCM_SP_INST    : label is "LOW";
  attribute DUTY_CYCLE_CORRECTION of DCM_SP_INST : label is "TRUE";
  attribute FACTORY_JF of DCM_SP_INST            : label is "C080";
  attribute PHASE_SHIFT of DCM_SP_INST           : label is "0";
  attribute STARTUP_WAIT of DCM_SP_INST          : label is "TRUE";

  component IBUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;


  component BUFGCE
    port(
      O  : out std_ulogic;
      CE : in  std_ulogic;
      I  : in  std_ulogic
      );
  end component;

  signal DCM_LOCK_DEL    : std_logic_vector(3 downto 0) := x"0";
  signal DCM_LOCKED      : std_logic;
  signal ASIC_CLK_int    : std_logic;
  signal CLK_16_INT      : std_logic;
  signal CLK_64_INT      : std_logic;
  signal CLK_IN_INT      : std_logic;
  signal CLK_16_UB       : std_logic;
  signal CLK_64_UB       : std_logic;
  signal CLOCK_RESET     : boolean;
  signal ASIC_CLK_GEN    : unsigned(1 downto 0) := "00";
  signal ASIC_CLK_EN_int : std_logic;
  

begin
  CLK_16 <= CLK_IN_INT;
  CLK_64 <= CLK_64_INT;

  DCM_SP_INST : DCM_SP
    -- synopsys translate_off
    generic map(
      CLK_FEEDBACK          => "1X",
      CLKDV_DIVIDE          => 2.0,
      CLKFX_DIVIDE          => 2,
      CLKFX_MULTIPLY        => 8,
      CLKIN_DIVIDE_BY_2     => false,
      CLKIN_PERIOD          => 62.500,
      CLKOUT_PHASE_SHIFT    => "NONE",
      DESKEW_ADJUST         => "SYSTEM_SYNCHRONOUS",
      DFS_FREQUENCY_MODE    => "LOW",
      DLL_FREQUENCY_MODE    => "LOW",
      DUTY_CYCLE_CORRECTION => true,
      FACTORY_JF            => x"C080",
      PHASE_SHIFT           => 0,
      STARTUP_WAIT          => true
      )
    -- synopsys translate_on
    port map (
      CLKFB    => CLK_16_int,
      CLKIN    => CLK_IN_INT,
      DSSEN    => '0',
      PSCLK    => '0',
      PSEN     => '0',
      PSINCDEC => '0',
      RST      => '0',
      CLKDV    => open,
      CLKFX    => CLK_64_UB,
      CLKFX180 => open,
      CLK0     => CLK_16_UB,
      CLK2X    => open,
      CLK2X180 => open,
      CLK90    => open,
      CLK180   => open,
      CLK270   => open,
      LOCKED   => DCM_LOCKED,
      PSDONE   => open,
      STATUS   => open
      );


  CLK_64_BUF : BUFG
    port map (
      O => CLK_64_int,
      I => CLK_64_UB
      );

  CLK_16_BUF : BUFG
    port map (
      O => CLK_16_int,
      I => CLK_16_UB
      );

  CLK_INBG : IBUFG
    port map (
      O => CLK_IN_INT,
      I => CLK_IN
      );

  BEEBUS_CLK_BUF : BUFG
    port map (
      O => BEEBUS_CLK,
      I => CLK_IN_INT
      );

  ASIC_BG : BUFG
    port map (
      O => ASIC_CLK,
      I => ASIC_CLK_GEN(1)
      );

  ASIC_on_REG : process (CLK_64_int) is
  begin
    if CLK_64_int'event and CLK_64_int = '1' then
      if ASIC_CLK_EN then
        ASIC_CLK_GEN <= ASIC_CLK_GEN+1;
      end if;
    end if;
  end process ASIC_on_REG;
  asic_on <= ASIC_CLK_GEN(1) = '1';


  POWER_UP_RESET : process (CLK_IN_INT) is
  begin
    if CLK_IN_INT'event and CLK_IN_INT = '1' then
      DCM_LOCK_DEL(3 downto 0) <= DCM_LOCK_DEL(2 downto 0)& DCM_LOCKED;
      POWERUP_RESET            <= DCM_LOCK_DEL(3) = '0';
    end if;
  end process POWER_UP_RESET;

  CLOCK_RESET_ext <= CLOCK_RESET;
  DCM_LOCKED_ext  <= DCM_LOCKED = '1';
  ASIC_CLK_EN_ext <= ASIC_CLK_EN_int;

END ARCHITECTURE struct;

