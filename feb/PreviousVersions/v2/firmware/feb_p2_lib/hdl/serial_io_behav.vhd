--
-- VHDL Architecture feb_p2_lib.serial_io.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 15:52:20 11/ 6/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;
LIBRARY unisim;
USE unisim.all;
library harvard_std;
USE harvard_std.lppc_custom_fn_pkg.all;

ENTITY serial_io IS
   PORT( 
      CLK     : IN     std_logic_vector (15 DOWNTO 0);
      PAR_IN  : IN     std_logic_vector ( 9 DOWNTO 0 );
      SER_OUT : IN     std_logic;
      SER_IN  : IN     std_logic;
      PAR_OUT : IN     std_logic_vector ( 9 DOWNTO 0 )
   );

-- Declarations

END serial_io ;

--
ARCHITECTURE behav OF serial_io IS
BEGIN
END ARCHITECTURE behav;

