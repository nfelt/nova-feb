--
-- VHDL Architecture feb_p2_lib.clock_control.struct
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 14:38:14 03/ 9/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library UNISIM;
use UNISIM.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity clock_control is
  port(
    DCM_CLK        : in  std_logic;
    ASIC_CLK       : out std_logic;
    dcm_lo         : out std_logic;
    dcm_cr         : out std_logic;
    ASIC_CLK_EN    : in  boolean;
    ADC_BEEBUS_CLK : out std_logic;
    DAQ_CLK        : out std_logic;
    RESET_DCM      : in  boolean;
    POWERUP_RESET  : out boolean
    );

-- Declarations

end clock_control;

--
architecture struct of clock_control is

  attribute CLK_FEEDBACK          : string;
  attribute CLKDV_DIVIDE          : string;
  attribute CLKFX_DIVIDE          : string;
  attribute CLKFX_MULTIPLY        : string;
  attribute CLKIN_DIVIDE_BY_2     : string;
  attribute CLKIN_PERIOD          : string;
  attribute CLKOUT_PHASE_SHIFT    : string;
  attribute DESKEW_ADJUST         : string;
  attribute DFS_FREQUENCY_MODE    : string;
  attribute DLL_FREQUENCY_MODE    : string;
  attribute DUTY_CYCLE_CORRECTION : string;
  attribute FACTORY_JF            : string;
  attribute PHASE_SHIFT           : string;
  attribute STARTUP_WAIT          : string;

  component DCM_SP
    -- synopsys translate_off
    generic(CLK_FEEDBACK          : string     := "1X";
            CLKDV_DIVIDE          : real       := 2.0;
            CLKFX_DIVIDE          : integer    := 1;
            CLKFX_MULTIPLY        : integer    := 4;
            CLKIN_DIVIDE_BY_2     : boolean    := false;
            CLKIN_PERIOD          : real       := 10.0;
            CLKOUT_PHASE_SHIFT    : string     := "NONE";
            DESKEW_ADJUST         : string     := "SYSTEM_SYNCHRONOUS";
            DFS_FREQUENCY_MODE    : string     := "LOW";
            DLL_FREQUENCY_MODE    : string     := "LOW";
            DUTY_CYCLE_CORRECTION : boolean    := true;
            FACTORY_JF            : bit_vector := x"C080";
            PHASE_SHIFT           : integer    := 0;
            STARTUP_WAIT          : boolean    := false;
            DSS_MODE              : string     := "NONE");
    -- synopsys translate_on
    port (CLKIN    : in  std_logic;
          CLKFB    : in  std_logic;
          RST      : in  std_logic;
          PSEN     : in  std_logic;
          PSINCDEC : in  std_logic;
          PSCLK    : in  std_logic;
          DSSEN    : in  std_logic;
          CLK0     : out std_logic;
          CLK90    : out std_logic;
          CLK180   : out std_logic;
          CLK270   : out std_logic;
          CLKDV    : out std_logic;
          CLK2X    : out std_logic;
          CLK2X180 : out std_logic;
          CLKFX    : out std_logic;
          CLKFX180 : out std_logic;
          STATUS   : out std_logic_vector (7 downto 0);
          LOCKED   : out std_logic;
          PSDONE   : out std_logic);
  end component;

  attribute CLK_FEEDBACK of DCM_SP_INST          : label is "1X";
  attribute CLKDV_DIVIDE of DCM_SP_INST          : label is "2.0";
  attribute CLKFX_DIVIDE of DCM_SP_INST          : label is "1";
  attribute CLKFX_MULTIPLY of DCM_SP_INST        : label is "4";
  attribute CLKIN_DIVIDE_BY_2 of DCM_SP_INST     : label is "FALSE";
  attribute CLKIN_PERIOD of DCM_SP_INST          : label is "62.500";
  attribute CLKOUT_PHASE_SHIFT of DCM_SP_INST    : label is "NONE";
  attribute DESKEW_ADJUST of DCM_SP_INST         : label is "SYSTEM_SYNCHRONOUS";
  attribute DFS_FREQUENCY_MODE of DCM_SP_INST    : label is "LOW";
  attribute DLL_FREQUENCY_MODE of DCM_SP_INST    : label is "LOW";
  attribute DUTY_CYCLE_CORRECTION of DCM_SP_INST : label is "TRUE";
  attribute FACTORY_JF of DCM_SP_INST            : label is "C080";
  attribute PHASE_SHIFT of DCM_SP_INST           : label is "0";
  attribute STARTUP_WAIT of DCM_SP_INST          : label is "FALSE";

  component IBUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;


  component BUFGCE
    port(
      O  : out std_ulogic;
      CE : in  std_ulogic;
      I  : in  std_ulogic
      );
  end component;

  signal DCM_LOCK_DEL    : std_logic_vector(3 downto 0) := x"0";
  signal DCM_LOCKED      : std_logic;
  signal DCM_RESET_COUNT : unsigned(17 downto 0)        := "000000000000000000";
  signal CLK_16_UB       : std_logic;
  signal CLK_16_INT      : std_logic;
  signal CLK_IN_INT      : std_logic;
  signal CLK_32_UB       : std_logic;
  signal CLK_32_INT      : std_logic;
  signal CLOCK_RESET     : boolean;
  

begin
  ADC_BEEBUS_CLK <= CLK_16_INT;

  DCM_SP_INST : DCM_SP
    -- synopsys translate_off
    generic map(
      CLK_FEEDBACK          => "1X",
      CLKDV_DIVIDE          => 2.0,
      CLKFX_DIVIDE          => 1,
      CLKFX_MULTIPLY        => 4,
      CLKIN_DIVIDE_BY_2     => false,
      CLKIN_PERIOD          => 62.500,
      CLKOUT_PHASE_SHIFT    => "NONE",
      DESKEW_ADJUST         => "SYSTEM_SYNCHRONOUS",
      DFS_FREQUENCY_MODE    => "LOW",
      DLL_FREQUENCY_MODE    => "LOW",
      DUTY_CYCLE_CORRECTION => true,
      FACTORY_JF            => x"C080",
      PHASE_SHIFT           => 0,
      STARTUP_WAIT          => false
      )
    -- synopsys translate_on
    port map (
      CLKFB    => CLK_32_INT,
      CLKIN    => DCM_CLK,
      DSSEN    => '0',
      PSCLK    => '0',
      PSEN     => '0',
      PSINCDEC => '0',
      RST      => bool2sl(RESET_DCM),
      CLKDV    => open,
      CLKFX    => open,
      CLKFX180 => open,
      CLK0     => CLK_16_UB,
      CLK2X    => CLK_32_UB,
      CLK2X180 => open,
      CLK90    => open,
      CLK180   => open,
      CLK270   => open,
      LOCKED   => DCM_LOCKED,
      PSDONE   => open,
      STATUS   => open
      );

  CLK_FB : BUFG
    port map (
      O => CLK_16_INT,
      I => CLK_16_UB
      );

  CLK_32_BUF : BUFG
    port map (
      O => CLK_32_INT,
      I => CLK_32_UB
      );

--  CLK_INBG : IBUFG
--    port map (
--      O => CLK_IN_INT,
--      I => CLK_IN
--      );

  ASIC_BGCE : BUFGCE
    port map (
      O  => ASIC_CLK,
      CE => BOOL2SL(ASIC_CLK_EN),
      I  => DCM_CLK
      );

  ASIC_CLK <= '0';
  DAQ_CLK  <= CLK_32_INT;

  POWER_UP_RESET : process (DCM_CLK) is
  begin
    if DCM_CLK'event and DCM_CLK = '1' then
      DCM_LOCK_DEL(3 downto 0) <= DCM_LOCK_DEL(2 downto 0)& DCM_LOCKED;
      POWERUP_RESET            <= DCM_LOCK_DEL(3) = '0';
    end if;
  end process POWER_UP_RESET;

  RESET_ON_UNLOCK : process (DCM_CLK) is
  begin
    if DCM_CLK'event and DCM_CLK = '1' then
      if DCM_RESET_COUNT x"ff"; then
                                   DCM_RESET_COUNT <= x"000000";
                                 else
                                   DCM_RESET_COUNT <= DCM_RESET_COUNT + 1;
                                 end if;
                                   CLOCK_RESET <= DCM_RESET_COUNT = x"ff";
                                 end if;
                                 end process RESET_ON_UNLOCK;
                                   dcm_lo <= DCM_LOCKED;
                                   dcm_cr <= bool2sl(CLOCK_RESET);
                                   
                                 end architecture struct;

