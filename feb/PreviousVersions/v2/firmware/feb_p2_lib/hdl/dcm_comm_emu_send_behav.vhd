--
-- VHDL Architecture feb_p2_lib.dcm_comm_emu_send.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 12:06:49 11/20/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity dcm_comm_emu_send is
  port(
    clk_32         : in  std_logic;
    DATA_IN        : in  std_logic_vector(7 downto 0);
    data_10par_out : out std_logic_vector(9 downto 0);
    SER_DATA       : out std_logic;
    SYNC           : out std_logic
    );

-- Declarations

end dcm_comm_emu_send;

--
architecture behav of dcm_comm_emu_send is

  signal ENCODED_DATA    : std_logic_vector(9 downto 0) := "0000000000";
  signal LAST_DATA_SENT : std_logic_vector(7 downto 0) := x"00";
  signal LATCHED_DATA_IN : std_logic_vector(7 downto 0) := x"00";
  signal LATCHED_K_IN    : std_logic                    := '0';
  signal SHIFT_REG       : std_logic_vector(9 downto 0) := "0000000000";
  signal BIT_COUNT       : integer range 15 downto 0    := 0;
  signal CLK_3_2_sig     : std_logic                    := '0';
  signal CLK_3_2_int     : std_logic                    := '0';

  component encode_8b10b
    port (
      din  : in  std_logic_vector(7 downto 0);
      kin  : in  std_logic;
      clk  : in  std_logic;
      dout : out std_logic_vector(9 downto 0));
  end component;

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;

  
begin

  
  DCM_COMM_ENCODE : encode_8b10b

    port map (
      din  => LATCHED_DATA_IN,
      kin  => LATCHED_K_IN,
      clk  => CLK_3_2_int,
      dout => ENCODED_DATA
      );

  par_to_ser : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '0' then
      SHIFT_REG <= SHIFT_REG(0) & SHIFT_REG(9 downto 1);
      BIT_COUNT <= BIT_COUNT +1;

      if (BIT_COUNT = 4) then
        CLK_3_2_sig <= '1';
      end if;

      if BIT_COUNT >= 9 then
        SHIFT_REG      <= ENCODED_DATA;
        BIT_COUNT      <= 0;
        DATA_10par_out <= SHIFT_REG;
        CLK_3_2_sig    <= '0';
        if LAST_DATA_SENT /= DATA_IN then
          LAST_DATA_SENT <= DATA_IN;
          LATCHED_DATA_IN <= DATA_IN;
          LATCHED_K_IN    <= '0';
        else
          LATCHED_DATA_IN <= "00111100";
          LATCHED_K_IN    <= '1';
        end if;
      end if;
      
    end if;
  end process par_to_ser;
  SER_DATA <= SHIFT_REG(0);

  COMM_CLK_IN : BUFG
    port map (
      O => CLK_3_2_int,
      I => CLK_3_2_sig
      );

  SYNC <= '0';
end architecture behav;

