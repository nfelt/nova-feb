--
-- VHDL Architecture feb_p2_lib.comm_decode.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 16:42:19 11/ 7/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity comm_decode is
  port(
    CLK_32         : in  std_logic;
    DATA_OUT       : out std_logic_vector (7 downto 0);
    DATA_10par_out : out std_logic_vector (9 downto 0);
    DATA_STRB      : out boolean;
    LINKED         : out boolean;
    COMM_ERROR     : out boolean;
    SERIAL_IN      : in  std_logic
    );

-- Declarations

end comm_decode;

--
architecture behav of comm_decode is
  signal ENCODED_DATA : std_logic_vector(9 downto 0) := "0000000000";
  signal SHIFT_REG    : std_logic_vector(9 downto 0) := "0000000000";
  signal DATA_FRAME   : std_logic                    := '0';
  signal LINK_COUNT   : unsigned(3 downto 0)         := x"0";
  signal BIT_COUNT    : unsigned(3 downto 0)         := x"0";
  signal CLK_3_2_sig  : std_logic                    := '0';
  signal CLK_3_2_int  : std_logic                    := '0';
  signal LINKED_int  : boolean                    := false;
  signal KOUT  : std_logic                    := '0';

  component decode_8b10b
    port (
      clk      : in  std_logic;
      din      : in  std_logic_vector(9 downto 0);
      dout     : out std_logic_vector(7 downto 0);
      kout     : out std_logic;
      code_err : out std_logic);
  end component;

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;

  
begin
  
  DCM_COMM_DECODE : decode_8b10b
    port map (
      clk      => CLK_3_2_int,
      din      => ENCODED_DATA,
      dout     => DATA_OUT,
      kout     => KOUT,
      code_err => open
      );

  ser_to_par : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '1' then
      DATA_STRB <= KOUT = '0';
      SHIFT_REG <= SERIAL_IN & SHIFT_REG(9 downto 1);
      BIT_COUNT <= BIT_COUNT +1;

      if (SHIFT_REG(6 downto 0) = "1111100") or
        (SHIFT_REG(6 downto 0) = "0000011") then
        BIT_COUNT  <= x"0";
        COMM_ERROR <= LINKED_int and not BIT_COUNT = x"9";
        LINKED_int     <= true;
      end if;

      if BIT_COUNT = 9 then
        ENCODED_DATA <= SHIFT_REG;
        CLK_3_2_sig <= '0';
      elsif BIT_COUNT = 4 then
        CLK_3_2_sig  <= BOOL2SL(LINKED_int);
      end if;
      
    end if;
  end process ser_to_par;
  DATA_10par_out <= ENCODED_DATA;

  COMM_CLK_IN : BUFG
    port map (
      O => CLK_3_2_int,
      I => CLK_3_2_sig
      );
LINKED <= LINKED_int;
end architecture behav;

