--
-- VHDL Architecture feb_p2_lib.dcm_comm_emu_clk.struct
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 16:38:44 11/25/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- Generate all clocking signals based on board XTAL and
-- FNAL DCM LVDS communication clock.
-- Spartan 3 part has min freq clk_in of ~18 MHz THe dcmfx
-- alone can accept a lower clock in freq however the only
-- output that can produce 16 MHz is the cloxkdv out
-- Two DCMs are used here. one to produce the 32 Mhz clk and
-- one to produce the 64Mhz clk.  The powerup reset is
-- based on the last DCM to lock.
-------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

ENTITY dcm_comm_emu_clk IS
   PORT( 
      DCM_CLK       : IN     std_logic;
      CLK_IN        : IN     std_logic;
      CLK_16        : OUT    std_logic;
      CLK_32        : OUT    std_logic;
      CLK_64        : OUT    std_logic;
      BEEBUS_CLK    : OUT    std_logic;
      ASIC_CLK      : OUT    std_logic;
      ASIC_CLK_EN   : IN     boolean;
      asic_on       : OUT    boolean;
      POWERUP_RESET : OUT    boolean
   );

-- Declarations

END dcm_comm_emu_clk ;

--
architecture struct of dcm_comm_emu_clk is

  component DCM_SP
    generic(
      CLK_FEEDBACK          : string;
      CLKDV_DIVIDE          : real;
      CLKFX_DIVIDE          : integer;
      CLKFX_MULTIPLY        : integer;
      CLKIN_DIVIDE_BY_2     : boolean;
      CLKIN_PERIOD          : real;
      CLKOUT_PHASE_SHIFT    : string;
      DESKEW_ADJUST         : string;
      DFS_FREQUENCY_MODE    : string;
      DLL_FREQUENCY_MODE    : string;
      DUTY_CYCLE_CORRECTION : boolean;
      FACTORY_JF            : bit_vector;
      PHASE_SHIFT           : integer;
      STARTUP_WAIT          : boolean
      );
    port (CLKIN    : in  std_logic;
          CLKFB    : in  std_logic;
          RST      : in  std_logic;
          PSEN     : in  std_logic;
          PSINCDEC : in  std_logic;
          PSCLK    : in  std_logic;
          DSSEN    : in  std_logic;
          CLK0     : out std_logic;
          CLK90    : out std_logic;
          CLK180   : out std_logic;
          CLK270   : out std_logic;
          CLKDV    : out std_logic;
          CLK2X    : out std_logic;
          CLK2X180 : out std_logic;
          CLKFX    : out std_logic;
          CLKFX180 : out std_logic;
          STATUS   : out std_logic_vector (7 downto 0);
          LOCKED   : out std_logic;
          PSDONE   : out std_logic);
  end component;


  component IBUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;


  component BUFGCE
    port(
      O  : out std_ulogic;
      CE : in  std_ulogic;
      I  : in  std_ulogic
      );
  end component;

  signal DCM_LOCK_DEL    : std_logic_vector(3 downto 0) := x"0";
  signal DCM_64_LOCK_DEL : std_logic_vector(7 downto 0) := x"00";
  signal DCM_64_RESET    : std_logic;
  signal CLK_32_UB       : std_logic;
  signal CLK_32_INT      : std_logic;
  signal DCM_32_LOCKED   : std_logic;
  signal CLK_64_UB       : std_logic;
  signal CLK_64_INT      : std_logic;
  signal DCM_64_LOCKED   : std_logic;
  signal CLK_IN_INT      : std_logic;
  signal CLK_16_INT      : std_logic;
  signal ASIC_CLK_GEN    : unsigned(1 downto 0)         := "00";
  

begin
  CLK_16 <= CLK_16_int;
  CLK_32 <= CLK_32_INT;
  CLK_64 <= CLK_64_INT;

  DCM_32 : DCM_SP
    generic map(
      CLK_FEEDBACK          => "2X",
      CLKDV_DIVIDE          => 2.0,
      CLKFX_DIVIDE          => 2,
      CLKFX_MULTIPLY        => 8,
      CLKIN_DIVIDE_BY_2     => false,
      CLKIN_PERIOD          => 62.500,
      CLKOUT_PHASE_SHIFT    => "NONE",
      DESKEW_ADJUST         => "SYSTEM_SYNCHRONOUS",
      DFS_FREQUENCY_MODE    => "LOW",
      DLL_FREQUENCY_MODE    => "LOW",
      DUTY_CYCLE_CORRECTION => true,
      FACTORY_JF            => x"C080",
      PHASE_SHIFT           => 0,
      STARTUP_WAIT          => false
      )
    port map (
      CLKFB    => CLK_32_int,
      CLKIN    => CLK_16_int,
      DSSEN    => '0',
      PSCLK    => '0',
      PSEN     => '0',
      PSINCDEC => '0',
      RST      => '0',
      CLKDV    => open,
      CLKFX    => open,
      CLKFX180 => open,
      CLK0     => open,
      CLK2X    => CLK_32_UB,
      CLK2X180 => open,
      CLK90    => open,
      CLK180   => open,
      CLK270   => open,
      LOCKED   => DCM_32_LOCKED,
      PSDONE   => open,
      STATUS   => open
      );

  CLK_INBG : IBUFG
    port map (
      O => CLK_IN_INT,
      I => CLK_IN
      );

  CLK_16_BUF : BUFG
    port map (
      O => CLK_16_int,
      I => CLK_IN_int
      );

  CLK_32_BUF : BUFG
    port map (
      O => CLK_32_int,
      I => CLK_32_UB
      );

  
  DCM_64 : DCM_SP
    generic map(
      CLK_FEEDBACK          => "2X",
      CLKDV_DIVIDE          => 2.0,
      CLKFX_DIVIDE          => 2,
      CLKFX_MULTIPLY        => 8,
      CLKIN_DIVIDE_BY_2     => false,
      CLKIN_PERIOD          => 31.250,
      CLKOUT_PHASE_SHIFT    => "NONE",
      DESKEW_ADJUST         => "SYSTEM_SYNCHRONOUS",
      DFS_FREQUENCY_MODE    => "LOW",
      DLL_FREQUENCY_MODE    => "LOW",
      DUTY_CYCLE_CORRECTION => true,
      FACTORY_JF            => x"C080",
      PHASE_SHIFT           => 0,
      STARTUP_WAIT          => false
      )
    port map (
      CLKFB    => CLK_64_int,
      CLKIN    => CLK_32_INT,
      DSSEN    => '0',
      PSCLK    => '0',
      PSEN     => '0',
      PSINCDEC => '0',
      RST      => DCM_64_RESET,
      CLKDV    => open,
      CLKFX    => open,
      CLKFX180 => open,
      CLK0     => open,
      CLK2X    => CLK_64_UB,
      CLK2X180 => open,
      CLK90    => open,
      CLK180   => open,
      CLK270   => open,
      LOCKED   => DCM_64_LOCKED,
      PSDONE   => open,
      STATUS   => open
      );

  CLK_64_BUF : BUFG
    port map (
      O => CLK_64_int,
      I => CLK_64_UB
      );

  BEEBUS_BUF : BUFG
    port map (
      O => BEEBUS_CLK,
      I => CLK_IN_INT
      );

  ASIC_on_REG : process (CLK_64_int) is
  begin
    if CLK_64_int'event and CLK_64_int = '1' then
      if ASIC_CLK_EN then
        ASIC_CLK_GEN <= ASIC_CLK_GEN+1;
      end if;
    end if;
  end process ASIC_on_REG;
  asic_on  <= ASIC_CLK_GEN(1) = '1';
  ASIC_CLK <= ASIC_CLK_GEN(1);
  dcm_64_RESET_PROC : process (CLK_16_int) is
  begin
    if CLK_16_int'event and CLK_16_int = '1' then
      DCM_64_LOCK_DEL(7 downto 0) <= DCM_64_LOCK_DEL(6 downto 0)& DCM_32_LOCKED;
      DCM_64_RESET                <= not DCM_64_LOCK_DEL(7);
    end if;
  end process dcm_64_RESET_PROC;

  POWER_UP_RESET : process (CLK_16_int) is
  begin
    if CLK_16_int'event and CLK_16_int = '1' then
      DCM_LOCK_DEL(3 downto 0) <= DCM_LOCK_DEL(2 downto 0)& DCM_64_LOCKED;
      POWERUP_RESET            <= DCM_LOCK_DEL(3) = '0';
    end if;
  end process POWER_UP_RESET;

end architecture struct;

