--
-- VHDL Architecture feb_p2_lib.clock_control.struct
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 14:38:14 03/ 9/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library UNISIM;
use UNISIM.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity clock_control is
  port(
    CLK_IN         : in  std_logic;
    DCM_CLK        : in  std_logic;
    ASIC_CLK       : out std_logic;
    dcm_lo         : out std_logic;
    dcm_cr         : out std_logic;
    ASIC_CLK_EN    : in  boolean;
    ADC_BEEBUS_CLK : out std_logic;
    DAQ_CLK        : out std_logic;
    POWERUP_RESET  : out boolean
    );

-- Declarations

end clock_control;

--
architecture struct of clock_control is
  component DCM_SP
    generic(
      CLKDV_DIVIDE          : real       := 2.0;
      CLKFX_DIVIDE          : integer    := 1;
      CLKFX_MULTIPLY        : integer    := 2;
      CLKIN_DIVIDE_BY_2     : boolean    := false;
      CLKIN_PERIOD          : real       := 62.5;
      CLKOUT_PHASE_SHIFT    : string     := "NONE";
      CLK_FEEDBACK          : string     := "1X";
      DESKEW_ADJUST         : string     := "SYSTEM_SYNCHRONOUS";
      DFS_FREQUENCY_MODE    : string     := "LOW";
      DLL_FREQUENCY_MODE    : string     := "LOW";
      DUTY_CYCLE_CORRECTION : boolean    := true;
      FACTORY_JF            : bit_vector := X"c080";
      PHASE_SHIFT           : integer    := 0;
      STARTUP_WAIT          : boolean    := false
      );

    port (
      CLK0     : out std_ulogic;
      CLK180   : out std_ulogic;
      CLK270   : out std_ulogic;
      CLK2X    : out std_ulogic;
      CLK2X180 : out std_ulogic;
      CLK90    : out std_ulogic;
      CLKDV    : out std_ulogic;
      CLKFX    : out std_ulogic;
      CLKFX180 : out std_ulogic;
      LOCKED   : out std_ulogic;
      PSDONE   : out std_ulogic;
      STATUS   : out std_logic_vector(7 downto 0);
      CLKFB    : in  std_ulogic;
      CLKIN    : in  std_ulogic;
      PSCLK    : in  std_ulogic;
      PSEN     : in  std_ulogic;
      PSINCDEC : in  std_ulogic;
      RST      : in  std_ulogic
      );
  end component;

  component IBUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;


  component BUFGCE
    port(
      O  : out std_ulogic;
      CE : in  std_ulogic;
      I  : in  std_ulogic
      );
  end component;

  signal DCM_LOCK_DEL    : std_logic_vector(3 downto 0) := x"0";
  signal DCM_LOCKED      : std_logic;
  signal DCM_RESET_COUNT : unsigned(7 downto 0)         := x"00";
  signal CLK_16_INT      : std_logic;
  signal CLK_IN_INT      : std_logic;
  signal CLK_16_UB       : std_logic;
  signal CLK_32_UB       : std_logic;
  signal CLOCK_RESET     : boolean;
  

begin

  CLK_IN_DCM : DCM_SP
    generic map (
      CLKDV_DIVIDE          => 2.0,
      CLKFX_DIVIDE          => 1,
      CLKFX_MULTIPLY        => 2,
      CLKIN_DIVIDE_BY_2     => false,
      CLKIN_PERIOD          => 62.5,
      CLKOUT_PHASE_SHIFT    => "NONE",
      CLK_FEEDBACK          => "1X",
      DESKEW_ADJUST         => "SYSTEM_SYNCHRONOUS",
      DFS_FREQUENCY_MODE    => "LOW",
      DLL_FREQUENCY_MODE    => "LOW",
      DUTY_CYCLE_CORRECTION => true,
      FACTORY_JF            => X"c080",
      PHASE_SHIFT           => 0,
      STARTUP_WAIT          => false
      )

    port map (
      CLK0     => CLK_16_UB,
      CLK180   => open,
      CLK270   => open,
      CLK2X    => CLK_32_UB,
      CLK2X180 => open,
      CLK90    => open,
      CLKDV    => open,
      CLKFX    => open,
      CLKFX180 => open,
      STATUS   => open,
      LOCKED   => dcm_lockED,
      PSDONE   => open,
      CLKFB    => CLK_16_INT,
      CLKIN    => DCM_CLK,
      PSCLK    => '0',
      PSEN     => '0',
      PSINCDEC => '0',
      RST      => bool2sl(CLOCK_RESET)
      );

  CLK_FB : BUFG
    port map (
      O => CLK_16_INT,
      I => CLK_16_UB
      );

  CLK_INBG : IBUFG
    port map (
      O => CLK_IN_INT,
      I => CLK_IN
      );
  CLK_32_BUF : BUFG
    port map (
      O => DAQ_CLK,
      I => CLK_32_UB
      );

  ASIC_BGCE : BUFGCE
    port map (
      O  => ASIC_CLK,
      CE => BOOL2SL(ASIC_CLK_EN),
      I  => CLK_16_UB
      );
  ADC_BEEBUS_CLK <= CLK_16_INT;

  POWER_UP_RESET : process (DCM_CLK) is
  begin
    if DCM_CLK'event and DCM_CLK = '1' then
      DCM_LOCK_DEL(3 downto 0) <= DCM_LOCK_DEL(2 downto 0)& DCM_LOCKED;
      POWERUP_RESET            <= DCM_LOCK_DEL(3) = '0';
    end if;
  end process POWER_UP_RESET;

  RESET_ON_UNLOCK : process (DCM_CLK) is
  begin
    if DCM_CLK'event and DCM_CLK = '1' then
      if DCM_LOCKED = '1' then
        DCM_RESET_COUNT <= x"FF";
      else
        DCM_RESET_COUNT <= DCM_RESET_COUNT + 1;
      end if;
      CLOCK_RESET <= DCM_RESET_COUNT(7) = '0';
    end if;
  end process RESET_ON_UNLOCK;
      dcm_lo      <= DCM_LOCKED;
      dcm_cr <=  bool2sl(CLOCK_RESET);

  
end architecture struct;

