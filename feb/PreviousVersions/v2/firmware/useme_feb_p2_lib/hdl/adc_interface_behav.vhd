--
-- VHDL Architecture feb_p2_lib.adc_interface.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 11:43:21 01/22/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;
LIBRARY unisim;
USE unisim.all;
library harvard_std;
USE harvard_std.lppc_custom_fn_pkg.all;

ENTITY adc_interface IS
   PORT( 
      D_IN_A        : IN     std_logic_vector (11 DOWNTO 0);
      D_IN_B        : IN     std_logic_vector (11 DOWNTO 0);
      LOCAL_CLK     : IN     std_logic;
      DATA_CH_00_15 : OUT    unsigned (11 DOWNTO 0);
      DATA_CH_16_31 : OUT    unsigned (11 DOWNTO 0);
      ENABLE        : OUT    std_logic_vector (3 DOWNTO 0);
      MODE          : OUT    unsigned (2 DOWNTO 0)
   );

-- Declarations

END adc_interface ;

--
architecture behav of adc_interface is

begin

  ENABLE <= "1111";
  MODE   <= "000";

  process (LOCAL_CLK) is
  begin
    if LOCAL_CLK'event and LOCAL_CLK = '1' then
      DATA_CH_00_15 <= unsigned(not D_IN_A);
      DATA_CH_16_31 <= unsigned(not D_IN_B);
    end if;
  end process;
end architecture behav;

