--
-- VHDL Architecture feb_p2_lib.filter.struct
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 17:07:20 04/ 2/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;
LIBRARY unisim;
USE unisim.all;
library harvard_std;USE harvard_std.lppc_custom_fn_pkg.all;

ENTITY filter IS
   PORT( 
      DATA_CH_00_15       : IN     unsigned (15 DOWNTO 0);
      FILTERED_DATA_15_00 : OUT    unsigned (5 DOWNTO 0)
   );

-- Declarations

END filter ;

--
ARCHITECTURE struct OF filter IS
BEGIN
END ARCHITECTURE struct;

