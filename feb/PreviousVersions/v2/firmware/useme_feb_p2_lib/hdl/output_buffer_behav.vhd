--
-- VHDL Architecture feb_p2_lib.output_buffer.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 11:49:19 03/ 8/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;
LIBRARY unisim;
USE unisim.all;
library harvard_std;
USE harvard_std.lppc_custom_fn_pkg.all;

ENTITY output_buffer IS
   PORT( 
      BEEBUS_ADDR     : IN     unsigned (15 DOWNTO 0);
      BEEBUS_DATA     : INOUT  unsigned (31 DOWNTO 0);
      BEEBUS_CLK      : IN     std_logic;
      BEEBUS_READ     : IN     boolean;
      BEEBUS_STRB     : IN     boolean;
      DAQ_DATA        : IN     unsigned (15 DOWNTO 0);
      DAQ_DATA_STRB   : IN     boolean;
      RESET_DAQ       : IN     boolean;
      BUFFER_READ_RDY : OUT    boolean;
      OVERFLOW_ERROR  : OUT    boolean;
      CLK             : IN     std_logic
   );

-- Declarations

END output_buffer ;

architecture behav of output_buffer is
  type BUFFER_TYPE is array (4095 downto 0)
    of std_logic_vector (15 downto 0);
  type BUFFER_SEG_FULL_TYPE is array (15 downto 0)
    of boolean;
  
  signal ADC_BUFFER            : BUFFER_TYPE;
  signal WRITE_PTR             : unsigned(11 downto 0) := "000000000000";
  signal READ_PTR              : unsigned(11 downto 0) := "000000000000";
  signal fake_data             : unsigned(15 downto 0) := "0000000000000000";
  signal BUFFER_SEG_FULL       : BUFFER_SEG_FULL_TYPE;
  signal BUFFER_SEG_SENT       : BUFFER_SEG_FULL_TYPE;
  signal LAST_BUFFER_SEG_READ  : unsigned(3 downto 0)  := "0000";
  signal LAST_BUFFER_SEG_WRITE : unsigned(3 downto 0)  := "0000";
  signal RESET_READ_PTR        : boolean               := false;
  signal READ_PTR_RESETED      : boolean               := false;

  alias BUFFER_SEG_READ is READ_PTR(11 downto 8);
  alias BUFFER_SEG_WRITE is WRITE_PTR(11 downto 8);
  
begin
  
  BEEBUS_DATA <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";


  process (BEEBUS_CLK)
    variable j : integer;
  begin
    if BEEBUS_CLK'event and BEEBUS_CLK = '1' then
      BEEBUS_DATA     <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
      BUFFER_READ_RDY <= BUFFER_SEG_FULL(TO_INTEGER(BUFFER_SEG_READ));

      if RESET_READ_PTR then
        READ_PTR         <= x"000";
        READ_PTR_RESETED <= true;
        for j in 0 to 15 loop
          BUFFER_SEG_SENT(j) <= false;
        end loop;
        
      else
        READ_PTR_RESETED <= false;

        if BEEBUS_ADDR = x"0000" and BEEBUS_READ then
          BEEBUS_DATA <= x"0000" & unsigned(ADC_BUFFER(TO_INTEGER(READ_PTR)));
          READ_PTR    <= READ_PTR + 1;
        end if;

        for j in 0 to 15 loop
          BUFFER_SEG_SENT(j) <= BUFFER_SEG_SENT(j)and BUFFER_SEG_FULL(j);
        end loop;
        LAST_BUFFER_SEG_READ <= BUFFER_SEG_READ;
        if BUFFER_SEG_READ /= LAST_BUFFER_SEG_READ then
          BUFFER_SEG_SENT(TO_INTEGER(LAST_BUFFER_SEG_READ)) <= true;
        end if;
      end if;
      
    end if;
  end process;

  process (CLK)
    variable i : integer;
  begin
    if CLK 'event and CLK = '1' then
      
      if RESET_DAQ then
        fake_data             <= x"0000";
        WRITE_PTR             <= x"000";
        LAST_BUFFER_SEG_WRITE <= x"0";
        RESET_READ_PTR        <= true;
        OVERFLOW_ERROR        <= false;
        for i in 0 to 15 loop
          BUFFER_SEG_FULL(i) <= false;
        end loop;
        
      else
        RESET_READ_PTR <= RESET_READ_PTR and not READ_PTR_RESETED;

        if DAQ_DATA_STRB then
          if not BUFFER_SEG_FULL(TO_INTEGER(BUFFER_SEG_WRITE)) then
--            ADC_BUFFER(TO_INTEGER(WRITE_PTR)) <= "0000" & std_logic_vector(write_ptr);
            ADC_BUFFER(TO_INTEGER(WRITE_PTR)) <= std_logic_vector(DAQ_DATA);
            WRITE_PTR                         <= WRITE_PTR + 1;
            fake_data                         <= fake_data + 1;
          else
            OVERFLOW_ERROR <= true;
          end if;
        end if;

        for i in 0 to 15 loop
          BUFFER_SEG_FULL(i) <= BUFFER_SEG_FULL(i)and not BUFFER_SEG_SENT(i);
        end loop;
        LAST_BUFFER_SEG_WRITE <= BUFFER_SEG_WRITE;
        if BUFFER_SEG_WRITE /= LAST_BUFFER_SEG_WRITE then
          BUFFER_SEG_FULL(TO_INTEGER(LAST_BUFFER_SEG_WRITE)) <= true;
        end if;
        
      end if;
    end if;
  end process;
  
end architecture behav;


