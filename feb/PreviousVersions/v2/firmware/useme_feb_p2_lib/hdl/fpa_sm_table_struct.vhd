--
-- VHDL Architecture bee_p1_lib.fpa_sm_table.struct
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC4)
--          at - 10:59:11 07/ 5/2006
--
-- using Mentor Graphics HDL Designer(TM) 2005.2 (Build 37)
--
-- hds interface_start
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;
LIBRARY unisim;
USE unisim.all;
library harvard_std;
USE harvard_std.lppc_custom_fn_pkg.all;

--library harvard_std;
ENTITY fpa_sm_table IS
   PORT( 
      BEEBUS_READ : IN     boolean;
      RESET       : IN     boolean;
      BEEBUS_STRB : IN     boolean;
      BEEBUS_CLK  : IN     std_logic;
      BEEBUS_DATA : INOUT  unsigned (31 DOWNTO 0);
      BEEBUS_ADDR : IN     unsigned (15 DOWNTO 0)
   );

-- Declarations

END fpa_sm_table ;
-- hds interface_end

architecture struct of fpa_sm_table is

  type TABLE_TYPE is array (1023 downto 0)
    of std_logic_vector (31 downto 0);
  signal FPA_SM_TABLE      : TABLE_TYPE;
  signal TABLE_INSTRUCTION : unsigned (31 downto 0);
  
begin


  process (BEEBUS_CLK)
  begin
    if BEEBUS_CLK'event and BEEBUS_CLK = '1' then
      BEEBUS_DATA <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
      if (BEEBUS_ADDR >= x"1000") and(BEEBUS_ADDR < x"2000")then
        if BEEBUS_READ then
          BEEBUS_DATA <= unsigned(FPA_SM_TABLE (to_integer(BEEBUS_ADDR(9 downto 0))));
        elsif BEEBUS_STRB then
          FPA_SM_TABLE (to_integer(BEEBUS_ADDR(9 downto 0))) <= std_logic_vector(BEEBUS_DATA);
        end if;
      end if;
    end if;
  end process;
  
end architecture struct;
