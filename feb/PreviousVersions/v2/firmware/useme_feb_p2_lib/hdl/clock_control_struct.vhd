--
-- VHDL Architecture feb_p2_lib.clock_control.struct
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 14:38:14 03/ 9/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library UNISIM;
use UNISIM.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity clock_control is
  port(
    CLK_IN        : in  std_logic;
    ASIC_CLK      : out std_logic;
    ASIC_CLK_EN   : in  boolean;
    ADC_CLK       : out std_logic;
    BEEBUS_CLK    : out std_logic;
    USB_IFCLK     : out std_logic;
    DAQ_CLK       : out std_logic;
    POWERUP_RESET : out boolean
    );

-- Declarations

end clock_control;

--
architecture struct of clock_control is
  component DCM_SP
    generic(
      CLKDV_DIVIDE          : real       := 2.0;
      CLKFX_DIVIDE          : integer    := 1;
      CLKFX_MULTIPLY        : integer    := 2;
      CLKIN_DIVIDE_BY_2     : boolean    := false;
      CLKIN_PERIOD          : real       := 62.5;
      CLKOUT_PHASE_SHIFT    : string     := "NONE";
      CLK_FEEDBACK          : string     := "NONE";
      DESKEW_ADJUST         : string     := "SYSTEM_SYNCHRONOUS";
      DFS_FREQUENCY_MODE    : string     := "LOW";
      DLL_FREQUENCY_MODE    : string     := "LOW";
      DUTY_CYCLE_CORRECTION : boolean    := true;
      FACTORY_JF            : bit_vector := X"c080";
      PHASE_SHIFT           : integer    := 0;
      STARTUP_WAIT          : boolean    := true
      );

    port (
      CLK0     : out std_ulogic;
      CLK180   : out std_ulogic;
      CLK270   : out std_ulogic;
      CLK2X    : out std_ulogic;
      CLK2X180 : out std_ulogic;
      CLK90    : out std_ulogic;
      CLKDV    : out std_ulogic;
      CLKFX    : out std_ulogic;
      CLKFX180 : out std_ulogic;
      LOCKED   : out std_ulogic;
      PSDONE   : out std_ulogic;
      STATUS   : out std_logic_vector(7 downto 0);
      CLKFB    : in  std_ulogic;
      CLKIN    : in  std_ulogic;
      PSCLK    : in  std_ulogic;
      PSEN     : in  std_ulogic;
      PSINCDEC : in  std_ulogic;
      RST      : in  std_ulogic
      );
  end component;

  component IBUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;


  component BUFGCE
    port(
      O  : out std_ulogic;
      CE : in  std_ulogic;
      I  : in  std_ulogic
      );
  end component;

  signal DCM_LOCK         : std_logic_vector(3 downto 0) := x"0";
  signal DCM_DELAY        : std_logic_vector(3 downto 0) := x"0";
  signal CLK_IN_int       : std_logic                    := '1';
  signal CLK_IN_DCM_CLKFX : std_logic                    := '1';
  signal DCM_RESET        : std_logic                    := '1';
  

begin

  CLK_IN_DCM : DCM_SP
    generic map (
      CLKDV_DIVIDE          => 2.0,
      CLKFX_DIVIDE          => 1,
      CLKFX_MULTIPLY        => 2,
      CLKIN_DIVIDE_BY_2     => false,
      CLKIN_PERIOD          => 62.5,
      CLKOUT_PHASE_SHIFT    => "NONE",
      CLK_FEEDBACK          => "NONE",
      DESKEW_ADJUST         => "SYSTEM_SYNCHRONOUS",
      DFS_FREQUENCY_MODE    => "LOW",
      DLL_FREQUENCY_MODE    => "LOW",
      DUTY_CYCLE_CORRECTION => true,
      FACTORY_JF            => X"c080",
      PHASE_SHIFT           => 0,
      STARTUP_WAIT          => true
      )

    port map (
      CLK0     => open,
      CLK180   => open,
      CLK270   => open,
      CLK2X    => open,
      CLK2X180 => open,
      CLK90    => open,
      CLKDV    => open,
      CLKFX    => CLK_IN_DCM_CLKFX,
      CLKFX180 => open,
      STATUS   => open,
      LOCKED   => dcm_lock(0),
      PSDONE   => open,
      CLKFB    => '0',
      CLKIN    => CLK_IN_int,
      PSCLK    => '0',
      PSEN     => '0',
      PSINCDEC => '0',
      RST      => DCM_RESET
      );

  DCM_CLK_IN : IBUFG
    port map (
      O => CLK_IN_int,
      I => CLK_IN
      );

  CLK_FX_BG : BUFG
    port map (
      O => DAQ_CLK,
      I => CLK_IN_DCM_CLKFX
      );

  ASIC_BGCE : BUFGCE
    port map (
      O  => ASIC_CLK,
      CE => BOOL2SL(ASIC_CLK_EN),
      I  => CLK_IN_int
      );


  ADC_CLK    <= CLK_IN_int;
  BEEBUS_CLK <= CLK_IN_int;
  USB_IFCLK  <= CLK_IN_int;

  POWER_UP_RESET : process (CLK_IN_int) is
  begin
    if CLK_IN_int'event and CLK_IN_int = '1' then
      DCM_LOCK(3 downto 1) <= DCM_LOCK(2 downto 0);
    end if;
  end process POWER_UP_RESET;

  POWER_UP_DELAY : process (CLK_IN_int) is
  begin
    if CLK_IN_int'event and CLK_IN_int = '1' then
      DCM_DELAY(3 downto 0) <= DCM_DELAY(2 downto 0) & '1';
    end if;
  end process POWER_UP_DELAY;

  DCM_RESET     <= not DCM_DELAY(3);
  POWERUP_RESET <= DCM_LOCK(3) = '0';
  
end architecture struct;

