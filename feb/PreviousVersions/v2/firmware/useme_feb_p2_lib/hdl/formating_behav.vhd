--
-- VHDL Architecture feb_p2_lib.formating.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 14:35:28 04/ 5/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std; use harvard_std.lppc_custom_fn_pkg.all;

entity formating is
  port(
    MAGNITUDE        : in  signed (13 downto 0);
    TIMESTAMP        : in  unsigned (15 downto 0);
    TRIGGER_STRB     : in  boolean;
    GOT_ANYTHING_NEW : out boolean
    );

-- Declarations

end formating;

--
architecture behav of formating is
begin

  process (clk) is
  begin  -- process
    if clk'event and clk = '1' then     -- rising clock edge
      if < reset name > = '1' then      -- synchronous reset (active high)

      else
        
    end if;
  end if;
end process;
end architecture behav;

