###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_11_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_10_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_9_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_8_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_7_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_6_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_5_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_4_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_3_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_2_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_1_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_0_u -design rtl 

set_attribute -name iostandard -value "DEFAULT" -instance -type string I12/u -design rtl 

set_attribute -name iostandard -value "DEFAULT" -instance -type string I13/u -design rtl 

set_attribute -name CLKDV_DIVIDE -value "2.0" -instance -type default U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKFX_DIVIDE -value "1" -instance -type integer U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKFX_MULTIPLY -value "2" -instance -type integer U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKIN_DIVIDE_BY_2 -value "0" -instance -type default U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKIN_PERIOD -value "62.5" -instance -type default U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKOUT_PHASE_SHIFT -value "NONE" -instance -type string U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLK_FEEDBACK -value "NONE" -instance -type string U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name DESKEW_ADJUST -value "SYSTEM_SYNCHRONOUS" -instance -type string U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name DFS_FREQUENCY_MODE -value "LOW" -instance -type string U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name DLL_FREQUENCY_MODE -value "LOW" -instance -type string U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name DUTY_CYCLE_CORRECTION -value "1" -instance -type default U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name FACTORY_JF -value "c080" -instance -type default U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name PHASE_SHIFT -value "0" -instance -type integer U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name STARTUP_WAIT -value "1" -instance -type default U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name ram_processed -value "true" -instance U_2/U_1/PEDESTAL_TABLE/PEDESTAL_TABLE -design rtl 

set_attribute -name ram_processed -value "true" -instance U_2/U_1/THRESHOLD_TABLE/THRESHOLD_TABLE -design rtl 



set_attribute -name ram_processed -value "true" -instance U_0/ADC_BUFFER/ADC_BUFFER -design rtl 


set_attribute -name state_vector -value "current_state" -type string /feb_p2_lib/usb_translator_notri/fsm_unfold_737_XRTL -design rtl 

##################
# Clocks
##################
create_clock { U_5/U_3/CLK_IN_DCM/CLKFX } -domain ClockDomain0 -name U_5/U_3/CLK_IN_DCM/CLKFX -period 31.200000 -waveform { 0.000000 15.600000 } -design rtl 
create_clock { CLK_IN } -domain ClockDomain0 -name CLK_IN -period 31.200000 -waveform { 0.000000 15.600000 } -design rtl 

