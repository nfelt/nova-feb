###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name NOPAD -value "TRUE" -port CLK_IN -design gatelevel 
set_attribute -name CLKDV_DIVIDE -value "2.0" -instance -type default U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKFX_DIVIDE -value "1" -instance -type integer U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKFX_MULTIPLY -value "2" -instance -type integer U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKIN_DIVIDE_BY_2 -value "0" -instance -type default U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKIN_PERIOD -value "62.5" -instance -type default U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKOUT_PHASE_SHIFT -value "NONE" -instance -type string U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLK_FEEDBACK -value "NONE" -instance -type string U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name DESKEW_ADJUST -value "SYSTEM_SYNCHRONOUS" -instance -type string U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name DFS_FREQUENCY_MODE -value "LOW" -instance -type string U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name DLL_FREQUENCY_MODE -value "LOW" -instance -type string U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name DUTY_CYCLE_CORRECTION -value "1" -instance -type default U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name FACTORY_JF -value "c080" -instance -type default U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name PHASE_SHIFT -value "0" -instance -type integer U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name STARTUP_WAIT -value "1" -instance -type default U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string reg_DCM_LOCK(3) -design gatelevel 
set_attribute -name PART -value "3S1600EFG320-4" -type string /feb_p2_lib/controller/struct -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(22) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(21) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(20) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(19) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(18) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(17) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(16) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(15) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(14) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(13) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(12) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(11) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(10) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(9) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(8) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(7) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(6) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(5) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(4) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(3) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(2) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(1) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(0) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_ASIC_SHIFT_REGISTER(0)_repl0 -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_ASIC_SRCK_int_repl0 -design gatelevel 

set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_EXTERNAL_TRIGGER -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(31) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(30) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(29) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(28) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(27) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(26) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(25) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(24) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(23) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(22) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(21) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(20) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(19) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(18) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(17) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(16) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(15) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(14) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(13) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(12) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(11) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(10) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(9) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(8) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(7) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(6) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(5) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(4) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(3) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(2) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(1) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(0) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(31) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(30) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(29) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(28) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(27) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(26) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(25) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(24) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(23) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(22) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(21) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(20) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(19) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(18) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(17) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(16) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(15) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(14) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(13) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(12) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(11) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(10) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(9) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(8) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(7) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(6) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(5) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(4) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(3) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(2) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(1) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_COMMAND_REGISTER(0) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_CONTOLLER_STATE(1)_repl2 -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0/reg_RESET_DAQ_int_repl0 -design gatelevel 



##################
# Clocks
##################
create_clock { CLK_IN } -domain ClockDomain0 -name CLK_IN -period 31.200000 -waveform { 0.000000 15.600000 } -design gatelevel 

