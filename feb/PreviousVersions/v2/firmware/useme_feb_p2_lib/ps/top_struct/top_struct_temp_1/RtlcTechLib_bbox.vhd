
-- 
-- Definition of  IBUFDS
-- 
--      09/07/07 15:31:50
--      
--      Precision Physical Synthesis, 2006a3.24
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity IBUFDS is
   generic (CAPACITANCE : string := "DONT_CARE"
      ;
      DIFF_TERM : boolean := FALSE;
      IBUF_DELAY_VALUE : string := "0";
      IFD_DELAY_VALUE : string := "AUTO";
      IOSTANDARD : string := "DEFAULT") ;
   
   port (
      O : OUT std_logic ;
      I : IN std_logic ;
      IB : IN std_logic) ;attribute RTLC_TECH_CELL: boolean;
   attribute RTLC_TECH_CELL of 
      IBUFDS : entity is true;
      end IBUFDS ;

architecture NETLIST of IBUFDS is
begin
end NETLIST ;


-- 
-- Definition of  DCM_SP
-- 
--      09/07/07 15:31:50
--      
--      Precision Physical Synthesis, 2006a3.24
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity DCM_SP is
   generic (CLKDV_DIVIDE : real := 2.0;
      CLKFX_DIVIDE : integer := 1;
      CLKFX_MULTIPLY : integer := 4;
      CLKIN_DIVIDE_BY_2 : boolean := FALSE;
      CLKIN_PERIOD : real := 10.0;
      CLKOUT_PHASE_SHIFT : string := "NONE";
      CLK_FEEDBACK : string := "1X"
      ;
      DESKEW_ADJUST : string := "SYSTEM_SYNCHRONOUS"
      ;
      DFS_FREQUENCY_MODE : string := "LOW"
      ;
      DLL_FREQUENCY_MODE : string := "LOW";
      DSS_MODE : string := "NONE";
      DUTY_CYCLE_CORRECTION : boolean := TRUE
      ;
      FACTORY_JF : bit_vector := X"C080";
      PHASE_SHIFT : integer := 0;
      STARTUP_WAIT : boolean := FALSE) ;
   
   port (
      CLK0 : OUT std_logic ;
      CLK180 : OUT std_logic ;
      CLK270 : OUT std_logic ;
      CLK2X : OUT std_logic ;
      CLK2X180 : OUT std_logic ;
      CLK90 : OUT std_logic ;
      CLKDV : OUT std_logic ;
      CLKFX : OUT std_logic ;
      CLKFX180 : OUT std_logic ;
      LOCKED : OUT std_logic ;
      PSDONE : OUT std_logic ;
      STATUS : OUT std_logic_vector (7 DOWNTO 0) ;
      CLKFB : IN std_logic ;
      CLKIN : IN std_logic ;
      DSSEN : IN std_logic ;
      PSCLK : IN std_logic ;
      PSEN : IN std_logic ;
      PSINCDEC : IN std_logic ;
      RST : IN std_logic) ;attribute RTLC_TECH_CELL: boolean;
   attribute RTLC_TECH_CELL of 
      DCM_SP : entity is true;
      end DCM_SP ;

architecture NETLIST of DCM_SP is
begin
end NETLIST ;


-- 
-- Definition of  BUFGCE
-- 
--      09/07/07 15:31:50
--      
--      Precision Physical Synthesis, 2006a3.24
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity BUFGCE is
   port (
      O : OUT std_logic ;
      CE : IN std_logic ;
      I : IN std_logic) ;attribute RTLC_TECH_CELL: boolean;
   attribute RTLC_TECH_CELL of 
      BUFGCE : entity is true;
      end BUFGCE ;

architecture NETLIST of BUFGCE is
begin
end NETLIST ;


-- 
-- Definition of  IBUFG
-- 
--      09/07/07 15:31:50
--      
--      Precision Physical Synthesis, 2006a3.24
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity IBUFG is
   generic (CAPACITANCE : string := "DONT_CARE"
      ;
      IBUF_DELAY_VALUE : string := "0";
      IOSTANDARD : string := "DEFAULT") ;
   
   port (
      O : OUT std_logic ;
      I : IN std_logic) ;attribute RTLC_TECH_CELL: boolean;
   attribute RTLC_TECH_CELL of 
      IBUFG : entity is true;
      end IBUFG ;

architecture NETLIST of IBUFG is
begin
end NETLIST ;


-- 
-- Definition of  OBUFDS
-- 
--      09/07/07 15:31:50
--      
--      Precision Physical Synthesis, 2006a3.24
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity OBUFDS is
   generic (CAPACITANCE : string := "DONT_CARE"
      ;
      IOSTANDARD : string := "DEFAULT") ;
   
   port (
      O : OUT std_logic ;
      OB : OUT std_logic ;
      I : IN std_logic) ;attribute RTLC_TECH_CELL: boolean;
   attribute RTLC_TECH_CELL of 
      OBUFDS : entity is true;
      end OBUFDS ;

architecture NETLIST of OBUFDS is
begin
end NETLIST ;


-- 
-- Definition of  BUFG
-- 
--      09/07/07 15:31:50
--      
--      Precision Physical Synthesis, 2006a3.24
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity BUFG is
   port (
      O : OUT std_logic ;
      I : IN std_logic) ;attribute RTLC_TECH_CELL: boolean;
   attribute RTLC_TECH_CELL of 
      BUFG : entity is true;
      end BUFG ;

architecture NETLIST of BUFG is
begin
end NETLIST ;

