onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic /top/usb_slwr
add wave -noupdate -format Literal -radix hexadecimal /top/usb_fd
add wave -noupdate -expand -group BEEBUS
add wave -noupdate -group BEEBUS -format Literal -radix hexadecimal /top/beebus_addr
add wave -noupdate -group BEEBUS -format Logic /top/beebus_clk
add wave -noupdate -group BEEBUS -format Literal -radix hexadecimal /top/beebus_data
add wave -noupdate -group BEEBUS -format Logic /top/beebus_read
add wave -noupdate -group BEEBUS -format Logic /top/beebus_strb
add wave -noupdate -format Logic /top/powerup_reset
add wave -noupdate -format Logic /top/reset_daq
add wave -noupdate -format Logic /top/u_1/local_clk
add wave -noupdate -format Logic /top/u_5/u_3/asic_clk_en
add wave -noupdate -format Logic /top/usb_reset_b
add wave -noupdate -format Logic /top/enable_daq
add wave -noupdate -format Logic /top/daq_clk
add wave -noupdate -format Logic /top/adc_clk
add wave -noupdate -format Logic /top/asic_clk
add wave -noupdate -color Aquamarine -format Logic -height 22 /top/fmat_data_strb
add wave -noupdate -color Aquamarine -format Literal -height 22 -radix hexadecimal /top/fmat_data
add wave -noupdate -format Literal /top/data_ch_00_15
add wave -noupdate -format Logic /top/u_2/u_1/got_anything_new
add wave -noupdate -format Logic /top/u_2/u_1/data_for_you
add wave -noupdate -format Literal /top/u_2/u_0/fmat_state
add wave -noupdate -format Literal /top/u_2/u_1/chan_count
add wave -noupdate -format Literal -expand /top/u_2/u_1/chan_data
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {755361440 ps} 0} {{Cursor 2} {426670323 ps} 0} {{Cursor 3} {1972950000 ps} 0} {{Cursor 4} {2792512691 ps} 0}
configure wave -namecolwidth 281
configure wave -valuecolwidth 106
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
update
WaveRestoreZoom {610074268 ps} {610646148 ps}
