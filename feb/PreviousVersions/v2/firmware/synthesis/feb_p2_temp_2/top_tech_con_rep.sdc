###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(11) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(10) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(9) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(8) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(7) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(6) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(5) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(4) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(3) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(2) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(1) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(0) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(11) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(10) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(9) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(8) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(7) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(6) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(5) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(4) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(3) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(2) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(1) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(0) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_CLKINN -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_CLKINP -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_OUTCLK_N -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_OUTCLK_P -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_11_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_10_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_9_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_8_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_7_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_6_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_5_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_4_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_3_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_2_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_1_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_0_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I12_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I13_u -design gatelevel 
set_attribute -name PART -value "3S1600EFG320-4" -type string /work/top/struct -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/icon/view_1_precision_icon -design gatelevel 
set_attribute -name COREGEN -value "TRUE" /test_lib/icon/view_1_precision_icon -design gatelevel 

set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/vio/view_1_precision_vio -design gatelevel 
set_attribute -name COREGEN -value "TRUE" /test_lib/vio/view_1_precision_vio -design gatelevel 

set_attribute -name WRITE_MODE -value "READ_FIRST" -instance U_4_FPA_SM_TABLE/ix45489z7749 -design gatelevel 
set_attribute -name WRITE_MODE -value "READ_FIRST" -instance U_4_FPA_SM_TABLE/ix61445z7749 -design gatelevel 

set_attribute -name state_vector -value "current_state" /work/usb_translator_notri/fsm_unfold_1367 -design gatelevel 

##################
# Clocks
##################
create_clock { CLK_IN } -domain ClockDomain0 -name CLK_IN -period 5.000000 -waveform { 0.000000 2.500000 } -design gatelevel 

