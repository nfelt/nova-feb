###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/icon/view_1_precision_icon_XRTL -design rtl 
set_attribute -name COREGEN -value "TRUE" /test_lib/icon/view_1_precision_icon_XRTL -design rtl 

set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/vio/view_1_precision_vio_XRTL -design rtl 
set_attribute -name COREGEN -value "TRUE" /test_lib/vio/view_1_precision_vio_XRTL -design rtl 

set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_11_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_10_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_9_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_8_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_7_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_6_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_5_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_4_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_3_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_2_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_1_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_0_u -design rtl 

set_attribute -name iostandard -value "DEFAULT" -instance -type string I12/u -design rtl 

set_attribute -name iostandard -value "DEFAULT" -instance -type string I13/u -design rtl 

set_attribute -name ram_processed -value "true" -instance U_4/FPA_SM_TABLE/FPA_SM_TABLE -design rtl 


set_attribute -name state_vector -value "current_state" -type string /work/usb_translator_notri/fsm_unfold_1367_XRTL -design rtl 

##################
# Clocks
##################
create_clock { CLK_IN } -domain ClockDomain0 -name CLK_IN -period 5.000000 -waveform { 0.000000 2.500000 } -design rtl 

