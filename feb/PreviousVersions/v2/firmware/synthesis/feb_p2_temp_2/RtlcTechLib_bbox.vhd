
-- 
-- Definition of  IBUFDS
-- 
--      02/15/07 17:23:35
--      
--      Precision RTL Synthesis, 2006a.101
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity IBUFDS is
   generic (CAPACITANCE : string;
      DIFF_TERM : boolean;
      IBUF_DELAY_VALUE : string;
      IFD_DELAY_VALUE : string;
      IOSTANDARD : string) ;
   
   port (
      O : OUT std_logic ;
      I : IN std_logic ;
      IB : IN std_logic) ;attribute RTLC_TECH_CELL: boolean;
   attribute RTLC_TECH_CELL of 
      IBUFDS : entity is true;
      end IBUFDS ;

architecture NETLIST of IBUFDS is
begin
end NETLIST ;


-- 
-- Definition of  IBUFGDS
-- 
--      02/15/07 17:23:35
--      
--      Precision RTL Synthesis, 2006a.101
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity IBUFGDS is
   generic (CAPACITANCE : string;
      DIFF_TERM : boolean;
      IBUF_DELAY_VALUE : string;
      IOSTANDARD : string) ;
   
   port (
      O : OUT std_logic ;
      I : IN std_logic ;
      IB : IN std_logic) ;attribute RTLC_TECH_CELL: boolean;
   attribute RTLC_TECH_CELL of 
      IBUFGDS : entity is true;
      end IBUFGDS ;

architecture NETLIST of IBUFGDS is
begin
end NETLIST ;


-- 
-- Definition of  OBUFDS
-- 
--      02/15/07 17:23:35
--      
--      Precision RTL Synthesis, 2006a.101
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity OBUFDS is
   generic (CAPACITANCE : string;
      IOSTANDARD : string) ;
   
   port (
      O : OUT std_logic ;
      OB : OUT std_logic ;
      I : IN std_logic) ;attribute RTLC_TECH_CELL: boolean;
   attribute RTLC_TECH_CELL of 
      OBUFDS : entity is true;
      end OBUFDS ;

architecture NETLIST of OBUFDS is
begin
end NETLIST ;

