##
##  Copyright (c) Mentor Graphics Corporation, 1996-2004, All Rights Reserved.
##             Portions copyright 1991-2004 Compuware Corporation
##                       UNPUBLISHED, LICENSED SOFTWARE.
##            CONFIDENTIAL AND PROPRIETARY INFORMATION WHICH IS THE
##          PROPERTY OF MENTOR GRAPHICS CORPORATION OR ITS LICENSORS
#
# File created on: 12/12/07 12:21:17
#

#
# Boundary Constraints
#
puts "Creating boundary constraints. . ."

#
# Attributes
#
puts "Setting attributes. . ."
if [catch {set_attribute {ADC_CLKINN} -port -name {LOC} -value {t8}} result]\
	{puts {Warning: set_attribute LOC on ADC_CLKINN failed}}
if [catch {set_attribute {ADC_CLKINN} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_CLKINN failed}}
if [catch {set_attribute {ADC_CLKINP} -port -name {LOC} -value {R8}} result]\
	{puts {Warning: set_attribute LOC on ADC_CLKINP failed}}
if [catch {set_attribute {ADC_CLKINP} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_CLKINP failed}}
if [catch {set_attribute {ADC_ENABLE(0)} -port -name {LOC} -value {a4}} result]\
	{puts {Warning: set_attribute LOC on ADC_ENABLE(0) failed}}
if [catch {set_attribute {ADC_ENABLE(0)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_ENABLE(0) failed}}
if [catch {set_attribute {ADC_ENABLE(1)} -port -name {LOC} -value {a6}} result]\
	{puts {Warning: set_attribute LOC on ADC_ENABLE(1) failed}}
if [catch {set_attribute {ADC_ENABLE(1)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_ENABLE(1) failed}}
if [catch {set_attribute {ADC_ENABLE(2)} -port -name {LOC} -value {b4}} result]\
	{puts {Warning: set_attribute LOC on ADC_ENABLE(2) failed}}
if [catch {set_attribute {ADC_ENABLE(2)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_ENABLE(2) failed}}
if [catch {set_attribute {ADC_ENABLE(3)} -port -name {LOC} -value {b6}} result]\
	{puts {Warning: set_attribute LOC on ADC_ENABLE(3) failed}}
if [catch {set_attribute {ADC_ENABLE(3)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_ENABLE(3) failed}}
if [catch {set_attribute {ADC_MODE(0)} -port -name {LOC} -value {u5}} result]\
	{puts {Warning: set_attribute LOC on ADC_MODE(0) failed}}
if [catch {set_attribute {ADC_MODE(0)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_MODE(0) failed}}
if [catch {set_attribute {ADC_MODE(1)} -port -name {LOC} -value {v6}} result]\
	{puts {Warning: set_attribute LOC on ADC_MODE(1) failed}}
if [catch {set_attribute {ADC_MODE(1)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_MODE(1) failed}}
if [catch {set_attribute {ADC_MODE(2)} -port -name {LOC} -value {v7}} result]\
	{puts {Warning: set_attribute LOC on ADC_MODE(2) failed}}
if [catch {set_attribute {ADC_MODE(2)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_MODE(2) failed}}
if [catch {set_attribute {ASIC_CHIPRESET} -port -name {LOC} -value {n7}} result]\
	{puts {Warning: set_attribute LOC on ASIC_CHIPRESET failed}}
if [catch {set_attribute {ASIC_CHIPRESET} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ASIC_CHIPRESET failed}}
if [catch {set_attribute {ASIC_INTEGRST} -port -name {LOC} -value {p6}} result]\
	{puts {Warning: set_attribute LOC on ASIC_INTEGRST failed}}
if [catch {set_attribute {ASIC_INTEGRST} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ASIC_INTEGRST failed}}
if [catch {set_attribute {ASIC_OUTCLK_N} -port -name {LOC} -value {t4}} result]\
	{puts {Warning: set_attribute LOC on ASIC_OUTCLK_N failed}}
if [catch {set_attribute {ASIC_OUTCLK_N} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ASIC_OUTCLK_N failed}}
if [catch {set_attribute {ASIC_OUTCLK_P} -port -name {LOC} -value {U4}} result]\
	{puts {Warning: set_attribute LOC on ASIC_OUTCLK_P failed}}
if [catch {set_attribute {ASIC_OUTCLK_P} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ASIC_OUTCLK_P failed}}
if [catch {set_attribute {ASIC_SHAPERRST} -port -name {LOC} -value {r6}} result]\
	{puts {Warning: set_attribute LOC on ASIC_SHAPERRST failed}}
if [catch {set_attribute {ASIC_SHAPERRST} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ASIC_SHAPERRST failed}}
if [catch {set_attribute {ASIC_SHIFTIN} -port -name {LOC} -value {n8}} result]\
	{puts {Warning: set_attribute LOC on ASIC_SHIFTIN failed}}
if [catch {set_attribute {ASIC_SHIFTIN} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ASIC_SHIFTIN failed}}
if [catch {set_attribute {ASIC_SHIFTOUT} -port -name {LOC} -value {p8}} result]\
	{puts {Warning: set_attribute LOC on ASIC_SHIFTOUT failed}}
if [catch {set_attribute {ASIC_SHIFTOUT} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ASIC_SHIFTOUT failed}}
if [catch {set_attribute {ASIC_SRCK} -port -name {LOC} -value {p7}} result]\
	{puts {Warning: set_attribute LOC on ASIC_SRCK failed}}
if [catch {set_attribute {ASIC_SRCK} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ASIC_SRCK failed}}
if [catch {set_attribute {ASIC_TESTINJECT} -port -name {LOC} -value {r5}} result]\
	{puts {Warning: set_attribute LOC on ASIC_TESTINJECT failed}}
if [catch {set_attribute {ASIC_TESTINJECT} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ASIC_TESTINJECT failed}}
if [catch {set_attribute {CLK_IN} -port -name {LOC} -value {B10}} result]\
	{puts {Warning: set_attribute LOC on CLK_IN failed}}
if [catch {set_attribute {CLK_IN} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on CLK_IN failed}}
if [catch {set_attribute {DCM_CLK_N} -port -name {LOC} -value {E10}} result]\
	{puts {Warning: set_attribute LOC on DCM_CLK_N failed}}
if [catch {set_attribute {DCM_CLK_N} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on DCM_CLK_N failed}}
if [catch {set_attribute {DCM_CLK_P} -port -name {LOC} -value {D10}} result]\
	{puts {Warning: set_attribute LOC on DCM_CLK_P failed}}
if [catch {set_attribute {DCM_CLK_P} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on DCM_CLK_P failed}}
if [catch {set_attribute {DCM_EMU_CLK_N} -port -name {LOC} -value {F9}} result]\
	{puts {Warning: set_attribute LOC on DCM_EMU_CLK_N failed}}
if [catch {set_attribute {DCM_EMU_CLK_N} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on DCM_EMU_CLK_N failed}}
if [catch {set_attribute {DCM_EMU_CLK_P} -port -name {LOC} -value {E9}} result]\
	{puts {Warning: set_attribute LOC on DCM_EMU_CLK_P failed}}
if [catch {set_attribute {DCM_EMU_CLK_P} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on DCM_EMU_CLK_P failed}}
if [catch {set_attribute {LA(0)} -port -name {LOC} -value {a13}} result]\
	{puts {Warning: set_attribute LOC on LA(0) failed}}
if [catch {set_attribute {LA(0)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(0) failed}}
if [catch {set_attribute {LA(1)} -port -name {LOC} -value {b13}} result]\
	{puts {Warning: set_attribute LOC on LA(1) failed}}
if [catch {set_attribute {LA(1)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(1) failed}}
if [catch {set_attribute {LA(2)} -port -name {LOC} -value {a14}} result]\
	{puts {Warning: set_attribute LOC on LA(2) failed}}
if [catch {set_attribute {LA(2)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(2) failed}}
if [catch {set_attribute {LA(3)} -port -name {LOC} -value {B14}} result]\
	{puts {Warning: set_attribute LOC on LA(3) failed}}
if [catch {set_attribute {LA(3)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(3) failed}}
if [catch {set_attribute {LA(4)} -port -name {LOC} -value {a16}} result]\
	{puts {Warning: set_attribute LOC on LA(4) failed}}
if [catch {set_attribute {LA(4)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(4) failed}}
if [catch {set_attribute {LA(5)} -port -name {LOC} -value {b11}} result]\
	{puts {Warning: set_attribute LOC on LA(5) failed}}
if [catch {set_attribute {LA(5)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(5) failed}}
if [catch {set_attribute {LA(6)} -port -name {LOC} -value {a11}} result]\
	{puts {Warning: set_attribute LOC on LA(6) failed}}
if [catch {set_attribute {LA(6)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(6) failed}}
if [catch {set_attribute {LA(7)} -port -name {LOC} -value {a8}} result]\
	{puts {Warning: set_attribute LOC on LA(7) failed}}
if [catch {set_attribute {LA(7)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(7) failed}}
if [catch {set_attribute {LA(8)} -port -name {LOC} -value {a7}} result]\
	{puts {Warning: set_attribute LOC on LA(8) failed}}
if [catch {set_attribute {LA(8)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(8) failed}}
if [catch {set_attribute {SER_NUM_CS_B} -port -name {LOC} -value {p13}} result]\
	{puts {Warning: set_attribute LOC on SER_NUM_CS_B failed}}
if [catch {set_attribute {SER_NUM_CS_B} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on SER_NUM_CS_B failed}}
if [catch {set_attribute {SPI_DIN} -port -name {LOC} -value {v12}} result]\
	{puts {Warning: set_attribute LOC on SPI_DIN failed}}
if [catch {set_attribute {SPI_DIN} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on SPI_DIN failed}}
if [catch {set_attribute {SPI_SCLK} -port -name {LOC} -value {p12}} result]\
	{puts {Warning: set_attribute LOC on SPI_SCLK failed}}
if [catch {set_attribute {SPI_SCLK} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on SPI_SCLK failed}}
if [catch {set_attribute {TEC_ADC_CS_B} -port -name {LOC} -value {t12}} result]\
	{puts {Warning: set_attribute LOC on TEC_ADC_CS_B failed}}
if [catch {set_attribute {TEC_ADC_CS_B} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on TEC_ADC_CS_B failed}}
if [catch {set_attribute {TEC_DAC_LDAC_B} -port -name {LOC} -value {r13}} result]\
	{puts {Warning: set_attribute LOC on TEC_DAC_LDAC_B failed}}
if [catch {set_attribute {TEC_DAC_LDAC_B} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on TEC_DAC_LDAC_B failed}}
if [catch {set_attribute {TEC_DAC_SYNC_B} -port -name {LOC} -value {n12}} result]\
	{puts {Warning: set_attribute LOC on TEC_DAC_SYNC_B failed}}
if [catch {set_attribute {TEC_DAC_SYNC_B} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on TEC_DAC_SYNC_B failed}}
if [catch {set_attribute {TEC_ENABLE} -port -name {LOC} -value {t16}} result]\
	{puts {Warning: set_attribute LOC on TEC_ENABLE failed}}
if [catch {set_attribute {TEC_ENABLE} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on TEC_ENABLE failed}}
if [catch {set_attribute {TEMP_SENSOR_CS_B} -port -name {LOC} -value {r14}} result]\
	{puts {Warning: set_attribute LOC on TEMP_SENSOR_CS_B failed}}
if [catch {set_attribute {TEMP_SENSOR_CS_B} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on TEMP_SENSOR_CS_B failed}}
if [catch {set_attribute {USB_FD(0)} -port -name {LOC} -value {e15}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(0) failed}}
if [catch {set_attribute {USB_FD(0)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(0) failed}}
if [catch {set_attribute {USB_FD(1)} -port -name {LOC} -value {e16}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(1) failed}}
if [catch {set_attribute {USB_FD(1)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(1) failed}}
if [catch {set_attribute {USB_FD(10)} -port -name {LOC} -value {m15}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(10) failed}}
if [catch {set_attribute {USB_FD(10)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(10) failed}}
if [catch {set_attribute {USB_FD(11)} -port -name {LOC} -value {m14}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(11) failed}}
if [catch {set_attribute {USB_FD(11)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(11) failed}}
if [catch {set_attribute {USB_FD(12)} -port -name {LOC} -value {n18}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(12) failed}}
if [catch {set_attribute {USB_FD(12)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(12) failed}}
if [catch {set_attribute {USB_FD(13)} -port -name {LOC} -value {n15}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(13) failed}}
if [catch {set_attribute {USB_FD(13)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(13) failed}}
if [catch {set_attribute {USB_FD(14)} -port -name {LOC} -value {p18}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(14) failed}}
if [catch {set_attribute {USB_FD(14)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(14) failed}}
if [catch {set_attribute {USB_FD(15)} -port -name {LOC} -value {p17}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(15) failed}}
if [catch {set_attribute {USB_FD(15)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(15) failed}}
if [catch {set_attribute {USB_FD(2)} -port -name {LOC} -value {f14}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(2) failed}}
if [catch {set_attribute {USB_FD(2)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(2) failed}}
if [catch {set_attribute {USB_FD(3)} -port -name {LOC} -value {f15}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(3) failed}}
if [catch {set_attribute {USB_FD(3)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(3) failed}}
if [catch {set_attribute {USB_FD(4)} -port -name {LOC} -value {f17}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(4) failed}}
if [catch {set_attribute {USB_FD(4)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(4) failed}}
if [catch {set_attribute {USB_FD(5)} -port -name {LOC} -value {f18}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(5) failed}}
if [catch {set_attribute {USB_FD(5)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(5) failed}}
if [catch {set_attribute {USB_FD(6)} -port -name {LOC} -value {g15}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(6) failed}}
if [catch {set_attribute {USB_FD(6)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(6) failed}}
if [catch {set_attribute {USB_FD(7)} -port -name {LOC} -value {g16}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(7) failed}}
if [catch {set_attribute {USB_FD(7)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(7) failed}}
if [catch {set_attribute {USB_FD(8)} -port -name {LOC} -value {m18}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(8) failed}}
if [catch {set_attribute {USB_FD(8)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(8) failed}}
if [catch {set_attribute {USB_FD(9)} -port -name {LOC} -value {m16}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(9) failed}}
if [catch {set_attribute {USB_FD(9)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(9) failed}}
if [catch {set_attribute {USB_FIFOADR(0)} -port -name {LOC} -value {K15}} result]\
	{puts {Warning: set_attribute LOC on USB_FIFOADR(0) failed}}
if [catch {set_attribute {USB_FIFOADR(0)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FIFOADR(0) failed}}
if [catch {set_attribute {USB_FIFOADR(1)} -port -name {LOC} -value {K14}} result]\
	{puts {Warning: set_attribute LOC on USB_FIFOADR(1) failed}}
if [catch {set_attribute {USB_FIFOADR(1)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FIFOADR(1) failed}}
if [catch {set_attribute {USB_FLAGB} -port -name {LOC} -value {H18}} result]\
	{puts {Warning: set_attribute LOC on USB_FLAGB failed}}
if [catch {set_attribute {USB_FLAGB} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FLAGB failed}}
if [catch {set_attribute {USB_FLAGC} -port -name {LOC} -value {H17}} result]\
	{puts {Warning: set_attribute LOC on USB_FLAGC failed}}
if [catch {set_attribute {USB_FLAGC} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FLAGC failed}}
if [catch {set_attribute {USB_IFCLK} -port -name {LOC} -value {J14}} result]\
	{puts {Warning: set_attribute LOC on USB_IFCLK failed}}
if [catch {set_attribute {USB_IFCLK} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_IFCLK failed}}
if [catch {set_attribute {USB_PKTEND} -port -name {LOC} -value {L18}} result]\
	{puts {Warning: set_attribute LOC on USB_PKTEND failed}}
if [catch {set_attribute {USB_PKTEND} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_PKTEND failed}}
if [catch {set_attribute {USB_RESET_B} -port -name {LOC} -value {L16}} result]\
	{puts {Warning: set_attribute LOC on USB_RESET_B failed}}
if [catch {set_attribute {USB_RESET_B} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_RESET_B failed}}
if [catch {set_attribute {USB_SCL} -port -name {LOC} -value {C18}} result]\
	{puts {Warning: set_attribute LOC on USB_SCL failed}}
if [catch {set_attribute {USB_SCL} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_SCL failed}}
if [catch {set_attribute {USB_SDA} -port -name {LOC} -value {D17}} result]\
	{puts {Warning: set_attribute LOC on USB_SDA failed}}
if [catch {set_attribute {USB_SDA} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_SDA failed}}
if [catch {set_attribute {USB_SLOE} -port -name {LOC} -value {J15}} result]\
	{puts {Warning: set_attribute LOC on USB_SLOE failed}}
if [catch {set_attribute {USB_SLOE} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_SLOE failed}}
if [catch {set_attribute {USB_SLRD} -port -name {LOC} -value {T18}} result]\
	{puts {Warning: set_attribute LOC on USB_SLRD failed}}
if [catch {set_attribute {USB_SLRD} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_SLRD failed}}
if [catch {set_attribute {USB_SLWR} -port -name {LOC} -value {T17}} result]\
	{puts {Warning: set_attribute LOC on USB_SLWR failed}}
if [catch {set_attribute {USB_SLWR} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_SLWR failed}}
if [catch {set_attribute {USB_WAKEUP_B} -port -name {LOC} -value {L15}} result]\
	{puts {Warning: set_attribute LOC on USB_WAKEUP_B failed}}
if [catch {set_attribute {USB_WAKEUP_B} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_WAKEUP_B failed}}
if [catch {set_attribute {USB_WU2} -port -name {LOC} -value {D16}} result]\
	{puts {Warning: set_attribute LOC on USB_WU2 failed}}
if [catch {set_attribute {USB_WU2} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_WU2 failed}}
