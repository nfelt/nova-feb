###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/icon/view_1_precision_icon_XRTL -design rtl 
set_attribute -name COREGEN -value "TRUE" /test_lib/icon/view_1_precision_icon_XRTL -design rtl 

set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/vio/view_1_precision_vio_XRTL -design rtl 
set_attribute -name COREGEN -value "TRUE" /test_lib/vio/view_1_precision_vio_XRTL -design rtl 

set_attribute -name iostandard -value "DEFAULT" -instance -type string I12/u -design rtl 

set_attribute -name iostandard -value "DEFAULT" -instance -type string I13/u -design rtl 

set_attribute -name ram_processed -value "true" -instance U_4/FPA_SM_TABLE/FPA_SM_TABLE -design rtl 


set_attribute -name state_vector -value "current_state" -type string /work/usb_translator_notri/fsm_unfold_1367_XRTL -design rtl 

##################
# Clocks
##################
create_clock { U_2/vio/i_vio/gen_async_out/0/async_out_cell/user_reg/Q } -domain ClockDomain3 -name U_2/vio/i_vio/gen_async_out/0/async_out_cell/user_reg/Q -period 5.000000 -waveform { 0.000000 2.500000 } -design rtl 
create_clock { U_0/I0/BEEBUS_READ_or_0/d } -domain ClockDomain4 -name U_0/I0/BEEBUS_READ_or_0/d -period 5.000000 -waveform { 0.000000 2.500000 } -design rtl 
create_clock { U_0/I0/reg_BEEBUS_STRB_cld/out } -domain ClockDomain2 -name U_0/I0/reg_BEEBUS_STRB_cld/out -period 10.000000 -waveform { 0.000000 5.000000 } -design rtl 
create_clock { USB_FLAGC } -domain ClockDomain1 -name USB_FLAGC -period 5.000000 -waveform { 0.000000 2.500000 } -design rtl 
create_clock { CLK_IN } -domain ClockDomain2 -name CLK_IN -period 5.000000 -waveform { 0.000000 2.500000 } -design rtl 
create_clock { USB_FLAGB } -domain ClockDomain0 -name USB_FLAGB -period 5.000000 -waveform { 0.000000 2.500000 } -design rtl 

