###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name NOPAD -value "TRUE" -port ADC_CLKINN -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_CLKINP -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_OUTCLK_N -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_OUTCLK_P -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I12_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I13_u -design gatelevel 
set_attribute -name PART -value "3S1600EFG320-4" -type string /work/top/struct -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/icon/view_1_precision_icon -design gatelevel 
set_attribute -name COREGEN -value "TRUE" /test_lib/icon/view_1_precision_icon -design gatelevel 

set_attribute -name IOB -value "TRUE" -instance -type string U_2/vio/i_vio/gen_async_out/1/async_out_cell/user_reg -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/vio/view_1_precision_vio -design gatelevel 
set_attribute -name COREGEN -value "TRUE" /test_lib/vio/view_1_precision_vio -design gatelevel 

set_attribute -name WRITE_MODE -value "READ_FIRST" -instance U_4_FPA_SM_TABLE/ix45489z7749 -design gatelevel 
set_attribute -name WRITE_MODE -value "READ_FIRST" -instance U_4_FPA_SM_TABLE/ix61445z7749 -design gatelevel 

set_attribute -name state_vector -value "current_state" /work/usb_translator_notri/fsm_unfold_1367 -design gatelevel 

##################
# Clocks
##################
create_clock { U_2/vio/i_vio/gen_async_out/0/async_out_cell/user_reg/Q } -domain ClockDomain3 -name U_2/vio/i_vio/gen_async_out/0/async_out_cell/user_reg/Q -period 5.000000 -waveform { 0.000000 2.500000 } -design gatelevel 
create_clock { U_0_I0/reg_BEEBUS_STRB_cld/Q } -domain ClockDomain2 -name U_0/I0/reg_BEEBUS_STRB_cld/out -period 10.000000 -waveform { 0.000000 5.000000 } -design gatelevel 
create_clock { USB_FLAGB } -domain ClockDomain0 -name USB_FLAGB -period 5.000000 -waveform { 0.000000 2.500000 } -design gatelevel 
create_clock { USB_FLAGC } -domain ClockDomain1 -name USB_FLAGC -period 5.000000 -waveform { 0.000000 2.500000 } -design gatelevel 
create_clock { CLK_IN } -domain ClockDomain2 -name CLK_IN -period 5.000000 -waveform { 0.000000 2.500000 } -design gatelevel 

