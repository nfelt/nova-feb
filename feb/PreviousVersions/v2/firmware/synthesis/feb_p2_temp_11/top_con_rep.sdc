###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name CLKDV_DIVIDE -value "2.0" -instance -type default U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKFX_DIVIDE -value "1" -instance -type integer U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKFX_MULTIPLY -value "2" -instance -type integer U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKIN_DIVIDE_BY_2 -value "0" -instance -type default U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKIN_PERIOD -value "62.5" -instance -type default U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKOUT_PHASE_SHIFT -value "NONE" -instance -type string U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLK_FEEDBACK -value "NONE" -instance -type string U_3/CLK_IN_DCM -design rtl 
set_attribute -name DESKEW_ADJUST -value "SYSTEM_SYNCHRONOUS" -instance -type string U_3/CLK_IN_DCM -design rtl 
set_attribute -name DFS_FREQUENCY_MODE -value "LOW" -instance -type string U_3/CLK_IN_DCM -design rtl 
set_attribute -name DLL_FREQUENCY_MODE -value "LOW" -instance -type string U_3/CLK_IN_DCM -design rtl 
set_attribute -name DUTY_CYCLE_CORRECTION -value "1" -instance -type default U_3/CLK_IN_DCM -design rtl 
set_attribute -name FACTORY_JF -value "c080" -instance -type default U_3/CLK_IN_DCM -design rtl 
set_attribute -name PHASE_SHIFT -value "0" -instance -type integer U_3/CLK_IN_DCM -design rtl 
set_attribute -name STARTUP_WAIT -value "1" -instance -type default U_3/CLK_IN_DCM -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/decode_8b10b/view_1_precision_decode_8b10b_XRTL -design rtl 
set_attribute -name COREGEN -value "TRUE" /test_lib/decode_8b10b/view_1_precision_decode_8b10b_XRTL -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/XST_GND -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f6_3 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5_8 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000020 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000020 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000019 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000019 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5_7 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000018 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000018 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000017 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000017 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f6_2 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5_6 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000016 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000016 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000015 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000015 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5_5 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000014 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000014 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000013 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000013 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f6_1 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5_4 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000012 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000012 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000011 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000011 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5_3 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000010 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000010 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00009 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00009 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f6_0 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5_2 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00008 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00008 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00007 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00007 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5_1 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00006 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00006 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00005 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00005 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f6 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5_0 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00004 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00004 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00003 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00003 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00002 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00002 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00001 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00001 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/dout_7 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/dout_6 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/dout_5 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/dout_4 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/dout_3 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/dout_2 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/dout_1 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/dout_0 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/kout -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/b3(5)31 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/b3(5)31 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/b3(5)11 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/b3(5)11 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/b3(6)1 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/b3(6)1 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/b3(5)1 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/b3(5)1 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/b3(5)21 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/b3(5)21 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/k28_or0000_SW0 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/k28_or0000_SW0 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/k28_or0000 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/k28_or0000 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/k15 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/k15 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/k47 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/k47 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/b3(7)_SW2 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/b3(7)_SW2 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/b3(7)_SW3 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/b3(7)_SW3 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/b3(7) -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/b3(7) -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/k67 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/k67_F -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/k67_F -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/k67_G -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_11/DCM_COMM_DECODE/BU2/U0/first_decoder/k67_G -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_11/DCM_COMM_DECODE/BU2/XST_VCC -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/decode_8b10b_decode_8b10b_xst_1/view_1_precision_decode_8b10b_decode_8b10b_xst_1_XRTL -design rtl 
set_attribute -name NDF -value "TRUE" /test_lib/decode_8b10b_decode_8b10b_xst_1/view_1_precision_decode_8b10b_decode_8b10b_xst_1_XRTL -design rtl 
set_attribute -name contains_init_values -value "true" /test_lib/decode_8b10b_decode_8b10b_xst_1/view_1_precision_decode_8b10b_decode_8b10b_xst_1_XRTL -design rtl 
set_attribute -name NGC_DONTTOUCH -value "true" /test_lib/decode_8b10b_decode_8b10b_xst_1/view_1_precision_decode_8b10b_decode_8b10b_xst_1_XRTL -design rtl 


set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/encode_8b10b/view_1_precision_encode_8b10b_XRTL -design rtl 
set_attribute -name COREGEN -value "TRUE" /test_lib/encode_8b10b/view_1_precision_encode_8b10b_XRTL -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/XST_GND -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/XST_VCC -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_2 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_0 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_1 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_3 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_4 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_7 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_5 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_6 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_8 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_9 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/disp_run -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/k_invalid -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/k_invalid_cmp_eq00001 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/k_invalid_cmp_eq00001 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)61 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)61 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/k28_0_and00001 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/k28_0_and00001 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)41 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)41 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)21 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)21 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)111 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)111 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/pdes61_SW0 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/pdes61_SW0 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)1_SW0 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)1_SW0 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)1 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)1 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/k_invalid_mux000312 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/k_invalid_mux000312 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/k_invalid_mux000396 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/k_invalid_mux000396 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_pdes411 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_pdes411 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)98 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)98 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)133 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)133 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)1 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)1 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)2 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)2 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)_f5 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)_f6 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)3 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)3 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)4 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)4 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)_f5_0 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)5 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)5 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)6 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)6 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)_f5_1 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)_f6_0 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)_f7 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)113 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)113 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)131 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)131 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)146 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)146 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)275 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)275 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b421 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b421 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b422 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b422 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b423 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b423 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b431 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b431 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b432 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b432 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)1 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)1 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)2 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)2 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)_f5 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)3 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)3 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)_f6 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)4 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)4 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)5 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)5 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)_f5_0 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)6 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)6 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)_f6_0 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)_f7 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)1 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)1 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)2 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)2 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)_f5 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)3 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)3 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)_f6 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)4 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)4 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)5 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)5 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)6 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)6 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)_f5_0 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)_f6_0 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)_f7 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)210 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)210 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b4135 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b4135 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW0 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW0 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW1 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW1 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b4180 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b4180 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/pdes62 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/pdes62 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)259_SW0 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)259_SW0 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)259_SW1 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)259_SW1 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)259 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)259 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/pdes62_SW0 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/pdes62_SW0 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b4180_SW0 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b4180_SW0 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b44_f5 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b444 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b444 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b445 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b445 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b43_f5 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b42_f5 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b42_f6 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b42_f6 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b43_f6 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b43_f6 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b44_f6 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b44_f6 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW2 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW2_F -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW2_F -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW2_G -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW2_G -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW3 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW3_F -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW3_F -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW3_G -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW3_G -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW4 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW4_F -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW4_F -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW4_G -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW4_G -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW5 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW5_F -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW5_F -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW5_G -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW5_G -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW6 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW6_F -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW6_F -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW6_G -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW6_G -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW7 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW7_F -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW7_F -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW7_G -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW7_G -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/k_invalid_mux000364 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/k_invalid_mux000364 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)10_SW1 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)10_SW1 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)69_SW0 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)69_SW0 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)69_SW1 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)69_SW1 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)69 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)69 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/disp_run_1 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/disp_run_2 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172_SW1 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172_SW1_F -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172_SW1_F -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172_SW1_G -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172_SW1_G -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)141 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)141_F -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)141_F -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)141_G -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)141_G -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)69_SW01 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)69_SW01 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)69_SW02 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)69_SW02 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)69_SW0_f5 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172_SW01 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172_SW01 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172_SW0_f5 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)169 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)169 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b433 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b433 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b4180_SW1 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b4180_SW1 -design rtl 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b443 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4/U_7/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b443 -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/encode_8b10b_encode_8b10b_xst_1/view_1_precision_encode_8b10b_encode_8b10b_xst_1_XRTL -design rtl 
set_attribute -name NDF -value "TRUE" /test_lib/encode_8b10b_encode_8b10b_xst_1/view_1_precision_encode_8b10b_encode_8b10b_xst_1_XRTL -design rtl 
set_attribute -name contains_init_values -value "true" /test_lib/encode_8b10b_encode_8b10b_xst_1/view_1_precision_encode_8b10b_encode_8b10b_xst_1_XRTL -design rtl 
set_attribute -name NGC_DONTTOUCH -value "true" /test_lib/encode_8b10b_encode_8b10b_xst_1/view_1_precision_encode_8b10b_encode_8b10b_xst_1_XRTL -design rtl 


set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_11_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_10_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_9_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_8_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_7_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_6_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_5_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_4_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_3_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_2_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_1_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_0_u -design rtl 

set_attribute -name iostandard -value "DEFAULT" -instance -type string I12/u -design rtl 

set_attribute -name iostandard -value "DEFAULT" -instance -type string I13/u -design rtl 

set_attribute -name ram_processed -value "true" -instance U_2/U_1/PEDESTAL_TABLE/PEDESTAL_TABLE -design rtl 

set_attribute -name ram_processed -value "true" -instance U_2/U_1/THRESHOLD_TABLE/THRESHOLD_TABLE -design rtl 



set_attribute -name ram_processed -value "true" -instance U_0/ADC_BUFFER/ADC_BUFFER -design rtl 


set_attribute -name state_vector -value "current_state" -type string /work/usb_translator_notri/fsm_unfold_737_XRTL -design rtl 



##################
# Clocks
##################
create_clock { U_4/U_7/reg_CLK_3_2_sig/out } -domain ClockDomain2 -name U_4/U_7/reg_CLK_3_2_sig/out -period 50.000000 -waveform { 0.000000 25.000000 } -design rtl 
create_clock { U_4/U_11/reg_CLK_3_2_sig/out } -domain ClockDomain1 -name U_4/U_11/reg_CLK_3_2_sig/out -period 50.000000 -waveform { 0.000000 25.000000 } -design rtl 
create_clock { U_3/ASIC_BGCE/O } -domain ClockDomain0 -name U_3/ASIC_BGCE/O -period 50.000000 -waveform { 0.000000 25.000000 } -design rtl 
create_clock { CLK_IN } -domain ClockDomain0 -name CLK_IN -period 50.000000 -waveform { 0.000000 25.000000 } -design rtl 
create_clock { U_3/CLK_IN_DCM/CLKFX } -domain ClockDomain1 -name U_3/CLK_IN_DCM/CLKFX -period 25.000000 -waveform { 0.000000 12.500000 } -design rtl 

