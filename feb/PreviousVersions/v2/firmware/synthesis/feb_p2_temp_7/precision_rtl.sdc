###################################################################################
# Mentor Graphics Corporation
#
# This file is not a constraints report, nor does it list all the
# RTL constraints in the design. This file is created and used by Precision
# to track user RTL constraints that are set during design iterations.
# You should not edit this file because doing so might cause improper
# constraints in the design.
#
# If you want to list all RTL design constraints, use the command
#       report_constraints -design rtl
# or double-click on the RTL Constraints Report node in the Output Files
# Folder in the GUI.
#
# For a detailed discussion of how to set constraints, please refer to
# Precision documentation which is available from the Help pulldown menu.
###################################################################################

#################
# Attributes
#################
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_5/U_3/CLK_IN_DCM -design rtl 

##################
# Clocks
##################
create_clock  -name daq_clk -period 5.000000 -waveform { 0.000000 2.500000 } -design rtl 

