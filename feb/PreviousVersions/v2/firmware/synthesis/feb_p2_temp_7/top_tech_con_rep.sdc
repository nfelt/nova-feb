###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(11) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(10) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(9) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(8) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(7) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(6) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(5) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(4) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(3) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(2) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(1) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(0) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(11) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(10) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(9) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(8) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(7) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(6) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(5) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(4) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(3) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(2) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(1) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(0) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_SHIFTOUT -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port CLK_IN -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FLAGB -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FLAGC -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_CLKINN -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_CLKINP -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_ENABLE(3) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_ENABLE(2) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_ENABLE(1) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_ENABLE(0) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_MODE(2) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_MODE(1) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_MODE(0) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_CHIPRESET -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_INTEGRST -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_OUTCLK_N -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_OUTCLK_P -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_SHAPERRST -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_SHIFTIN -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_SRCK -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_TESTINJECT -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port LA(8) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port LA(7) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port LA(6) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port LA(5) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port LA(4) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port LA(3) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port LA(2) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port LA(1) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port LA(0) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port SER_NUM_CS_B -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port SPI_DIN -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port SPI_SCLK -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port TEC_ADC_CS_B -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port TEC_DAC_LDAC_B -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port TEC_DAC_SYNC_B -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port TEC_ENABLE -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port TEMP_SENSOR_CS_B -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FIFOADR(1) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FIFOADR(0) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_IFCLK -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_PKTEND -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_RESET_B -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_SLOE -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_SLRD -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_SLWR -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_WAKEUP_B -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_WU2 -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FD(15) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FD(14) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FD(13) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FD(12) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FD(11) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FD(10) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FD(9) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FD(8) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FD(7) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FD(6) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FD(5) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FD(4) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FD(3) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FD(2) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FD(1) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_FD(0) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_SCL -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_SDA -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_11_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_10_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_9_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_8_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_7_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_6_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_5_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_4_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_3_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_2_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_1_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_0_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I12_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I13_u -design gatelevel 
set_attribute -name CLKDV_DIVIDE -value "2.0" -instance -type default U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKFX_DIVIDE -value "1" -instance -type integer U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKFX_MULTIPLY -value "2" -instance -type integer U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKIN_DIVIDE_BY_2 -value "0" -instance -type default U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKIN_PERIOD -value "62.5" -instance -type default U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKOUT_PHASE_SHIFT -value "NONE" -instance -type string U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLK_FEEDBACK -value "NONE" -instance -type string U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name DESKEW_ADJUST -value "SYSTEM_SYNCHRONOUS" -instance -type string U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name DFS_FREQUENCY_MODE -value "LOW" -instance -type string U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name DLL_FREQUENCY_MODE -value "LOW" -instance -type string U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name DUTY_CYCLE_CORRECTION -value "1" -instance -type default U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name FACTORY_JF -value "c080" -instance -type default U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name PHASE_SHIFT -value "0" -instance -type integer U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name STARTUP_WAIT -value "1" -instance -type default U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(11) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(10) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(9) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(8) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(7) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(6) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(5) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(4) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(3) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(2) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(1) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(0) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string reg_DCM_LOCK(3)_repl3 -design gatelevel 
set_attribute -name PART -value "3S1600EFG320-4" -type string /work/top/struct -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/vio/view_1_precision_vio -design gatelevel 
set_attribute -name COREGEN -value "TRUE" /test_lib/vio/view_1_precision_vio -design gatelevel 

set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/icon/view_1_precision_icon -design gatelevel 
set_attribute -name COREGEN -value "TRUE" /test_lib/icon/view_1_precision_icon -design gatelevel 

set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_0/ADC_BUFFER/ix13887z26572 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_0/ADC_BUFFER/ix9899z26572 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_0/ADC_BUFFER/ix34372z26572 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_0/ADC_BUFFER/ix38360z26572 -design gatelevel 


set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_SLRD_cld -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(15) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(14) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(13) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(12) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(11) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(10) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(9) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(8) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(7) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(6) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(5) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(4) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(3) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(2) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(1) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(0) -design gatelevel 
set_attribute -name state_vector -value "current_state" /work/usb_translator_notri/fsm_unfold_737 -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_5_U_1/reg_ASIC_SRCK_int_repl0 -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_5_U_1/reg_ASIC_SHIFT_REGISTER(0)_repl0 -design gatelevel 

set_attribute -name IOB -value "TRUE" -instance -type string U_5_U_2/reg_EXTERNAL_TRIGGER -design gatelevel 

##################
# Clocks
##################
create_clock { U_1_reg_DATA_CH_00_15(1)/Q } -domain ClockDomain31 -name U_1_reg_DATA_CH_00_15(1)/Q -period 20.000000 -waveform { 0.000000 10.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(8)/Q } -domain ClockDomain24 -name U_1_reg_DATA_CH_00_15(8)/Q -period 20.000000 -waveform { 0.000000 10.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(0)/Q } -domain ClockDomain32 -name U_1_reg_DATA_CH_00_15(0)/Q -period 20.000000 -waveform { 0.000000 10.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(7)/Q } -domain ClockDomain25 -name U_1_reg_DATA_CH_00_15(7)/Q -period 20.000000 -waveform { 0.000000 10.000000 } -design gatelevel 
create_clock { U_5_U_3_CLK_IN_DCM/CLKFX } -domain ClockDomain0 -name U_5_U_3_CLK_IN_DCM/CLKFX -period 10.000000 -waveform { 0.000000 5.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(6)/Q } -domain ClockDomain26 -name U_1_reg_DATA_CH_00_15(6)/Q -period 20.000000 -waveform { 0.000000 10.000000 } -design gatelevel 
create_clock { U_0/reg_BUFFER_READ_RDY/Q } -domain ClockDomain15 -name U_0/reg_BUFFER_READ_RDY/out -period 20.000000 -waveform { 0.000000 10.000000 } -design gatelevel 
create_clock { U_5_U_0_reg_CONTOLLER_STATE(1)/Q } -domain ClockDomain19 -name U_5_U_0_reg_CONTOLLER_STATE(1)/Q -period 20.000000 -waveform { 0.000000 10.000000 } -design gatelevel 
create_clock { U_0/reg_OVERFLOW_ERROR/Q } -domain ClockDomain14 -name U_0/reg_OVERFLOW_ERROR/out -period 20.000000 -waveform { 0.000000 10.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(5)/Q } -domain ClockDomain27 -name U_1_reg_DATA_CH_00_15(5)/Q -period 20.000000 -waveform { 0.000000 10.000000 } -design gatelevel 
create_clock { icon_1/icon/u_icon/i_yes_bscan/u_bs/i_v2/u_bs/DRCK1 } -domain ClockDomain17 -name icon_1/icon/u_icon/i_yes_bscan/u_bs/i_v2/u_bs/DRCK1 -period 10.000000 -waveform { 0.000000 5.000000 } -design gatelevel 
create_clock { U_2_U_0/reg_FMAT_DATA_STRB/Q } -domain ClockDomain20 -name U_2_U_0/reg_FMAT_DATA_STRB/Q -period 20.000000 -waveform { 0.000000 10.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(4)/Q } -domain ClockDomain28 -name U_1_reg_DATA_CH_00_15(4)/Q -period 20.000000 -waveform { 0.000000 10.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(11)/Q } -domain ClockDomain21 -name U_1_reg_DATA_CH_00_15(11)/Q -period 20.000000 -waveform { 0.000000 10.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(3)/Q } -domain ClockDomain29 -name U_1_reg_DATA_CH_00_15(3)/Q -period 20.000000 -waveform { 0.000000 10.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(10)/Q } -domain ClockDomain22 -name U_1_reg_DATA_CH_00_15(10)/Q -period 20.000000 -waveform { 0.000000 10.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(2)/Q } -domain ClockDomain30 -name U_1_reg_DATA_CH_00_15(2)/Q -period 20.000000 -waveform { 0.000000 10.000000 } -design gatelevel 
create_clock { icon_1/icon/u_icon/i_yes_bscan/u_bs/i_v2/u_bs/UPDATE } -domain ClockDomain18 -name icon_1/icon/u_icon/i_yes_bscan/u_bs/i_v2/u_bs/UPDATE -period 10.000000 -waveform { 0.000000 5.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(9)/Q } -domain ClockDomain23 -name U_1_reg_DATA_CH_00_15(9)/Q -period 20.000000 -waveform { 0.000000 10.000000 } -design gatelevel 
create_clock { CLK_IN } -domain ClockDomain0 -name CLK_IN -period 10.000000 -waveform { 0.000000 5.000000 } -design gatelevel 

