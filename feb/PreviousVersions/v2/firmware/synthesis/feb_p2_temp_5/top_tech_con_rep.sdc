###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(11) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(10) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(9) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(8) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(7) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(6) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(5) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(4) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(3) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(2) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(1) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(0) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(11) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(10) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(9) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(8) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(7) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(6) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(5) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(4) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(3) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(2) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(1) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(0) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port CLK_IN -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_CLKINN -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_CLKINP -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_OUTCLK_N -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_OUTCLK_P -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_11_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_10_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_9_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_8_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_7_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_6_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_5_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_4_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_3_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_2_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_1_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_0_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I12_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I13_u -design gatelevel 
set_attribute -name CLKDV_DIVIDE -value "2.0" -instance -type default U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKFX_DIVIDE -value "2" -instance -type integer U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKFX_MULTIPLY -value "3" -instance -type integer U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKIN_DIVIDE_BY_2 -value "0" -instance -type default U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKIN_PERIOD -value "62.5" -instance -type default U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKOUT_PHASE_SHIFT -value "NONE" -instance -type string U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLK_FEEDBACK -value "1X" -instance -type string U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name DESKEW_ADJUST -value "SYSTEM_SYNCHRONOUS" -instance -type string U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name DFS_FREQUENCY_MODE -value "LOW" -instance -type string U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name DLL_FREQUENCY_MODE -value "LOW" -instance -type string U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name DUTY_CYCLE_CORRECTION -value "1" -instance -type default U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name FACTORY_JF -value "c080" -instance -type default U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name PHASE_SHIFT -value "0" -instance -type integer U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name STARTUP_WAIT -value "1" -instance -type default U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_5_U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(11) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(10) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(9) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(8) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(7) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(6) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(5) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(4) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(3) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(2) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(1) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(0) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string reg_DCM_LOCK(3)_repl0 -design gatelevel 
set_attribute -name PART -value "3S1600EFG320-4" -type string /work/top/struct -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/vio/view_1_precision_vio -design gatelevel 
set_attribute -name COREGEN -value "TRUE" /test_lib/vio/view_1_precision_vio -design gatelevel 

set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/icon/view_1_precision_icon -design gatelevel 
set_attribute -name COREGEN -value "TRUE" /test_lib/icon/view_1_precision_icon -design gatelevel 

set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_0/ADC_BUFFER/ix13887z26572 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_0/ADC_BUFFER/ix9899z26572 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_0/ADC_BUFFER/ix34372z26572 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_0/ADC_BUFFER/ix38360z26572 -design gatelevel 


set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_SLRD_cld -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(15) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(14) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(13) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(12) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(11) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(10) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(9) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(8) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(7) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(6) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(5) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(4) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(3) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(2) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(1) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(0) -design gatelevel 
set_attribute -name state_vector -value "current_state" /work/usb_translator_notri/fsm_unfold_737 -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_5_U_1/reg_ASIC_SHIFT_REGISTER(0)_repl0 -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_5_U_1/reg_ASIC_SRCK_int_repl0 -design gatelevel 

set_attribute -name IOB -value "TRUE" -instance -type string U_5_U_2/reg_EXTERNAL_TRIGGER -design gatelevel 

##################
# Clocks
##################
create_clock  -domain Design_Clock -name Design_Clock -period 40.000000 -waveform { 0.000000 20.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(10)/Q } -domain ClockDomain18 -name U_1_reg_DATA_CH_00_15(10)/Q -period 80.000000 -waveform { 0.000000 40.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(2)/Q } -domain ClockDomain26 -name U_1_reg_DATA_CH_00_15(2)/Q -period 80.000000 -waveform { 0.000000 40.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(9)/Q } -domain ClockDomain19 -name U_1_reg_DATA_CH_00_15(9)/Q -period 80.000000 -waveform { 0.000000 40.000000 } -design gatelevel 
create_clock { U_2_U_0/reg_FMAT_DATA_STRB/Q } -domain ClockDomain13 -name U_2/U_0/reg_FMAT_DATA_STRB/out -period 80.000000 -waveform { 0.000000 40.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(1)/Q } -domain ClockDomain27 -name U_1_reg_DATA_CH_00_15(1)/Q -period 80.000000 -waveform { 0.000000 40.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(8)/Q } -domain ClockDomain20 -name U_1_reg_DATA_CH_00_15(8)/Q -period 80.000000 -waveform { 0.000000 40.000000 } -design gatelevel 
create_clock { U_0/reg_OVERFLOW_ERROR/Q } -domain ClockDomain14 -name U_0/reg_OVERFLOW_ERROR/out -period 80.000000 -waveform { 0.000000 40.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(0)/Q } -domain ClockDomain28 -name U_1_reg_DATA_CH_00_15(0)/Q -period 80.000000 -waveform { 0.000000 40.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(7)/Q } -domain ClockDomain21 -name U_1_reg_DATA_CH_00_15(7)/Q -period 80.000000 -waveform { 0.000000 40.000000 } -design gatelevel 
create_clock { U_5_U_3_CLK_IN_DCM/CLK0 } -domain ClockDomain0 -name U_5/U_3/CLK_IN_DCM/CLK0 -period 40.000000 -waveform { 0.000000 20.000000 } -design gatelevel 
create_clock { U_5_U_0/reg_CONTOLLER_STATE(1)/Q } -domain ClockDomain16 -name U_5/U_0/reg_CONTOLLER_STATE(1)/out -period 80.000000 -waveform { 0.000000 40.000000 } -design gatelevel 
create_clock { U_0/reg_BUFFER_READ_RDY/Q } -domain ClockDomain15 -name U_0/reg_BUFFER_READ_RDY/out -period 80.000000 -waveform { 0.000000 40.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(6)/Q } -domain ClockDomain22 -name U_1_reg_DATA_CH_00_15(6)/Q -period 80.000000 -waveform { 0.000000 40.000000 } -design gatelevel 
create_clock { icon_1/icon/u_icon/i_yes_bscan/u_bs/i_v2/u_bs/DRCK1 } -domain ClockDomain17 -name icon_1/icon/u_icon/i_yes_bscan/u_bs/i_v2/u_bs/DRCK1 -period 40.000000 -waveform { 0.000000 20.000000 } -design gatelevel 
create_clock { icon_1/icon/u_icon/i_yes_bscan/u_bs/i_v2/u_bs/UPDATE } -domain ClockDomain18 -name icon_1/icon/u_icon/i_yes_bscan/u_bs/i_v2/u_bs/UPDATE -period 40.000000 -waveform { 0.000000 20.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(5)/Q } -domain ClockDomain23 -name U_1_reg_DATA_CH_00_15(5)/Q -period 80.000000 -waveform { 0.000000 40.000000 } -design gatelevel 
create_clock { U_5_U_3_CLK_IN_DCM/CLK180 } -domain ClockDomain0 -name U_5/U_3/CLK_IN_DCM/CLK180 -period 40.000000 -waveform { 0.000000 20.000000 } -design gatelevel 
create_clock { U_5_U_3_CLK_IN_DCM/CLK2X } -domain ClockDomain0 -name U_5/U_3/CLK_IN_DCM/CLK2X -period 40.000000 -waveform { 0.000000 20.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(4)/Q } -domain ClockDomain24 -name U_1_reg_DATA_CH_00_15(4)/Q -period 80.000000 -waveform { 0.000000 40.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(11)/Q } -domain ClockDomain17 -name U_1_reg_DATA_CH_00_15(11)/Q -period 80.000000 -waveform { 0.000000 40.000000 } -design gatelevel 
create_clock { U_1_reg_DATA_CH_00_15(3)/Q } -domain ClockDomain25 -name U_1_reg_DATA_CH_00_15(3)/Q -period 80.000000 -waveform { 0.000000 40.000000 } -design gatelevel 

##################
# Input delays
##################
set_input_delay 3.000 -clock Design_Clock -add_delay  -design gatelevel  {CLK_IN USB_SCL USB_SDA}
set_input_delay 3.000 -clock U_5/U_3/CLK_IN_DCM/CLK0 -add_delay  -design gatelevel  {ASIC_SHIFTOUT USB_FD(*) USB_FLAGB USB_FLAGC}
set_input_delay 3.000 -clock U_5/U_3/CLK_IN_DCM/CLK2X -add_delay  -design gatelevel  {ADC_DOUTAN(*) ADC_DOUTAP(*) USB_FD(0) USB_FD(1) USB_FD(10) USB_FD(11) USB_FD(2) USB_FD(3) USB_FD(4) USB_FD(5) USB_FD(6) USB_FD(7) USB_FD(8) USB_FD(9)}

###################
# Output delays
###################
set_output_delay 3.000 -clock Design_Clock -add_delay  -design gatelevel  {ADC_CLKINN ADC_CLKINP ADC_ENABLE(*) ADC_MODE(*) ASIC_INTEGRST ASIC_OUTCLK_N ASIC_OUTCLK_P ASIC_SHAPERRST LA(0) LA(1) LA(2) LA(3) LA(4) LA(5) LA(7) LA(8) SER_NUM_CS_B SPI_DIN SPI_SCLK TEC_ADC_CS_B TEC_DAC_LDAC_B TEC_DAC_SYNC_B TEC_ENABLE TEMP_SENSOR_CS_B USB_FIFOADR(0) USB_IFCLK USB_SCL USB_SDA USB_WAKEUP_B USB_WU2}
set_output_delay 3.000 -clock U_5/U_3/CLK_IN_DCM/CLK0 -add_delay  -design gatelevel  {ASIC_CHIPRESET ASIC_SHIFTIN ASIC_SRCK ASIC_TESTINJECT LA(6) USB_FD(*) USB_FIFOADR(1) USB_PKTEND USB_RESET_B USB_SLOE USB_SLRD USB_SLWR}

