##
##  Copyright (c) Mentor Graphics Corporation, 1996-2004, All Rights Reserved.
##             Portions copyright 1991-2004 Compuware Corporation
##                       UNPUBLISHED, LICENSED SOFTWARE.
##            CONFIDENTIAL AND PROPRIETARY INFORMATION WHICH IS THE
##          PROPERTY OF MENTOR GRAPHICS CORPORATION OR ITS LICENSORS
#
# File created on: 09/25/07 16:30:09
#

#
# Boundary Constraints
#
puts "Creating boundary constraints. . ."

#
# Attributes
#
puts "Setting attributes. . ."
if [catch {set_attribute {ADC_CLKINN} -port -name {PIN_NUMBER} -value {t8}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_CLKINN failed}}
if [catch {set_attribute {ADC_CLKINP} -port -name {PIN_NUMBER} -value {R8}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_CLKINP failed}}
if [catch {set_attribute {ADC_DOUTAN(0)} -port -name {PIN_NUMBER} -value {C2}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAN(0) failed}}
if [catch {set_attribute {ADC_DOUTAN(1)} -port -name {PIN_NUMBER} -value {D2}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAN(1) failed}}
if [catch {set_attribute {ADC_DOUTAN(10)} -port -name {PIN_NUMBER} -value {J4}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAN(10) failed}}
if [catch {set_attribute {ADC_DOUTAN(11)} -port -name {PIN_NUMBER} -value {J2}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAN(11) failed}}
if [catch {set_attribute {ADC_DOUTAN(2)} -port -name {PIN_NUMBER} -value {E1}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAN(2) failed}}
if [catch {set_attribute {ADC_DOUTAN(3)} -port -name {PIN_NUMBER} -value {E3}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAN(3) failed}}
if [catch {set_attribute {ADC_DOUTAN(4)} -port -name {PIN_NUMBER} -value {F2}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAN(4) failed}}
if [catch {set_attribute {ADC_DOUTAN(5)} -port -name {PIN_NUMBER} -value {G4}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAN(5) failed}}
if [catch {set_attribute {ADC_DOUTAN(6)} -port -name {PIN_NUMBER} -value {G5}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAN(6) failed}}
if [catch {set_attribute {ADC_DOUTAN(7)} -port -name {PIN_NUMBER} -value {H5}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAN(7) failed}}
if [catch {set_attribute {ADC_DOUTAN(8)} -port -name {PIN_NUMBER} -value {H3}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAN(8) failed}}
if [catch {set_attribute {ADC_DOUTAN(9)} -port -name {PIN_NUMBER} -value {H1}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAN(9) failed}}
if [catch {set_attribute {ADC_DOUTAP(0)} -port -name {PIN_NUMBER} -value {C1}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAP(0) failed}}
if [catch {set_attribute {ADC_DOUTAP(1)} -port -name {PIN_NUMBER} -value {D1}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAP(1) failed}}
if [catch {set_attribute {ADC_DOUTAP(10)} -port -name {PIN_NUMBER} -value {J5}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAP(10) failed}}
if [catch {set_attribute {ADC_DOUTAP(11)} -port -name {PIN_NUMBER} -value {J1}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAP(11) failed}}
if [catch {set_attribute {ADC_DOUTAP(2)} -port -name {PIN_NUMBER} -value {E2}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAP(2) failed}}
if [catch {set_attribute {ADC_DOUTAP(3)} -port -name {PIN_NUMBER} -value {E4}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAP(3) failed}}
if [catch {set_attribute {ADC_DOUTAP(4)} -port -name {PIN_NUMBER} -value {F1}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAP(4) failed}}
if [catch {set_attribute {ADC_DOUTAP(5)} -port -name {PIN_NUMBER} -value {G3}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAP(5) failed}}
if [catch {set_attribute {ADC_DOUTAP(6)} -port -name {PIN_NUMBER} -value {G6}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAP(6) failed}}
if [catch {set_attribute {ADC_DOUTAP(7)} -port -name {PIN_NUMBER} -value {H6}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAP(7) failed}}
if [catch {set_attribute {ADC_DOUTAP(8)} -port -name {PIN_NUMBER} -value {H4}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAP(8) failed}}
if [catch {set_attribute {ADC_DOUTAP(9)} -port -name {PIN_NUMBER} -value {H2}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTAP(9) failed}}
if [catch {set_attribute {ADC_DOUTBN(0)} -port -name {PIN_NUMBER} -value {k4}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBN(0) failed}}
if [catch {set_attribute {ADC_DOUTBN(1)} -port -name {PIN_NUMBER} -value {k5}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBN(1) failed}}
if [catch {set_attribute {ADC_DOUTBN(10)} -port -name {PIN_NUMBER} -value {r2}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBN(10) failed}}
if [catch {set_attribute {ADC_DOUTBN(11)} -port -name {PIN_NUMBER} -value {t1}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBN(11) failed}}
if [catch {set_attribute {ADC_DOUTBN(2)} -port -name {PIN_NUMBER} -value {l2}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBN(2) failed}}
if [catch {set_attribute {ADC_DOUTBN(3)} -port -name {PIN_NUMBER} -value {l4}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBN(3) failed}}
if [catch {set_attribute {ADC_DOUTBN(4)} -port -name {PIN_NUMBER} -value {l5}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBN(4) failed}}
if [catch {set_attribute {ADC_DOUTBN(5)} -port -name {PIN_NUMBER} -value {m3}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBN(5) failed}}
if [catch {set_attribute {ADC_DOUTBN(6)} -port -name {PIN_NUMBER} -value {m6}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBN(6) failed}}
if [catch {set_attribute {ADC_DOUTBN(7)} -port -name {PIN_NUMBER} -value {n5}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBN(7) failed}}
if [catch {set_attribute {ADC_DOUTBN(8)} -port -name {PIN_NUMBER} -value {p1}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBN(8) failed}}
if [catch {set_attribute {ADC_DOUTBN(9)} -port -name {PIN_NUMBER} -value {p4}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBN(9) failed}}
if [catch {set_attribute {ADC_DOUTBP(0)} -port -name {PIN_NUMBER} -value {K3}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBP(0) failed}}
if [catch {set_attribute {ADC_DOUTBP(1)} -port -name {PIN_NUMBER} -value {K6}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBP(1) failed}}
if [catch {set_attribute {ADC_DOUTBP(10)} -port -name {PIN_NUMBER} -value {R3}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBP(10) failed}}
if [catch {set_attribute {ADC_DOUTBP(11)} -port -name {PIN_NUMBER} -value {T2}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBP(11) failed}}
if [catch {set_attribute {ADC_DOUTBP(2)} -port -name {PIN_NUMBER} -value {L1}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBP(2) failed}}
if [catch {set_attribute {ADC_DOUTBP(3)} -port -name {PIN_NUMBER} -value {L3}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBP(3) failed}}
if [catch {set_attribute {ADC_DOUTBP(4)} -port -name {PIN_NUMBER} -value {L6}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBP(4) failed}}
if [catch {set_attribute {ADC_DOUTBP(5)} -port -name {PIN_NUMBER} -value {M4}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBP(5) failed}}
if [catch {set_attribute {ADC_DOUTBP(6)} -port -name {PIN_NUMBER} -value {M5}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBP(6) failed}}
if [catch {set_attribute {ADC_DOUTBP(7)} -port -name {PIN_NUMBER} -value {N4}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBP(7) failed}}
if [catch {set_attribute {ADC_DOUTBP(8)} -port -name {PIN_NUMBER} -value {P2}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBP(8) failed}}
if [catch {set_attribute {ADC_DOUTBP(9)} -port -name {PIN_NUMBER} -value {P3}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_DOUTBP(9) failed}}
if [catch {set_attribute {ADC_ENABLE(0)} -port -name {PIN_NUMBER} -value {a4}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_ENABLE(0) failed}}
if [catch {set_attribute {ADC_ENABLE(1)} -port -name {PIN_NUMBER} -value {a6}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_ENABLE(1) failed}}
if [catch {set_attribute {ADC_ENABLE(2)} -port -name {PIN_NUMBER} -value {b4}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_ENABLE(2) failed}}
if [catch {set_attribute {ADC_ENABLE(3)} -port -name {PIN_NUMBER} -value {b6}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_ENABLE(3) failed}}
if [catch {set_attribute {ADC_MODE(0)} -port -name {PIN_NUMBER} -value {u5}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_MODE(0) failed}}
if [catch {set_attribute {ADC_MODE(1)} -port -name {PIN_NUMBER} -value {v6}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_MODE(1) failed}}
if [catch {set_attribute {ADC_MODE(2)} -port -name {PIN_NUMBER} -value {v7}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ADC_MODE(2) failed}}
if [catch {set_attribute {ASIC_CHIPRESET} -port -name {PIN_NUMBER} -value {n7}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ASIC_CHIPRESET failed}}
if [catch {set_attribute {ASIC_INTEGRST} -port -name {PIN_NUMBER} -value {r6}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ASIC_INTEGRST failed}}
if [catch {set_attribute {ASIC_OUTCLK_N} -port -name {PIN_NUMBER} -value {t4}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ASIC_OUTCLK_N failed}}
if [catch {set_attribute {ASIC_OUTCLK_P} -port -name {PIN_NUMBER} -value {U4}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ASIC_OUTCLK_P failed}}
if [catch {set_attribute {ASIC_SHAPERRST} -port -name {PIN_NUMBER} -value {p6}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ASIC_SHAPERRST failed}}
if [catch {set_attribute {ASIC_SHIFTIN} -port -name {PIN_NUMBER} -value {n8}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ASIC_SHIFTIN failed}}
if [catch {set_attribute {ASIC_SHIFTOUT} -port -name {PIN_NUMBER} -value {p8}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ASIC_SHIFTOUT failed}}
if [catch {set_attribute {ASIC_SRCK} -port -name {PIN_NUMBER} -value {p7}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ASIC_SRCK failed}}
if [catch {set_attribute {ASIC_TESTINJECT} -port -name {PIN_NUMBER} -value {r5}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on ASIC_TESTINJECT failed}}
if [catch {set_attribute {CLK_IN} -port -name {PIN_NUMBER} -value {B10}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on CLK_IN failed}}
if [catch {set_attribute {LA(0)} -port -name {PIN_NUMBER} -value {a13}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on LA(0) failed}}
if [catch {set_attribute {LA(1)} -port -name {PIN_NUMBER} -value {b13}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on LA(1) failed}}
if [catch {set_attribute {LA(2)} -port -name {PIN_NUMBER} -value {b14}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on LA(2) failed}}
if [catch {set_attribute {LA(3)} -port -name {PIN_NUMBER} -value {a14}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on LA(3) failed}}
if [catch {set_attribute {LA(4)} -port -name {PIN_NUMBER} -value {a16}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on LA(4) failed}}
if [catch {set_attribute {LA(5)} -port -name {PIN_NUMBER} -value {b11}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on LA(5) failed}}
if [catch {set_attribute {LA(6)} -port -name {PIN_NUMBER} -value {a11}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on LA(6) failed}}
if [catch {set_attribute {LA(7)} -port -name {PIN_NUMBER} -value {a8}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on LA(7) failed}}
if [catch {set_attribute {LA(8)} -port -name {PIN_NUMBER} -value {a7}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on LA(8) failed}}
if [catch {set_attribute {SER_NUM_CS_B} -port -name {PIN_NUMBER} -value {p13}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on SER_NUM_CS_B failed}}
if [catch {set_attribute {SPI_DIN} -port -name {PIN_NUMBER} -value {v12}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on SPI_DIN failed}}
if [catch {set_attribute {SPI_SCLK} -port -name {PIN_NUMBER} -value {p12}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on SPI_SCLK failed}}
if [catch {set_attribute {TEC_ADC_CS_B} -port -name {PIN_NUMBER} -value {t12}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on TEC_ADC_CS_B failed}}
if [catch {set_attribute {TEC_DAC_LDAC_B} -port -name {PIN_NUMBER} -value {r13}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on TEC_DAC_LDAC_B failed}}
if [catch {set_attribute {TEC_DAC_SYNC_B} -port -name {PIN_NUMBER} -value {n12}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on TEC_DAC_SYNC_B failed}}
if [catch {set_attribute {TEC_ENABLE} -port -name {PIN_NUMBER} -value {t16}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on TEC_ENABLE failed}}
if [catch {set_attribute {TEMP_SENSOR_CS_B} -port -name {PIN_NUMBER} -value {r14}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on TEMP_SENSOR_CS_B failed}}
if [catch {set_attribute {USB_FD(0)} -port -name {PIN_NUMBER} -value {e15}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FD(0) failed}}
if [catch {set_attribute {USB_FD(1)} -port -name {PIN_NUMBER} -value {e16}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FD(1) failed}}
if [catch {set_attribute {USB_FD(10)} -port -name {PIN_NUMBER} -value {m15}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FD(10) failed}}
if [catch {set_attribute {USB_FD(11)} -port -name {PIN_NUMBER} -value {m14}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FD(11) failed}}
if [catch {set_attribute {USB_FD(12)} -port -name {PIN_NUMBER} -value {n18}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FD(12) failed}}
if [catch {set_attribute {USB_FD(13)} -port -name {PIN_NUMBER} -value {n15}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FD(13) failed}}
if [catch {set_attribute {USB_FD(14)} -port -name {PIN_NUMBER} -value {p18}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FD(14) failed}}
if [catch {set_attribute {USB_FD(15)} -port -name {PIN_NUMBER} -value {p17}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FD(15) failed}}
if [catch {set_attribute {USB_FD(2)} -port -name {PIN_NUMBER} -value {f14}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FD(2) failed}}
if [catch {set_attribute {USB_FD(3)} -port -name {PIN_NUMBER} -value {f15}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FD(3) failed}}
if [catch {set_attribute {USB_FD(4)} -port -name {PIN_NUMBER} -value {f17}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FD(4) failed}}
if [catch {set_attribute {USB_FD(5)} -port -name {PIN_NUMBER} -value {f18}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FD(5) failed}}
if [catch {set_attribute {USB_FD(6)} -port -name {PIN_NUMBER} -value {g15}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FD(6) failed}}
if [catch {set_attribute {USB_FD(7)} -port -name {PIN_NUMBER} -value {g16}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FD(7) failed}}
if [catch {set_attribute {USB_FD(8)} -port -name {PIN_NUMBER} -value {m18}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FD(8) failed}}
if [catch {set_attribute {USB_FD(9)} -port -name {PIN_NUMBER} -value {m16}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FD(9) failed}}
if [catch {set_attribute {USB_FIFOADR(0)} -port -name {PIN_NUMBER} -value {K15}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FIFOADR(0) failed}}
if [catch {set_attribute {USB_FIFOADR(1)} -port -name {PIN_NUMBER} -value {K14}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FIFOADR(1) failed}}
if [catch {set_attribute {USB_FLAGB} -port -name {PIN_NUMBER} -value {H18}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FLAGB failed}}
if [catch {set_attribute {USB_FLAGC} -port -name {PIN_NUMBER} -value {H17}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_FLAGC failed}}
if [catch {set_attribute {USB_IFCLK} -port -name {PIN_NUMBER} -value {J14}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_IFCLK failed}}
if [catch {set_attribute {USB_PKTEND} -port -name {PIN_NUMBER} -value {L18}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_PKTEND failed}}
if [catch {set_attribute {USB_RESET_B} -port -name {PIN_NUMBER} -value {L16}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_RESET_B failed}}
if [catch {set_attribute {USB_SCL} -port -name {PIN_NUMBER} -value {C18}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_SCL failed}}
if [catch {set_attribute {USB_SDA} -port -name {PIN_NUMBER} -value {D17}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_SDA failed}}
if [catch {set_attribute {USB_SLOE} -port -name {PIN_NUMBER} -value {J15}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_SLOE failed}}
if [catch {set_attribute {USB_SLRD} -port -name {PIN_NUMBER} -value {T18}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_SLRD failed}}
if [catch {set_attribute {USB_SLWR} -port -name {PIN_NUMBER} -value {T17}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_SLWR failed}}
if [catch {set_attribute {USB_WAKEUP_B} -port -name {PIN_NUMBER} -value {L15}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_WAKEUP_B failed}}
if [catch {set_attribute {USB_WU2} -port -name {PIN_NUMBER} -value {D16}} result]\
	{puts {Warning: set_attribute PIN_NUMBER on USB_WU2 failed}}
