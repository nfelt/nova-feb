###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/vio/view_1_precision_vio_XRTL -design rtl 
set_attribute -name COREGEN -value "TRUE" /test_lib/vio/view_1_precision_vio_XRTL -design rtl 

set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/icon/view_1_precision_icon_XRTL -design rtl 
set_attribute -name COREGEN -value "TRUE" /test_lib/icon/view_1_precision_icon_XRTL -design rtl 

set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_11_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_10_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_9_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_8_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_7_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_6_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_5_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_4_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_3_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_2_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_1_u -design rtl 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_0_u -design rtl 

set_attribute -name iostandard -value "DEFAULT" -instance -type string I12/u -design rtl 

set_attribute -name iostandard -value "DEFAULT" -instance -type string I13/u -design rtl 

set_attribute -name CLKDV_DIVIDE -value "2.0" -instance -type default U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKFX_DIVIDE -value "2" -instance -type integer U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKFX_MULTIPLY -value "3" -instance -type integer U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKIN_DIVIDE_BY_2 -value "0" -instance -type default U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKIN_PERIOD -value "62.5" -instance -type default U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLKOUT_PHASE_SHIFT -value "NONE" -instance -type string U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name CLK_FEEDBACK -value "1X" -instance -type string U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name DESKEW_ADJUST -value "SYSTEM_SYNCHRONOUS" -instance -type string U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name DFS_FREQUENCY_MODE -value "LOW" -instance -type string U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name DLL_FREQUENCY_MODE -value "LOW" -instance -type string U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name DUTY_CYCLE_CORRECTION -value "1" -instance -type default U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name FACTORY_JF -value "c080" -instance -type default U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name PHASE_SHIFT -value "0" -instance -type integer U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name STARTUP_WAIT -value "1" -instance -type default U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_5/U_3/CLK_IN_DCM -design rtl 
set_attribute -name ram_processed -value "true" -instance U_2/U_1/PEDESTAL_TABLE/PEDESTAL_TABLE -design rtl 

set_attribute -name ram_processed -value "true" -instance U_2/U_1/THRESHOLD_TABLE/THRESHOLD_TABLE -design rtl 



set_attribute -name ram_processed -value "true" -instance U_0/ADC_BUFFER/ADC_BUFFER -design rtl 


set_attribute -name state_vector -value "current_state" -type string /work/usb_translator_notri/fsm_unfold_737_XRTL -design rtl 

##################
# Clocks
##################
create_clock { U_1/reg_DATA_CH_00_15(0)/out } -domain ClockDomain1 -name U_1/reg_DATA_CH_00_15(0)/out -period 10.000000 -waveform { 0.000000 5.000000 } -design rtl 
create_clock { U_1/reg_DATA_CH_00_15(7)/out } -domain ClockDomain8 -name U_1/reg_DATA_CH_00_15(7)/out -period 10.000000 -waveform { 0.000000 5.000000 } -design rtl 
create_clock { U_5/U_0/reg_CONTOLLER_STATE(1)/out } -domain ClockDomain15 -name U_5/U_0/reg_CONTOLLER_STATE(1)/out -period 10.000000 -waveform { 0.000000 5.000000 } -design rtl 
create_clock { U_1/reg_DATA_CH_00_15(1)/out } -domain ClockDomain2 -name U_1/reg_DATA_CH_00_15(1)/out -period 10.000000 -waveform { 0.000000 5.000000 } -design rtl 
create_clock { U_1/reg_DATA_CH_00_15(8)/out } -domain ClockDomain9 -name U_1/reg_DATA_CH_00_15(8)/out -period 10.000000 -waveform { 0.000000 5.000000 } -design rtl 
create_clock { U_1/reg_DATA_CH_00_15(5)/out } -domain ClockDomain6 -name U_1/reg_DATA_CH_00_15(5)/out -period 10.000000 -waveform { 0.000000 5.000000 } -design rtl 
create_clock { icon_1/icon/u_icon/i_yes_bscan/u_bs/i_v2/u_bs/DRCK1 } -domain ClockDomain16 -name icon_1/icon/u_icon/i_yes_bscan/u_bs/i_v2/u_bs/DRCK1 -period 5.000000 -waveform { 0.000000 2.500000 } -design rtl 
create_clock { U_1/reg_DATA_CH_00_15(2)/out } -domain ClockDomain3 -name U_1/reg_DATA_CH_00_15(2)/out -period 10.000000 -waveform { 0.000000 5.000000 } -design rtl 
create_clock { U_1/reg_DATA_CH_00_15(9)/out } -domain ClockDomain10 -name U_1/reg_DATA_CH_00_15(9)/out -period 10.000000 -waveform { 0.000000 5.000000 } -design rtl 
create_clock { icon_1/icon/u_icon/i_yes_bscan/u_bs/i_v2/u_bs/UPDATE } -domain ClockDomain17 -name icon_1/icon/u_icon/i_yes_bscan/u_bs/i_v2/u_bs/UPDATE -period 5.000000 -waveform { 0.000000 2.500000 } -design rtl 
create_clock { U_1/reg_DATA_CH_00_15(3)/out } -domain ClockDomain4 -name U_1/reg_DATA_CH_00_15(3)/out -period 10.000000 -waveform { 0.000000 5.000000 } -design rtl 
create_clock { U_5/U_3/CLK_IN_DCM/CLK0 } -domain ClockDomain0 -name U_5/U_3/CLK_IN_DCM/CLK0 -period 5.000000 -waveform { 0.000000 2.500000 } -design rtl 
create_clock { U_5/U_3/ASIC_BGCE/O } -domain ClockDomain0 -name U_5/U_3/ASIC_BGCE/O -period 5.000000 -waveform { 0.000000 2.500000 } -design rtl 
create_clock { U_1/reg_DATA_CH_00_15(10)/out } -domain ClockDomain11 -name U_1/reg_DATA_CH_00_15(10)/out -period 10.000000 -waveform { 0.000000 5.000000 } -design rtl 
create_clock { U_1/reg_DATA_CH_00_15(11)/out } -domain ClockDomain12 -name U_1/reg_DATA_CH_00_15(11)/out -period 10.000000 -waveform { 0.000000 5.000000 } -design rtl 
create_clock { U_5/U_3/CLK_IN_DCM/CLK180 } -domain ClockDomain0 -name U_5/U_3/CLK_IN_DCM/CLK180 -period 5.000000 -waveform { 0.000000 2.500000 } -design rtl 
create_clock { U_1/reg_DATA_CH_00_15(4)/out } -domain ClockDomain5 -name U_1/reg_DATA_CH_00_15(4)/out -period 10.000000 -waveform { 0.000000 5.000000 } -design rtl 
create_clock { U_2/U_0/reg_FMAT_DATA_STRB/out } -domain ClockDomain13 -name U_2/U_0/reg_FMAT_DATA_STRB/out -period 10.000000 -waveform { 0.000000 5.000000 } -design rtl 
create_clock { U_5/U_3/CLK_IN_DCM/CLK2X } -domain ClockDomain0 -name U_5/U_3/CLK_IN_DCM/CLK2X -period 5.000000 -waveform { 0.000000 2.500000 } -design rtl 
create_clock { U_1/reg_DATA_CH_00_15(6)/out } -domain ClockDomain7 -name U_1/reg_DATA_CH_00_15(6)/out -period 10.000000 -waveform { 0.000000 5.000000 } -design rtl 
create_clock { U_1/reg_CHAN_0_STRB/out } -domain ClockDomain14 -name U_1/reg_CHAN_0_STRB/out -period 10.000000 -waveform { 0.000000 5.000000 } -design rtl 

