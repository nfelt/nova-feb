###################################################################################
# Mentor Graphics Corporation
#
# This file is not a constraints report, nor does it list all the
# Tech constraints in the design. This file is created and used by Precision
# to track user Tech constraints that are set during design iterations.
# You should not edit this file because doing so might cause improper
# constraints in the design.
#
# If you want to list all Tech design constraints, use the command
#       report_constraints -design gatelevel
# or double-click on the Tech Constraints Report node in the Output Files
# Folder in the GUI.
#
# For a detailed discussion of how to set constraints, please refer to
# Precision documentation which is available from the Help pulldown menu.
###################################################################################

#################
# Attributes
#################
set_attribute -name PRESERVE_SIGNAL -value "TRUE" -net ASIC_CLK -design gatelevel 
set_attribute -name PRESERVE_SIGNAL -value "TRUE" -net BEEBUS_CLK -design gatelevel 
set_attribute -name PRESERVE_SIGNAL -value "TRUE" -net LA(0) -design gatelevel 
set_attribute -name PRESERVE_SIGNAL -value "TRUE" -net LA(1) -design gatelevel 

##################
# Clocks
##################
create_clock { CLK_IN } -domain ClockDomain0 -name CLK_IN -period 50.000000 -waveform { 0.000000 25.000000 } -design gatelevel 

