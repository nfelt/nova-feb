###################################################################################
# Mentor Graphics Corporation
#
# This file is not a constraints report, nor does it list all the
# RTL constraints in the design. This file is created and used by Precision
# to track user RTL constraints that are set during design iterations.
# You should not edit this file because doing so might cause improper
# constraints in the design.
#
# If you want to list all RTL design constraints, use the command
#       report_constraints -design rtl
# or double-click on the RTL Constraints Report node in the Output Files
# Folder in the GUI.
#
# For a detailed discussion of how to set constraints, please refer to
# Precision documentation which is available from the Help pulldown menu.
###################################################################################

#################
# Attributes
#################
set_attribute -name PRESERVE_SIGNAL -value "TRUE" -net LA(1) -design rtl 
set_attribute -name PRESERVE_SIGNAL -value "TRUE" -net LA(0) -design rtl 
set_attribute -name PRESERVE_SIGNAL -value "TRUE" -net BEEBUS_CLK -design rtl 
set_attribute -name PRESERVE_SIGNAL -value "TRUE" -net U_9/CLK_32 -design rtl 
set_attribute -name PRESERVE_SIGNAL -value "TRUE" -net U_9/ASIC_CLK -design rtl 

##################
# Clocks
##################
create_clock { CLK_IN } -domain ClockDomain3 -name CLK_IN -period 50.000000 -waveform { 0.000000 25.000000 } -design rtl 
create_clock { U_9/DCM_SP_INST/CLK2X } -domain ClockDomain3 -name U_9/DCM_SP_INST/CLK2X -period 25.000000 -waveform { 0.000000 12.500000 } -design rtl 

