###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name PRESERVE_SIGNAL -value "TRUE" -net BEEBUS_CLK -design gatelevel 
set_attribute -name PRESERVE_SIGNAL -value "TRUE" -net CLK_32_dup_326 -design gatelevel 
set_attribute -name PRESERVE_SIGNAL -value "TRUE" -net LA(0) -design gatelevel 
set_attribute -name PRESERVE_SIGNAL -value "TRUE" -net LA(1) -design gatelevel 
set_attribute -name PRESERVE_SIGNAL -value "TRUE" -net LA(2) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(11) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(10) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(9) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(8) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(7) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(6) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(5) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(4) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(3) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(2) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(1) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(0) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(11) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(10) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(9) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(8) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(7) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(6) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(5) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(4) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(3) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(2) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(1) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(0) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port CLK_IN -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_CLKINN -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_CLKINP -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_OUTCLK_N -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_OUTCLK_P -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port DCM_EMU_CLK_N -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port DCM_EMU_CLK_P -design gatelevel 
set_attribute -name CLK_FEEDBACK -value "2X" -instance -type string U_9_DCM_SP_INST -design gatelevel 
set_attribute -name CLKDV_DIVIDE -value "2.0" -instance -type string U_9_DCM_SP_INST -design gatelevel 
set_attribute -name CLKFX_DIVIDE -value "2" -instance -type string U_9_DCM_SP_INST -design gatelevel 
set_attribute -name CLKFX_MULTIPLY -value "2" -instance -type string U_9_DCM_SP_INST -design gatelevel 
set_attribute -name CLKIN_DIVIDE_BY_2 -value "FALSE" -instance -type string U_9_DCM_SP_INST -design gatelevel 
set_attribute -name CLKIN_PERIOD -value "62.500" -instance -type string U_9_DCM_SP_INST -design gatelevel 
set_attribute -name CLKOUT_PHASE_SHIFT -value "NONE" -instance -type string U_9_DCM_SP_INST -design gatelevel 
set_attribute -name DESKEW_ADJUST -value "SYSTEM_SYNCHRONOUS" -instance -type string U_9_DCM_SP_INST -design gatelevel 
set_attribute -name DFS_FREQUENCY_MODE -value "LOW" -instance -type string U_9_DCM_SP_INST -design gatelevel 
set_attribute -name DLL_FREQUENCY_MODE -value "LOW" -instance -type string U_9_DCM_SP_INST -design gatelevel 
set_attribute -name DUTY_CYCLE_CORRECTION -value "TRUE" -instance -type string U_9_DCM_SP_INST -design gatelevel 
set_attribute -name FACTORY_JF -value "C080" -instance -type string U_9_DCM_SP_INST -design gatelevel 
set_attribute -name PHASE_SHIFT -value "0" -instance -type string U_9_DCM_SP_INST -design gatelevel 
set_attribute -name STARTUP_WAIT -value "TRUE" -instance -type string U_9_DCM_SP_INST -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_11_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_11_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_10_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_10_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_9_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_9_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_8_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_8_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_7_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_7_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_6_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_6_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_5_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_5_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_4_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_4_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_3_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_3_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_2_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_2_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_1_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_1_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_0_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_0_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I12_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I13_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I14_u -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_9_reg_ASIC_CLK_EN_int -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(11) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(10) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(9) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(8) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(7) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(6) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(5) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(4) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(3) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(2) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(1) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1_reg_DATA_CH_00_15(0) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_9_reg_ASIC_on_int_repl2 -design gatelevel 
set_attribute -name PART -value "3s1600efg320-5" -type string /work/top/struct -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/encode_8b10b/view_1_precision_encode_8b10b -design gatelevel 
set_attribute -name COREGEN -value "TRUE" /test_lib/encode_8b10b/view_1_precision_encode_8b10b -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/XST_GND -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/XST_VCC -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_8 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/dout_9 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/disp_run -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/k_invalid -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/k_invalid_cmp_eq00001 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/k_invalid_cmp_eq00001 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)61 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)61 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/k28_0_and00001 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/k28_0_and00001 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)41 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)41 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)21 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)21 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)111 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)111 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/pdes61_SW0 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/pdes61_SW0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)1_SW0 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)1_SW0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/k_invalid_mux000312 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/k_invalid_mux000312 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/k_invalid_mux000396 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/k_invalid_mux000396 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_pdes411 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_pdes411 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)98 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)98 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)133 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)133 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)2 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)_f5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)_f6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)3 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)4 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)_f5_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)5 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)6 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)_f5_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)_f6_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(0)_f7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)113 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)113 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)131 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)131 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)146 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)146 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)275 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)275 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b421 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b421 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b422 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b422 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b423 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b423 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b431 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b431 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b432 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b432 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)2 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)_f5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)3 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)_f6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)4 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)5 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)_f5_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)6 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)_f6_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(3)_f7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)2 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)_f5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)3 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)_f6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)4 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)5 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)6 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)_f5_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)_f6_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(1)_f7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)210 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)210 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b4135 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b4135 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW0 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b4180 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b4180 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/pdes62 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/pdes62 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)259_SW0 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)259_SW0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)259_SW1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)259_SW1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)259 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)259 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/pdes62_SW0 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/pdes62_SW0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b4180_SW0 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b4180_SW0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b44_f5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b444 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b444 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b445 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b445 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b43_f5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b42_f5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b42_f6 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b42_f6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b43_f6 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b43_f6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b44_f6 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b44_f6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW2_F -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW2_F -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW2_G -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW2_G -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW3_F -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW3_F -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW3_G -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW3_G -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW4_F -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW4_F -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW4_G -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW4_G -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW5_F -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW5_F -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW5_G -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW5_G -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW6_F -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW6_F -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW6_G -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW6_G -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW7_F -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW7_F -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW7_G -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/a720_SW7_G -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/k_invalid_mux000364 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/k_invalid_mux000364 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)10_SW1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)10_SW1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)69_SW0 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)69_SW0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)69_SW1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)69_SW1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)69 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)69 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/disp_run_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/disp_run_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172_SW1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172_SW1_F -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172_SW1_F -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172_SW1_G -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172_SW1_G -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)141 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)141_F -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)141_F -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)141_G -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)141_G -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)69_SW01 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)69_SW01 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)69_SW02 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)69_SW02 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(5)69_SW0_f5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172_SW01 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172_SW01 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(4)172_SW0_f5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)169 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/b6(2)169 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b433 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b433 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b4180_SW1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b4180_SW1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b443 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_10/DCM_COMM_ENCODE/BU2/U0/first_encoder/Mmux_b443 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/encode_8b10b_encode_8b10b_xst_1/view_1_precision_encode_8b10b_encode_8b10b_xst_1 -design gatelevel 
set_attribute -name NDF -value "TRUE" /test_lib/encode_8b10b_encode_8b10b_xst_1/view_1_precision_encode_8b10b_encode_8b10b_xst_1 -design gatelevel 
set_attribute -name contains_init_values -value "true" /test_lib/encode_8b10b_encode_8b10b_xst_1/view_1_precision_encode_8b10b_encode_8b10b_xst_1 -design gatelevel 
set_attribute -name NGC_DONTTOUCH -value "true" /test_lib/encode_8b10b_encode_8b10b_xst_1/view_1_precision_encode_8b10b_encode_8b10b_xst_1 -design gatelevel 



set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_0/ADC_BUFFER/ix13887z26572 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_0/ADC_BUFFER/ix9899z26572 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_0/ADC_BUFFER/ix34372z26572 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_0/ADC_BUFFER/ix38360z26572 -design gatelevel 


set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_SLRD_cld -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(15) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(14) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(13) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(12) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(11) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(10) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(9) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(8) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(7) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(6) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(5) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(4) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(3) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(2) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(1) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_6/reg_P_TYPE(0) -design gatelevel 
set_attribute -name state_vector -value "current_state" /work/usb_translator_notri/fsm_unfold_737 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/decode_8b10b/view_1_precision_decode_8b10b -design gatelevel 
set_attribute -name COREGEN -value "TRUE" /test_lib/decode_8b10b/view_1_precision_decode_8b10b -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/XST_GND -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f6_3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5_8 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000020 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000020 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000019 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000019 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5_7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000018 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000018 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000017 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000017 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f6_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5_6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000016 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000016 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000015 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000015 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5_5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000014 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000014 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000013 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000013 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f6_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5_4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000012 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000012 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000011 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000011 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5_3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000010 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom000010 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00009 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00009 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f6_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00008 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00008 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00007 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00007 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00006 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00006 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00005 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00005 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00004 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00004 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00003 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00003 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom0000_f5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00002 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00002 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00001 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/Mrom_din_5_0_rom00001 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/dout_7 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/dout_6 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/dout_5 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/dout_4 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/dout_3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/dout_2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/dout_1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/dout_0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/kout -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/b3(5)31 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/b3(5)31 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/b3(5)11 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/b3(5)11 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/b3(6)1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/b3(6)1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/b3(5)1 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/b3(5)1 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/b3(5)21 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/b3(5)21 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/k28_or0000_SW0 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/k28_or0000_SW0 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/k28_or0000 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/k28_or0000 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/k15 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/k15 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/k47 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/k47 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/b3(7)_SW2 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/b3(7)_SW2 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/b3(7)_SW3 -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/b3(7)_SW3 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/b3(7) -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/b3(7) -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/k67 -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/k67_F -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/k67_F -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/k67_G -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" -instance U_4_U_11_DCM_COMM_DECODE/BU2/U0/first_decoder/k67_G -design gatelevel 
set_attribute -name XSTLIB -value "TRUE" -instance -type boolean U_4_U_11_DCM_COMM_DECODE/BU2/XST_VCC -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/decode_8b10b_decode_8b10b_xst_1/view_1_precision_decode_8b10b_decode_8b10b_xst_1 -design gatelevel 
set_attribute -name NDF -value "TRUE" /test_lib/decode_8b10b_decode_8b10b_xst_1/view_1_precision_decode_8b10b_decode_8b10b_xst_1 -design gatelevel 
set_attribute -name contains_init_values -value "true" /test_lib/decode_8b10b_decode_8b10b_xst_1/view_1_precision_decode_8b10b_decode_8b10b_xst_1 -design gatelevel 
set_attribute -name NGC_DONTTOUCH -value "true" /test_lib/decode_8b10b_decode_8b10b_xst_1/view_1_precision_decode_8b10b_decode_8b10b_xst_1 -design gatelevel 


set_attribute -name IOB -value "TRUE" -instance -type string U_5_U_1/reg_ASIC_INTEGRST -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_5_U_1/reg_ASIC_CHIPRESET_repl1 -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_5_U_1/reg_ASIC_SHIFT_REGISTER(0)_repl0 -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_5_U_1/reg_ASIC_SRCK_int_repl0 -design gatelevel 

##################
# Clocks
##################
create_clock { U_10/reg_CLK_3_2_sig/Q } -domain ClockDomain1 -name U_10/reg_CLK_3_2_sig/out -period 50.000000 -waveform { 0.000000 25.000000 } -design gatelevel 
create_clock { U_4_U_11_reg_CLK_3_2_sig/Q } -domain ClockDomain2 -name U_4/U_11/reg_CLK_3_2_sig/out -period 50.000000 -waveform { 0.000000 25.000000 } -design gatelevel 
create_clock { CLK_IN } -domain ClockDomain0 -name CLK_IN -period 50.000000 -waveform { 0.000000 25.000000 } -design gatelevel 
create_clock { U_9_DCM_SP_INST/CLK2X } -domain ClockDomain3 -name U_9/DCM_SP_INST/CLK2X -period 25.000000 -waveform { 0.000000 12.500000 } -design gatelevel 

