restart -force -nowave
set clk_in_period 20
set clk_3_2_period [expr $clk_in_period * 10]
set clk_16_period [expr $clk_in_period * 2]
set clk_32_period $clk_in_period
set clk_64_period [expr $clk_in_period / 2]
set dcm_clk_period $clk_in_period

# comment the following when DCM and change the DCM_CLK stimulus clock testing.
force -freeze sim:/top/u_9/clk_3_2 1 0, 0 [expr $clk_3_2_period /2] ns -r $clk_3_2_period ns
force -freeze sim:/top/u_9/clk_16_int 1 0, 0 [expr $clk_16_period /2] ns -r $clk_16_period ns
force -freeze sim:/top/u_9/clk_32_int 1 0, 0 [expr $clk_32_period /2] ns -r $clk_32_period ns
force -freeze sim:/top/u_9/clk_64_int 1 0, 0 [expr $clk_64_period /2] ns -r $clk_64_period ns
#force -freeze sim:/top/dcm_clk 1 0, 0 [expr $dcm_clk_period /2] ns -r $dcm_clk_period ns
force -freeze sim:/top/u_9/dcm_32_locked 1 {200 ns}

force -freeze sim:/top/usb_flagb false 0
force -freeze sim:/top/usb_flagc true 0
force -drive sim:/top/usb_fd ZZZZZZZZZZZZZZZZ 0
force -deposit sim:/top/u_4/beebus_addr ZZZZZZZZZZZZZZZZ 0
force -deposit sim:/top/u_6/beebus_addr 0000000000000000 0
force -deposit sim:/top/u_2/u_1/chan_data(0).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(1).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(2).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(3).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(4).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(5).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(6).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(7).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(8).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(9).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(10).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(11).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(12).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(13).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(14).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(15).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(16).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(17).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(18).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(19).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(20).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(21).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(22).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(23).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(24).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(25).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(26).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(27).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(28).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(29).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(30).magnitude 16#000 0
force -deposit sim:/top/u_2/u_1/chan_data(31).magnitude 16#000 0

run [expr $clk_16_period * 8] ns

view wave 

proc xml {xml_file} {
global clk_16_period
puts $xml_file.xml
set infile [open $xml_file  r]
set outfile [open logfile.txt w]
set EPT0_FIFO_FULL false
set EPT2_FIFO_FULL false
set EPT0_FIFO_EMPTY true
set EPT2_FIFO_EMPTY true
set WAIT_FOR_PACKET false

while {-1 != [gets $infile command]} {
    set i [string first \" $command]
    if {$i != -1} then {
	set j [string last \"\/ $command"]
	set time [string range $command [expr $i+1] [expr $j-1]]
	set command [string range $command 0 [expr $i -2]]
    }
    switch $command {
	puts $command
	"<writeblock>" {
	    puts "sending packeta"
	    set EPT0_FIFO_FULL true
	    set EPT0_FIFO_EMPTY false
	}
	"<readblock/>" {
	    puts "receiving  packet"
	    set WAIT_FOR_PACKET true
	}
 	"<sleep time" {
	    puts "sleep for $time ms"
	    run  [expr $time *10]us
	}
    }

    while {$EPT0_FIFO_FULL || $WAIT_FOR_PACKET} {
	puts [examine -hex sim:/top/usb_fifoadr]
	switch [examine -hex sim:/top/usb_fifoadr] {
	    0 {
		puts "Pointing to USB address 0"
		puts "Wait for packet = "
		puts $WAIT_FOR_PACKET
		force -freeze sim:/top/usb_flagb $EPT0_FIFO_FULL 0
		force -freeze sim:/top/usb_flagc $EPT0_FIFO_EMPTY 0
		if {[examine sim:/top/usb_sloe] == "true"} then {
		    gets $infile EPT0_FD
		    while {$EPT0_FIFO_FULL == "true"} {
			force -drive sim:/top/usb_fd 16\#$EPT0_FD 0
			gets $infile EPT0_FD
			if {"</writeblock>" == $EPT0_FD} then {
			    set EPT0_FIFO_FULL false
			    set EPT0_FIFO_EMPTY true
			    force -freeze sim:/top/usb_flagb $EPT0_FIFO_FULL 0
			    force -freeze sim:/top/usb_flagc $EPT0_FIFO_EMPTY 0
			}
			run $clk_16_period ns
		    }
		    force -drive sim:/top/usb_fd ZZZZZZZZZZZZZZZZ 0
		}
	    }

	    2 {
		puts "Pointing to USB address 2"
		force -freeze sim:/top/usb_flagb $EPT2_FIFO_FULL 1
		force -freeze sim:/top/usb_flagc $EPT2_FIFO_EMPTY 0
		while {[examine sim:/top/usb_slwr] == "true"} {
		    puts $outfile [examine -hex sim:/top/usb_fd]
		    set WAIT_FOR_PACKET false
		    run $clk_16_period ns
		}
	    }

	    default {   
		puts "aaie nutin"
	    }
	}
	run $clk_16_period ns
    }
}
close $infile
close $outfile
}

