clear all;
% ! C:\data\daq\novacmdapp -o -e -f C:\data\daq\cmds\ped_trig.xml C:\data\daq\cmds\outfile.txt
% ! novacmdapp -o -e -f scripts\feb_test.xml data\txt\outfile.txt
% ! "C:\Program Files\XEmacs\XEmacs-21.4.20\i586-pc-win32\xemacs.exe"
% C:\data\daq\cmds\outfile.txt
number_of_channels = 32;
m = char(textread('/home/nate/Desktop/outfile.txt', '%s', 'whitespace', ' -'));
chan_length = length(m)/number_of_channels;
chan_nun= hex2dec(m(:,1));
chan_data = hex2dec(m(:,2:4));
chan_data = reshape(chan_data,number_of_channels,chan_length)';
size(chan_data)
chan_0_index = -find((chan_nun(1:32)==15) & (chan_nun(2:33)==0))+1;
chan_data_tmp = circshift(chan_data,[0,chan_0_index]);
%chan_data(:,[1:(number_of_channels/2)]) = chan_data_tmp (:,[2:2:number_of_channels]);
%chan_data(:,[(number_of_channels/2+1):number_of_channels]) = chan_data_tmp (:,[1:2:number_of_channels]);


DCS = [1 0 0 0 -1];
chan_data = filter(DCS,1,chan_data);
chan_data = chan_data(5:length(chan_data),1:32);



Fs = 2e6;                    % Sampling frequency
T = 1/Fs;                     % Sample time
L = length(chan_data(:,1));                     % Length of signal
t = (0:L-1)*T;                % Time vector

NFFT = 2^nextpow2(L); % Next power of 2 from length of y
Y = fft(chan_data,NFFT)/L;
f = Fs/2*linspace(0,1,NFFT/2);
fm = [0:31];
% Plot single-sided amplitude spectrum.
figure(1);
mag = 2*abs(Y(1:NFFT/2,:));
surf(fm,f,mag), 
shading interp
title('Single-Sided Amplitude Spectrum of y(t)')
xlabel('Frequency (Hz)')
ylabel('|Y(f)|')
