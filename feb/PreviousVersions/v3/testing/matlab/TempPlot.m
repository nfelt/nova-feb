clear all;
reg_data = char(textread('/home/nate/Desktop/TempData.txt', '%s', 'whitespace', ' -'));
pick_index = 4:4:length(reg_data);
temp_data = reg_data(pick_index,:);
temp_data = hex2dec(temp_data(:,1:4));
temp_data = floor(temp_data/4)*.03125;
time_data = 0:1/360:(length(temp_data)/360)-1/360;
figure(1);
plot(time_data,temp_data);
title(['Temperature starting at powerup']);
%legend('ch0','ch1','ch2','ch3','ch4','ch5','ch6','ch7','ch8','ch9','ch10','ch11','ch12','ch13','ch14','ch15''ch16','ch17','ch18','ch19','ch20','ch21','ch22','ch22','ch23','ch24','ch25','ch26','ch27','ch28','ch29','ch30','ch31');
ylabel('Temp (c)');
xlabel('Time (h)');