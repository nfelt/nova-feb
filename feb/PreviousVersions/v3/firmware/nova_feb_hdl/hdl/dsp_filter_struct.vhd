-- VHDL Entity nova_feb.dsp_filter.symbol
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 14:34:07 06/18/09
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2008.1a (Build 9)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;
LIBRARY unisim;
USE unisim.all;
library harvard_std;
USE harvard_std.lppc_custom_fn_pkg.all;

ENTITY dsp_filter IS
   PORT( 
      BEEBUS_ADDR    : IN     unsigned (15 DOWNTO 0);
      BEEBUS_CLK     : IN     std_logic;
      BEEBUS_READ    : IN     boolean;
      BEEBUS_STRB    : IN     boolean;
      CHAN_NUMBER_IN : IN     unsigned (4 DOWNTO 0) := "00000";
      CLK            : IN     std_logic;
      DATA_CH_15_00  : IN     unsigned (11 DOWNTO 0);
      ENABLE_DAQ     : IN     boolean;
      OSCOPE_TRIGGER : IN     boolean;
      CHAN0STRB_M    : OUT    std_logic;
      CHAN0STRB_N    : OUT    std_logic;
      FMAT_DATA      : OUT    unsigned (15 DOWNTO 0);
      FMAT_DATA_STRB : OUT    boolean;
      BEEBUS_DATA    : INOUT  unsigned (31 DOWNTO 0)
   );

-- Declarations

END dsp_filter ;

--
-- VHDL Architecture nova_feb.dsp_filter.struct
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 14:34:07 06/18/09
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2008.1a (Build 9)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;
LIBRARY unisim;
USE unisim.all;
library harvard_std;USE harvard_std.lppc_custom_fn_pkg.all;

LIBRARY nova_feb;

ARCHITECTURE struct OF dsp_filter IS

   -- Architecture declarations

   -- Internal signal declarations
   SIGNAL CHAN_NUMBER      : unsigned(4 DOWNTO 0);
   SIGNAL CONTINUOUS_DATA  : boolean;
   SIGNAL GOT_ANYTHING_NEW : boolean;
   SIGNAL MAGNITUDE        : signed(12 DOWNTO 0);
   SIGNAL TIMESTAMP        : unsigned(31 DOWNTO 0);
   SIGNAL TRIGGER_STRB     : boolean;


   -- Component Declarations
   COMPONENT data_format
   PORT (
      CHAN_NUMBER      : IN     unsigned (4 DOWNTO 0);
      MAGNITUDE        : IN     signed (12 DOWNTO 0);
      TIMESTAMP        : IN     unsigned (31 DOWNTO 0);
      TRIGGER_STRB     : IN     boolean ;
      GOT_ANYTHING_NEW : OUT    boolean ;
      FMAT_DATA        : OUT    unsigned (15 DOWNTO 0);
      FMAT_DATA_STRB   : OUT    boolean ;
      CONTINUOUS_DATA  : IN     boolean ;
      CLK              : IN     std_logic 
   );
   END COMPONENT;
   COMPONENT dsp_dcs8
   PORT (
      DATA_in16        : IN     unsigned (11 DOWNTO 0) := x"000";
      BEEBUS_READ      : IN     boolean                := false;
      BEEBUS_STRB      : IN     boolean                := false;
      BEEBUS_CLK       : IN     std_logic ;
      BEEBUS_DATA      : INOUT  unsigned (31 DOWNTO 0) := x"00000000";
      BEEBUS_ADDR      : IN     unsigned (15 DOWNTO 0) := "0000000000000000";
      CHAN_NUMBER_IN   : IN     unsigned (4 DOWNTO 0)  := "00000";
      CHAN_NUMBER      : OUT    unsigned (4 DOWNTO 0)  := "00000";
      TIMESTAMP        : OUT    unsigned (31 DOWNTO 0) := x"00000000";
      MAGNITUDE        : OUT    signed (12 DOWNTO 0)   := "0000000000000";
      DATA_FOR_YOU     : OUT    boolean                := false;
      GOT_ANYTHING_NEW : IN     boolean                := false;
      CONTINUOUS_DATA  : OUT    boolean                := false;
      CLK              : IN     std_logic              := '1';
      OSCOPE_TRIGGER   : IN     boolean                := false;
      ENABLE_DAQ       : IN     boolean                := false
   );
   END COMPONENT;

   -- Optional embedded configurations
   -- pragma synthesis_off
   FOR ALL : data_format USE ENTITY nova_feb.data_format;
   FOR ALL : dsp_dcs8 USE ENTITY nova_feb.dsp_dcs8;
   -- pragma synthesis_on


BEGIN
   -- Architecture concurrent statements
   -- HDL Embedded Text Block 1 eb1
   -- eb1 1
   CHAN0STRB_N <= not(CHAN_NUMBER(4) or
                      CHAN_NUMBER(3) or
                      CHAN_NUMBER(2) or
                      CHAN_NUMBER(1) or
                      CHAN_NUMBER(0));
   CHAN0STRB_M <= (MAGNITUDE(5) or
                     MAGNITUDE(4) or
                     MAGNITUDE(3) or
                     MAGNITUDE(2) or
                     MAGNITUDE(1) or
                     MAGNITUDE(0));


   -- Instance port mappings.
   U_2 : data_format
      PORT MAP (
         CHAN_NUMBER      => CHAN_NUMBER,
         MAGNITUDE        => MAGNITUDE,
         TIMESTAMP        => TIMESTAMP,
         TRIGGER_STRB     => TRIGGER_STRB,
         GOT_ANYTHING_NEW => GOT_ANYTHING_NEW,
         FMAT_DATA        => FMAT_DATA,
         FMAT_DATA_STRB   => FMAT_DATA_STRB,
         CONTINUOUS_DATA  => CONTINUOUS_DATA,
         CLK              => CLK
      );
   U_1 : dsp_dcs8
      PORT MAP (
         DATA_in16        => DATA_CH_15_00,
         BEEBUS_READ      => BEEBUS_READ,
         BEEBUS_STRB      => BEEBUS_STRB,
         BEEBUS_CLK       => BEEBUS_CLK,
         BEEBUS_DATA      => BEEBUS_DATA,
         BEEBUS_ADDR      => BEEBUS_ADDR,
         CHAN_NUMBER_IN   => CHAN_NUMBER_IN,
         CHAN_NUMBER      => CHAN_NUMBER,
         TIMESTAMP        => TIMESTAMP,
         MAGNITUDE        => MAGNITUDE,
         DATA_FOR_YOU     => TRIGGER_STRB,
         GOT_ANYTHING_NEW => GOT_ANYTHING_NEW,
         CONTINUOUS_DATA  => CONTINUOUS_DATA,
         CLK              => CLK,
         OSCOPE_TRIGGER   => OSCOPE_TRIGGER,
         ENABLE_DAQ       => ENABLE_DAQ
      );

END struct;
