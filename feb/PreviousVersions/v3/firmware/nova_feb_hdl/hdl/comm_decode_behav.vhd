--
-- VHDL Architecture feb_p2_lib.comm_decode.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 16:42:19 11/ 7/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

ENTITY comm_decode IS
   PORT( 
      CLK_32       : IN     std_logic;
      CLK_3_2      : IN     std_logic;
      DATA_RX      : OUT    std_logic_vector (7 DOWNTO 0);
      DATA_TX      : IN     std_logic_vector (7 DOWNTO 0);
      DATA_TX_STRB : IN     boolean;
      DATA_RX_STRB : OUT    boolean;
      LINKED       : OUT    boolean;
      COMM_ERROR   : OUT    boolean;
      SERIAL_TX    : OUT    std_logic;
      SERIAL_RX    : IN     std_logic
   );

-- Declarations

END comm_decode ;


--
architecture behav of comm_decode is
  signal DATA_RX_int      : std_logic_vector(7 downto 0) := "00000000";
  signal DATA_RX_STRB_int : boolean                      := false;
  signal ENCODE_DIN       : std_logic_vector(7 downto 0) := "00000000";
  signal ENCODED_TX       : std_logic_vector(9 downto 0) := "0000000000";
  signal ENCODED_RX       : std_logic_vector(9 downto 0) := "0000000000";
  signal SHIFT_REG_TX     : std_logic_vector(9 downto 0) := "0000000000";
  signal SHIFT_REG_RX     : std_logic_vector(9 downto 0) := "0000000000";
  signal BIT_COUNT        : unsigned(3 downto 0)         := x"0";
  signal LINKED_int       : boolean                      := false;
  signal KOUT             : std_logic                    := '0';
  signal KIN              : std_logic                    := '0';

  component decode_8b10b
    port (
      clk      : in  std_logic;
      din      : in  std_logic_vector(9 downto 0);
      dout     : out std_logic_vector(7 downto 0);
      kout     : out std_logic;
      code_err : out std_logic);
  end component;

  component encode_8b10b
    port (
      din  : in  std_logic_vector(7 downto 0);
      kin  : in  std_logic;
      clk  : in  std_logic;
      dout : out std_logic_vector(9 downto 0));
  end component;

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;

  
begin
  DCM_COMM_DECODE : decode_8b10b
    port map (
      clk      => CLK_3_2,
      din      => ENCODED_RX,
      dout     => DATA_RX_int,
      kout     => KOUT,
      code_err => open
      );

  DCM_COMM_ENCODE : encode_8b10b

    port map (
      din  => ENCODE_DIN,
      kin  => KIN,
      clk  => CLK_3_2,
      dout => ENCODED_TX
      );

  FRAME_BITS : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '1' then
      BIT_COUNT <= BIT_COUNT +1;

      if (SHIFT_REG_RX = "1001111100") or
        (SHIFT_REG_RX = "0110000011") or
        (BIT_COUNT = 9) then
        BIT_COUNT  <= x"0";
        --FIX LINK STATUS AND ERROR FLAG
        COMM_ERROR <= LINKED_int and not BIT_COUNT = x"9";
        LINKED_int <= true;
      end if;

    end if;
  end process FRAME_BITS;

  SER_DES : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '1' then
      SHIFT_REG_TX <= '0' & SHIFT_REG_TX(9 downto 1);
      SHIFT_REG_RX <= SERIAL_RX & SHIFT_REG_RX(9 downto 1);

      if BIT_COUNT = 9 then
        SHIFT_REG_TX <= ENCODED_TX;
        ENCODED_RX   <= SHIFT_REG_RX;
      end if;
      
    end if;
  end process SER_DES;

-- the following will be changed to a dual port memory FIFO
  IN_BUFFER : process (CLK_3_2) is
  begin
    if CLK_3_2'event and CLK_3_2 = '1' then
      
      if DATA_TX_STRB then
        ENCODE_DIN <= DATA_TX;
        KIN        <= '0';
      else
        ENCODE_DIN <= x"3C";
        KIN        <= '1';
      end if;

    end if;
  end process IN_BUFFER;



  LATCH_RX : process (CLK_3_2) is
  begin
    if CLK_3_2'event and CLK_3_2 = '1' then
      DATA_RX_STRB <= DATA_RX_STRB_int;
      DATA_RX      <= DATA_RX_int;
    end if;
  end process LATCH_RX;
  DATA_RX_STRB_int <= KOUT = '0';
  SERIAL_TX        <= SHIFT_REG_TX(0);
  LINKED           <= LINKED_int;
end architecture behav;

