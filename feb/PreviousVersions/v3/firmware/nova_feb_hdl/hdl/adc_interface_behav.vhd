--
-- VHDL Architecture feb_p2_lib.adc_interface.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 11:43:21 01/22/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity adc_interface is
  port(
    D_IN_A         : in  std_logic_vector (11 downto 0);
    D_IN_B         : in  std_logic_vector (11 downto 0);
    DATA           : out unsigned (11 downto 0);
    CHAN_NUMBER    : out unsigned (4 downto 0);
    RESET_CHAN_PTR : in  boolean;
    ENABLE_DAQ     : in  boolean;
    CLK_16         : in  std_logic;
    CLK_64         : in  std_logic
    );

-- Declarations

end adc_interface;

--
architecture behav of adc_interface is
  signal CHAN_PTR         : unsigned (4 downto 0)  := "00000";
  signal DATA_CHAN_A_RISE : unsigned (11 downto 0) := x"000";
  signal DATA_CHAN_B_RISE : unsigned (11 downto 0) := x"000";
  signal DATA_CHAN_A_FALL : unsigned (11 downto 0) := x"000";
  signal DATA_CHAN_B_FALL : unsigned (11 downto 0) := x"000";

begin

  process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '1' then
      DATA_CHAN_A_RISE <= unsigned(D_IN_A);
      DATA_CHAN_B_RISE <= unsigned(D_IN_B);
    end if;
  end process;

  process (CLK_16) is
  begin
    if CLK_16'event and CLK_16 = '0' then
-- selecteither sim or ASIC friendly unsigned(not D_IN_A)
      DATA_CHAN_A_FALL <= unsigned(not D_IN_A);
      DATA_CHAN_B_FALL <= unsigned(D_IN_B);
    end if;
  end process;

  process (CLK_64) is
  begin
    if CLK_64'event and CLK_64 = '1' then
      if RESET_CHAN_PTR then
        CHAN_PTR <= "00000";
      elsif ENABLE_DAQ then
        CHAN_PTR    <= CHAN_PTR+1;
        CHAN_NUMBER <= CHAN_PTR(0) & not CHAN_PTR(1) & CHAN_PTR(4 downto 2);
      end if;
      case CHAN_PTR(1 downto 0) is
        when "00" =>
          DATA <= DATA_CHAN_A_FALL;
        when "01" =>
          DATA <= DATA_CHAN_B_FALL;
        when "10" =>
          DATA <= DATA_CHAN_A_RISE;
        when "11" =>
          DATA <= DATA_CHAN_B_RISE;
        when others => null;
      end case;
      -- untangle ASIC mux ADC operation.
    end if;
  end process;

end architecture behav;

