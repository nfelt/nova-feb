-- VHDL Entity nova_feb.dcm_comm_interface.symbol
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 14:28:25 02/22/10
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2008.1a (Build 9)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;
LIBRARY unisim;
USE unisim.all;
library harvard_std;
USE harvard_std.lppc_custom_fn_pkg.all;

ENTITY dcm_comm_interface IS
   PORT( 
      CLK_3_2     : IN     std_logic;
      DCM_COMMAND : IN     std_logic;
      DCM_SYNC    : IN     std_logic;
      RESET       : IN     boolean;
      clk_32      : IN     std_logic;
      COMM_ERROR  : OUT    boolean;
      DATA_STRB   : OUT    boolean;
      DCM_DATA    : OUT    std_logic;
      LINKED      : OUT    boolean;
      REGOUT_DATA : INOUT  std_logic_vector (7 DOWNTO 0)
   );

-- Declarations

END dcm_comm_interface ;

--
-- VHDL Architecture nova_feb.dcm_comm_interface.struct
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 14:28:26 02/22/10
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2008.1a (Build 9)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;
LIBRARY unisim;
USE unisim.all;
library harvard_std;
USE harvard_std.lppc_custom_fn_pkg.all;

LIBRARY nova_feb;

ARCHITECTURE struct OF dcm_comm_interface IS

   -- Architecture declarations

   -- Internal signal declarations
   SIGNAL BEEBUS_ADDR          : unsigned(15 DOWNTO 0);
   SIGNAL BEEBUS_DATA          : unsigned(15 DOWNTO 0);
   SIGNAL BEEBUS_READ          : boolean;
   SIGNAL BEEBUS_STRB          : boolean;
   SIGNAL DATA_RX              : std_logic_vector(7 DOWNTO 0);
   SIGNAL DATA_RX_STRB         : boolean;
   SIGNAL DCM_DATA_BEEBUS      : std_logic_vector(15 DOWNTO 0);
   SIGNAL DCM_DATA_BEEBUS_STRB : boolean;

   -- Implicit buffer signal declarations
   SIGNAL DCM_DATA_internal : std_logic;


   -- Component Declarations
   COMPONENT comm_decode
   PORT (
      CLK_32       : IN     std_logic ;
      CLK_3_2      : IN     std_logic ;
      DATA_RX      : OUT    std_logic_vector (7 DOWNTO 0);
      DATA_TX      : IN     std_logic_vector (7 DOWNTO 0);
      DATA_TX_STRB : IN     boolean ;
      DATA_RX_STRB : OUT    boolean ;
      LINKED       : OUT    boolean ;
      COMM_ERROR   : OUT    boolean ;
      SERIAL_TX    : OUT    std_logic ;
      SERIAL_RX    : IN     std_logic 
   );
   END COMPONENT;
   COMPONENT dcm_com_beebus_interface
   PORT (
      CLK_3_2              : IN     std_logic ;
      BEEBUS_ADDR          : OUT    unsigned (15 DOWNTO 0);
      BEEBUS_CLK           : OUT    std_logic ;
      BEEBUS_READ          : OUT    boolean ;
      ERROR_FLAG           : OUT    boolean ;
      BEEBUS_STRB          : OUT    boolean ;
      DCM_DATA_BEEBUS      : OUT    std_logic_vector (15 DOWNTO 0);
      DCM_DATA_BEEBUS_STRB : OUT    boolean ;
      DATA_RX              : IN     std_logic_vector (7 DOWNTO 0);
      DATA_RX_STRB         : IN     boolean ;
      RESET                : IN     boolean ;
      BEEBUS_DATA          : INOUT  unsigned (15 DOWNTO 0)
   );
   END COMPONENT;
   COMPONENT dcm_comm_emu_get
   PORT (
      CLK_32         : IN     std_logic ;
      DATA_OUT       : OUT    std_logic_vector (7 DOWNTO 0);
      DATA_10par_out : OUT    std_logic_vector (9 DOWNTO 0);
      DATA_STRB      : OUT    boolean ;
      LINKED         : OUT    boolean ;
      COMM_ERROR     : OUT    boolean ;
      SERIAL_IN      : IN     std_logic 
   );
   END COMPONENT;
   COMPONENT reg
   PORT (
      BEEBUS_ADDR : IN     unsigned (15 DOWNTO 0);
      BEEBUS_DATA : INOUT  unsigned (15 DOWNTO 0);
      BEEBUS_READ : IN     boolean ;
      BEEBUS_CLK  : IN     std_logic ;
      BEEBUS_STRB : IN     boolean ;
      REG_DATA    : INOUT  std_logic_vector (15 DOWNTO 0)
   );
   END COMPONENT;

   -- Optional embedded configurations
   -- pragma synthesis_off
   FOR ALL : comm_decode USE ENTITY nova_feb.comm_decode;
   FOR ALL : dcm_com_beebus_interface USE ENTITY nova_feb.dcm_com_beebus_interface;
   FOR ALL : dcm_comm_emu_get USE ENTITY nova_feb.dcm_comm_emu_get;
   FOR ALL : reg USE ENTITY nova_feb.reg;
   -- pragma synthesis_on


BEGIN

   -- Instance port mappings.
   U_11 : comm_decode
      PORT MAP (
         CLK_32       => clk_32,
         CLK_3_2      => CLK_3_2,
         DATA_RX      => DATA_RX,
         DATA_TX      => DATA_RX,
         DATA_TX_STRB => DATA_RX_STRB,
         DATA_RX_STRB => DATA_RX_STRB,
         LINKED       => LINKED,
         COMM_ERROR   => COMM_ERROR,
         SERIAL_TX    => DCM_DATA_internal,
         SERIAL_RX    => DCM_COMMAND
      );
   U_1 : dcm_com_beebus_interface
      PORT MAP (
         CLK_3_2              => CLK_3_2,
         BEEBUS_ADDR          => BEEBUS_ADDR,
         BEEBUS_CLK           => OPEN,
         BEEBUS_READ          => BEEBUS_READ,
         ERROR_FLAG           => OPEN,
         BEEBUS_STRB          => BEEBUS_STRB,
         DCM_DATA_BEEBUS      => DCM_DATA_BEEBUS,
         DCM_DATA_BEEBUS_STRB => DCM_DATA_BEEBUS_STRB,
         DATA_RX              => DATA_RX,
         DATA_RX_STRB         => DATA_RX_STRB,
         RESET                => RESET,
         BEEBUS_DATA          => BEEBUS_DATA
      );
   U_0 : dcm_comm_emu_get
      PORT MAP (
         CLK_32         => clk_32,
         DATA_OUT       => REGOUT_DATA,
         DATA_10par_out => OPEN,
         DATA_STRB      => DATA_STRB,
         LINKED         => OPEN,
         COMM_ERROR     => OPEN,
         SERIAL_IN      => DCM_DATA_internal
      );
   U_8 : reg
      PORT MAP (
         BEEBUS_ADDR => BEEBUS_ADDR,
         BEEBUS_DATA => BEEBUS_DATA,
         BEEBUS_READ => BEEBUS_READ,
         BEEBUS_CLK  => CLK_3_2,
         BEEBUS_STRB => BEEBUS_STRB,
         REG_DATA    => OPEN
      );

   -- Implicit buffered output assignments
   DCM_DATA <= DCM_DATA_internal;

END struct;
