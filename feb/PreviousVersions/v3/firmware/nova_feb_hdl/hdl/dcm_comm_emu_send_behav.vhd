--
-- VHDL Architecture feb_p2_lib.dcm_comm_emu_send.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 12:06:49 11/20/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity dcm_comm_emu_send is
  port(
    BEEBUS_ADDR : in    unsigned (15 downto 0);
    BEEBUS_DATA : inout unsigned (31 downto 0);
    BEEBUS_READ : in    boolean;
    BEEBUS_CLK  : in    std_logic;
    BEEBUS_STRB : in    boolean;
    RESET       : in    boolean;
    clk_32      : in    std_logic;
    clk_3_2     : in    std_logic;
    SER_DATA    : out   std_logic
    );

-- Declarations

end dcm_comm_emu_send;

--
architecture behav of dcm_comm_emu_send is

  type STATE_TYPE is (
    WAIT_FOR_NEW_COMMAND,
    SEND_COMMAND,
    SEND_ADDR_UBYTE,
    SEND_ADDR_LBYTE,
    SEND_DATA_UBYTE,
    SEND_DATA_LBYTE
    );

  -- State vector declaration
  -- attribute state_vector          : string;
--  attribute state_vector of BEHAV : architecture is "current_state";

  attribute ENUM_ENCODING               : string;
  attribute ENUM_ENCODING of STATE_TYPE : type is
    "000" &                             -- WAIT_FOR_NEW_COMMAND
    "001" &                             -- SEND_COMMAND
    "010" &                             -- SEND_ADDR_UBYTE
    "011" &                             -- SEND_ADDR_LBYTE
    "100" &                             -- SEND_DATA_UBYTE
    "101";                              -- SEND_DATA_LBYTE
  

  signal SEND_DCM_CMD   : boolean                       := false;
  signal CURRENT_STATE  : STATE_TYPE;
  signal LAST_DATA_SENT : std_logic_vector(7 downto 0)  := x"00";
  signal DCM_CMD_TYPE   : std_logic_vector(7 downto 0)  := x"00";
  signal DCM_CMD_ADDR   : std_logic_vector(15 downto 0) := x"0000";
  signal DCM_CMD_DATA   : std_logic_vector(15 downto 0) := x"0000";
  signal ENCODE_DIN     : std_logic_vector(7 downto 0)  := x"00";
  signal ENCODE_KIN     : std_logic                     := '0';
  signal ENCODE_DOUT    : std_logic_vector(9 downto 0)  := "0000000000";
  signal SHIFT_REG      : std_logic_vector(9 downto 0)  := "0000000000";
  signal BIT_COUNT      : integer range 15 downto 0     := 0;

  component encode_8b10b
    port (
      din  : in  std_logic_vector(7 downto 0);
      kin  : in  std_logic;
      clk  : in  std_logic;
      dout : out std_logic_vector(9 downto 0));
  end component;

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;

  
begin
  
  RECEIVE_COMMAND : process (BEEBUS_CLK) is
  begin
    if BEEBUS_CLK'event and BEEBUS_CLK = '1' then
      BEEBUS_DATA <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";

      if (current_state = SEND_COMMAND) then
        SEND_DCM_CMD <= false;
      else
        SEND_DCM_CMD <= SEND_DCM_CMD;
      end if;

      if (BEEBUS_ADDR = x"0030")then
        if BEEBUS_STRB then
          DCM_CMD_TYPE <= std_logic_vector(BEEBUS_DATA(7 downto 0));
        end if;
      end if;

      if (BEEBUS_ADDR = x"0031")then
        if BEEBUS_STRB then
          DCM_CMD_ADDR <= std_logic_vector(BEEBUS_DATA(15 downto 0));
        end if;
      end if;

      if (BEEBUS_ADDR = x"0032")then
        if BEEBUS_READ then
          BEEBUS_DATA <= x"0000" & unsigned(DCM_CMD_DATA);
        elsif BEEBUS_STRB then
          DCM_CMD_DATA <= std_logic_vector(BEEBUS_DATA(15 downto 0));
        end if;
      end if;

      if (BEEBUS_ADDR = x"0033")then
        if BEEBUS_STRB then
          SEND_DCM_CMD <= true;
        end if;
      end if;
      
    end if;
  end process RECEIVE_COMMAND;

  DCM_COMM_ENCODE : encode_8b10b

    port map (
      din  => ENCODE_DIN,
      kin  => ENCODE_KIN,
      clk  => CLK_3_2,
      dout => ENCODE_DOUT
      );

  par_to_ser : process (CLK_32) is
  begin
    if CLK_32'event and CLK_32 = '0' then
      SHIFT_REG <= '0' & SHIFT_REG(9 downto 1);
      BIT_COUNT <= BIT_COUNT +1;

      if BIT_COUNT >= 9 then
        SHIFT_REG <= ENCODE_DOUT;
        BIT_COUNT <= 0;
      end if;
    end if;
  end process par_to_ser;

  SER_DATA <= SHIFT_REG(0);

  CLOCKED : process (CLK_3_2) is
  begin
    if CLK_3_2'event and CLK_3_2 = '1' then
      ENCODE_KIN <= '0';
      if RESET then
        current_state <= WAIT_FOR_NEW_COMMAND;
      else
        case current_state is
          
          when WAIT_FOR_NEW_COMMAND =>
            ENCODE_DIN <= "00111100";
            ENCODE_KIN <= '1';
            if SEND_DCM_CMD then
              CURRENT_STATE <= SEND_COMMAND;
            else
              CURRENT_STATE <= WAIT_FOR_NEW_COMMAND;
            end if;

          when SEND_COMMAND =>
            ENCODE_DIN    <= DCM_CMD_TYPE;
            CURRENT_STATE <= SEND_ADDR_UBYTE;

          when SEND_ADDR_UBYTE =>
            ENCODE_DIN    <= DCM_CMD_ADDR(15 downto 8);
            CURRENT_STATE <= SEND_ADDR_LBYTE;

          when SEND_ADDR_LBYTE =>
            ENCODE_DIN    <= DCM_CMD_ADDR(7 downto 0);
            CURRENT_STATE <= SEND_DATA_UBYTE;

          when SEND_DATA_UBYTE =>
            ENCODE_DIN    <= DCM_CMD_DATA(15 downto 8);
            CURRENT_STATE <= SEND_DATA_LBYTE;

          when SEND_DATA_LBYTE =>
            ENCODE_DIN    <= DCM_CMD_DATA(7 downto 0);
            CURRENT_STATE <= WAIT_FOR_NEW_COMMAND;

          when others =>
            ENCODE_DIN    <= "00111100";
            ENCODE_KIN    <= '1';
            CURRENT_STATE <= WAIT_FOR_NEW_COMMAND;
        end case;

      end if;
    end if;
  end process CLOCKED;

end architecture behav;



