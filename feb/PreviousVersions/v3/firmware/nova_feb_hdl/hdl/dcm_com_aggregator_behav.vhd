--
-- VHDL Architecture nova_feb.dcm_com_aggregator.behav
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 11:04:09 08/04/09
--
-- using Mentor Graphics HDL Designer(TM) 2008.1a (Build 9)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;
LIBRARY unisim;
USE unisim.all;
library harvard_std;
USE harvard_std.lppc_custom_fn_pkg.all;

ENTITY dcm_com_aggregator IS
   PORT( 
      CLK_3_2              : IN     std_logic;
      DCM_DATA_BEEBUS      : IN     std_logic_vector (15 DOWNTO 0);
      DCM_DATA_BEEBUS_STRB : IN     boolean;
      DATA_TX              : OUT    std_logic_vector (7 DOWNTO 0);
      DATA_TX_STRB         : OUT    boolean
   );

-- Declarations

END dcm_com_aggregator ;

--
architecture behav of dcm_com_aggregator is
  type STATE_TYPE is (
    WAIT_FOR_COMMA,
    WAIT_FOR_COMMAND,
    GET_ADDR_UBYTE,
    GET_ADDR_LBYTE,
    GET_DATA_UBYTE,
    GET_DATA_LBYTE,
    READ_REG_DATA,
    READ_REG_MT_DATA
    );

  type COMMAND_TYPE is (
    IDLE,
    NULL_CMD,
    WRITE_REG,
    READ_REG
    );

  -- State vector declaration
  -- attribute state_vector          : string;
--  attribute state_vector of BEHAV : architecture is "current_state";

  attribute ENUM_ENCODING               : string;
  attribute ENUM_ENCODING of STATE_TYPE : type is
    "000" &                             -- WAIT_FOR_COMMA
    "001" &                             -- WAIT_FOR_COMMAND
    "010" &                             -- GET_ADDR_UBYTE
    "011" &                             -- GET_ADDR_LBYTE
    "100" &                             -- GET_DATA_UBYTE
    "101" &                             -- GET_DATA_LBYTE
    "100" &                             -- READ_REG_DATA
    "101";                              -- READ_REG_MT_DATA
  
  attribute ENUM_ENCODING of COMMAND_TYPE : type is
    "00" &                              -- IDLE
    "01" &                              -- NULL_CMD
    "10" &                              -- WRITE_REG
    "11";                               -- READ_REG
  signal CURRENT_STATE      : STATE_TYPE;
  signal NEXT_STATE         : STATE_TYPE;
  signal CURRENT_COMMAND    : COMMAND_TYPE;
  signal RX_UBYTE           : std_logic_vector (7 downto 0);
  signal DCM_REQUESTED_DATA : unsigned (15 downto 0);

begin
  CLOCKED : process (CLK_3_2) is
  begin  -- process CLOCKED_PROCESS
    if CLK_3_2'event and CLK_3_2 = '1' then  -- rising clock edge
      BEEBUS_READ          <= false;
      BEEBUS_STRB          <= false;
      BEEBUS_DATA          <= "ZZZZZZZZZZZZZZZZ";
      DCM_DATA_BEEBUS_STRB <= false;
      if RESET then
        current_state <= WAIT_FOR_COMMA;
      else
        current_state <= next_state;
        case current_state is
          
          when WAIT_FOR_COMMAND =>
            if DATA_RX_STRB then
              case DATA_RX(7 downto 4) is
                when "0000" =>
                  CURRENT_COMMAND <= NULL_CMD;
                when "0010" =>
                  CURRENT_COMMAND <= WRITE_REG;
                when "0011" =>
                  CURRENT_COMMAND <= READ_REG;
                when others =>
                  ERROR_FLAG      <= true;
                  CURRENT_COMMAND <= IDLE;
              end case;
            end if;

          when GET_ADDR_UBYTE =>
            BEEBUS_ADDR(15 downto 8) <= unsigned(DATA_RX);
          when GET_ADDR_LBYTE =>
            BEEBUS_ADDR(7 downto 0) <= unsigned(DATA_RX);
            BEEBUS_READ             <= (CURRENT_COMMAND = READ_REG);
          when GET_DATA_UBYTE =>
            RX_UBYTE <= DATA_RX;
          when GET_DATA_LBYTE =>
            BEEBUS_DATA <= unsigned(RX_UBYTE) & unsigned(DATA_RX);
            BEEBUS_STRB <= true;
          when READ_REG_DATA =>
            DCM_DATA_BEEBUS      <= BEEBUS_DATA;
            DCM_DATA_BEEBUS_STRB <= true;
          when others =>
            null;
        end case;

      end if;
    end if;
  end process CLOCKED;

  STATE_DIRECTION : process (CURRENT_STATE, CURRENT_COMMAND, DATA_RX_STRB) is
  begin
    case current_state is
      when WAIT_FOR_COMMA =>
        if ((not DATA_RX_STRB) and (DATA_RX = x"3C")) then
          next_state <= WAIT_FOR_COMMAND;
        else
          next_state <= WAIT_FOR_COMMA;
        end if;

      when WAIT_FOR_COMMAND =>
        if DATA_RX_STRB then
          next_state <= GET_ADDR_UBYTE;
        else
          next_state <= WAIT_FOR_COMMAND;
        end if;

      when GET_ADDR_UBYTE =>
        next_state <= GET_ADDR_LBYTE;
        
      when GET_ADDR_LBYTE =>
        if (CURRENT_COMMAND = READ_REG) then
          next_state <= READ_REG_DATA;
        elsif (CURRENT_COMMAND = WRITE_REG) then
          next_state <= GET_DATA_UBYTE;
        else
          next_state <= WAIT_FOR_COMMA;
        end if;

      when GET_DATA_UBYTE =>
        next_state <= GET_DATA_LBYTE;

      when GET_DATA_LBYTE =>
        next_state <= WAIT_FOR_COMMA;

      when READ_REG_DATA =>
        next_state <= READ_REG_MT_DATA;

      when READ_REG_MT_DATA =>
        next_state <= WAIT_FOR_COMMA;

      when others =>
        next_state <= WAIT_FOR_COMMA;

    end case;
    
  end process STATE_DIRECTION;
end architecture behav;

