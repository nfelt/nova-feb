--
-- VHDL Architecture feb_p2_lib.sparsification.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 17:10:44 04/ 2/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity dsp_dcs8 is
  port(
    DATA_in16        : in    unsigned (11 downto 0) := x"000";
    BEEBUS_READ      : in    boolean                := false;
    BEEBUS_STRB      : in    boolean                := false;
    BEEBUS_CLK       : in    std_logic;
    BEEBUS_DATA      : inout unsigned (31 downto 0) := x"00000000";
    BEEBUS_ADDR      : in    unsigned (15 downto 0) := "0000000000000000";
    CHAN_NUMBER_IN   : in    unsigned (4 downto 0)  := "00000";
    CHAN_NUMBER      : out   unsigned (4 downto 0)  := "00000";
    TIMESTAMP        : out   unsigned (31 downto 0) := x"00000000";
    MAGNITUDE        : out   signed (12 downto 0)   := "0000000000000";
    DATA_FOR_YOU     : out   boolean                := false;
    GOT_ANYTHING_NEW : in    boolean                := false;
    CONTINUOUS_DATA  : out   boolean                := false;
    CLK              : in    std_logic              := '1';
    OSCOPE_TRIGGER   : in    boolean                := false;
    ENABLE_DAQ       : in    boolean                := false
    );

-- Declarations

end dsp_dcs8;

--
architecture behav of dsp_dcs8 is

  type CHAN_STATE_TYPE is (
    WAIT_FOR_TRIG,
    TRIGGERED,
    READ_RDY
    );
  attribute enum_encoding                    : string;
  attribute enum_encoding of CHAN_STATE_TYPE : type is "00 01 11";

  type CHAN_TYPE is record
    DEL_0      : signed(12 downto 0);
    DEL_1      : signed(12 downto 0);
    DEL_2      : signed(12 downto 0);
    DEL_3      : signed(12 downto 0);
    DCS        : signed(12 downto 0);
    MAGNITUDE  : signed(12 downto 0);
    TIMESTAMP  : unsigned(31 downto 0);
    CHAN_STATE : CHAN_STATE_TYPE;
  end record CHAN_TYPE;

  type CHAN_DATA_TYPE is array (31 downto 0)
    of CHAN_TYPE;
  
  signal CHAN_DATA : CHAN_DATA_TYPE;

  type TABLE_TYPE is array (31 downto 0)
    of unsigned(11 downto 0);
  
  signal PEDESTAL_TABLE  : TABLE_TYPE;
  signal THRESHOLD_TABLE : TABLE_TYPE;

  type TEST_MODE_TYPE is (
    NORMAL_DSP,
    OSCILLOSCOPE
    );

  signal TEST_MODE : TEST_MODE_TYPE := NORMAL_DSP;

  signal TEST_MODE_CMD    : unsigned(3 downto 0)      := x"0";
  signal CHAN_NUMBER_DSP  : unsigned (4 downto 0)     := "00000";
  signal MAGNITUDE_DSP    : signed (12 downto 0)      := "0000000000000";
  signal CHAN_DATA_DCS    : signed (12 downto 0)      := "0000000000000";
  signal CHAN_COUNT       : integer range 31 downto 0 := 0;
  signal TIMESTAMP_CLOCK  : unsigned(31 downto 0)     := x"00000000";
  signal ENABLE_DSP       : boolean                   := false;
  signal OSCOPE_TRIGGERED : boolean                   := false;

begin
  
  process (BEEBUS_CLK)
  begin
    if BEEBUS_CLK'event and BEEBUS_CLK = '1' then
      BEEBUS_DATA <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
      if (BEEBUS_ADDR = x"0005") then
        if BEEBUS_READ then
          BEEBUS_DATA <= x"0000000" & TEST_MODE_CMD;
        elsif BEEBUS_STRB then
          TEST_MODE_CMD <= BEEBUS_DATA(3 downto 0);
        end if;
      end if;
      if (BEEBUS_ADDR >= x"1000") and(BEEBUS_ADDR < x"1020")then
        if BEEBUS_READ then
          BEEBUS_DATA <= x"00000" & PEDESTAL_TABLE (to_integer(BEEBUS_ADDR(5 downto 0)));
        elsif BEEBUS_STRB then
          PEDESTAL_TABLE (to_integer(BEEBUS_ADDR(5 downto 0))) <= (BEEBUS_DATA(11 downto 0));
        end if;
      end if;
      if (BEEBUS_ADDR >= x"1100") and(BEEBUS_ADDR < x"1120")then
        if BEEBUS_READ then
          BEEBUS_DATA <= x"00000" & THRESHOLD_TABLE (to_integer(BEEBUS_ADDR(5 downto 0)));
        elsif BEEBUS_STRB then
          THRESHOLD_TABLE (to_integer(BEEBUS_ADDR(5 downto 0))) <= (BEEBUS_DATA(11 downto 0));
        end if;
      end if;
    end if;
  end process;
-- Do DCS with FIR 1 0 0 -1
-- with DCS value trigger when over threshold
-- when drop to < 1/2 threshold then mark as READ_RDY
  
  process (clk) is
  begin
    if clk'event and clk = '1' then
      CHAN_COUNT                  <= to_integer(CHAN_NUMBER_IN);
      TIMESTAMP_CLOCK             <= TIMESTAMP_CLOCK + 1;
      CHAN_DATA(CHAN_COUNT).DEL_0 <= signed('0' & DATA_IN16) - signed('0' & PEDESTAL_TABLE(CHAN_COUNT));
      CHAN_DATA(CHAN_COUNT).DEL_1 <= CHAN_DATA(CHAN_COUNT).DEL_0;
      CHAN_DATA(CHAN_COUNT).DEL_2 <= CHAN_DATA(CHAN_COUNT).DEL_1;
      CHAN_DATA(CHAN_COUNT).DEL_3 <= CHAN_DATA(CHAN_COUNT).DEL_2;
      CHAN_DATA(CHAN_COUNT).DCS   <= CHAN_DATA(CHAN_COUNT).DEL_0 - CHAN_DATA(CHAN_COUNT).DEL_3;
      DATA_FOR_YOU                <= false;
      if not (CHAN_DATA(CHAN_COUNT).CHAN_STATE = READ_RDY) then
        if CHAN_DATA(CHAN_COUNT).DCS > CHAN_DATA(CHAN_COUNT).MAGNITUDE and ENABLE_DSP then
          CHAN_DATA(CHAN_COUNT).CHAN_STATE <= TRIGGERED;
          CHAN_DATA(CHAN_COUNT).MAGNITUDE  <= CHAN_DATA(CHAN_COUNT).DCS;
          CHAN_DATA(CHAN_COUNT).TIMESTAMP  <= TIMESTAMP_CLOCK;
        elsif CHAN_DATA(CHAN_COUNT).DCS < signed('0' & THRESHOLD_TABLE(CHAN_COUNT)(11 downto 1)) then
          if CHAN_DATA(CHAN_COUNT).CHAN_STATE = TRIGGERED then
            CHAN_DATA(CHAN_COUNT).CHAN_STATE <= READ_RDY;
          end if;
        end if;

      elsif GOT_ANYTHING_NEW then
        CHAN_NUMBER_DSP                  <= to_UNSIGNED(CHAN_COUNT, 5);
        TIMESTAMP                        <= CHAN_DATA(CHAN_COUNT).TIMESTAMP;
        MAGNITUDE_DSP                    <= CHAN_DATA(CHAN_COUNT).MAGNITUDE;
        DATA_FOR_YOU                     <= CHAN_DATA(CHAN_COUNT).CHAN_STATE = READ_RDY;
        CHAN_DATA(CHAN_COUNT).MAGNITUDE  <= signed('0' & THRESHOLD_TABLE(CHAN_COUNT));
        CHAN_DATA(CHAN_COUNT).CHAN_STATE <= WAIT_FOR_TRIG;
        
      end if;
    end if;
  end process;

  decode_mode : process (clk) is
  begin
    if clk'event and clk = '1' then
      case TEST_MODE_CMD is
        when x"0" =>
          TEST_MODE <= NORMAL_DSP;
        when x"1" =>
          TEST_MODE <= OSCILLOSCOPE;
        when others => null;
      end case;
    end if;
  end process decode_mode;

  SCOPE_TRIGGER : process (clk) is
  begin
    if clk'event and clk = '1' then
      if not ENABLE_DAQ then
        OSCOPE_TRIGGERED <= false;
      elsif OSCOPE_TRIGGER then
        OSCOPE_TRIGGERED <= true;
      end if;
    end if;
  end process SCOPE_TRIGGER;

  select_output : process (CHAN_NUMBER_IN, CHAN_NUMBER_DSP, MAGNITUDE_DSP, DATA_in16, CHAN_COUNT, TEST_MODE, ENABLE_DAQ, OSCOPE_TRIGGERED) is
  begin
    case TEST_MODE is
      when NORMAL_DSP =>
        CHAN_NUMBER     <= CHAN_NUMBER_DSP;
        MAGNITUDE       <= MAGNITUDE_DSP;
        CONTINUOUS_DATA <= false;
        ENABLE_DSP      <= ENABLE_DAQ;
      when OSCILLOSCOPE =>
        CHAN_NUMBER     <= UNSIGNED(CHAN_NUMBER_IN);
        MAGNITUDE       <= signed('0' & DATA_in16);
        CONTINUOUS_DATA <= OSCOPE_TRIGGERED;
        ENABLE_DSP      <= false;
      when others =>
        CHAN_NUMBER     <= to_UNSIGNED(CHAN_COUNT, 5);
        MAGNITUDE       <= '0' & x"BAD";
        CONTINUOUS_DATA <= false;
        ENABLE_DSP      <= false;
    end case;
  end process select_output;

end architecture BEHAV;

