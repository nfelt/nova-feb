--
-- VHDL Architecture nova_feb.spi_interface.behav
--
-- Created:
--          by - nate.nate (heplpc2)
--          at - 11:13:08 04/14/09
--
-- using Mentor Graphics HDL Designer(TM) 2008.1a (Build 9)

-------------------------------------------------------------------------------
-- !!! the spi din dout needs to have a workaround for FEB3 !!!
-- Needs to have clock domain sync if beebus is changed to async clk

-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity spi_interface is
  port(
    BEEBUS_ADDR     : in    unsigned (15 downto 0);
    BEEBUS_CLK      : in    std_logic;
    BEEBUS_DATA     : inout unsigned (31 downto 0);
    BEEBUS_READ     : in    boolean;
    BEEBUS_STRB     : in    boolean;
    UPDATE_TEMP_REG : in    boolean;
    CLK_16          : in    std_logic;
    SPI_DIO         : inout std_logic;
    SPI_DOUT        : in    std_logic;
    SPI_SCLK        : out   std_logic;
    TEC_DAC_LDAC    : out   boolean;
    TEC_DAC_SYNC    : out   boolean;
    TEC_ADC_CS      : out   boolean;
    TEMP_SENSOR_CS  : out   boolean
    );

-- Declarations

end spi_interface;
architecture behav of spi_interface is
  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;

  component BUFGCE_1
    port(
      O  : out std_ulogic;
      CE : in  std_ulogic;
      I  : in  std_ulogic
      );
  end component;
  
  type SPI_STATE_TYPE is (
    IDLE,
    WRITE_DAC,
    READ_ADC,
    READ_TEMP
    );
  signal SPI_COMMAND     : unsigned(2 downto 0)      := "000";
  signal DC_CHAN_SELECT  : std_logic                 := '0';
  signal SPI_DATA        : unsigned(11 downto 0)     := x"000";
  signal SPI_STATE       : SPI_STATE_TYPE            := IDLE;
  signal NEXT_SPI_STATE  : SPI_STATE_TYPE            := IDLE;
  signal BIT_COUNT       : integer range 15 downto 0 := 0;
  signal NEXT_BIT_COUNT  : integer range 15 downto 0 := 0;
  signal SPI_SREG        : unsigned(15 downto 0)     := x"0000";
  signal START_WRITE_DAC : boolean                   := false;
  signal START_READ_ADC  : boolean                   := false;
  signal START_READ_TEMP : boolean                   := false;
  signal SPI_CLK_CE      : std_logic                 := '0';
  
begin
  --Needs to have clock domain sync if beebus is changed to async clk
  RECEIVE_COMMAND : process (BEEBUS_CLK) is
  begin
    if BEEBUS_CLK'event and BEEBUS_CLK = '1' then
      BEEBUS_DATA <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
      --PULSE COMMAND FOR ONLY ONE CLOCK
      SPI_COMMAND <= "000";

      if (BEEBUS_ADDR = x"0009" and BEEBUS_READ)then
        if SPI_STATE /= IDLE then
          BEEBUS_DATA <= x"80000000";
        else
          BEEBUS_DATA <= x"0000" & SPI_SREG;
        end if;
      end if;

      if (BEEBUS_ADDR = x"0009" and BEEBUS_STRB)then
        SPI_COMMAND    <= BEEBUS_DATA(15 downto 13);
        DC_CHAN_SELECT <= BEEBUS_DATA(12);
        SPI_DATA       <= BEEBUS_DATA(11 downto 0);
      end if;
      
    end if;
  end process RECEIVE_COMMAND;

  -- SPI bus clocked process
  UPDATE_SPI_STATE : process (CLK_16) is
  begin
    if clk_16'event and clk_16 = '1' then
      SPI_STATE <= NEXT_SPI_STATE;
      BIT_COUNT <= NEXT_BIT_COUNT;
      TEC_DAC_LDAC <= false;
      
      case SPI_STATE is
        
        when IDLE =>
          if START_WRITE_DAC then
            --if next operation is a DAC write then append first 4 config bits
            SPI_SREG <= DC_CHAN_SELECT & "100" & SPI_DATA;
          end if;

        when WRITE_DAC =>
          --pulse LDAC after register is loaded
          TEC_DAC_LDAC <= BIT_COUNT = 0;

        when READ_ADC =>
          SPI_SREG(BIT_COUNT) <= SPI_DOUT;

        when READ_TEMP =>
          SPI_SREG(BIT_COUNT) <= SPI_DIO;

        when others => null;
      end case;
    end if;
  end process UPDATE_SPI_STATE;

  --SPI bus combinitorial process
  SPI_NAVIGATAION : process (SPI_STATE, SPI_COMMAND, BIT_COUNT, SPI_SREG, DC_CHAN_SELECT) is
  begin
    TEC_DAC_SYNC    <= false;
    TEC_ADC_CS      <= false;
    TEMP_SENSOR_CS  <= false;
    --enable CLK except when IDLE
    SPI_CLK_CE      <= '1';
    SPI_DIO         <= 'Z';
    START_WRITE_DAC <= SPI_COMMAND = "001";
    START_READ_ADC  <= SPI_COMMAND = "010";
    START_READ_TEMP <= SPI_COMMAND = "011";


    if BIT_COUNT = 0 then
      NEXT_SPI_STATE <= IDLE;
      NEXT_BIT_COUNT <= 0;
    else
      NEXT_SPI_STATE <= SPI_STATE;
      NEXT_BIT_COUNT <= BIT_COUNT-1;
    end if;

    case SPI_STATE is
      
      when IDLE =>
        SPI_CLK_CE <= '0';

        if START_WRITE_DAC then
          NEXT_SPI_STATE <= WRITE_DAC;
          NEXT_BIT_COUNT <= 15;
          
        elsif START_READ_ADC then
          NEXT_SPI_STATE <= READ_ADC;
          NEXT_BIT_COUNT <= 15;
          
        elsif START_READ_TEMP then
          NEXT_SPI_STATE <= READ_TEMP;
          NEXT_BIT_COUNT <= 15;
          
        else
          NEXT_SPI_STATE <= IDLE;
        end if;

      when WRITE_DAC =>
        TEC_DAC_SYNC <= true;
        SPI_DIO      <= SPI_SREG(BIT_COUNT);

      when READ_ADC =>
        TEC_ADC_CS <= true;
        SPI_DIO    <= DC_CHAN_SELECT;

      when READ_TEMP =>
        TEMP_SENSOR_CS <= true;
        
      when others => null;
    end case;
  end process SPI_NAVIGATAION;

  SPI_CLK_BUF : BUFGCE_1
    port map (
      O  => SPI_SCLK,
      CE => SPI_CLK_CE,
      I  => CLK_16
      );

end architecture behav;

