--
-- VHDL Architecture feb_p2_lib.ASIC_CTRL.BEHAV
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 17:00:32 02/26/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity ASIC_CTRL is
  port(
    BEEBUS_ADDR    : in    unsigned (15 downto 0);
    BEEBUS_CLK     : in    std_logic;
    BEEBUS_DATA    : inout unsigned (31 downto 0);
    BEEBUS_READ    : in    boolean;
    BEEBUS_STRB    : in    boolean;
    CLK            : in    std_logic;
    ASIC_CHIPRESET : out   boolean;
    ASIC_SHIFTIN   : out   std_logic;
    ASIC_SHIFTOUT  : in    std_logic;
    ASIC_SRCK      : out   std_logic;
    ASIC_IBIAS     : out   boolean;
    ASIC_INTEGRST  : out   boolean
    );

-- Declarations

end ASIC_CTRL;

--
architecture BEHAV of ASIC_CTRL is

  
  type ASIC_SREG_TYPE is array (6 downto 0)
    of std_logic_vector (31 downto 0);
  signal COMMAND_REGISTER : ASIC_SREG_TYPE;

  signal ASIC_SHIFT_REGISTER   : std_logic_vector (219 downto 0) := x"7FFFFFF00000000FFFFFFFF00000000FFFFFFFF00000000FFFFFFFF";
  signal ASIC_INTEGRST_COUNTER : unsigned(23 downto 0)           := x"000000";
  signal ASIC_IBIAS_COUNTER    : unsigned(23 downto 0)           := x"000000";
  signal ASIC_SHIFT_COUNT      : unsigned(7 downto 0)            := "00000000";
  signal ASIC_COMMAND_SENT     : boolean                         := false;
  signal ASIC_COMMAND_SENT_c1  : boolean                         := false;
  signal ASIC_COMMAND_SENT_c2  : boolean                         := false;
  signal DAQ_INTERUPT          : boolean                         := false;
  signal GET_NEW_COMMAND       : boolean                         := false;
  signal GOT_THAT_COMMAND      : boolean                         := false;
  signal NEW_COMMAND           : boolean                         := false;
  signal ASIC_SRCK_int         : std_logic                       := '0';




  
begin

  RECEIVE_COMMAND : process (BEEBUS_CLK) is
  begin
    if BEEBUS_CLK'event and BEEBUS_CLK = '1' then
      BEEBUS_DATA  <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
      DAQ_INTERUPT <= DAQ_INTERUPT and not GOT_THAT_COMMAND;

      if (BEEBUS_ADDR = x"0007" and BEEBUS_STRB)then
        ASIC_INTEGRST_COUNTER <= BEEBUS_DATA(23 downto 0);
      elsif ASIC_INTEGRST_COUNTER /= "000000" then
        ASIC_INTEGRST_COUNTER <= ASIC_INTEGRST_COUNTER-1;
        ASIC_INTEGRST         <= true;
      else
        ASIC_INTEGRST <= false;
      end if;

      if (BEEBUS_ADDR = x"0008" and BEEBUS_STRB)then
        ASIC_IBIAS_COUNTER <= BEEBUS_DATA(23 downto 0);
      elsif ASIC_IBIAS_COUNTER /= "000000" then
        ASIC_IBIAS_COUNTER <= ASIC_IBIAS_COUNTER-1;
        ASIC_IBIAS         <= true;
      else
        ASIC_IBIAS <= false;
      end if;

      if (BEEBUS_ADDR(15 downto 3) = "0000000000010") and
        (BEEBUS_ADDR(2 downto 0) /= "111") then
        if BEEBUS_READ then
          BEEBUS_DATA <= x"beefcafe";
        elsif BEEBUS_STRB then
          COMMAND_REGISTER(to_integer(BEEBUS_ADDR(3 downto 0))) <= std_logic_vector(BEEBUS_DATA);
          DAQ_INTERUPT                                          <= (BEEBUS_ADDR(2 downto 0) = "110");
        end if;
      end if;
    end if;
  end process RECEIVE_COMMAND;


  UPDATE_CONTOLLER_STATE : process (CLK) is
  begin
    if clk'event and clk = '1' then
      GET_NEW_COMMAND      <= DAQ_INTERUPT;
      GOT_THAT_COMMAND     <= GET_NEW_COMMAND;
      ASIC_COMMAND_SENT_c1 <= ASIC_COMMAND_SENT;
      ASIC_COMMAND_SENT_c2 <= ASIC_COMMAND_SENT_c1;

      if NEW_COMMAND then
        ASIC_SHIFT_COUNT                    <= "00000000";
        ASIC_SHIFT_REGISTER(31 downto 0)    <= COMMAND_REGISTER(0);
        ASIC_SHIFT_REGISTER(63 downto 32)   <= COMMAND_REGISTER(1);
        ASIC_SHIFT_REGISTER(95 downto 64)   <= COMMAND_REGISTER(2);
        ASIC_SHIFT_REGISTER(127 downto 96)  <= COMMAND_REGISTER(3);
        ASIC_SHIFT_REGISTER(159 downto 128) <= COMMAND_REGISTER(4);
        ASIC_SHIFT_REGISTER(191 downto 160) <= COMMAND_REGISTER(5);
        ASIC_SHIFT_REGISTER(219 downto 192) <= COMMAND_REGISTER(6)(27 downto 0);
        ASIC_SRCK_int                       <= '0';
      elsif not ASIC_COMMAND_SENT then
        ASIC_SRCK_int <= not ASIC_SRCK_int;
        if ASIC_SRCK_int = '1' then
          ASIC_SHIFT_REGISTER <= ASIC_SHIFTOUT & ASIC_SHIFT_REGISTER(219 downto 1);
          ASIC_SHIFT_COUNT    <= ASIC_SHIFT_COUNT+1;
        end if;
      end if;
    end if;
  end process UPDATE_CONTOLLER_STATE;

  DELAY_CHIPRESET : process (CLK) is
  begin
    if clk'event and clk = '1' then
      ASIC_CHIPRESET <= ASIC_COMMAND_SENT_c1 and not ASIC_COMMAND_SENT_c2;
    end if;
  end process DELAY_CHIPRESET;
  
  update_asic : process (ASIC_SRCK_int, ASIC_SHIFT_REGISTER(0), ASIC_COMMAND_SENT_c1,
                         ASIC_COMMAND_SENT_c2, ASIC_SHIFT_COUNT, GET_NEW_COMMAND,
                         GOT_THAT_COMMAND) is
  begin
    NEW_COMMAND       <= GET_NEW_COMMAND and not GOT_THAT_COMMAND;
    ASIC_COMMAND_SENT <= ASIC_SHIFT_COUNT = "11011100";
    ASIC_SHIFTIN      <= ASIC_SHIFT_REGISTER(0);
    ASIC_SRCK         <= ASIC_SRCK_int;
  end process update_asic;

end architecture BEHAV;

