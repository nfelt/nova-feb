###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name NOPAD -value "TRUE" -port ASIC_SHIFTOUT -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_ADDR(15) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_ADDR(14) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_ADDR(13) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_ADDR(12) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_ADDR(11) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_ADDR(10) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_ADDR(9) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_ADDR(8) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_ADDR(7) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_ADDR(6) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_ADDR(5) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_ADDR(4) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_ADDR(3) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_ADDR(2) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_ADDR(1) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_ADDR(0) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_READ -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_STRB -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port CLK_IN -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_CLK -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_CHIPRESET -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_CLK -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_SHIFTIN -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_SRCK -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_TESTINJECT -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_CLK -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port DAQ_CLK -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ENABLE_DAQ -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port EXTERNAL_TRIGGER -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port INTERNAL_TRIGGER -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port POWERUP_RESET -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port RESET_DAQ -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port USB_IFCLK -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(31) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(30) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(29) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(28) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(27) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(26) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(25) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(24) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(23) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(22) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(21) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(20) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(19) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(18) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(17) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(16) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(15) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(14) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(13) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(12) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(11) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(10) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(9) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(8) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(7) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(6) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(5) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(4) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(3) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(2) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(1) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port BEEBUS_DATA(0) -design gatelevel 
set_attribute -name CLKDV_DIVIDE -value "2.0" -instance -type default U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKFX_DIVIDE -value "1" -instance -type integer U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKFX_MULTIPLY -value "2" -instance -type integer U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKIN_DIVIDE_BY_2 -value "0" -instance -type default U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKIN_PERIOD -value "62.5" -instance -type default U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLKOUT_PHASE_SHIFT -value "NONE" -instance -type string U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name CLK_FEEDBACK -value "NONE" -instance -type string U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name DESKEW_ADJUST -value "SYSTEM_SYNCHRONOUS" -instance -type string U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name DFS_FREQUENCY_MODE -value "LOW" -instance -type string U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name DLL_FREQUENCY_MODE -value "LOW" -instance -type string U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name DUTY_CYCLE_CORRECTION -value "1" -instance -type default U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name FACTORY_JF -value "c080" -instance -type default U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name PHASE_SHIFT -value "0" -instance -type integer U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name STARTUP_WAIT -value "1" -instance -type default U_3_CLK_IN_DCM -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_CONTOLLER_STATE(1)_repl2 -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string reg_DCM_LOCK(3) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(31) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(30) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(29) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(28) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(27) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(26) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(25) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(24) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(23) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(22) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(21) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(20) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(19) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(18) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(17) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(16) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(15) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(14) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(13) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(12) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(11) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(10) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(9) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(8) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(7) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(6) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(5) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(4) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(3) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(2) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(1) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_COMMAND_REGISTER(0) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_0_reg_RESET_DAQ_int_repl0 -design gatelevel 
set_attribute -name PART -value "3S1600EFG320-4" -type string /feb_p2_lib/controller/struct -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_ASIC_SRCK_int_repl0 -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(22) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(21) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(20) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(19) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(18) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(17) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(16) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(15) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(14) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(13) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(12) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(11) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(10) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(9) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(8) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(7) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(6) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(5) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(4) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(3) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(2) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(1) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_COMMAND_REGISTER(0) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_1/reg_ASIC_SHIFT_REGISTER(0)_repl0 -design gatelevel 

set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_EXTERNAL_TRIGGER -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(31) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(30) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(29) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(28) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(27) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(26) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(25) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(24) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(23) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(22) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(21) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(20) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(19) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(18) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(17) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(16) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(15) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(14) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(13) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(12) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(11) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(10) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(9) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(8) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(7) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(6) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(5) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(4) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(3) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(2) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(1) -design gatelevel 
set_attribute -name IOB -value "TRUE" -instance -type string U_2/reg_COMMAND_REGISTER(0) -design gatelevel 

##################
# Clocks
##################
create_clock { CLK_IN } -domain ClockDomain0 -name CLK_IN -period 31.200000 -waveform { 0.000000 15.600000 } -design gatelevel 

