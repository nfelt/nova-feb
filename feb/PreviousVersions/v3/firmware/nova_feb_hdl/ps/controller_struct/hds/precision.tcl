## SETUP
puts "Info: HDL Designer Synthesis run started"

## Set current project and folder
if {[string length [info commands new_project]]} {
   new_project -name controller_struct -folder "C:/data/projects/nova/firmware/feb_p2/feb_p2_lib/ps/controller_struct"
} else {
   set_working_dir "C:/data/projects/nova/firmware/feb_p2/feb_p2_lib/ps/controller_struct"
}

## Implementation settings
MGS_Gui::notify_gui lock
setup_design -manufacturer "Xilinx" -family "SPARTAN3E" -part "3S1600EFG320" -speed "4" 

## Design Settings
setup_design -addio=true
setup_design -use_safe_fsm=false
setup_design -retiming=false
if {[catch {setup_design -2004c_compile_mode=false}]} {
catch {setup_design -frontend_2004=false}
}

setup_design -vhdl=false
setup_design -verilog=false
setup_design -edif=true
setup_design -frequency="32"

## Read in source files
catch {source "C:/data/projects/nova/firmware/feb_p2/feb_p2_lib/ps/controller_struct/hds/add_files.tcl"}
MGS_Gui::notify_gui unlock

## Compile & Synthesize
MGS_Gui::notify_gui lock
if {[catch compile]} {
   MGS_Gui::notify_gui unlock
} elseif {[catch synthesize]} {
   MGS_Gui::notify_gui unlock
} else {
   MGS_Gui::notify_gui unlock
}

puts "Info: HDL Designer Synthesis run finished"


