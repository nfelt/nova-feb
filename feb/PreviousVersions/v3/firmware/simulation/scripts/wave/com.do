onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group BEEBUS -format Literal -radix hexadecimal /top/beebus_addr
add wave -noupdate -expand -group BEEBUS -format Logic /top/clk_16
add wave -noupdate -expand -group BEEBUS -format Literal -radix hexadecimal /top/beebus_data
add wave -noupdate -expand -group BEEBUS -format Logic /top/beebus_read
add wave -noupdate -expand -group BEEBUS -format Logic /top/beebus_strb
add wave -noupdate -format Logic /top/powerup_reset
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_11/data_rx
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_1/data_rx
add wave -noupdate -format Logic /top/u_4/u_1/data_rx_strb
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_1/beebus_addr
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_1/beebus_data
add wave -noupdate -format Logic /top/u_4/u_1/beebus_strb
add wave -noupdate -format Logic /top/u_4/u_1/beebus_read
add wave -noupdate -format Literal /top/u_4/u_1/current_state
add wave -noupdate -format Literal /top/u_4/u_1/current_command
add wave -noupdate -format Logic /top/u_4/clk_3_2
add wave -noupdate -format Logic /top/u_4/clk_32
add wave -noupdate -format Literal /top/u_4/u_1/rx_ubyte
add wave -noupdate -format Literal -radix hexadecimal /top/u_4/u_1/data_rx
add wave -noupdate -format Logic /top/u_4/u_1/data_rx_strb
add wave -noupdate -format Literal -radix hexadecimal /top/u_11/encode_din
add wave -noupdate -format Literal -radix hexadecimal /top/u_11/dcm_cmd_type
add wave -noupdate -format Literal -radix hexadecimal /top/u_11/dcm_cmd_addr
add wave -noupdate -format Literal -radix hexadecimal /top/u_11/dcm_cmd_data
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {10073729 ns} 0} {{Cursor 2} {5327 ns} 0} {{Cursor 3} {1972950 ns} 0} {{Cursor 4} {1079882 ns} 0}
configure wave -namecolwidth 281
configure wave -valuecolwidth 106
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {12367 ns} {12997 ns}
