onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic /top/usb_slwr
add wave -noupdate -format Logic /top/usb_flagb
add wave -noupdate -format Logic /top/usb_flagc
add wave -noupdate -format Logic /top/usb_sloe
add wave -noupdate -format Literal -radix hexadecimal /top/usb_fd
add wave -noupdate -expand -group BEEBUS
add wave -noupdate -group BEEBUS -format Literal -radix hexadecimal /top/beebus_addr
add wave -noupdate -group BEEBUS -format Logic /top/clk_16
add wave -noupdate -group BEEBUS -format Literal -radix hexadecimal /top/beebus_data
add wave -noupdate -group BEEBUS -format Logic /top/beebus_read
add wave -noupdate -group BEEBUS -format Logic /top/beebus_strb
add wave -noupdate -format Logic /top/powerup_reset
add wave -noupdate -format Logic /top/reset_daq
add wave -noupdate -format Logic /top/usb_reset_b
add wave -noupdate -format Logic /top/enable_daq
add wave -noupdate -format Logic /top/clk_16
add wave -noupdate -format Logic /top/clk_32
add wave -noupdate -format Logic /top/clk_64
add wave -noupdate -format Logic /top/asic_clk
add wave -noupdate -format Logic /top/asic_srck
add wave -noupdate -format Logic /top/asic_shiftin
add wave -noupdate -format Logic /top/asic_chipreset

TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {755361440 ps} 0} {{Cursor 2} {426670323 ps} 0} {{Cursor 3} {1972950000 ps} 0} {{Cursor 4} {2792512691 ps} 0}
configure wave -namecolwidth 281
configure wave -valuecolwidth 106
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
update
WaveRestoreZoom {610074268 ps} {610646148 ps}
