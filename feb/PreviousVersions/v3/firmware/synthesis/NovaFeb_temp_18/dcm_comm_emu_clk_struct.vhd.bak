--
-- VHDL Architecture feb_p2_lib.dcm_comm_emu_clk.struct
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC2)
--          at - 16:38:44 11/25/2007
--
-- using Mentor Graphics HDL Designer(TM) 2006.1 (Build 72)
-------------------------------------------------------------------------------
-- WHOAMI
-- Generate all clocking signals based on board XTAL and
-- FNAL DCM LVDS communication clock.
-------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;
library unisim;
use unisim.all;
library harvard_std;
use harvard_std.lppc_custom_fn_pkg.all;

entity dcm_comm_emu_clk is
  port(
    DCM_CLK       : in  std_logic;
    CLK_IN        : in  std_logic;
    CLK_16        : out std_logic;
    CLK_32        : out std_logic;
    CLK_64        : out std_logic;
    ASIC_CLK      : out std_logic;
    ASIC_CLK_EN   : in  boolean;
    asic_on       : out boolean;
    POWERUP_RESET : out boolean
    );

-- Declarations

end dcm_comm_emu_clk;

--
architecture struct of dcm_comm_emu_clk is

  component DCM_SP
    generic(
      CLK_FEEDBACK          : string;
      CLKDV_DIVIDE          : real;
      CLKFX_DIVIDE          : integer;
      CLKFX_MULTIPLY        : integer;
      CLKIN_DIVIDE_BY_2     : boolean;
      CLKIN_PERIOD          : real;
      CLKOUT_PHASE_SHIFT    : string;
      DESKEW_ADJUST         : string;
      DLL_FREQUENCY_MODE    : string;
      DUTY_CYCLE_CORRECTION : boolean;
      PHASE_SHIFT           : integer;
      STARTUP_WAIT          : boolean
      );
    port (CLKIN    : in  std_logic;
          CLKFB    : in  std_logic;
          RST      : in  std_logic;
          PSEN     : in  std_logic;
          PSINCDEC : in  std_logic;
          PSCLK    : in  std_logic;
          CLK0     : out std_logic;
          CLK90    : out std_logic;
          CLK180   : out std_logic;
          CLK270   : out std_logic;
          CLKDV    : out std_logic;
          CLK2X    : out std_logic;
          CLK2X180 : out std_logic;
          CLKFX    : out std_logic;
          CLKFX180 : out std_logic;
          STATUS   : out std_logic_vector (7 downto 0);
          LOCKED   : out std_logic;
          PSDONE   : out std_logic);
  end component;

  component BUFG
    port(
      O : out std_ulogic;
      I : in  std_ulogic
      );
  end component;

  component BUFGCE
    port(
      O  : out std_ulogic;
      CE : in  std_ulogic;
      I  : in  std_ulogic
      );
  end component;

  signal DCM_CLK_16    : std_logic;
  signal DCM_CLK_32    : std_logic;
  signal DCM_CLK_64    : std_logic;
  signal CLK_16_int    : std_logic;
  signal CLK_32_int    : std_logic;
  signal CLK_64_int    : std_logic;
  signal DCM_32_LOCKED : std_logic;

begin
  CLK_16 <= CLK_16_int;
  CLK_32 <= CLK_32_int;
  CLK_64 <= CLK_64_int;

  DCM_32 : DCM_SP
    generic map(
      CLK_FEEDBACK          => "1X",
      CLKDV_DIVIDE          => 2.0,
      CLKFX_DIVIDE          => 2,
      CLKFX_MULTIPLY        => 2,
      CLKIN_DIVIDE_BY_2     => false,
      CLKIN_PERIOD          => 31.25,
      CLKOUT_PHASE_SHIFT    => "NONE",
      DESKEW_ADJUST         => "SYSTEM_SYNCHRONOUS",
      DLL_FREQUENCY_MODE    => "LOW",
      DUTY_CYCLE_CORRECTION => true,
      PHASE_SHIFT           => 0,
      STARTUP_WAIT          => true
      )
    port map (
      CLKFB    => CLK_32_int,
      CLKIN    => CLK_IN,
      PSCLK    => '0',
      PSEN     => '0',
      PSINCDEC => '0',
      RST      => '0',
      CLKDV    => DCM_CLK_16,
      CLKFX    => open,
      CLKFX180 => open,
      CLK0     => DCM_CLK_32,
      CLK2X    => DCM_CLK_64,
      CLK2X180 => open,
      CLK90    => open,
      CLK180   => open,
      CLK270   => open,
      LOCKED   => DCM_32_LOCKED,
      PSDONE   => open,
      STATUS   => open
      );

  CLK_16_BUFG : BUFG
    port map (
      O => CLK_16_int,
      I => DCM_CLK_16
      );

  CLK_32_BUFG : BUFG
    port map (
      O => CLK_32_int,
      I => DCM_CLK_32
      );

  CLK_64_BUFG : BUFG
    port map (
      O => CLK_64_int,
      I => DCM_CLK_64
      );

  CLK_ASIC_BUFGCE : BUFGCE
    port map (
      O  => ASIC_CLK,
      CE => ASIC_CLK_EN,
      I  => CLK_32_int
      );

end architecture struct;

