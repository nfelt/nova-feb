##
##  Copyright (c) Mentor Graphics Corporation, 1996-2004, All Rights Reserved.
##             Portions copyright 1991-2008 Compuware Corporation
##                       UNPUBLISHED, LICENSED SOFTWARE.
##            CONFIDENTIAL AND PROPRIETARY INFORMATION WHICH IS THE
##          PROPERTY OF MENTOR GRAPHICS CORPORATION OR ITS LICENSORS
#
# File created on: Thu Jun 18 14:36:52 2009
#

#
# Find clocks
#
puts "Finding clock. . ."
set MgsClocks {}
set MgsClockSrcs {}
set MgsClocks [concat $MgsClocks {DCM_CLK_N}]
set MgsClockSrcs [concat $MgsClockSrcs [MGS_Design::get_drivers {DCM_CLK_N}] ]
set MgsClocks [concat $MgsClocks {DCM_CLK_P}]
set MgsClockSrcs [concat $MgsClockSrcs [MGS_Design::get_drivers {DCM_CLK_P}] ]

#
# Create Clocks
#
puts "Creating clocks. . ."
remove_constraints
# line 125
#
set ucf {TIMESPEC "TS_DCM_CLK_N" = PERIOD "DCM_CLK_N" 25 HIGH 50% ;}
create_clock -name {DCM_CLK_N} -domain {TS_DCM_CLK_N} -period 25 -waveform { 0 12.5 } [MGS_Design::get_drivers {DCM_CLK_N}] 
# line 125
#
set ucf {TIMESPEC "TS_DCM_CLK_P" = PERIOD "DCM_CLK_P" 25 HIGH 50% ;}
create_clock -name {DCM_CLK_P} -domain {TS_DCM_CLK_P} -period 25 -waveform { 0 12.5 } [MGS_Design::get_drivers {DCM_CLK_P}] 
# line 122 ucf_timespec_t
# line 124 ucf_timespec_t

#
# Boundary Constraints
#
puts "Creating boundary constraints. . ."

#
# Attributes
#
puts "Setting attributes. . ."
# line 1 ucf_object_t
if [catch {set_attribute {ADC_CLKINN} -port -name {LOC} -value {AB4}} result]\
	{puts {Warning: set_attribute LOC on ADC_CLKINN failed}}
# line 1 ucf_object_t
if [catch {set_attribute {ADC_CLKINN} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_CLKINN failed}}
# line 2 ucf_object_t
if [catch {set_attribute {ADC_CLKINP} -port -name {LOC} -value {AB3}} result]\
	{puts {Warning: set_attribute LOC on ADC_CLKINP failed}}
# line 2 ucf_object_t
if [catch {set_attribute {ADC_CLKINP} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_CLKINP failed}}
# line 3 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(0)} -port -name {LOC} -value {E1}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAN(0) failed}}
# line 3 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(0)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAN(0) failed}}
# line 4 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(1)} -port -name {LOC} -value {F1}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAN(1) failed}}
# line 4 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(1)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAN(1) failed}}
# line 5 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(10)} -port -name {LOC} -value {M1}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAN(10) failed}}
# line 5 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(10)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAN(10) failed}}
# line 6 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(11)} -port -name {LOC} -value {N3}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAN(11) failed}}
# line 6 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(11)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAN(11) failed}}
# line 7 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(2)} -port -name {LOC} -value {F3}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAN(2) failed}}
# line 7 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(2)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAN(2) failed}}
# line 8 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(3)} -port -name {LOC} -value {G1}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAN(3) failed}}
# line 8 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(3)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAN(3) failed}}
# line 9 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(4)} -port -name {LOC} -value {H3}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAN(4) failed}}
# line 9 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(4)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAN(4) failed}}
# line 10 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(5)} -port -name {LOC} -value {J2}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAN(5) failed}}
# line 10 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(5)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAN(5) failed}}
# line 11 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(6)} -port -name {LOC} -value {K1}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAN(6) failed}}
# line 11 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(6)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAN(6) failed}}
# line 12 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(7)} -port -name {LOC} -value {K3}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAN(7) failed}}
# line 12 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(7)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAN(7) failed}}
# line 13 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(8)} -port -name {LOC} -value {L1}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAN(8) failed}}
# line 13 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(8)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAN(8) failed}}
# line 14 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(9)} -port -name {LOC} -value {M3}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAN(9) failed}}
# line 14 ucf_object_t
if [catch {set_attribute {ADC_DOUTAN(9)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAN(9) failed}}
# line 15 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(0)} -port -name {LOC} -value {E2}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAP(0) failed}}
# line 15 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(0)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAP(0) failed}}
# line 16 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(1)} -port -name {LOC} -value {F2}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAP(1) failed}}
# line 16 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(1)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAP(1) failed}}
# line 17 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(10)} -port -name {LOC} -value {M2}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAP(10) failed}}
# line 17 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(10)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAP(10) failed}}
# line 18 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(11)} -port -name {LOC} -value {N4}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAP(11) failed}}
# line 18 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(11)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAP(11) failed}}
# line 19 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(2)} -port -name {LOC} -value {F4}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAP(2) failed}}
# line 19 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(2)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAP(2) failed}}
# line 20 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(3)} -port -name {LOC} -value {G2}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAP(3) failed}}
# line 20 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(3)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAP(3) failed}}
# line 21 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(4)} -port -name {LOC} -value {H4}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAP(4) failed}}
# line 21 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(4)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAP(4) failed}}
# line 22 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(5)} -port -name {LOC} -value {J3}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAP(5) failed}}
# line 22 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(5)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAP(5) failed}}
# line 23 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(6)} -port -name {LOC} -value {K2}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAP(6) failed}}
# line 23 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(6)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAP(6) failed}}
# line 24 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(7)} -port -name {LOC} -value {K4}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAP(7) failed}}
# line 24 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(7)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAP(7) failed}}
# line 25 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(8)} -port -name {LOC} -value {L2}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAP(8) failed}}
# line 25 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(8)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAP(8) failed}}
# line 26 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(9)} -port -name {LOC} -value {L4}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTAP(9) failed}}
# line 26 ucf_object_t
if [catch {set_attribute {ADC_DOUTAP(9)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTAP(9) failed}}
# line 27 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(0)} -port -name {LOC} -value {P4}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBN(0) failed}}
# line 27 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(0)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBN(0) failed}}
# line 28 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(1)} -port -name {LOC} -value {P6}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBN(1) failed}}
# line 28 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(1)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBN(1) failed}}
# line 29 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(10)} -port -name {LOC} -value {AB2}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBN(10) failed}}
# line 29 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(10)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBN(10) failed}}
# line 30 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(11)} -port -name {LOC} -value {AA4}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBN(11) failed}}
# line 30 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(11)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBN(11) failed}}
# line 31 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(2)} -port -name {LOC} -value {T4}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBN(2) failed}}
# line 31 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(2)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBN(2) failed}}
# line 32 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(3)} -port -name {LOC} -value {R6}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBN(3) failed}}
# line 32 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(3)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBN(3) failed}}
# line 33 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(4)} -port -name {LOC} -value {T2}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBN(4) failed}}
# line 33 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(4)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBN(4) failed}}
# line 34 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(5)} -port -name {LOC} -value {U2}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBN(5) failed}}
# line 34 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(5)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBN(5) failed}}
# line 35 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(6)} -port -name {LOC} -value {V3}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBN(6) failed}}
# line 35 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(6)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBN(6) failed}}
# line 36 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(7)} -port -name {LOC} -value {W2}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBN(7) failed}}
# line 36 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(7)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBN(7) failed}}
# line 37 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(8)} -port -name {LOC} -value {Y2}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBN(8) failed}}
# line 37 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(8)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBN(8) failed}}
# line 38 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(9)} -port -name {LOC} -value {Y5}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBN(9) failed}}
# line 38 ucf_object_t
if [catch {set_attribute {ADC_DOUTBN(9)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBN(9) failed}}
# line 39 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(0)} -port -name {LOC} -value {P3}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBP(0) failed}}
# line 39 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(0)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBP(0) failed}}
# line 40 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(1)} -port -name {LOC} -value {P5}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBP(1) failed}}
# line 40 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(1)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBP(1) failed}}
# line 41 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(10)} -port -name {LOC} -value {AB1}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBP(10) failed}}
# line 41 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(10)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBP(10) failed}}
# line 42 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(11)} -port -name {LOC} -value {AA3}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBP(11) failed}}
# line 42 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(11)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBP(11) failed}}
# line 43 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(2)} -port -name {LOC} -value {R3}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBP(2) failed}}
# line 43 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(2)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBP(2) failed}}
# line 44 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(3)} -port -name {LOC} -value {R5}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBP(3) failed}}
# line 44 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(3)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBP(3) failed}}
# line 45 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(4)} -port -name {LOC} -value {T1}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBP(4) failed}}
# line 45 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(4)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBP(4) failed}}
# line 46 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(5)} -port -name {LOC} -value {U1}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBP(5) failed}}
# line 46 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(5)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBP(5) failed}}
# line 47 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(6)} -port -name {LOC} -value {V2}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBP(6) failed}}
# line 47 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(6)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBP(6) failed}}
# line 48 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(7)} -port -name {LOC} -value {W1}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBP(7) failed}}
# line 48 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(7)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBP(7) failed}}
# line 49 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(8)} -port -name {LOC} -value {Y1}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBP(8) failed}}
# line 49 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(8)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBP(8) failed}}
# line 50 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(9)} -port -name {LOC} -value {Y4}} result]\
	{puts {Warning: set_attribute LOC on ADC_DOUTBP(9) failed}}
# line 50 ucf_object_t
if [catch {set_attribute {ADC_DOUTBP(9)} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_DOUTBP(9) failed}}
# line 51 ucf_object_t
if [catch {set_attribute {ADC_ERROR} -port -name {LOC} -value {AA1}} result]\
	{puts {Warning: set_attribute LOC on ADC_ERROR failed}}
# line 51 ucf_object_t
if [catch {set_attribute {ADC_ERROR} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ADC_ERROR failed}}
# line 52 ucf_object_t
if [catch {set_attribute {ASIC_CHIPRESET} -port -name {LOC} -value {AA2}} result]\
	{puts {Warning: set_attribute LOC on ASIC_CHIPRESET failed}}
# line 52 ucf_object_t
if [catch {set_attribute {ASIC_CHIPRESET} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ASIC_CHIPRESET failed}}
# line 53 ucf_object_t
if [catch {set_attribute {ASIC_OUTCLK_N} -port -name {LOC} -value {AD2}} result]\
	{puts {Warning: set_attribute LOC on ASIC_OUTCLK_N failed}}
# line 53 ucf_object_t
if [catch {set_attribute {ASIC_OUTCLK_N} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ASIC_OUTCLK_N failed}}
# line 54 ucf_object_t
if [catch {set_attribute {ASIC_OUTCLK_P} -port -name {LOC} -value {AD1}} result]\
	{puts {Warning: set_attribute LOC on ASIC_OUTCLK_P failed}}
# line 54 ucf_object_t
if [catch {set_attribute {ASIC_OUTCLK_P} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ASIC_OUTCLK_P failed}}
# line 55 ucf_object_t
if [catch {set_attribute {ASIC_SHIFTIN} -port -name {LOC} -value {AA5}} result]\
	{puts {Warning: set_attribute LOC on ASIC_SHIFTIN failed}}
# line 55 ucf_object_t
if [catch {set_attribute {ASIC_SHIFTIN} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ASIC_SHIFTIN failed}}
# line 56 ucf_object_t
if [catch {set_attribute {ASIC_SRCK} -port -name {LOC} -value {AC2}} result]\
	{puts {Warning: set_attribute LOC on ASIC_SRCK failed}}
# line 56 ucf_object_t
if [catch {set_attribute {ASIC_SRCK} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ASIC_SRCK failed}}
# line 57 ucf_object_t
if [catch {set_attribute {ASIC_SHIFTOUT} -port -name {LOC} -value {D1}} result]\
	{puts {Warning: set_attribute LOC on ASIC_SHIFTOUT failed}}
# line 57 ucf_object_t
if [catch {set_attribute {ASIC_SHIFTOUT} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ASIC_SHIFTOUT failed}}
# line 58 ucf_object_t
if [catch {set_attribute {ASIC_SHAPERRST} -port -name {LOC} -value {D2}} result]\
	{puts {Warning: set_attribute LOC on ASIC_SHAPERRST failed}}
# line 58 ucf_object_t
if [catch {set_attribute {ASIC_SHAPERRST} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ASIC_SHAPERRST failed}}
# line 59 ucf_object_t
if [catch {set_attribute {ASIC_INTEGRST} -port -name {LOC} -value {E3}} result]\
	{puts {Warning: set_attribute LOC on ASIC_INTEGRST failed}}
# line 59 ucf_object_t
if [catch {set_attribute {ASIC_INTEGRST} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ASIC_INTEGRST failed}}
# line 60 ucf_object_t
if [catch {set_attribute {ASIC_TESTINJECT} -port -name {LOC} -value {AC1}} result]\
	{puts {Warning: set_attribute LOC on ASIC_TESTINJECT failed}}
# line 60 ucf_object_t
if [catch {set_attribute {ASIC_TESTINJECT} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on ASIC_TESTINJECT failed}}
# line 61 ucf_object_t
if [catch {set_attribute {DCM_CLK_N} -port -name {LOC} -value {B14}} result]\
	{puts {Warning: set_attribute LOC on DCM_CLK_N failed}}
# line 61 ucf_object_t
if [catch {set_attribute {DCM_CLK_N} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on DCM_CLK_N failed}}
# line 62 ucf_object_t
if [catch {set_attribute {DCM_CLK_P} -port -name {LOC} -value {C14}} result]\
	{puts {Warning: set_attribute LOC on DCM_CLK_P failed}}
# line 62 ucf_object_t
if [catch {set_attribute {DCM_CLK_P} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on DCM_CLK_P failed}}
# line 63 ucf_object_t
if [catch {set_attribute {LA(0)} -port -name {LOC} -value {K26}} result]\
	{puts {Warning: set_attribute LOC on LA(0) failed}}
# line 63 ucf_object_t
if [catch {set_attribute {LA(0)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(0) failed}}
# line 64 ucf_object_t
if [catch {set_attribute {LA(1)} -port -name {LOC} -value {C23}} result]\
	{puts {Warning: set_attribute LOC on LA(1) failed}}
# line 64 ucf_object_t
if [catch {set_attribute {LA(1)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(1) failed}}
# line 65 ucf_object_t
if [catch {set_attribute {LA(2)} -port -name {LOC} -value {K25}} result]\
	{puts {Warning: set_attribute LOC on LA(2) failed}}
# line 65 ucf_object_t
if [catch {set_attribute {LA(2)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(2) failed}}
# line 66 ucf_object_t
if [catch {set_attribute {LA(3)} -port -name {LOC} -value {C22}} result]\
	{puts {Warning: set_attribute LOC on LA(3) failed}}
# line 66 ucf_object_t
if [catch {set_attribute {LA(3)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(3) failed}}
# line 67 ucf_object_t
if [catch {set_attribute {LA(4)} -port -name {LOC} -value {G26}} result]\
	{puts {Warning: set_attribute LOC on LA(4) failed}}
# line 67 ucf_object_t
if [catch {set_attribute {LA(4)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(4) failed}}
# line 68 ucf_object_t
if [catch {set_attribute {LA(5)} -port -name {LOC} -value {G17}} result]\
	{puts {Warning: set_attribute LOC on LA(5) failed}}
# line 68 ucf_object_t
if [catch {set_attribute {LA(5)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(5) failed}}
# line 69 ucf_object_t
if [catch {set_attribute {LA(6)} -port -name {LOC} -value {K24}} result]\
	{puts {Warning: set_attribute LOC on LA(6) failed}}
# line 69 ucf_object_t
if [catch {set_attribute {LA(6)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(6) failed}}
# line 70 ucf_object_t
if [catch {set_attribute {LA(7)} -port -name {LOC} -value {G16}} result]\
	{puts {Warning: set_attribute LOC on LA(7) failed}}
# line 70 ucf_object_t
if [catch {set_attribute {LA(7)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(7) failed}}
# line 71 ucf_object_t
if [catch {set_attribute {LA(8)} -port -name {LOC} -value {J24}} result]\
	{puts {Warning: set_attribute LOC on LA(8) failed}}
# line 71 ucf_object_t
if [catch {set_attribute {LA(8)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(8) failed}}
# line 72 ucf_object_t
if [catch {set_attribute {LA(9)} -port -name {LOC} -value {G15}} result]\
	{puts {Warning: set_attribute LOC on LA(9) failed}}
# line 72 ucf_object_t
if [catch {set_attribute {LA(9)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(9) failed}}
# line 73 ucf_object_t
if [catch {set_attribute {LA(10)} -port -name {LOC} -value {J25}} result]\
	{puts {Warning: set_attribute LOC on LA(10) failed}}
# line 73 ucf_object_t
if [catch {set_attribute {LA(10)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(10) failed}}
# line 74 ucf_object_t
if [catch {set_attribute {LA(11)} -port -name {LOC} -value {G14}} result]\
	{puts {Warning: set_attribute LOC on LA(11) failed}}
# line 74 ucf_object_t
if [catch {set_attribute {LA(11)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(11) failed}}
# line 75 ucf_object_t
if [catch {set_attribute {LA(12)} -port -name {LOC} -value {J23}} result]\
	{puts {Warning: set_attribute LOC on LA(12) failed}}
# line 75 ucf_object_t
if [catch {set_attribute {LA(12)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(12) failed}}
# line 76 ucf_object_t
if [catch {set_attribute {LA(13)} -port -name {LOC} -value {B23}} result]\
	{puts {Warning: set_attribute LOC on LA(13) failed}}
# line 76 ucf_object_t
if [catch {set_attribute {LA(13)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(13) failed}}
# line 77 ucf_object_t
if [catch {set_attribute {LA(14)} -port -name {LOC} -value {J22}} result]\
	{puts {Warning: set_attribute LOC on LA(14) failed}}
# line 77 ucf_object_t
if [catch {set_attribute {LA(14)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(14) failed}}
# line 78 ucf_object_t
if [catch {set_attribute {LA(15)} -port -name {LOC} -value {A23}} result]\
	{puts {Warning: set_attribute LOC on LA(15) failed}}
# line 78 ucf_object_t
if [catch {set_attribute {LA(15)} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on LA(15) failed}}
# line 79 ucf_object_t
if [catch {set_attribute {INSTRUMENT_TRIGGER} -port -name {LOC} -value {A22}} result]\
	{puts {Warning: set_attribute LOC on INSTRUMENT_TRIGGER failed}}
# line 79 ucf_object_t
if [catch {set_attribute {INSTRUMENT_TRIGGER} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on INSTRUMENT_TRIGGER failed}}
# line 80 ucf_object_t
if [catch {set_attribute {SER_NUM_CS_B} -port -name {LOC} -value {P25}} result]\
	{puts {Warning: set_attribute LOC on SER_NUM_CS_B failed}}
# line 80 ucf_object_t
if [catch {set_attribute {SER_NUM_CS_B} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on SER_NUM_CS_B failed}}
# line 81 ucf_object_t
if [catch {set_attribute {SPI_DIN} -port -name {LOC} -value {U26}} result]\
	{puts {Warning: set_attribute LOC on SPI_DIN failed}}
# line 81 ucf_object_t
if [catch {set_attribute {SPI_DIN} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on SPI_DIN failed}}
# line 82 ucf_object_t
if [catch {set_attribute {SPI_DOUT} -port -name {LOC} -value {R26}} result]\
	{puts {Warning: set_attribute LOC on SPI_DOUT failed}}
# line 82 ucf_object_t
if [catch {set_attribute {SPI_DOUT} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on SPI_DOUT failed}}
# line 83 ucf_object_t
if [catch {set_attribute {SPI_SCLK} -port -name {LOC} -value {T26}} result]\
	{puts {Warning: set_attribute LOC on SPI_SCLK failed}}
# line 83 ucf_object_t
if [catch {set_attribute {SPI_SCLK} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on SPI_SCLK failed}}
# line 84 ucf_object_t
if [catch {set_attribute {TEC_ADC_CS_B} -port -name {LOC} -value {V25}} result]\
	{puts {Warning: set_attribute LOC on TEC_ADC_CS_B failed}}
# line 84 ucf_object_t
if [catch {set_attribute {TEC_ADC_CS_B} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on TEC_ADC_CS_B failed}}
# line 85 ucf_object_t
if [catch {set_attribute {TEC_DAC_LDAC_B} -port -name {LOC} -value {Y26}} result]\
	{puts {Warning: set_attribute LOC on TEC_DAC_LDAC_B failed}}
# line 85 ucf_object_t
if [catch {set_attribute {TEC_DAC_LDAC_B} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on TEC_DAC_LDAC_B failed}}
# line 86 ucf_object_t
if [catch {set_attribute {TEC_DAC_SYNC_B} -port -name {LOC} -value {AA26}} result]\
	{puts {Warning: set_attribute LOC on TEC_DAC_SYNC_B failed}}
# line 86 ucf_object_t
if [catch {set_attribute {TEC_DAC_SYNC_B} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on TEC_DAC_SYNC_B failed}}
# line 87 ucf_object_t
if [catch {set_attribute {TEC_ENABLE} -port -name {LOC} -value {U25}} result]\
	{puts {Warning: set_attribute LOC on TEC_ENABLE failed}}
# line 87 ucf_object_t
if [catch {set_attribute {TEC_ENABLE} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on TEC_ENABLE failed}}
# line 88 ucf_object_t
if [catch {set_attribute {TEMP_SENSOR_CS_B} -port -name {LOC} -value {AB26}} result]\
	{puts {Warning: set_attribute LOC on TEMP_SENSOR_CS_B failed}}
# line 88 ucf_object_t
if [catch {set_attribute {TEMP_SENSOR_CS_B} -port -name {IOSTANDARD} -value {LVCMOS25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on TEMP_SENSOR_CS_B failed}}
# line 89 ucf_object_t
if [catch {set_attribute {USB_FD(0)} -port -name {LOC} -value {B12}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(0) failed}}
# line 89 ucf_object_t
if [catch {set_attribute {USB_FD(0)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(0) failed}}
# line 90 ucf_object_t
if [catch {set_attribute {USB_FD(1)} -port -name {LOC} -value {A12}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(1) failed}}
# line 90 ucf_object_t
if [catch {set_attribute {USB_FD(1)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(1) failed}}
# line 91 ucf_object_t
if [catch {set_attribute {USB_FD(10)} -port -name {LOC} -value {B6}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(10) failed}}
# line 91 ucf_object_t
if [catch {set_attribute {USB_FD(10)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(10) failed}}
# line 92 ucf_object_t
if [catch {set_attribute {USB_FD(11)} -port -name {LOC} -value {A6}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(11) failed}}
# line 92 ucf_object_t
if [catch {set_attribute {USB_FD(11)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(11) failed}}
# line 93 ucf_object_t
if [catch {set_attribute {USB_FD(12)} -port -name {LOC} -value {B5}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(12) failed}}
# line 93 ucf_object_t
if [catch {set_attribute {USB_FD(12)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(12) failed}}
# line 94 ucf_object_t
if [catch {set_attribute {USB_FD(13)} -port -name {LOC} -value {A5}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(13) failed}}
# line 94 ucf_object_t
if [catch {set_attribute {USB_FD(13)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(13) failed}}
# line 95 ucf_object_t
if [catch {set_attribute {USB_FD(14)} -port -name {LOC} -value {B4}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(14) failed}}
# line 95 ucf_object_t
if [catch {set_attribute {USB_FD(14)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(14) failed}}
# line 96 ucf_object_t
if [catch {set_attribute {USB_FD(15)} -port -name {LOC} -value {A4}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(15) failed}}
# line 96 ucf_object_t
if [catch {set_attribute {USB_FD(15)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(15) failed}}
# line 97 ucf_object_t
if [catch {set_attribute {USB_FD(2)} -port -name {LOC} -value {B11}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(2) failed}}
# line 97 ucf_object_t
if [catch {set_attribute {USB_FD(2)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(2) failed}}
# line 98 ucf_object_t
if [catch {set_attribute {USB_FD(3)} -port -name {LOC} -value {A11}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(3) failed}}
# line 98 ucf_object_t
if [catch {set_attribute {USB_FD(3)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(3) failed}}
# line 99 ucf_object_t
if [catch {set_attribute {USB_FD(4)} -port -name {LOC} -value {B10}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(4) failed}}
# line 99 ucf_object_t
if [catch {set_attribute {USB_FD(4)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(4) failed}}
# line 100 ucf_object_t
if [catch {set_attribute {USB_FD(5)} -port -name {LOC} -value {A10}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(5) failed}}
# line 100 ucf_object_t
if [catch {set_attribute {USB_FD(5)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(5) failed}}
# line 101 ucf_object_t
if [catch {set_attribute {USB_FD(6)} -port -name {LOC} -value {B9}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(6) failed}}
# line 101 ucf_object_t
if [catch {set_attribute {USB_FD(6)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(6) failed}}
# line 102 ucf_object_t
if [catch {set_attribute {USB_FD(7)} -port -name {LOC} -value {C9}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(7) failed}}
# line 102 ucf_object_t
if [catch {set_attribute {USB_FD(7)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(7) failed}}
# line 103 ucf_object_t
if [catch {set_attribute {USB_FD(8)} -port -name {LOC} -value {C5}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(8) failed}}
# line 103 ucf_object_t
if [catch {set_attribute {USB_FD(8)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(8) failed}}
# line 104 ucf_object_t
if [catch {set_attribute {USB_FD(9)} -port -name {LOC} -value {A7}} result]\
	{puts {Warning: set_attribute LOC on USB_FD(9) failed}}
# line 104 ucf_object_t
if [catch {set_attribute {USB_FD(9)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FD(9) failed}}
# line 105 ucf_object_t
if [catch {set_attribute {USB_FIFOADR(0)} -port -name {LOC} -value {C4}} result]\
	{puts {Warning: set_attribute LOC on USB_FIFOADR(0) failed}}
# line 105 ucf_object_t
if [catch {set_attribute {USB_FIFOADR(0)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FIFOADR(0) failed}}
# line 106 ucf_object_t
if [catch {set_attribute {USB_FIFOADR(1)} -port -name {LOC} -value {B7}} result]\
	{puts {Warning: set_attribute LOC on USB_FIFOADR(1) failed}}
# line 106 ucf_object_t
if [catch {set_attribute {USB_FIFOADR(1)} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FIFOADR(1) failed}}
# line 107 ucf_object_t
if [catch {set_attribute {USB_FLAGB} -port -name {LOC} -value {C8}} result]\
	{puts {Warning: set_attribute LOC on USB_FLAGB failed}}
# line 107 ucf_object_t
if [catch {set_attribute {USB_FLAGB} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FLAGB failed}}
# line 108 ucf_object_t
if [catch {set_attribute {USB_FLAGC} -port -name {LOC} -value {A8}} result]\
	{puts {Warning: set_attribute LOC on USB_FLAGC failed}}
# line 108 ucf_object_t
if [catch {set_attribute {USB_FLAGC} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_FLAGC failed}}
# line 109 ucf_object_t
if [catch {set_attribute {USB_IFCLK} -port -name {LOC} -value {A13}} result]\
	{puts {Warning: set_attribute LOC on USB_IFCLK failed}}
# line 109 ucf_object_t
if [catch {set_attribute {USB_IFCLK} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_IFCLK failed}}
# line 110 ucf_object_t
if [catch {set_attribute {USB_PKTEND} -port -name {LOC} -value {C6}} result]\
	{puts {Warning: set_attribute LOC on USB_PKTEND failed}}
# line 110 ucf_object_t
if [catch {set_attribute {USB_PKTEND} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_PKTEND failed}}
# line 111 ucf_object_t
if [catch {set_attribute {USB_RESET_B} -port -name {LOC} -value {B13}} result]\
	{puts {Warning: set_attribute LOC on USB_RESET_B failed}}
# line 111 ucf_object_t
if [catch {set_attribute {USB_RESET_B} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_RESET_B failed}}
# line 112 ucf_object_t
if [catch {set_attribute {USB_SLOE} -port -name {LOC} -value {B8}} result]\
	{puts {Warning: set_attribute LOC on USB_SLOE failed}}
# line 112 ucf_object_t
if [catch {set_attribute {USB_SLOE} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_SLOE failed}}
# line 113 ucf_object_t
if [catch {set_attribute {USB_SLRD} -port -name {LOC} -value {A3}} result]\
	{puts {Warning: set_attribute LOC on USB_SLRD failed}}
# line 113 ucf_object_t
if [catch {set_attribute {USB_SLRD} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_SLRD failed}}
# line 114 ucf_object_t
if [catch {set_attribute {USB_SLWR} -port -name {LOC} -value {B3}} result]\
	{puts {Warning: set_attribute LOC on USB_SLWR failed}}
# line 114 ucf_object_t
if [catch {set_attribute {USB_SLWR} -port -name {IOSTANDARD} -value {LVCMOS33}} result]\
	{puts {Warning: set_attribute IOSTANDARD on USB_SLWR failed}}
# line 115 ucf_object_t
if [catch {set_attribute {DCM_COMMAND_N} -port -name {LOC} -value {C19}} result]\
	{puts {Warning: set_attribute LOC on DCM_COMMAND_N failed}}
# line 115 ucf_object_t
if [catch {set_attribute {DCM_COMMAND_N} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on DCM_COMMAND_N failed}}
# line 116 ucf_object_t
if [catch {set_attribute {DCM_COMMAND_P} -port -name {LOC} -value {D19}} result]\
	{puts {Warning: set_attribute LOC on DCM_COMMAND_P failed}}
# line 116 ucf_object_t
if [catch {set_attribute {DCM_COMMAND_P} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on DCM_COMMAND_P failed}}
# line 117 ucf_object_t
if [catch {set_attribute {DCM_SYNC_N} -port -name {LOC} -value {C21}} result]\
	{puts {Warning: set_attribute LOC on DCM_SYNC_N failed}}
# line 117 ucf_object_t
if [catch {set_attribute {DCM_SYNC_N} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on DCM_SYNC_N failed}}
# line 118 ucf_object_t
if [catch {set_attribute {DCM_SYNC_P} -port -name {LOC} -value {D21}} result]\
	{puts {Warning: set_attribute LOC on DCM_SYNC_P failed}}
# line 118 ucf_object_t
if [catch {set_attribute {DCM_SYNC_P} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on DCM_SYNC_P failed}}
# line 119 ucf_object_t
if [catch {set_attribute {DCM_DATA_N} -port -name {LOC} -value {D17}} result]\
	{puts {Warning: set_attribute LOC on DCM_DATA_N failed}}
# line 119 ucf_object_t
if [catch {set_attribute {DCM_DATA_N} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on DCM_DATA_N failed}}
# line 120 ucf_object_t
if [catch {set_attribute {DCM_DATA_P} -port -name {LOC} -value {E17}} result]\
	{puts {Warning: set_attribute LOC on DCM_DATA_P failed}}
# line 120 ucf_object_t
if [catch {set_attribute {DCM_DATA_P} -port -name {IOSTANDARD} -value {LVDS_25}} result]\
	{puts {Warning: set_attribute IOSTANDARD on DCM_DATA_P failed}}
