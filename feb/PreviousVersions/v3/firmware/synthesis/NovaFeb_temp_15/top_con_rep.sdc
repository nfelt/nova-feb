###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name iostandard -value "DEFAULT" -instance -type string U_10/u -design rtl 

set_attribute -name iostandard -value "lvds_25" -instance -type string U_7/u -design rtl 

set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_11_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6/lvds_ibuf_vec_11_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_10_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6/lvds_ibuf_vec_10_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_9_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6/lvds_ibuf_vec_9_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_8_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6/lvds_ibuf_vec_8_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_7_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6/lvds_ibuf_vec_7_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_6_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6/lvds_ibuf_vec_6_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_5_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6/lvds_ibuf_vec_5_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_4_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6/lvds_ibuf_vec_4_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_3_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6/lvds_ibuf_vec_3_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_2_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6/lvds_ibuf_vec_2_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_1_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6/lvds_ibuf_vec_1_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6/lvds_ibuf_vec_0_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6/lvds_ibuf_vec_0_u -design rtl 

set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9/lvds_ibuf_vec_11_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9/lvds_ibuf_vec_11_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9/lvds_ibuf_vec_10_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9/lvds_ibuf_vec_10_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9/lvds_ibuf_vec_9_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9/lvds_ibuf_vec_9_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9/lvds_ibuf_vec_8_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9/lvds_ibuf_vec_8_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9/lvds_ibuf_vec_7_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9/lvds_ibuf_vec_7_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9/lvds_ibuf_vec_6_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9/lvds_ibuf_vec_6_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9/lvds_ibuf_vec_5_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9/lvds_ibuf_vec_5_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9/lvds_ibuf_vec_4_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9/lvds_ibuf_vec_4_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9/lvds_ibuf_vec_3_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9/lvds_ibuf_vec_3_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9/lvds_ibuf_vec_2_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9/lvds_ibuf_vec_2_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9/lvds_ibuf_vec_1_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9/lvds_ibuf_vec_1_u -design rtl 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9/lvds_ibuf_vec_0_u -design rtl 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9/lvds_ibuf_vec_0_u -design rtl 

set_attribute -name iostandard -value "DEFAULT" -instance -type string I13/u -design rtl 

set_attribute -name iostandard -value "DEFAULT" -instance -type string I14/u -design rtl 

set_attribute -name iostandard -value "DEFAULT" -instance -type string I15/u -design rtl 

set_attribute -name STARTUP_WAIT -value "1" -instance -type default U_9/DCM_32 -design rtl 
set_attribute -name PHASE_SHIFT -value "0" -instance -type integer U_9/DCM_32 -design rtl 
set_attribute -name DUTY_CYCLE_CORRECTION -value "1" -instance -type default U_9/DCM_32 -design rtl 
set_attribute -name DFS_FREQUENCY_MODE -value "LOW" -instance -type string U_9/DCM_32 -design rtl 
set_attribute -name DESKEW_ADJUST -value "SYSTEM_SYNCHRONOUS" -instance -type string U_9/DCM_32 -design rtl 
set_attribute -name CLKOUT_PHASE_SHIFT -value "NONE" -instance -type string U_9/DCM_32 -design rtl 
set_attribute -name CLKIN_PERIOD -value "31.25" -instance -type default U_9/DCM_32 -design rtl 
set_attribute -name CLKIN_DIVIDE_BY_2 -value "0" -instance -type default U_9/DCM_32 -design rtl 
set_attribute -name CLKFX_MULTIPLY -value "2" -instance -type integer U_9/DCM_32 -design rtl 
set_attribute -name CLKFX_DIVIDE -value "2" -instance -type integer U_9/DCM_32 -design rtl 
set_attribute -name CLKDV_DIVIDE -value "2.0" -instance -type default U_9/DCM_32 -design rtl 
set_attribute -name CLK_FEEDBACK -value "1X" -instance -type string U_9/DCM_32 -design rtl 
set_attribute -name DLL_FREQUENCY_MODE -value "LOW" -instance -type string U_9/DCM_32 -design rtl 
set_attribute -name SIM_MODE -value "SAFE" -instance -type string U_9/DCM_32 -design rtl 

set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/decode_8b10b/view_1_precision_decode_8b10b_XRTL -design rtl 
set_attribute -name COREGEN -value "TRUE" /test_lib/decode_8b10b/view_1_precision_decode_8b10b_XRTL -design rtl 

set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/encode_8b10b/view_1_precision_encode_8b10b_XRTL -design rtl 
set_attribute -name COREGEN -value "TRUE" /test_lib/encode_8b10b/view_1_precision_encode_8b10b_XRTL -design rtl 

set_attribute -name COREGEN -value "TRUE" /test_lib/decode_8b10b/view_1_precision_decode_8b10b_unfold_1_XRTL -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/decode_8b10b/view_1_precision_decode_8b10b_unfold_1_XRTL -design rtl 

set_attribute -name COREGEN -value "TRUE" /test_lib/encode_8b10b/view_1_precision_encode_8b10b_unfold_1_XRTL -design rtl 
set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/encode_8b10b/view_1_precision_encode_8b10b_unfold_1_XRTL -design rtl 

set_attribute -name ram_processed -value "true" -instance U_2/U_1/PEDESTAL_TABLE/PEDESTAL_TABLE -design rtl 

set_attribute -name ram_processed -value "true" -instance U_2/U_1/THRESHOLD_TABLE/THRESHOLD_TABLE -design rtl 

set_attribute -name ram_processed -value "true" -instance U_2/U_1/CHAN_DATA/CHAN_DATA -design rtl 

set_attribute -name ram_processed -value "true" -instance U_2/U_1/CHAN_DATA_0/CHAN_DATA_0 -design rtl 

set_attribute -name ram_processed -value "true" -instance U_2/U_1/CHAN_DATA_1/CHAN_DATA_1 -design rtl 

set_attribute -name ram_processed -value "true" -instance U_2/U_1/merged/merged -design rtl 



set_attribute -name ram_processed -value "true" -instance U_0/ADC_BUFFER/ADC_BUFFER -design rtl 


set_attribute -name state_vector -value "current_state" -type string /work/usb_translator_notri/fsm_unfold_737_XRTL -design rtl 



##################
# Clocks
##################
create_clock { DCM_CLK_P } -domain ClockDomain0 -name DCM_CLK_P -period 25.000000 -waveform { 0.000000 12.500000 } -design rtl 
create_generated_clock { U_5/U_3/modgen_counter_SPI_SCLK_COUNT/reg_q(11)/out } -domain ClockDomain1 -name U_5/U_3/modgen_counter_SPI_SCLK_COUNT/reg_q(11)/out -multiply_by 1  -divide_by 4 -source DCM_CLK_P -duty_cycle 50.000000  -design rtl 
create_generated_clock { U_4/U_11/reg_CLK_3_2_sig/out } -domain ClockDomain2 -name U_4/U_11/reg_CLK_3_2_sig/Q -multiply_by 1  -divide_by 2 -source DCM_CLK_P -duty_cycle 50.000000  -design rtl 

