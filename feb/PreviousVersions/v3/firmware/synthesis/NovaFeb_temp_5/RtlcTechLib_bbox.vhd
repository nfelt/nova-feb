
-- 
-- Definition of  IBUFDS
-- 
--      Thu May  7 16:10:56 2009
--      
--      Precision RTL Synthesis, 2008a.47
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity IBUFDS is 
   generic (CAPACITANCE : string;
      DIFF_TERM : boolean;
      IBUF_DELAY_VALUE : string;
      IFD_DELAY_VALUE : string;
      IOSTANDARD : string) ;
   
   port (
      O : OUT std_logic ;
      I : IN std_logic ;
      IB : IN std_logic) ;attribute RTLC_TECH_CELL: boolean;
   attribute RTLC_TECH_CELL of 
      IBUFDS : entity is true;
      end IBUFDS ;

architecture NETLIST of IBUFDS is       
      begin
      end NETLIST ;
      

-- 
-- Definition of  IBUFGDS
-- 
--      Thu May  7 16:10:56 2009
--      
--      Precision RTL Synthesis, 2008a.47
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity IBUFGDS is 
   generic (CAPACITANCE : string;
      DIFF_TERM : boolean;
      IBUF_DELAY_VALUE : string;
      IOSTANDARD : string) ;
   
   port (
      O : OUT std_logic ;
      I : IN std_logic ;
      IB : IN std_logic) ;attribute RTLC_TECH_CELL: boolean;
   attribute RTLC_TECH_CELL of 
      IBUFGDS : entity is true;
      end IBUFGDS ;

architecture NETLIST of IBUFGDS is       
      begin
      end NETLIST ;
      

-- 
-- Definition of  DCM
-- 
--      Thu May  7 16:10:56 2009
--      
--      Precision RTL Synthesis, 2008a.47
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity DCM is 
   generic (CLKDV_DIVIDE : real;
      CLKFX_DIVIDE : integer;
      CLKFX_MULTIPLY : integer;
      CLKIN_DIVIDE_BY_2 : boolean;
      CLKIN_PERIOD : real;
      CLKOUT_PHASE_SHIFT : string;
      CLK_FEEDBACK : string;
      DESKEW_ADJUST : string;
      DFS_FREQUENCY_MODE : string;
      DLL_FREQUENCY_MODE : string;
      DSS_MODE : string;
      DUTY_CYCLE_CORRECTION : boolean;
      FACTORY_JF : bit_vector;
      phase_shift : integer;
      STARTUP_WAIT : boolean) ;
   
   port (
      CLK0 : OUT std_logic ;
      CLK180 : OUT std_logic ;
      CLK270 : OUT std_logic ;
      CLK2X : OUT std_logic ;
      CLK2X180 : OUT std_logic ;
      CLK90 : OUT std_logic ;
      CLKDV : OUT std_logic ;
      CLKFX : OUT std_logic ;
      CLKFX180 : OUT std_logic ;
      LOCKED : OUT std_logic ;
      PSDONE : OUT std_logic ;
      STATUS : OUT std_logic_vector (7 DOWNTO 0) ;
      CLKFB : IN std_logic ;
      CLKIN : IN std_logic ;
      DSSEN : IN std_logic ;
      PSCLK : IN std_logic ;
      PSEN : IN std_logic ;
      PSINCDEC : IN std_logic ;
      RST : IN std_logic) ;attribute RTLC_TECH_CELL: boolean;
   attribute RTLC_TECH_CELL of 
      DCM : entity is true;
      end DCM ;

architecture NETLIST of DCM is       
      begin
      end NETLIST ;
      

-- 
-- Definition of  BUFGCE
-- 
--      Thu May  7 16:10:56 2009
--      
--      Precision RTL Synthesis, 2008a.47
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity BUFGCE is 
   port (
      O : OUT std_logic ;
      CE : IN std_logic ;
      I : IN std_logic) ;attribute RTLC_TECH_CELL: boolean;
   attribute RTLC_TECH_CELL of 
      BUFGCE : entity is true;
      end BUFGCE ;

architecture NETLIST of BUFGCE is       
      begin
      end NETLIST ;
      

-- 
-- Definition of  OBUFDS
-- 
--      Thu May  7 16:10:56 2009
--      
--      Precision RTL Synthesis, 2008a.47
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity OBUFDS is 
   generic (CAPACITANCE : string;
      IOSTANDARD : string) ;
   
   port (
      O : OUT std_logic ;
      OB : OUT std_logic ;
      I : IN std_logic) ;attribute RTLC_TECH_CELL: boolean;
   attribute RTLC_TECH_CELL of 
      OBUFDS : entity is true;
      end OBUFDS ;

architecture NETLIST of OBUFDS is       
      begin
      end NETLIST ;
      

-- 
-- Definition of  BUFG
-- 
--      Thu May  7 16:10:56 2009
--      
--      Precision RTL Synthesis, 2008a.47
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity BUFG is 
   port (
      O : OUT std_logic ;
      I : IN std_logic) ;attribute RTLC_TECH_CELL: boolean;
   attribute RTLC_TECH_CELL of 
      BUFG : entity is true;
      end BUFG ;

architecture NETLIST of BUFG is       
      begin
      end NETLIST ;
      
