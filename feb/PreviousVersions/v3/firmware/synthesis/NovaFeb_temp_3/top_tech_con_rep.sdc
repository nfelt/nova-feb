###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name MAX_FANOUT -value "10000" -net LA_dup0(0) -design gatelevel 
set_attribute -name MAX_FANOUT -value "10000" -net DCM_CLK -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(11) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(10) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(9) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(8) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(7) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(6) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(5) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(4) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(3) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(2) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(1) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAN(0) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(11) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(10) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(9) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(8) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(7) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(6) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(5) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(4) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(3) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(2) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(1) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTAP(0) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBN(11) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBN(10) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBN(9) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBN(8) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBN(7) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBN(6) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBN(5) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBN(4) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBN(3) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBN(2) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBN(1) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBN(0) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBP(11) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBP(10) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBP(9) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBP(8) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBP(7) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBP(6) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBP(5) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBP(4) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBP(3) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBP(2) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBP(1) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_DOUTBP(0) -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port DCM_CLK_N -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port DCM_CLK_P -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port DCM_COMMAND_N -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port DCM_COMMAND_P -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port DCM_SYNC_N -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port DCM_SYNC_P -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_CLKINN -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ADC_CLKINP -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_OUTCLK_N -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port ASIC_OUTCLK_P -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port DCM_DATA_N -design gatelevel 
set_attribute -name NOPAD -value "TRUE" -port DCM_DATA_P -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string U_3_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string U_8_u -design gatelevel 
set_attribute -name iostandard -value "lvds_25" -instance -type string U_7_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_11_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_11_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_10_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_10_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_9_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_9_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_8_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_8_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_7_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_7_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_6_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_6_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_5_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_5_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_4_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_4_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_3_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_3_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_2_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_2_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_1_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_1_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I6_lvds_ibuf_vec_0_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I6_lvds_ibuf_vec_0_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9_lvds_ibuf_vec_11_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9_lvds_ibuf_vec_11_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9_lvds_ibuf_vec_10_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9_lvds_ibuf_vec_10_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9_lvds_ibuf_vec_9_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9_lvds_ibuf_vec_9_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9_lvds_ibuf_vec_8_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9_lvds_ibuf_vec_8_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9_lvds_ibuf_vec_7_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9_lvds_ibuf_vec_7_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9_lvds_ibuf_vec_6_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9_lvds_ibuf_vec_6_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9_lvds_ibuf_vec_5_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9_lvds_ibuf_vec_5_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9_lvds_ibuf_vec_4_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9_lvds_ibuf_vec_4_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9_lvds_ibuf_vec_3_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9_lvds_ibuf_vec_3_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9_lvds_ibuf_vec_2_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9_lvds_ibuf_vec_2_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9_lvds_ibuf_vec_1_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9_lvds_ibuf_vec_1_u -design gatelevel 
set_attribute -name IOSTANDARD -value "DEFAULT" -instance -type string I9_lvds_ibuf_vec_0_u -design gatelevel 
set_attribute -name DIFF_TERM -value "0" -instance -type default I9_lvds_ibuf_vec_0_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I13_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I14_u -design gatelevel 
set_attribute -name iostandard -value "DEFAULT" -instance -type string I15_u -design gatelevel 
set_attribute -name CLK_FEEDBACK -value "1X" -instance -type string U_9_DCM_32 -design gatelevel 
set_attribute -name CLKDV_DIVIDE -value "2.0" -instance -type default U_9_DCM_32 -design gatelevel 
set_attribute -name CLKFX_DIVIDE -value "2" -instance -type integer U_9_DCM_32 -design gatelevel 
set_attribute -name CLKFX_MULTIPLY -value "2" -instance -type integer U_9_DCM_32 -design gatelevel 
set_attribute -name CLKIN_DIVIDE_BY_2 -value "0" -instance -type default U_9_DCM_32 -design gatelevel 
set_attribute -name CLKIN_PERIOD -value "31.25" -instance -type default U_9_DCM_32 -design gatelevel 
set_attribute -name CLKOUT_PHASE_SHIFT -value "NONE" -instance -type string U_9_DCM_32 -design gatelevel 
set_attribute -name DESKEW_ADJUST -value "SYSTEM_SYNCHRONOUS" -instance -type string U_9_DCM_32 -design gatelevel 
set_attribute -name DFS_FREQUENCY_MODE -value "LOW" -instance -type string U_9_DCM_32 -design gatelevel 
set_attribute -name DUTY_CYCLE_CORRECTION -value "1" -instance -type default U_9_DCM_32 -design gatelevel 
set_attribute -name PHASE_SHIFT -value "0" -instance -type integer U_9_DCM_32 -design gatelevel 
set_attribute -name STARTUP_WAIT -value "1" -instance -type default U_9_DCM_32 -design gatelevel 
set_attribute -name DLL_FREQUENCY_MODE -value "LOW" -instance -type string U_9_DCM_32 -design gatelevel 
set_attribute -name SIM_MODE -value "SAFE" -instance -type string U_9_DCM_32 -design gatelevel 
set_attribute -name PART -value "3s4000fg676-5" -type string /work/top/struct -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_0/ADC_BUFFER/ix13887z26572 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_0/ADC_BUFFER/ix9899z26572 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_0/ADC_BUFFER/ix34372z26572 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_0/ADC_BUFFER/ix38360z26572 -design gatelevel 


set_attribute -name state_vector -value "current_state" /work/usb_translator_notri/fsm_unfold_737 -design gatelevel 
set_attribute -name COREGEN -value "TRUE" /test_lib/encode_8b10b/view_1_precision_encode_8b10b -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/encode_8b10b/view_1_precision_encode_8b10b -design gatelevel 


set_attribute -name COREGEN -value "TRUE" /test_lib/decode_8b10b/view_1_precision_decode_8b10b -design gatelevel 
set_attribute -name DONT_TOUCH -value "TRUE" /test_lib/decode_8b10b/view_1_precision_decode_8b10b -design gatelevel 

set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_2_U_1/CHAN_DATA_1_ix26354z55803 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_2_U_1/PEDESTAL_TABLE/ix39507z34088 -design gatelevel 

set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_2_U_1/THRESHOLD_TABLE/ix39507z34088 -design gatelevel 

set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_2_U_1/CHAN_DATA/ix35369z34088 -design gatelevel 

set_attribute -name WRITE_MODE -value "NO_CHANGE" -instance U_2_U_1/CHAN_DATA_0/ix61445z5757 -design gatelevel 

set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_2_U_1/merged/ix60298z30776 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance U_2_U_1/merged/ix26672z30776 -design gatelevel 




##################
# Clocks
##################

