clear all;
figure(1);
list = [42 44 47 50 54 58 62 68 75]
freq = int8(1000./list);
for i = 1:9;
DATAin = dlmread(['C:\data\datar_',num2str(list(i)),'.txt'],'');
CH = DATAin(:,[2 3 4 1]);
for k = 2:4
%linear regression
XDAT = CH(:,1);
YDAT = CH(:,k);
PXDAT(:,k-1) = XDAT;
PYDAT(:,k-1) = YDAT;
p0=polyfit(XDAT,YDAT,1);
SLOPE(i,k-1) = p0(1);
yy0=polyval(p0,XDAT);
PYY0(:,k-1) = yy0;
end
figure(i)
plot(PXDAT,PYDAT,'.',PXDAT,PYY0,':'); 
title(['Measured Chan 1 - 3 vs Chan 0  at ',num2str(freq(i)),' MHz sample rate'])
xlabel('Chan 0')
ylabel('Chan 1 - 3')
legend('Chan 1','Chan 2','Chan 3',2)
end


figure(70)

plot((1000./list),SLOPE,'--.')
xlabel('Sample rate in MHz')
ylabel('Linear fit slope')
legend('Chan 1','Chan 2','Chan 3',2)
grid on;

