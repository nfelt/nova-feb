
clear all;
figure(1);
list = [42 44 47 50 54 58 62 68 75]
freq = int8(1000./list);
i = 1;
DATAin = dlmread(['C:\data\dataq_',num2str(list(i)),'.txt'],'');
[m n] = size(DATAin);
n_words = m * n;
ch_dat = DATAin([1:n_words]);



figure(1);

subplot(2,1,1)
plot(ch_dat,'.')
title(['ADC "Pedestal" data at ',num2str(freq(i)),' MHz sample rate']);

subplot(2,1,2)
hbin = min(ch_dat):1:max(ch_dat);
hist(ch_dat,hbin);
title(['Hist. STD = ', num2str(std(ch_dat))])
label('frequency (Hz)')