clear all;
figure(1);
list = [80 68 58 50 44 41 70 60 51 46]
freq = int8(1000./list);
DAC_VAL = 16143:-1:124
%DAC_VAL = DAC_VAL/4;
for i = 1:10;
DATAin = dlmread(['C:\data\datanl_',num2str(list(i)),'.txt'],'');

DATAin = DATAin';
[m n] = size(DATAin);
n_words = m * n;
CH_DAT = DATAin([1:n_words]);
CHALL(:,i) = CH_DAT;
%linear regression
p0=polyfit(DAC_VAL,CH_DAT,1);
%SLOPE(i,k-1) = p0(1);
yy0=polyval(p0,DAC_VAL);

residu = yy0-CH_DAT;
fir = filter(ones(1,200)/200,1,residu);
figure(i)
plot(DAC_VAL,residu,'.',DAC_VAL,fir,'.');
%axis([0 4000 -3 3])

title(['Residuals from linear ramp input at ',num2str(freq(i)),' MHz sample rate'])
xlabel('DAC setting')
ylabel('ADC output')
%legend('Chan 1','Chan 2','Chan 3',2)
max(fir)
end
figure(100)
plot(CHALL);



