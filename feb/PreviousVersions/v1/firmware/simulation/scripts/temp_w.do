onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic /top/clk
add wave -noupdate -format Logic /top/USB_IFCLK

add wave -noupdate -format Logic /top/RESET
add wave -noupdate -format Logic /top/RUN

add wave -noupdate -format Logic /top/USB_SLOE
add wave -noupdate -format Logic /top/USB_SLRD
add wave -noupdate -format Logic /top/USB_SLWR
add wave -noupdate -format Logic -radix hexadecimal /top/USB_FD
add wave -noupdate -format Logic /top/USB_FLAGA
add wave -noupdate -format Logic /top/USB_FLAGB
add wave -noupdate -format Logic /top/USB_FLAGC
add wave -noupdate -format Logic /top/USB_FLAGD

add wave -noupdate -divider divo

TreeUpdate [SetDefaultTree]
WaveRestoreCursors {1221620 ps}
WaveRestoreZoom {1951790 ps} {2524130 ps}
configure wave -namecolwidth 223
configure wave -valuecolwidth 52
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
#add wave -noupdate -format Logic -radix hexadecimal /zpd_top/ff0/i9/mem_sel
#add wave -noupdate -format Literal -radix hexadecimal /zpd_top/ff0/i9/mem_addr
