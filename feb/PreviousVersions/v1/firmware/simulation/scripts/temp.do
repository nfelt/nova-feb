echo  adc_eval  test
restart
onbreak {run -continue}

force -freeze USB_IFCLK 1 0, 0 {50 ns} -r 100
force -freeze clk 1 0, 0 {50 ns} -r 100

# init usb_interface signals
force USB_FLAGA false
force USB_FLAGB false
force USB_FLAGC true
force USB_FLAGD false
force -drive sim:/top/usb_fd 1010101010101010 0
force USB_PA0 false
force USB_PA1 false

# init dac_interface signals
force DAC1_PLL_LOCK false

# init adc2_interface signals
force ADC2_DAP 0
force ADC2_DAN 1
force ADC2_DBP 0
force ADC2_DBN 1
force ADC2_DCP 0
force ADC2_DCN 1
force ADC2_DDP 0
force ADC2_DDN 1
force ADC2_FCOP 0
force ADC2_FCON 1
force ADC2_DCOP 0
force ADC2_DCON 1

# init adc_interface signals
force ADC_DOUTAP 16#000
force ADC_DOUTAN 16#FFF
force ADC_CLKOUTAP 0
force ADC_CLKOUTAN 1
force ADC_DOUTBP 16#000
force ADC_DOUTBN 16#FFF
force ADC_CLKOUTBP 0
force ADC_CLKOUTBN 1
force ADC_ERROR false

# do reset then run
run 300
force -deposit sim:/top/i5/u_2/board_mode 16#0000 0
run 500
force -deposit sim:/top/i5/u_2/board_mode 16#0001 0
run 500
force -deposit sim:/top/i5/u_2/board_mode 16#0002 0
run 500

force USB_FLAGC false
run 100
force -drive sim:/top/usb_fd 16#0c0b 0
run 100
force -drive sim:/top/usb_fd 16#0a09 0
run 100
force -drive sim:/top/usb_fd 16#0207 0
run 100
force -drive sim:/top/usb_fd 16#0605 0
run 100
force -drive sim:/top/usb_fd 16#0403 0
run 100
force -drive sim:/top/usb_fd 16#0201 0
run 100
force -drive sim:/top/usb_fd 16#0c0b 0
run 100
force -drive sim:/top/usb_fd 16#0a09 0
run 100
force -drive sim:/top/usb_fd 16#0207 0
run 100 
force USB_FLAGC true
force -drive sim:/top/usb_fd 16#ZZZZ 0
run 100


run 800

#force data 16#0001;  force d_strb 1 0, 0 10 ns; run 160 ns
#force I11/PHICONV_ADDR 16#1
#force MIO_DATA 16#ZZZZZZZZ
