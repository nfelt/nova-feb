-- VHDL Entity adc_eval_lib.top.symbol
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC4)
--          at - 18:18:33 01/30/2006
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2005.2 (Build 37)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;

ENTITY top IS
   PORT( 
      ADC2_DAN      : IN     std_logic;
      ADC2_DAP      : IN     std_logic;
      ADC2_DBN      : IN     std_logic;
      ADC2_DBP      : IN     std_logic;
      ADC2_DCN      : IN     std_logic;
      ADC2_DCON     : IN     std_logic;
      ADC2_DCOP     : IN     std_logic;
      ADC2_DCP      : IN     std_logic;
      ADC2_DDN      : IN     std_logic;
      ADC2_DDP      : IN     std_logic;
      ADC2_FCON     : IN     std_logic;
      ADC2_FCOP     : IN     std_logic;
      ADC_CLKOUTAN  : IN     std_logic;
      ADC_CLKOUTAP  : IN     std_logic;
      ADC_CLKOUTBN  : IN     std_logic;
      ADC_CLKOUTBP  : IN     std_logic;
      ADC_DOUTAN    : IN     std_logic_vector (11 DOWNTO 0);
      ADC_DOUTAP    : IN     std_logic_vector (11 DOWNTO 0);
      ADC_DOUTBN    : IN     std_logic_vector (11 DOWNTO 0);
      ADC_DOUTBP    : IN     std_logic_vector (11 DOWNTO 0);
      ADC_ERROR     : IN     std_logic_vector (15 DOWNTO 0);
      CLK           : IN     std_logic;
      DAQ1_PLL_LOCK : IN     boolean;
      USB_FLAGA     : IN     boolean;
      USB_FLAGB     : IN     boolean;
      USB_FLAGC     : IN     boolean;
      USB_FLAGD     : IN     boolean;
      USB_PA0       : IN     boolean;
      USB_PA1       : IN     boolean;
      ADC2_CLK      : OUT    std_logic;
      ADC2_PWDN     : OUT    boolean;
      ADC_CLKINN    : OUT    std_logic;
      ADC_CLKINP    : OUT    std_logic;
      ADC_ENABLE    : OUT    std_logic_vector (3 DOWNTO 0);
      ADC_MODE      : OUT    std_logic_vector (2 DOWNTO 0);
      DAQ1_CLKN     : OUT    std_logic;
      DAQ1_CLKP     : OUT    std_logic;
      DAQ1_P1B      : OUT    unsigned (13 DOWNTO 0);
      DAQ1_P2B      : OUT    unsigned (13 DOWNTO 0);
      DAQ1_RESET    : OUT    boolean;
      USB_CLK       : OUT    std_logic;
      USB_FIFOADR   : OUT    unsigned (1 DOWNTO 0);
      USB_IFCLK     : OUT    std_logic;
      USB_PKTEND_B  : OUT    boolean;
      USB_RESET_B   : OUT    boolean;
      USB_SLOE      : OUT    boolean;
      USB_SLRD      : OUT    boolean;
      USB_SLWR      : OUT    boolean;
      USB_WAKEUP_B  : OUT    boolean;
      USB_WU2       : OUT    boolean;
      USB_FD        : INOUT  unsigned (15 DOWNTO 0);
      USB_SCL       : INOUT  std_logic;
      USB_SDA       : INOUT  std_logic
   );

-- Declarations

END top ;

--
-- VHDL Architecture adc_eval_lib.top.struct
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC4)
--          at - 18:18:34 01/30/2006
--
-- Generated by Mentor Graphics' HDL Designer(TM) 2005.2 (Build 37)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;

LIBRARY unisim;
USE unisim.all;

LIBRARY adc_eval_lib;
LIBRARY harvard_std;

ARCHITECTURE struct OF top IS

   -- Architecture declarations

   -- Internal signal declarations
   SIGNAL ADC2_CHIP_EN     : boolean;
   SIGNAL ADC_CHAN_EN      : std_logic_vector(3 DOWNTO 0);
   SIGNAL ADC_CHIP_EN      : boolean;
   SIGNAL ADC_DATA_OUT     : unsigned(15 DOWNTO 0);
   SIGNAL ADC_DATA_STRB    : boolean;
   SIGNAL CH_EN            : std_logic_vector( 3 DOWNTO 0 );
   SIGNAL COMMAND_QUEUED   : boolean;
   SIGNAL COMMAND_STRB     : boolean;
   SIGNAL DAC_DATA_IN      : unsigned(13 DOWNTO 0);
   SIGNAL DAQ1_CLK         : std_logic;
   SIGNAL DAQ_DATA_IN      : unsigned(13 DOWNTO 0);
   SIGNAL FIFO_OUT_EN      : boolean;
   SIGNAL FIFO_RESET       : boolean;
   SIGNAL IN_CLK           : std_logic;
   SIGNAL OUT_CLK          : std_logic;
   SIGNAL OUT_STRB         : std_logic;
   SIGNAL RESET            : boolean;
   SIGNAL RUN              : boolean;
   SIGNAL USB_DATA         : unsigned(15 DOWNTO 0);
   SIGNAL USB_OUT_ENPT_RDY : boolean;
   SIGNAL USB_RESET        : boolean;
   SIGNAL i                : std_logic;
   SIGNAL o                : std_logic;
   SIGNAL o1               : std_logic_vector(11 DOWNTO 0);     -- output
   SIGNAL o2               : std_logic_vector(11 DOWNTO 0);     -- output
   SIGNAL o3               : std_logic;
   SIGNAL o4               : std_logic;                         -- output
   SIGNAL o5               : std_logic;                         -- output
   SIGNAL o6               : std_logic;                         -- output
   SIGNAL o7               : std_logic;                         -- output
   SIGNAL o8               : std_logic;
   SIGNAL o9               : std_logic;

   -- Implicit buffer signal declarations
   SIGNAL USB_IFCLK_internal : std_logic;


   -- Component Declarations
   COMPONENT adc2_interface
   PORT (
      CHAN_EN      : IN     std_logic_vector ( 3 DOWNTO 0 );
      CHIP_EN      : IN     boolean ;
      DA           : IN     std_logic ;
      DB           : IN     std_logic ;
      DC           : IN     std_logic ;
      DCO          : IN     std_logic ;
      DD           : IN     std_logic ;
      FCO          : IN     std_logic ;
      GLOBAL_RESET : IN     boolean ;
      GLOBAL_RUN   : IN     boolean ;
      LOCAL_CLK    : IN     std_logic ;
      CLK          : OUT    std_logic ;
      DATA_OUT     : OUT    unsigned (15 DOWNTO 0);
      DATA_STRB    : OUT    boolean ;
      PWDN         : OUT    boolean 
   );
   END COMPONENT;
   COMPONENT adc_interface
   PORT (
      CHIP_EN      : IN     boolean ;
      CH_EN        : IN     std_logic_vector ( 3 DOWNTO 0 );
      CLK_INA      : IN     std_logic ;
      CLK_INB      : IN     std_logic ;
      D_IN_A       : IN     std_logic_vector (11 DOWNTO 0);
      D_IN_B       : IN     std_logic_vector (11 DOWNTO 0);
      ERROR        : IN     std_logic_vector (15 DOWNTO 0);
      GLOBAL_RESET : IN     boolean ;
      GLOBAL_RUN   : IN     boolean ;
      LOCAL_CLK    : IN     std_logic ;
      CLK_OUT      : OUT    std_logic ;
      DATA_OUT     : OUT    unsigned (15 DOWNTO 0);
      DATA_STRB    : OUT    boolean ;
      ENABLE       : OUT    std_logic_vector ( 3 DOWNTO 0 );
      MODE         : OUT    std_logic_vector (2 DOWNTO 0)
   );
   END COMPONENT;
   COMPONENT controller
   PORT (
      CLK              : IN     std_logic ;
      COMMAND_QUEUED   : IN     boolean ;
      USB_OUT_ENPT_RDY : IN     boolean ;
      ADC_CHIP_EN      : OUT    boolean ;
      FIFO_OUT_EN      : OUT    boolean ;
      FIFO_RESET       : OUT    boolean ;
      OUT_STRB         : OUT    std_logic ;
      RESET            : OUT    boolean ;
      RUN              : OUT    boolean ;
      USB_DATA         : OUT    unsigned (15 DOWNTO 0);
      USB_RESET        : OUT    boolean 
   );
   END COMPONENT;
   COMPONENT dac_interface
   PORT (
      LOCAL_CLK    : IN     std_logic ;
      CLK          : OUT    std_logic ;
      P1B          : OUT    unsigned (13 DOWNTO 0);
      RESET        : OUT    boolean ;
      DATA_IN      : IN     unsigned ( 13 DOWNTO 0 );
      GLOBAL_RUN   : IN     boolean ;
      GLOBAL_RESET : IN     boolean ;
      P2B          : OUT    unsigned ( 13 DOWNTO 0 );
      PLL_LOCK     : IN     boolean 
   );
   END COMPONENT;
   COMPONENT data_buffer
   PORT (
      ADC_DATA_OUT  : IN     unsigned (15 DOWNTO 0);
      ADC_DATA_STRB : IN     boolean ;
      FIFO_OUT_EN   : IN     boolean ;
      FIFO_RESET    : IN     boolean ;
      IN_CLK        : IN     std_logic ;
      OUT_CLK       : IN     std_logic ;
      OUT_STRB      : OUT    std_logic ;
      USB_DATA      : OUT    unsigned (15 DOWNTO 0)
   );
   END COMPONENT;
   COMPONENT usb_interface
   PORT (
      FLAGA          : IN     boolean ;
      FLAGB          : IN     boolean ;
      FLAGC          : IN     boolean ;
      FLAGD          : IN     boolean ;
      IFCLK          : IN     std_logic ;
      LOCAL_CLK      : IN     std_logic ;
      LOCAL_RESET    : IN     boolean ;
      OUT_STRB       : IN     std_logic ;
      PA0            : IN     boolean ;
      PA1            : IN     boolean ;
      CLK            : OUT    std_logic ;
      COMMAND_QUEUED : OUT    boolean ;
      FIFOADR        : OUT    unsigned (1 DOWNTO 0);
      OUT_ENPT_RDY   : OUT    boolean ;
      PKTEND_B       : OUT    boolean ;
      RESET_B        : OUT    boolean ;
      SLOE           : OUT    boolean ;
      SLRD           : OUT    boolean ;
      SLWR           : OUT    boolean ;
      WAKEUP_B       : OUT    boolean ;
      WU2            : OUT    boolean ;
      DATA           : INOUT  unsigned (15 DOWNTO 0);
      FD             : INOUT  unsigned (15 DOWNTO 0);
      SCL            : INOUT  std_logic ;
      SDA            : INOUT  std_logic 
   );
   END COMPONENT;
   COMPONENT lvds_ibuf
   PORT (
      O  : OUT    std_logic ;
      I  : IN     std_logic ;
      IB : IN     std_logic 
   );
   END COMPONENT;
   COMPONENT lvds_ibufg
   PORT (
      i  : IN     std_logic ;
      ib : IN     std_logic ;
      o  : OUT    std_logic 
   );
   END COMPONENT;
   COMPONENT lvds_ibufn
   PORT (
      i  : IN     std_logic_vector ; -- LVDS positive input
      ib : IN     std_logic_vector ; -- LVDS negative intput (input bar)
      o  : OUT    std_logic_vector   -- output
   );
   END COMPONENT;
   COMPONENT lvds_obuf
   PORT (
      i  : IN     std_logic ;
      o  : OUT    std_logic ;
      ob : OUT    std_logic 
   );
   END COMPONENT;

   -- Optional embedded configurations
   -- pragma synthesis_off
   FOR ALL : adc2_interface USE ENTITY adc_eval_lib.adc2_interface;
   FOR ALL : adc_interface USE ENTITY adc_eval_lib.adc_interface;
   FOR ALL : controller USE ENTITY adc_eval_lib.controller;
   FOR ALL : dac_interface USE ENTITY adc_eval_lib.dac_interface;
   FOR ALL : data_buffer USE ENTITY adc_eval_lib.data_buffer;
   FOR ALL : lvds_ibuf USE ENTITY harvard_std.lvds_ibuf;
   FOR ALL : lvds_ibufg USE ENTITY harvard_std.lvds_ibufg;
   FOR ALL : lvds_ibufn USE ENTITY harvard_std.lvds_ibufn;
   FOR ALL : lvds_obuf USE ENTITY harvard_std.lvds_obuf;
   FOR ALL : usb_interface USE ENTITY adc_eval_lib.usb_interface;
   -- pragma synthesis_on


BEGIN
   -- Architecture concurrent statements
   -- HDL Embedded Text Block 1 eb1
   -- eb1 1
    DAQ1_CLKP <= DAQ1_CLK;
    DAQ1_CLKN <= not DAQ1_CLK;


   -- Instance port mappings.
   I3 : adc2_interface
      PORT MAP (
         CHAN_EN      => ADC_CHAN_EN,
         CHIP_EN      => ADC2_CHIP_EN,
         DA           => o4,
         DB           => o5,
         DC           => o6,
         DCO          => o9,
         DD           => o7,
         FCO          => o8,
         GLOBAL_RESET => RESET,
         GLOBAL_RUN   => RUN,
         LOCAL_CLK    => CLK,
         CLK          => ADC2_CLK,
         DATA_OUT     => ADC_DATA_OUT,
         DATA_STRB    => ADC_DATA_STRB,
         PWDN         => ADC2_PWDN
      );
   I2 : adc_interface
      PORT MAP (
         CHIP_EN      => ADC_CHIP_EN,
         CH_EN        => ADC_CHAN_EN,
         CLK_INA      => o3,
         CLK_INB      => o,
         D_IN_A       => o1,
         D_IN_B       => o2,
         ERROR        => ADC_ERROR,
         GLOBAL_RESET => RESET,
         GLOBAL_RUN   => RUN,
         LOCAL_CLK    => CLK,
         CLK_OUT      => i,
         DATA_OUT     => ADC_DATA_OUT,
         DATA_STRB    => ADC_DATA_STRB,
         ENABLE       => ADC_ENABLE,
         MODE         => ADC_MODE
      );
   I5 : controller
      PORT MAP (
         CLK              => CLK,
         COMMAND_QUEUED   => COMMAND_QUEUED,
         USB_OUT_ENPT_RDY => USB_OUT_ENPT_RDY,
         ADC_CHIP_EN      => ADC_CHIP_EN,
         FIFO_OUT_EN      => FIFO_OUT_EN,
         FIFO_RESET       => FIFO_RESET,
         OUT_STRB         => OUT_STRB,
         RESET            => RESET,
         RUN              => RUN,
         USB_DATA         => USB_DATA,
         USB_RESET        => USB_RESET
      );
   I4 : dac_interface
      PORT MAP (
         LOCAL_CLK    => CLK,
         CLK          => DAQ1_CLK,
         P1B          => DAQ1_P1B,
         RESET        => DAQ1_RESET,
         DATA_IN      => DAC_DATA_IN,
         GLOBAL_RUN   => RUN,
         GLOBAL_RESET => RESET,
         P2B          => DAQ1_P2B,
         PLL_LOCK     => DAQ1_PLL_LOCK
      );
   I8 : data_buffer
      PORT MAP (
         ADC_DATA_OUT  => ADC_DATA_OUT,
         ADC_DATA_STRB => ADC_DATA_STRB,
         FIFO_OUT_EN   => FIFO_OUT_EN,
         FIFO_RESET    => FIFO_RESET,
         IN_CLK        => IN_CLK,
         OUT_CLK       => OUT_CLK,
         OUT_STRB      => OUT_STRB,
         USB_DATA      => USB_DATA
      );
   I16 : usb_interface
      PORT MAP (
         FLAGA          => USB_FLAGA,
         FLAGB          => USB_FLAGB,
         FLAGC          => USB_FLAGC,
         FLAGD          => USB_FLAGD,
         IFCLK          => USB_IFCLK_internal,
         LOCAL_CLK      => CLK,
         LOCAL_RESET    => USB_RESET,
         OUT_STRB       => OUT_STRB,
         PA0            => USB_PA0,
         PA1            => USB_PA1,
         CLK            => USB_CLK,
         COMMAND_QUEUED => COMMAND_QUEUED,
         FIFOADR        => USB_FIFOADR,
         OUT_ENPT_RDY   => USB_OUT_ENPT_RDY,
         PKTEND_B       => USB_PKTEND_B,
         RESET_B        => USB_RESET_B,
         SLOE           => USB_SLOE,
         SLRD           => USB_SLRD,
         SLWR           => USB_SLWR,
         WAKEUP_B       => USB_WAKEUP_B,
         WU2            => USB_WU2,
         DATA           => USB_DATA,
         FD             => USB_FD,
         SCL            => USB_SCL,
         SDA            => USB_SDA
      );
   I0 : lvds_ibuf
      PORT MAP (
         O  => o4,
         I  => ADC2_DAP,
         IB => ADC2_DAN
      );
   I1 : lvds_ibuf
      PORT MAP (
         O  => o5,
         I  => ADC2_DBP,
         IB => ADC2_DBN
      );
   I7 : lvds_ibuf
      PORT MAP (
         O  => o6,
         I  => ADC2_DCP,
         IB => ADC2_DCN
      );
   I15 : lvds_ibuf
      PORT MAP (
         O  => o7,
         I  => ADC2_DDP,
         IB => ADC2_DDN
      );
   I10 : lvds_ibufg
      PORT MAP (
         i  => ADC_CLKOUTBP,
         ib => ADC_CLKOUTBN,
         o  => o
      );
   I11 : lvds_ibufg
      PORT MAP (
         i  => ADC_CLKOUTAP,
         ib => ADC_CLKOUTAN,
         o  => o3
      );
   I13 : lvds_ibufg
      PORT MAP (
         i  => ADC2_FCOP,
         ib => ADC2_FCON,
         o  => o8
      );
   I14 : lvds_ibufg
      PORT MAP (
         i  => ADC2_DCOP,
         ib => ADC2_DCON,
         o  => o9
      );
   I6 : lvds_ibufn
      PORT MAP (
         i  => ADC_DOUTAP,
         ib => ADC_DOUTAN,
         o  => o1
      );
   I9 : lvds_ibufn
      PORT MAP (
         i  => ADC_DOUTBP,
         ib => ADC_DOUTBN,
         o  => o2
      );
   I12 : lvds_obuf
      PORT MAP (
         i  => i,
         o  => ADC_CLKINP,
         ob => ADC_CLKINN
      );

   -- Implicit buffered output assignments
   USB_IFCLK <= USB_IFCLK_internal;

END struct;
