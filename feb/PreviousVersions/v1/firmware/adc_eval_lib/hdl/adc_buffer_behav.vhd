--
-- VHDL Architecture adc_eval_lib.adc_buffer.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC4)
--          at - 10:03:02 03/ 1/2006
--
-- using Mentor Graphics HDL Designer(TM) 2005.2 (Build 37)
--
architecture behav of adc_buffer is

  type ram_type is array (16383 downto 0)
    of std_logic_vector (11 downto 0);
  signal RAM_EVEN   : ram_type;
  signal RAM_ODD    : ram_type;
  signal WRITE_ADDR : unsigned (13 downto 0);
  
begin

  process (WRITE_CLK)
  begin
    if WRITE_CLK'event and WRITE_CLK = '1' then
      if WE then
        RAM_EVEN (to_integer(WRITE_ADDR)) <= std_logic_vector(WRITE_DATA);
      end if;
    end if;
  end process;

  process (READ_CLK)
  begin
    if READ_CLK'event and READ_CLK = '1' then
      if EVEN_OE then
        READ_DATA <= unsigned("0000" & RAM_EVEN(to_integer(READ_ADDR)));
      else
        READ_DATA <= "ZZZZZZZZZZZZZZZZ";
      end if;
    end if;
  end process;

  process (WRITE_CLK)
  begin
    if WRITE_CLK'event and WRITE_CLK = '0' then
      if WE then
        RAM_ODD (to_integer(WRITE_ADDR)) <= std_logic_vector(WRITE_DATA);
      end if;
    end if;
  end process;

  process (READ_CLK)
  begin
    if READ_CLK'event and READ_CLK = '1' then
      if ODD_OE then
        READ_DATA <= unsigned("0000" & RAM_ODD(to_integer(READ_ADDR)));
      else
        READ_DATA <= "ZZZZZZZZZZZZZZZZ";
      end if;
    end if;
  end process;


  write_ptr_gen : process (WRITE_CLK) is
  begin
    if WRITE_CLK'event and WRITE_CLK = '1' then
      if RESET then
        WRITE_ADDR <= "00000000000000";
      elsif WE and not (write_addr = "11111111111111") then
        WRITE_ADDR <= WRITE_ADDR + 1;
      end if;
    end if;
  end process write_ptr_gen;

end architecture behav;

