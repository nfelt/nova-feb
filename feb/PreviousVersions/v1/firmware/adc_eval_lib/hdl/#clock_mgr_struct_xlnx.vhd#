--
-- VHDL Architecture adc_eval_lib.clock_mgr.struct_xlnx
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC4)
--          at - 10:31:21 03/15/2006
--
-- using Mentor Graphics HDL Designer(TM) 2005.2 (Build 37)
--
architecture struct_xlnx of clock_mgr is

  signal RST_int     : std_logic;
  signal DCM2_CLKIN  : std_logic;
  signal DCM2_CLK0   : std_logic;
  signal DCM1_CLKFX  : std_logic;
  signal DCM1_CLK2X  : std_logic;
  signal DCM1_CLK0   : std_logic;
  signal DCM1_LOCKED : std_logic;
  signal DCM2_RST    : std_logic;
  signal DCM2_CLKFB  : std_logic;
  signal DCM2_CLK270  : std_logic;
  signal DCM1_CLKFB  : std_logic;

  component BUFGMUX
    port (O  : out std_ulogic;
          I0 : in  std_ulogic;
          I1 : in  std_ulogic;
          S  : in  std_ulogic);
  end component;

  component DCM

    generic (CLKDV_DIVIDE          : real             := 2.0;  --  Divide by: 1.5;2.0;2.5;3.0;3.5;4.0;4.5;5.0;5.5;6.0;6.5
             --     7.0;7.5;8.0;9.0;10.0;11.0;12.0;13.0;14.0;15.0 or 16.0
             CLKFX_DIVIDE          : integer          := 4;  --  Can be any interger from 1 to 32
             CLKFX_MULTIPLY        : integer          := 1;  --  Can be any integer from 1 to
--  32 was 22/6
             CLKIN_DIVIDE_BY_2     : boolean          := false;  --  TRUE/FALSE to enable CLKIN divide by two feature
             CLKIN_PERIOD          : real             := 21.0;  --  Specify period of input clock
             CLKOUT_PHASE_SHIFT    : string           := "NONE";  --  Specify phase shift of NONE; FIXED or VARIABLE
             CLK_FEEDBACK          : string           := "1X";  --  Specify clock feedback of NONE; 1X or 2X
             DESKEW_ADJUST         : string           := "SYSTEM_SYNCHRONOUS";  --  SOURCE_SYNCHRONOUS; SYSTEM_SYNCHRONOUS or
                                        --     an integer from 0 to 15
             DFS_FREQUENCY_MODE    : string           := "HIGH";  --  HIGH or LOW frequency mode for frequency synthesis
             DLL_FREQUENCY_MODE    : string           := "LOW";  --  HIGH or LOW frequency mode for DLL
             DUTY_CYCLE_CORRECTION : boolean          := true;  --  Duty cycle correction; TRUE or FALSE
             FACTORY_JF            : std_logic_vector := X"C080";  --  FACTORY JF Values
             PHASE_SHIFT           : integer          := 0;  --  Amount of fixed phase shift from -255 to 255
             STARTUP_WAIT          : boolean          := false  --  Delay configuration DONE until DCM LOCK; TRUE/FALSE
             ); 
    port (CLK0     : out std_ulogic;
          CLK180   : out std_ulogic;
          CLK270   : out std_ulogic;
          CLK2X    : out std_ulogic;
          CLK2X180 : out std_ulogic;
          CLK90    : out std_ulogic;
          CLKDV    : out std_ulogic;
          CLKFX    : out std_ulogic;
          CLKFX180 : out std_ulogic;
          LOCKED   : out std_ulogic;
          PSDONE   : out std_ulogic;
          STATUS   : out std_logic_vector (7 downto 0);
          CLKFB    : in  std_ulogic;
          CLKIN    : in  std_ulogic;
          PSCLK    : in  std_ulogic;
          PSEN     : in  std_ulogic;
          PSINCDEC : in  std_ulogic;
          RST      : in  std_ulogic
          );
  end component;
  
begin

  DCM_inst1 : DCM
    generic map (
      CLKDV_DIVIDE          => 2.0,  --  Divide by: 1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0,6.5
      --     7.0,7.5,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0 or 16.0
      CLKFX_DIVIDE          => 18,       --  Can be any interger from 1 to 32
      CLKFX_MULTIPLY        => 16,      --  Can be any integer from 1 to 32
      CLKIN_DIVIDE_BY_2     => false,  --  TRUE/FALSE to enable CLKIN divide by two feature
      CLKIN_PERIOD          => 21.0,    --  Specify period of input clock
      CLKOUT_PHASE_SHIFT    => "NONE",  --  Specify phase shift of NONE, FIXED or VARIABLE
      CLK_FEEDBACK          => "1X",  --  Specify clock feedback of NONE, 1X or 2X
      DESKEW_ADJUST         => "SYSTEM_SYNCHRONOUS",  --  SOURCE_SYNCHRONOUS, SYSTEM_SYNCHRONOUS or
                                        --     an integer from 0 to 15
      DFS_FREQUENCY_MODE    => "LOW",  --  HIGH or LOW frequency mode for frequency synthesis
      DLL_FREQUENCY_MODE    => "LOW",   --  HIGH or LOW frequency mode for DLL
      DUTY_CYCLE_CORRECTION => true,  --  Duty cycle correction, TRUE or FALSE
      FACTORY_JF            => X"C080",               --  FACTORY JF Values
      PHASE_SHIFT           => 0,  --  Amount of fixed phase shift from -255 to 255
      STARTUP_WAIT          => false)  --  Delay configuration DONE until DCM LOCK, TRUE/FALSE
    port map (
      CLK0     => DCM1_CLK0,            -- 0 degree DCM CLK ouptput
      CLK180   => open,                 -- 180 degree DCM CLK output
      CLK270   => open,                 -- 270 degree DCM CLK output
      CLK2X    => DCM1_CLK2X,           -- 2X DCM CLK output
      CLK2X180 => open,                 -- 2X, 180 degree DCM CLK out
      CLK90    => open,                 -- 90 degree DCM CLK output
      CLKDV    => open,                 -- Divided DCM CLK out (CLKDV_DIVIDE)
      CLKFX    => DCM1_CLKFX,           -- DCM CLK synthesis out (M/D)
      CLKFX180 => open,                 -- 180 degree CLK synthesis out
      LOCKED   => DCM1_LOCKED,          -- DCM LOCK status output
      PSDONE   => open,                 -- Dynamic phase adjust done output
      STATUS   => open,                 -- 8-bit DCM status bits output
      CLKFB    => DCM1_CLKFB,           -- DCM clock feedback
      CLKIN    => CLKIN,           -- Clock input (from IBUFG, BUFG or DCM)
      PSCLK    => CLKIN,                -- Dynamic phase adjust clock input
      PSEN     => '0',                  -- Dynamic phase adjust enable input
      PSINCDEC => '0',             -- Dynamic phase adjust increment/decrement
      RST      => RST_int               -- DCM asynchronous reset input
      );

  BUFMUX_DCM1FB1 : BUFGMUX
    port map (O  => DCM1_CLKFB,
              I0 => DCM1_CLK0,
              I1 => '0',
              S  => '0');

  BUFMUX_DCM2FB1 : BUFGMUX
    port map (O  => DCM2_CLKFB,
              I0 => DCM2_CLK0,
              I1 => '0',
              S  => '0');

  BUFMUX_DCM1CLKOUT : BUFGMUX
    port map (O  => DCM2_CLKIN,
              I0 => DCM1_CLKFX,
              I1 => '0',
              S  => '0');

  BUFMUX_DAC_CLK : BUFGMUX
    port map (O  => DAC_CLK,
              I0 => DCM2_CLK0,
              I1 => '0',
              S  => DISABLE_DAC_CLK);

  BUFMUX_ADC_CLK : BUFGMUX
    port map (O  => ADC_CLK,
              I0 => DCM2_CLK0,
              I1 => '0',
              S  => DISABLE_ADC_CLK);



  DCM_inst2 : DCM
    generic map (
      CLKDV_DIVIDE          => 2.0,  --  Divide by: 1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0,6.5
      --     7.0,7.5,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0 or 16.0
      CLKFX_DIVIDE          => 2,       --  Can be any interger from 1 to 32
      CLKFX_MULTIPLY        => 2,       --  Can be any integer from 1 to 32
      CLKIN_DIVIDE_BY_2     => true,  --  TRUE/FALSE to enable CLKIN divide by two feature
      CLKIN_PERIOD          => 21.0,    --  Specify period of input clock
      CLKOUT_PHASE_SHIFT    => "NONE",  --  Specify phase shift of NONE, FIXED or VARIABLE
      CLK_FEEDBACK          => "1X",  --  Specify clock feedback of NONE, 1X or 2X
      DESKEW_ADJUST         => "SYSTEM_SYNCHRONOUS",  --  SOURCE_SYNCHRONOUS, SYSTEM_SYNCHRONOUS or
                                        --     an integer from 0 to 15
      DFS_FREQUENCY_MODE    => "LOW",  --  HIGH or LOW frequency mode for frequency synthesis
      DLL_FREQUENCY_MODE    => "LOW",   --  HIGH or LOW frequency mode for DLL
      DUTY_CYCLE_CORRECTION => true,  --  Duty cycle correction, TRUE or FALSE
      FACTORY_JF            => X"C080",               --  FACTORY JF Values
      PHASE_SHIFT           => 0,  --  Amount of fixed phase shift from -255 to 255
      STARTUP_WAIT          => false)  --  Delay configuration DONE until DCM LOCK, TRUE/FALSE
    port map (
      CLK0     => DCM2_CLK0,            -- 0 degree DCM CLK ouptput
      CLK180   => open,                 -- 180 degree DCM CLK output
      CLK270   => DCM2_CLK270,                 -- 270 degree DCM CLK output
      CLK2X    => open,                 -- 2X DCM CLK output
      CLK2X180 => open,                 -- 2X, 180 degree DCM CLK out
      CLK90    => DAC_DATA_CLK,         -- 90 degree DCM CLK output
      CLKDV    => open,                 -- Divided DCM CLK out (CLKDV_DIVIDE)
      CLKFX    => open,                 -- DCM CLK synthesis out (M/D)
      CLKFX180 => open,                 -- 180 degree CLK synthesis out
      LOCKED   => LOCKED ,              -- DCM LOCK status output
      PSDONE   => open,                 -- Dynamic phase adjust done output
      STATUS   => open,                 -- 8-bit DCM status bits output
      CLKFB    => DCM2_CLKFB,           -- DCM clock feedback
      CLKIN    => DCM2_CLKIN,      -- Clock input (from IBUFG, BUFG or DCM)
      PSCLK    => DCM2_CLKIN,           -- Dynamic phase adjust clock input
      PSEN     => '0',                  -- Dynamic phase adjust enable input
      PSINCDEC => '0',             -- Dynamic phase adjust increment/decrement
      RST      => DCM2_RST              -- DCM asynchronous reset input
      );

  DCM2_RST <= not DCM1_LOCKED;
  CLK0     <= DCM2_CLK0;
  CONV : process (RST) is
  begin
    if RST then
      RST_int <= '1';
    else
      RST_int <= '0';
    end if;
  end process CONV;
  
end architecture struct_xlnx;
