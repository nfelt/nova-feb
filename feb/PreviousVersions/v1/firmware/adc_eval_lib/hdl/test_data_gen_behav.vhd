--
-- VHDL Architecture adc_eval_lib.test_data_gen.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC4)
--          at - 19:53:25 02/22/2006
--
-- using Mentor Graphics HDL Designer(TM) 2005.2 (Build 37)
--
architecture behav of test_data_gen is
begin

  type ram_type is array (31 downto 0)
    of std_logic_vector (15 downto 0);
  signal RAM          : ram_type;
  signal MEM_DATA     : unsigned (15 downto 0);
  signal PATTERN_ADDR : unsigned (4 downto 0);
  signal READ_ADDR    : unsigned (4 downto 0);
  signal DONE         : boolean;

  begin

    process (clk)
    begin
      if clk'event and clk = '1' then
        if USB_WE then
          RAM (to_integer(USB_WRITE_ADDR)) <= std_logic_vector(USB_WRITE_DATA);
        end if;
        MEM_DATA <= unsigned(RAM(to_integer(READ_ADDR)));
      end if;
    end process;

    pattern_gen : process (clk) is
    begin
      if CLK'event and CLK = '1' then
        if RESET then
          PATTERN_ADDR <= 0;
          DONE         <= false;
        elsif RUN and not DONE then
          PATTERN_ADDR = PATTERN_ADDR +1;
          DONE = not LOOP_TEST and PATTERN_ADDR = 31;
        end if;
      end if;
    end process pattern_gen;

    select_read_addr : process (PATTERN_ADDR, READ_ADDR) is
    begin
      if RUN then
        READ_ADDR <= PATTERN_ADDR;
        DAC_DATA  <= MEM_DATA;
      else
        READ_ADDR <= USB_READ_ADDR;
        DAC_DATA  <= x"0000";
      end if;

      if not RUN and USB_OE then
        USB_DATA <= MEM_DATA;
      else
        USB_DATA <= "ZZZZZZZZZZZZZZZZ";
      end if;
    end process select_read_addr;
    
  end architecture behav;

