--
-- VHDL Architecture adc_eval_lib.dac_interface.dummy
--
-- Created:
--          by - nate.UNKNOWN (HEPLPC4)
--          at - 11:15:18 01/12/2006
--
-- using Mentor Graphics HDL Designer(TM) 2005.1b (Build 6)
--
architecture dummy of dac_interface is
begin
  CLK   <= LOCAL_CLK;
  P1B   <= DATA_IN;
  P2B   <= DATA_IN;
  RESET <= LOCAL_RESET;
end architecture dummy;

