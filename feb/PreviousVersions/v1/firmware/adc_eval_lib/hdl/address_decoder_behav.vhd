--
-- VHDL Architecture adc_eval_lib.address_decoder.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC4)
--          at - 17:15:56 02/17/2006
--
-- using Mentor Graphics HDL Designer(TM) 2005.2 (Build 37)
--
architecture behav of address_decoder is
begin
  sel_read : process(READ_ADDR)
  begin
    SEL_BOARD_MODE_OE   <= READ_ADDR = x"0001";
    SEL_STATUS_OE       <= READ_ADDR = x"0002";
    SEL_TEST_MODE_OE    <= READ_ADDR = x"0006";
    SEL_DAC_CONSTANT_OE <= READ_ADDR = x"0007";
    SEL_TEST_PATTERN_OE <= (READ_ADDR >= x"2000" and READ_ADDR <= x"2200");
    SEL_ADC_BUFFER0_OE  <= (READ_ADDR >= x"8000" and READ_ADDR <= x"bFFF");
    SEL_ADC_BUFFER1_OE  <= (READ_ADDR >= x"c000" and READ_ADDR <= x"fFFF");
  end process sel_read;

  sel_write : process(WRITE_ADDR, WE)
  begin
    SEL_BOARD_MODE_WE   <= WRITE_ADDR = x"0001" and WE;
    SEL_STATUS_WE       <= WRITE_ADDR = x"0002" and WE;
    SEL_TEST_MODE_WE    <= WRITE_ADDR = x"0006" and WE;
    SEL_DAC_CONSTANT_WE <= WRITE_ADDR = x"0007" and WE;
    SEL_TEST_PATTERN_WE <= (WRITE_ADDR >= x"2000" and WRITE_ADDR <= x"2200" and WE);
    SEL_ADC_BUFFER0_WE  <= (WRITE_ADDR >= x"8000" and WRITE_ADDR <= x"bFFF" and WE);
    SEL_ADC_BUFFER1_WE  <= (WRITE_ADDR >= x"c000" and WRITE_ADDR <= x"fFFF" and WE);
  end process sel_write;
end behav;

