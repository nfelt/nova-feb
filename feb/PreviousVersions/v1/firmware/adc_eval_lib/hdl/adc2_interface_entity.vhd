--
-- VHDL Entity adc_eval_lib.adc2_interface.arch_name
--
-- Created:
--          by - nate.UNKNOWN (HEPLPC4)
--          at - 10:54:25 01/11/2006
--
-- using Mentor Graphics HDL Designer(TM) 2005.1b (Build 6)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;

LIBRARY unisim;
USE unisim.all;

ENTITY adc2_interface IS
   PORT( 
      CHAN_EN      : IN     std_logic_vector ( 3 DOWNTO 0 );
      CHIP_EN      : IN     boolean;
      DA           : IN     std_logic;
      DB           : IN     std_logic;
      DC           : IN     std_logic;
      DCO          : IN     std_logic;
      DD           : IN     std_logic;
      FCO          : IN     std_logic;
      GLOBAL_RESET : IN     boolean;
      GLOBAL_RUN   : IN     boolean;
      LOCAL_CLK    : IN     std_logic;
      DATA_OUT     : OUT    unsigned (15 DOWNTO 0);
      DATA_STRB    : OUT    std_logic
   );

-- Declarations

END adc2_interface ;

