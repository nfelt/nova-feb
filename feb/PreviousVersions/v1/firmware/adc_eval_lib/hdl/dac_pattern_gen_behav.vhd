--
-- VHDL Architecture adc_eval_lib.dac_pattern_gen.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC4)
--          at - 11:27:15 02/23/2006
--
-- using Mentor Graphics HDL Designer(TM) 2005.2 (Build 37)
--
architecture behav of dac_pattern_gen is
  
  signal RAMP                 : unsigned (13 downto 0);
  signal DAC_PATTERN_DATA_DEL : unsigned (13 downto 0);
  signal SEL_ODD              : boolean;
  
begin

  countup : process (clk) is
  begin

    if clk'event and clk = '1' then
      RAMP    <= RAMP;
      SEL_ODD <= SEL_ODD;
      if (RESET or RAMP = DAC_MAX) then
        SEL_ODD <= false;
        RAMP    <= DAC_MIN;
      elsif RUN then
        SEL_ODD <= not SEL_ODD;
        RAMP    <= RAMP + 1;
      end if;
    end if;
  end process countup;

  sel_constant_chan : process (SEL_ODD, RAMP) is
  begin
    if SEL_ODD or RAMP_ONLY then
      DAC_PATTERN_DATA <= RAMP;
    else
      DAC_PATTERN_DATA <= DAC_MIN;
    end if;
  end process sel_constant_chan;


  
end architecture behav;

