--
-- VHDL Architecture adc_eval_lib.dac_pattern_gen.fsm_behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC4)
--          at - 10:38:08 03/10/2006
--
-- using Mentor Graphics HDL Designer(TM) 2005.2 (Build 37)
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

architecture fsm_behav of dac_pattern_gen is

  -- Architecture Declarations
  signal DONE                 : boolean;
  signal RAMP                 : unsigned(13 downto 0);
  signal DAC_PATTERN_DATA_int : unsigned(13 downto 0);

  subtype STATE_TYPE is
    std_logic_vector(2 downto 0);

  attribute state_vector              : string;
  attribute state_vector of fsm_behav : architecture is "CURRENT_STATE";

  constant IDLE : STATE_TYPE := "000";
  constant CH0  : STATE_TYPE := "001";
  constant CH1  : STATE_TYPE := "010";
  constant CH2  : STATE_TYPE := "011";
  constant CH3  : STATE_TYPE := "100";

  signal CURRENT_STATE : STATE_TYPE;
  signal NEXT_STATE    : STATE_TYPE;
  
begin
  CLOCKED_PROC : process (CLK, RESET)
  begin
    if (RESET = true) then
      CURRENT_STATE <= IDLE;
      RAMP          <= DAC_MIN;
      DONE          <= false;
    elsif (CLK'event and CLK = '1') then
      CURRENT_STATE <= NEXT_STATE;

      if USE_DAC_CONSTANT then
        DAC_PATTERN_DATA <= DAC_CONSTANT;
      else
        DAC_PATTERN_DATA <= DAC_PATTERN_DATA_int;
      end if;

      if (RAMP > DAC_MAX) then
        RAMP <= DAC_MIN;
        DONE <= not CONTINUOUS_DAC;
      end if;

      case CURRENT_STATE is
        when CH0 =>
          RAMP <= RAMP + 1;
        when CH1 =>
          RAMP <= RAMP + 1;
        when CH2 =>
          RAMP <= RAMP + 1;
        when CH3 =>
          RAMP <= RAMP + 1;
        when others =>
          null;
      end case;
      
    end if;
  end process CLOCKED_PROC;

  NEXTSTATE_PROC : process (DONE, RUN, CURRENT_STATE)
  begin
    RUNNING_TEST         <= false;
    DAC_PATTERN_DATA_int <= DAC_MIN;

    case CURRENT_STATE is
      
      when IDLE =>
        if (RUN and not DONE) then
          next_state <= CH0;
        else
          next_state <= IDLE;
        end if;
        
      when CH0 =>
        RUNNING_TEST         <= true;
        DAC_PATTERN_DATA_int <= ramp;
        NEXT_STATE           <= CH1;
        
      when CH1 =>
        RUNNING_TEST         <= true;
        DAC_PATTERN_DATA_int <= ramp;
        NEXT_STATE           <= CH2;
        
      when CH2 =>
        RUNNING_TEST         <= true;
        DAC_PATTERN_DATA_int <= ramp;
        NEXT_STATE           <= CH3;
        
      when CH3 =>
        RUNNING_TEST         <= true;
        DAC_PATTERN_DATA_int <= ramp;
        if (DONE) then
          NEXT_STATE <= IDLE;
        else
          NEXT_STATE <= CH0;
        end if;
        
      when others =>
        NEXT_STATE <= IDLE;
    end case;
  end process NEXTSTATE_PROC;
end architecture fsm_behav;
