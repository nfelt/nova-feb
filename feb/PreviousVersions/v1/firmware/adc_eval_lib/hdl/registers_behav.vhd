--
-- VHDL Architecture adc_eval_lib.registers.behav
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC4)
--          at - 18:44:11 02/17/2006
--
-- using Mentor Graphics HDL Designer(TM) 2005.2 (Build 37)
--
architecture behav of registers is
  
  signal BOARD_MODE : unsigned(15 downto 0);
  signal DAC_CONSTANT_int : unsigned(13 downto 0);
  signal TEST_MODE  : unsigned(15 downto 0);
  signal RESET_int  : boolean;

begin
  reg_io : process (IO_CLK) is
  begin
    if IO_CLK'event and IO_CLK = '1' then
      READ_DATA <= "ZZZZZZZZZZZZZZZZ";

      if SEL_BOARD_MODE_OE then
        READ_DATA <= BOARD_MODE;
      end if;
      if SEL_BOARD_MODE_WE then
        BOARD_MODE <= WRITE_DATA;
      end if;

      if SEL_STATUS_OE then
        READ_DATA <= STATUS;
      end if;

      if SEL_TEST_MODE_OE then
        READ_DATA <= TEST_MODE;
      end if;
      if SEL_TEST_MODE_WE then
        TEST_MODE <= WRITE_DATA;
      end if;

      if SEL_DAC_CONSTANT_OE then
        READ_DATA <= "00" & DAC_CONSTANT_int;
      end if;
      if SEL_DAC_CONSTANT_WE then
        DAC_CONSTANT_int <= WRITE_DATA(13 downto 0);
      end if;
      
    end if;
  end process reg_io;
  mode_out : process (MODE_CLK) is
  begin
    if MODE_CLK'event and MODE_CLK = '1' then
      RUN              <= BOARD_MODE(1) = '1';
      ADC_MODE_SEL     <= BOARD_MODE(6 downto 4);
      DAC_LOCAL_RESET  <= TEST_MODE(6) = '1';
      USE_DAC_CONSTANT <= not (DAC_CONSTANT_int = "00000000000000");
      DISABLE_DAC_CLK  <= TEST_MODE(4);
      DISABLE_ADC_CLK  <= TEST_MODE(3);
      CONTINUOUS_DAC   <= TEST_MODE(2) = '1';
      RAMP_ONLY        <= TEST_MODE(1) = '1';
      DAC_TO_USB       <= TEST_MODE(0) = '1';
    end if;
  end process mode_out;
--requird for DCM to be reset without CLK signal
  RESET <= BOARD_MODE(0) = '1';
DAC_CONSTANT <= DAC_CONSTANT_int;
end architecture behav;

