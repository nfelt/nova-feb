--
-- VHDL Architecture usb_comm_lib.coun_tup.behav
--
-- Created:
--          by - nate.UNKNOWN (HEPLPC4)
--          at - 13:14:53 05/19/2005
--
-- using Mentor Graphics HDL Designer(TM) 2004.1 (Build 41)
--
library ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;


entity coun_tup is
  port(
    clk     : in  std_logic;
    count   : out unsigned (15 downto 0);
    TRIGGER : out boolean
    );

-- Declarations

end coun_tup;

--
architecture behav of coun_tup is

  signal count_int : unsigned(15 downto 0);
  
begin
  
  counter : process (clk) is
  begin
    if clk'event and clk = '1' then
      count_int <= count_int + 1;

	  TRIGGER <= (count_int = 0);

    end if;
  end process counter;
  count <= count_int;
end architecture behav;

