--
-- VHDL Architecture adc_eval_lib.adc_interface.dummy
--
-- Created:
--          by - nate.UNKNOWN (HEPLPC4)
--          at - 10:46:54 01/12/2006
--
-- using Mentor Graphics HDL Designer(TM) 2005.1b (Build 6)
--
architecture dummy of adc_interface is
begin

  CLK_OUT   <= LOCAL_CLK;
  DATA_OUT  <= unsigned (D_IN_A);
  DATA_STRB <= 'Z';
  DATA_CLK <= CLK_INA;
  ENABLE    <= "1111";
  MODE      <= MODE_SEL;
end architecture dummy;

