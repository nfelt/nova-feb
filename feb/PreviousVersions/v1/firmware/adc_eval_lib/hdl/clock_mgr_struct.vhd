--
-- VHDL Architecture adc_eval_lib.clock_mgr.struct
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC4)
--          at - 16:41:24 03/ 3/2006
--
-- using Mentor Graphics HDL Designer(TM) 2005.2 (Build 37)
--
architecture struct of clock_mgr is
  signal CLK0_INT : std_logic;
  signal RST_int  : std_logic;
  component DCM
    -- synthesis translate_off
    generic (CLK_FEEDBACK          : string  := "1X";
             CLKDV_DIVIDE          : real    := 2.0;  -- (1.5, 2.0, 2.5, 3.0, 4.0, 5.0, 8.0, 16.0)
             CLKFX_DIVIDE          : integer := 4;  -- (1 to 4096)
             CLKFX_MULTIPLY        : integer := 1;  -- (1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 5.0, 5.5,
             -- 6.0, 6.5, 7.0, 7.5, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0)
             CLKIN_DIVIDE_BY_2     : boolean := false;   -- (TRUE, FALSE)
             CLKOUT_PHASE_SHIFT    : string  := "NONE";
             DESKEW_ADJUST         : string  := "SYSTEM_SYNCHRONOUS";
             DFS_FREQUENCY_MODE    : string  := "LOW";
             DLL_FREQUENCY_MODE    : string  := "LOW";
             DSS_MODE              : string  := "NONE";
             DUTY_CYCLE_CORRECTION : boolean := true;    -- (TRUE, FALSE)
             PHASE_SHIFT           : real    := 0;
             STARTUP_WAIT          : boolean := false);  -- (TRUE, FALSE)
    -- synthesis translate_on
    port (CLK0 : out std_ulogic;
          CLK180   : out std_ulogic;
          CLK270   : out std_ulogic;
          CLK2X    : out std_ulogic;
          CLK2X180 : out std_ulogic;
          CLK90    : out std_ulogic;
          CLKDV    : out std_ulogic;
          CLKFX    : out std_ulogic;
          CLKFX180 : out std_ulogic;
          LOCKED   : out std_ulogic;
          PSDONE   : out std_ulogic;
          STATUS   : out std_logic_vector (7 downto 0);
          CLKFB    : in  std_ulogic;
          CLKIN    : in  std_ulogic;
          PSCLK    : in  std_ulogic;
          PSEN     : in  std_ulogic;
          PSINCDEC : in  std_ulogic;
          RST      : in  std_ulogic
          );
  end component;

  attribute CLK_FEEDBACK          : string;
  attribute CLKIN_PERIOD          : string;
  attribute CLKDV_DIVIDE          : real;
  attribute CLKFX_DIVIDE          : integer;
  attribute CLKFX_MULTIPLY        : integer;
  attribute CLKIN_DIVIDE_BY_2     : string;
  attribute CLKOUT_PHASE_SHIFT    : string;
  attribute DESKEW_ADJUST         : string;
  attribute DFS_FREQUENCY_MODE    : string;
  attribute DLL_FREQUENCY_MODE    : string;
  attribute DSS_MODE              : string;
  attribute DUTY_CYCLE_CORRECTION : boolean;
  attribute PHASE_SHIFT           : real;
  attribute STARTUP_WAIT          : boolean;

  attribute CLK_FEEDBACK of DCM_inst          : label is "1X";
  attribute CLKIN_PERIOD of DCM_inst          : label is "83";
  attribute CLKDV_DIVIDE of DCM_inst          : label is 2.0;
-- (1.5,2,2.5,3,4, 5, 8, 16) are valid for CLKDV_DIVIDE
  attribute CLKFX_DIVIDE of DCM_inst          : label is 3;
  attribute CLKFX_MULTIPLY of DCM_inst        : label is 11;
  attribute CLKIN_DIVIDE_BY_2 of DCM_inst     : label is "FALSE";
  attribute CLKOUT_PHASE_SHIFT of DCM_inst    : label is "NONE";
  attribute DESKEW_ADJUST of DCM_inst         : label is "SYSTEM_SYNCHRONOUS";
  attribute DFS_FREQUENCY_MODE of DCM_inst    : label is "LOW";
  attribute DLL_FREQUENCY_MODE of DCM_inst    : label is "LOW";
  attribute DSS_MODE of DCM_inst              : label is "NONE";
  attribute DUTY_CYCLE_CORRECTION of DCM_inst : label is true;
-- (TRUE, FALSE) are valid for DUTY_CYCLE_CORRECTION
  attribute PHASE_SHIFT of DCM_inst           : label is 0.0;
  attribute STARTUP_WAIT of DCM_inst          : label is false;  -- (TRUE,FALSE) 



  
begin
  DCM_inst : DCM
    -- synthesis translate_off
    generic map(CLK_FEEDBACK          => "string_value",
                CLKDV_DIVIDE          => string_value�,  -- (1.5,2,2.5,3,4,5,8,16)
                CLKFX_DIVIDE          => integer_value,
                CLKFX_MULTIPLY        => integer_value,
                CLKIN_DIVIDE_BY_2     => boolean_value,  -- (TRUE, FALSE)
                CLKOUT_PHASE_SHIFT    => "string_value",
                DESKEW_ADJUST         => "string_value",
                DFS_FREQUENCY_MODE    => "string_value",
                DLL_FREQUENCY_MODE    => "string_value",
                DSS_MODE              => "string_value",
                DUTY_CYCLE_CORRECTION => boolean_value,  -- (TRUE, FALSE)
                PHASE_SHIFT           => integer_value,
                STARTUP_WAIT          => boolean)        -- (TRUE, FALSE)
    -- synthesis translate_on
    port map (
      CLK0 => CLK0_INT,             -- 0 degree DCM CLK ouptput
      CLK180   => open,                 -- 180 degree DCM CLK output
      CLK270   => open,                 -- 270 degree DCM CLK output
      CLK2X    => open,                 -- 2X DCM CLK output
      CLK2X180 => open,                 -- 2X, 180 degree DCM CLK out
      CLK90    => CLK90,                 -- 90 degree DCM CLK output
      CLKDV    => CLKDV,                -- Divided DCM CLK out (CLKDV_DIVIDE)
      CLKFX    => CLKFX,                -- DCM CLK synthesis out (M/D)
      CLKFX180 => CLKFX180,             -- 180 degree CLK synthesis out
      LOCKED   => LOCKED,               -- DCM LOCK status output
      PSDONE   => open,                 -- Dynamic phase adjust done output
      STATUS   => open,                 -- 8-bit DCM status bits output
      CLKFB    => CLK0_INT,             -- DCM clock feedback
      CLKIN    => CLKIN,     -- Clock input (from IBUFG, BUFG or DCM)
      PSCLK    => CLKIN,                -- Dynamic phase adjust clock input
      PSEN     => '0',                  -- Dynamic phase adjust enable input
      PSINCDEC => '0',       -- Dynamic phase adjust increment/decrement
      RST      => RST_int               -- DCM asynchronous reset input
      );

  -- End of DCM_inst instantiation

  CONV : process (RST) is
  begin
    if RST then
      RST_int <= '1';
    else
      RST_int <= '0';
    end if;
  end process CONV;
  CLK0 <= CLK0_INT;


end architecture struct;

