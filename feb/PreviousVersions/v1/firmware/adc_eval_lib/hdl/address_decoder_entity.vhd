--
-- VHDL Entity adc_eval_lib.address_decoder.arch_name
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC4)
--          at - 17:15:13 02/17/2006
--
-- using Mentor Graphics HDL Designer(TM) 2005.2 (Build 37)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;

ENTITY address_decoder IS
   PORT( 
      READ_ADDR           : IN     unsigned (15 DOWNTO 0);
      WRITE_ADDR          : IN     unsigned (15 DOWNTO 0);
      SEL_BOARD_MODE_OE   : OUT    boolean;
      SEL_STATUS_OE       : OUT    boolean;
      SEL_TEST_MODE_OE    : OUT    boolean;
      SEL_TEST_PATTERN_OE : OUT    boolean;
      SEL_BOARD_MODE_WE   : OUT    boolean;
      SEL_STATUS_WE       : OUT    boolean;
      SEL_TEST_MODE_WE    : OUT    boolean;
      SEL_TEST_PATTERN_WE : OUT    boolean;
      WE                  : IN     boolean;
      SEL_ADC_BUFFER0_WE  : OUT    boolean;
      SEL_ADC_BUFFER1_WE  : OUT    boolean;
      SEL_ADC_BUFFER0_OE  : OUT    boolean;
      SEL_ADC_BUFFER1_OE  : OUT    boolean;
      SEL_DAC_CONSTANT_OE : OUT    boolean;
      SEL_DAC_CONSTANT_WE : OUT    boolean
   );

-- Declarations

END address_decoder ;
