--
-- VHDL Entity adc_eval_lib.dac_interface.arch_name
--
-- Created:
--          by - nate.UNKNOWN (HEPLPC4)
--          at - 11:14:49 01/12/2006
--
-- using Mentor Graphics HDL Designer(TM) 2005.1b (Build 6)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;

LIBRARY unisim;
USE unisim.all;

ENTITY dac_interface IS
   PORT( 
      LOCAL_CLK    : IN     std_logic;
      CLK          : OUT    std_logic;
      P1B          : OUT    unsigned (13 DOWNTO 0);
      RESET        : OUT    boolean;
      DATA_IN      : IN     unsigned ( 13 DOWNTO 0 );
      GLOBAL_RUN   : IN     boolean;
      GLOBAL_RESET : IN     boolean;
      P2B          : OUT    unsigned ( 13 DOWNTO 0 );
      PLL_LOCK     : IN     boolean;
      LOCAL_RESET  : IN     boolean
   );

-- Declarations

END dac_interface ;

