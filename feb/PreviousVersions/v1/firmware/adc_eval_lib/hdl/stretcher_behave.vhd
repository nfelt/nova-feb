--
-- VHDL Architecture adc_eval_lib.stretcher.behave
--
-- Created:
--          by - Nate.UNKNOWN (HEPLPC4)
--          at - 11:52:27 02/23/2006
--
-- using Mentor Graphics HDL Designer(TM) 2005.2 (Build 37)
--
architecture behave of stretcher is

  signal STRETCH_TIME  : unsigned(19 downto 0);
  signal STRETCHED_int : boolean;
  
begin

  STRETCHED_int <= not (STRETCH_TIME = x"FFFFF");
  STRETCHED     <= STRETCHED_int;

  measure_delay : process (clk) is
  begin
    if CLK'event and CLK = '1' then
      if SIG_IN then
        STRETCH_TIME <= x"00000";
      elsif STRETCHED_int then
        STRETCH_TIME <= STRETCH_TIME + 1;
      end if;
    end if;
  end process measure_delay;
  
end architecture behave;

