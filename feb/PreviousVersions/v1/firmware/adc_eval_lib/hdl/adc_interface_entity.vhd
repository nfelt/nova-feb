--
-- VHDL Entity adc_eval_lib.adc_interface.arch_name
--
-- Created:
--          by - nate.UNKNOWN (HEPLPC4)
--          at - 16:57:59 01/11/2006
--
-- using Mentor Graphics HDL Designer(TM) 2005.1b (Build 6)
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.NUMERIC_STD.all;

LIBRARY unisim;
USE unisim.all;

ENTITY adc_interface IS
   PORT( 
      CHIP_EN      : IN     boolean;
      CH_EN        : IN     std_logic_vector ( 3 DOWNTO 0 );
      CLK_INA      : IN     std_logic;
      CLK_INB      : IN     std_logic;
      D_IN_A       : IN     std_logic_vector (11 DOWNTO 0);
      D_IN_B       : IN     std_logic_vector (11 DOWNTO 0);
      ERROR        : IN     boolean;
      GLOBAL_RESET : IN     boolean;
      GLOBAL_RUN   : IN     boolean;
      LOCAL_CLK    : IN     std_logic;
      MODE_SEL     : IN     unsigned ( 2 DOWNTO 0 );
      CLK_OUT      : OUT    std_logic;
      DATA_CLK     : OUT    std_logic;
      DATA_OUT     : OUT    unsigned ( 11 DOWNTO 0 );
      DATA_STRB    : OUT    std_logic;
      ENABLE       : OUT    std_logic_vector ( 3 DOWNTO 0 );
      MODE         : OUT    unsigned (2 DOWNTO 0)
   );

-- Declarations

END adc_interface ;

