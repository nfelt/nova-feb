--
-- VHDL Architecture adc_eval_lib.adc2_interface.dummy
--
-- Created:
--          by - nate.UNKNOWN (HEPLPC4)
--          at - 10:27:13 01/12/2006
--
-- using Mentor Graphics HDL Designer(TM) 2005.1b (Build 6)
--
architecture dummy of adc2_interface is
begin
  DATA_OUT  <= "ZZZZZZZZZZZZZZZZ";
  DATA_STRB <= 'Z';
end architecture dummy;
