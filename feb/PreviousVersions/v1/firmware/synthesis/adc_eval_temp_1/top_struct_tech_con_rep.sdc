###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name iostandard -value "lvds_25" -instance -type string I12_u -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(0) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(0) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(0) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(1) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(1) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(1) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(2) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(2) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(2) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(3) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(3) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(3) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(4) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(4) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(4) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(5) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(5) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(5) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(6) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(6) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(6) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(7) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(7) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(7) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(8) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(8) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(8) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(9) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(9) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(9) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(10) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(10) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(10) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(11) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(11) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(11) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(12) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(12) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(12) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(13) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(13) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(13) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(14) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(14) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(14) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(15) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(15) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(15) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_SLWR_obuf -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_SLWR_obuf -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_SLWR_obuf -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_SLRD_obuf -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_SLRD_obuf -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_SLRD_obuf -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_SLOE_obuf -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_SLOE_obuf -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_SLOE_obuf -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_PKTEND_B_obuf -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_PKTEND_B_obuf -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_PKTEND_B_obuf -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FIFOADR_obuf(1) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FIFOADR_obuf(1) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FIFOADR_obuf(1) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FLAGC_ibuf -design gatelevel 
set_attribute -name PART -value "2VP4fg456-7" -type string /work/top/struct -design gatelevel 
set_attribute -name state_vector -value "current_state" /work/usb_translator_notri/fsm_unfold_1368 -design gatelevel 



##################
# Clocks
##################
create_clock { USB_IFCLK } -domain ClockDomain0 -name USB_IFCLK -period 10.000000 -waveform { 0.000000 5.000000 } -design gatelevel 

