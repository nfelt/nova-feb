
-- 
-- Definition of  OBUFDS
-- 
--      02/10/06 19:32:27
--      
--      Precision RTL Synthesis, 2005b.110
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity OBUFDS is
   generic (CAPACITANCE : string;
      IOSTANDARD : string) ;
   
   port (
      O : OUT std_logic ;
      OB : OUT std_logic ;
      I : IN std_logic) ;attribute RTLC_TECH_CELL: boolean;
   attribute RTLC_TECH_CELL of 
      OBUFDS : entity is true;
      end OBUFDS ;

architecture NETLIST of OBUFDS is
begin
end NETLIST ;

