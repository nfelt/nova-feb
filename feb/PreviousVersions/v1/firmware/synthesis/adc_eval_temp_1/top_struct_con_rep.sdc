###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name iostandard -value "lvds_25" -instance -type string I12/u -design rtl 

set_attribute -name state_vector -value "current_state" -type string /work/usb_translator_notri/fsm_unfold_1368_XRTL -design rtl 




##################
# Clocks
##################
create_clock { USB_IFCLK } -domain ClockDomain0 -name USB_IFCLK -period 10.000000 -waveform { 0.000000 5.000000 } -design rtl 

