##
##  Copyright (c) Mentor Graphics Corporation, 1996-2004, All Rights Reserved.
##             Portions copyright 1991-2004 Compuware Corporation
##                       UNPUBLISHED, LICENSED SOFTWARE.
##            CONFIDENTIAL AND PROPRIETARY INFORMATION WHICH IS THE
##          PROPERTY OF MENTOR GRAPHICS CORPORATION OR ITS LICENSORS
#
# File created on: 03/07/06 15:48:29
#

#
# Configuration specified in UCF
#
# CONFIG STEPPING = 0 ;
set proj_part [report_project -part]
if { ([string compare -nocase -length 2 $proj_part "2v"]  == 0) &&
     ([string compare -nocase -length 3 $proj_part "2vp"] != 0) } {
   set proj_stepping [report_project -speed_stepping]
   set proj_speed    [report_project -speed]
   if { [string length $proj_stepping] != 1 } {
      puts "#Warning: UCF CONFIG STEPPING differ from current setting!"
      puts "#Warning: Adjusting speed grade to ${proj_speed}"
      setup_design -speed ${proj_speed}
   }
}

#
# Find clocks
#
puts "Finding clock. . ."
set MgsClocks {}
set MgsClockSrcs {}
set MgsClocks [concat $MgsClocks {xmplr_USB_IFCLK}]
set MgsClockSrcs [concat $MgsClockSrcs [MGS_Design::get_drivers {USB_IFCLK}] ]
set MgsClocks [concat $MgsClocks {xmplr_ADC_CLKOUTAP}]
set MgsClockSrcs [concat $MgsClockSrcs [MGS_Design::get_drivers {ADC_CLKOUTAP}] ]

#
# Create Clocks
#
puts "Creating clocks. . ."
remove_constraints
#
set ucf {TIMESPEC "TS_clk_ClockDomain0" = PERIOD "xmplr_USB_IFCLK" 10 HIGH 50% ;}
create_clock -name {xmplr_USB_IFCLK} -domain {TS_clk_ClockDomain0} -period 10 -waveform { 0 5 } [MGS_Design::get_drivers {USB_IFCLK}] 
#
set ucf {TIMESPEC "TS_clk_ClockDomain1" = PERIOD "xmplr_ADC_CLKOUTAP" 10 HIGH 50% ;}
create_clock -name {xmplr_ADC_CLKOUTAP} -domain {TS_clk_ClockDomain1} -period 10 -waveform { 0 5 } [MGS_Design::get_drivers {ADC_CLKOUTAP}] 

#
# Boundary Constraints
#
puts "Creating boundary constraints. . ."

#
# Attributes
#
puts "Setting attributes. . ."
