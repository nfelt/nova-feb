###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name iostandard -value "lvds_25" -instance -type string I11/u -design rtl 

set_attribute -name iostandard -value "lvds_25" -instance -type string I6/lvds_ibuf_vec_11_u -design rtl 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6/lvds_ibuf_vec_10_u -design rtl 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6/lvds_ibuf_vec_9_u -design rtl 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6/lvds_ibuf_vec_8_u -design rtl 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6/lvds_ibuf_vec_7_u -design rtl 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6/lvds_ibuf_vec_6_u -design rtl 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6/lvds_ibuf_vec_5_u -design rtl 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6/lvds_ibuf_vec_4_u -design rtl 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6/lvds_ibuf_vec_3_u -design rtl 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6/lvds_ibuf_vec_2_u -design rtl 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6/lvds_ibuf_vec_1_u -design rtl 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6/lvds_ibuf_vec_0_u -design rtl 

set_attribute -name iostandard -value "lvds_25" -instance -type string I12/u -design rtl 

set_attribute -name CLK_FEEDBACK -value "1X" -instance -type string I5/U_6/DCM_inst -design rtl 
set_attribute -name CLKDV_DIVIDE -value "2.0" -instance -type default I5/U_6/DCM_inst -design rtl 
set_attribute -name CLKFX_DIVIDE -value "8" -instance -type default I5/U_6/DCM_inst -design rtl 
set_attribute -name CLKFX_MULTIPLY -value "2" -instance -type default I5/U_6/DCM_inst -design rtl 
set_attribute -name CLKIN_DIVIDE_BY_2 -value "FALSE" -instance -type string I5/U_6/DCM_inst -design rtl 
set_attribute -name CLKOUT_PHASE_SHIFT -value "NONE" -instance -type string I5/U_6/DCM_inst -design rtl 
set_attribute -name DESKEW_ADJUST -value "SYSTEM_SYNCHRONOUS" -instance -type string I5/U_6/DCM_inst -design rtl 
set_attribute -name DFS_FREQUENCY_MODE -value "LOW" -instance -type string I5/U_6/DCM_inst -design rtl 
set_attribute -name DLL_FREQUENCY_MODE -value "LOW" -instance -type string I5/U_6/DCM_inst -design rtl 
set_attribute -name DSS_MODE -value "NONE" -instance -type string I5/U_6/DCM_inst -design rtl 
set_attribute -name DUTY_CYCLE_CORRECTION -value "1" -instance -type default I5/U_6/DCM_inst -design rtl 
set_attribute -name PHASE_SHIFT -value "0.0" -instance -type default I5/U_6/DCM_inst -design rtl 
set_attribute -name STARTUP_WAIT -value "0" -instance -type default I5/U_6/DCM_inst -design rtl 

set_attribute -name ram_class_key -value "ram_new4" -instance I5/U_5/merged -design rtl 
set_attribute -name ram_processed -value "true" -instance I5/U_5/merged/merged -design rtl 


set_attribute -name state_vector -value "current_state" -type string /work/usb_translator_notri/fsm_unfold_1368 -design rtl 

##################
# Clocks
##################
create_clock { ADC_CLKOUTAP } -domain ClockDomain1 -name ADC_CLKOUTAP -period 10.000000 -waveform { 0.000000 5.000000 } -design rtl 
create_clock { USB_IFCLK } -domain ClockDomain0 -name USB_IFCLK -period 10.000000 -waveform { 0.000000 5.000000 } -design rtl 

