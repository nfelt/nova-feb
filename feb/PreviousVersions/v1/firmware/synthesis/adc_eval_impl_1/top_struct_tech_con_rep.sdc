###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name iostandard -value "lvds_25" -instance -type string I11_u -design gatelevel 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6_lvds_ibuf_vec_11_u -design gatelevel 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6_lvds_ibuf_vec_10_u -design gatelevel 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6_lvds_ibuf_vec_9_u -design gatelevel 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6_lvds_ibuf_vec_8_u -design gatelevel 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6_lvds_ibuf_vec_7_u -design gatelevel 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6_lvds_ibuf_vec_6_u -design gatelevel 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6_lvds_ibuf_vec_5_u -design gatelevel 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6_lvds_ibuf_vec_4_u -design gatelevel 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6_lvds_ibuf_vec_3_u -design gatelevel 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6_lvds_ibuf_vec_2_u -design gatelevel 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6_lvds_ibuf_vec_1_u -design gatelevel 
set_attribute -name iostandard -value "lvds_25" -instance -type string I6_lvds_ibuf_vec_0_u -design gatelevel 
set_attribute -name iostandard -value "lvds_25" -instance -type string I12_u -design gatelevel 
set_attribute -name CLKDV_DIVIDE -value "2.0" -instance -type DEFAULT I5_U_6_DCM_inst1 -design gatelevel 
set_attribute -name CLKFX_DIVIDE -value "8" -instance -type INTEGER I5_U_6_DCM_inst1 -design gatelevel 
set_attribute -name CLKFX_MULTIPLY -value "9" -instance -type INTEGER I5_U_6_DCM_inst1 -design gatelevel 
set_attribute -name CLKIN_DIVIDE_BY_2 -value "0" -instance -type DEFAULT I5_U_6_DCM_inst1 -design gatelevel 
set_attribute -name CLKIN_PERIOD -value "21.0" -instance -type DEFAULT I5_U_6_DCM_inst1 -design gatelevel 
set_attribute -name CLKOUT_PHASE_SHIFT -value "NONE" -instance -type STRING I5_U_6_DCM_inst1 -design gatelevel 
set_attribute -name CLK_FEEDBACK -value "1X" -instance -type STRING I5_U_6_DCM_inst1 -design gatelevel 
set_attribute -name DESKEW_ADJUST -value "SYSTEM_SYNCHRONOUS" -instance -type STRING I5_U_6_DCM_inst1 -design gatelevel 
set_attribute -name DFS_FREQUENCY_MODE -value "LOW" -instance -type STRING I5_U_6_DCM_inst1 -design gatelevel 
set_attribute -name DLL_FREQUENCY_MODE -value "LOW" -instance -type STRING I5_U_6_DCM_inst1 -design gatelevel 
set_attribute -name DUTY_CYCLE_CORRECTION -value "1" -instance -type DEFAULT I5_U_6_DCM_inst1 -design gatelevel 
set_attribute -name FACTORY_JF -value "X"C080"" -instance -type DEFAULT I5_U_6_DCM_inst1 -design gatelevel 
set_attribute -name PHASE_SHIFT -value "0" -instance -type INTEGER I5_U_6_DCM_inst1 -design gatelevel 
set_attribute -name STARTUP_WAIT -value "0" -instance -type DEFAULT I5_U_6_DCM_inst1 -design gatelevel 
set_attribute -name CLKDV_DIVIDE -value "2.0" -instance -type DEFAULT I5_U_6_DCM_inst2 -design gatelevel 
set_attribute -name CLKFX_DIVIDE -value "2" -instance -type INTEGER I5_U_6_DCM_inst2 -design gatelevel 
set_attribute -name CLKFX_MULTIPLY -value "2" -instance -type INTEGER I5_U_6_DCM_inst2 -design gatelevel 
set_attribute -name CLKIN_DIVIDE_BY_2 -value "1" -instance -type DEFAULT I5_U_6_DCM_inst2 -design gatelevel 
set_attribute -name CLKIN_PERIOD -value "21.0" -instance -type DEFAULT I5_U_6_DCM_inst2 -design gatelevel 
set_attribute -name CLKOUT_PHASE_SHIFT -value "NONE" -instance -type STRING I5_U_6_DCM_inst2 -design gatelevel 
set_attribute -name CLK_FEEDBACK -value "1X" -instance -type STRING I5_U_6_DCM_inst2 -design gatelevel 
set_attribute -name DESKEW_ADJUST -value "SYSTEM_SYNCHRONOUS" -instance -type STRING I5_U_6_DCM_inst2 -design gatelevel 
set_attribute -name DFS_FREQUENCY_MODE -value "LOW" -instance -type STRING I5_U_6_DCM_inst2 -design gatelevel 
set_attribute -name DLL_FREQUENCY_MODE -value "LOW" -instance -type STRING I5_U_6_DCM_inst2 -design gatelevel 
set_attribute -name DUTY_CYCLE_CORRECTION -value "1" -instance -type DEFAULT I5_U_6_DCM_inst2 -design gatelevel 
set_attribute -name FACTORY_JF -value "X"C080"" -instance -type DEFAULT I5_U_6_DCM_inst2 -design gatelevel 
set_attribute -name PHASE_SHIFT -value "0" -instance -type INTEGER I5_U_6_DCM_inst2 -design gatelevel 
set_attribute -name STARTUP_WAIT -value "0" -instance -type DEFAULT I5_U_6_DCM_inst2 -design gatelevel 
set_attribute -name ram_class_key -value "ram_new2" -instance I5_U_5_merged -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(0) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(0) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(0) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(1) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(1) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(1) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(2) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(2) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(2) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(3) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(3) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(3) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(4) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(4) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(4) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(5) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(5) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(5) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(6) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(6) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(6) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(7) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(7) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(7) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(8) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(8) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(8) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(9) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(9) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(9) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(10) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(10) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(10) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(11) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(11) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(11) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(12) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(12) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(12) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(13) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(13) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(13) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(14) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(14) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(14) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FD_bdbuf(15) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FD_bdbuf(15) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FD_bdbuf(15) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_SLWR_obuf -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_SLWR_obuf -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_SLWR_obuf -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_SLRD_obuf -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_SLRD_obuf -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_SLRD_obuf -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_SLOE_obuf -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_SLOE_obuf -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_SLOE_obuf -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_PKTEND_obuf -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_PKTEND_obuf -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_PKTEND_obuf -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FIFOADR_obuf(1) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance USB_FIFOADR_obuf(1) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance USB_FIFOADR_obuf(1) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance FP_LED_obuf(0) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance FP_LED_obuf(0) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance FP_LED_obuf(0) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance FP_LED_obuf(1) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance FP_LED_obuf(1) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance FP_LED_obuf(1) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance FP_LED_obuf(2) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance FP_LED_obuf(2) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance FP_LED_obuf(2) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance FP_LED_obuf(3) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance FP_LED_obuf(3) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance FP_LED_obuf(3) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_RESET_obuf -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_RESET_obuf -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_RESET_obuf -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P2B_obuf(0) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P2B_obuf(0) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P2B_obuf(0) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P2B_obuf(1) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P2B_obuf(1) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P2B_obuf(1) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P2B_obuf(2) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P2B_obuf(2) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P2B_obuf(2) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P2B_obuf(3) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P2B_obuf(3) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P2B_obuf(3) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P2B_obuf(4) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P2B_obuf(4) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P2B_obuf(4) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P2B_obuf(5) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P2B_obuf(5) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P2B_obuf(5) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P2B_obuf(6) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P2B_obuf(6) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P2B_obuf(6) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P2B_obuf(7) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P2B_obuf(7) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P2B_obuf(7) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P2B_obuf(8) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P2B_obuf(8) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P2B_obuf(8) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P2B_obuf(9) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P2B_obuf(9) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P2B_obuf(9) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P2B_obuf(10) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P2B_obuf(10) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P2B_obuf(10) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P2B_obuf(11) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P2B_obuf(11) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P2B_obuf(11) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P2B_obuf(12) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P2B_obuf(12) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P2B_obuf(12) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P2B_obuf(13) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P2B_obuf(13) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P2B_obuf(13) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P1B_obuf(0) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P1B_obuf(0) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P1B_obuf(0) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P1B_obuf(1) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P1B_obuf(1) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P1B_obuf(1) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P1B_obuf(2) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P1B_obuf(2) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P1B_obuf(2) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P1B_obuf(3) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P1B_obuf(3) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P1B_obuf(3) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P1B_obuf(4) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P1B_obuf(4) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P1B_obuf(4) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P1B_obuf(5) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P1B_obuf(5) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P1B_obuf(5) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P1B_obuf(6) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P1B_obuf(6) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P1B_obuf(6) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P1B_obuf(7) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P1B_obuf(7) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P1B_obuf(7) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P1B_obuf(8) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P1B_obuf(8) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P1B_obuf(8) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P1B_obuf(9) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P1B_obuf(9) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P1B_obuf(9) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P1B_obuf(10) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P1B_obuf(10) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P1B_obuf(10) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P1B_obuf(11) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P1B_obuf(11) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P1B_obuf(11) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P1B_obuf(12) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P1B_obuf(12) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P1B_obuf(12) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_P1B_obuf(13) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_P1B_obuf(13) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_P1B_obuf(13) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_CLKP_obuf -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_CLKP_obuf -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_CLKP_obuf -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance DAC1_CLKN_obuf -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance DAC1_CLKN_obuf -design gatelevel 
set_attribute -name DRIVE -value "12" -instance DAC1_CLKN_obuf -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance ADC_MODE_obuf(0) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance ADC_MODE_obuf(0) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance ADC_MODE_obuf(0) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance ADC_MODE_obuf(1) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance ADC_MODE_obuf(1) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance ADC_MODE_obuf(1) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance ADC_MODE_obuf(2) -design gatelevel 
set_attribute -name SLEW -value "SLOW" -instance ADC_MODE_obuf(2) -design gatelevel 
set_attribute -name DRIVE -value "12" -instance ADC_MODE_obuf(2) -design gatelevel 
set_attribute -name IOSTANDARD -value "LVCMOS25" -instance USB_FLAGC_ibuf -design gatelevel 
set_attribute -name PART -value "2VP4fg456-7" -type string /work/top/struct -design gatelevel 
set_attribute -name state_vector -value "CURRENT_STATE" /work/dac_pattern_gen_l0_r13_3FFF_l0_r13_0000/fsm_behav -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix16878z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix15881z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix14884z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix13887z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix12890z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix11893z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix10896z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix9899z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix8902z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix7905z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix33375z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix34372z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix35369z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix36366z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix37363z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix38360z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix39357z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix40354z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix41351z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix42348z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix44344z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix45341z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix46338z64220 -design gatelevel 
set_attribute -name WRITE_MODE_A -value "READ_FIRST" -instance I5_U_5_merged/ix47335z64220 -design gatelevel 

set_attribute -name state_vector -value "current_state" /work/usb_translator_notri/fsm_unfold_1368 -design gatelevel 

##################
# Clocks
##################
create_clock { I5_U_6_BUFMUX_DCM1CLKOUT/O } -domain ClockDomain0 -name I5_U_6_BUFMUX_DCM1CLKOUT/O -period 4.444444 -waveform { 0.000000 2.222222 } -design gatelevel 
create_clock { USB_IFCLK } -domain ClockDomain0 -name USB_IFCLK -period 5.000000 -waveform { 0.000000 2.500000 } -design gatelevel 
create_clock { ADC_CLKOUTAP } -domain ClockDomain1 -name ADC_CLKOUTAP -period 5.000000 -waveform { 0.000000 2.500000 } -design gatelevel 

